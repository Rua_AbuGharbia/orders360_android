package com.telephoenic.orders360.mvp;

import android.content.Context;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnResponseImpl;
import com.telephoenic.orders360.controller.server.controller.GetWebserviceTask;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.DialogWrapper;

import org.springframework.util.LinkedMultiValueMap;


public class InteractorImpl extends OnResponseImpl implements Interactor {

    private Context context;

    public InteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void sendRequestToServer(Object objectRequest, final OnFinishedListener listener , Class webserviceClass) {

        try{

            new GetWebserviceTask(context, webserviceClass) {
                @Override
                public void onResponseReady(Object response) {
                    super.onResponseReady(response);

                    if (response != null) {
                        if(response instanceof LinkedMultiValueMap){
                            listener.onSuccess((LinkedMultiValueMap) response);
                        } else if (response instanceof String) {
                            listener.onSuccess(response.toString());
                        } else if (response instanceof ResponseWrapper) {
                            listener.onError();
                            onSuccess(response, context);
                        }
                    } else {
                        listener.onError();
                        DialogWrapper dialogWrapper = new DialogWrapper();
                        dialogWrapper.setContext(context);
                        dialogWrapper.setTitle(context.getString(R.string.titles_dialog_general_error));
                        dialogWrapper.setMessage(context.getString(R.string.unknown_error));
                        dialogWrapper.setIsFinished(false);
                        AppUtils.showAlertDialog(dialogWrapper);
                    }
                }

                @Override
                public void exceptionHandler(Exception exception) {
                    listener.onError();
                    onException(exception, context);
                }

            }.execute(JsonUtils.toJsonString(objectRequest));
        }catch (Exception exception){

        }
    }

    @Override
    public void onSuccess(Object response, Context context) {
        super.onSuccess(response, context);
    }

    @Override
    public void onException(Exception exception, Context context) {
        super.onException(exception, context);
    }
}
