/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.telephoenic.orders360.mvp;

import android.content.Context;

import org.springframework.util.LinkedMultiValueMap;


public class PresenterImpl implements Presenter, Interactor.OnFinishedListener {

    private MVPView MVPView;
    private Interactor interactor;

    public PresenterImpl(MVPView MVPView, Context context) {
        this.MVPView = MVPView;
        this.interactor = new InteractorImpl(context);
    }

    @Override
    public void validateCredentials(Object objectRequest ,Class webserviceClass) {
        if (MVPView != null) {
            MVPView.showProgress();
        }

        interactor.sendRequestToServer(objectRequest, this ,webserviceClass);
    }

    @Override
    public void onDestroy() {
        MVPView = null;
    }


    @Override
    public void onSuccess(String response) {
        if (MVPView != null) {
            MVPView.hideProgress();
            MVPView.navigate(response);
        }
    }

    @Override
    public void onSuccess(LinkedMultiValueMap response) {
        if (MVPView != null) {
            MVPView.hideProgress();
            MVPView.navigate(response);
        }
    }

    @Override
    public void onError() {
        if (MVPView != null) {
            MVPView.hideProgress();
        }
    }

}
