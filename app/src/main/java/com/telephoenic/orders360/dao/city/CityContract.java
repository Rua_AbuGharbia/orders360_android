package com.telephoenic.orders360.dao.city;

import com.telephoenic.orders360.controller.server.model.CityModel;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface CityContract {
    Single<List<CityModel>> getAllCities(Long countryId);
}
