package com.telephoenic.orders360.dao.salesAgent;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.User;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/8/2019.
 */

public class SalesAgentDao implements SalesAgentContract {

   private VendorAPI vendorAPI ;

    public SalesAgentDao() {
        vendorAPI = ApiService.getApiService(VendorAPI.class);
    }

    @Override
    public Single<User> getVendor(Long userId) {
        return vendorAPI.getVendor(userId);
    }

    interface VendorAPI {
        @GET("sales_agent/find/{id}")
        Single<User> getVendor(@Path("id") Long id);
    }

}
