package com.telephoenic.orders360.dao.promotion;

import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;

import io.reactivex.Single;
import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface PromotionContract {

    Single<PromotionWraper> getAllPromotion(int pageNumber, int pageSize, long vendorId, long groupId, String search, ProductFilter productFilter);

    Single<ResponseBody> getPromotion(String prodId , String groupId);
}
