package com.telephoenic.orders360.dao.ship;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalUnsuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.FromToSerialModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentResponse;
import com.telephoenic.orders360.controller.server.model.shiporder.SerialsQuantity;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SingleSerialModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ShipDao implements ShipContract {
    private ShipApi shipApi;

    public ShipDao() {
        shipApi = ApiService.getApiService(ShipApi.class);
    }


    @Override
    public Single<List<ShipModel>> getShipOrders(int pageIndex, int pageSize, Long deliveryUserId, String statusCode, ShipmentOrderFilter shipmentOrderFilter,String search_term) {
        return shipApi.getShipOrders(pageIndex, pageSize, deliveryUserId, statusCode, shipmentOrderFilter,search_term);
    }

    @Override
    public Single<List<ShipItemsModel>> getShipItemOrder(Long id) {
        return shipApi.getShipItemOrder(id);
    }

    @Override
    public Completable shipItems(Long shipmentItemId) {
        return shipApi.shipItems(shipmentItemId);
    }

    @Override
    public Completable saveSingleSerials(long itemId, List<SingleSerialModel> serials) {
        return shipApi.saveSingleSerials(itemId, serials);
    }

    @Override
    public Completable saveQuantitySerials(long itemId, List<SerialsQuantity> serials) {
        return shipApi.saveQuantitySerials(itemId, serials);
    }

    @Override
    public Completable saveRangeSerials(long itemId, List<FromToSerialModel> serials) {
        return shipApi.saveRangeSerials(itemId, serials);
    }

    @Override
    public Completable confirmArrivalSuccessful(long shipmentId, ArrivalSuccessfulModel arrivalSuccessfulModel) {
        return shipApi.confirmArrivalSuccessful(shipmentId, arrivalSuccessfulModel);
    }

    @Override
    public Completable confirmArrivalUnSuccessful(long shipmentId, ArrivalUnsuccessfulModel arrivalUnsuccessfulModel) {
        return shipApi.confirmArrivalUnSuccessful(shipmentId, arrivalUnsuccessfulModel);
    }

    @Override
    public Single<List<OrderRejectedReason>> getReason(String code, Long vendorId) {
        return shipApi.getReason(code, vendorId);
    }

    @Override
    public Single<PaymentResponse> getPaymentMethod() {
        return shipApi.getPaymentMethod();
    }

    //Status Code
    //RTS -> Ready to ship
    //SH -> Shipped
    interface ShipApi {
        @POST("shipment_order/find_shipments_by_status/{pageNum}/{pageSize}/{deliveryUserId}/{statusCode}")
        Single<List<ShipModel>> getShipOrders(@Path("pageNum") int pageNum,
                                              @Path("pageSize") int pageSize,
                                              @Path("deliveryUserId") Long deliveryUserId,
                                              @Path("statusCode") String statusCode,
                                              @Body ShipmentOrderFilter shipmentOrderFilter,
                                              @Query("search_term") String search_term);

        @GET("shipment_order_item/find_shipment_detail_with_serials/{shipmentItemId}")
        Single<List<ShipItemsModel>> getShipItemOrder(@Path("shipmentItemId") long shipmentItemId);

        @PUT("shipment_order/change_to_shipped/{shipmentItemId}")
        Completable shipItems(@Path("shipmentItemId") long shipmentItemId);

        @POST("shipment_serial_single/save/{itemId}")
        Completable saveSingleSerials(@Path("itemId") long itemId, @Body List<SingleSerialModel> serials);

        @POST("shipment_serial_quantity/save/{itemId}")
        Completable saveQuantitySerials(@Path("itemId") long itemId, @Body List<SerialsQuantity> serials);

        @POST("shipment_serial_range/save/{itemId}")
        Completable saveRangeSerials(@Path("itemId") long itemId, @Body List<FromToSerialModel> serials);

        @POST("shipment_arrival_successful/confirm_arrival/{shipmentItemId}")
        Completable confirmArrivalSuccessful(@Path("shipmentItemId") long shipmentItemId, @Body ArrivalSuccessfulModel arrivalSuccessfulModel);

        @POST("shipment_arrival_unsuccessful/confirm_arrival/{shipmentItemId}")
        Completable confirmArrivalUnSuccessful(@Path("shipmentItemId") long shipmentItemId, @Body ArrivalUnsuccessfulModel arrivalUnsuccessfulModel);

        @GET("order-rejected-reason/find_reasons_by_parent/{code}/{vendorId}")
        Single<List<OrderRejectedReason>> getReason(@Path("code") String code, @Path("vendorId") Long vendorId);

        @GET("payment_method/all")
        Single<PaymentResponse> getPaymentMethod();

    }
}
