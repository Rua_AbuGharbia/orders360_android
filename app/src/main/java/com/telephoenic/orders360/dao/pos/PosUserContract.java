package com.telephoenic.orders360.dao.pos;

import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.PosGroupModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.controller.server.model.User;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/9/2019.
 */

public interface PosUserContract {

    Maybe<User> getPosUserByMobile(String mobileNumber, long userId);

    Completable sendPosUser(long userId , long vendorId , User user);

    Completable upDatePosLocation(User user);

    Completable updateUserProfile(User user);

    Single<ResponseBody> updateRegistrationID (UpdateGPSRequest updateGPSRequest);

    Single<PosGroupModel> getPosGroupId(Long posId, Long vendorId);

    Single<ResponseBody> getAllPoints(String posId , String vendorId);

}
