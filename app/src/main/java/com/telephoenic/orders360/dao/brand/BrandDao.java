package com.telephoenic.orders360.dao.brand;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.BrandModel;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class BrandDao implements BrandContract {

    private BrandAPI brandAPI;

    public BrandDao() {
        brandAPI = ApiService.getApiService(BrandAPI.class);
    }

    @Override
    public Single<List<BrandWrapper>> getBrands(Long vendorId) {
        return brandAPI.getAllBrands(vendorId, AppConstants.IS_PROMOTION);
    }

    interface BrandAPI {
        @GET("brand/get-mobile-brands/{vendorId}")
        Single<List<BrandWrapper>> getAllBrands(@Path("vendorId") Long vendorId, @Query("isPromotion") Boolean isPromotion);
    }
}
