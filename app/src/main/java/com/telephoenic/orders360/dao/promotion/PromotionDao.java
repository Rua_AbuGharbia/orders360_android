package com.telephoenic.orders360.dao.promotion;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class PromotionDao implements PromotionContract {

    private PromotionAPI promotionAPI;

    public PromotionDao() {
        promotionAPI = ApiService.getApiService(PromotionAPI.class);
    }

    @Override
    public Single<PromotionWraper> getAllPromotion(int pageNumber, int pageSize, long vendorId, long groupId, String search, ProductFilter productFilter) {
        return promotionAPI.getAllPromotion(pageNumber, pageSize, vendorId, groupId, search, productFilter);
    }

    @Override
    public Single<ResponseBody> getPromotion(String prodId, String groupId) {
        return promotionAPI.getPromotion(prodId, groupId);
    }

    interface PromotionAPI {
        @POST("promotion/product_with_promotion/{pageNum}/{pageSize}/{vendorId}/{groupId}")
        Single<PromotionWraper> getAllPromotion(@Path("pageNum") int pageNum
                , @Path("pageSize") int pageSize, @Path("vendorId") long vendorId
                , @Path("groupId") long groupId, @Query("name") String search, @Body ProductFilter productFilter);

        @GET("promotion/by_pos_and_prod/{prodId}/{groupId}")
        Single<ResponseBody> getPromotion(@Path("prodId") String prodId, @Path("groupId") String groupId);

    }
}
