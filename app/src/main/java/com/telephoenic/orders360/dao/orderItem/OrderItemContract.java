package com.telephoenic.orders360.dao.orderItem;

import com.telephoenic.orders360.controller.server.model.OrderItemModel;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface OrderItemContract {
    Single<OrderItemModel> getMyOrderItem(String itemId);
}
