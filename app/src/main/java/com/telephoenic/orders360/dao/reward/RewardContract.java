package com.telephoenic.orders360.dao.reward;

import io.reactivex.Single;
import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/3/2019.
 */

public interface RewardContract {

    Single<ResponseBody> redeemPoints (String posId ,String productId ,String vendorId);
}
