package com.telephoenic.orders360.dao.cart;

import com.telephoenic.orders360.controller.server.model.AddToCartModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public interface CartContract {

    Single<ResponseBody> addToCart(List<AddToCartModel> addToCartModel);

}
