package com.telephoenic.orders360.dao.order;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.controller.server.model.OrderModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class OrderDao implements OrderContract {
    private OrderAPI orderAPI;

    public OrderDao() {
        orderAPI = ApiService.getApiService(OrderAPI.class);
    }


    @Override
    public Single<ResponseBody> getPOSOrder(String page, String pageSize, String posId, String vendorId) {
        return orderAPI.getPOSOrder(page, pageSize, posId, vendorId);
    }

    @Override
    public Single<ResponseBody> getSalesOrder(int page, int pageSize, Long salesAgentId) {
        return orderAPI.getSalesOrder(page, pageSize, salesAgentId);
    }

    @Override
    public Single<ResponseBody> saveOrder(List<OrderModel> orderModelsRequest) {
        return orderAPI.saveOrder(orderModelsRequest);
    }

    interface OrderAPI {

        @GET("order/all_order/{page}/{pageSize}/{posId}/{vendorId}")
        Single<ResponseBody> getPOSOrder (@Path("page") String page, @Path("pageSize")String pageSize,
                                          @Path("posId")String posId, @Path("vendorId")String vendorId);

        @GET("order/order_for_pos_by_sales_agent/{page}/{pageSize}/{salesAgentId}")
        Single<ResponseBody> getSalesOrder(@Path("page") int page, @Path("pageSize") int pageSize, @Path("salesAgentId") Long salesAgentId);

        @POST("order/save_all")
        Single<ResponseBody> saveOrder(@Body List<OrderModel> orderModelsRequest);

    }

}
