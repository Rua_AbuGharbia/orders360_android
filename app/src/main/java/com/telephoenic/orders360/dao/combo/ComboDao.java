package com.telephoenic.orders360.dao.combo;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.BundleWraper;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class ComboDao implements ComboContract {

    private ComboAPI comboAPI ;

    public ComboDao() {
        comboAPI = ApiService.getApiService(ComboAPI.class);
    }

    @Override
    public Single<BundleWraper> getAllBundle(int pageNumber, int pageSize, long vendorId, long userId, String search) {
        return comboAPI.getAllBundle(pageNumber, pageSize, vendorId, userId, search);
    }

    interface ComboAPI {
        @GET("combo/get_combo_by_name/{pageNum}/{pageSize}/{vendorId}/{userID}")
        Single<BundleWraper> getAllBundle(@Path("pageNum") int pageNum
                , @Path("pageSize") int pageSize, @Path("vendorId") long vendorId
                , @Path("userID") long userID, @Query("name") String search);
    }
}
