package com.telephoenic.orders360.dao.cart;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class CartDao implements CartContract {
    private CartApi cartApi;

    public CartDao() {
        cartApi = ApiService.getApiService(CartApi.class);
    }

    @Override
    public Single<ResponseBody> addToCart(List<AddToCartModel> addToCartModel) {
        return cartApi.addItemCart(addToCartModel);
    }

    interface CartApi {
        @POST("cart/save_cart")
        Single<ResponseBody> addItemCart(@Body List<AddToCartModel> addToCartModel);
    }
}
