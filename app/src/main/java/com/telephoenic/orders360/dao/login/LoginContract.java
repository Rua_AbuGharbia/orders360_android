package com.telephoenic.orders360.dao.login;

import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;

import io.reactivex.Single;
import okhttp3.ResponseBody;


/**
 * Created by Ru'a on 11/26/2019.
 */

public interface LoginContract  {
    Single<PosProfileWrapper> login(String base64Name );
}
