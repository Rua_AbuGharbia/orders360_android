package com.telephoenic.orders360.dao.user;

import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.UserPassword;

import io.reactivex.Single;

public interface UserContract {
    Single<Boolean> checkMobileNumberAvailability(String mobileNumber);

    Single<String> sendPassCode(String mobileNumber, long otp);

    Single<Boolean> changePasswordFromOTP(UserPassword userPassword);

    Single<Boolean> changePasswordInsideApp(UserPassword userPassword);

    Single<User> getUserProfile(long id);
}
