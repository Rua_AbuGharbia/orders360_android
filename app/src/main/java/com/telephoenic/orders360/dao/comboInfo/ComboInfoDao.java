package com.telephoenic.orders360.dao.comboInfo;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class ComboInfoDao implements ComboInfoContract {

    private ComboInfoAPI comboInfoAPI ;

    public ComboInfoDao() {
        comboInfoAPI = ApiService.getApiService(ComboInfoAPI.class);
    }

    @Override
    public Single<List<ComboInfoWrapper>> getBundleInfo(String comboId) {
        return comboInfoAPI.getBundleInfo(comboId);
    }

    interface ComboInfoAPI {
        @GET("combo_info/items/{comboId}")
        Single<List<ComboInfoWrapper>> getBundleInfo(@Path("comboId") String comboId);

    }
}
