package com.telephoenic.orders360.dao.product;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ProductDao implements ProductContract {
   private ProductApi productApi;
    public ProductDao (){
        productApi = ApiService.getApiService(ProductApi.class);
    }
    @Override
    public Single<List<ProductWrapper>> getAllProduct(int pageNumber, int pageSize, long vendorId, long posId,long groupId, String search, ProductFilter productFilter) {
        return productApi.getAllProduct(pageNumber, pageSize, vendorId, posId, groupId, search,productFilter);
    }

    @Override
    public Single<ResponseBody> getProductWithPoints(String pageNum, String pageSize, String vendorId) {
        return productApi.getProductWithPoints(pageNum, pageSize, vendorId);
    }

    interface ProductApi {
        @POST("product/get_all_products/{pageNum}/{pageSize}/{vendorId}/{posId}/{posGroupId}")
        Single<List<ProductWrapper>> getAllProduct(@Path("pageNum") int pageNum
                , @Path("pageSize") int pageSize, @Path("vendorId") long vendorId
                , @Path("posId") long posId,@Path("posGroupId")long posGroupId, @Query("name") String search, @Body  ProductFilter productFilter);


        @GET("product/prod_reward_points/{pageNum}/{pageSize}/{vendorId}")
        Single<ResponseBody> getProductWithPoints(@Path("pageNum")String pageNum ,@Path("pageSize")String pageSize ,@Path("vendorId")String vendorId);

    }
}
