package com.telephoenic.orders360.dao.posGroup;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.GroupModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class PosGroupDao implements PosGroupContract {
    private PosGroupAPI posGroupAPI;

    public PosGroupDao() {
        posGroupAPI = ApiService.getApiService(PosGroupAPI.class);
    }

    @Override
    public Single<List<GroupModel>> getAllGroups(Long vendorId) {
        return posGroupAPI.getAllGroups(vendorId);
    }

    interface PosGroupAPI {
        @GET("pos_group/allPosGroups/{vendorId}")
        Single<List<GroupModel>> getAllGroups(@Path("vendorId") Long vendorId);
    }
}
