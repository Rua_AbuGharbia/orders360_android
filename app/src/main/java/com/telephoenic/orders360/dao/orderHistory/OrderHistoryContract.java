package com.telephoenic.orders360.dao.orderHistory;

import com.telephoenic.orders360.controller.server.model.CancelOrderModel;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface OrderHistoryContract {

    Single<List<OrderHistoryModel>> getOrderHistory(int id);

    Single<List<OrderRejectedReason>> getReason(Long vendorId);

    Completable cancelOrder(CancelOrderModel cancelOrderModel);

    Single<List<ShipModel>> getShipOrders(int pageIndex, int pageSize, Long orderId);

}
