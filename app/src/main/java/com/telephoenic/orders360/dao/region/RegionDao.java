package com.telephoenic.orders360.dao.region;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.RegionModel;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class RegionDao implements RegionContract {

    private RegionAPI regionAPI;

    public RegionDao() {
        regionAPI = ApiService.getApiService(RegionAPI.class);
    }

    @Override
    public Single<ResponseBody> getAllRegionsAtSelectedCity(RegionModel regionModel) {
        return regionAPI.getAllRegionsAtSelectedCity(regionModel);
    }

    interface RegionAPI {
        @POST("region/find-by-example")
        Single<ResponseBody> getAllRegionsAtSelectedCity(@Body RegionModel regionModel);
    }

}
