package com.telephoenic.orders360.dao.ship;

import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalUnsuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.FromToSerialModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentMethodModel;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentResponse;
import com.telephoenic.orders360.controller.server.model.shiporder.SerialsQuantity;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SingleSerialModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface ShipContract {
    Single<List<ShipModel>> getShipOrders(int pageIndex, int pageSize, Long deliveryUserId, String statusCode, ShipmentOrderFilter shipmentOrderFilter,String search_term);

    Single<List<ShipItemsModel>> getShipItemOrder(Long id);

    Completable shipItems(Long shipmentItemId);

    Completable saveSingleSerials(long itemId, List<SingleSerialModel> serials);

    Completable saveQuantitySerials(long itemId, List<SerialsQuantity> serials);

    Completable saveRangeSerials(long itemId, List<FromToSerialModel> serials);

    Completable confirmArrivalSuccessful(long shipmentId, ArrivalSuccessfulModel arrivalSuccessfulModel);

    Completable confirmArrivalUnSuccessful(long shipmentId, ArrivalUnsuccessfulModel arrivalUnsuccessfulModel);

    Single<List<OrderRejectedReason>> getReason(String code, Long vendorId);

    Single<PaymentResponse> getPaymentMethod();


}
