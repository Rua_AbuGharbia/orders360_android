package com.telephoenic.orders360.dao.survey;

import com.telephoenic.orders360.controller.server.model.survey.SurveyDetailsResponse;
import com.telephoenic.orders360.controller.server.model.survey.SurveyQuestionWrapper;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/5/2019.
 */

public interface SurveyContract {
    Single<ResponseBody> getAllSurveys(String pageNumber ,String pageSize, String vendorId, String posUserId);

    Single<SurveyDetailsResponse> getSurveyDetails(Long surveyId ,Long participantId);

    Single<ResponseBody> submitSurvey(Long surveyId ,Long participantId ,List<SurveyQuestionWrapper> questionsWithAnswers);

}
