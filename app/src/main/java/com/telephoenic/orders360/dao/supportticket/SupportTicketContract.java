package com.telephoenic.orders360.dao.supportticket;

import com.telephoenic.orders360.controller.server.model.SendTicketRequest;
import com.telephoenic.orders360.controller.server.model.SupportTicketWrapper;
import com.telephoenic.orders360.controller.server.model.TicketModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public interface SupportTicketContract {

    Single<SupportTicketWrapper> getType(int pageNumber, int pageSize, Long vendorId);

    Single<List<TicketModel>> getAllTicket (String posId , String vendorID);

    Single<ResponseBody> saveTicket (SendTicketRequest sendTicketRequest);
}
