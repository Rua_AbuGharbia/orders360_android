package com.telephoenic.orders360.dao.deliveryuser;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.WarehouseModel;
import com.telephoenic.orders360.dao.salesAgent.SalesAgentContract;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/8/2019.
 */

public class DeliveryUserDao implements DeliveryUserContract {

    private VendorAPI vendorAPI;

    public DeliveryUserDao() {
        vendorAPI = ApiService.getApiService(VendorAPI.class);
    }

    @Override
    public Single<User> getVendor(Long userId) {
        return vendorAPI.getVendor(userId);
    }

    @Override
    public Single<List<WarehouseModel>> getWarehouse(Long userId) {
        return vendorAPI.getWarehouse(userId);
    }

    @Override
    public Single<List<CityModel>> getCity() {
        return vendorAPI.getCity();
    }

    @Override
    public Single<List<RegionModel>> getRegion(Long cityId) {
        return vendorAPI.getRegion(cityId);
    }

    interface VendorAPI {
        @GET("delivery_user/find/{id}")
        Single<User> getVendor(@Path("id") Long id);

        @GET("warehouse/find_by_vendorId/{id}")
        Single<List<WarehouseModel>> getWarehouse(@Path("id") Long id);

        @GET("city/find_city_by_delivery_user")
        Single<List<CityModel>> getCity();

        @GET("region/find_regions_by_city/{id}")
        Single<List<RegionModel>> getRegion(@Path("id") Long cityId);
    }

}
