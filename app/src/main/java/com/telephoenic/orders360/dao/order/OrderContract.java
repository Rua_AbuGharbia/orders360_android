package com.telephoenic.orders360.dao.order;

import com.telephoenic.orders360.controller.server.model.OrderModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public interface OrderContract {

    Single<ResponseBody> getPOSOrder(String page, String pageSize, String posId, String vendorId);

    Single<ResponseBody> getSalesOrder(int page , int pageSize , Long salesAgentId);

    Single<ResponseBody> saveOrder(List<OrderModel> orderModelsRequest);

}
