package com.telephoenic.orders360.dao.comboInfo;

import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface ComboInfoContract {
    Single<List<ComboInfoWrapper>> getBundleInfo (String comboId);
}
