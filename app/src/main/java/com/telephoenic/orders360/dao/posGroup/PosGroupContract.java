package com.telephoenic.orders360.dao.posGroup;

import com.telephoenic.orders360.controller.server.model.GroupModel;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface PosGroupContract {
    Single<List<GroupModel>> getAllGroups(Long vendorId);
}
