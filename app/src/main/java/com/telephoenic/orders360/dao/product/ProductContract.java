package com.telephoenic.orders360.dao.product;

import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public interface ProductContract {

    Single<List<ProductWrapper>> getAllProduct(int pageNumber, int pageSize, long vendorId, long posId,long groupId, String search, ProductFilter productFilter);

    Single<ResponseBody> getProductWithPoints (String pageNum , String pageSize ,String vendorId);

}
