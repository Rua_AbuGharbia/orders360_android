package com.telephoenic.orders360.dao.orderItem;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.dao.order.OrderDao;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class OrderItemDao implements OrderItemContract {

    OrderItemAPI orderItemAPI ;

    public OrderItemDao() {
        orderItemAPI = ApiService.getApiService(OrderItemAPI.class);
    }

    @Override
    public Single<OrderItemModel> getMyOrderItem(String itemId) {
        return orderItemAPI.orderHistory(itemId);
    }

    interface OrderItemAPI {
        @GET("order_item/order_item_detail/{orderId}")
        Single<OrderItemModel> orderHistory(@Path("orderId") String itemId);
    }
}
