package com.telephoenic.orders360.dao.city;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.CityModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class CityDao implements CityContract {

    CityAPI cityAPI;

    public CityDao() {
        cityAPI = ApiService.getApiService(CityAPI.class);
    }

    @Override
    public Single<List<CityModel>> getAllCities(Long countryId) {
        return cityAPI.getAllCities(countryId);
    }

    interface CityAPI {
        @GET("city/findByCountryId/{countryId}")
        Single<List<CityModel>> getAllCities(@Path("countryId") Long countryId);
    }
}
