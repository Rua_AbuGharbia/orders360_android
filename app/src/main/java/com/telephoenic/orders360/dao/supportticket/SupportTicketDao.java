package com.telephoenic.orders360.dao.supportticket;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.SendTicketRequest;
import com.telephoenic.orders360.controller.server.model.SupportTicketWrapper;
import com.telephoenic.orders360.controller.server.model.TicketModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class SupportTicketDao implements SupportTicketContract {
    private SupportTicketApi supportTicketApi;

    public SupportTicketDao() {
        supportTicketApi = ApiService.getApiService(SupportTicketApi.class);
    }

    @Override
    public Single<SupportTicketWrapper> getType(int pageNumber, int pageSize, Long vendorId) {
        return supportTicketApi.getType(pageNumber, pageSize, vendorId);
    }

    @Override
    public Single<List<TicketModel>> getAllTicket(String posId, String vendorID) {
        return supportTicketApi.getAllTicket(posId, vendorID);
    }

    @Override
    public Single<ResponseBody> saveTicket(SendTicketRequest sendTicketRequest) {
        return supportTicketApi.saveTicket(sendTicketRequest);
    }

    interface SupportTicketApi {
        @GET("ticket-type/get-mobile-tickets/{pageNumber}/{pageSize}/{vendorId}")
        Single<SupportTicketWrapper> getType(@Path("pageNumber") int pageNumber, @Path("pageSize") int pageSize,
                                             @Path("vendorId") Long vendorId);

        @GET("support_ticket/ticket_for_pos/{posId}/{vendorId}")
        Single<List<TicketModel>> getAllTicket (@Path("posId") String posId ,@Path("vendorId") String vendorId);

        @POST("support_ticket/save")
        Single<ResponseBody> saveTicket (@Body SendTicketRequest sendTicketRequest);
    }
}
