package com.telephoenic.orders360.dao.brand;

import com.telephoenic.orders360.controller.server.model.BrandWrapper;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface BrandContract {

    Single<List<BrandWrapper>> getBrands(Long vendorId);
}
