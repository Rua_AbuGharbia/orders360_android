package com.telephoenic.orders360.dao.user;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.UserPassword;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class UserDao implements UserContract {
    private UserApi userApi;

    public UserDao() {
        userApi = ApiService.getApiService(UserApi.class);
    }

    @Override
    public Single<Boolean> checkMobileNumberAvailability(String mobileNumber) {
        return userApi.checkMobileNumber(mobileNumber);
    }

    @Override
    public Single<String> sendPassCode(String mobileNumber, long otp) {
        return userApi.sendPassCode(mobileNumber, otp);
    }

    @Override
    public Single<Boolean> changePasswordFromOTP(UserPassword userPassword) {
        return userApi.changePasswordFromOTP(userPassword);
    }

    @Override
    public Single<Boolean> changePasswordInsideApp(UserPassword userPassword) {
        return userApi.changePasswordInsideApp(userPassword);
    }

    @Override
    public Single<User> getUserProfile(long id) {
        return userApi.getUserProfile(id);
    }

    interface UserApi {
        @GET("user/send_otp/{mobileNo}")
        Single<Boolean> checkMobileNumber(@Path("mobileNo") String mobileNumber);

        @GET("user/check_otp/{mobileNo}/{otp}")
        Single<String> sendPassCode(@Path("mobileNo") String mobileNo, @Path("otp") long otp);

        @POST("user/confirm_change_password")
        Single<Boolean> changePasswordFromOTP(@Body UserPassword userPassword);

        @POST("user/change_password")
        Single<Boolean> changePasswordInsideApp(@Body UserPassword userPassword);

        @GET("user/find/{id}")
        Single<User> getUserProfile(@Path("id") long id);
    }
}
