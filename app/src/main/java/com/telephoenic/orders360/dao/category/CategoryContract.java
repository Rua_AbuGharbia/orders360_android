package com.telephoenic.orders360.dao.category;

import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;

import java.util.List;

import io.reactivex.Single;

public interface CategoryContract {

    Single<List<CategoryNodeWrapper>> getCategory(Long vendorId);

}
