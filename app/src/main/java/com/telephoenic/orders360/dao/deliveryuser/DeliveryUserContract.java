package com.telephoenic.orders360.dao.deliveryuser;

import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.WarehouseModel;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/8/2019.
 */

public interface DeliveryUserContract {
    Single<User> getVendor(Long userId);

    Single<List<WarehouseModel>> getWarehouse(Long userId);

    Single<List<CityModel>> getCity();

    Single<List<RegionModel>> getRegion(Long cityId);
}
