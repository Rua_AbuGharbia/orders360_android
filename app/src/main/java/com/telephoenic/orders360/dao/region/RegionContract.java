package com.telephoenic.orders360.dao.region;

import com.telephoenic.orders360.controller.server.model.RegionModel;

import io.reactivex.Single;
import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface RegionContract {

    Single<ResponseBody> getAllRegionsAtSelectedCity(RegionModel regionModel);

}
