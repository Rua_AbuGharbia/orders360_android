package com.telephoenic.orders360.dao.essentialData;

import com.telephoenic.orders360.controller.server.model.EssentialData;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/3/2019.
 */

public interface EssentialDataContract {
    Single<EssentialData> getEssentialData (long userId);
}
