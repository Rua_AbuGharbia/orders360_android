package com.telephoenic.orders360.dao.essentialData;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.EssentialData;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/3/2019.
 */

public class EssentialDataDao implements EssentialDataContract {
    private EssentialDataAPI essentialDataAPI ;

    public EssentialDataDao() {
        essentialDataAPI = ApiService.getApiService(EssentialDataAPI.class);
    }

    @Override
    public Single<EssentialData> getEssentialData(long userId) {
        return essentialDataAPI.getEssentialData(userId);
    }

    interface EssentialDataAPI {
        @GET("essential_data/user_essential_data/{userId}")
        Single<EssentialData> getEssentialData(@Path("userId") long userId);
    }
}
