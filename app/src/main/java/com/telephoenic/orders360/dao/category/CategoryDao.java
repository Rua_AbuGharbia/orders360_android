package com.telephoenic.orders360.dao.category;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.BrandModel;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class CategoryDao implements CategoryContract {

    private CategoryApi categoryApi;

    public CategoryDao() {
        categoryApi = ApiService.getApiService(CategoryApi.class);
    }

    @Override
    public Single<List<CategoryNodeWrapper>> getCategory(Long vendorId) {
        return categoryApi.getAllCategory(vendorId, AppConstants.IS_PROMOTION);
    }

    interface CategoryApi {
        @GET("category/all_category_for_vendor/{vendorId}")
        Single<List<CategoryNodeWrapper>> getAllCategory(@Path("vendorId") Long vendorId, @Query("isPromotion") Boolean isPromotion);
    }
}
