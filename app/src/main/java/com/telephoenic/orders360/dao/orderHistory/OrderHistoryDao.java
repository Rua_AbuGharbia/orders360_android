package com.telephoenic.orders360.dao.orderHistory;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.CancelOrderModel;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class OrderHistoryDao implements OrderHistoryContract {

    private OrderHistoryAPI orderHistoryAPI;

    public OrderHistoryDao() {
        orderHistoryAPI = ApiService.getApiService(OrderHistoryAPI.class);
    }

    public Single<List<OrderHistoryModel>> getOrderHistory(int id) {
        return orderHistoryAPI.getOrderHistory(id);
    }

    @Override
    public Single<List<OrderRejectedReason>> getReason(Long vendorId) {
        return orderHistoryAPI.getReason(AppConstants.CANCEL_CODE, vendorId);
    }

    @Override
    public Completable cancelOrder(CancelOrderModel cancelOrderModel) {
        return orderHistoryAPI.cancelOrder(cancelOrderModel);
    }

    @Override
    public Single<List<ShipModel>> getShipOrders(int pageIndex, int pageSize, Long orderId) {
        return orderHistoryAPI.getShipOrders(pageIndex, pageSize, orderId);
    }

    interface OrderHistoryAPI {
        @GET("order_history/items/{id}")
        Single<List<OrderHistoryModel>> getOrderHistory(@Path("id") int id);

        @GET("order-rejected-reason/find_reasons_by_parent/{code}/{vendorId}")
        Single<List<OrderRejectedReason>> getReason(@Path("code") String code, @Path("vendorId") Long vendorId);

        @POST("order/cancel_order")
        Completable cancelOrder(@Body CancelOrderModel cancelOrderModel);

        @GET("shipment_order/find_shipments_by_order_id/{pageNum}/{pageSize}/{orderId}")
        Single<List<ShipModel>> getShipOrders(@Path("pageNum") int pageNum,
                                              @Path("pageSize") int pageSize,
                                              @Path("orderId") Long orderId);
    }
}
