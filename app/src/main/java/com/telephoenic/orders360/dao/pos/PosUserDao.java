package com.telephoenic.orders360.dao.pos;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.PosGroupModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.controller.server.model.User;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/9/2019.
 */

public class PosUserDao implements PosUserContract {

    private PosUserAPI posUserAPI;

    public PosUserDao() {
        posUserAPI = ApiService.getApiService(PosUserAPI.class);
    }

    @Override
    public Maybe<User> getPosUserByMobile(String mobileNumber, long userId) {
        return posUserAPI.getPosUserByMobile(mobileNumber, userId);
    }

    @Override
    public Completable sendPosUser(long userId, long vendorId, User user) {
        return posUserAPI.sendPosUser(userId, vendorId, user);
    }

    @Override
    public Completable upDatePosLocation(User user) {
        return posUserAPI.upDatePosLocation(user);
    }

    @Override
    public Completable updateUserProfile(User user) {
        return posUserAPI.updateUserProfile(user);
    }

    @Override
    public Single<ResponseBody> updateRegistrationID(UpdateGPSRequest updateGPSRequest) {
        return posUserAPI.updateRegistrationID(updateGPSRequest);
    }

    @Override
    public Single<PosGroupModel> getPosGroupId(Long posId, Long vendorId) {
        return posUserAPI.getGroupId(posId, vendorId);
    }

    @Override
    public Single<ResponseBody> getAllPoints(String posId, String vendorId) {
        return posUserAPI.getAllPoints(posId,vendorId);
    }

    interface PosUserAPI {
        @GET("pos_user/get_pos_by_mobileNo/{mobileNumber}/{userId}")
        Maybe<User> getPosUserByMobile(@Path("mobileNumber") String mobileNumber, @Path("userId") long userId);

        @POST("pos_user/save_all/{userId}/{vendorId}")
        Completable sendPosUser(@Path("userId") long userId,@Path("vendorId") long vendorId, @Body User user);

        @POST("pos_user/update_location")
        Completable upDatePosLocation(@Body User user);

        @POST("pos_user/update_profile")
        Completable updateUserProfile(@Body User user);

        @POST("pos_user/update_pos_gps")
        Single<ResponseBody> updateRegistrationID(@Body UpdateGPSRequest updateGPSRequest);

        @GET("pos_user/find_group_by_posId/{posId}/{vendorId}")
        Single<PosGroupModel> getGroupId(@Path("posId") Long posId, @Path("vendorId") Long vendorId);

        @GET("pos_user/total_points/{posId}/{vendorId}")
        Single<ResponseBody> getAllPoints (@Path("posId") String posId , @Path("vendorId") String vendorId);
    }
}
