package com.telephoenic.orders360.dao.login;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Ru'a on 11/26/2019.
 */

public class LoginDao implements LoginContract {

  private  LoginApi loginApi ;

    public LoginDao() {
        loginApi = ApiService.getApiService(LoginApi.class);
    }

    @Override
    public Single<PosProfileWrapper> login(String base64Name) {
        return loginApi.login(base64Name);
    }


    interface LoginApi {
        @GET("auth/login")
        Single<PosProfileWrapper> login(@Header("Authorization") String base64Name);
    }
}
