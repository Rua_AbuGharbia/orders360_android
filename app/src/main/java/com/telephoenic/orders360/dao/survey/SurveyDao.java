package com.telephoenic.orders360.dao.survey;

import com.telephoenic.orders360.basemodel.data.network.ApiService;
import com.telephoenic.orders360.controller.server.model.survey.SurveyDetailsResponse;
import com.telephoenic.orders360.controller.server.model.survey.SurveyQuestionWrapper;
import com.telephoenic.orders360.controller.server.model.survey.SurveyResponse;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/5/2019.
 */

public class SurveyDao implements SurveyContract {
    private SurveyAPI surveyAPI;

    public SurveyDao() {
        surveyAPI = ApiService.getApiService(SurveyAPI.class);
    }

    @Override
    public Single<ResponseBody> getAllSurveys(String pageNumber, String pageSize, String vendorId, String posUserId) {
        return surveyAPI.getAllSurveys(pageNumber, pageSize, vendorId, posUserId);
    }

    @Override
    public Single<SurveyDetailsResponse> getSurveyDetails(Long surveyId, Long participantId) {
        return surveyAPI.getSurveyDetails(surveyId, participantId);
    }

    @Override
    public Single<ResponseBody> submitSurvey(Long surveyId, Long participantId, List<SurveyQuestionWrapper> questionsWithAnswers) {
        return surveyAPI.submitSurvey(surveyId, participantId, questionsWithAnswers);
    }

    interface SurveyAPI {
        @GET("survey/active_surveys_per_vendor/{pageNumber}/{pageSize}/{vendorId}/{posUserId}")
        Single<ResponseBody> getAllSurveys(@Path("pageNumber") String pageNumber,@Path("pageSize") String pageSize,
                                           @Path("vendorId")String vendorId, @Path("posUserId")String posUserId);

        @GET("user_survey/survey_with_user_answers/{surveyId}/{participantId}")
        Single<SurveyDetailsResponse> getSurveyDetails(@Path("surveyId") Long surveyId , @Path("participantId") Long participantId);

        @POST("user_survey/submit_survey/{surveyId}/{participantId}")
        Single<ResponseBody> submitSurvey(@Path("surveyId") Long surveyId ,@Path("participantId") Long participantId ,
                                          @Body List<SurveyQuestionWrapper> questionsWithAnswers);
    }
}
