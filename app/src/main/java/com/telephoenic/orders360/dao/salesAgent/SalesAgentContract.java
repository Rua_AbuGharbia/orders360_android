package com.telephoenic.orders360.dao.salesAgent;

import com.telephoenic.orders360.controller.server.model.User;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/8/2019.
 */

public interface SalesAgentContract {
    Single<User> getVendor (Long userId);
}
