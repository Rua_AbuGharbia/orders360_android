package com.telephoenic.orders360.dao.reward;

import com.telephoenic.orders360.basemodel.data.network.ApiService;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ru'a on 12/3/2019.
 */

public class RewardDao implements RewardContract {

    private RewardAPI rewardAPI;

    public RewardDao() {
        rewardAPI = ApiService.getApiService(RewardAPI.class);
    }

    @Override
    public Single<ResponseBody> redeemPoints(String posId, String productId, String vendorId) {
        return rewardAPI.redeemPoints(posId, productId, vendorId);
    }

    interface RewardAPI {

        @POST("reward/redeem/{posId}/{productId}/{vendorId}")
        Single<ResponseBody> redeemPoints (@Path("posId") String posId ,@Path("productId") String productId , @Path("vendorId") String vendorId);
    }
}
