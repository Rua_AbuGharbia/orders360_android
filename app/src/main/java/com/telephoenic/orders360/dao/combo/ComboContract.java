package com.telephoenic.orders360.dao.combo;

import com.telephoenic.orders360.controller.server.BundleWraper;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public interface ComboContract {
    Single<BundleWraper> getAllBundle(int pageNumber, int pageSize, long vendorId, long userId, String search);
}
