package com.telephoenic.orders360.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;

import static android.content.Context.MODE_PRIVATE;

public class AppPrefs {
    private static final String LANGUAGE = "language";
    private static final String IS_REMEMBER_ME = "is_remember_me";
    private static final String DATA_BASE = "data_base";
    private static final String USER_NAME = "user_name";
    private static final String PASSWORD = "password";
    private static final String POS = "pos";
    private static final String ORDER_NUMBER = "order_number";
    private static final String LOGIN_BASE64 = "login_base64";
    private static final String TOKEN = "token";
    private static final String CUSTOMER_lIST = "customer_list";
    private static final String COMBO_INFO_LIST = "combo_info_list";
    private static final String PREF_REGISTRATION_ID = "reg_id";
    private static final String PREF_APP_VERSION = "app_version";
    private static final String VENDOR_ID = "vendor_id";
    private static final String COUNTRY_ID = "country_id";
    private static final String VENDOR_LOGO = "vendor_logo";
    private static final String CURRENCY = "currency";
    private static final String INSTALLEDRIGHTKNOW = "com.telephoenic.orders360.model_INSTALLEDRIGHTKNOW";
    private static final String VENDOR_TYPE = "vendor_type";
    public static String USER_ID = "";

    private static SharedPreferences prefs = null;

    private static SharedPreferences getPrefs(Context context) {
        if (prefs == null) {
            prefs = context.getSharedPreferences(context.getPackageName(), MODE_PRIVATE);
        }
        return prefs;
    }

    public static void setLanguage(Context context, String language) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(LANGUAGE, language).apply();
    }

    public static String getLanguage(Context context) {
        String language = getPrefs(context).getString(LANGUAGE, "en");
        return language;
    }

    public static void setIsRememberMe(Context context, boolean isChecked) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putBoolean(IS_REMEMBER_ME, isChecked).apply();
    }

    public static boolean getIsRememberMe(Context context) {
        return getPrefs(context).getBoolean(IS_REMEMBER_ME, false);
    }

    public static void setDataBase(Context context, boolean isChecked) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putBoolean(DATA_BASE, isChecked).commit();
    }

    public static boolean getDataBase(Context context) {
        return getPrefs(context).getBoolean(DATA_BASE, true);
    }

    public static void setUserName(Context context, String userName) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(USER_NAME, userName).apply();
    }

    public static String getUserName(Context context) {
        return getPrefs(context).getString(USER_NAME, null);
    }

    public static void setPassword(Context context, String password) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(PASSWORD, password).apply();
    }

    public static String getPassword(Context context) {
        return getPrefs(context).getString(PASSWORD, null);
    }

    public static void setPOS(Context context, String pos) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(POS, pos).apply();
    }

    public static String getPOS(Context context) {
        return getPrefs(context).getString(POS, null);
    }

    public static void setOrderNumber(Context context, String orderNumber) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(ORDER_NUMBER, orderNumber).apply();
    }

    public static String getOrderNumber(Context context) {
        return getPrefs(context).getString(ORDER_NUMBER, null);
    }


    public static void setLoginBase64(Context context, String loginBase64) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(LOGIN_BASE64, loginBase64).apply();
    }

    public static String getLoginBase64(Context context) {
        return getPrefs(context).getString(LOGIN_BASE64, null);
    }

    public static void setToken(Context context, String token) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(TOKEN, token).apply();
    }

    public static String getToken(Context context) {
        return getPrefs(context).getString(TOKEN, null);
    }

    public static void setCustomerlist(Context context, String lookupList) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(CUSTOMER_lIST, lookupList).apply();
    }

    public static String getCustomerlist(Context context) {
        return getPrefs(context).getString(CUSTOMER_lIST, " ");
    }

    public static void setComboInfoList(Context context, String lookupList) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(COMBO_INFO_LIST, lookupList).apply();
    }

    public static String getComboInfoList(Context context) {
        return getPrefs(context).getString(COMBO_INFO_LIST, " ");
    }

    public static void setRegistrationId(Context context, String registrationId) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(PREF_REGISTRATION_ID, registrationId);
        prefEditor.apply();
    }

    public static String getRegistrationId(Context context) {
        return getPrefs(context).getString(PREF_REGISTRATION_ID, null);
    }

    public static void setAppVersion(Context context, int appVersion) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putInt(PREF_APP_VERSION, appVersion);
        prefEditor.apply();
    }

    public static int getAppVersion(Context context) {
        return getPrefs(context).getInt(PREF_APP_VERSION, -1);
    }


    public static void setVendorID(Context context, Long vendorId) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putLong(VENDOR_ID + USER_ID, vendorId);
        prefEditor.apply();
    }

    public static Long getVendorID(Context context) {
        return getPrefs(context).getLong(VENDOR_ID + USER_ID, -1);
    }

    public static void setCountryId(Context context, Long countryId) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putLong(COUNTRY_ID, countryId);
        prefEditor.apply();
    }

    public static Long getCountryId(Context context) {
        return getPrefs(context).getLong(COUNTRY_ID, -1);
    }

    public static void setCurrency(Context context, String currency) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(CURRENCY, currency);
        prefEditor.apply();
    }

    public static void setVendorLogo(Context context, String imageUrl) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(VENDOR_LOGO, imageUrl);
        prefEditor.apply();
    }

    public static String getCurrency(Context context) {
        return getPrefs(context).getString(CURRENCY, "");
    }

    public static String getVendorLogo(Context context) {
        return getPrefs(context).getString(VENDOR_LOGO, "");
    }

    public static boolean isInstalledRightKnow(Context context) {
        return getPrefs(context).getBoolean(INSTALLEDRIGHTKNOW, true);
    }

    public static void setInstalledRightKnowFlag(Context context, boolean installedRightKnow) {
        getPrefs(context).edit().putBoolean(INSTALLEDRIGHTKNOW, false).apply();
    }

    public static void setVendorType(Context context, String type) {
        SharedPreferences.Editor prefEditor = getPrefs(context).edit();
        prefEditor.putString(VENDOR_TYPE, type);
        prefEditor.apply();
    }

    public static String getVendorType(Context context) {
        return getPrefs(context).getString(VENDOR_TYPE, "");
    }
}