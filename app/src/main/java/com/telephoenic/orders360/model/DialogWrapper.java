package com.telephoenic.orders360.model;

import android.content.Context;

/**
 * Created by Ihab on 8/25/2016.
 */
public class DialogWrapper {

    private Context context;
    private String message;
    private String title;
    private boolean isFinished;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setIsFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }
}
