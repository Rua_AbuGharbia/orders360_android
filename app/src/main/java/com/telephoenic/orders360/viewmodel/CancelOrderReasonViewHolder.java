package com.telephoenic.orders360.viewmodel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CancelOrderReasonViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ReasonName_RadioButton)
    public RadioButton reasonNameRadioButton;

    public CancelOrderReasonViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
