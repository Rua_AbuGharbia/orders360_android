package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.repository.CartRepository;

import java.util.List;

import okhttp3.Response;
import okhttp3.ResponseBody;

public class CartViewModel extends ViewModel {
    private CartRepository cartRepository;

    public CartViewModel() {
        cartRepository = new CartRepository();
    }

   public LiveData<ResponseWrapper<ResponseBody>> addToCart(List<AddToCartModel> addToCartModel) {
        return cartRepository.addToCart(addToCartModel);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        cartRepository.unsubscribe();
    }
}
