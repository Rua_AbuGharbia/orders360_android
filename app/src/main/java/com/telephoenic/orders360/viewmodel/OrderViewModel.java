package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.repository.OrderRepository;

import java.util.List;

import okhttp3.ResponseBody;


public class OrderViewModel extends ViewModel {
    private OrderRepository orderRepository;

    public OrderViewModel() {
        orderRepository = new OrderRepository();
    }

    public LiveData<ResponseWrapper<ResponseBody>> getPOSOrder(String page, String pageSize, String posId, String vendorId){
        return orderRepository.getPOSOrder(page, pageSize, posId, vendorId);
    }

    public LiveData<ResponseWrapper<ResponseBody>> getSalesOrder(int page , int pageSize , Long salesAgentId){
        return orderRepository.getSalesOrder(page, pageSize, salesAgentId);
    }

    public LiveData<ResponseWrapper<ResponseBody>> saveOrder(List<OrderModel> orderModelsRequest){
        return orderRepository.saveOrder(orderModelsRequest);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        orderRepository.unsubscribe();
    }
}
