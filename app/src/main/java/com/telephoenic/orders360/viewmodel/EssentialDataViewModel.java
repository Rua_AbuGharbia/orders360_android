package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.EssentialData;
import com.telephoenic.orders360.repository.EssentialDataRepository;

/**
 * Created by Ru'a on 12/3/2019.
 */

public class EssentialDataViewModel extends ViewModel {

    private EssentialDataRepository essentialDataRepository;

    public EssentialDataViewModel() {
        essentialDataRepository = new EssentialDataRepository();
    }

    public LiveData<ResponseWrapper<EssentialData>> getEssentialData (long userId){
        return essentialDataRepository.getEssentialData(userId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        essentialDataRepository.unsubscribe();
    }
}
