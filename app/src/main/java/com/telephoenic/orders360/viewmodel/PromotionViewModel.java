package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;
import com.telephoenic.orders360.repository.PromotionRepository;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class PromotionViewModel extends ViewModel {
    private PromotionRepository promotionRepository;

    public PromotionViewModel() {
        promotionRepository = new PromotionRepository();
    }

    public LiveData<ResponseWrapper<PromotionWraper>> getAllPromotion(int pageNumber, int pageSize, long vendorId, long groupId, String search, ProductFilter productFilter) {
        promotionRepository = new PromotionRepository();
        return promotionRepository.getGetAllPromotion(pageNumber, pageSize, vendorId, groupId, search, productFilter);
    }

    public LiveData<ResponseWrapper<ResponseBody>> getPromotion(String prodId, String groupId){
        promotionRepository = new PromotionRepository();
        return promotionRepository.getPromotion(prodId, groupId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        promotionRepository.unsubscribe();
    }

    public void resetData(){
        promotionRepository.unsubscribe();
    }
}
