package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.repository.OrderItemRepository;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class OrderItemViewModel extends ViewModel {

    OrderItemRepository orderItemRepository ;

    public OrderItemViewModel() {
        orderItemRepository = new OrderItemRepository();
    }

    public LiveData<ResponseWrapper<OrderItemModel>> getMyOrderHistory(String itemId, Context context, Fragment fragment) {
        return orderItemRepository.getMyOrder(itemId,context,fragment);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        orderItemRepository.unsubscribe();
    }
}
