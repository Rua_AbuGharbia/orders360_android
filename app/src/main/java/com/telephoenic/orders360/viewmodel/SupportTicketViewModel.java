package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.SendTicketRequest;
import com.telephoenic.orders360.controller.server.model.SupportTicketWrapper;
import com.telephoenic.orders360.controller.server.model.TicketModel;
import com.telephoenic.orders360.repository.SupportTicketRepository;

import java.util.List;

import okhttp3.ResponseBody;

public class SupportTicketViewModel extends ViewModel {
    private SupportTicketRepository supportTicketRepository;

    public SupportTicketViewModel() {
        super();
        supportTicketRepository = new SupportTicketRepository();
    }

    public LiveData<ResponseWrapper<SupportTicketWrapper>> getType(int pageNumber, int pageSize, Long vendorId) {
        return supportTicketRepository.getType(pageNumber, pageSize, vendorId);
    }

    public LiveData<ResponseWrapper<List<TicketModel>>> getAllTicket (String posId ,String vendorId){
        return supportTicketRepository.getAllTicket(posId, vendorId);
    }

    public LiveData<ResponseWrapper<ResponseBody>> saveTicket(SendTicketRequest sendTicketRequest){
        return supportTicketRepository.saveTicket(sendTicketRequest);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        supportTicketRepository.unsubscribe();
    }
}
