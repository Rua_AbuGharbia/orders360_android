package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.repository.LoginRepository;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 11/26/2019.
 */

public class LoginViewModel extends ViewModel {
    private LoginRepository loginRepository ;

    public LoginViewModel() {
        loginRepository = new LoginRepository();
    }

    public LiveData<ResponseWrapper<PosProfileWrapper>> login(Context context ,String base64Name) {
        return loginRepository.login(context ,base64Name);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        loginRepository.unsubscribe();
    }
}
