package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.repository.RegionRepository;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class RegionViewModel extends ViewModel {

    private RegionRepository regionRepository ;

    public RegionViewModel() {
        regionRepository = new RegionRepository();
    }

    public LiveData<ResponseWrapper<ResponseBody>> getAllRegionsAtSelectedCity(RegionModel regionModel) {
        return regionRepository.getAllRegionsAtSelectedCity(regionModel);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        regionRepository.unsubscribe();
    }
}
