package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.survey.SurveyDetailsResponse;
import com.telephoenic.orders360.controller.server.model.survey.SurveyQuestionWrapper;
import com.telephoenic.orders360.repository.SurveyRepository;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/5/2019.
 */

public class SurveyViewModel extends ViewModel {

    private SurveyRepository surveyRepository ;

    public SurveyViewModel() {
        surveyRepository = new SurveyRepository();
    }

    public LiveData<ResponseWrapper<ResponseBody>> getAllSurveys(String pageNumber, String pageSize, String vendorId, String posUserId){
        return surveyRepository.getAllSurvey(pageNumber, pageSize, vendorId, posUserId);
    }

    public LiveData<ResponseWrapper<SurveyDetailsResponse>> getSurveyDetails(Long surveyId , Long participantId){
        return surveyRepository.getSurveyDetails(surveyId, participantId);
    }

    public LiveData<ResponseWrapper<ResponseBody>> submitSurvey(Long surveyId, Long participantId, List<SurveyQuestionWrapper> questionsWithAnswers){
        return surveyRepository.submitSurvey(surveyId, participantId, questionsWithAnswers);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        surveyRepository.unsubscribe();
    }
}
