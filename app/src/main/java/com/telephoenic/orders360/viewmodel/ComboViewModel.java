package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.BundleWraper;
import com.telephoenic.orders360.repository.ComboRepository;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class ComboViewModel extends ViewModel {

    private ComboRepository comboRepository ;

    public ComboViewModel() {
        comboRepository = new ComboRepository();
    }

    public LiveData<ResponseWrapper<BundleWraper>> getAllBundle(int pageNumber, int pageSize, long vendorId, long userID, String search) {
        comboRepository =new ComboRepository();
        return comboRepository.getAllBundle(pageNumber, pageSize, vendorId, userID, search);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        comboRepository.unsubscribe();
    }
}
