package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.repository.ComboInfoRepository;
import com.telephoenic.orders360.repository.ProductRepository;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class ComboInfoViewModel extends ViewModel {
    private ComboInfoRepository comboInfoRepository;

    public ComboInfoViewModel() {
        comboInfoRepository = new ComboInfoRepository();
    }

    public LiveData<ResponseWrapper<List<ComboInfoWrapper>>> getBundleInfo (String comboId){
        comboInfoRepository = new ComboInfoRepository();
        return comboInfoRepository.getBundleInfo(comboId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        comboInfoRepository.unsubscribe();
    }
}
