package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.BundleWraper;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;
import com.telephoenic.orders360.repository.ProductRepository;

import java.util.List;

import okhttp3.ResponseBody;

public class ProductViewModel extends ViewModel {
    private ProductRepository productRepository;

    public ProductViewModel() {
        productRepository = new ProductRepository();
    }

    public LiveData<ResponseWrapper<List<ProductWrapper>>> getAllProduct(int pageNumber, int pageSize, long vendorId, long posId,long groupId, String search, ProductFilter productFilter) {
        productRepository =new ProductRepository();
        return productRepository.getGetAllProduct(pageNumber, pageSize, vendorId, posId,groupId, search, productFilter);
    }

    public LiveData<ResponseWrapper<ResponseBody>> getProductWithPoints (String pageNum, String pageSize, String vendorId){
        return productRepository.getProductWithPoints(pageNum, pageSize, vendorId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        productRepository.unsubscribe();
    }
    public void resetData(){
        productRepository.unsubscribe();
    }
}
