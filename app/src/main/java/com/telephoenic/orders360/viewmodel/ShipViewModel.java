package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalUnsuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.FromToSerialModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentMethodModel;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentResponse;
import com.telephoenic.orders360.controller.server.model.shiporder.SerialsQuantity;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SingleSerialModel;
import com.telephoenic.orders360.repository.ShipRepository;

import java.util.List;

public class ShipViewModel extends ViewModel {

    private ShipRepository shipRepository;

    public ShipViewModel() {
        shipRepository = new ShipRepository();
    }

    public LiveData<ResponseWrapper<List<ShipModel>>> getShipOrders(int pageIndex, int pageSize, Long deliveryUserId, String status, ShipmentOrderFilter shipmentOrderFilter, String search_term) {
        return shipRepository.getShipOrders(pageIndex, pageSize, deliveryUserId, status, shipmentOrderFilter, search_term);
    }

    public LiveData<ResponseWrapper<List<ShipItemsModel>>> getShipItemOrder(Long id) {
        return shipRepository.getShipItemOrders(id);
    }

    public LiveData<ResponseWrapper> shipItemOrders(Long orderItemId) {
        return shipRepository.shipItemOrders(orderItemId);
    }

    public LiveData<ResponseWrapper> saveSingleSerials(Long itemId, List<SingleSerialModel> serials) {
        return shipRepository.saveSingleSerials(itemId, serials);
    }

    public LiveData<ResponseWrapper> saveQuantitySerials(Long itemId, List<SerialsQuantity> serials) {
        return shipRepository.saveQuantitySerials(itemId, serials);
    }

    public LiveData<ResponseWrapper> saveRangeSerials(Long itemId, List<FromToSerialModel> serials) {
        return shipRepository.saveRangeSerials(itemId, serials);
    }

    public LiveData<ResponseWrapper> confirmArrivalUnSuccess(Long shipmentId, ArrivalUnsuccessfulModel unsuccessfulModel) {
        return shipRepository.confirmArrivalUnSuccess(shipmentId, unsuccessfulModel);
    }

    public LiveData<ResponseWrapper> confirmArrivalSuccess(Long shipmentId, ArrivalSuccessfulModel arrivalSuccessfulModel) {
        return shipRepository.confirmArrivalSuccess(shipmentId, arrivalSuccessfulModel);
    }

    public LiveData<ResponseWrapper<List<OrderRejectedReason>>> getReason(String code, Long vendorId) {
        return shipRepository.getReason(code, vendorId);
    }

    public LiveData<ResponseWrapper<PaymentResponse>> getPaymentMethod() {
        return shipRepository.getPaymentMethod();

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        shipRepository.unsubscribe();
    }
}
