package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.PosGroupModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.repository.PosUserRepository;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/9/2019.
 */

public class PosUserViewModel extends ViewModel {
    private PosUserRepository posUserRepository;

    public PosUserViewModel() {
        posUserRepository = new PosUserRepository();
    }

    public LiveData<ResponseWrapper<User>> getPosUserByMobile(String mobileNumber, long userId) {
        return posUserRepository.getPosUserByMobile(mobileNumber, userId);
    }

    public LiveData<ResponseWrapper> sendPosUser(long userId, long vendorId, User user) {
        return posUserRepository.sendPosUser(userId, vendorId, user);
    }

    public LiveData<ResponseWrapper> upDatePosLocation(User user){
        return posUserRepository.upDatePosLocation(user);
    }

    public LiveData<ResponseWrapper> updateUserProfile(User user){
        return posUserRepository.updateUserProfile(user);
    }

    public LiveData<ResponseWrapper<ResponseBody>> updateRegistrationID(UpdateGPSRequest updateGPSRequest){
        return posUserRepository.updateRegistrationID(updateGPSRequest);
    }

    public LiveData<ResponseWrapper<PosGroupModel>> getPosGroupId(Long posId, Long vendorId){
        return posUserRepository.getPosGroupId(posId, vendorId);
    }

    public LiveData<ResponseWrapper<ResponseBody>> getAllPoints (String posId , String vendorId){
        return posUserRepository.getAllPoints(posId, vendorId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        posUserRepository.unsubscribe();
    }
}
