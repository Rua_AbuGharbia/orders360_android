package com.telephoenic.orders360.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.UserPassword;
import com.telephoenic.orders360.repository.UserRepository;

public class UserViewModel extends ViewModel {
    private UserRepository userRepository;

    public UserViewModel() {
        userRepository = new UserRepository();
    }

    public LiveData<ResponseWrapper<Boolean>> checkMobileNumber(String mobileNumber, Context context, Activity activity) {
        return userRepository.getCheckMobileNumber(mobileNumber, context, activity);
    }

    public LiveData<ResponseWrapper<String>> sendPassCode(String mobileNumber, long otp, Context context, Activity activity) {
        return userRepository.sendPassCode(mobileNumber, otp, context, activity);
    }

    public LiveData<ResponseWrapper<Boolean>> changePassword(UserPassword userPassword, Context context, Activity activity) {
        return userRepository.changePassword(userPassword, context, activity);
    }

    public LiveData<ResponseWrapper<Boolean>> changePasswordInsideApp(UserPassword userPassword){
        return userRepository.changePasswordInsideApp(userPassword);
    }

    public LiveData<ResponseWrapper<User>> getUserProfile(long id){
        return userRepository.getUserProfile(id);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        userRepository.unsubscribe();
    }
}
