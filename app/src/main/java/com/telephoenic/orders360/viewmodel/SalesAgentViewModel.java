package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.repository.SalesAgentRepository;

/**
 * Created by Ru'a on 12/8/2019.
 */

public class SalesAgentViewModel extends ViewModel {

    private SalesAgentRepository salesAgentRepository;

    public SalesAgentViewModel() {
        salesAgentRepository = new SalesAgentRepository();
    }

    public LiveData<ResponseWrapper<User>> getVendor(Long userId){
        return salesAgentRepository.getVendor(userId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        salesAgentRepository.unsubscribe();
    }
}
