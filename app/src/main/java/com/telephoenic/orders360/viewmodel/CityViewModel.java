package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.repository.CityRepository;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class CityViewModel extends ViewModel {

    private CityRepository cityRepository ;

    public CityViewModel() {
        cityRepository = new CityRepository();
    }

    public LiveData<ResponseWrapper<List<CityModel>>> getAllCities(Long countryId) {
        return cityRepository.getAllCities(countryId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        cityRepository.unsubscribe();
    }
}
