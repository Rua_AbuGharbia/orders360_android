package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.WarehouseModel;
import com.telephoenic.orders360.repository.DeliveryUserRepository;
import com.telephoenic.orders360.repository.SalesAgentRepository;

import java.util.List;

public class DeliveryUserViewModel extends ViewModel {

    private DeliveryUserRepository deliveryUserRepository;

    public DeliveryUserViewModel() {
        deliveryUserRepository = new DeliveryUserRepository();
    }

    public LiveData<ResponseWrapper<User>> getVendor(Long userId) {
        return deliveryUserRepository.getVendor(userId);
    }

    public LiveData<ResponseWrapper<List<WarehouseModel>>> getWarehouse(Long userId) {
        return deliveryUserRepository.getWarehouse(userId);
    }

    public LiveData<ResponseWrapper<List<CityModel>>> getCity() {
        return deliveryUserRepository.getCity();
    }

    public LiveData<ResponseWrapper<List<RegionModel>>> getRegion(Long cityId) {
        return deliveryUserRepository.getRegion(cityId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        deliveryUserRepository.unsubscribe();
    }
}
