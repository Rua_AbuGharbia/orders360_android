package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.repository.PosGroupRepository;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class PosGroupViewModel extends ViewModel {
    private PosGroupRepository posGroupRepository;

    public PosGroupViewModel() {
        posGroupRepository = new PosGroupRepository();
    }

    public LiveData<ResponseWrapper<List<GroupModel>>> getAllGroups(Long vendorId) {
        return posGroupRepository.getAllGroups(vendorId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        posGroupRepository.unsubscribe();
    }
}
