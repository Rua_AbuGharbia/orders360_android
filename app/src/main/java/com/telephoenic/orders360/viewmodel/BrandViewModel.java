package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.BrandModel;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.repository.BrandRepository;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class BrandViewModel extends ViewModel {

    private BrandRepository brandRepository ;

    public BrandViewModel() {
        brandRepository = new BrandRepository();
    }

    public LiveData<ResponseWrapper<List<BrandWrapper>>> getBrands(Long vendorID) {
        return brandRepository.getBrandsLiveData(vendorID);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        brandRepository.unsubscribe();
    }
}
