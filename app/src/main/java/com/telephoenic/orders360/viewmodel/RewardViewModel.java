package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.repository.RewardRepository;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/3/2019.
 */

public class RewardViewModel extends ViewModel {

    private RewardRepository rewardRepository;

    public RewardViewModel() {
        rewardRepository = new RewardRepository();
    }

    public LiveData<ResponseWrapper<ResponseBody>> redeemPoints (String posId, String productId, String vendorId){
        return rewardRepository.redeemPoints(posId, productId, vendorId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        rewardRepository.unsubscribe();
    }
}
