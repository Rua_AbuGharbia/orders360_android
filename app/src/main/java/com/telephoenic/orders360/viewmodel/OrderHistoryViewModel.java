package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.controller.server.model.CancelOrderModel;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.repository.OrderHistoryRepository;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class OrderHistoryViewModel extends ViewModel {
    private OrderHistoryRepository orderHistoryRepository;

    public OrderHistoryViewModel() {
        orderHistoryRepository = new OrderHistoryRepository();
    }

    public LiveData<ResponseWrapper<List<OrderHistoryModel>>> getOrderHistory(int id) {
        return orderHistoryRepository.getOrderHistory(id);
    }

    public LiveData<ResponseWrapper<List<OrderRejectedReason>>> getOrderReason(Long vendorId) {
        return orderHistoryRepository.getReason(vendorId);
    }

    public LiveData<ResponseWrapper> cancelOrder(CancelOrderModel cancelOrderModel) {
        return orderHistoryRepository.cancelOrder(cancelOrderModel);
    }

    public LiveData<ResponseWrapper<List<ShipModel>>> getOrderShip(int pageIndex, int pageSize, Long orderId) {
        return orderHistoryRepository.getOrderShip(pageIndex, pageSize, orderId);

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        orderHistoryRepository.unsubscribe();
    }
}
