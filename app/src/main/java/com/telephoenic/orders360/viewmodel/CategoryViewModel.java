package com.telephoenic.orders360.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.BrandModel;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.repository.CategoryRepository;

import java.util.List;

public class CategoryViewModel extends ViewModel {
    private CategoryRepository categoryRepository;

    public CategoryViewModel() {
        categoryRepository = new CategoryRepository();
    }

    public LiveData<ResponseWrapper<List<CategoryNodeWrapper>>> getCategory(Context context, Fragment fragment) {
        return categoryRepository.getCategory(AppPrefs.getVendorID(context), fragment, context);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        categoryRepository.unsubscribe();
    }
}
