package com.telephoenic.orders360.basemodel.data.network;

import android.support.annotation.NonNull;

import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;
import com.telephoenic.orders360.model.AppPrefs;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AcceptLanguage implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;
        request.newBuilder();
        newRequest = request.newBuilder().addHeader("Accept-Language", SharedPreferenceManager.getLanguage(Order360Application.getInstance().getApplicationContext()))
                .build();
        return chain.proceed(newRequest);
    }
}
