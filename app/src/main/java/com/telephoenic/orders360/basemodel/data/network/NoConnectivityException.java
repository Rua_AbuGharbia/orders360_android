package com.telephoenic.orders360.basemodel.data.network;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "Please Check Your Network Connection!";
    }
}