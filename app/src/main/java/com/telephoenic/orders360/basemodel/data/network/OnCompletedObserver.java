package com.telephoenic.orders360.basemodel.data.network;


import android.text.TextUtils;
import android.util.Log;

import com.telephoenic.orders360.controller.server.model.ErrorResponse;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.retrofit.ErrorUtils;
import com.telephoenic.orders360.retrofit.ResponseAPIError;

import org.springframework.http.HttpStatus;

import java.net.SocketTimeoutException;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableMaybeObserver;
import retrofit2.HttpException;
import retrofit2.Response;

public abstract class OnCompletedObserver<T> extends DisposableCompletableObserver {

    private static final String TAG = "OnCompletedObserver";


    @Override
    public void onError(Throwable e) {
        if (e instanceof com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
            Response<?> body = ((com.jakewharton.retrofit2.adapter.rxjava2.HttpException) e).response();
            if (body.code() == (HttpStatus.OK.value())) {
                onResponse();
            }

            ResponseAPIError responseAPIError = ErrorUtils.parseError(body);
//            if (responseAPIError != null)
//                onFailure(responseAPIError.getMessage());
            if (responseAPIError != null) {
                if (!TextUtils.isEmpty(responseAPIError.getMessage())) {
                    onFailure(responseAPIError.getMessage());
                } else if (body.code() == (HttpStatus.UNAUTHORIZED.value())) {
                    onFailure("Invalid username or password");
                }
            }
            int code = body.code();
            onFailure("Error");
            if (code >= 400 && code < 500) {
                onFailure("Error");
            } else if (code >= 500 && code < 600) {
                onFailure("Server Error");
            }
        } else if (e instanceof NoConnectivityException) {
            noConnection();
//            onFailure("No Internet Connection");

        } else if (e instanceof HttpException) {
            Response<?> body2 = ((HttpException) e).response();
            int code = body2.code();
            onFailure("Error");
            if (code == 401) {
                onFailure("Error");
            } else if (code >= 400 && code < 500) {
                onFailure("Error");
            } else if (code >= 500 && code < 600) {
                onFailure("Server Error");
            }
        } else if (e instanceof SocketTimeoutException) {
            onFailure("failed to connect");
        } else {
            onFailure(e.getMessage());
        }
    }

    @Override
    public void onComplete() {
        Log.e(TAG, "onComplete: ");
        onResponse();
    }

    protected abstract void noConnection();

    protected abstract void onResponse();

    protected abstract void onFailure(String error);


}
