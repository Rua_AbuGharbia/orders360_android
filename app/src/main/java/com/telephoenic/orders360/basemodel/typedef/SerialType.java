package com.telephoenic.orders360.basemodel.typedef;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.telephoenic.orders360.basemodel.typedef.SerialType.FROM_TO;
import static com.telephoenic.orders360.basemodel.typedef.SerialType.QUANTITY;
import static com.telephoenic.orders360.basemodel.typedef.SerialType.SINGLE;


@IntDef({SINGLE, FROM_TO,QUANTITY})
@Retention(RetentionPolicy.SOURCE)
public @interface SerialType {
    int SINGLE = 0;
    int FROM_TO = 1;
    int QUANTITY = 2;
}
