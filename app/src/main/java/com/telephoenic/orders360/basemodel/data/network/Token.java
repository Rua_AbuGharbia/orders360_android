package com.telephoenic.orders360.basemodel.data.network;


import android.util.Log;

import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.repository.BaseRepository;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class Token implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest, request2,request3;;
        request.newBuilder();
        Chain chain12 =chain;

        if (AppPrefs.getToken(Order360Application.getInstance().getApplicationContext()) != null) {
            newRequest = request.newBuilder().addHeader(AppConstants.X_AUTH_TOKEN, AppPrefs.getToken(Order360Application.getInstance().getBaseContext()))
                    .build();


            Response response = chain12.proceed(newRequest);
            if (response.code() == 401) {
                BaseRepository baseRepository = new BaseRepository();
                AppPrefs.setToken(Order360Application.getInstance().getApplicationContext(), null);
                baseRepository.getNewToken(AppPrefs.getLoginBase64(Order360Application.getInstance()));
                if (! baseRepository.getNewToken(AppPrefs.getLoginBase64(Order360Application.getInstance())).hasError()){
                    request2 = request.newBuilder().addHeader("Authorization", "Basic " + AppPrefs.getLoginBase64(Order360Application.getInstance().getBaseContext()))
                            .build();
                    AppPrefs.setToken(Order360Application.getInstance().getApplicationContext(), chain.proceed(request2).headers().get(AppConstants.X_AUTH_TOKEN));
                    return chain.proceed(request2);
                }
            }
//            return chain.proceed(newRequest);
            return response;
        } else {
            request2 = request.newBuilder().addHeader("Authorization", "Basic " + AppPrefs.getLoginBase64(Order360Application.getInstance().getBaseContext()))
                    .build();
            AppPrefs.setToken(Order360Application.getInstance().getApplicationContext(), chain.proceed(request2).headers().get(AppConstants.X_AUTH_TOKEN));
            return chain.proceed(request2);
        }
    }
}
