package com.telephoenic.orders360.basemodel.data.network;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.retrofit.NullOnEmptyConverterFactory;
import com.telephoenic.orders360.retrofit.ResponseAPIError;
import com.telephoenic.orders360.retrofit.RetrofitClient;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiService {

    public static <T> T getApiService(Class<T> claz) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.URL_LIVE)
                .addCallAdapterFactory(new RxThreadCallAdapterFactory(Schedulers.io(), AndroidSchedulers.mainThread()))
                .addConverterFactory(new NullOnEmptyConverterFactory()) // to handel the null response from the server with this class
                .addConverterFactory(GsonConverterFactory.create(getGsonFactory()))
                .client(getOkHttpClient())
                .build();

        return retrofit.create(claz);
    }

    private static OkHttpClient getOkHttpClient() {

        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new ConnectivityInterceptor())
                .addInterceptor(new Token())
                .addInterceptor(new ContentType())
                .addInterceptor(new AcceptLanguage())
                .addInterceptor(getOkHttpLogging())
                .build();
    }

    private static HttpLoggingInterceptor getOkHttpLogging() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return logging;
    }

    private static Gson getGsonFactory() {
        return new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return false;
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();
    }

    public static ResponseAPIError parseError(Response<?> response) {
        Converter<ResponseBody, ResponseAPIError> converter =
                RetrofitClient.getInstance()
                        .responseBodyConverter(ResponseAPIError.class, new Annotation[0]);

        ResponseAPIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ResponseAPIError();
        }

        return error;
    }
}
