package com.telephoenic.orders360.basemodel.typedef;

import android.support.annotation.IntDef;

import com.telephoenic.orders360.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.telephoenic.orders360.basemodel.typedef.Error.NOT_EDHORAZ;

/**
 * Created by Ru'a on 12/8/2019.
 */
@IntDef(NOT_EDHORAZ)
@Retention(RetentionPolicy.SOURCE)
public @interface Error {
    int NOT_EDHORAZ = R.string.current_password_did_not_match_existing_password;
}
