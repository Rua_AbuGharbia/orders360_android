package com.telephoenic.orders360.basemodel.data.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

public class RxThreadCallAdapterFactory extends CallAdapter.Factory {
    private RxJava2CallAdapterFactory wrappedCallAdapterFactory;
    private Scheduler subscribeScheduler;
    private Scheduler observerScheduler;

    public RxThreadCallAdapterFactory(Scheduler subscribeScheduler, Scheduler observerScheduler) {
        wrappedCallAdapterFactory = RxJava2CallAdapterFactory.create();
        this.subscribeScheduler = subscribeScheduler;
        this.observerScheduler = observerScheduler;
    }

    @Nullable
    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        CallAdapter<?, ?> callAdapter = wrappedCallAdapterFactory.get(returnType, annotations, retrofit);
        if (callAdapter == null)
            return null;

        Class<?> rawType = getRawType(returnType);

        boolean isCompletable = rawType == Completable.class;
        boolean isFlowable = rawType == Flowable.class;
        boolean isSingle = rawType == Single.class;
        boolean isMaybe = rawType == Maybe.class;

        return new ThreadCallAdapter<>(callAdapter, isCompletable, isFlowable, isMaybe, isSingle);
    }

    final class ThreadCallAdapter<T> implements CallAdapter<T, Object> {
        CallAdapter delegateAdapter;
        private final boolean isCompletable;
        private final boolean isFlowable;
        private final boolean isMaybe;
        private final boolean isSingle;

        ThreadCallAdapter(CallAdapter delegateAdapter, boolean isCompletable,
                          boolean isFlowable, boolean isMaybe, boolean isSingle) {
            this.delegateAdapter = delegateAdapter;
            this.isCompletable = isCompletable;
            this.isFlowable = isFlowable;
            this.isMaybe = isMaybe;
            this.isSingle = isSingle;
        }

        @Override
        public Type responseType() {
            return delegateAdapter.responseType();
        }

        @NonNull
        @SuppressWarnings("unchecked")
        @Override
        public Object adapt(@NonNull Call<T> call) {
            if (isCompletable) {
                return ((Completable) delegateAdapter.adapt(call)).subscribeOn(subscribeScheduler)
                        .observeOn(observerScheduler);
            }
            if (isFlowable) {
                return ((Flowable<Object>) delegateAdapter.adapt(call)).subscribeOn(subscribeScheduler)
                        .observeOn(observerScheduler);
            }
            if (isMaybe) {
                return ((Maybe<Object>) delegateAdapter.adapt(call)).subscribeOn(subscribeScheduler)
                        .observeOn(observerScheduler);
            }
            if (isSingle) {
                return ((Single) delegateAdapter.adapt(call)).subscribeOn(subscribeScheduler)
                        .observeOn(observerScheduler);
            }
            return ((Observable<Object>) delegateAdapter.adapt(call)).subscribeOn(subscribeScheduler)
                    .observeOn(observerScheduler);
        }
    }

}
