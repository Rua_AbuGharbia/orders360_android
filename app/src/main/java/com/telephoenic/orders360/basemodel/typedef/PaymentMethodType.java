package com.telephoenic.orders360.basemodel.typedef;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.telephoenic.orders360.basemodel.typedef.PaymentMethodType.CASH;
import static com.telephoenic.orders360.basemodel.typedef.PaymentMethodType.CLAIM;

@IntDef({CASH,CLAIM, PaymentMethodType.CREDIT_CARD, PaymentMethodType.CHEQUE})
@Retention(RetentionPolicy.SOURCE)
public @interface PaymentMethodType {
    int CASH = 0;
    int CLAIM = 1;
    int CREDIT_CARD = 2;
    int CHEQUE = 3;
}
