package com.telephoenic.orders360.basemodel.typedef;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.telephoenic.orders360.basemodel.typedef.MaxQtyItem.MAX_QTY;

@IntDef(MAX_QTY)
@Retention(RetentionPolicy.SOURCE)
public @interface MaxQtyItem {
    int MAX_QTY = 9999;
}
