package com.telephoenic.orders360.basemodel.data.network;

public interface RequestCallback {
    <T> void onSuccess(T response);

    <T> void onFailure(T error);
}
