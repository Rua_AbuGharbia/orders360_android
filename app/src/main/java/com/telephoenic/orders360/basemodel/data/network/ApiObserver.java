package com.telephoenic.orders360.basemodel.data.network;


import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.telephoenic.orders360.controller.constants.AppConstants;


public abstract class ApiObserver<T> implements Observer<ResponseWrapper<T>> {

    private static final String TAG = "ApiObserver";

    @Override
    public void onChanged(@Nullable ResponseWrapper<T> responseWrapper) {

        if (responseWrapper != null) {
            if (responseWrapper.hasError()) {
                if (AppConstants.INTERNET_CONNECTION.equals(responseWrapper.getErrorCode())) {
                    noConnection();
                } else {
                    onError(responseWrapper.getErrorMessage());
                }
            } else {
                onSuccess(responseWrapper.getContent());
            }
        }

    }

    protected abstract void noConnection();

    protected abstract void onSuccess(T data);

    protected abstract void onError(String error);

}
