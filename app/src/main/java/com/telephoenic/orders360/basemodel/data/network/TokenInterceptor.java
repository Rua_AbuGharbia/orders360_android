package com.telephoenic.orders360.basemodel.data.network;


import android.util.Log;
import android.widget.Toast;

import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.model.AppPrefs;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest, request2;
        request.newBuilder();
        newRequest = request.newBuilder().addHeader("Authorization", "Basic " + AppPrefs.getLoginBase64(Order360Application.getInstance().getBaseContext()))
                .build();
//        Log.d("HEDER",chain.proceed(newRequest).header(AppConstants.X_AUTH_TOKEN));
        Log.d("HEDER2",chain.proceed(newRequest).headers().get(AppConstants.X_AUTH_TOKEN));
        AppPrefs.setToken(Order360Application.getInstance().getApplicationContext(), chain.proceed(newRequest).headers().get(AppConstants.X_AUTH_TOKEN));
//        Toast.makeText(Order360Application.getInstance().getApplicationContext(),chain.proceed(newRequest).headers().get(AppConstants.X_AUTH_TOKEN)+"",Toast.LENGTH_LONG).show();
//        Log.d("HEDER3",chain.proceed(newRequest).headers().toString());
//        Log.d("HEDER4",chain.proceed(newRequest).headers().name(0));
      //  Log.d(" HEADER2",newRequest.headers().get(AppConstants.X_AUTH_TOKEN));
      //  Log.d(" HEADER",newRequest.headers().g);
        return chain.proceed(newRequest);
    }
}
