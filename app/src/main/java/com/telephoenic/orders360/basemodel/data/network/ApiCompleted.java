package com.telephoenic.orders360.basemodel.data.network;


import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.telephoenic.orders360.controller.constants.AppConstants;


public abstract class ApiCompleted implements Observer<ResponseWrapper> {

    @Override
    public void onChanged(@Nullable ResponseWrapper responseWrapper) {

        if (responseWrapper != null) {
            if (responseWrapper.hasError()) {
                if(AppConstants.INTERNET_CONNECTION.equals(responseWrapper.getErrorCode())){
                    noConnection();
                } else {
                    onError(responseWrapper.getErrorMessage());
                }
            } else {
                onSuccess();
            }
        }

    }

    protected abstract void noConnection();

    protected abstract void onSuccess();

    protected abstract void onError(String error);

}
