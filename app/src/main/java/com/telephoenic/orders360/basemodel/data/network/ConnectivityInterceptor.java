package com.telephoenic.orders360.basemodel.data.network;

import android.support.annotation.NonNull;

import com.telephoenic.orders360.basemodel.data.networkutils.NetworkConnectionUtils;
import com.telephoenic.orders360.controller.application.Order360Application;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectivityInterceptor implements Interceptor {

    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (!NetworkConnectionUtils.isNetworkAvailable(Order360Application.getInstance().getApplicationContext())) {
            throw new NoConnectivityException();
        }

        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }

}