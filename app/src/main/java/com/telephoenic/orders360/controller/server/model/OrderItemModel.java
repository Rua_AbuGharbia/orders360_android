package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class OrderItemModel implements Serializable {

    public OrderItemModel() {

    }

    private List<SingleItem> singleItems;
    private List<PromotionItem> promotionItems;
    private List<ComboItem> comboItems;

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("product")
    private ProductWrapper product;

    @JsonProperty("deliverdQuantity")
    private Integer deliverdQuantity;

    @JsonProperty("orderedQuantity")
    private Integer orderedQuantity;

    @JsonProperty("totalAmount")
    private Double totalAmount;

    @JsonProperty("OrderItemHistory")
    private List<OrderHistoryModel> orderItemHistory;

    @JsonProperty("combo")
    private ComboWrapper combo;

    private String type;

    private Integer promotionDetailId;

    private String name;

    private boolean hasDiscount;

    @JsonProperty("price")
    protected Double price;

    @JsonProperty("discountedPrice")
    protected Double discountedPrice;

    @JsonProperty("discountPercentage")
    protected Double discountPercentage;

    public boolean isHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }


    public OrderItemModel(Integer id, Integer orderedQuantity, Double totalAmount, String name, Double price, boolean hasDiscount) {
        this.orderedQuantity = orderedQuantity;
        this.totalAmount = totalAmount;
        this.name = name;
        this.price = price;
        this.id = id;
        this.hasDiscount = hasDiscount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductWrapper getProduct() {
        return product;
    }

    public void setProduct(ProductWrapper product) {
        this.product = product;
    }

    public Integer getDeliverdQuantity() {
        return deliverdQuantity;
    }

    public void setDeliverdQuantity(Integer deliverdQuantity) {
        this.deliverdQuantity = deliverdQuantity;
    }

    public Integer getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(Integer orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<OrderHistoryModel> getOrderItemHistory() {
        return orderItemHistory;
    }

    public void setOrderItemHistory(List<OrderHistoryModel> orderItemHistory) {
        this.orderItemHistory = orderItemHistory;
    }

    public ComboWrapper getCombo() {
        return combo;
    }

    public void setCombo(ComboWrapper combo) {
        this.combo = combo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPromotionDetailId() {
        return promotionDetailId;
    }

    public void setPromotionDetailId(Integer promotionDetailId) {
        this.promotionDetailId = promotionDetailId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<SingleItem> getSingleItems() {
        return singleItems;
    }

    public void setSingleItems(List<SingleItem> singleItems) {
        this.singleItems = singleItems;
    }

    public List<PromotionItem> getPromotionItems() {
        return promotionItems;
    }

    public void setPromotionItems(List<PromotionItem> promotionItems) {
        this.promotionItems = promotionItems;
    }

    public List<ComboItem> getComboItems() {
        return comboItems;
    }

    public void setComboItems(List<ComboItem> comboItems) {
        this.comboItems = comboItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

}