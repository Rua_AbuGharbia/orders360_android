package com.telephoenic.orders360.controller.server.model;

import java.util.List;

public class ProductFilter {
    private Long categoryId;
    private List<Long> brandIds;
    private Boolean hasDiscount;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public List<Long> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(List<Long> brandIds) {
        this.brandIds = brandIds;
    }

    public Boolean getPriceWithDiscount() {
        return hasDiscount;
    }

    public void setPriceWithDiscount(Boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }
}
