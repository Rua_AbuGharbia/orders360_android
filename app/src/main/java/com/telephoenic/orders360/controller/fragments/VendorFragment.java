package com.telephoenic.orders360.controller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.VendorListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class VendorFragment extends BlankFragment implements OnRecyclerClick {
    @BindView(R.id.Done_SelectVendor_Button)
    Button doneSelectVendorButton;
    private String TAG = VendorFragment.class.getName();
    public static final String VENDOR_LIST = "vendor_list";

    List<VendorWrapper> listOfVendor;
    List<VendorWrapper> searchListOfVendor;
    private ArrayList<VendorWrapper> vendorWrappers;
    private RecyclerView recyclerView;
    public static boolean searchFlag = false;
    private SearchView vendorsSearchView;
    private VendorListAdapter adapter;
    private Integer selectPosition;
    private Integer filterPosition;

    public VendorFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vendor, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void createComponents(View v) {
        vendorWrappers = ( ArrayList<VendorWrapper>) getArguments().getSerializable(VENDOR_LIST);
        vendorsSearchView = v.findViewById(R.id.searchView_vendor);
        recyclerView = v.findViewById(R.id.RecyclerView);
        int id = vendorsSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        final TextView textView = (TextView) vendorsSearchView.findViewById(id);
        textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        listOfVendor = new ArrayList<>();
        searchListOfVendor = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(50);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        adapter = new VendorListAdapter(vendorWrappers, this, getActivity(),String.valueOf(System.currentTimeMillis()));
        recyclerView.setAdapter(adapter);
        vendorsSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchFlag = true;
                adapter.notifyDataSetChanged();
            }
        });

        vendorsSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.filterList(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filterList(newText);
                return false;
            }
        });

        vendorsSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return false;
            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void getPosition(int position) {
        selectPosition = position;
        filterPosition = position;

        for (int i = 0; i < adapter.vendorsLisFilter.size(); i++) {
            if (i == position) {
                adapter.vendorsLisFilter.get(position).setSelected(true);
            } else {
                adapter.vendorsLisFilter.get(i).setSelected(false);
            }
        }
        for (int i = 0; i < vendorWrappers.size(); i++) {
            if (vendorWrappers.get(i).getId().equals(adapter.vendorsLisFilter.get(selectPosition).getId())) {
                filterPosition = i;
                vendorWrappers.get(i).setSelected(true);
            } else {
                vendorWrappers.get(i).setSelected(false);
            }
        }
        adapter.notifyDataSetChanged();

    }

    @OnClick(R.id.Done_SelectVendor_Button)
    public void onViewClicked() {
        if (filterPosition != null) {
            AppPrefs.setVendorID(getActivity(), vendorWrappers.get(filterPosition).getId());
        }
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), MainActivity.class));
    }
}
