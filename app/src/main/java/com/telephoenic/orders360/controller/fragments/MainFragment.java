package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.DetailsActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.controller.server.model.EssentialData;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.PosGroupModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.VendorAd;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.FadePageTransformer;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.BannerAdapter;
import com.telephoenic.orders360.view.adapters.InfinitePagerAdapter;
import com.telephoenic.orders360.view.adapters.MainBrandAdapter;
import com.telephoenic.orders360.view.adapters.MainFragmentVendorRecyclerViewAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.InfiniteViewPager;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.customcontrols.SelectVendorDialog;
import com.telephoenic.orders360.viewmodel.BrandViewModel;
import com.telephoenic.orders360.viewmodel.DeliveryUserViewModel;
import com.telephoenic.orders360.viewmodel.EssentialDataViewModel;
import com.telephoenic.orders360.viewmodel.PosGroupViewModel;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;
import com.telephoenic.orders360.viewmodel.SalesAgentViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.telephoenic.orders360.controller.constants.MainFragmentCategories.ORDER_NOW;
import static com.telephoenic.orders360.controller.constants.MainFragmentCategories.VENDORS;

public class MainFragment extends BlankFragment implements MainFragmentVendorRecyclerViewAdapter.ItemClickListener, DialogUtils.OnDialogClickListener
        , ImageController, OnRecyclerClick {

    private static final String TAG = "MainFragment";
    @BindView(R.id.TitleLinearLayout)
    LinearLayout titleLinearLayout;
    @BindView(R.id.PagerCardView)
    CardView pagerCardView;
    @BindView(R.id.ImageOrder_CardView)
    CardView imageOrderCardView;
    @BindView(R.id.vendor_name_TextView)
    TextView vendorNameTextView;
    @BindView(R.id.MoreClick_TextView)
    TextView MoreClickTextView;
    @BindView(R.id.Brand_RecyclerView)
    RecyclerView brandRecyclerView;
    @BindView(R.id.brand_loading_view)
    SpinKitView brandLoadingView;

    private String myService = "";

    private RecyclerView mVendorsRecyclerView;
    private MainFragmentVendorRecyclerViewAdapter mVendorAdapter;
    private ArrayList<VendorWrapper> vendorsList;
    private ArrayList<String> adsImagesList = new ArrayList<>();
    private List<VendorAd> ads = new ArrayList<>();

    private CardView singleVendorCardView;
    private CircleImageView sigleVendorCircleImageView;
    private TextView sigleVendorNameTextView;
    private boolean isAttached;

    private LinearLayout imagesLayout;
    private InfiniteViewPager imagesViewPager;
    private PagerAdapter bannerAdapter;

    private Context context;
    private int screenSize;
    private Handler handler;
    private int delay = 5000; //milliseconds
    private int page = 0;
    private BrandViewModel brandViewModel;
    private PosUserViewModel posUserViewModel;
    private VendorWrapper currantVendor;
    private Button orderNowButton;
    private EssentialDataViewModel essentialDataViewModel;
    private PosGroupViewModel groupViewModel;
    private List<GroupModel> groupModelList;
    private ListItemDialog listItemDialog;

    Runnable runnable = new Runnable() {
        public void run() {
            if (bannerAdapter != null) {
                if (bannerAdapter.getCount() == page) {
                    page = 0;
                } else {
                    page++;
                }
                imagesViewPager.setCurrentItem(page, true);
                handler.postDelayed(this, delay);
            }
        }
    };

    PosProfileWrapper posProfileWrapper;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.title_activity_main));
        View mView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, mView);
        groupViewModel = ViewModelProviders.of(this).get(PosGroupViewModel.class);
        listItemDialog = new ListItemDialog(this);

        return mView;
    }

    private void createComponents(View v) {
        pagerCardView.setVisibility(View.GONE);
        brandViewModel = ViewModelProviders.of(this).get(BrandViewModel.class);
        posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);
        essentialDataViewModel = ViewModelProviders.of(this).get(EssentialDataViewModel.class);
        // new for single vendor response
        singleVendorCardView = v.findViewById(R.id.single_vendor_cardView);
        sigleVendorCircleImageView = v.findViewById(R.id.single_vendor_circle_imageView);
        sigleVendorNameTextView = v.findViewById(R.id.single_vendor_name_textView);
        mVendorsRecyclerView = v.findViewById(R.id.main_Fragment_vendorRecyclerView);
        orderNowButton = v.findViewById(R.id.order_now_button);
        imagesLayout = v.findViewById(R.id.layout_images_number);
        imagesViewPager = v.findViewById(R.id.pager_photos);
        imageOrderCardView.setVisibility(View.VISIBLE);
        orderNowButton.setVisibility(View.VISIBLE);
        handler = new Handler();
        Log.e(TAG, "createComponents: "+   ScreenDensityUtil.getScreenSizeHeight(getActivity())+"" );

        posProfileWrapper = AppUtils.getProfile(getContext());
        AppPrefs.USER_ID = String.valueOf(posProfileWrapper.getUser().getId());

        if (posProfileWrapper.getUser() != null) {
            if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_SALES_MAN)) {
                getVendorIdFromServer(posProfileWrapper.getUser());
            } else if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_DELIVERY_MAN)) {
                getVendorIdDeliveryUser(posProfileWrapper.getUser());
            }
        }

        getEssentialData(); // call the server to get the vendor list and the ads images


        orderNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_POS)) {
                    if (vendorsList == null) {
                        SelectVendorDialog selectVendorDialog = new SelectVendorDialog();
                        selectVendorDialog.show(getFragmentManager(), "");
                    } else {
                        if (AppPrefs.getVendorID(context) == -1 || vendorsList.size() < 1) {
                            SelectVendorDialog selectVendorDialog = new SelectVendorDialog();
                            selectVendorDialog.show(getFragmentManager(), "");
                        } else {
                            clearFilter();
                            nextActivity();

                        }
                    }
                } else if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_SALES_MAN)) {
                    if (AppPrefs.getVendorID(context) != -1) {
                        getGroup(AppPrefs.getVendorID(getContext()));
                    }

                }
            }
        });


        List<String> privilegeList = posProfileWrapper.getUser().getPermissions();

        for (String privilege : privilegeList) {
            switch (privilege) {
                case AppConstants.ROLE_PRODUCT_ADD:
                case AppConstants.ROLE_PROMOTION_ADD:
                case AppConstants.ROLE_COMBO_ADD:
                    orderNowButton.setVisibility(View.VISIBLE);
                    orderNowButton.setText(getActivity().getString(R.string.order_now_text));
                    break;

                case AppConstants.ROLE_PRODUCT_VIEW:
                case AppConstants.ROLE_PROMOTION_VIEW:
                case AppConstants.ROLE_COMBO_VIEW:
                    orderNowButton.setVisibility(View.VISIBLE);
                    orderNowButton.setText(getActivity().getString(R.string.view_products_text));
                    break;
                case AppConstants.ROLE_SHIP_ORDERS:
                    orderNowButton.setVisibility(View.GONE);
                    break;

            }
        }

    }

    private void getVendorIdFromServer(User user) {
        myService = AppConstants.SERVICE_SALES_VENDOR;
        SalesAgentViewModel salesAgentViewModel = ViewModelProviders.of(this).get(SalesAgentViewModel.class);
        salesAgentViewModel.getVendor(user.getId()).observe(this, new ApiObserver<User>() {
            @Override
            protected void noConnection() {
//                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(User data) {
                try {
                    if (data != null) {
                        if (data.getVendor() != null) {
                            VendorWrapper vendorWrapper = data.getVendor();
                            saveVendor(vendorWrapper);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getVendorIdDeliveryUser(User user) {
        myService = AppConstants.SERVICE_SALES_VENDOR;
        DeliveryUserViewModel deliveryUserViewModel = ViewModelProviders.of(this).get(DeliveryUserViewModel.class);
        deliveryUserViewModel.getVendor(user.getId()).observe(this, new ApiObserver<User>() {
            @Override
            protected void noConnection() {
//                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(User data) {
                try {
                    if (data != null) {
                        if (data.getVendor() != null) {
                            VendorWrapper vendorWrapper = data.getVendor();
                            saveVendor(vendorWrapper);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onItemClick(View view, int position) {
        if (!vendorsList.get(position).isSelected()) {
            saveVendor(vendorsList.get(position));
            AppUtils.changeVendorLogo(getActivity());
            SaveCart.getInstance().SaveVendor(vendorsList.get(position));
            SaveCart.getInstance().getSelectVendorCart();
            adsImagesList = getAdsImagesList(ads);
            fullAdapter();
            AppUtils.updateNotifactionOrder(getActivity());
            for (int i = 0; i < vendorsList.size(); i++) {
                if (position == i) {
                    vendorsList.get(position).setSelected(true);
                    vendorNameTextView.setText(vendorsList.get(position).getName());

                } else {
                    vendorsList.get(i).setSelected(false);
                }

            }
            mVendorAdapter.notifyDataSetChanged();


            getBrand();
        }
    }

    @OnClick(R.id.MoreClick_TextView)
    public void onMoreViewClicked() {
        Bundle vendorBundle = new Bundle();
        vendorBundle.putSerializable(VendorFragment.VENDOR_LIST, vendorsList);
        vendorBundle.putInt(IntentExtraNames.INDEX, VENDORS.getIndex());
        vendorBundle.putString(IntentExtraNames.TITLE, getContext().getResources().getString(R.string.fragment_vendor_title));
        IntentFactory.startActivity(getActivity(), DetailsActivity.class, false, true, vendorBundle);
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    private void getEssentialData() {
        LoadingDialog.showDialog(getActivity());
        myService = AppConstants.SERVICE_ESSENTIAL_DATA;
        essentialDataViewModel.getEssentialData(AppUtils.getProfile(getActivity()).getUser().getId()).observe(this, new ApiObserver<EssentialData>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(EssentialData data) {
                LoadingDialog.dismiss();
                if (data.getCarts() != null) {
                    if (AppUtils.change != null && AppUtils.change) {
                        AppUtils.change = false;
                    } else {
                        SaveCart.getInstance().ClearCart();
                        SaveCart.getInstance().appDateCart(data.getCarts());
                    }
                }
                pagerCardView.setVisibility(View.VISIBLE);

                if (data.getVendors() != null) {
                    vendorsList = data.getVendors();
                }
                if (vendorsList != null) {
                    posProfileWrapper.getUser().setAssignVendor(true);
                    AppPrefs.setPOS(getContext(), JsonUtils.toJsonString(posProfileWrapper));
                    if (vendorsList.size() == 1) {
                        VendorWrapper vendor = vendorsList.get(0);
                        saveVendor(vendor);
                        SaveCart.getInstance().SaveVendor(vendor);
                        SaveCart.getInstance().getSelectVendorCart();
                        AppUtils.updateNotifactionOrder(getActivity());
                        getBrand();
                        // change the view to a card view.
                        // remove the Recycler View
                        vendorNameTextView.setVisibility(View.GONE);
                        MoreClickTextView.setVisibility(View.GONE);
                        mVendorsRecyclerView.setVisibility(View.GONE);
                        // show the Card View
                        singleVendorCardView.setVisibility(View.VISIBLE);
                        if (isAttached) {
                            //Glide call
                            try {
                                if (getActivity() != null)
                                    Glide.with(getActivity())
                                            .load(AppConstants.URL_LIVE + "vendor/attachments/" + vendor.getId() + "/" + vendor.getLogoImageName())
                                            .dontTransform()
                                            .crossFade(800)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .error(R.drawable.place_holder_order)
                                            .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                            .into(sigleVendorCircleImageView);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        sigleVendorNameTextView.setText(vendor.getName());
                    } else if (vendorsList.size() > 1) {
                        //  saveVendor(vendorsList.get(0));
                        // show the list (Recycler view)
                        if (singleVendorCardView.getVisibility() == View.VISIBLE) {
                            singleVendorCardView.setVisibility(View.GONE);
                        }

                        if (mVendorsRecyclerView.getVisibility() == View.GONE) {
                            mVendorsRecyclerView.setVisibility(View.VISIBLE);
                            vendorNameTextView.setVisibility(View.VISIBLE);
                        }
                        if (vendorNameTextView.getVisibility() == View.GONE) {
                            vendorNameTextView.setVisibility(View.VISIBLE);

                        }

                        initVendorRecyclerView();
                    }
                } else {
                    if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_POS)) {
                        posProfileWrapper.getUser().setAssignVendor(false);
                    } else {
                        posProfileWrapper.getUser().setAssignVendor(true);
                    }
                    AppPrefs.setPOS(getContext(), JsonUtils.toJsonString(posProfileWrapper));
                }

                if (data.getAds() != null) {
                    ads = data.getAds();
                    adsImagesList = getAdsImagesList(ads);
                } else {
                    adsImagesList.add("");
                }
                fullAdapter();
            }


            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
               /* try {
                    DialogWrapper dialogWrapper = new DialogWrapper();
                    dialogWrapper.setContext(getContext());
                    dialogWrapper.setTitle(getContext().getString(R.string.titles_dialog_general_error));
                    dialogWrapper.setMessage(error);
                    dialogWrapper.setIsFinished(false);
                    AppUtils.showAlertDialog(dialogWrapper);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

            }
        });
    }

    private void saveVendor(VendorWrapper vendorWrapper) {
        currantVendor = vendorWrapper;
        AppPrefs.setVendorID(getActivity(), vendorWrapper.getId());
        AppPrefs.setCountryId(getContext(), vendorWrapper.getCountry().getId());
        AppPrefs.setCurrency(getContext(), vendorWrapper.getCurrency().getCode());
        AppPrefs.setVendorType(getContext(), vendorWrapper.getPromotionType().getCode());
        AppPrefs.setVendorLogo(getActivity(), vendorWrapper.getLogoImageName());
        AppConstants.CURRENCY = vendorWrapper.getCurrency().getCode();
        if (vendorWrapper.isHasPromotionOther() != null)
            AppConstants.HAS_PROMOTION_OTHER = vendorWrapper.isHasPromotionOther();

        if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_POS)) {
            posUserViewModel.getPosGroupId(posProfileWrapper.getUser().getId(), vendorWrapper.getId()).observe(this, new ApiObserver<PosGroupModel>() {
                @Override
                protected void noConnection() {

                }

                @Override
                protected void onSuccess(PosGroupModel data) {
                    posProfileWrapper.getUser().setPosGroup(data.getId());
                    AppPrefs.setPOS(getContext(), JsonUtils.toJsonString(posProfileWrapper));
                }

                @Override
                protected void onError(String error) {
                    if (error != null) {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void initVendorRecyclerView() {
        if (vendorsList == null) {
            Toast.makeText(getActivity(), R.string.no_vendors_for_this_user, Toast.LENGTH_SHORT).show();
            return;
        }
        if (getActivity() != null)
            switch (vendorsList.size()) {
                case 2:
                case 3:
                    screenSize = (int) (ScreenDensityUtil.getScreenSizeWidth(getActivity()) / 3.2);
                    break;
                default:
                    screenSize = (int) (ScreenDensityUtil.getScreenSizeWidth(getActivity()) / 4.3);
                    break;
            }
        if (AppPrefs.getVendorID(context) != -1) {
            for (int i = 0; i < vendorsList.size(); i++) {
                if (vendorsList.get(i).getId().equals(AppPrefs.getVendorID(context))) {
                    vendorsList.get(i).setSelected(true);
                    VendorWrapper vendorWrapper = vendorsList.get(i);
                    vendorNameTextView.setText(String.valueOf(vendorWrapper.getName()));
                    saveVendor(vendorWrapper);
                    SaveCart.getInstance().SaveVendor(vendorWrapper);
                    AppUtils.changeVendorLogo(getActivity());
                    SaveCart.getInstance().getSelectVendorCart();
                    AppUtils.updateNotifactionOrder(getActivity());
                    Collections.swap(vendorsList, i, 0);
                }
            }

        } else {
            vendorNameTextView.setText(R.string.please_select_vendor);
        }
        if (vendorsList.size() > 4) {
            MoreClickTextView.setVisibility(View.VISIBLE);
        } else if (MoreClickTextView.getVisibility() == View.VISIBLE) {
            MoreClickTextView.setVisibility(View.GONE);
        }
        mVendorAdapter = new MainFragmentVendorRecyclerViewAdapter(getActivity(), vendorsList, screenSize, String.valueOf(System.currentTimeMillis()));
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mVendorAdapter.setClickListener(this);
        mVendorsRecyclerView.setAdapter(mVendorAdapter);
        mVendorsRecyclerView.setLayoutManager(horizontalLayoutManager);
        getBrand();
    }

    private void fullAdapter() {
        bannerAdapter = new InfinitePagerAdapter(new BannerAdapter(getActivity(), adsImagesList, getContext(), String.valueOf(System.currentTimeMillis())));
        imagesViewPager.setPageTransformer(true, new FadePageTransformer());
        imagesViewPager.setAdapter(bannerAdapter);
        imagesViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                changeViews();
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, delay);
            }

            @Override
            public void onPageSelected(int position) {
                page = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        initViews();
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
        LoadingDialog.dismiss();
        if (currantVendor != null) {
            AppPrefs.setVendorID(getActivity(), currantVendor.getId());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isInternetConnection) {
            this.onViewCreated(Objects.requireNonNull(getView()), new Bundle());
        } else {
            handler.postDelayed(runnable, delay);
        }
    }

    private void nextActivity() {
        Bundle bundle = new Bundle();
        bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, 3);
        bundle.putInt(IntentExtraNames.INDEX, ORDER_NOW.getIndex());
        if (getContext() != null)
            bundle.putString(IntentExtraNames.TITLE, getContext().getResources().getString(R.string.button_label_order_now));
        IntentFactory.startActivity(getActivity(), DetailsActivity.class, false, true, bundle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && !isInternetConnection) {
            isInternetConnection = true;
            this.onViewCreated(Objects.requireNonNull(getView()), new Bundle());
        } else {
            this.context = getContext();
            isAttached = true;
        }
    }

    private void initViews() {
        imagesLayout.removeAllViews();
        imagesLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        List<ImageView> imageViews = new ArrayList<>();
        for (int i = 0; i < adsImagesList.size(); i++) {
            ImageView imageView = new ImageView(context);
            imageView.setPadding(8, 0, 8, 0);
            imageView.setMinimumWidth(35);
            imageView.setMinimumHeight(35);
            if (i == 0) {
                imageView.setImageResource(R.drawable.loading2);
                imageView.setColorFilter(ContextCompat.getColor(context, R.color.default_blue_light), PorterDuff.Mode.MULTIPLY);

            } else {
                imageView.setImageResource(R.drawable.loading2);
                imageView.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.MULTIPLY);

            }
            imageViews.add(imageView);
            imagesLayout.addView(imageView);
        }
        changeViews();
    }

    private void changeViews() {
        //LinearLayout, which contains several child TextViews and ImageViews ...
        for (int i = 0; i < imagesLayout.getChildCount(); i++) {
            ImageView imageView = ((ImageView) imagesLayout.getChildAt(i));
            if (imagesViewPager.getCurrentItem() == i) {
                imageView.setImageResource(R.drawable.loading2);
                imageView.setColorFilter(ContextCompat.getColor(context, R.color.default_blue_light), PorterDuff.Mode.MULTIPLY);

            } else {
                imageView.setImageResource(R.drawable.loading2);
                imageView.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.MULTIPLY);

            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }

    private ArrayList<String> getAdsImagesList(List<VendorAd> vendorAdList) {
        ArrayList<String> images = new ArrayList<>();
        Long vendorId = AppPrefs.getVendorID(getContext());
        if (vendorId.equals(-1L)) {
            images.add("");
        } else {
            for (VendorAd item : vendorAdList) {
                if (item.getVendor().getId().equals(vendorId)) {
                    images.add(item.getId() + "/" + item.getImagesName());
                }
            }
        }
        if (images.size() == 0) {
            images.add("");
        }
        return images;
    }

    private void clearFilter() {
        AppUtils.filterCategory = null;
        AppUtils.selectCategory = null;
        AppUtils.allBrand = null;
        AppUtils.selectBrands = null;
    }

    private void getBrand() {
        myService = AppConstants.SERVICE_BRAND;
        AppConstants.IS_PROMOTION = false;

        brandLoadingView.setVisibility(View.VISIBLE);
        brandRecyclerView.setVisibility(View.GONE);
        brandViewModel.getBrands(AppPrefs.getVendorID(context)).observe(this, new ApiObserver<List<BrandWrapper>>() {
            @Override
            protected void noConnection() {
                brandLoadingView.setVisibility(View.GONE);
                brandRecyclerView.setVisibility(View.GONE);
//                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<BrandWrapper> data) {
                brandLoadingView.setVisibility(View.GONE);
                brandRecyclerView.setVisibility(View.VISIBLE);
                brandRecyclerView.setAdapter(new MainBrandAdapter(getContext(), data, String.valueOf(System.currentTimeMillis())));
            }

            @Override
            protected void onError(String error) {
                brandLoadingView.setVisibility(View.GONE);
                brandRecyclerView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LoadingDialog.dismiss();
        isAttached = false;
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if (AppConstants.SERVICE_ESSENTIAL_DATA.equals(myService)) {
                getEssentialData();
            } else if (AppConstants.SERVICE_BRAND.equals(myService)) {
                getBrand();
            } else if (AppConstants.SERVICE_SALES_VENDOR.equals(myService) && posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_DELIVERY_MAN)) {
                getVendorIdDeliveryUser(posProfileWrapper.getUser());
            } else if (AppConstants.SERVICE_SALES_VENDOR.equals(myService) && posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_SALES_MAN)) {
                getVendorIdFromServer(posProfileWrapper.getUser());
            }
        }
    }

    private void getGroup(Long vendorId) {
        LoadingDialog.showDialog(getContext());
        groupViewModel.getAllGroups(vendorId).observe(this, new ApiObserver<List<GroupModel>>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();

            }

            @Override
            protected void onSuccess(List<GroupModel> data) {
                LoadingDialog.dismiss();
                if (data != null) {
                    groupModelList = data;
                    showGroupList();
                }

            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showGroupList() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ListItemDialog.DATA, (Serializable) groupModelList);
        bundle.putString(ListItemDialog.TITLE, getString(R.string.please_select_group));
        listItemDialog = new ListItemDialog(this);
        listItemDialog.setArguments(bundle);
        if (getFragmentManager() != null)
            listItemDialog.show(getFragmentManager(), "");

    }

    @Override
    public void getPosition(int position) {
        listItemDialog.dismiss();
        posProfileWrapper.getUser().setPosGroup(groupModelList.get(position).getId());
        AppPrefs.setPOS(getContext(), JsonUtils.toJsonString(posProfileWrapper));
        clearFilter();
        nextActivity();
    }
}