package com.telephoenic.orders360.controller.interfaces;

public interface GetAmount {
    void getAmount (Double amount);
}
