package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class OrderItems implements Serializable  {

	private static final long serialVersionUID = 1L;

	@JsonProperty("orderedQuantity")
	protected String orderedQuantity;

	@JsonProperty("totalAmount")
	protected Double totalAmount;

	@JsonProperty("expiryDate")
	protected String expiryDate;

	@JsonProperty("price")
	protected Double price;

	@JsonProperty("vat")
	protected Double vat;

	@JsonProperty("object")
	private Object object;

	public String getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(String orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getVat() {
		return vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}