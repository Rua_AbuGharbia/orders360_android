package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class PromotionValue implements Serializable {

	private Long id ;

	private Long freeItem;

	private Promotion promotion;

	private Integer promotionType;

	private Long promoitonTransient;

	private String count ;// TODO Added By Rua  // Ordered PromotionValueQuantity

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFreeItem() {
		return freeItem;
	}

	public void setFreeItem(Long freeItem) {
		this.freeItem = freeItem;
	}
	
	@JsonIgnore
	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public Long getPromoitonTransient() {
		return promoitonTransient;
	}

	public void setPromoitonTransient(Long promoitonTransient) {
		this.promoitonTransient = promoitonTransient;
	}
}
