package com.telephoenic.orders360.controller.exception;

/**
 * Created by Ihab on 9/16/2015.
 */
public class ExceptionHandling extends Exception {

    private String message;

    public ExceptionHandling(String message){
        super(message);
        this.message = message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }



}
