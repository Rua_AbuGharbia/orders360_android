package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import java.util.Locale;

public class LocaleUtils {


    public static Locale getCurrentLocale(@NonNull Context context) {

        if (context == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        } else {
            return context.getResources().getConfiguration().locale;
        }
    }

    public static String getCurrentLocaleString(Context context) {

        Locale locale = getCurrentLocale(context);
        if (locale == null) {
            return null;
        }
        return locale.toString();
    }

    public static String getCurrentLanguage(Context context) {

        Locale locale = getCurrentLocale(context);
        if (locale == null) {
            return null;
        }
        return locale.getLanguage();
    }
}
