package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductsPointsModel;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.ProductsPointsAdapter;
import com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.ProductViewModel;
import com.telephoenic.orders360.viewmodel.RewardViewModel;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;


public class RewardsFragment extends BlankFragment implements ConfirmOrderDialog.onClickStatus , DialogUtils.OnDialogClickListener
        ,ImageController {
    private String TAG = RewardsFragment.class.getName();
    ListView productPointsListView;
    ProductsPointsAdapter productsPointsAdapter;
    List<ProductsPointsModel> productPointsList;
    LinearLayout emptyLayout ;
    TextView emptyText ;

    TextView myPoints;
    Long pointsDouble;
    Long vendorID;
    int indexClick;
    ProductsPointsModel productsPointsModel;

    private RewardViewModel rewardViewModel;
    private ProductViewModel productViewModel;

    private int pageNumber = 0;
    boolean flag_loading = true;

    PosProfileWrapper posProfileWrapper ;
    private String myService = "";

    public RewardsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rewards, container, false);
    }

    private void createComponents(View v) {

        productPointsListView = v.findViewById(R.id.product_points_list);
        emptyLayout = v.findViewById(R.id.empty_layout);
        emptyText = v.findViewById(R.id.text_empty_message);
        emptyText.setText(getResources().getString(R.string.no_product_available));
        myPoints = v.findViewById(R.id.text_points);

        productPointsList = new ArrayList<>();

        Bundle bundle = getArguments();

        if (bundle != null) {
            pointsDouble = bundle.getLong(IntentExtraNames.POINTS);
            vendorID = bundle.getLong(IntentExtraNames.ID);
        }

        posProfileWrapper  = AppUtils.getProfile(getContext());

        if (pointsDouble == null) {
            Log.e(TAG, "createComponents: pointsDouble = " + pointsDouble);
            pointsDouble = 0L;
        }

        myPoints.setText(pointsDouble + "");

        prepareAndSendProductRequest();

        productPointsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int firstVisibleItem1 = 0, visibleItemCount1 = 0 ,totalItemCount1 =0 ;
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                final int lastItem = firstVisibleItem1 + visibleItemCount1;
                if (lastItem == totalItemCount1 && scrollState == SCROLL_STATE_IDLE) {
                    if(!flag_loading)
                    {
                        flag_loading = true;
                        //productListView.addFooterView(footerProgressBar);
                        prepareAndSendProductRequest();

                    }
                }
            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                firstVisibleItem1 = firstVisibleItem;
                visibleItemCount1 = visibleItemCount;
                totalItemCount1 = totalItemCount;

            }
        });

    }

    private void prepareAndSendProductRequest() {
        myService = AppConstants.SERVICE_PRODUCT_WITH_POINTS;
        LoadingDialog.showDialog(getActivity());
        if (AppPrefs.getVendorID(getContext()) != -1) {
            productViewModel.getProductWithPoints(pageNumber+"","10",AppPrefs.getVendorID(getContext()).toString())
                    .observe(this, new ApiObserver<ResponseBody>() {
                        @Override
                        protected void noConnection() {
                            LoadingDialog.dismiss();
                            noConnectionInternetDialog();
                        }

                        @Override
                        protected void onSuccess(ResponseBody data) {
                            LoadingDialog.dismiss();

                            try {
                                JSONObject jsonObj = new JSONObject(data.string());
                                JSONArray jsonArray = jsonObj.getJSONArray("content");
                                if (jsonArray.length() > 0) {
                                    flag_loading = false;
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ProductsPointsModel product = JsonUtils.toJavaObject(jsonArray.get(i).toString(), ProductsPointsModel.class);
                                        productPointsList.add(product);
                                    }
                                    pageNumber++;
                                }

                                fillData();
                                if(productPointsList.size()==0){
                                    productPointsListView.setVisibility(View.GONE);
                                    emptyLayout.setVisibility(View.VISIBLE);
                                } else {
                                    productPointsListView.setVisibility(View.VISIBLE);
                                    emptyLayout.setVisibility(View.GONE);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        protected void onError(String error) {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void prepareAndSendRedeemRequest() {
        myService =AppConstants.SERVICE_REDEEM_POINTS;
        LoadingDialog.showDialog(getContext());
        ProductsPointsModel model = productPointsList.get(indexClick);
        if (AppPrefs.getVendorID(getContext()) != -1) {
            rewardViewModel.redeemPoints(posProfileWrapper.getUser().getId().toString(),
                    model.getId(),AppPrefs.getVendorID(getContext()).toString()).observe(this, new ApiObserver<ResponseBody>() {
                @Override
                protected void noConnection() {
                    LoadingDialog.dismiss();
                    noConnectionInternetDialog();
                }

                @Override
                protected void onSuccess(ResponseBody data) {
                    LoadingDialog.dismiss();
                    try {
                        AppUtils.showAlertToast(getContext().getResources().getString(R.string.toast_redeemed_successfully), getContext());
                        myPoints.setText(data.string());
                        if(!data.string().equals("")){
                            pointsDouble = pointsDouble.valueOf(data.string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                    }
                }

                @Override
                protected void onError(String error) {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void getStatusClick(int status) {

        switch (status){
            case ConfirmOrderDialog.StatusClick.OKAY:
                String points = productsPointsModel.getCostInRewardPoints();
                Log.e(TAG, "onClick: points : " + points);
                int productPoints = Integer.parseInt(points);
                Long intPoints = pointsDouble;
                if (productPoints > intPoints) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.toast_points_not_enough), Toast.LENGTH_SHORT).show();
                } else {// TODO call redeem serviec
                    prepareAndSendRedeemRequest();
                }
                break;
        }

    }

    private void fillData() {
        if (productsPointsAdapter == null) {
            productsPointsAdapter = new ProductsPointsAdapter(getActivity(), R.layout.item_product_points, productPointsList, onIconClicked,String.valueOf(System.currentTimeMillis()));
            productPointsListView.setAdapter(productsPointsAdapter);
        } else {
            productsPointsAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(pageNumber >0){
            pageNumber--;
        }
        productsPointsAdapter = null;
    }

    private View.OnClickListener onIconClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            indexClick = (Integer) v.getTag();
            switch (v.getId()) {
                case R.id.button_product_points:
                    displayConfirmDialog();
                    break;
            }
        }
    };

    public void displayConfirmDialog() {
        productsPointsModel = productPointsList.get(indexClick);
        String msg = getString(R.string.Confirmation_question);
        Bundle bundle = new Bundle();
        bundle.putString(ConfirmOrderDialog.MASSAGE,msg);
        ConfirmOrderDialog  confirmOrderDialog = new ConfirmOrderDialog();
        confirmOrderDialog.setArguments(bundle);
        confirmOrderDialog.onClickStatus(this);
        confirmOrderDialog.show(getFragmentManager(),"");

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rewardViewModel = ViewModelProviders.of(this).get(RewardViewModel.class);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        createComponents(view);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_PRODUCT_WITH_POINTS.equals(myService)){
                prepareAndSendProductRequest();
            }else if(AppConstants.SERVICE_REDEEM_POINTS.equals(myService)){
                prepareAndSendRedeemRequest();
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }

}
