package com.telephoenic.orders360.controller.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.utils.AppUtils;


public class BlankFragment extends Fragment {
    protected boolean isInternetConnection = true;

    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_blank, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    protected void gotoFragment(Fragment fragment, FragmentManager fragmentManager, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
//
// Transaction Animations before replace otherwise, it will have no effect.
        fragmentTransaction.replace(R.id.container, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    protected void gotoFragment(Fragment fragment, FragmentManager fragmentManager, boolean addToBackStack, Bundle data) {
        fragment.setArguments(data);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    protected void gotoFragment(Fragment fragment, FragmentManager fragmentManager) {
        gotoFragment(fragment, fragmentManager, true);
    }

    protected void gotoFragment(Fragment fragment, FragmentManager fragmentManager, Bundle bundle) {
        gotoFragment(fragment, fragmentManager, true, bundle);
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                 @NonNull Fragment fragment,
                                 @NonNull String fragmentTag,
                                 @Nullable String backStackStateName) {
        getFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .commit();
    }

}
