package com.telephoenic.orders360.controller.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingeSerialViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Scan_ImageView)
    public ImageView scanImageView;
    @BindInput(R.id.FromNumberScan_EditText)
    public Orders360EditText numberScanEditText;
    @BindView(R.id.Cancel_ImageView)
    public ImageView cancelImageView;
    @BindView(R.id.From_TextView)
    public TextView fromTextView;
    @BindView(R.id.To_TextView)
    public TextView toTextView;
    @BindInput(R.id.ToNumberScan_EditText)
    public Orders360EditText toNumberScanEditText;

    public SingeSerialViewHolder(@NonNull View itemView) {
        super(itemView);
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this, itemView);
        ButterKnife.bind(this, itemView);
    }
}
