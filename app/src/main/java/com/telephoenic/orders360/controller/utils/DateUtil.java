package com.telephoenic.orders360.controller.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    public static String DEFAULT_DATE = "dd/MM/yyyy";

    public static Date parseDate(String date) {
        if(date == null || "".equals(date)) {
            return null;
        }
        return new Date(Long.valueOf(date));
    }

    public static String formatDate(Date date) {
        return formatDate(date, DEFAULT_DATE);
    }

    public static String formatDate(Date date, String pattern) {
        if(date == null) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.ENGLISH);
        return formatter.format(date);
    }
}
