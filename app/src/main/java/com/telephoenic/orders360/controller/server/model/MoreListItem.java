package com.telephoenic.orders360.controller.server.model;

import java.util.List;

public class MoreListItem {

    private String name, itemType;
    private int image, type;
    private boolean showItem;
    private List<MoreItemChild> moreItemChildren;
    // set enum here to know.

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public MoreListItem(String name, int image, int type) {
        this.name = name;
        this.image = image;
        this.type = type;
    }

    public MoreListItem(String name, String itemType, int image, List<MoreItemChild> moreItemChildren, boolean showItem) {
        this.name = name;
        this.itemType = itemType;
        this.image = image;
        this.moreItemChildren = moreItemChildren;
        this.showItem = showItem;
    }

    public MoreListItem() {
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public List<MoreItemChild> getMoreItemChildren() {
        return moreItemChildren;
    }

    public void setMoreItemChildren(List<MoreItemChild> moreItemChildren) {
        this.moreItemChildren = moreItemChildren;
    }

    public boolean isShowItem() {
        return showItem;
    }

    public void setShowItem(boolean showItem) {
        this.showItem = showItem;
    }
}
