package com.telephoenic.orders360.controller.interfaces;

import android.content.Context;

public interface OnResponse {

    void onSuccess(Object response, Context context);

    void onException(Exception exception, Context context);
}