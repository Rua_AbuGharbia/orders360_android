package com.telephoenic.orders360.controller.utils;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Filter;

import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.view.adapters.VendorListAdapter;

import java.util.ArrayList;

public class FilterUtils extends Filter {
    private ArrayList<VendorWrapper> itemList;
    private ArrayList<VendorWrapper> filteredItemList;
    private VendorListAdapter adapter;

    public FilterUtils(ArrayList<VendorWrapper> itemList, VendorListAdapter adapter ) {
        this.itemList = itemList;
        this.adapter = adapter;
        this.filteredItemList = new ArrayList<>();
    }


    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredItemList.clear();
        final FilterResults results = new FilterResults();

        //here you need to add proper items do filteredContactList
        for (final VendorWrapper item : itemList) {
            if (item.getName().toLowerCase().trim().contains(constraint.toString().trim().toLowerCase())) {
                filteredItemList.add(item);
                Log.e("NAme",item.getName()+"");
            }
        }

        results.values = filteredItemList;
        results.count = filteredItemList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.setList(filteredItemList);

    }
}
