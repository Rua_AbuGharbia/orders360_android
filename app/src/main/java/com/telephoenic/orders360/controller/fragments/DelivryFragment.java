package com.telephoenic.orders360.controller.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.adapters.DeliveryOrdersAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DelivryFragment extends BlankFragment implements OnRecyclerClick {

    private static final int NO_SELECT_ITEM = -1;
    @BindView(R.id.RightMenu_FrameLayout)
    FrameLayout RightMenuFrameLayout;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.Filter_ImageView)
    ImageView filterImageView;
    @BindView(R.id.Filter_TextView)
    TextView filterTextView;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    private FilterDeleverFragment filterFragment;
    private DeliveryOrdersAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.delver_fragment, container, false);
        ButterKnife.bind(this, view);
        if (getActivity() != null)
            createComponents(view);

        return view;
    }

    private void createComponents(View v) {
        filterFragment = new FilterDeleverFragment();
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawerLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        recyclerView.setAdapter(new DeliveryOrdersAdapter(getContext(), this));

        if (drawerLayout != null) {
            replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), "Orders Delivery");

        //     AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_orders));
        createComponents(view);
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick({R.id.Filter_ImageView, R.id.Filter_TextView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Filter_ImageView:
            case R.id.Filter_TextView:
                if (drawerLayout != null) {
                    replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
                    drawerLayout.openDrawer(Gravity.END);
                }
                break;
        }
    }

    @Override
    public void getPosition(int position) {
       // gotoFragment(new ReadyToShipFragment(), getActivity().getSupportFragmentManager());
        gotoFragment(new OrderDeliveryDetailsFragment(), getActivity().getSupportFragmentManager());
    }
}
