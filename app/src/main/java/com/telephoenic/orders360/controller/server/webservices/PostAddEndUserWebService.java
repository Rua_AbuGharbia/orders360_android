package com.telephoenic.orders360.controller.server.webservices;


import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.controller.RestWebService;

public class PostAddEndUserWebService extends RestWebService {
    @Override
    protected Object callService(String... parameters) {

        final String url = AppConstants.URL_LIVE + "campaign/end_user/save";
        Object response = callPostService(url, parameters[0]);

        return response;
    }
}