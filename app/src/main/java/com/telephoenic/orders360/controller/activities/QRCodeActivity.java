package com.telephoenic.orders360.controller.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.telephoenic.orders360.R;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QRCodeActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    @BindView(R.id.ScanBar_View)
    View scanBarView;
    private QRCodeReaderView qrCodeReaderView;
    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        qrCodeReaderView = findViewById(R.id.qrdecoderview);

        animation = AnimationUtils.loadAnimation(QRCodeActivity.this, R.anim.scan_animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                scanBarView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        try {
            qrCodeReaderView.setBackCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Use this function to enable/disable decoding
//        qrCodeReaderView.setQRDecodingEnabled(true);
//
//        // Use this function to change the autofocus interval (default is 5 secs)
//        qrCodeReaderView.setAutofocusInterval(2000L);
//
//        // Use this function to enable/disable Torch
//        qrCodeReaderView.setTorchEnabled(true);
//
//        // Use this function to set front camera preview
//       qrCodeReaderView.setFrontCamera();
//
//        // Use this function to set back camera preview
//        qrCodeReaderView.setBackCamera();

    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        animation.cancel();
        scanBarView.setVisibility(View.GONE);
        scanBarView.clearAnimation();
        Toast.makeText(this, text + "", Toast.LENGTH_SHORT).show();
        qrCodeReaderView.setQRDecodingEnabled(false);
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            qrCodeReaderView.startCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    @OnClick(R.id.order_now_button)
    public void onViewClicked() {
        scanBarView.setVisibility(View.VISIBLE);
        scanBarView.startAnimation(animation);
        qrCodeReaderView.setQRDecodingEnabled(true);
        qrCodeReaderView.setOnQRCodeReadListener(this);
    }
}
