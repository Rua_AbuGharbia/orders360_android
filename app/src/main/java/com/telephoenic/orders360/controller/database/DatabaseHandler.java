package com.telephoenic.orders360.controller.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Order360_POS_users";
    private static final String TABLE_LIST = "user";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE_NUMBER = "mobile_number";
    private static final String KEY_LOGGED_USER_LOCATION = "logged_user_location";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_PASSWORD_CHANGED = "password_changed";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_LIST + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"+
                KEY_EMAIL + " TEXT," + KEY_MOBILE_NUMBER + " TEXT," + KEY_LOGGED_USER_LOCATION + " TEXT," +
                KEY_PASSWORD + " TEXT," +
                KEY_PASSWORD_CHANGED +" TEXT"+ ")";
        try {
            db.execSQL(CREATE_USER_TABLE);
        }catch (Exception e){
            Log.d("Error : ",e.getMessage().toString());
            System.out.print(e.toString());
        }

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);
        // Create tables again
        onCreate(db);
    }

    // code to add the new user
   public boolean addUser(User user) {
       try{
           SQLiteDatabase db = this.getWritableDatabase();
           ContentValues values = new ContentValues();
           //values.put(KEY_ID, user.get_id());
           values.put(KEY_NAME, user.get_name());
           values.put(KEY_EMAIL, user.get_email());
           values.put(KEY_MOBILE_NUMBER, user.get_mobileNumber());
           values.put(KEY_LOGGED_USER_LOCATION, user.get_loggedUserLocation());
           values.put(KEY_PASSWORD, user.get_password());
           values.put(KEY_PASSWORD_CHANGED, user.get_passwordChanged());
           // Inserting Row
           db.insert(TABLE_LIST, null, values);
           //2nd argument is String containing nullColumnHack
           db.close(); // Closing database connection
           return true;

       }catch (Exception e){
           Log.d("Error : ",e.getMessage().toString());
           return false;
       }

    }

    // code to get the single contact
    public User getUser(int id) {
        User user = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.query(TABLE_LIST, new String[]{KEY_ID,
                            KEY_NAME, KEY_EMAIL, KEY_MOBILE_NUMBER, KEY_LOGGED_USER_LOCATION,KEY_PASSWORD,KEY_PASSWORD_CHANGED}, KEY_ID + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
           /* if (cursor != null)
                cursor.moveToFirst();*/
            if (cursor != null && cursor.getCount() > 0)
                if (cursor.moveToFirst())
                    user = new User(cursor.getInt(0),
                            cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // return user
        return user;
    }

    // code to get the single contact
    public User getUser(String userName ,String password ) {
        try{

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.query(TABLE_LIST, new String[] { KEY_ID, KEY_NAME,
                            KEY_EMAIL ,KEY_MOBILE_NUMBER, KEY_LOGGED_USER_LOCATION,KEY_PASSWORD,KEY_PASSWORD_CHANGED}, KEY_NAME + "=?" + " and "+KEY_PASSWORD + "=?",
                    new String[] {userName , password }, null, null, null, null);
            if (cursor != null){
                cursor.moveToFirst();
                User user = new User(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6));
                return user;
            }

        }catch (Exception e){
            Log.e("Error",e.getMessage().toString());
            return null;
        }

        return null;
    }

    // code to get all contacts in a list view
    public List<User> getAllContacts() {
        List<User> users = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.set_id(cursor.getInt(0));
                user.set_name(cursor.getString(1));
                user.set_email(cursor.getString(2));
                user.set_mobileNumber(cursor.getString(3));
                user.set_loggedUserLocation(cursor.getString(4));
                user.set_password(cursor.getString(5));
                user.set_passwordChanged(cursor.getString(6));
                // Adding user to list
                users.add(user);
            } while (cursor.moveToNext());
        }

        // return contact list
        return users;
    }

    // code to update the single user
    public int updateContact(User user) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            // values.put(KEY_ID, user.get_id());
            // values.put(KEY_NAME, user.get_name());
            // values.put(KEY_EMAIL, user.get_email());
            // values.put(KEY_MOBILE_NUMBER, user.get_phoneNumber());
            //  values.put(KEY_USER_TYPE, user.get_userType());
            //  values.put(KEY_LOGGED_USER_LOCATION, user.get_loggedUserLocation());
            values.put(KEY_PASSWORD, user.get_password());
            values.put(KEY_PASSWORD_CHANGED, user.get_passwordChanged());

            // updating row
            return db.update(TABLE_LIST, values, KEY_ID + " = ?",
                    new String[] { String.valueOf(user.get_id()) });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    // Deleting single user
    public void deleteContact(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LIST, KEY_ID + " = ?",
                new String[] { String.valueOf(user.get_id()) });
        db.close();
    }

    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }
}
