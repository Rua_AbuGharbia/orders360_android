package com.telephoenic.orders360.controller.server.model;

import java.util.ArrayList;
import java.util.List;

public class EssentialData {

    private ArrayList<VendorWrapper> vendors;
    //  private ArrayList<String> images;
    private List<VendorAd> ads;
    private List<AddToCartModel> carts;

    private static EssentialData essentialData = null;

    private EssentialData() {
        if (essentialData != null) {
            // handel java reflection API
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public ArrayList<VendorWrapper> getVendors() {
        return vendors;
    }

    public void setVendors(ArrayList<VendorWrapper> vendors) {
        this.vendors = vendors;
    }

    public void setCarts(List<AddToCartModel> carts) {
        this.carts = carts;
    }

    public List<AddToCartModel> getCarts() {
        return carts;
    }

//    public ArrayList<String> getImages() {
//        return images;
//    }
//
//    public void setImages(ArrayList<String> images) {
//        this.images = images;
//    }

    public List<VendorAd> getAds() {
        return ads;
    }

    public void setAds(List<VendorAd> ads) {
        this.ads = ads;
    }

    @Override
    public String toString() {
        return "EssentialData{" +
                "vendors=" + vendors +
                ", ads=" + ads +
                ",carts=" + carts +
                '}';
    }

    public synchronized static EssentialData getInstance() {
        if (essentialData == null) {
            essentialData = new EssentialData();
        }
        return essentialData;
    }
}