package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;

public class PosUser extends User implements Serializable {

    private Double latitude;
    private Double longitude;
    private String ownerName;
    private String ownerMobileNumber;
    private String landNumber;
    private String fax;
    private PosTypeModel posType;
    private RegionModel region;
    private String registrationId;
    private Boolean assignVendor;

    public PosUser(double latitude, double longitude, String ownerName, String ownerMobileNumber, String landNumber, String fax, PosTypeModel posType, RegionModel region, String registrationId, boolean assignVendor) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.ownerName = ownerName;
        this.ownerMobileNumber = ownerMobileNumber;
        this.landNumber = landNumber;
        this.fax = fax;
        this.posType = posType;
        this.region = region;
        this.registrationId = registrationId;
        this.assignVendor = assignVendor;
    }

    public PosUser() {
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerMobileNumber() {
        return ownerMobileNumber;
    }

    public void setOwnerMobileNumber(String ownerMobileNumber) {
        this.ownerMobileNumber = ownerMobileNumber;
    }

    public String getLandNumber() {
        return landNumber;
    }

    public void setLandNumber(String landNumber) {
        this.landNumber = landNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public PosTypeModel getPosType() {
        return posType;
    }

    public void setPosType(PosTypeModel posType) {
        this.posType = posType;
    }

    public RegionModel getRegion() {
        return region;
    }

    public void setRegion(RegionModel region) {
        this.region = region;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public boolean isAssignVendor() {
        return assignVendor;
    }

    public void setAssignVendor(boolean assignVendor) {
        this.assignVendor = assignVendor;
    }

    @Override
    public String toString() {
        return "PosUser{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", ownerMobileNumber='" + ownerMobileNumber + '\'' +
                ", landNumber='" + landNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", posType=" + posType +
                ", region=" + region +
                ", registrationId='" + registrationId + '\'' +
                ", assignVendor=" + assignVendor +
                '}';
    }
}
