package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class OrderModel implements Serializable {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("deleted")
    private boolean deleted;

    @JsonProperty("inactive")
    private boolean inactive;

    @JsonProperty("orderNumber")
    private String orderNumber;

    @JsonProperty("createdDate")
    private String createdDate;

    private Double totalAmount;

    @JsonProperty("posUser")
    private User posUser;

    @JsonProperty("orderStatus")
    private DeliveryStatus orderStatus;

    public Audit getAudit() {
        return audit;
    }

    public void setAudit(Audit audit) {
        this.audit = audit;
    }

    private Audit audit;

	/*@JsonProperty("orderItems")
	private List<Object> orderItems;*/

    @JsonProperty("singleItems")
    private List<SingleItem> singleItems;

    @JsonProperty("comboItems")
    private List<ComboItem> comboItems;

    @JsonProperty("promotionItems")   // TODO List of Promotion Items
    private List<PromotionItem> promotionItems;

    @JsonProperty("vendor")
    private VendorWrapper vendor;

    private List<CartRewardItem> rewardItems;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public User getPosUser() {
        return posUser;
    }

    public void setPosUser(User posUser) {
        this.posUser = posUser;
    }

    public DeliveryStatus getDeliveryStatus() {
        return orderStatus;
    }

    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.orderStatus = deliveryStatus;
    }

    public List<SingleItem> getSingleItems() {
        return singleItems;
    }

    public void setSingleItems(List<SingleItem> singleItems) {
        this.singleItems = singleItems;
    }

    public List<ComboItem> getComboItems() {
        return comboItems;
    }

    public void setComboItems(List<ComboItem> comboItems) {
        this.comboItems = comboItems;
    }

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    public List<PromotionItem> getPromotionItems() {
        return promotionItems;
    }

    public void setPromotionItems(List<PromotionItem> promotionItems) {
        this.promotionItems = promotionItems;
    }

    public DeliveryStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(DeliveryStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<CartRewardItem> getRewardItems() {
        return rewardItems;
    }

    public void setRewardItems(List<CartRewardItem> rewardItems) {
        this.rewardItems = rewardItems;
    }
}