package com.telephoenic.orders360.controller.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;


public class ProfilePersonallyFragment extends BlankFragment {
    private String TAG = ProfilePersonallyFragment.class.getName();

    TextView nameTextView , mobileTextView , emailTextView  ;
    private int id;

    public ProfilePersonallyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_profile_personly, container, false);
    }

    private void createComponents(View v){
        nameTextView = v.findViewById(R.id.textview_name) ;
        mobileTextView = v.findViewById(R.id.textview_moblie) ;
        emailTextView = v.findViewById(R.id.textview_email) ;
        PosProfileWrapper posProfileWrapper = AppUtils.getProfile(getContext());
        //User posProfileWrapper = getUser(id);
        if(posProfileWrapper !=null){
            nameTextView.setText(posProfileWrapper.getUser().getName());
            mobileTextView.setText(posProfileWrapper.getUser().getMobileNumber());
            emailTextView.setText(posProfileWrapper.getUser().getEmail());
        }

    }
 /*   private User getUser(int id){
        User user = null;
        DatabaseHandler databaseHandler = new DatabaseHandler(getActivity());
        user = databaseHandler.getUser(id);
        return user;
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

      /*  Bundle bundle = getArguments();
        if(bundle!= null){
            id = bundle.getInt(IntentExtraNames.ID);
        }*/
        createComponents(view);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
