package com.telephoenic.orders360.controller.interfaces;

import android.content.Intent;

public interface ImageController {
    void onMyActivityResult(int requestCode, int resultCode, Intent data);
}
