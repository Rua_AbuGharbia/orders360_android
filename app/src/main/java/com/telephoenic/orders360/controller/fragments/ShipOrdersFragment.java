package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.EndlessRecyclerViewScrollListener;

import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.ReadyToShipOrdersAdapter;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ShipOrdersFragment extends BlankFragment implements OnRecyclerClick, FilterDeleverFragment.getShipFilterData {

    public static final String DELIVERY_STATUS = "delivery_status";
    private static final int NO_SELECT_ITEM = -1;
    @BindView(R.id.RightMenu_FrameLayout)
    FrameLayout RightMenuFrameLayout;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.Filter_ImageView)
    ImageView filterImageView;
    @BindView(R.id.Filter_TextView)
    TextView filterTextView;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.searchView_products)
    SearchView searchViewProducts;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    private FilterDeleverFragment filterFragment;
    private ReadyToShipOrdersAdapter adapter;
    private ShipViewModel shipViewModel;
    private List<ShipModel> shipViewModelList;
    int pageNumber = 0;
    int pageSize = 30;
    private boolean hasNext = false;
    private String status = "";
    private PosProfileWrapper posProfileWrapper;
    private ShipmentOrderFilter shipmentOrderFilter = new ShipmentOrderFilter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.delver_fragment, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            status = getArguments().getString(DELIVERY_STATUS);
        }
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        shipViewModelList = new ArrayList<>();
        posProfileWrapper = AppUtils.getProfile(getContext());
        int id = searchViewProducts.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        final TextView searchText = searchViewProducts.findViewById(id);
        Typeface typeface = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.order360_normal_font);
        searchText.setTypeface(typeface);
        searchText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        searchText.setTextSize(14);
        if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ENGLISH)) {
            searchText.setPadding(0, 5, 0, 0);
        } else {
            searchText.setPadding(0, 16, 16, 0);
        }

        searchViewProducts.setQueryHint(getString(R.string.hint_name)+","+getString(R.string.shipment_number));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        searchViewProducts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shipViewModelList.removeAll(shipViewModelList);
                adapter.notifyDataSetChanged();
                pageNumber = 0;
                getReadyToShipOrders(pageNumber, pageSize, shipmentOrderFilter, query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText != null) {
                    shipViewModelList.removeAll(shipViewModelList);
                    adapter.notifyDataSetChanged();
                    pageNumber = 0;
                    if (newText.equals("")) {
                        getReadyToShipOrders(pageNumber, pageSize, shipmentOrderFilter, newText);

                    }
                    return false;

                } else {
                    return true;
                }
            }
        });
        searchViewProducts.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                shipViewModelList.removeAll(shipViewModelList);
                adapter.notifyDataSetChanged();
                getReadyToShipOrders(pageNumber, pageSize, shipmentOrderFilter, "");
                return false;
            }
        });
    }

    private void createComponents() {
        Bundle bundle = new Bundle();
        bundle.putString(FilterDeleverFragment.SHIP_STATUS,status);
        filterFragment = new FilterDeleverFragment();
        filterFragment.setArguments(bundle);
        filterFragment.filterShipDataListener(this);
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawerLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);

        adapter = new ReadyToShipOrdersAdapter(getContext(), shipViewModelList, this, status);
        recyclerView.setAdapter(adapter);
        final EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) recyclerView.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (hasNext) {
                    pageNumber++;
                    getReadyToShipOrders(pageNumber, pageSize, shipmentOrderFilter, "");
                }

            }
        };
        recyclerView.addOnScrollListener(scrollListener);
        getReadyToShipOrders(pageNumber, pageSize, shipmentOrderFilter, "");

        if (drawerLayout != null) {
            replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            if (status.equals(AppConstants.READY_TO_SHIP_STATUS)) {
                AppUtils.changeTitle(getActivity(), getString(R.string.ship_orders));
            } else {
                AppUtils.changeTitle(getActivity(), getString(R.string.delivery_orders));
            }
        }


        //
        //  AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_order
        createComponents();
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick({R.id.Filter_ImageView, R.id.Filter_TextView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Filter_ImageView:
            case R.id.Filter_TextView:
                if (drawerLayout != null) {
                    replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
                    drawerLayout.openDrawer(Gravity.END);
                }
                break;
        }
    }

    @Override
    public void getPosition(int position) {
        Bundle bundle = new Bundle();
        if (status.equals(AppConstants.READY_TO_SHIP_STATUS)) {
            bundle.putLong(ReadyToShipFragment.ORDER_ID, shipViewModelList.get(position).getShipmentId());
            gotoFragment(new ReadyToShipFragment(), getActivity().getSupportFragmentManager(), bundle);
        } else {
            bundle.putLong(OrderDeliveryDetailsFragment.ORDER_ID, shipViewModelList.get(position).getShipmentId());
            bundle.putSerializable(OrderDeliveryDetailsFragment.ITEM_DATA, shipViewModelList.get(position));
            gotoFragment(new OrderDeliveryDetailsFragment(), getActivity().getSupportFragmentManager(), bundle);
        }
    }

    private void getReadyToShipOrders(int pageIndex, final int pageSize, ShipmentOrderFilter orderFilter, String search_term) {
        paginationLoadingView.setVisibility(View.VISIBLE);
        shipViewModel.getShipOrders(pageIndex, pageSize, posProfileWrapper.getUser().getId(), status, orderFilter, search_term).observe(this, new ApiObserver<List<ShipModel>>() {
            @Override
            protected void noConnection() {
                paginationLoadingView.setVisibility(View.GONE);

            }

            @Override
            protected void onSuccess(List<ShipModel> data) {
                paginationLoadingView.setVisibility(View.GONE);
                hasNext = data.size() >= pageSize;
                shipViewModelList.addAll(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            protected void onError(String error) {
                paginationLoadingView.setVisibility(View.GONE);
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void getFilterData(ShipmentOrderFilter orderFilter) {
        shipmentOrderFilter = orderFilter;
        shipViewModelList.removeAll(shipViewModelList);
        adapter.notifyDataSetChanged();
        drawerLayout.closeDrawers();
        getReadyToShipOrders(pageNumber, pageSize, shipmentOrderFilter, "");

    }
}
