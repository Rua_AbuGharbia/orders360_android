package com.telephoenic.orders360.controller.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.telephoenic.orders360.R;

import java.util.Objects;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.profile_activity_toolbar_text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
