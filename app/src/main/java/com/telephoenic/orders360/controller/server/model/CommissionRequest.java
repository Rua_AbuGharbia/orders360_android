package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 02-Nov-17.
 */

public class CommissionRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("posId")
    private String posId;

    @JsonProperty("prodId")
    private String prodId;
    private String groupId;

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
