package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemListDialogViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.NameItem_TextView)
    public TextView nameItemTextView;
    @BindView(R.id.Line_View)
    public View lineView;

    public ItemListDialogViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
