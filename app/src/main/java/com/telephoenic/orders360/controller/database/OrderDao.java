package com.telephoenic.orders360.controller.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.telephoenic.orders360.controller.server.model.OrderMap;

import java.util.List;

/**
 * Created by Rua on 27-Feb-18.
 */

@Dao
public interface OrderDao {

    @Query("SELECT * FROM orders")
    List<OrderMap> getAll();

   /* @Query("SELECT * FROM orders WHERE order_id =:order_id")
    OrderMap getOrderByID(int order_id);*/

   /* @Query("SELECT * FROM orders WHERE user_id =:user_id")
    List<OrderMap> findOrder(Long user_id);*/

    @Query("SELECT * FROM orders WHERE user_id =:user_id and vendor_id = :vendor_id")
    OrderMap findOrder(Long user_id, Long vendor_id);

    @Insert
    void insertAll(List<OrderMap> orders);

    @Insert
    void insertOrder(OrderMap order);

    @Update
    void updateOrder(OrderMap order);

    @Delete
    void deleteOrder(OrderMap order);

    @Query("DELETE FROM orders WHERE user_id =:user_id and vendor_id = :vendor_id")
    void deleteOrdersByPos(Long user_id, Long vendor_id);

}
