package com.telephoenic.orders360.controller.server.model;

import com.telephoenic.orders360.controller.fragments.MoreFragment;

public class MoreItemChild {
    private String title;
    private int icon;
    private @MoreFragment.childClick int childClick;

    public int getChildClick() {
        return childClick;
    }

    public void setChildClick(@MoreFragment.childClick int childClick) {
        this.childClick = childClick;
    }



    public MoreItemChild(String title, int icon,@MoreFragment.childClick int childClick) {
        this.title = title;
        this.icon = icon;
        this.childClick = childClick;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
