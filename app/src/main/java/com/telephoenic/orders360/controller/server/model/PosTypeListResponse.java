package com.telephoenic.orders360.controller.server.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PosTypeListResponse {

    @SerializedName("content")
    private List<PosTypeModel> posTypeModelsList;

    public List<PosTypeModel> getPosTypeModelsList() {
        return posTypeModelsList;
    }

    public void setPosTypeModelsList(List<PosTypeModel> posTypeModelsList) {
        this.posTypeModelsList = posTypeModelsList;
    }

    @Override
    public String toString() {
        return "PosTypeListResponse{" +
                "posTypeModelsList=" + posTypeModelsList +
                '}';
    }
}
