package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ComboItem extends OrderItems implements Serializable  {

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("deleted")
	private boolean deleted;

	@JsonProperty("inactive")
	private boolean inactive;

	@JsonProperty("combo")
	private ComboWrapper combo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public ComboWrapper getCombo() {
		return combo;
	}

	public void setCombo(ComboWrapper combo) {
		this.combo = combo;
	}
}