package com.telephoenic.orders360.controller.server.model.survey;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class SurveyAnswersRequest implements Serializable {

	@JsonProperty("surveyId")
	private Long surveyId;

	@JsonProperty("participantId")
	private Long participantId;

	@JsonProperty("questionsWithAnswers")
	private List<SurveyQuestionWrapper> questionsWithAnswers;

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	public Long getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}

	public List<SurveyQuestionWrapper> getQuestionsWithAnswers() {
		return questionsWithAnswers;
	}

	public void setQuestionsWithAnswers(List<SurveyQuestionWrapper> questionsWithAnswers) {
		this.questionsWithAnswers = questionsWithAnswers;
	}
}
