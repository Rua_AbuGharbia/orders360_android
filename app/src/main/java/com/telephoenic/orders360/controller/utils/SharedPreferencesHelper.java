package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHelper {


    public static void clear(Context context, String sharedPreferenceKey) {

        SharedPreferences prefs = context.getSharedPreferences(sharedPreferenceKey, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }

    public static String getStringByKey(Context context, String key, String sharedPreferenceKey) {

        return context.getSharedPreferences(sharedPreferenceKey, 0)
                .getString(key, null);
    }

    public static void setStringByKey(Context context, String key, String value, String sharedPreferenceKey) {

        context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                .putString(key, value).apply();
    }

    public static Integer getIntByKey(Context context, String key, String sharedPreferenceKey) {

        int value = context.getSharedPreferences(sharedPreferenceKey, 0)
                .getInt(key, -1);
        if (value == -1) {
            return null;
        }

        return value;
    }

    public static void setIntByKey(Context context, String key, Integer value, String sharedPreferenceKey) {

        if (value == null) {
            context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                    .putInt(key, -1).apply();
        } else {
            context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                    .putInt(key, value).apply();
        }

    }

    public static Float getFloatByKey(Context context, String key, String sharedPreferenceKey) {

        float value = context.getSharedPreferences(sharedPreferenceKey, 0)
                .getFloat(key, -1);
        if (value == -1) {
            return null;
        }

        return value;
    }

    public static void setFloatByKey(Context context, String key, Float value, String sharedPreferenceKey) {

        if (value == null) {
            context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                    .putFloat(key, -1).apply();
        } else {
            context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                    .putFloat(key, value).apply();
        }
    }

    public static boolean getBooleanByKey(Context context, String key, String sharedPreferenceKey) {

        return context.getSharedPreferences(sharedPreferenceKey, 0)
                .getBoolean(key, false);
    }

    public static boolean getBooleanByKeyDefaultTrue(Context context, String key, String sharedPreferenceKey) {

        return context.getSharedPreferences(sharedPreferenceKey, 0)
                .getBoolean(key, true);
    }

    public static void setBooleanByKey(Context context, String key, Boolean value, String sharedPreferenceKey) {

        if (context == null) {
            return;
        }
        if (value == null) {
            context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                    .putBoolean(key, false).apply();
        } else {
            context.getSharedPreferences(sharedPreferenceKey, 0).edit()
                    .putBoolean(key, value).apply();
        }
    }
}
