package com.telephoenic.orders360.controller.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.mvp.MVPView;

import org.springframework.util.LinkedMultiValueMap;

public class PhoneNumberActivity extends AppCompatActivity implements View.OnClickListener , MVPView {
    private String TAG = PhoneNumberActivity.class.getName();
    private Button verifyBtn;
    private EditText phoneNumber;

    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number);

        createComponents();
        getSupportActionBar().hide();

    }

    private void createComponents(){
        verifyBtn = findViewById(R.id.button_verify) ;
        phoneNumber = findViewById(R.id.edit_text_verify);
        verifyBtn.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.wait_msg));
        progressDialog.setCancelable(false);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_verify:
                if(isValidInputs()){
                    navigate("");
                 }
               break;

            case R.id.txt_forget_password :
                break;
        }

    }

    private boolean isValidInputs (){
        if (TextUtils.isEmpty(phoneNumber.getText().toString().trim())) {
            Snackbar.make(getCurrentFocus(), R.string.toast_required_fill_phone, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            phoneNumber.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void navigate(String response) {
        Bundle bundle = getIntent().getBundleExtra(IntentExtraNames.BUNDLE);
        int id = bundle.getInt(IntentExtraNames.ID);
        Bundle newbundle = new Bundle();
        newbundle.putInt(IntentExtraNames.ID,id);
        IntentFactory.startActivity(PhoneNumberActivity.this,PINCodeActivity.class,true,true,newbundle);

    }

    @Override
    public void navigate(LinkedMultiValueMap response) {

    }
}
