package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class VendorListViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.VendorSelect_ImageView)
    public ImageView vendorSelectImageView;
    @BindView(R.id.VendorLogo_RowVendor_CircleImageView)
    public CircleImageView vendorLogoRowVendorCircleImageView;
    @BindView(R.id.VendorName_RowVendor_TextView)
    public TextView vendorNameRowVendorTextView;
    @BindView(R.id.VendorDescrption_RowVendor_TextView)
    public TextView vendorDescrptionRowVendorTextView;

    public VendorListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
