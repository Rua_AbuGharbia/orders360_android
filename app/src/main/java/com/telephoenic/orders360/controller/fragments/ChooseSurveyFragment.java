package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IntDef;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.survey.SurveyRequest;
import com.telephoenic.orders360.controller.server.model.survey.SurveyResponse;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.EndlessRecyclerViewScrollListener;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.SurveyAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.SurveyViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;

public class ChooseSurveyFragment extends BlankFragment implements OnRecyclerClick , DialogUtils.OnDialogClickListener
        ,ImageController {

    private RecyclerView surveyListView;
    private SurveyAdapter surveyAdapter;
    private LinearLayout emptyLayout;
    private TextView emptyTextView;
    private List<SurveyResponse> surveyList = new ArrayList<>();
    private SurveyRequest surveyRequest;
    private Long vendorId;

    private boolean flag_loading = true;
    private int pageNumber = 0;
    private int pageSize = 10;
    private View view;

    private SurveyViewModel surveyViewModel ;
    private PosProfileWrapper posProfileWrapper ;

    public ChooseSurveyFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_choose_survey, container, false);
        pageNumber = 0;
        surveyAdapter = null;
        surveyList.clear();
        final Bundle bundle = getArguments();
        if (bundle != null) {
            vendorId = bundle.getLong(IntentExtraNames.ID);
        }
        createComponents(view);

        prepareRequest();
        sendRequest();


        return view;
    }

    private void createComponents(View v) {
        surveyViewModel = ViewModelProviders.of(this).get(SurveyViewModel.class);
        AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_survey));
        surveyListView = v.findViewById(R.id.survey_list);
        emptyLayout = v.findViewById(R.id.empty_layout);
        emptyTextView = v.findViewById(R.id.text_empty_message);
        emptyTextView.setText(R.string.no_surveys_available);

        posProfileWrapper = AppUtils.getProfile(getContext());

        final EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) surveyListView.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (!flag_loading && totalItemsCount >= pageSize) {
                    flag_loading = true;
                    prepareRequest();
                    sendRequest();
                }
            }
        };

        surveyListView.addOnScrollListener(scrollListener);
    }

    private void prepareRequest() {
        if (AppPrefs.getVendorID(getContext()) != -1) {
            vendorId = AppPrefs.getVendorID(getContext());
        }
        surveyRequest = new SurveyRequest();
        surveyRequest.setVendorId(vendorId);
        surveyRequest.setPageNumber(pageNumber + "");
        surveyRequest.setPageSize(pageSize + "");
    }

    private void sendRequest() {
       LoadingDialog.showDialog(getContext());
        surveyViewModel.getAllSurveys(String.valueOf(pageNumber),String.valueOf(pageSize),
                String.valueOf(vendorId),posProfileWrapper.getUser().getId().toString())
                .observe(this, new ApiObserver<ResponseBody>() {
                    @Override
                    protected void noConnection() {
                        LoadingDialog.dismiss();
                        noConnectionInternetDialog();
                    }

                    @Override
                    protected void onSuccess(ResponseBody data) {
                        LoadingDialog.dismiss();
                        try {
                            JSONObject jsonObj = new JSONObject(data.string());
                            final JSONArray jsonArray = jsonObj.getJSONArray("content");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                SurveyResponse survey = JsonUtils.toJavaObject(jsonArray.get(i).toString(), SurveyResponse.class);
                                surveyList.add(survey);
                            }
                            Log.e("FFf", jsonArray.length() + "");
                            if (jsonArray.length() > 0) {
                                pageNumber++;
                                flag_loading = false;
                                fullData();
                            }
                            if (surveyList.size() == 0) {
                                surveyListView.setVisibility(View.GONE);
                                emptyLayout.setVisibility(View.VISIBLE);
                            } else {
                                surveyListView.setVisibility(View.VISIBLE);
                                emptyLayout.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    protected void onError(String error) {
                        LoadingDialog.dismiss();
                    }
                });

    }

    private void fullData() {
        if (surveyAdapter == null) {
            surveyAdapter = new SurveyAdapter(getContext(), surveyList, String.valueOf(System.currentTimeMillis()), this);
            surveyListView.setAdapter(surveyAdapter);
        } else {
            surveyAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void getPosition(int position) {
        Bundle bundle = new Bundle();
        bundle.putLong(IntentExtraNames.ID, surveyList.get(position).getId());
        gotoFragment(new SurveyDetailsFragment(), getActivity().getSupportFragmentManager(), bundle);
    }

    @IntDef({surveyStatus.NEW, surveyStatus.SUBMITTED})
    public  @interface surveyStatus {
        int NEW = 1;
        int SUBMITTED = 2;
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            sendRequest();
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}