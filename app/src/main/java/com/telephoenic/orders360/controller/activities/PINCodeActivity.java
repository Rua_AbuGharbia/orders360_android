package com.telephoenic.orders360.controller.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PasswordChangeRequest;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.mvp.MVPView;
import com.telephoenic.orders360.mvp.Presenter;
import com.telephoenic.orders360.mvp.PresenterImpl;

import org.springframework.util.LinkedMultiValueMap;

import static com.telephoenic.orders360.controller.constants.AppConstants.MY_ACTIVITY;

public class PINCodeActivity extends AppCompatActivity implements MVPView{
    private String TAG = PINCodeActivity.class.getName();

    private PasswordChangeRequest passwordChangeRequest ;
    protected ProgressDialog progressDialog;
    Presenter changePasswordPresenter;

    EditText pinCodeEditText1, pinCodeEditText2, pinCodeEditText3, pinCodeEditText4, pinCodeEditText5, pinCodeEditText6 ;
    LinearLayout pinLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pincode);

        Bundle bundle = getIntent().getExtras();
       /* if(bundle!=null){
            passwordChangeRequest = (PasswordChangeRequest) bundle.getSerializable("passwordChangeRequest");
        }*/

            passwordChangeRequest = new PasswordChangeRequest();


        //AppUtils.initToolbar(getSupportActionBar(),this,getResources().getString(R.string.title_pin_code),false);
        getSupportActionBar().hide();
        AppUtils.getLayoutDirection(this);
        createComponents();
    }

    private void createComponents(){
        pinCodeEditText1 = findViewById(R.id.etxt_pin_code1);
        pinCodeEditText2 = findViewById(R.id.etxt_pin_code2);
        pinCodeEditText3 = findViewById(R.id.etxt_pin_code3);
        pinCodeEditText4 = findViewById(R.id.etxt_pin_code4);
        pinCodeEditText5 = findViewById(R.id.etxt_pin_code5);
        pinCodeEditText6 = findViewById(R.id.etxt_pin_code6);

        pinLayout = findViewById(R.id.linear_layout_pin_code_en);

        pinLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        progressDialog = new ProgressDialog(PINCodeActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.wait_msg));
        progressDialog.setCancelable(false);

        pinCodeEditText1.requestFocus();
        changePasswordPresenter = new PresenterImpl(this,this);


        pinCodeEditText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinCodeEditText1.getText().toString().length()== count && count > 0)     //size as per your requirement
                {
                    pinCodeEditText2.requestFocus();

                    if(isValidInputs()){
                        sendRequest();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pinCodeEditText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinCodeEditText2.getText().toString().length()==count && count > 0)     //size as per your requirement
                {
                    pinCodeEditText3.requestFocus();

                    if(isValidInputs()){
                        sendRequest();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pinCodeEditText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinCodeEditText3.getText().toString().length()==count && count > 0)     //size as per your requirement
                {
                    pinCodeEditText4.requestFocus();
                    if(isValidInputs()){
                        sendRequest();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pinCodeEditText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinCodeEditText4.getText().toString().length()==count && count > 0)     //size as per your requirement
                {
                    pinCodeEditText5.requestFocus();
                    if(isValidInputs()){
                        sendRequest();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pinCodeEditText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinCodeEditText5.getText().toString().length()==count && count > 0)     //size as per your requirement
                {
                    pinCodeEditText6.requestFocus();
                    if(isValidInputs()){
                        sendRequest();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pinCodeEditText6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinCodeEditText6.getText().toString().length()==count && count > 0)     //size as per your requirement
                {
                    if(isValidInputs()){
                        sendRequest();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void sendRequest(){
        AppUtils.hideKeyboard(pinCodeEditText1);
        AppUtils.hideKeyboard(pinCodeEditText2);
        AppUtils.hideKeyboard(pinCodeEditText3);
        AppUtils.hideKeyboard(pinCodeEditText4);
        AppUtils.hideKeyboard(pinCodeEditText5);
        AppUtils.hideKeyboard(pinCodeEditText6);

        String pinCode = "" ;
        pinCode  = pinCodeEditText1.getText().toString()+pinCodeEditText2.getText().toString()
                +pinCodeEditText3.getText().toString()+pinCodeEditText4.getText().toString()+
                pinCodeEditText5.getText().toString()+pinCodeEditText6.getText().toString();
        passwordChangeRequest.setPinCode(pinCode);

        Bundle bundle = getIntent().getBundleExtra(IntentExtraNames.BUNDLE);
        int id = bundle.getInt(IntentExtraNames.ID);
        Bundle newbundle = new Bundle();
        newbundle.putInt(IntentExtraNames.ID,id);
        IntentFactory.startActivity(PINCodeActivity.this,MainActivity.class,true ,true,newbundle);
       // IntentFactory.startActivity(PINCodeActivity.this,MainActivity.class,true);
        MY_ACTIVITY = "Home";
        //finish();

         /*   try {
            changePasswordPresenter.validateCredentials(passwordChangeRequest,PostChangePasswordWebService.class);
        }catch (Exception e){
            e.getMessage();
        }*/
    }

    public boolean isValidInputs() {
        if (TextUtils.isEmpty(pinCodeEditText1.getText().toString().trim())) {
            pinCodeEditText1.requestFocus();
            return false;
        } if (TextUtils.isEmpty(pinCodeEditText2.getText().toString().trim())) {
            pinCodeEditText2.requestFocus();
            return false;
        } if (TextUtils.isEmpty(pinCodeEditText3.getText().toString().trim())) {
            pinCodeEditText3.requestFocus();
            return false;
        } if (TextUtils.isEmpty(pinCodeEditText4.getText().toString().trim())) {
            pinCodeEditText4.requestFocus();
            return false;
        }if (TextUtils.isEmpty(pinCodeEditText5.getText().toString().trim())) {
            pinCodeEditText5.requestFocus();
            return false;
        }if (TextUtils.isEmpty(pinCodeEditText6.getText().toString().trim())) {
            pinCodeEditText6.requestFocus();
            return false;
        }

        return true;
    }
    private void reset (){
        pinCodeEditText1.setText("");
        pinCodeEditText2.setText("");
        pinCodeEditText3.setText("");
        pinCodeEditText4.setText("");
        pinCodeEditText5.setText("");
        pinCodeEditText6.setText("");
        pinCodeEditText1.requestFocus();
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.hide();

    }

    @Override
    public void navigate(String response) {

    }

    @Override
    public void navigate(LinkedMultiValueMap response) {

    }
}
