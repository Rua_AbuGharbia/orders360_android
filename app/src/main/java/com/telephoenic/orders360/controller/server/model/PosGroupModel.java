package com.telephoenic.orders360.controller.server.model;

public class PosGroupModel {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
