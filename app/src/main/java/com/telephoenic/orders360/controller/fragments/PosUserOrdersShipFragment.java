package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.EndlessRecyclerViewScrollListener;
import com.telephoenic.orders360.view.adapters.PosUserShipOrdersAdapter;
import com.telephoenic.orders360.viewmodel.OrderHistoryViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class PosUserOrdersShipFragment extends BlankFragment implements OnRecyclerClick {

    public static final String ORDER_ID = "order_id";
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    private PosUserShipOrdersAdapter adapter;
    private OrderHistoryViewModel orderHistoryViewModel;
    private List<ShipModel> shipViewModelList;
    int pageNumber = 0;
    int pageSize = 30;
    private boolean hasNext = false;
    private Long orderId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pos_ship_order_fragment, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            orderId = getArguments().getLong(ORDER_ID);
        }
        orderHistoryViewModel = ViewModelProviders.of(this).get(OrderHistoryViewModel.class);
        shipViewModelList = new ArrayList<>();

        return view;
    }


    private void createComponents() {
        adapter = new PosUserShipOrdersAdapter(getContext(), shipViewModelList, this);
        recyclerView.setAdapter(adapter);
        final EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) recyclerView.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (hasNext) {
                    pageNumber++;
                    getReadyToShipOrders(pageNumber, pageSize, orderId);
                }

            }
        };
        recyclerView.addOnScrollListener(scrollListener);
        getReadyToShipOrders(pageNumber, pageSize, orderId);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            AppUtils.changeTitle(getActivity(), getString(R.string.ship_orders));
        }
        createComponents();
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void getPosition(int position) {
        Bundle bundle = new Bundle();
        bundle.putLong(PosUserShipOrderDetailsFragment.ORDER_ID, shipViewModelList.get(position).getShipmentId());
        gotoFragment(new PosUserShipOrderDetailsFragment(), getActivity().getSupportFragmentManager(), bundle);

    }

    private void getReadyToShipOrders(int pageIndex, final int pageSize, Long orderId) {
        paginationLoadingView.setVisibility(View.VISIBLE);
        orderHistoryViewModel.getOrderShip(pageIndex, pageSize, orderId).observe(this, new ApiObserver<List<ShipModel>>() {
            @Override
            protected void noConnection() {
                paginationLoadingView.setVisibility(View.GONE);

            }

            @Override
            protected void onSuccess(List<ShipModel> data) {
                paginationLoadingView.setVisibility(View.GONE);
                hasNext = data.size() >= pageSize;
                shipViewModelList.addAll(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            protected void onError(String error) {
                paginationLoadingView.setVisibility(View.GONE);
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
