package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

	@JsonProperty("errorCode")
	private String errorCode;

	@JsonProperty("developerMessage")
	private String developerMessage;

	@JsonProperty("errorMsg1")
	private String errorMsg1;

	@JsonProperty("errorMsg2")
	private String errorMsg2;

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the developerMessage
	 */
	public String getDeveloperMessage() {
		return developerMessage;
	}

	/**
	 * @param developerMessage
	 *            the developerMessage to set
	 */
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	/**
	 * @return the errorMsg1
	 */
	public String getErrorMsg1() {
		return errorMsg1;
	}

	/**
	 * @param errorMsg1
	 *            the errorMsg1 to set
	 */
	public void setErrorMsg1(String errorMsg1) {
		this.errorMsg1 = errorMsg1;
	}

	/**
	 * @return the errorMsg2
	 */
	public String getErrorMsg2() {
		return errorMsg2;
	}

	/**
	 * @param errorMsg2
	 *            the errorMsg2 to set
	 */
	public void setErrorMsg2(String errorMsg2) {
		this.errorMsg2 = errorMsg2;
	}

}
