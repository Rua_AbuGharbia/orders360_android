package com.telephoenic.orders360.controller.server.model;

/**
 * Created by ruaabugharbia on 12-Sep-18.
 */

public class ImagesModel {

    private int imageID;

    private String imageName;

    public ImagesModel(int imageID, String imageName) {
        this.imageID = imageID;
        this.imageName = imageName;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
