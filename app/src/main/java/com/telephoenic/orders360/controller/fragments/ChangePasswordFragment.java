package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.UserPassword;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.NoInternetConnectionDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.UserViewModel;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {
    @BindInput(R.id.Current_Password_EditText)
    Orders360EditText currentPasswordEditText;
    @BindInput(R.id.New_Password_EditText)
    Orders360EditText newPasswordEditText;
    @BindInput(R.id.Confirm_Password_EditText)
    Orders360EditText confirmPasswordEditText;
    private UserViewModel userViewModel;

    public ChangePasswordFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.change_password_text));
        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password_fragment, container, false);
        ButterKnife.bind(this, view);
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this, view);
        if (getActivity() != null)
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    private void upDatePassword() {
        String currentPassword = currentPasswordEditText.getEditTextContent().trim();
        String newPassword = newPasswordEditText.getEditTextContent().trim();
        String confirmPassword = confirmPasswordEditText.getEditTextContent().trim();

        if (TextUtils.isEmpty(currentPassword)) {
            currentPasswordEditText.setErrorMessage(getResources().getString(R.string.please_enter_current_password));
            currentPasswordEditText.validate();
            currentPasswordEditText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(newPassword)) {
            newPasswordEditText.setErrorMessage(getResources().getString(R.string.please_enter_new_password));
            newPasswordEditText.validate();
            newPasswordEditText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            confirmPasswordEditText.setErrorMessage(getResources().getString(R.string.please_enter_confirm_new_password));
            confirmPasswordEditText.validate();
            confirmPasswordEditText.requestFocus();
            return;
        }

        if (!Objects.equals(newPassword, confirmPassword)) {
            confirmPasswordEditText.setErrorMessage(getResources().getString(R.string.passwords_did_not_match));
            confirmPasswordEditText.validate();
            confirmPasswordEditText.requestFocus();
            return;

        }

        PosProfileWrapper posProfile = AppUtils.getProfile(getContext());
        UserPassword userPassword = new UserPassword();
        userPassword.setForceFirstLogin(false);
        userPassword.setOldPassword(currentPassword);
        userPassword.setNewPassword(newPassword);
        userPassword.setPosId(posProfile.getUser().getId());

        LoadingDialog.showDialog(getContext());

        userViewModel.changePasswordInsideApp(userPassword).observe(this, new ApiObserver<Boolean>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(Boolean data) {
                LoadingDialog.dismiss();
                if (data != null) {
                    if (data) {
                        Toast.makeText(getContext(), R.string.password_changed_successfully, Toast.LENGTH_SHORT).show();
                        if (getActivity() != null)
                            getActivity().onBackPressed();
                    } else {
                        currentPasswordEditText.setErrorMessage(getResources().getString(R.string.current_password_did_not_match_existing_password));
                        currentPasswordEditText.validate();
                        currentPasswordEditText.requestFocus();
                        Toast.makeText(getContext(), getResources().getString(R.string.current_password_did_not_match_existing_password), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(),error,Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick({R.id.pos_details_cancel_button, R.id.profile_fragment_save_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.pos_details_cancel_button:
                if (getActivity() != null)
                    getActivity().onBackPressed();
                break;
            case R.id.profile_fragment_save_button:
                upDatePassword();
                break;
        }
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {

    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            upDatePassword();
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}
