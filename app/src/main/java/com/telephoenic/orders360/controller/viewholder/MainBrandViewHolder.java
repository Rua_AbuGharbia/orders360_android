package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainBrandViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Brand_ImageView)
    public ImageView brandImageView;
    @BindView(R.id.pagination_loading_view)
    public SpinKitView loadingView;


    public MainBrandViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
