package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SurveyViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.image_vendor)
    public ImageView imageVendor;
    @BindView(R.id.txt_title)
    public TextView txtTitle;
    @BindView(R.id.vendor_name)
    public TextView vendorName;
    @BindView(R.id.startDate_title)
    public TextView startDateTitle;
    @BindView(R.id.startDate)
    public TextView startDate;
    @BindView(R.id.endDate_title)
    public TextView endDateTitle;
    @BindView(R.id.endDate)
    public TextView endDate;
    @BindView(R.id.points)
    public TextView points;
    @BindView(R.id.submit)
    public TextView submitText;
    @BindView(R.id.image_arrow)
    public ImageView imageArrow;

    public SurveyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
