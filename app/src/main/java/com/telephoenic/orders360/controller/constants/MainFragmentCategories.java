package com.telephoenic.orders360.controller.constants;

public enum MainFragmentCategories {
	MAIN_FRAGMENT(0, "Main Fragment") , CATEGORIES(1, "categories"), SETTINGS(2, "settings"),
	PURCHASE(3, "Purchase"),  PURCHASE_HISTORY(4, "Purchase History"), PRIVACY_POLICY(5, "Privacy Policy"),SURVEY(6, "Survey"),
	CAMPAIGN(7, "Campaign"), PROFILE(8, "My Profile"), SALES_MAN(9, "Sales Man"),
	SUPPORT_TICKETS(10, "Support Tickets") , POINTS(11, "Points"), ORDER_NOW(12, "OrderDao Now"), SPECIAL_OFFERS(13, "Special Offers"),VENDORS(14, "Vendors") , ADDPOS(15 , "ADD POS"),
	SALESMAN(16, "Salesman"),MORE(17, "More"),PROMOTION(18,"Promotion"),BUNDLE(19,"Bundle"),
	SHIP_ORDERS(20,"Ship Orders"),DELIVERY_ORDERS(21,"Delivery Orders");
	
	private int catIndex;
	private String catName;

	MainFragmentCategories(int categoryIndex , String categoryName){
		catIndex = categoryIndex;
		catName = categoryName;
	}
	
	public int getIndex(){
		return catIndex;
	}
	
	public String getName(){
		return catName;
	}
	
	public static MainFragmentCategories getByIndex(int index){
		for(MainFragmentCategories category : values()){
			if(category.catIndex == index){
				return category;
			}
		}
		return null;
	}
	
}