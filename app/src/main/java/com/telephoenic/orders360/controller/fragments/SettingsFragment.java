package com.telephoenic.orders360.controller.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SettingsFragment extends BlankFragment {
    @BindInput(R.id.ChangeLanguage_editText)
    Orders360EditText changeLanguageEditText;
    private String TAG = SettingsFragment.class.getName();

    Button buttonSave;
    private ListItemDialog listItemDialog;
    private String[] arr2;
    private List<String> LanguageList;
    private int selectPosition;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this, view);
        return view;
    }

    private void createComponents(View v) {
        arr2 = getResources().getStringArray(R.array.language_array);
        LanguageList = new ArrayList<String>(Arrays.asList(arr2));
        if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ARABIC)) {
            selectPosition = 1;
            changeLanguageEditText.setText(LanguageList.get(1));
        } else {
            selectPosition = 0;
            changeLanguageEditText.setText(LanguageList.get(0));

        }
        changeLanguageEditText.getContentEditText().setFocusable(false);
        changeLanguageEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(ListItemDialog.DATA, (Serializable) LanguageList);
                bundle.putString(ListItemDialog.TITLE, getString(R.string.language_settings_text));
                listItemDialog = new ListItemDialog(languageClick);
                listItemDialog.setArguments(bundle);
                if (getFragmentManager() != null) {
                    listItemDialog.show(getFragmentManager(), "");
                }
            }
        });


        buttonSave = v.findViewById(R.id.button_save);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (selectPosition) {
                    case 0:
                        if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ARABIC)) {
                            LanguageManager.autoFlipLanguage(getActivity());
                        } else {
                            Objects.requireNonNull(getActivity()).onBackPressed();
                        }
                        break;
                    case 1:
                        if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ENGLISH)) {
                            LanguageManager.autoFlipLanguage(getActivity());
                        } else {
                            Objects.requireNonNull(getActivity()).onBackPressed();
                        }
                        break;
                }
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.language_settings_text));
        createComponents(view);
    }

    private OnRecyclerClick languageClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            selectPosition = position;
            switch (position) {
                case 0:
                    listItemDialog.dismiss();
                    changeLanguageEditText.setText(LanguageList.get(position));
                    break;
                case 1:
                    listItemDialog.dismiss();
                    changeLanguageEditText.setText(LanguageList.get(position));
                    break;
            }
        }
    };

}
