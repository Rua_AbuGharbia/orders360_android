package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;

public class OrderRejectedReason implements Serializable {
    private String name;
    private Long id;
    private Boolean select;

    public OrderRejectedReason(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    public OrderRejectedReason() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }
}
