package com.telephoenic.orders360.controller.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.controller.activities.PosLocationActivity;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.server.controller.GetWebserviceTask;
import com.telephoenic.orders360.controller.server.model.ErrorResponse;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.PosUser;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.controller.server.model.SearchRequest;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.webservices.GetSearchPOSWebService;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.SearchPOSAdapter;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;


public class SearchPOSFragment extends BlankFragment {
    private String TAG = SearchPOSFragment.class.getName();

    private SearchView nameSearchView;
    private ListView searchByNameList;
    private SearchRequest searchRequest;
    private int pageNumber = 0, pageSize = 10;
    private PosProfileWrapper posProfileWrapper ;
    private List<User> posUsersList ;
    private List<User> posUserSearchList ;
    private SearchPOSAdapter searchPOSAdapter ;
    boolean flag_loading = true;
    boolean flag_searching = false;

    public SearchPOSFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_pos, container, false);
    }

    private void createComponents(View v){
        nameSearchView = v.findViewById(R.id.searchView_pos);
        searchByNameList = v.findViewById(R.id.searchByNamelistView);
        posProfileWrapper = AppUtils.getProfile(getContext());
        posUsersList = new ArrayList<>();
        posUserSearchList = new ArrayList<>();

        nameSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageNumber = 0;
                flag_searching = true;
                posUserSearchList.clear();
            }
        });

        prepareRequest();
        try {
            getSearchPOSWebService();
        } catch (ExceptionHandling exceptionHandling) {
            exceptionHandling.printStackTrace();
        } finally {
        }

        nameSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                pageNumber = 0;
                flag_searching = true;
                posUserSearchList.clear();
                prepareRequest();
                try {
                    getSearchPOSWebService();
                } catch (ExceptionHandling exceptionHandling) {
                    exceptionHandling.printStackTrace();
                } finally {
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.equals("")){
                    posUserSearchList.clear();
                    posUsersList.clear();
                    flag_searching = false;
                    pageNumber = 0 ;
                }
                return false;
            }
        });

        nameSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                posUserSearchList.clear();
                posUsersList.clear();
                flag_searching = false;
                pageNumber = 0 ;
                prepareRequest();
                try {
                    getSearchPOSWebService();
                } catch (ExceptionHandling exceptionHandling) {
                    exceptionHandling.printStackTrace();
                } finally {
                }
                return false;
            }
        });


        searchByNameList.setOnScrollListener(new AbsListView.OnScrollListener() {
            int firstVisibleItem1 = 0, visibleItemCount1 = 0 ,totalItemCount1 =0 ;
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                final int lastItem = firstVisibleItem1 + visibleItemCount1;
                if (lastItem == totalItemCount1 && scrollState == SCROLL_STATE_IDLE) {
                    if(flag_loading == false)
                    {
                        flag_loading = true;
                        // productListView.addFooterView(footerProgressBar);
                        //allProducts=true;
                        prepareRequest();
                        try {
                            getSearchPOSWebService();
                        } catch (ExceptionHandling exceptionHandling) {
                            exceptionHandling.printStackTrace();
                        } finally {
                        }


                    }
                }
            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                firstVisibleItem1 = firstVisibleItem;
                visibleItemCount1 = visibleItemCount;
                totalItemCount1 = totalItemCount;

            }
        });

        //updated by diaa
        searchByNameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User posUser = null;
                if(flag_searching){
                    if(posUserSearchList != null){
                        posUser = posUserSearchList.get(i);
                    }
                } else {
                    if(posUsersList != null){
                        posUser =posUsersList.get(i);
                    }
                }

                if (posUser!=null){
                    Intent intent = new Intent(getContext(),PosLocationActivity.class);
                    intent.putExtra(IntentExtraNames.LATITUDE,posUser.getLatitude());
                    intent.putExtra(IntentExtraNames.LONGITUDE,posUser.getLongitude());
                    intent.putExtra(IntentExtraNames.NAME,posUser.getName());
                    intent.putExtra(IntentExtraNames.NUMBER,posUser.getMobileNumber());
                    intent.putExtra(IntentExtraNames.CITY_NAME,posUser.getRegion().getCity().getName());
                    intent.putExtra(IntentExtraNames.REGION_NAME,posUser.getRegion().getName());
                    startActivity(intent);
                }

            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void prepareRequest(){
        searchRequest = new SearchRequest();
        if(posProfileWrapper!=null){
            searchRequest.setSalesSupervisorId(posProfileWrapper.getUser().getId().toString());
        }
        searchRequest.setPageSize(pageSize);
        searchRequest.setPage(pageNumber);
        searchRequest.setSearch_term(nameSearchView.getQuery().toString().trim());
    }


    private void getSearchPOSWebService() throws ExceptionHandling {
        try{
            new GetWebserviceTask(getContext(), GetSearchPOSWebService.class) {
                @Override
                public void onResponseReady(Object response) {
                    super.onResponseReady(response);
                    flag_loading = false;
                    if (response != null) {
                        if (response instanceof String) {
                            try {
                                Log.e("Rua",response.toString());
                                List<User> users = GetSearchPOSWebService.POSParser.parse(response.toString());
                                if(users.size()>0){
                                    if(flag_searching){
                                        posUserSearchList.addAll(users);
                                    } else {
                                        posUsersList.addAll(users);
                                    }
                                    pageNumber +=pageSize ;
                                    fillPOS();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if(response instanceof ResponseWrapper){
                            ResponseWrapper responseWrapper = (ResponseWrapper) response;
                            ErrorResponse errorResponse = (ErrorResponse)responseWrapper.getResponse();
                            String errorCode = errorResponse.getErrorCode();
                            String unauthorizedCode = String.valueOf(HttpStatus.UNAUTHORIZED.value());
                            if(unauthorizedCode.equals(errorCode)){
                                AppPrefs.setPOS(getContext(),null);
                                IntentFactory.startActivity(getActivity(), LoginActivity.class,true);
                            }
                        }
                    }
                }
            }.execute(JsonUtils.toJsonString(searchRequest));
        }catch (Exception e){
            throw new ExceptionHandling(e.toString());}
    }


    private void fillPOS(){
        if(flag_searching){
            searchPOSAdapter = new SearchPOSAdapter(getContext() ,R.layout.item_search_pos, posUserSearchList);
        } else {
            searchPOSAdapter = new SearchPOSAdapter(getContext() ,R.layout.item_search_pos, posUsersList);
        }
        searchByNameList.setAdapter(searchPOSAdapter);
    }

}
