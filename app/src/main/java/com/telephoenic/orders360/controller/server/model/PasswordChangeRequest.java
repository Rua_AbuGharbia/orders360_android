package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PasswordChangeRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("posId")
    private Long posId; //Logged User ID

    //validate password
    @JsonProperty("oldPassword")
    private String oldPassword;

    //change password
    @JsonProperty("newPassword")
    private String newPassword;

    @JsonProperty("confirmNewPassword")
    private String confirmNewPassword;

    @JsonProperty("pinCode")
    private String pinCode;

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}