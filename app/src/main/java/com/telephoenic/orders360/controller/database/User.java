package com.telephoenic.orders360.controller.database;



public class User {

    int _id;
    String _name;
    String _email;
    String _mobileNumber;
    String _loggedUserLocation;
    String _password;
    String _passwordChanged;

    public User(){

    }

    public User(int id,String name, String email, String mobileNumber , String loggedUserLocation ,String password,String passwordChanged){
        this._id = id;
        this._name = name;
        this._email = email;
        this._mobileNumber= mobileNumber;
        this._loggedUserLocation = loggedUserLocation;
        this._passwordChanged = passwordChanged;
        this._password= password;
    }

    public User( String name, String email, String mobileNumber  , String loggedUserLocation , String password , String passwordChanged){
        this._name = name;
        this._email = email;
        this._mobileNumber= mobileNumber;
        this._loggedUserLocation = loggedUserLocation;
        this._passwordChanged = passwordChanged;
        this._password= password;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_mobileNumber() {
        return _mobileNumber;
    }

    public void set_mobileNumber(String _mobileNumber) {
        this._mobileNumber = _mobileNumber;
    }

    public String get_loggedUserLocation() {
        return _loggedUserLocation;
    }

    public void set_loggedUserLocation(String _loggedUserLocation) {
        this._loggedUserLocation = _loggedUserLocation;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public String get_passwordChanged() {
        return _passwordChanged;
    }

    public void set_passwordChanged(String _passwordChanged) {
        this._passwordChanged = _passwordChanged;
    }
}
