package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;

/**
 * Created by rua on 11-Apr-19.
 */

public class PromotionValueAttribute extends PromotionValue implements Serializable {

    private PromotionAttribute promotionAttribute;

    public PromotionAttribute getPromotionAttribute() {
        return promotionAttribute;
    }

    public void setPromotionAttribute(PromotionAttribute promotionAttribute) {
        this.promotionAttribute = promotionAttribute;
    }
}
