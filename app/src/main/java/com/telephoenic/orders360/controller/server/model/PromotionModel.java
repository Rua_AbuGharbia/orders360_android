package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;

/**
 * Created by rua on 25-Oct-18.
 */

public class PromotionModel implements Serializable {

    String promotionName ;
    boolean isSelected ;
    String count ;

    public PromotionModel(String promotionName, boolean isSelected) {
        this.promotionName = promotionName;
        this.isSelected = isSelected;
    }

    public PromotionModel(String promotionName, boolean isSelected,String count) {
        this.promotionName = promotionName;
        this.isSelected = isSelected;
        this.count = count;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
