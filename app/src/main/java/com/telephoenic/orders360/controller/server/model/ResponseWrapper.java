package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ResponseWrapper<T> {

	@JsonProperty("responseType")
	private String responseType;
	@JsonProperty("message")
	private String message;
	@JsonProperty("httpCode")
	private String httpCode;
	@JsonProperty("httpStatus")
	private HttpStatus httpStatus;
	@JsonProperty("response")
	private T response;
	@JsonProperty("responses")
	private List<T> responses;

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	public List<T> getResponses() {
		return responses;
	}

	public void setResponses(List<T> responses) {
		this.responses = responses;
	}
}