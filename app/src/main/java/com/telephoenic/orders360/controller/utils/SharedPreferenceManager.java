package com.telephoenic.orders360.controller.utils;

import android.content.Context;

public class SharedPreferenceManager {

    private static final String APP_PREFERENCES_KEY = "ORDER_PREFERENCES";
    private static final String COUNTRY_KEY = "COUNTRY_KEY";
    private static final String LANGUAGE_KEY = "LANGUAGE_KEY";

    public static String getCountry(Context context) {
        return SharedPreferencesHelper.getStringByKey(context, COUNTRY_KEY, APP_PREFERENCES_KEY);
    }

    public static void setCountry(Context context, String value) {
        SharedPreferencesHelper.setStringByKey(context, COUNTRY_KEY, value, APP_PREFERENCES_KEY);
    }

    public static void setLanguage(Context context, String value) {
        SharedPreferencesHelper.setStringByKey(context, LANGUAGE_KEY, value, APP_PREFERENCES_KEY);
    }

    public static String getLanguage(Context context) {
        return SharedPreferencesHelper.getStringByKey(context, LANGUAGE_KEY, APP_PREFERENCES_KEY);
    }

    public static void clearData(Context context) {
        SharedPreferencesHelper.clear(context, APP_PREFERENCES_KEY);
    }
}
