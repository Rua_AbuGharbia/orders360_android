package com.telephoenic.orders360.controller.fragments;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.WarehouseModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.DeliveryUserViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FilterDeleverFragment extends BlankFragment implements DatePickerDialog.OnDateSetListener {
    public static String SHIP_STATUS = "ship_status";

    @BindView(R.id.view5)
    View view5;
    @BindView(R.id.Done_Filter_TextView)
    TextView doneFilterTextView;
    @BindView(R.id.Reset_Filter_TextView)
    TextView resetFilterTextView;
    @BindView(R.id.Name_RowFilter_TextView)
    TextView nameRowFilterTextView;
    @BindView(R.id.NameRoot_RowFilter_TextView)
    TextView nameRootRowFilterTextView;
    @BindView(R.id.Image_Rotation_ImageView)
    ImageView imageRotationImageView;
    @BindView(R.id.ChangeBackGround_ConstraintLayout)
    ConstraintLayout changeBackGroundConstraintLayout;
    @BindView(R.id.category_loading_view)
    SpinKitView categoryLoadingView;
    @BindView(R.id.nav_category)
    ConstraintLayout navCategory;
    @BindView(R.id.ImageBrand_Rotation_ImageView)
    ImageView ImageBrandRotationImageView;
    @BindView(R.id.Brand_CardView)
    ConstraintLayout BrandCardView;
    @BindView(R.id.brand_loading_view)
    SpinKitView brandLoadingView;
    @BindView(R.id.nav_brands)
    ConstraintLayout navBrands;
    @BindView(R.id.ImageDiscount_Rotation_ImageView)
    ImageView ImageDiscountRotationImageView;
    @BindView(R.id.Discount_CardView)
    ConstraintLayout DiscountCardView;
    @BindInput(R.id.pos_details_selectcity_editText)
    Orders360EditText selectCityEditText;
    @BindInput(R.id.pos_details_selectregin_editText)
    Orders360EditText selectReginEditText;
    @BindView(R.id.discont_loading_view)
    SpinKitView discontLoadingView;
    @BindView(R.id.nav_discont)
    ConstraintLayout navDiscont;
    @BindView(R.id.ImageType_Rotation_ImageView)
    ImageView ImageTypeRotationImageView;
    @BindView(R.id.Type_CardView)
    ConstraintLayout TypeCardView;
    @BindView(R.id.checkBox)
    CheckBox checkBox;
    @BindView(R.id.nav_type)
    ConstraintLayout navType;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.ImageWarehouse_Rotation_ImageView)
    ImageView imageWarehouseRotationImageView;
    @BindView(R.id.Warehouse_CardView)
    ConstraintLayout warehouseCardView;
    @BindView(R.id.FromWarehouse)
    Orders360EditText fromWarehouse;
    @BindView(R.id.nav_Warehouse)
    ConstraintLayout navWarehouse;
    Unbinder unbinder;
    @BindView(R.id.FromOrderDate_Orders360EditText)
    Orders360EditText fromOrderDateOrders360EditText;
    @BindView(R.id.ToOrderDate_Orders360EditText)
    Orders360EditText toOrderDateOrders360EditText;
    @BindView(R.id.FromShipDate_Orders360EditText)
    Orders360EditText fromShipDateOrders360EditText;
    @BindView(R.id.ToShipDate_Orders360EditText)
    Orders360EditText toShipDateOrders360EditText;
    private ShipmentOrderFilter orderFilter;

    private OrdersView ordersView;
    private DatePickerDialog datePickerDialog;
    private getShipFilterData getShipFilterData;
    private DeliveryUserViewModel deliveryUserViewModel;
    private List<WarehouseModel> warehouseModelList = new ArrayList<>();
    private List<CityModel> cityModelList = new ArrayList<>();
    private PosProfileWrapper profileWrapper;
    private ListItemDialog listItemDialog;
    private List<RegionModel> regionModelList = new ArrayList<>();
    private String shipStatus="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fitler_delvery_fragment, container, false);
        if (getActivity() != null)
            ButterKnife.bind(this, view);
        ordersView = new OrdersView();
        ordersView.bind(this, view);
        TypeCardView.setVisibility(View.GONE);

        if (getArguments() !=null){
            shipStatus = getArguments().getString(SHIP_STATUS);
        }
        if (shipStatus.equals(AppConstants.SHIPPED_STATUS)) {
            warehouseCardView.setVisibility(View.GONE);
        }
        orderFilter = new ShipmentOrderFilter();
        deliveryUserViewModel = ViewModelProviders.of(this).get(DeliveryUserViewModel.class);
        profileWrapper = AppUtils.getProfile(getContext());
        getWarehouse(AppPrefs.getVendorID(getActivity()));
        getCity();
        createComponents();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && !isInternetConnection) {
            if (getView() != null)
                this.onViewCreated(getView(), new Bundle());
        }
    }

    private void createComponents() {
        fromOrderDateOrders360EditText.getContentEditText().setFocusable(false);
        fromOrderDateOrders360EditText.getContentEditText().setCursorVisible(false);
        toOrderDateOrders360EditText.getContentEditText().setFocusable(false);
        toOrderDateOrders360EditText.getContentEditText().setCursorVisible(false);
        fromShipDateOrders360EditText.getContentEditText().setFocusable(false);
        fromShipDateOrders360EditText.getContentEditText().setCursorVisible(false);
        toShipDateOrders360EditText.getContentEditText().setFocusable(false);
        toShipDateOrders360EditText.getContentEditText().setCursorVisible(false);
        selectCityEditText.getContentEditText().setFocusable(false);
        selectCityEditText.getContentEditText().setCursorVisible(false);
        selectReginEditText.getContentEditText().setFocusable(false);
        selectReginEditText.getContentEditText().setCursorVisible(false);
        fromWarehouse.getContentEditText().setFocusable(false);
        fromWarehouse.getContentEditText().setCursorVisible(false);

        fromWarehouse.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (warehouseModelList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) warehouseModelList);
                    bundle.putString(ListItemDialog.TITLE, "Warehouse");
                    listItemDialog = new ListItemDialog(warehouseClick);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }
            }
        });


        selectCityEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cityModelList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) cityModelList);
                    bundle.putString(ListItemDialog.TITLE, getString(R.string.city_text));
                    listItemDialog = new ListItemDialog(city);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }
            }
        });

        selectReginEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (regionModelList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) regionModelList);
                    bundle.putString(ListItemDialog.TITLE, getString(R.string.region_text));
                    listItemDialog = new ListItemDialog(regionClick);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }

            }
        });
        fromOrderDateOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(
                        Objects.requireNonNull(getActivity()), fromOrderDateListener,  Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                if (orderFilter.getOrderDateTo() != null) {

                    datePickerDialog.getDatePicker().setMaxDate(orderFilter.getOrderDateTo());

                }
                datePickerDialog.show();

            }
        });
        toOrderDateOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog = new DatePickerDialog(
                        Objects.requireNonNull(getActivity()), toOrderDateListener,  Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                if (orderFilter.getOrderDateFrom() != null) {

                    datePickerDialog.getDatePicker().setMinDate(orderFilter.getOrderDateFrom());
                }
                datePickerDialog.show();

            }
        });
        fromShipDateOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(
                        Objects.requireNonNull(getActivity()), fromShipDateListener,  Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                if (orderFilter.getShipmentDateTo() != null) {

                    datePickerDialog.getDatePicker().setMaxDate(orderFilter.getShipmentDateTo());
                }
                datePickerDialog.show();

            }
        });
        Date c = Calendar.getInstance().getTime();
        toShipDateOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(
                        Objects.requireNonNull(getActivity()), toShipDateListener, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                if (orderFilter.getShipmentDateFrom() != null) {

                    datePickerDialog.getDatePicker().setMinDate(orderFilter.getShipmentDateFrom());
                }

                datePickerDialog.show();

            }
        });
    }

    @OnClick({R.id.Brand_CardView, R.id.Discount_CardView, R.id.Type_CardView, R.id.ChangeBackGround_ConstraintLayout, R.id.Warehouse_CardView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Brand_CardView:
                if (navBrands.getVisibility() == View.VISIBLE) {
                    ImageBrandRotationImageView.setRotation(270);
                    navBrands.setVisibility(View.GONE);
                } else {
                    ImageBrandRotationImageView.setRotation(90);
                    navBrands.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.Discount_CardView:
                if (navDiscont.getVisibility() == View.VISIBLE) {
                    ImageDiscountRotationImageView.setRotation(270);
                    navDiscont.setVisibility(View.GONE);
                } else {
                    ImageDiscountRotationImageView.setRotation(90);
                    navDiscont.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.Type_CardView:
                if (navType.getVisibility() == View.VISIBLE) {
                    ImageTypeRotationImageView.setRotation(270);
                    navType.setVisibility(View.GONE);
                } else {
                    ImageTypeRotationImageView.setRotation(90);
                    navType.setVisibility(View.VISIBLE);
                    checkBox.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ChangeBackGround_ConstraintLayout:
                if (navCategory.getVisibility() == View.VISIBLE) {
                    imageRotationImageView.setRotation(270);
                    navCategory.setVisibility(View.GONE);
                } else {
                    imageRotationImageView.setRotation(90);
                    navCategory.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.Warehouse_CardView:
                if (navWarehouse.getVisibility() == View.VISIBLE) {
                    imageWarehouseRotationImageView.setRotation(270);
                    navWarehouse.setVisibility(View.GONE);
                } else {
                    imageWarehouseRotationImageView.setRotation(90);
                    navWarehouse.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    private DatePickerDialog.OnDateSetListener fromOrderDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            month = month + 1;
            fromOrderDateOrders360EditText.setText(year + "/" + month
                    + "/" + dayOfMonth);

            orderFilter.setOrderDateFrom(calendar.getTimeInMillis());
        }
    };
    private DatePickerDialog.OnDateSetListener toOrderDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            month = month + 1;
            toOrderDateOrders360EditText.setText(year + "/" + month
                    + "/" + dayOfMonth);
            orderFilter.setOrderDateTo(calendar.getTimeInMillis());

        }
    };

    private DatePickerDialog.OnDateSetListener fromShipDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            month = month + 1;
            fromShipDateOrders360EditText.setText(year + "/" + month
                    + "/" + dayOfMonth);
            orderFilter.setShipmentDateFrom(calendar.getTimeInMillis());


        }
    };

    private DatePickerDialog.OnDateSetListener toShipDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            month = month + 1;
            toShipDateOrders360EditText.setText(year + "/" + month
                    + "/" + dayOfMonth);
            orderFilter.setShipmentDateTo(calendar.getTimeInMillis());
        }
    };

    @OnClick({R.id.Done_Filter_TextView, R.id.Reset_Filter_TextView})
    public void onViewClicke2d(View view) {
        switch (view.getId()) {
            case R.id.Done_Filter_TextView:
                if (getShipFilterData != null)
                    getShipFilterData.getFilterData(orderFilter);
                break;
            case R.id.Reset_Filter_TextView:
                orderFilter = new ShipmentOrderFilter();
                resetData();
                if (getShipFilterData != null)
                    getShipFilterData.getFilterData(orderFilter);

                break;
        }
    }

    interface getShipFilterData {
        void getFilterData(ShipmentOrderFilter orderFilter);
    }

    public void filterShipDataListener(getShipFilterData getShipFilterData) {
        this.getShipFilterData = getShipFilterData;
    }

    private void getWarehouse(Long userId) {
        deliveryUserViewModel.getWarehouse(userId).observe(this, new ApiObserver<List<WarehouseModel>>() {
            @Override
            protected void noConnection() {

            }

            @Override
            protected void onSuccess(List<WarehouseModel> data) {
                warehouseModelList = data;

            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getCity() {
        deliveryUserViewModel.getCity().observe(this, new ApiObserver<List<CityModel>>() {
            @Override
            protected void noConnection() {

            }

            @Override
            protected void onSuccess(List<CityModel> data) {
                cityModelList = data;

            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private OnRecyclerClick city = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            selectCityEditText.setText(cityModelList.get(position).getName());
            selectReginEditText.setText("All Areas");
            orderFilter.setCityId(cityModelList.get(position).getId());
            regionModelList = new ArrayList<>();
            orderFilter.setRegionId(null);
            getRegion(cityModelList.get(position).getId());
        }
    };

    private OnRecyclerClick warehouseClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            fromWarehouse.setText(warehouseModelList.get(position).getName());
            orderFilter.setWarehouseId(warehouseModelList.get(position).getId());
        }
    };

    private void getRegion(Long cityId) {
        deliveryUserViewModel.getRegion(cityId).observe(this, new ApiObserver<List<RegionModel>>() {
            @Override
            protected void noConnection() {

            }

            @Override
            protected void onSuccess(List<RegionModel> data) {
                regionModelList = data;

            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private OnRecyclerClick regionClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            selectReginEditText.setText(regionModelList.get(position).getName());
            orderFilter.setRegionId(regionModelList.get(position).getId());
        }
    };
    private void resetData(){
        regionModelList = new ArrayList<>();
        fromShipDateOrders360EditText.getContentEditText().setText("");
        toShipDateOrders360EditText.getContentEditText().setText("");
        fromOrderDateOrders360EditText.getContentEditText().setText("");
        toOrderDateOrders360EditText.getContentEditText().setText("");
        selectCityEditText.getContentEditText().setText("");
        selectReginEditText.getContentEditText().setText("");
        fromWarehouse.getContentEditText().setText("");

    }
}
