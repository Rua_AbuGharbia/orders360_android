package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;
import java.util.Date;

public class ChequePaymentsModel implements Serializable {
    private PaymentMethodModel paymentMethod;
    private Double amount;
    private String number;
    private String payTo;
    private String bankName;
    private String issuerName;
    private Date dueDate2;
    private Long dueDate;

    public ChequePaymentsModel(PaymentMethodModel paymentMethod, Double amount, String number,
                               String payTo, String bankName, String issuerName, Date dueDate2,Long dueDate) {
        this.paymentMethod = paymentMethod;
        this.amount = amount;
        this.number = number;
        this.payTo = payTo;
        this.bankName = bankName;
        this.issuerName = issuerName;
        this.dueDate2 = dueDate2;
        this.dueDate =dueDate;
    }

    public ChequePaymentsModel() {
    }

    public PaymentMethodModel getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethodModel paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public Date getDueDate2() {
        return dueDate2;
    }

    public void setDueDate2(Date dueDate2) {
        this.dueDate2 = dueDate2;
    }

    public Long getDueDate() {
        return dueDate;
    }


    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }
}
