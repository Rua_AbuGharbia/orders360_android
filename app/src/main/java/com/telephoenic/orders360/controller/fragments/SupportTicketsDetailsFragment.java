package com.telephoenic.orders360.controller.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.TicketModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.DateUtil;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SupportTicketsDetailsFragment extends BlankFragment {
    public static String TICKET_MODEL = "ticket";
    @BindView(R.id.SubjectTicket_TextView)
    TextView subjectTicketTextView;
    @BindView(R.id.MessageTicket_TextView)
    TextView messageTicketTextView;
    @BindView(R.id.RepliedTicket_TextView)
    TextView repliedTicketTextView;
    @BindView(R.id.TypeTicket_TextView)
    TextView typeTicketTextView;
    @BindView(R.id.CreateDateTicket_TextView)
    TextView createDateTicketTextView;
    @BindView(R.id.ReplyDateTicket_TextView)
    TextView replyDateTicketTextView;
    private TicketModel ticketModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getActivity() !=null)
        AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_communication));
        View view = inflater.inflate(R.layout.fragment_support_tickets_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents();
    }

    private void createComponents() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            ticketModel = (TicketModel) bundle.getSerializable(TICKET_MODEL);
          //  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            String dateCreate = DateUtil.formatDate(DateUtil.parseDate(ticketModel.getAudit().getCreatedDate()));
            createDateTicketTextView.setText(dateCreate);
            subjectTicketTextView.setText(ticketModel.getSubject());
            messageTicketTextView.setText(ticketModel.getPmsg());
            if (ticketModel.getSupportTicketType()!=null)
            typeTicketTextView.setText(ticketModel.getSupportTicketType().getName().trim());
            if (ticketModel.getVmsg() == null || TextUtils.isEmpty(ticketModel.getVmsg())) {
                repliedTicketTextView.setText(R.string.not_replied);
                repliedTicketTextView.setTextColor(getContext().getResources().getColor(R.color.red_400));
            } else {
             //   SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
              //  String dateString = formatter.format(new Date(Long.valueOf(String.valueOf(ticketModel.getAudit().getUpdatedDate()))));
                String dateString = DateUtil.formatDate(DateUtil.parseDate(ticketModel.getAudit().getUpdatedDate()));
                replyDateTicketTextView.setText(dateString);
                repliedTicketTextView.setText(ticketModel.getVmsg());
            }
        }
    }
}
