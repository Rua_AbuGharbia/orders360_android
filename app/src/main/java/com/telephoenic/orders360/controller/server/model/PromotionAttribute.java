package com.telephoenic.orders360.controller.server.model;

public class PromotionAttribute extends PromotionValue {

	private String attribute;

	private Long value;

	private VendorWrapper vendor;

	public PromotionAttribute(Long value) {
		this.value = value;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public VendorWrapper getVendor() {
		return vendor;
	}

	public void setVendor(VendorWrapper vendor) {
		this.vendor = vendor;
	}

}
