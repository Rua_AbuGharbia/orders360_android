package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.EndlessRecyclerViewScrollListener;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.OrderAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.OrderViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;

public class PurchaseHistoryFragment extends BlankFragment implements DialogUtils.OnDialogClickListener , ImageController {

    private String TAG = PurchaseHistoryFragment.class.getName();
    private RecyclerView purchaseListView;
    private OrderAdapter orderAdapter;
    private List<OrderModel> purchaseList;

    private PosProfileWrapper posProfileWrapper;
    private boolean viewPOSName = false;
    private int pageNumber = 0;
    private int pageSize = 30;

    private String type;

    private boolean hasNext = true;
    private SpinKitView loadingIndicatorView;

    private LinearLayout emptyLayout;
    private TextView emptyText;

    private OrderViewModel orderViewModel;

    private String myService = "";

    public PurchaseHistoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_purchase_history, container, false);
    }

    private void createComponents(View v) {
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        loadingIndicatorView = v.findViewById(R.id.pagination_loading_view);
        purchaseListView = v.findViewById(R.id.purchase_list_history);
        emptyLayout = v.findViewById(R.id.empty_layout);
        emptyText = v.findViewById(R.id.text_empty_message);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        purchaseListView.setLayoutManager(layoutManager);
        purchaseListView.setHasFixedSize(true);
        purchaseListView.setItemViewCacheSize(20);
        purchaseListView.setDrawingCacheEnabled(true);
        purchaseListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        purchaseListView.setNestedScrollingEnabled(false);
        purchaseListView.getLayoutManager().setMeasurementCacheEnabled(false);
        purchaseListView.getLayoutManager().setItemPrefetchEnabled(true);
        purchaseList = new ArrayList<>();
        posProfileWrapper = AppUtils.getProfile(getContext());
        privilege();
        prepareAndSendRequest();

        emptyText.setText(R.string.no_orders_available);


        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                prepareAndSendRequest();

            }
        };
        purchaseListView.addOnScrollListener(scrollListener);

    }

    private void prepareAndSendRequest() {
        try {
            if (viewPOSName) {
                type = AppConstants.ROLE_ORDER_DETAIL_FOR_SALES;
                if (loadingIndicatorView.getVisibility() == View.GONE) {
                    loadingIndicatorView.setVisibility(View.VISIBLE);
                }
                sendSalesRequest();
            } else {
                type = AppConstants.ROLE_ORDER_DETAIL_FOR_POS;
                if (loadingIndicatorView.getVisibility() == View.GONE) {
                    loadingIndicatorView.setVisibility(View.VISIBLE);
                }
                sendPOSRequest();
            }

        } catch (ExceptionHandling exceptionHandling) {
            Log.e("Exception ", exceptionHandling.toString());
        }
    }

    private void privilege() {
        if (posProfileWrapper != null) {
            List<String> privilegeList = posProfileWrapper.getUser().getPermissions();
            for (String item : privilegeList) {
                if (AppConstants.ROLE_POS_ORDERS_VIEW.equals(item)) { // TODO set ROLE_ORDER_DETAIL_FOR_SALES
                    viewPOSName = true;
//                    posNameTextView.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void sendSalesRequest() throws ExceptionHandling {
        myService = AppConstants.SERVICE_SALES_ORDER;
        try {
            orderViewModel.getSalesOrder(pageNumber,pageSize,posProfileWrapper.getUser().getId()).observe(this, new ApiObserver<ResponseBody>() {
                @Override
                protected void noConnection() {
                    loadingIndicatorView.setVisibility(View.GONE);
                    noConnectionInternetDialog();
                }

                @Override
                protected void onSuccess(ResponseBody data) {
                    loadingIndicatorView.setVisibility(View.GONE);
                    if(data!=null){
                        try {
                            List<OrderModel> orderModels = AppUtils.listOrderModelParse(data.string());
                            if (orderModels.size() > 0) {
                                purchaseList.addAll(orderModels);
                                fillData();
                                pageNumber += pageSize;
                            }
                            if (purchaseList.size() == 0) {
                                emptyLayout.setVisibility(View.VISIBLE);
                                purchaseListView.setVisibility(View.GONE);
                            } else {
                                emptyLayout.setVisibility(View.GONE);
                                purchaseListView.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }  else {
                        if (purchaseList.size() == 0) {
                            emptyLayout.setVisibility(View.VISIBLE);
                            purchaseListView.setVisibility(View.GONE);
                        } else {
                            emptyLayout.setVisibility(View.GONE);
                            purchaseListView.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                protected void onError(String error) {
                    loadingIndicatorView.setVisibility(View.GONE);
                    Toast.makeText(getContext(),error,Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            loadingIndicatorView.setVisibility(View.GONE);
            if (purchaseList.size() == 0) {
                emptyLayout.setVisibility(View.VISIBLE);
                purchaseListView.setVisibility(View.GONE);
            } else {
                emptyLayout.setVisibility(View.GONE);
                purchaseListView.setVisibility(View.VISIBLE);
            }
            throw new ExceptionHandling(e.toString());
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void sendPOSRequest() throws ExceptionHandling {
        myService = AppConstants.SERVICE_POS_ORDER;
        try {
            orderViewModel.getPOSOrder(String.valueOf(pageNumber),String.valueOf(pageSize),
                    posProfileWrapper.getUser().getId().toString(),AppPrefs.getVendorID(getContext()).toString())
                    .observe(this, new ApiObserver<ResponseBody>() {
                        @Override
                        protected void noConnection() {
                            loadingIndicatorView.setVisibility(View.GONE);
                            noConnectionInternetDialog();
                        }

                        @Override
                        protected void onSuccess(ResponseBody data) {
                            loadingIndicatorView.setVisibility(View.GONE);
                            try {
                                JSONObject jsonObj = new JSONObject(data.string());
                                JSONArray jsonArray = jsonObj.getJSONArray("content");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    OrderModel orderModel = JsonUtils.toJavaObject(jsonArray.get(i).toString(), OrderModel.class);
                                    purchaseList.add(orderModel);
                                }

                                if (purchaseList.size() > 0) {
                                    fillData();
                                    pageNumber ++ ;
                                }
                                if(purchaseList.size()==0){
                                    emptyLayout.setVisibility(View.VISIBLE);
                                    purchaseListView.setVisibility(View.GONE);
                                } else {
                                    emptyLayout.setVisibility(View.GONE);
                                    purchaseListView.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        protected void onError(String error) {
                            loadingIndicatorView.setVisibility(View.GONE);
                            Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();
                        }
                    });
        } catch (Exception e) {
            loadingIndicatorView.setVisibility(View.GONE);
            if (purchaseList.size() == 0) {
                emptyLayout.setVisibility(View.VISIBLE);
                purchaseListView.setVisibility(View.GONE);
            } else {
                emptyLayout.setVisibility(View.GONE);
                purchaseListView.setVisibility(View.VISIBLE);
            }
            throw new ExceptionHandling(e.toString());
        }
    }

    private void fillData() {
        if (orderAdapter == null) {
            if (purchaseList == null) {
                return;
            }
            if (getActivity() != null) {
                orderAdapter = new OrderAdapter(getActivity(), R.layout.item_purchase, purchaseList, onClickListener, viewPOSName, String.valueOf(System.currentTimeMillis()));
                purchaseListView.setAdapter(orderAdapter);
            } else {
                Log.e("getActivity", "is null");
            }
        } else {
            orderAdapter.notifyDataSetChanged();
            if (loadingIndicatorView.getVisibility() == View.VISIBLE) {
                loadingIndicatorView.setVisibility(View.GONE);
            }
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int indexClick = (Integer) view.getTag();
            OrderModel model = purchaseList.get(indexClick);
            switch (view.getId()) {
                case R.id.layout_purchase:
                    Bundle bundle = new Bundle();
                    bundle.putInt(IntentExtraNames.ID, model.getId());
                    bundle.putSerializable(IntentExtraNames.OBJECT, model);
                    gotoFragment(new OrderItemFragment(), getActivity().getSupportFragmentManager(), bundle);
                    break;
            }
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pageNumber = 0;
        orderAdapter = null;
        AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_orders));
        createComponents(view);
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            try {
                if(AppConstants.SERVICE_POS_ORDER.equals(myService)){
                    sendPOSRequest();
                } else if(AppConstants.SERVICE_SALES_ORDER.equals(myService)){
                    sendSalesRequest();
                }
            } catch (ExceptionHandling exceptionHandling) {
                exceptionHandling.printStackTrace();
            }
        }
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);  //Or ACTION_WIRELESS_SETTINGS
    }

    @Override
    public void onCancelClicked() {

    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }
}