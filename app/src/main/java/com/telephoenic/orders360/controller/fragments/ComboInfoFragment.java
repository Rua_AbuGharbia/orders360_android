package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.basemodel.typedef.MaxQtyItem;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.controller.server.model.ComboInfoRequest;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.ComboInfoAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.CartViewModel;
import com.telephoenic.orders360.viewmodel.ComboInfoViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;

public class ComboInfoFragment extends BlankFragment implements DialogUtils.OnDialogClickListener{
    private String TAG = ComboInfoFragment.class.getName();
    private RecyclerView productListView;
    private List<ComboInfoWrapper> listOfInfoCombos;
    private ComboInfoAdapter comboInfoAdapter;
    private int indexClick;
    private int comboID;
    private ComboWrapper comboWrapper;
    private String type;

    protected ProgressDialog progressDialog;
    private ComboInfoRequest comboInfoRequest;

    private ImageView imageMinus, imagePlus;
    private EditText countEditText;
    private TextView totalPricaText;
    private Button addToCartButton;
    private PosProfileWrapper posProfileWrapper;
    private List<String> privilegeList;

    private boolean textChanged = false;
    private CartViewModel cartViewModel;

    private ConstraintLayout constraintLayout;
    private ComboInfoViewModel comboInfoViewModel;
    private String myService ="";

    public ComboInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_combo_info, container, false);
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.wait_msg));
        progressDialog.setCancelable(false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            comboWrapper = (ComboWrapper) bundle.getSerializable(IntentExtraNames.OBJECT);
            type = bundle.getString(IntentExtraNames.TYPE);
            comboID = comboWrapper.getId();
        }

        productListView = v.findViewById(R.id.product_listview);
        imageMinus = v.findViewById(R.id.image_minus);
        imagePlus = v.findViewById(R.id.image_plus);
        countEditText = v.findViewById(R.id.purchases_num);
        totalPricaText = v.findViewById(R.id.text_total_price);
        addToCartButton = v.findViewById(R.id.button_add_to_cart);
        constraintLayout = v.findViewById(R.id.constraintLayout);
        posProfileWrapper = AppUtils.getProfile(getContext());

        if (posProfileWrapper != null) {
            privilegeList = posProfileWrapper.getUser().getPermissions();
        }
        for (String item : privilegeList) {
            switch (item) {
                case AppConstants.ROLE_PRODUCT_ADD:
                case AppConstants.ROLE_PROMOTION_ADD:
                case AppConstants.ROLE_COMBO_ADD:
                    addToCartButton.setVisibility(View.VISIBLE);
                    constraintLayout.setVisibility(View.VISIBLE);
                    break;

                case AppConstants.ROLE_PRODUCT_VIEW:
                case AppConstants.ROLE_PROMOTION_VIEW:
                case AppConstants.ROLE_COMBO_VIEW:
                    addToCartButton.setVisibility(View.GONE);
                    constraintLayout.setVisibility(View.GONE);
                    break;
            }

        }

        listOfInfoCombos = new ArrayList<>();

        totalPricaText.setText(String.format("0.0 %s", AppConstants.CURRENCY));

        if (comboWrapper.getCount() != null) {
            Integer currentQty = Integer.parseInt(comboWrapper.getCount());
            countEditText.setText(String.valueOf(currentQty));
            double price = Double.parseDouble(comboWrapper.getCount()) * comboWrapper.getPrice();
          //  totalPricaText.setText(" " + String.format(Locale.ENGLISH, "%.2f", price) + " " + AppConstants.CURRENCY);
            totalPricaText.setText(String.format(" %s %s", AppUtils.amountFormat(price), AppConstants.CURRENCY));

        }


        countEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                textChanged = false;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!textChanged) {
                    if (s.toString().equals("")) {
                        comboWrapper.setCount("0");
                        textChanged = true;
                        // countEditText.setText("0");
                        totalPricaText.setText("0.0 " + AppConstants.CURRENCY);
                    } else {
                        comboWrapper.setCount(s + "");
                        textChanged = true;
//                        countEditText.setText(s+"");
                        double price = Double.parseDouble(comboWrapper.getCount()) * comboWrapper.getPrice();
                      //  totalPricaText.setText(" " + String.format(Locale.ENGLISH, "%.2f", price) + " " + AppConstants.CURRENCY);
                        totalPricaText.setText(String.format(" %s %s", AppUtils.amountFormat(price), AppConstants.CURRENCY));


                    }

                }

            }
        });


        imagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer currentQty;
                if (!TextUtils.isEmpty(countEditText.getText())) {
                    currentQty = Integer.parseInt(countEditText.getText().toString());

                } else {
                    currentQty = 0;
                }
                if (currentQty < MaxQtyItem.MAX_QTY) {
                    countEditText.setText(String.valueOf(++currentQty));
                    comboWrapper.setCount(currentQty.toString());
                    double price = Double.parseDouble(comboWrapper.getCount()) * comboWrapper.getPrice();
                   // totalPricaText.setText(" " + String.format(Locale.ENGLISH, "%.2f", price) + " " + AppConstants.CURRENCY);
                    totalPricaText.setText(String.format(" %s %s", AppUtils.amountFormat(price), AppConstants.CURRENCY));

                }


                // totalPricaText.setText(price+" "+AppConstants.CURRENCY);

            }
        });

        imageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(countEditText.getText())) {
                    Integer currentQty = Integer.parseInt(countEditText.getText().toString());
                    if (currentQty > 0) {
                        countEditText.setText(String.valueOf(--currentQty));
                        comboWrapper.setCount(currentQty.toString());
                        double price = Double.parseDouble(comboWrapper.getCount()) * comboWrapper.getPrice();
                      //  totalPricaText.setText(" " + String.format(Locale.ENGLISH, "%.2f", price) + " " + AppConstants.CURRENCY);
                        totalPricaText.setText(String.format(" %s %s", AppUtils.amountFormat(price), AppConstants.CURRENCY));

                    }
                }

            }
        });

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart(comboWrapper, countEditText);
            }
        });

    }

    private void addToCart(ComboWrapper comboWrapper,final TextView countText) {
        ComboWrapper newComboWrapper = new ComboWrapper();
        newComboWrapper.setId(comboWrapper.getId());
        newComboWrapper.setName(comboWrapper.getName());
        newComboWrapper.setPrice(comboWrapper.getPrice());
        newComboWrapper.setListOfComboInfo(comboWrapper.getListOfComboInfo());
        newComboWrapper.setStartDate(comboWrapper.getStartDate());
        newComboWrapper.setEndDate(comboWrapper.getEndDate());
        newComboWrapper.setCode(comboWrapper.getCode());
        newComboWrapper.setDeleted(comboWrapper.isDeleted());
        newComboWrapper.setInactive(comboWrapper.isInactive());
        newComboWrapper.setCount(comboWrapper.getCount());
        newComboWrapper.setLogoImageName(comboWrapper.getLogoImageName());
        if (newComboWrapper.getCount() == null || newComboWrapper.getCount().equals("0")) {
            AppUtils.showAlertToast(getContext().getString(R.string.fragment_purchase_warning_message2_combo), getContext());
            return;
        }


        ComboItem orderItems = new ComboItem();
        int quantity = Integer.parseInt(newComboWrapper.getCount());
        double productTotalAmount = newComboWrapper.getPrice() * quantity;
        // Long productTotalAmount = (new Double(d)).longValue();
        orderItems.setOrderedQuantity(newComboWrapper.getCount() + "");
        orderItems.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
        orderItems.setCombo(newComboWrapper);
        orderItems.setPrice(newComboWrapper.getPrice());
        orderItems.setExpiryDate(newComboWrapper.getExpiryDate());
        //orderItems.setVat(16.5);
        double vat = 0.0;
        List<ComboInfoWrapper> comboProducts = newComboWrapper.getListOfComboInfo();
        for (ComboInfoWrapper product : comboProducts) {
            if (product.getProduct().getVat() != null) {
                vat = vat + product.getProduct().getVat();
            }
        }
        orderItems.setVat(vat);
        List<AddToCartModel> saveCart = new ArrayList<>();
        if (type.equals(AppConstants.EDIT_ORDER)) {
            SaveCart.getInstance().changeProductOrderedQuantity(orderItems);
        } else {
            if (SaveCart.getInstance().getCompoCount(orderItems) <= MaxQtyItem.MAX_QTY) {
                SaveCart.getInstance().setBundleItem(orderItems);

            }
            else {
                Toast.makeText(getContext(), getContext().getString(R.string.you_have_exceeded_the_quantity_allowable), Toast.LENGTH_SHORT).show();
                return;
            }

        }

        VendorWrapper vendorWrapper = new VendorWrapper();
        vendorWrapper.setId(AppPrefs.getVendorID(getContext()));
        SaveCart.getInstance().getSelectVendorCart().setPosUser(posProfileWrapper.getUser());
        SaveCart.getInstance().getSelectVendorCart().setVendor(vendorWrapper);
        SaveCart.getInstance().prepearCartToSend(SaveCart.getInstance().getSelectVendorCart());
        saveCart.add(SaveCart.getInstance().getSelectVendorCart());
        final boolean[] save = new boolean[1];
        save[0] = true;
        myService = AppConstants.SERVICE_ADD_TO_CART;
        cartViewModel.addToCart(saveCart).observe(getActivity(), new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                save[0] = false;
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(ResponseBody data) {
                save[0] = true;
                AppUtils.showAlertToast(getContext().getString(R.string.fragment_product_add_to_cart_message_combo), getContext());
                if (AppConstants.NEW_ORDER.equals(type)) {
                    countText.setText("0");
                }
                AppUtils.updateNotifactionOrder(getActivity());
                getActivity().onBackPressed();
            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
                save[0] = false;
            }
        });
    }

    private void prepareRequest() {
        comboInfoRequest = new ComboInfoRequest();
        comboInfoRequest.setComboId(comboID + "");
    }

    @Override
    public void onResume() {
        super.onResume();
        prepareRequest();
        try {
            LoadingDialog.showDialog(getContext());
            getBundleInfo();
        } catch (Exception exceptionHandling) {
            exceptionHandling.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        comboInfoAdapter = null;
        listOfInfoCombos= new ArrayList<>();
    }

    private void getBundleInfo(){
        myService = AppConstants.SERVICE_GET_BUNDEL;
        comboInfoViewModel = new ComboInfoViewModel();
        comboInfoViewModel.getBundleInfo(comboInfoRequest.getComboId()).observe(this , new ApiObserver<List<ComboInfoWrapper>>(){
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<ComboInfoWrapper> data) {
                LoadingDialog.dismiss();
                if(data.size()>0){
                    listOfInfoCombos.addAll(data);
                }
                fillComboInfo();

            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();
               /* DialogWrapper dialogWrapper = new DialogWrapper();
                dialogWrapper.setContext(getContext());
                dialogWrapper.setTitle(getContext().getString(R.string.titles_dialog_general_error));
                dialogWrapper.setMessage(error);
                dialogWrapper.setIsFinished(false);
                AppUtils.showAlertDialog(dialogWrapper);*/
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        comboInfoViewModel = ViewModelProviders.of(this).get(ComboInfoViewModel.class);
        createComponents(view);
    }

    private void fillComboInfo() {
        if (comboInfoAdapter == null) {
            comboInfoAdapter = new ComboInfoAdapter(getContext(), R.layout.item_combo_info, listOfInfoCombos, String.valueOf(System.currentTimeMillis()));
            productListView.setAdapter(comboInfoAdapter);
        } else {
            comboInfoAdapter.notifyDataSetChanged();
        }
        comboWrapper.setListOfComboInfo(listOfInfoCombos);
    }

    @Override
    public void onOkClicked() {
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}

