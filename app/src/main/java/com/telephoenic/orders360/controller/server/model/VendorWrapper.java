package com.telephoenic.orders360.controller.server.model;


import android.support.annotation.CheckResult;

import java.io.Serializable;
import java.util.Arrays;


/**
 * Created by Rua on 11-Dec-17.
 */
public class VendorWrapper implements Serializable {

    private Long id;
    private Boolean deleted;
    private Boolean inactive;
    private String name;
    private String code;
    private String logoImageName;
    private Object[] listOfVendorAddress;
    private CurrencyModel currency;
    private PromotionType promotionType;
    private boolean isSelected;
    private CountryModel country;
    private Boolean hasPromotionOther;
    private Boolean pointForProductHasDiscount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLogoImageName() {
        return logoImageName;
    }

    public void setLogoImageName(String logoImageName) {
        this.logoImageName = logoImageName;
    }

    public Object[] getListOfVendorAddress() {
        return listOfVendorAddress;
    }

    public void setListOfVendorAddress(Object[] listOfVendorAddress) {
        this.listOfVendorAddress = listOfVendorAddress;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    @Override
    public String toString() {
        return "VendorWrapper{" +
                "id=" + id +
                ", deleted=" + deleted +
                ", inactive=" + inactive +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", logoImageName='" + logoImageName + '\'' +
                ", listOfVendorAddress=" + Arrays.toString(listOfVendorAddress) +
                ", currency=" + currency +
                '}';
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }

    public Boolean isHasPromotionOther() {
        return hasPromotionOther;
    }

    public void setHasPromotionOther(Boolean hasPromotionOther) {
        this.hasPromotionOther = hasPromotionOther;
    }

    public Boolean getPointForProductHasDiscount() {
        return pointForProductHasDiscount;
    }

    public void setPointForProductHasDiscount(Boolean pointForProductHasDiscount) {
        this.pointForProductHasDiscount = pointForProductHasDiscount;
    }
}
