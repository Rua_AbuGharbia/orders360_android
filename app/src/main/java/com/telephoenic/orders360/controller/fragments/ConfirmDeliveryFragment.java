package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.CashPayments;
import com.telephoenic.orders360.controller.server.model.shiporder.ChequePaymentsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ClaimPayments;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipmentArrival;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.view.adapters.OrderDeliveryDetailsAdapter;
import com.telephoenic.orders360.view.adapters.PaymentDetailsAdapter;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmDeliveryFragment extends BlankFragment implements OnRecyclerClick {
    public static String PAYMENT_DATA = "payment_data";
    public static String AMOUNT = "amount";
    public static String SHIPMENT_ARRIVAL = "shipmentArrival";
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.TotalAmount_TextView)
    TextView totalAmountTextView;
    @BindView(R.id.RecyclerView_Order)
    RecyclerView recyclerViewOrder;
    private PaymentDetailsAdapter paymentDetailsAdapter;
    private double amount;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    private ShipViewModel shipViewModel;
    private String TAG = OrderDeliveryDetailsFragment.class.getName();
    private List<ShipItemsModel> shipItemsModelList;
    private OrderDeliveryDetailsAdapter adapter;
    private List<Object> paymentData;
    private List<String> paymentDetsilsData;
    private ShipmentArrival shipmentArrival = new ShipmentArrival();
    private ArrivalSuccessfulModel arrivalSuccessfulModel = new ArrivalSuccessfulModel();
    private List<ChequePaymentsModel> chequePaymentsModelList = new ArrayList<>();
    private List<CashPayments> cashPaymentsList = new ArrayList<>();
    private List<ClaimPayments> claimPaymentsList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirm_delivery_fragment, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            amount = getArguments().getDouble(AMOUNT);
            paymentData = (List<Object>) getArguments().getSerializable(PAYMENT_DATA);
            shipmentArrival = (ShipmentArrival) getArguments().getSerializable(SHIPMENT_ARRIVAL);
        }
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {
        setPaymentData();
        paymentDetailsAdapter = new PaymentDetailsAdapter(getContext(), paymentDetsilsData);
        arrivalSuccessfulModel.setShipmentArrival(shipmentArrival);
        recyclerView.setAdapter(paymentDetailsAdapter);
        recyclerViewOrder.setNestedScrollingEnabled(false);
        recyclerView.setNestedScrollingEnabled(false);
        totalAmountTextView.setText("Invoice Amount : " + String.format(Locale.ENGLISH, "%.2f", amount) + " " + AppConstants.CURRENCY);
        shipItemsModelList = new ArrayList<>();
        adapter = new OrderDeliveryDetailsAdapter(getContext(), this, shipItemsModelList);
        recyclerViewOrder.setAdapter(adapter);
        getShipItem(shipmentArrival.getShipmentId());

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), "Confirm Delivery");
        createComponents(view);
    }


    @OnClick(R.id.Signature_Buttom)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(SignatureOrderFragment.ARRIVAL_DATA, arrivalSuccessfulModel);
        gotoFragment(new SignatureOrderFragment(), getFragmentManager(), bundle);
    }

    @Override
    public void getPosition(int position) {

    }

    private void setPaymentData() {
        paymentDetsilsData = new ArrayList<>();
        double cashAmount = 0;
        double claimAmount = 0;
        double chequeAmount = 0;
        for (int i = 0; i < paymentData.size(); i++) {
            if (paymentData.get(i) instanceof CashPayments) {
                CashPayments cashPayments = (CashPayments) paymentData.get(i);
                cashAmount = cashAmount + cashPayments.getAmount();
                cashPaymentsList.add(cashPayments);

            } else if (paymentData.get(i) instanceof ClaimPayments) {
                ClaimPayments claimPayments = (ClaimPayments) paymentData.get(i);
                claimAmount = claimAmount + claimPayments.getAmount();
                claimPaymentsList.add(claimPayments);

            } else {
                ChequePaymentsModel chequePaymentsModel = (ChequePaymentsModel) paymentData.get(i);
                chequeAmount = chequeAmount + chequePaymentsModel.getAmount();
                chequePaymentsModelList.add(chequePaymentsModel);
            }
        }
        if (cashAmount != 0) {
            arrivalSuccessfulModel.setCashPayments(cashPaymentsList);
            paymentDetsilsData.add("Cash Payment : " + cashAmount + " " + AppConstants.CURRENCY);
        }
        if (claimAmount != 0) {
            arrivalSuccessfulModel.setClaimPayments(claimPaymentsList);
            paymentDetsilsData.add("Claim Payment : " + claimAmount + " " + AppConstants.CURRENCY);
        }
        if (chequeAmount != 0) {
            arrivalSuccessfulModel.setChequePayments(chequePaymentsModelList);
            paymentDetsilsData.add("Cheque Payment : " + chequeAmount + " " + AppConstants.CURRENCY);
        }

    }

    private void getShipItem(Long id) {
        paginationLoadingView.setVisibility(View.VISIBLE);
        shipViewModel.getShipItemOrder(id).observe(this, new ApiObserver<List<ShipItemsModel>>() {
            @Override
            protected void noConnection() {
                paginationLoadingView.setVisibility(View.GONE);
                // noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<ShipItemsModel> data) {
                paginationLoadingView.setVisibility(View.GONE);
                shipItemsModelList.addAll(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            protected void onError(String error) {
                paginationLoadingView.setVisibility(View.GONE);
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        });
    }
}
