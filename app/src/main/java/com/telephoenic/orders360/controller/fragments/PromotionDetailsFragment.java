package com.telephoenic.orders360.controller.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PromotionItemModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.adapters.PromotionDetailsAdapter;
import com.telephoenic.orders360.view.adapters.PromotionItemAdapter;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PromotionDetailsFragment extends BlankFragment {

    @BindView(R.id.Product_Name_TextView)
    TextView productNameTextView;
    @BindView(R.id.Product_Image_ImageView)
    ImageView productImageImageView;
    @BindView(R.id.ProductPrice_TextView)
    TextView productPriceTextView;
    @BindView(R.id.EndDate_TextView)
    TextView endDateTextView;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.text_total_price_title)
    TextView textTotalPriceTitle;
    @BindView(R.id.text_total_price)
    TextView textTotalPrice;
    @BindView(R.id.purchases_num)
    EditText purchasesNum;
    @BindView(R.id.RecyclerViewTotal)
    RecyclerView recyclerViewTotal;
    ArrayList<PromotionItemModel> promotionItemModelArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.promotion_details_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppUtils.changeTitle(Objects.requireNonNull(getActivity()), getString(R.string.fragment_product_commission));
        createComponents();
    }

    private void createComponents() {
        recyclerView.setNestedScrollingEnabled(false);
        recyclerViewTotal.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(new PromotionDetailsAdapter(getContext()));
        setPromotionItemModelArrayList();
    }


    @OnClick({R.id.image_minus, R.id.image_plus, R.id.button_add_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_minus:
                break;
            case R.id.image_plus:
                break;
            case R.id.button_add_more:
                break;
        }
    }

    private void setPromotionItemModelArrayList() {
        promotionItemModelArrayList.add(new PromotionItemModel(getString(R.string.title_items), "10"));
        promotionItemModelArrayList.add(new PromotionItemModel(getString(R.string.title_free_items), "04"));
        promotionItemModelArrayList.add(new PromotionItemModel(getString(R.string.title_total_items), "14"));
        promotionItemModelArrayList.add(new PromotionItemModel(getString(R.string.fragment_purchase_history_title_total_amount), "62.2"));
        recyclerViewTotal.setAdapter(new PromotionItemAdapter(getContext(), promotionItemModelArrayList));

    }
}
