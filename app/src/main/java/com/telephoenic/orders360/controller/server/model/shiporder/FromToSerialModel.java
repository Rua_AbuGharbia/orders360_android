package com.telephoenic.orders360.controller.server.model.shiporder;

public class FromToSerialModel {
    private String fromSerial;
    private String toSerial;

    public FromToSerialModel(String fromSerial, String toSerial) {
        this.fromSerial = fromSerial;
        this.toSerial = toSerial;
    }

    public String getFromSerial() {
        return fromSerial;
    }

    public void setFromSerial(String fromSerial) {
        this.fromSerial = fromSerial;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }
}
