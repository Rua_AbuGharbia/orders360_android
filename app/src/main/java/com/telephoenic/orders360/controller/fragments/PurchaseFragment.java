package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.DetailsActivity;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.AppDatabase;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.SingleItem;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.model.DialogWrapper;
import com.telephoenic.orders360.view.adapters.CartAdapter;
import com.telephoenic.orders360.view.customcontrols.CommissionDialog;
import com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog;
import com.telephoenic.orders360.view.customcontrols.CustomAlertDialog;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.CartViewModel;
import com.telephoenic.orders360.viewmodel.OrderViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import okhttp3.ResponseBody;

import static com.telephoenic.orders360.controller.constants.MainFragmentCategories.ORDER_NOW;


public class PurchaseFragment extends BlankFragment implements View.OnClickListener, ConfirmOrderDialog.onClickStatus, CartAdapter.changeProductItem
        ,DialogUtils.OnDialogClickListener {
    private String TAG = PurchaseFragment.class.getName();
    private RecyclerView purchaseListView;
    private List<OrderModel> purchaseList;
    private LinearLayout addMoreLinearLayout;

    private CartAdapter cartAdapter;
    private CustomAlertDialog confirmDialog;

    private CardView layout;

    private static TextView sum;
    private TextView sumTitle, noItem;
    private Button byNow, addMore;
    private ImageView basketImage;
    private int indexClick;
    private static Double sumPrice = 0.0;
    private Integer totalPoints = 0;

    private OrderModel orderModel;
    private List<OrderModel> orderModelsRequest;
    private List<SingleItem> singleItemsList;
    private List<PromotionItem> promotionItemList;
    private List<ComboItem> comboItemsList;
    protected ProgressDialog progressDialog;
    private PosProfileWrapper posProfileWrapper;
    private Context context;
    private Animation hyperspaceJumpAnimation;
    private List<Object> allCartItem = new ArrayList<>();
    private ArrayList<AddToCartModel> cartModelArrayList = new ArrayList<>();
    private CustomAlertDialog confirmDeleteDialog;
    private CartViewModel cartViewModel;
    private boolean productChange = false;
    private OrderViewModel orderViewModel;

    public PurchaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_purchase, container, false);

    }

    private void createComponents(View v) {
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_cart));

        // cartButtonVisible =false;
        context = getActivity();

        posProfileWrapper = AppUtils.getProfile(getContext());
        purchaseListView = v.findViewById(R.id.purchase_list);
        sum = v.findViewById(R.id.text_price);
        sumTitle = v.findViewById(R.id.text_sum);
        basketImage = v.findViewById(R.id.image_basket);
        noItem = v.findViewById(R.id.text_no_items);
        byNow = v.findViewById(R.id.button_by_now);
        addMore = v.findViewById(R.id.button_add_more);
        layout = v.findViewById(R.id.layout);
        addMoreLinearLayout = v.findViewById(R.id.AddMore_LinearLayout);
        AppUtils.changeVendorLogo(getActivity());
        cartModelArrayList = new ArrayList<>();
        allCartItem = new ArrayList<>();

        cartModelArrayList.add(SaveCart.getInstance().getSelectVendorCart());
        if (cartModelArrayList.get(0).getSingleItems() != null) {
            allCartItem.addAll(cartModelArrayList.get(0).getSingleItems());
        }
        if (cartModelArrayList.get(0).getComboItems() != null) {
            allCartItem.addAll(cartModelArrayList.get(0).getComboItems());
        }

        if (cartModelArrayList.get(0).getPromotionItems() != null) {
            allCartItem.addAll(cartModelArrayList.get(0).getPromotionItems());
        }
       // AppDatabase.getOrderFromDataBase(getContext(), posProfileWrapper.getUser());
        byNow.setVisibility(allCartItem.size() != 0 ? View.VISIBLE : View.GONE);
        layout.setVisibility(allCartItem.size() != 0 ? View.VISIBLE : View.GONE);
        purchaseListView.setVisibility(allCartItem.size() != 0 ? View.VISIBLE : View.GONE);
        sum.setVisibility(allCartItem.size() != 0 ? View.VISIBLE : View.GONE);
        sumTitle.setVisibility(allCartItem.size() != 0 ? View.VISIBLE : View.GONE);
        basketImage.setVisibility(allCartItem.size() != 0 ? View.GONE : View.VISIBLE);
        noItem.setVisibility(allCartItem.size() != 0 ? View.GONE : View.VISIBLE);

        purchaseListView.setNestedScrollingEnabled(false);

        if (allCartItem.size() == 0) {
            addMore.setText(getContext().getResources().getString(R.string.fragment_purchase_add_item));
        }

        byNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean confirmFlag = true;
                if (allCartItem.size() == 0) {
                    AppUtils.showAlertToast(getString(R.string.fragment_purchase_warning_message), getActivity());
                } else {
                    if (confirmFlag) {
                        createConfirmDialog();
                    } else {
                        AppUtils.showAlertToast(getString(R.string.fragment_purchase_warning_message2), getActivity());
                    }
                }
            }
        });

        addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextActivity(3, R.string.button_label_order_now);

            }
        });

        purchaseList = new ArrayList<>();
        comboItemsList = new ArrayList<>();
        singleItemsList = new ArrayList<>();
        promotionItemList = new ArrayList<>();
        orderModelsRequest = new ArrayList<>();
        prepareList();
        setSum();
    }

    private void prepareList() {
        cartAdapter = new CartAdapter(getActivity(), R.layout.item_cart_product, allCartItem, onIconClicked, String.valueOf(System.currentTimeMillis()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        cartAdapter.setChangeProductItem(this);
        purchaseListView.setLayoutManager(mLayoutManager);
        purchaseListView.setAdapter(cartAdapter);
    }

    public void createConfirmDialog() {
        String msg = getString(R.string.titles_dialog_by_confirm_desc);
        ConfirmOrderDialog confirmOrderDialog = new ConfirmOrderDialog();
        confirmOrderDialog.onClickStatus(this);
        confirmOrderDialog.show(getFragmentManager(), "");
    }

    private View.OnClickListener onDeleteDialogClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_positive:
                    confirmDeleteDialog.dismiss();
                    cartAdapter.removeItem(indexClick);
                    cartAdapter.notifyDataSetChanged();
                    setSum();
                    AppDatabase.saveOrderToDatabase(getContext(), posProfileWrapper.getUser());
                    break;
                case R.id.btn_negative:
                    confirmDeleteDialog.dismiss();
                    break;
            }
        }
    };

    private View.OnClickListener onIconClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            indexClick = (Integer) v.getTag();
            int purchases = 0;
            Object object = allCartItem.get(indexClick);
            if (object instanceof ProductWrapper) {
                if (((ProductWrapper) object).getCount() != null) {
                    purchases = Integer.parseInt(((ProductWrapper) object).getCount());
                }
            }
            switch (v.getId()) {
                case R.id.image_close:
                    confirmDelete();
                    break;
                case R.id.image_plus:
                    purchases++;
                    if (object instanceof ProductWrapper) {
                        ((ProductWrapper) allCartItem.get(indexClick)).setCount(purchases + "");
                    } else if (object instanceof ComboWrapper) {
                        ((ComboWrapper) allCartItem.get(indexClick)).setCount(purchases + "");
                    }
                    cartAdapter.notifyDataSetChanged();
                    setSum();
                    AppDatabase.saveOrderToDatabase(getContext(), posProfileWrapper.getUser());
                    break;
                case R.id.image_minus:
                    if (purchases > 0) {
                        purchases--;
                        if (object instanceof ProductWrapper) {
                            ((ProductWrapper) allCartItem.get(indexClick)).setCount(purchases + "");
                        } else if (object instanceof ComboWrapper) {
                            ((ComboWrapper) allCartItem.get(indexClick)).setCount(purchases + "");
                        }
                        AppDatabase.saveOrderToDatabase(getContext(), posProfileWrapper.getUser());
                    }
                    cartAdapter.notifyDataSetChanged();
                    setSum();
                    break;
                case R.id.button_details:
                    ComboItem comboItem = ((ComboItem) allCartItem.get(indexClick));
                    ComboWrapper comboWrapper = comboItem.getCombo();
                    ComboWrapper newComboWrapper = new ComboWrapper();
                    newComboWrapper.setId(comboWrapper.getId());
                    newComboWrapper.setName(comboWrapper.getName());
                    newComboWrapper.setPrice(comboWrapper.getPrice());
                    newComboWrapper.setListOfComboInfo(comboWrapper.getListOfComboInfo());
                    newComboWrapper.setStartDate(comboWrapper.getStartDate());
                    newComboWrapper.setEndDate(comboWrapper.getEndDate());
                    newComboWrapper.setCode(comboWrapper.getCode());
                    newComboWrapper.setDeleted(comboWrapper.isDeleted());
                    newComboWrapper.setInactive(comboWrapper.isInactive());
                    newComboWrapper.setCount(comboItem.getOrderedQuantity());
                    newComboWrapper.setLogoImageName(comboWrapper.getLogoImageName());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(IntentExtraNames.OBJECT, newComboWrapper);
                    bundle.putString(IntentExtraNames.TYPE, AppConstants.EDIT_ORDER);
                    Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getContext().getResources().getString(R.string.combo_info_title));
                    gotoFragment(new ComboInfoFragment(), getActivity().getSupportFragmentManager(), bundle);
                    break;
                case R.id.button_commission:
                    PosProfileWrapper posProfile = AppUtils.getProfile(Order360Application.getInstance().getApplicationContext());
                    CommissionDialog commissionDialog = new CommissionDialog(getActivity(), posProfile.getUser().getId().toString(), (PromotionItem) allCartItem.get(indexClick), AppConstants.EDIT_ORDER);
                    gotoFragment(commissionDialog, getActivity().getSupportFragmentManager());
                    break;
            }
        }
    };

    @SuppressLint("SetTextI18n")
    public static void setSum() {
        sumPrice = 0.0;

        sumPrice = SaveCart.getInstance().getSelectVendorCart().getTotalAmount();

        sum.setText(" " + String.format(Locale.ENGLISH, "%.2f", sumPrice) + " " + AppConstants.CURRENCY);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_negative:
                confirmDialog.dismiss();
                cartAdapter.notifyDataSetChanged();
                break;
            case R.id.btn_positive:
                confirmDialog.dismiss();
                if (prepareRequest()) {
                    try {
                        makeOrderWebService();
                    } catch (ExceptionHandling e) {
                    }
                }
                break;
        }

    }

    private boolean prepareRequest() {
        comboItemsList.clear();
        singleItemsList.clear();
        promotionItemList.clear();
        orderModelsRequest.clear();
        orderModel = new OrderModel();
        orderModel.setPosUser(posProfileWrapper.getUser());
//        for (Object orderItem : allCartItem) {
//            if (orderItem instanceof SingleItem) {
//                SingleItem preperSingle = (SingleItem) orderItem;
//                ProductWrapper productWrapper = preperSingle.getProduct();
//                SingleItem singleItem = new SingleItem();
//                int quantity = Integer.parseInt(preperSingle.getOrderedQuantity());
//                if (quantity == 0) {
//                    AppUtils.showAlertToast(getContext().getString(R.string.fragment_purchase_warning_message2), getContext());
//                    return false;
//                }
//                if (productWrapper.getDiscount() == null) {
//                    productWrapper.setDiscount(0.0);
//                }
//                double productTotalAmount = (productWrapper.getPrice() - ((productWrapper.getPrice() * (productWrapper.getDiscount()) / 100))) * quantity;
//                singleItem.setOrderedQuantity(preperSingle.getOrderedQuantity() + "");
//                singleItem.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
//                singleItem.setDiscountedPrice(Double.valueOf(String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice() - ((productWrapper.getPrice() * (productWrapper.getDiscount()) / 100)))));
//
//                if (productWrapper.getDiscount() != null && productWrapper.getDiscount() > 0) {
//                    singleItem.setDiscountPercentage(productWrapper.getDiscount());
//                    singleItem.setHasDiscount(true);
//                    singleItem.setPrice(Double.valueOf(String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice())));
//                } else {
//                    singleItem.setDiscountPercentage(0.0);
//                    singleItem.setHasDiscount(false);
//                    singleItem.setPrice(Double.valueOf(String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice())));
//
//                }
//                singleItem.setExpiryDate(productWrapper.getExpiryDate());
//                singleItem.setVat(productWrapper.getVat());
//                singleItem.setProduct(productWrapper);
//
//                // TODO edit Promotion
//                if (AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(getContext()))) {
//                    List<PromotionValueQuantity> promotionList = singleItem.getProduct().getCommissionListQantity();
//                    if (promotionList != null && !promotionList.isEmpty()) {
//                        PromotionItem promotionItem = new PromotionItem();
//                        promotionItem.setPrice(((ProductWrapper) orderItem).getPrice());
//                        promotionItem.setExpiryDate(((ProductWrapper) orderItem).getExpiryDate());
//                        promotionItem.setVat(((ProductWrapper) orderItem).getVat());
//                        promotionItem.setProduct((ProductWrapper) orderItem);
//                        List<PromotionItemDetails> promotionItemDetailsList = new ArrayList<>();
//                        productTotalAmount = 0.0;
//                        Long freeItems = 0L;
//                        Long totalQuantity = 0L;
//                        int promotionID = 0;
//                        for (PromotionValueQuantity obj : promotionList) {
//                            Integer count = Integer.valueOf(obj.getCount());
//                            productTotalAmount += obj.getQuantity(); //* count ;
//                            PromotionItemDetails promotionItemDetails = new PromotionItemDetails();
//                            //promotionItemDetails.setId(obj.getId());
//                            if (obj.getPromoitonTransient() != null) {
//                                promotionID = obj.getPromoitonTransient().intValue();
//                            }
//                            promotionItem.setPromotion(new Promotion(promotionID));
//                            Long quantityInt = obj.getQuantity();
//                            promotionItemDetails.setQuantity(quantityInt.longValue());
//                            promotionItemDetails.setOrderQuantity(count.longValue());
//                            totalQuantity = totalQuantity + obj.getQuantity();
//                            Long integer = obj.getFreeItem();
//                            freeItems = freeItems + integer;
//                            promotionItemDetails.setFreeItem(Long.valueOf(integer.longValue()));
//                            promotionItemDetails.setPromotionType(obj.getPromotionType());
//                            promotionItemDetailsList.add(promotionItemDetails);
//                        }
//                        promotionItem.setPromotionItemDetailsList(promotionItemDetailsList);
//                        promotionItem.setOrderedQuantity(((ProductWrapper) orderItem).getCount() + "");
//                        productTotalAmount = productTotalAmount * ((ProductWrapper) orderItem).getPrice();
//                        promotionItem.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
//                        promotionItemList.add(promotionItem);
//                    } else {
//                        singleItemsList.add(singleItem);
//                    }
//
//                } else if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(getContext()))) {
//                    List<PromotionValueAttribute> promotionList = singleItem.getProduct().getCommissionListAttribute();
//                    if (promotionList != null && !promotionList.isEmpty()) {
//                        PromotionItem promotionItem = new PromotionItem();
//                        promotionItem.setPrice(((ProductWrapper) orderItem).getPrice());
//                        promotionItem.setExpiryDate(((ProductWrapper) orderItem).getExpiryDate());
//                        promotionItem.setVat(((ProductWrapper) orderItem).getVat());
//                        promotionItem.setProduct((ProductWrapper) orderItem);
//                        List<PromotionItemDetails> promotionItemDetailsList = new ArrayList<>();
//
//                        productTotalAmount = 0.0;
//                        Long freeItems = 0L;
//                        Long totalQuantity = 0L;
//                        int promotionID = 0;
//                        for (PromotionValueAttribute obj : promotionList) {
//                            Integer count = Integer.valueOf(obj.getCount());
//                            productTotalAmount += obj.getPromotionAttribute().getValue(); //* count ;
//                            PromotionItemDetails promotionItemDetails = new PromotionItemDetails();
//                            //promotionItemDetails.setId(obj.getId());
//                            if (obj.getPromoitonTransient() != null) {
//                                promotionID = obj.getPromoitonTransient().intValue();
//                            }
//                            promotionItem.setPromotion(new Promotion(promotionID));
//                            Long quantityInt = obj.getPromotionAttribute().getValue();
//                            promotionItemDetails.setQuantity(quantityInt.longValue());
//                            promotionItemDetails.setOrderQuantity(count.longValue());
//                            totalQuantity = totalQuantity + obj.getPromotionAttribute().getValue();
//                            Long integer = obj.getFreeItem();
//                            freeItems = freeItems + integer;
//                            promotionItemDetails.setFreeItem(Long.valueOf(integer.longValue()));
//                            promotionItemDetails.setPromotionType(obj.getPromotionType());
//                            promotionItemDetailsList.add(promotionItemDetails);
//                        }
//
//                        promotionItem.setPromotionItemDetailsList(promotionItemDetailsList);
//                        // promotionItem.setQuantity(Long.valueOf(totalQuantity.longValue()));
//                        //promotionItem.setFreeItems(Long.valueOf(freeItems.longValue()));
//                        promotionItem.setOrderedQuantity(((ProductWrapper) orderItem).getCount() + "");
//                        productTotalAmount = productTotalAmount * ((ProductWrapper) orderItem).getPrice();
//                        promotionItem.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
//                        promotionItemList.add(promotionItem);
//                    } else {
//                        singleItemsList.add(singleItem);
//                    }
//                }
//            } else if (orderItem instanceof ComboWrapper) {
//                ComboItem orderItems = new ComboItem();
//                int quantity = Integer.parseInt(((ComboWrapper) orderItem).getCount());
//                double productTotalAmount = ((ComboWrapper) orderItem).getPrice() * quantity;
//                // Long productTotalAmount = (new Double(d)).longValue();
//                orderItems.setOrderedQuantity(((ComboWrapper) orderItem).getCount() + "");
//                orderItems.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
//                orderItems.setCombo((ComboWrapper) orderItem);
//                orderItems.setPrice(((ComboWrapper) orderItem).getPrice());
//                orderItems.setExpiryDate(((ComboWrapper) orderItem).getExpiryDate());
//                //orderItems.setVat(16.5);
//                double vat = 0.0;
//                List<ComboInfoWrapper> comboProducts = ((ComboWrapper) orderItem).getListOfComboInfo();
//                for (ComboInfoWrapper product : comboProducts) {
//                    if (product.getProduct().getVat() != null) {
//                        vat = vat + product.getProduct().getVat();
//                    }
//                }
//                orderItems.setVat(vat);
//                comboItemsList.add(orderItems);
//            }
//        }
        orderModel.setTotalAmount((double) Math.round(sumPrice * 100) / 100);
        orderModel.setComboItems(SaveCart.getInstance().getSelectVendorCart().getComboItems());
        orderModel.setSingleItems(SaveCart.getInstance().getSelectVendorCart().getSingleItems());
        orderModel.setPromotionItems(SaveCart.getInstance().getSelectVendorCart().getPromotionItems());
        Random r = new Random();
        int orderNumber = r.nextInt(4000 - 1) + 1;
        orderModel.setOrderNumber(orderNumber + "");
        VendorWrapper vendor = new VendorWrapper();
        if (AppPrefs.getVendorID(getContext()) != -1) {
            vendor.setId(AppPrefs.getVendorID(getContext()));
        }
        orderModel.setVendor(vendor);
        orderModelsRequest.add(orderModel);

        return true;
    }

    private void makeOrderWebService() throws ExceptionHandling {

        LoadingDialog.showDialog(getContext());
        orderViewModel.saveOrder(orderModelsRequest).observe(this, new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(ResponseBody data) {
                LoadingDialog.dismiss();
                productChange = false;
                orderModelsRequest.clear();
                allCartItem.clear();
                SaveCart.getInstance().ClearCart();
                AppUtils.showAlertToast(Objects.requireNonNull(getActivity()).getString(R.string.fragment_purchase_order_success), getActivity());

                AppUtils.updateNotifactionOrder(getActivity());
                // Refresh Activity
                Bundle bundle = new Bundle();
                bundle.putLong(IntentExtraNames.ID, posProfileWrapper.getUser().getId());
                AppConstants.MY_ACTIVITY = "Login";
                IntentFactory.startActivity(getActivity(), MainActivity.class, true, false, bundle);
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                DialogWrapper dialogWrapper = new DialogWrapper();
                dialogWrapper.setContext(getActivity());
                dialogWrapper.setTitle(Objects.requireNonNull(getActivity()).getString(R.string.titles_dialog_general_error));
                dialogWrapper.setMessage(error);
                dialogWrapper.setIsFinished(false);
                AppUtils.showAlertDialog(dialogWrapper);
            }
        });
        /*
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    PostExample postExample = new PostExample();
                    String json = JsonUtils.toJsonString(orderModelsRequest);
                    Log.e("Req", json);
                    final Object response = postExample.post(AppConstants.URL_LIVE + "order/save_all", json);
                    if (response != null) {
                        if (response instanceof String) {
                            try {
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        LoadingDialog.dismiss();
                                        // Your UI updates here
                                        productChange = false;
                                        orderModelsRequest.clear();
                                        allCartItem.clear();
                                        SaveCart.getInstance().ClearCart();
                                        AppUtils.showAlertToast(Objects.requireNonNull(getActivity()).getString(R.string.fragment_purchase_order_success), getActivity());

                                        AppUtils.updateNotifactionOrder(getActivity());
                                        // Refresh Activity
                                        Bundle bundle = new Bundle();
                                        bundle.putLong(IntentExtraNames.ID, posProfileWrapper.getUser().getId());
                                        AppConstants.MY_ACTIVITY = "Login";
                                        IntentFactory.startActivity(getActivity(), MainActivity.class, true, false, bundle);

                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (response instanceof ResponseWrapper) {
                            final ResponseWrapper responseWrapper = (ResponseWrapper) response;
                            String errorCode = responseWrapper.getHttpCode();
                            String unauthorizedCode = String.valueOf(HttpStatus.UNAUTHORIZED.value());
                            if (unauthorizedCode.equals(errorCode)) {
                                LoadingDialog.dismiss();
                                AppPrefs.setPOS(getContext(), null);
                                IntentFactory.logOut(getActivity());
                            } else {
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        LoadingDialog.dismiss();
                                        // Your UI updates here
                                        DialogWrapper dialogWrapper = new DialogWrapper();
                                        dialogWrapper.setContext(getActivity());
                                        dialogWrapper.setTitle(Objects.requireNonNull(getActivity()).getString(R.string.titles_dialog_general_error));
                                        dialogWrapper.setMessage(responseWrapper.getMessage());
                                        dialogWrapper.setIsFinished(false);
                                        AppUtils.showAlertDialog(dialogWrapper);
                                    }
                                });
                            }
                        }
                    } else {
                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                LoadingDialog.dismiss();
                                // Your UI updates here
                                DialogWrapper dialogWrapper = new DialogWrapper();
                                dialogWrapper.setContext(getActivity());
                                dialogWrapper.setTitle(Objects.requireNonNull(getActivity()).getString(R.string.titles_dialog_general_error));
                                dialogWrapper.setMessage(getActivity().getString(R.string.unknown_error));
                                dialogWrapper.setIsFinished(false);
                                AppUtils.showAlertDialog(dialogWrapper);
                            }
                        });

                    }
                } catch (final Exception e) {
                    LoadingDialog.dismiss();
                    Handler mHandler = new Handler(Looper.getMainLooper());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // Your UI updates here
                            // Log.e("Fragment",e.getMessage());
                            e.printStackTrace();
                        }
                    });
                }
            }
        });

        thread.start();*/
    }

    private void confirmDelete() {
        Bundle bundle = new Bundle();
        String msg = getString(R.string.titles_dialog_delete_product);
//        confirmDeleteDialog = new CustomAlertDialog(getActivity(), R.string.btn_confirm, msg, R.string.btn_cancel, R.string.ok, onDeleteDialogClicked);
//        confirmDeleteDialog.show();
        ConfirmOrderDialog confirmOrderDialog = new ConfirmOrderDialog();
        bundle.putString(ConfirmOrderDialog.MASSAGE, msg);
        confirmOrderDialog.setArguments(bundle);
        confirmOrderDialog.onClickStatus(clickStatus);
        confirmOrderDialog.show(getFragmentManager(), "");
    }

    private void nextActivity(int numberOfTabs, int title) {
        Bundle bundle = new Bundle();
        bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, numberOfTabs);
        bundle.putInt(IntentExtraNames.INDEX, ORDER_NOW.getIndex());
        bundle.putString(IntentExtraNames.TITLE, getContext().getResources().getString(title));
        IntentFactory.startActivity(getActivity(), DetailsActivity.class, false, true, bundle);
    }

    @Override
    public void getStatusClick(@ConfirmOrderDialog.StatusClick int Status) {
        switch (Status) {
            case ConfirmOrderDialog.StatusClick.CANCEL:
                cartAdapter.notifyDataSetChanged();
                break;
            case ConfirmOrderDialog.StatusClick.OKAY:
                if (prepareRequest()) {
                    try {
                        makeOrderWebService();
                    } catch (ExceptionHandling e) {

                    }
                }
                break;
        }

    }

    private ConfirmOrderDialog.onClickStatus clickStatus = new ConfirmOrderDialog.onClickStatus() {
        @Override
        public void getStatusClick(int Status) {
            switch (Status) {
                case ConfirmOrderDialog.StatusClick.CANCEL:
                    break;
                case ConfirmOrderDialog.StatusClick.OKAY:
                    if (allCartItem.get(indexClick) instanceof SingleItem) {
                        SingleItem singleItem = (SingleItem) allCartItem.get(indexClick);
                        SaveCart.getInstance().DeleteItemCart(singleItem);
                    } else if (allCartItem.get(indexClick) instanceof ComboItem) {
                        ComboItem comboItem = (ComboItem) allCartItem.get(indexClick);
                        SaveCart.getInstance().DeleteItemCart(comboItem);
                    } else if (allCartItem.get(indexClick) instanceof PromotionItem) {
                        PromotionItem promotionItem = (PromotionItem) allCartItem.get(indexClick);
                        SaveCart.getInstance().DeleteItemCart(promotionItem);
                    }
                    pushCartToServer();
                    cartAdapter.removeItem(indexClick);
                    setSum();
                    AppUtils.updateNotifactionOrder(getActivity());
                    basketImage.setVisibility(allCartItem.size() != 0 ? View.GONE : View.VISIBLE);
                    noItem.setVisibility(allCartItem.size() != 0 ? View.GONE : View.VISIBLE);
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.onViewCreated(Objects.requireNonNull(getView()), new Bundle());
    }

    @Override
    public void change(boolean change) {
        productChange = change;

    }

    @Override
    public void onPause() {
        super.onPause();
        if (productChange) {
            pushCartToServer();
            AppUtils.change = true;
        }
    }


    private void pushCartToServer() {
        List<AddToCartModel> saveCart = new ArrayList<>();
        VendorWrapper vendorWrapper = new VendorWrapper();
        vendorWrapper.setId(AppPrefs.getVendorID(getContext()));
        SaveCart.getInstance().getSelectVendorCart().setPosUser(posProfileWrapper.getUser());
        SaveCart.getInstance().getSelectVendorCart().setVendor(vendorWrapper);
        SaveCart.getInstance().prepearCartToSend(SaveCart.getInstance().getSelectVendorCart());
        saveCart.add(SaveCart.getInstance().getSelectVendorCart());

        cartViewModel.addToCart(saveCart).observe(getActivity(), new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(ResponseBody data) {

            }

            @Override
            protected void onError(String error) {
                DialogWrapper dialogWrapper = new DialogWrapper();
                dialogWrapper.setContext(getContext());
                dialogWrapper.setTitle(getContext().getString(R.string.titles_dialog_general_error));
                dialogWrapper.setMessage(error);
                dialogWrapper.setIsFinished(false);
                AppUtils.showAlertDialog(dialogWrapper);
            }
        });
    }


    @Override
    public void onOkClicked() {
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

    }

    @Override
    public void onCancelClicked() {

    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }
}


//    private void editOrder (OrderModel orderModel){
//        String type = "" ;
//        if(orderModel.getSingleItems()!=null && orderModel.getSingleItems().size()>0){
//            type = AppConstants.VENDOR_FRAGMENT;
//        } else if(orderModel.getComboItems()!=null && orderModel.getComboItems().size()>0){
//            type = AppConstants.COMBO_OFFER;
//        }
//        Bundle bundle = new Bundle();
//        bundle.putInt(IntentExtraNames.ID,orderModel.getId());
//        bundle.putString(IntentExtraNames.DATABASE_TYPE,AppConstants.EDIT_ORDER);
//        bundle.putString(IntentExtraNames.TYPE,type);
//        bundle.putSerializable(IntentExtraNames.OBJECT,orderModel.getVendor());
//        gotoFragment(new OrderDetailsFragment(), Objects.requireNonNull(getActivity()).getSupportFragmentManager(),bundle);
//    }

