package com.telephoenic.orders360.controller.utils;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

public class PagerTransUtils implements ViewPager.PageTransformer {
    private float MAX_SCALE = 0.0f;

    @Override
    public void transformPage(@NonNull View page, float position) {
        page.setPivotX(page.getWidth() / 2);
        page.setPivotY(150);
        if (MAX_SCALE == 0.0f && position > 0.0f && position < 1.0f) {
            MAX_SCALE = position;
        }
        position = position - MAX_SCALE;
        float absolutePosition = Math.abs(position);
        if (position <= -1.0f || position >= 1.0f) {

        } else if (position == 0.0f) {
            page.setScaleX((1 + MAX_SCALE));
            page.setScaleY((1 + MAX_SCALE));
        } else {
          page.setScaleX(1 + MAX_SCALE * (1 - absolutePosition));
            page.setScaleY(1 + MAX_SCALE * (1 - absolutePosition));
        }
    }
}