package com.telephoenic.orders360.controller.server.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class CategoryNodeWrapper implements Serializable, Parcelable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private boolean deleted;
    private boolean inactive;
    private String name;
    private String description;
    private CategoryNodeWrapper parent;
    private String path;
    private boolean hasChilds;
    private List<CategoryNodeWrapper> nodes;
    private Boolean select ;
    private int nodeLevel;
    private Boolean showLevel;
    private int rootPadding;



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryNodeWrapper getParent() {
        return parent;
    }

    public void setParent(CategoryNodeWrapper parent) {
        this.parent = parent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isHasChild() {
        return hasChilds;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChilds = hasChild;
    }

    public List<CategoryNodeWrapper> getNodes() {
        return nodes;
    }

    public void setNodes(List<CategoryNodeWrapper> nodes) {
        this.nodes = nodes;
    }

    public int getNodeLevel() {
        return nodeLevel;
    }

    public void setNodeLevel(int nodeLevel) {
        this.nodeLevel = nodeLevel;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }

    public Boolean getShowLevel() {
        return showLevel;
    }

    public void setShowLevel(Boolean showLevel) {
        this.showLevel = showLevel;
    }

    public int getRootPadding() {
        return rootPadding;
    }

    public void setRootPadding(int rootPadding) {
        this.rootPadding = rootPadding;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    public static final Creator<CategoryNodeWrapper> CREATOR = new Creator<CategoryNodeWrapper>() {
        @Override
        public CategoryNodeWrapper createFromParcel(Parcel source) {
            return new CategoryNodeWrapper();
        }

        @Override
        public CategoryNodeWrapper[] newArray(int size) {
            return new CategoryNodeWrapper[size];
        }
    };
}
