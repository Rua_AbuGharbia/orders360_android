package com.telephoenic.orders360.controller.server.model.survey;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SurveyResponse implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("startDate")
    private String startDate;

    @JsonProperty("endDate")
    private String endDate;

    @JsonProperty("vendorName")
    private String vendorName;

    @JsonProperty("points")
    private Integer points;

    @JsonProperty("logoImageName")
    private String logoImageName;

    private  int submittedStatus;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getLogoImageName() {
        return logoImageName;
    }

    public void setLogoImageName(String logoImageName) {
        this.logoImageName = logoImageName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public int getSubmittedStatus() {
        return submittedStatus;
    }

    public void setSubmittedStatus(int submittedStatus) {
        this.submittedStatus = submittedStatus;
    }
}
