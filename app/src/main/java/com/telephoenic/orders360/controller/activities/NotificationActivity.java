package com.telephoenic.orders360.controller.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.model.NotificationResponse;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.TitleNotification_TextView)
    TextView titleNotificationTextView;
    @BindView(R.id.MessageNotification_TextView)
    TextView messageNotificationTextView;
    private NotificationResponse notificationResponse;
    Gson gson = new Gson();
    String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.white));
        }

        messageNotificationTextView.setMovementMethod(new ScrollingMovementMethod());

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("notification")) {
            NotificationResponse notificationResponse = extras.getParcelable("notification");
            if (notificationResponse != null) {
                title = notificationResponse.getTitle();
                messageNotificationTextView.setText(notificationResponse.getMessage());
                titleNotificationTextView.setText(title);
            }
        }
       /* if (getIntent().getStringExtra("notification") != null) {
            String notification = getIntent().getStringExtra("notification");
            notificationResponse = JsonUtils.toJavaObject(notification, NotificationResponse.class);
            title = notificationResponse.getTitle();
            msg.setText(notificationResponse.getMessage());
        }*/


    }
}
