package com.telephoenic.orders360.controller.server.model;

import java.util.List;

public class SupportTicketWrapper {
    private List<SupportTicketTypeModel> content;

    public List<SupportTicketTypeModel> getContent() {
        return content;
    }

    public void setContent(List<SupportTicketTypeModel> content) {
        this.content = content;
    }
}
