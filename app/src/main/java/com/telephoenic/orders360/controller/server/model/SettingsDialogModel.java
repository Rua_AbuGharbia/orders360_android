package com.telephoenic.orders360.controller.server.model;

public class SettingsDialogModel {

    private String dialog_title;
    private String dialog_message;
    private int dialog_image;

    public SettingsDialogModel(String dialog_title, String dialog_message, int dialog_image) {
        this.dialog_title = dialog_title;
        this.dialog_message = dialog_message;
        this.dialog_image = dialog_image;
    }

    public SettingsDialogModel() {
    }

    public String getDialog_title() {
        return dialog_title;
    }

    public void setDialog_title(String dialog_title) {
        this.dialog_title = dialog_title;
    }

    public String getDialog_message() {
        return dialog_message;
    }

    public void setDialog_message(String dialog_message) {
        this.dialog_message = dialog_message;
    }

    public int getDialog_image() {
        return dialog_image;
    }

    public void setDialog_image(int dialog_image) {
        this.dialog_image = dialog_image;
    }
}
