package com.telephoenic.orders360.controller.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.BarCodeActivity;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSaveData;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalUnsuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipmentArrival;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.telephoenic.orders360.controller.utils.AppUtils.isInternetConnected;

public class ConfirmArrivalDeliveryFragment extends BlankFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DialogUtils.OnDialogClickListener, OnMapReadyCallback {
    public static final String SHIPMENT_ID = "shipment_id";
    public static final String AMOUNT = "amount";
    public static final String INVOICE_NUMBER = "invoice_number";
    @BindView(R.id.Map_RelayiveLayout)
    RelativeLayout mapRelayiveLayout;
    @BindView(R.id.Confirm_Arrival_Buttom)
    Button confirmArrivalButtom;
    private int QR_CODE = 101;
    private int LOCATION_CODE = 201;
    @BindView(R.id.Successfully_RadioButton)
    RadioButton successfullyRadioButton;
    @BindView(R.id.NotSuccessfully_RadioButton)
    RadioButton notSuccessfullyRadioButton;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindInput(R.id.ReceiverName_Orders360EditText)
    Orders360EditText receiverNameOrders360EditText;
    @BindInput(R.id.MobileNumber_Orders360EditText)
    Orders360EditText mobileNumberOrders360EditText;
    @BindInput(R.id.Location_Orders360EditText)
    Orders360EditText locationOrders360EditText;
    @BindInput(R.id.QRCode_Orders360EditText)
    Orders360EditText qRCodeOrders360EditText;
    @BindInput(R.id.Reason_Orders360EditText)
    Orders360EditText reasonOrders360EditText;
    @BindView(R.id.Successfully_Group)
    Group successfullyGroup;
    @BindView(R.id.NotSuccessfully_Group)
    Group notSuccessfullyGroup;
    @BindInput(R.id.Note_Orders360EditText)
    Orders360EditText noteOrders360EditText;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    Unbinder unbinder;
    private OrdersView ordersView = new OrdersView();
    private Dialog dialog;

    //AlertDialog.Builder dialog;
    private GoogleMap mMap;
    private static final String TAG = "LocationActivity";
    private boolean newPos = false;
    private boolean isDialogVisible = false;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private double lat, lng;
    private LatLng currentLatLng;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 20000;
    private ArrivalUnsuccessfulModel unsuccessfulModel;

    private OrderRejectedReason reason;
    private List<OrderRejectedReason> rejectedReasonList;
    private ShipViewModel shipViewModel;
    private ListItemDialog listItemDialog;
    private Long shipmentId;
    private ShipmentArrival shipmentArrival = new ShipmentArrival();
    private double amount;
    private Long invoiceNumber;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirm_arrival_delivery_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        ordersView.bind(this, view);
        if (getArguments() != null) {
            shipmentId = getArguments().getLong(SHIPMENT_ID);
            invoiceNumber = getArguments().getLong(INVOICE_NUMBER);
            amount = getArguments().getDouble(AMOUNT);
        }
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        unsuccessfulModel = new ArrivalUnsuccessfulModel();
        reason = new OrderRejectedReason();
        inti();
        return view;
    }

    private void inti() {
        // LoadingDialog.showDialog(getActivity());
        shipViewModel.getReason(AppConstants.REASON_CODE, AppPrefs.getVendorID(getActivity())).observe(this, new ApiObserver<List<OrderRejectedReason>>() {
            @Override
            protected void noConnection() {
                //    LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<OrderRejectedReason> data) {
                //   LoadingDialog.dismiss();
                rejectedReasonList = new ArrayList<>();
                rejectedReasonList.addAll(data);

            }

            @Override
            protected void onError(String error) {
                //  LoadingDialog.dismiss();
                Toast.makeText(getActivity(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
        dialog = new Dialog(getActivity());

        final Intent qrIntent = new Intent(getActivity(), BarCodeActivity.class);
        reasonOrders360EditText.getContentEditText().setFocusable(false);
        locationOrders360EditText.getContentEditText().setFocusable(false);
        qRCodeOrders360EditText.getContentEditText().setFocusable(false);
        qRCodeOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    startActivityForResult(qrIntent, QR_CODE);
                //getActivity().startActivity(new Intent(getActivity(), QRCodeActivity.class));
            }
        });
        successfullyGroup.setVisibility(View.VISIBLE);
        notSuccessfullyGroup.setVisibility(View.GONE);
        successfullyRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    confirmArrivalButtom.setText(getString(R.string.button_labels_next));
                    successfullyGroup.setVisibility(Group.VISIBLE);
                    notSuccessfullyGroup.setVisibility(Group.GONE);
                } else {
                    confirmArrivalButtom.setText("Confirm arrival");
                    successfullyGroup.setVisibility(Group.GONE);
                    notSuccessfullyGroup.setVisibility(Group.VISIBLE);
                }
            }
        });

        locationOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LoadingDialog.showDialog(getContext());
                getLocation();
                if (mGoogleApiClient != null)
                    mGoogleApiClient.connect();

            }
        });

        reasonOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rejectedReasonList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) rejectedReasonList);
                    bundle.putString(ListItemDialog.TITLE, "Reason");
                    listItemDialog = new ListItemDialog(reasonClick);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }

            }
        });
        receiverNameOrders360EditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                shipmentArrival.setReceiver(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mobileNumberOrders360EditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                shipmentArrival.setMobileNumber(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        noteOrders360EditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                unsuccessfulModel.setNote(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void getLocation() {
        SupportMapFragment mapFragment = new SupportMapFragment();
        if (getFragmentManager() != null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        }
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), "Confirm Arrival");

        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick({R.id.Confirm_Arrival_Buttom, R.id.Arrival_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Confirm_Arrival_Buttom:
                if (successfullyRadioButton.isChecked()) {

                    if (shipmentArrival.getLatitude() == null) {
                        locationOrders360EditText.setErrorMessage(getString(R.string.please_get_your_location));
                        locationOrders360EditText.validate();
                    } else if (TextUtils.isEmpty(receiverNameOrders360EditText.getEditTextContent())) {
                        receiverNameOrders360EditText.setErrorMessage(getString(R.string.please_fill_receiver_name));
                        receiverNameOrders360EditText.validate();
                    } else if (TextUtils.isEmpty(mobileNumberOrders360EditText.getEditTextContent())) {
                        mobileNumberOrders360EditText.setErrorMessage(getString(R.string.please_fill_moble_number));
                        mobileNumberOrders360EditText.validate();
                    } else {
                        AppUtils.paymentMethodModelList = null;
                        Bundle bundle = new Bundle();
                        shipmentArrival.setShipmentId(shipmentId);
                        bundle.putSerializable(PaymentFragment.ARRIVAL_DATA, shipmentArrival);
                        bundle.putDouble(PaymentFragment.AMOUNT, amount);
                        bundle.putLong(PaymentFragment.INVOICE_NUMBER, invoiceNumber);
                        AppUtils.arrivalSaveData = new ArrivalSaveData();
                        AppUtils.arrivalSaveData.setLongitude(shipmentArrival.getLongitude());
                        AppUtils.arrivalSaveData.setLatitude(shipmentArrival.getLatitude());
                        AppUtils.arrivalSaveData.setMobileNumber(shipmentArrival.getMobileNumber());
                        AppUtils.arrivalSaveData.setReceiver(shipmentArrival.getReceiver());
                        gotoFragment(new PaymentFragment(), getFragmentManager(), bundle);
                    }
                } else {
                    if (unsuccessfulModel.getLatitude() == null) {
                        locationOrders360EditText.setErrorMessage(getString(R.string.please_get_your_location));
                        locationOrders360EditText.validate();
                    } else if (TextUtils.isEmpty(reasonOrders360EditText.getEditTextContent())) {
                        reasonOrders360EditText.setErrorMessage(getString(R.string.please_select_reason));
                        reasonOrders360EditText.validate();
                    } else {
                        LoadingDialog.showDialog(getActivity());
                        shipViewModel.confirmArrivalUnSuccess(shipmentId, unsuccessfulModel).observe(this, new ApiCompleted() {
                            @Override
                            protected void noConnection() {
                                LoadingDialog.dismiss();
                                noConnectionInternetDialog();
                            }

                            @Override
                            protected void onSuccess() {
                                LoadingDialog.dismiss();
                                Toast.makeText(getContext(), getString(R.string.confrim_arrival), Toast.LENGTH_SHORT).show();
                                if (getActivity() != null && getContext() != null)
                                    getContext().startActivity(new Intent(getActivity(), MainActivity.class));

                            }

                            @Override
                            protected void onError(String error) {
                                LoadingDialog.dismiss();
                                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                }

                break;
            case R.id.Arrival_Buttom:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QR_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                qRCodeOrders360EditText.getContentEditText().setText("Scan Successfully");
                qRCodeOrders360EditText.setEditTextEnable(false);
                qRCodeOrders360EditText.setClickable(false);
                qRCodeOrders360EditText.getContentEditText().setClickable(false);
            }
        } else if (requestCode == LOCATION_CODE) {
            getLocation();

            if (mGoogleApiClient != null)
                mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "onConnected Permission Denied", Toast.LENGTH_SHORT).show();
            return;
        }

        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();

        } else {
            LoadingDialog.dismiss();
            locationOrders360EditText.setText("Successfully");
            currentLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            mapRelayiveLayout.setVisibility(View.VISIBLE);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
            shipmentArrival.setLatitude(mLocation.getLatitude());
            shipmentArrival.setLongitude(mLocation.getLongitude());
            unsuccessfulModel.setLatitude(mLocation.getLatitude());
            unsuccessfulModel.setLongitude(mLocation.getLongitude());
        }

        if (isGpsEnabled()) {
            dismissLocationDialog();
        } else {
            LoadingDialog.dismiss();
            showLocationDialog();
        }
        if (!isInternetConnected(getActivity())) {
            noConnectionInternetDialog();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private boolean isGpsEnabled() {
        ContentResolver contentResolver = getActivity().getContentResolver();
        int mode = Settings.Secure.getInt(
                contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);
        return mode != Settings.Secure.LOCATION_MODE_OFF;
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }

    @Override

    public void onOkClicked() {

    }

    @Override
    public void onCancelClicked() {

    }

    private void showLocationDialog() {
        isDialogVisible = true;
        dialog.setContentView(R.layout.location__dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        (dialog.findViewById(R.id.location_dialog_cancel_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        (dialog.findViewById(R.id.location_dialog_settings_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent locationSettingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(locationSettingsIntent, 201);
                    }
                });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    protected void startLocationUpdates() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Start Location Updates Permission Denied", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void dismissLocationDialog() {
        if (isDialogVisible) {
            dialog.dismiss();
        }
    }

    private void getStreetName() {
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                StringBuilder strReturnedAddress = new StringBuilder();
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("");
                }
                locationOrders360EditText.setText(returnedAddress.getAddressLine(0));
                Toast.makeText(getContext(), returnedAddress.getAddressLine(0) + "", Toast.LENGTH_SHORT).show();

            } else {
                // et_lugar.setText("No Address returned!");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            // et_lugar.setText("Canont get Address!");
        }
    }

    private OnRecyclerClick reasonClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            reasonOrders360EditText.setText(rejectedReasonList.get(position).getName());
            reason = new OrderRejectedReason();
            reason.setId(rejectedReasonList.get(position).getId());
            unsuccessfulModel.setReason(reason);
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.setMyLocationEnabled(false);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        // Add a marker in saved location came from the previous activity by intent and move the camera.
        if (currentLatLng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUtils.arrivalSaveData != null) {
            receiverNameOrders360EditText.setText(AppUtils.arrivalSaveData.getReceiver() + "");
            mobileNumberOrders360EditText.setText(AppUtils.arrivalSaveData.getMobileNumber() + "");
            getLocation();
            if (mGoogleApiClient != null)
                mGoogleApiClient.connect();
        }
    }
}
