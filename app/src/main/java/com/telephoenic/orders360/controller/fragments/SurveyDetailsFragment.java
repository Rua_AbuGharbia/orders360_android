package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.survey.QuestionType;
import com.telephoenic.orders360.controller.server.model.survey.SurveyAnswerWrapper;
import com.telephoenic.orders360.controller.server.model.survey.SurveyAnswersRequest;
import com.telephoenic.orders360.controller.server.model.survey.SurveyDetailsRequest;
import com.telephoenic.orders360.controller.server.model.survey.SurveyDetailsResponse;
import com.telephoenic.orders360.controller.server.model.survey.SurveyQuestionWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.mvp.MVPView;
import com.telephoenic.orders360.mvp.Presenter;
import com.telephoenic.orders360.mvp.PresenterImpl;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.SurveyViewModel;

import org.springframework.util.LinkedMultiValueMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;


public class SurveyDetailsFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {
    private String TAG = SurveyDetailsFragment.class.getName();

    private SurveyDetailsResponse surveyResponse;
    private int questionPosition = 1;
    private boolean isEnabled = true; // TODO for  Enable  or Disable Survey
    private SurveyDetailsRequest surveyDetailsRequest;
    private SurveyAnswersRequest surveyAnswersRequest;

    private Long surveyID;

    private PosProfileWrapper posProfileWrapper;

    private SurveyViewModel surveyViewModel;


    private List<SurveyQuestionWrapper> radioButtonList;
    private List<SurveyQuestionWrapper> checkBoxList;
    private List<SurveyQuestionWrapper> textAreaList;

    private List<SurveyQuestionWrapper> questionsWithAnswers;
    private LinearLayout linearLayout;

    private Button submitButton;

    private int validationSize = 0;

    private String myService = "";

    public SurveyDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.survey_title));

        return inflater.inflate(R.layout.fragment_survey_details, container, false);
    }

    private void createComponents(View v) {

        linearLayout = v.findViewById(R.id.linear_layout);
        submitButton = v.findViewById(R.id.survey_submit);

        Bundle bundle = getArguments();
        if (bundle != null) {
            surveyID = bundle.getLong(IntentExtraNames.ID);
        }

        posProfileWrapper = AppUtils.getProfile(getContext());


        radioButtonList = new ArrayList<>();
        checkBoxList = new ArrayList<>();
        textAreaList = new ArrayList<>();

        sendRequest();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                surveySubmit();
            }
        });
    }

    private void sendRequest() {
        myService = AppConstants.SERVICE_SURVEY_DETAILS;
        LoadingDialog.showDialog(getContext());
        surveyViewModel.getSurveyDetails(surveyID,posProfileWrapper.getUser().getId()).observe(this, new ApiObserver<SurveyDetailsResponse>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(SurveyDetailsResponse data) {
                LoadingDialog.dismiss();

                List<SurveyQuestionWrapper> questionsList = data.getQuestions();
                for (SurveyQuestionWrapper wrapper : questionsList) {
                    if (AppConstants.CHECKBOX_QUESTION_CODE.equals(wrapper.getQuestionType().getCode())) {
                        checkBoxList.add(wrapper);
                    } else if (AppConstants.RADIO_QUESTION_CODE.equals(wrapper.getQuestionType().getCode())) {
                        radioButtonList.add(wrapper);
                    } else if (AppConstants.TEXTAREA_QUESTION_CODE.equals(wrapper.getQuestionType().getCode())) {
                        textAreaList.add(wrapper);
                    }
                }
                addRadioButton(radioButtonList);
                addCheckBox(checkBoxList);
                addTextArea(textAreaList);
                validationSize = questionsList.size();
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();
            }
        });

    }

    public void addRadioButton(List<SurveyQuestionWrapper> radioButtonList) {

        for (SurveyQuestionWrapper item : radioButtonList) {
            LinearLayout linearLayout1 = new LinearLayout(getActivity());
            linearLayout1.setOrientation(LinearLayout.HORIZONTAL);

            TextView textViewLabel = new TextView(getActivity());
            textViewLabel.setText(" " + "(" + questionPosition + ")" + " ");

            textViewLabel.setPadding(0, (int) getResources().getDimension(R.dimen.margin5), 0, (int) getResources().getDimension(R.dimen.margin5));
            linearLayout1.addView(textViewLabel);

            TextView textView = new TextView(getActivity());
            textView.setText(item.getName().trim());
            textView.setTextColor(getActivity().getResources().getColor(R.color.default_blue_light));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(0, (int) getResources().getDimension(R.dimen.margin5), 0, (int) getResources().getDimension(R.dimen.margin5));
            linearLayout1.addView(textView);

            linearLayout.addView(linearLayout1);

            if (item.getAnswerList() != null && item.getAnswerList().size() > 0) {
                RadioGroup radioGroup = new RadioGroup(getActivity());
                //radioGroup.setId(Integer.parseInt(item.getId()));
                radioGroup.setTag(item);
                for (SurveyAnswerWrapper itemSurvey : item.getAnswerList()) {
                    //if(AppPrefs.getLanguage(this).languge.equals("en")){
                    RadioButton radioButton = new RadioButton(getActivity());
                    radioButton.setId(itemSurvey.getId().intValue());
                    radioButton.setText(" " + itemSurvey.getName().trim());
                    radioButton.setEnabled(isEnabled);
                       /* if(itemSurvey.getSelected() != null) // TODO for selected Ansewr
                            radioButton.setChecked(itemSurvey.getSelected());*/
                    radioGroup.addView(radioButton);
                }
                linearLayout.addView(radioGroup);
            }
            View view = new View(getActivity());
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            view.setBackgroundColor(getResources().getColor(R.color.border_color));
            linearLayout.addView(view);
            questionPosition++;
        }
    }

    public void addCheckBox(List<SurveyQuestionWrapper> checkBoxList) {
        for (SurveyQuestionWrapper item : checkBoxList) {

            LinearLayout linearLayout1 = new LinearLayout(getActivity());
            linearLayout1.setOrientation(LinearLayout.HORIZONTAL);

            TextView textViewLabel = new TextView(getActivity());
            textViewLabel.setText(" " + "(" + questionPosition + ")" + " ");
            textViewLabel.setPadding(0, (int) getResources().getDimension(R.dimen.margin5), 0, (int) getResources().getDimension(R.dimen.margin5));
            linearLayout1.addView(textViewLabel);

            TextView textView = new TextView(getActivity());
            textView.setText(item.getName().trim());
            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setTextColor(getActivity().getResources().getColor(R.color.default_blue_light));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(0, (int) getResources().getDimension(R.dimen.margin5), 0, (int) getResources().getDimension(R.dimen.margin5));
            linearLayout1.addView(textView);

            linearLayout.addView(linearLayout1);

            if (item.getAnswerList() != null && item.getAnswerList().size() > 0) {

                for (SurveyAnswerWrapper itemSurvey : item.getAnswerList()) {

                    CheckBox checkBox = new CheckBox(getActivity());
                    checkBox.setText(itemSurvey.getName().trim());
                    checkBox.setId(itemSurvey.getId().intValue());
                    checkBox.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    checkBox.setTag(item);
                    checkBox.setEnabled(isEnabled);
                        /*if(itemSurvey.getSelected() != null)
                            checkBox.setChecked(itemSurvey.getSelected());*/
                    linearLayout.addView(checkBox);
                }
            }

            View view = new View(getActivity());
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            view.setBackgroundColor(getResources().getColor(R.color.border_color));
            linearLayout.addView(view);
            questionPosition++;
        }

    }

    public void addTextArea(List<SurveyQuestionWrapper> textAreaList) {

        for (SurveyQuestionWrapper item : textAreaList) {
            LinearLayout linearLayout1 = new LinearLayout(getActivity());
            linearLayout1.setOrientation(LinearLayout.HORIZONTAL);

            TextView textViewLabel = new TextView(getActivity());
            textViewLabel.setText(" " + "(" + questionPosition + ")" + " ");
            textViewLabel.setPadding(0, (int) getResources().getDimension(R.dimen.margin5), 0, (int) getResources().getDimension(R.dimen.margin5));
            linearLayout1.addView(textViewLabel);

            TextView textView = new TextView(getActivity());
            textView.setText(item.getName().trim());
            textView.setTextColor(getActivity().getResources().getColor(R.color.default_blue_light));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(0, (int) getResources().getDimension(R.dimen.margin5), 0, (int) getResources().getDimension(R.dimen.margin5));
            linearLayout1.addView(textView);

            linearLayout.addView(linearLayout1);

            EditText editText = new EditText(getActivity());
            editText.setId(item.getId().intValue());
            editText.setMinHeight((int) getResources().getDimension(R.dimen.activity_survey_text_area_height));
            editText.setTag(item);
            int padding = (int) getResources().getDimension(R.dimen.margin5);
            editText.setPadding(padding, padding, padding, padding);
            editText.setBackground(getResources().getDrawable(R.drawable.activity_login_form_box_bg));
            editText.setGravity(Gravity.TOP);

            editText.setEnabled(isEnabled);
            linearLayout.addView(editText);
            //editText.setText(item.getAnswerModelList().get(0).getTitle());
               /* if(item.getAnswerList() != null && item.getAnswerList().size() > 0) // todo for user Answer
                    editText.setText(item.getAnswerList().get(0).getName());*/

//            View view = new View(getActivity());
//            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
//            view.setBackgroundColor(getResources().getColor(R.color.border_color));
//            linearLayout.addView(view);

            questionPosition++;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        surveyViewModel = ViewModelProviders.of(this).get(SurveyViewModel.class);
        createComponents(view);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void surveySubmit() {
        surveyAnswersRequest = new SurveyAnswersRequest();
        surveyAnswersRequest.setSurveyId(surveyID);
        surveyAnswersRequest.setParticipantId(posProfileWrapper.getUser().getId());
        questionsWithAnswers = new ArrayList<>();

        ArrayList<RadioGroup> radioGroups = getAllRadioGroup(linearLayout);

        for (RadioGroup radioGroup : radioGroups) {
            SurveyQuestionWrapper obj = (SurveyQuestionWrapper) radioGroup.getTag();
            SurveyQuestionWrapper radioButtonResult = new SurveyQuestionWrapper();
            radioButtonResult.setId(obj.getId()); //setQuestionId
            radioButtonResult.setQuestionType(new QuestionType(AppConstants.RADIO_QUESTION_CODE));

            List<SurveyAnswerWrapper> answer = new ArrayList<>();
            SurveyAnswerWrapper answerWrapper = new SurveyAnswerWrapper();
            Long answerId = Long.valueOf(radioGroup.getCheckedRadioButtonId());
            Log.d("answerId", answerId + "");
            if (answerId != -1) {
                answerWrapper.setId(answerId);// answer ID
                answer.add(answerWrapper);
                radioButtonResult.setAnswerList(answer);
                questionsWithAnswers.add(radioButtonResult);
            }
        }

        ArrayList<CheckBox> checkBoxes = getAllCheckBox(linearLayout);

        Map<Long, List<CheckBox>> mapResult = getCheckBoxesMap(checkBoxes);

        Iterator myVeryOwnIterator = mapResult.keySet().iterator();

        while (myVeryOwnIterator.hasNext()) {
            Long key = (Long) myVeryOwnIterator.next(); // Question ID
            List<CheckBox> value = mapResult.get(key);  // answeres

            SurveyQuestionWrapper checkBoxResult = new SurveyQuestionWrapper();
            checkBoxResult.setId(key);
            checkBoxResult.setQuestionType(new QuestionType(AppConstants.CHECKBOX_QUESTION_CODE));

            List<SurveyAnswerWrapper> answer = new ArrayList<>();
            for (CheckBox checkBox : value) {
                SurveyAnswerWrapper answerWrapper = new SurveyAnswerWrapper();
                answerWrapper.setId(Long.valueOf(checkBox.getId()));// answer ID
                answer.add(answerWrapper);
            }
            if (answer.size() > 0) {
                checkBoxResult.setAnswerList(answer);
                questionsWithAnswers.add(checkBoxResult);
            }
        }

        ArrayList<EditText> editTexts = getAllEditText(linearLayout);

        for (EditText editText : editTexts) {

            SurveyQuestionWrapper wrapper = (SurveyQuestionWrapper) editText.getTag();

            SurveyQuestionWrapper textAreaResult = new SurveyQuestionWrapper();
            textAreaResult.setId(wrapper.getId());
            textAreaResult.setQuestionType(new QuestionType(AppConstants.TEXTAREA_QUESTION_CODE));
            List<SurveyAnswerWrapper> answer = new ArrayList<>();
            SurveyAnswerWrapper answerWrapper = new SurveyAnswerWrapper();
            String answerString = editText.getText().toString().trim();
            if (!answerString.equals("")) {
                answerWrapper.setName(answerString);
                answer.add(answerWrapper);
                textAreaResult.setAnswerList(answer);
                questionsWithAnswers.add(textAreaResult);
            }
        }
        if (questionsWithAnswers.size() == validationSize) {
            surveyAnswersRequest.setQuestionsWithAnswers(questionsWithAnswers);
            submitSurvey();
        } else {
            Toast.makeText(getContext(), R.string.please_answer_all_questions, Toast.LENGTH_SHORT).show();
        }

    }

    private void submitSurvey(){
        myService = AppConstants.SERVICE_SUBMIT_SURVEY;
        LoadingDialog.showDialog(getContext());
        surveyViewModel.submitSurvey(surveyID,posProfileWrapper.getUser().getId(),questionsWithAnswers)
                .observe(this, new ApiObserver<ResponseBody>() {
                    @Override
                    protected void noConnection() {
                        LoadingDialog.dismiss();
                        noConnectionInternetDialog();
                    }

                    @Override
                    protected void onSuccess(ResponseBody data) {
                        LoadingDialog.dismiss();

                        Toast.makeText(getContext(), getActivity().getString(R.string.survey_details_toast_message_sent), Toast.LENGTH_SHORT).show();
                        Bundle bundle = new Bundle();
                        bundle.putLong(IntentExtraNames.ID, posProfileWrapper.getUser().getId());
                        AppConstants.MY_ACTIVITY = "Login";
                        if (getActivity() != null)
                            getActivity().finish();
                        IntentFactory.startActivity(getActivity(), MainActivity.class, true, false, bundle);

                    }

                    @Override
                    protected void onError(String error) {
                        LoadingDialog.dismiss();
                        Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();
                    }
                });

    }

    private ArrayList<RadioGroup> getAllRadioGroup(View view) {
        ViewGroup vg = (ViewGroup) view;

        ArrayList<RadioGroup> radioGroups = new ArrayList<>();

        for (int i = 0; i < vg.getChildCount(); i++) {
            View vv = vg.getChildAt(i);
            if (vg.getChildAt(i) instanceof RadioGroup) {
                radioGroups.add((RadioGroup) vv);
            }
        }


        return radioGroups;
    }


    private ArrayList<CheckBox> getAllCheckBox(View view) {
        ViewGroup vg = (ViewGroup) view;

        ArrayList<CheckBox> ceckBoxes = new ArrayList<>();

        for (int i = 0; i < vg.getChildCount(); i++) {
            View vv = vg.getChildAt(i);
            if (vg.getChildAt(i) instanceof CheckBox) {
                ceckBoxes.add((CheckBox) vv);
            }
        }

// must return list of map
        return ceckBoxes;
    }


    public Map<Long, List<CheckBox>> getCheckBoxesMap(ArrayList<CheckBox> checkBoxes) {

        Map<Long, List<CheckBox>> map = new HashMap<>();

        List<CheckBox> list = null;

        for (CheckBox checkBox : checkBoxes) {
            SurveyQuestionWrapper wrapper = (SurveyQuestionWrapper) checkBox.getTag();
            if (map.get(wrapper.getId()) != null) {
                if (map.containsKey(wrapper.getId())) {
                    //if(wrapper.getIsRequired())
                    if (checkBox.isChecked())
                        map.get(wrapper.getId()).add(checkBox);
                }
            } else {
                list = new ArrayList<>();
                if (checkBox.isChecked()) {
                    list.add(checkBox);
                }

                //if(wrapper.getIsRequired())
                if (checkBox.isChecked())
                    map.put(wrapper.getId(), list);
            }

        }


        return map;
    }

    private ArrayList<EditText> getAllEditText(View view) {
        ViewGroup vg = (ViewGroup) view;

        ArrayList<EditText> editTexts = new ArrayList<>();

        for (int i = 0; i < vg.getChildCount(); i++) {
            View vv = vg.getChildAt(i);
            if (vg.getChildAt(i) instanceof EditText) {
                editTexts.add((EditText) vv);
            }
        }


        return editTexts;
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_SURVEY_DETAILS.equals(myService)){
                sendRequest();
            }else if(AppConstants.SERVICE_SUBMIT_SURVEY.equals(myService)){
                submitSurvey();
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}
