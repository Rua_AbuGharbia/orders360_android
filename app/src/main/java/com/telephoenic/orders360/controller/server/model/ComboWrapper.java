package com.telephoenic.orders360.controller.server.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rua on 08-Nov-17.
 */

public class ComboWrapper extends OrderItems implements Serializable  {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("deleted")
    private boolean deleted;

    @JsonProperty("inactive")
    private boolean inactive;

    @JsonProperty("name")
    private String name;

    @JsonProperty("startDate")
    private String  startDate;

    @JsonProperty("endDate")
    private String  endDate;

    @JsonProperty("code")
    private String code;

    @JsonProperty("listOfComboInfo")
    private List<ComboInfoWrapper> listOfComboInfo;

    @JsonProperty("logoImageName")
    private  String logoImageName ;

    private boolean canDeleted ;

    private  String count;

    public ComboWrapper() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public List<ComboInfoWrapper> getListOfComboInfo() {
        return listOfComboInfo;
    }

    public void setListOfComboInfo(List<ComboInfoWrapper> listOfComboInfo) {
        this.listOfComboInfo = listOfComboInfo;
    }

    public boolean isCanDeleted() {
        return canDeleted;
    }

    public void setCanDeleted(boolean canDeleted) {
        this.canDeleted = canDeleted;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLogoImageName() {
        return logoImageName;
    }

    public void setLogoImageName(String logoImageName) {
        this.logoImageName = logoImageName;
    }
}
