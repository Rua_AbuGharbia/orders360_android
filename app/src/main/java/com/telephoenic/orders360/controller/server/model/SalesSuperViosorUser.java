package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;

public class SalesSuperViosorUser extends User implements Serializable {

    private VendorWrapper vendor;

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }
}
