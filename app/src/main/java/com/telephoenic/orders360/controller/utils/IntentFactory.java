package com.telephoenic.orders360.controller.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.model.AppPrefs;


/**
 * Created by Ihab on 8/25/2016.
 */
public class IntentFactory {

    public static void startActivity(Activity activity, Class clazz , boolean finishActivity){
        if (activity ==null){
            return;
        }
        Intent intent = new Intent(activity, clazz);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        if(finishActivity){
            activity.finish();
        }
    }

    public static void startActivity(Activity activity, Class clazz , boolean finishActivity ,boolean withTransaction, Bundle bundle){
        if (activity ==null){
            return;
        }
        Intent intent = new Intent(activity, clazz);
        intent.putExtra(IntentExtraNames.BUNDLE,bundle);
        activity.startActivity(intent);
        if(withTransaction){
            activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        }
        if(finishActivity){
            activity.finish();
        }
    }

    public static void startActivityForResult(Activity activity , Class clazz ,int requestCode){
        Intent intent = new Intent(activity, clazz);
        activity.startActivityForResult(intent,requestCode);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }
    public static void logOut(Activity activity){
       // AppPrefs.setVendorID(activity,-1L);
        AppPrefs.setPOS(activity, null);
        Intent intnt = new Intent(activity, LoginActivity.class);
        // Closing all the Activities
        intnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        intnt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        // Staring Login Activity
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        activity.startActivity(intnt);
        activity.finishAffinity();
    }
}
