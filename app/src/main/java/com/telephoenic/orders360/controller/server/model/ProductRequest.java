package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 11-Dec-17.
 */


public class ProductRequest implements Serializable {

    @JsonProperty("vendor")
    VendorWrapper vendor;

    @JsonProperty("category")
    CategoryNodeWrapper category;

    @JsonProperty("pageNum")
    String pageNum;

    @JsonProperty("pageSize")
    String pageSize;

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    public CategoryNodeWrapper getCategory() {
        return category;
    }

    public void setCategory(CategoryNodeWrapper category) {
        this.category = category;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
