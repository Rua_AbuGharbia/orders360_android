package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;

public class ShipmentArrival implements Serializable {
    private Long shipmentId;
    private String receiver;
    private String mobileNumber;
    private Double latitude;
    private Double longitude;

    public ShipmentArrival(String receiver, String mobileNumber, Double latitude, Double longitude) {
        this.receiver = receiver;
        this.mobileNumber = mobileNumber;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public ShipmentArrival() {
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Long shipmentId) {
        this.shipmentId = shipmentId;
    }
}
