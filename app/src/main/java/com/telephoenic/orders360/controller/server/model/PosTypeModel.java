package com.telephoenic.orders360.controller.server.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// new By Diaa
public class PosTypeModel implements Serializable {

    @SerializedName("id")
    private Long id;
    @SerializedName("deleted")
    private Boolean deleted;
    @SerializedName("inactive")
    private Boolean inactive;
    @SerializedName("code")
    private String code;
    @SerializedName("type")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PosTypeModel{" +
                "id=" + id +
                ", deleted=" + deleted +
                ", inactive=" + inactive +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
