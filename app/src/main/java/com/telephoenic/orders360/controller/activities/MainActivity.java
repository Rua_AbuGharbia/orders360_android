package com.telephoenic.orders360.controller.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.constants.MainFragmentCategories;
import com.telephoenic.orders360.controller.fragments.AddPOSFragment;
import com.telephoenic.orders360.controller.fragments.CampaignFragment;
import com.telephoenic.orders360.controller.fragments.ChooseSurveyFragment;
import com.telephoenic.orders360.controller.fragments.ConfirmArrivalDeliveryFragment;
import com.telephoenic.orders360.controller.fragments.MainFragment;
import com.telephoenic.orders360.controller.fragments.MoreFragment;
import com.telephoenic.orders360.controller.fragments.PrivacyPolicyFragment;
import com.telephoenic.orders360.controller.fragments.ProfileFragment;
import com.telephoenic.orders360.controller.fragments.ProfilePersonallyFragment;
import com.telephoenic.orders360.controller.fragments.PurchaseFragment;
import com.telephoenic.orders360.controller.fragments.PurchaseHistoryFragment;
import com.telephoenic.orders360.controller.fragments.ReadyToShipFragment;
import com.telephoenic.orders360.controller.fragments.SalesmanFragment;
import com.telephoenic.orders360.controller.fragments.SearchPOSFragment;
import com.telephoenic.orders360.controller.fragments.SettingsFragment;
import com.telephoenic.orders360.controller.fragments.ShipOrdersFragment;
import com.telephoenic.orders360.controller.fragments.SupportTicketsFragment;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.mvp.MVPView;
import com.telephoenic.orders360.view.customcontrols.SelectVendorDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;

import org.springframework.util.LinkedMultiValueMap;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.telephoenic.orders360.controller.constants.MainFragmentCategories.MAIN_FRAGMENT;

public class MainActivity extends AppCompatActivity implements MVPView, MoreFragment.OnFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener {

    public static final String TAG = "MainActivity";
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.VendorLogo_ToolBar_CircleImageView)
    CircleImageView vendorLogoToolBarCircleImageView;

    private PosProfileWrapper posProfileWrapper;
    private ImageController imageController;
    private List<String> privilegeList;
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    Fragment fragment = null;
    private String fragmentTag = "home";
    private Context context;
    private AHBottomNavigation bottomNavigation;
    private int myCartIndex = -1;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.white));
        }
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (AppPrefs.isInstalledRightKnow(this)) {
            AppPrefs.setInstalledRightKnowFlag(this, false);
            this.startActivity(new Intent(MainActivity.this, OnBoardingActivity.class));
            finish();
            return;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        posProfileWrapper = AppUtils.getProfile(this);
        if (getIntent().getExtras() != null) {
            myCartIndex = getIntent().getExtras().getInt(TAG, -1);
        }

        showBottomNavigationItems();
        Typeface typeface = ResourcesCompat.getFont(Objects.requireNonNull(context), R.font.order360_normal_font);
        bottomNavigation.setTitleTypeface(typeface);
        if (myCartIndex != -1) {
            bottomNavigation.setCurrentItem(2);
        } else {
            replaceFragment(MAIN_FRAGMENT.getIndex(), false);
        }
    }

//    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
//            = new BottomNavigationView.OnNavigationItemSelectedListener() {
//        @Override
//        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            switch (item.getItemId()) {
//                case R.id.navigation_home:
//                    replaceFragment(MAIN_FRAGMENT.getIndex(), true);
//                    return true;
//                case R.id.navigation_my_orders:
//                    replaceFragment(PURCHASE_HISTORY.getIndex(), true);
//                    return true;
//                case R.id.navigation_my_cart:
//                    replaceFragment(MainFragmentCategories.PURCHASE.getIndex(), true);
//                    return true;
//                case R.id.navigation_support_ticket:
//                    replaceFragment(MainFragmentCategories.SUPPORT_TICKETS.getIndex(), true); // TODO for Testing
//                    return true;
//                case R.id.navigation_survey:
//                    replaceFragment(MainFragmentCategories.SURVEY.getIndex(), true);
//                    return true;
//            }
//            return false;
//        }
//    };

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void navigate(String response) {
    }

    @Override
    public void navigate(LinkedMultiValueMap response) {
    }


    private void replaceFragment(int index, boolean addToBackStack) {
        Bundle bundle = new Bundle();

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        switch (Objects.requireNonNull(MainFragmentCategories.getByIndex(index))) {
            case MAIN_FRAGMENT:
                fragment = new MainFragment();
                fragmentTag = "home";
                // fragment = new SearchPOSFragment();
                break;
            case SALES_MAN:
                fragment = new SalesmanFragment(); // TODO Go To SALES MAN Fragment
                break;
            case SETTINGS:
                fragment = new SettingsFragment();
                break;
            case PURCHASE_HISTORY:
                fragmentTag = "my order";
                fragment = new PurchaseHistoryFragment();
                //   fragment = new DelivryFragment();
                //   fragment = new ScanItemSerialsFragment();

                //  fragment = new ConfirmArrivalDeliveryFragment();
                break;

            case PURCHASE:
                fragment = new PurchaseFragment();
                break;
            case PRIVACY_POLICY:
                fragment = new PrivacyPolicyFragment();
                break;
            case SURVEY:
                fragment = new ChooseSurveyFragment();
                break;
            case CAMPAIGN:
                fragment = new CampaignFragment();
                break;
            case PROFILE:
                fragment = new ProfilePersonallyFragment();
                break;
            case SUPPORT_TICKETS:
                fragmentTag = "support tickets";
                fragment = new SupportTicketsFragment();
                break;
            case ORDER_NOW:
//                fragmentTag = "order now";
//                Bundle bundle = new Bundle();
//                bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, 3);

//                fragment = new OrderNowFragment();c
//                fragment.setArguments(bundle);
                break;
            case SPECIAL_OFFERS:
//                Bundle bundle1 = new Bundle();
//                bundle1.putInt(IntentExtraNames.NUMBER_OF_TABS, 2);
//                fragment = new OrderNowFragment();
//                fragment.setArguments(bundle1);
                break;
            case ADDPOS:
                fragment = new AddPOSFragment();
                break;
            case SALESMAN:
                fragment = new SearchPOSFragment();
                break;
            case MORE:
                fragmentTag = "more";
                fragment = new MoreFragment();
                break;
            case SHIP_ORDERS:
                bundle.putString(ShipOrdersFragment.DELIVERY_STATUS, AppConstants.READY_TO_SHIP_STATUS);
                fragment = new ShipOrdersFragment();
                fragment.setArguments(bundle);
                break;
            case DELIVERY_ORDERS:
                bundle = new Bundle();
                bundle.putString(ShipOrdersFragment.DELIVERY_STATUS, AppConstants.SHIPPED_STATUS);
                fragment = new ShipOrdersFragment();
                fragment.setArguments(bundle);

                break;
        }
        //   title = AppUtils.getTitle(fragment);
        //  getSupportActionBar().setTitle(title);
        //  AppUtils.changeTitle(this, title);
        // fragmentTransaction.remove(null);
        performNoBackStackTransaction(fragmentManager, fragmentTag, fragment);
//        fragmentTransaction.replace(R.id.container, fragment);
//        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//        if (addToBackStack) fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.updateNotifactionOrder(this);
    }


    @SuppressLint("ResourceAsColor")
    private void showBottomNavigationItems() {
        bottomNavigation = findViewById(R.id.navigation);

        AHBottomNavigationItem home = new AHBottomNavigationItem(getResources().getString(R.string.title_activity_main), R.drawable.ic_home);
        AHBottomNavigationItem myOrders = new AHBottomNavigationItem(getResources().getString(R.string.navigation_drawer_orders), R.drawable.ic_my_order);
        AHBottomNavigationItem shipeOrder = new AHBottomNavigationItem(getString(R.string.ship_orders), R.drawable.shipe_order_ic);
        AHBottomNavigationItem posOrders = new AHBottomNavigationItem(getResources().getString(R.string.navigation_drawer_pos_orders), R.drawable.my_orders);
        AHBottomNavigationItem posLocation = new AHBottomNavigationItem(getResources().getString(R.string.button_label_pos_location), R.drawable.ic_location_pin);
        AHBottomNavigationItem cart = new AHBottomNavigationItem(getResources().getString(R.string.navigation_drawer_cart), R.drawable.my_cart);
        AHBottomNavigationItem deleviryOrder = new AHBottomNavigationItem(getString(R.string.delivery_orders), R.drawable.ic_deleviry_car);
        AHBottomNavigationItem ticket = new AHBottomNavigationItem(getResources().getString(R.string.navigation_drawer_communication), R.drawable.ticket);
        AHBottomNavigationItem survey = new AHBottomNavigationItem(getResources().getString(R.string.navigation_drawer_survey), R.drawable.survey);
        AHBottomNavigationItem addPOS = new AHBottomNavigationItem(getResources().getString(R.string.add_pos_tab_text), R.drawable.add_pos_icon);
        AHBottomNavigationItem more = new AHBottomNavigationItem(getResources().getString(R.string.more_tab_text), R.drawable.ic_more);

        int itemCount = 0;
        bottomNavigation.addItem(home);
        itemCount++;

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.white));
        bottomNavigation.setAccentColor(getResources().getColor(R.color.verified_blue));

        if (posProfileWrapper != null) {
            privilegeList = posProfileWrapper.getUser().getPermissions();
            for (String item : privilegeList) {
                if (itemCount == 4) {
                    break;
                }
                if (AppConstants.ROLE_MY_ORDERS_VIEW.equals(item)) {
                    bottomNavigation.addItem(myOrders);
                    itemCount++;
                } else if (AppConstants.ROLE_SUPPORT_TICKET_SEND.equals(item)) { // TODO FOR support Ticet
                    bottomNavigation.addItem(ticket);
                    itemCount++;
                } else if (AppConstants.ROLE_POS_ORDERS_VIEW.equals(item)) {
                    bottomNavigation.addItem(posOrders);
                    itemCount++;
                } else if (AppConstants.ROLE_POS_ADD.equals(item)) {
                    bottomNavigation.addItem(addPOS);
                    itemCount++;

                } else if (AppConstants.ROLE_CART_EDIT.equals(item)) {
                    bottomNavigation.addItem(cart);
                } else if (AppConstants.ROLE_SHIP_ORDERS.equals(item)) {
                    bottomNavigation.addItem(shipeOrder);
                } else if (AppConstants.ROLE_DELIVER_ORDERS.equals(item)) {
                    bottomNavigation.addItem(deleviryOrder);
                }
            }
            bottomNavigation.addItem(more);
        }

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (wasSelected) {
                    return false;
                }
                posProfileWrapper = AppUtils.getProfile(MainActivity.this);
                String title = bottomNavigation.getItem(position).getTitle(MainActivity.this);
                if (title.equals(getResources().getString(R.string.more_tab_text))) {
                    replaceFragment(MainFragmentCategories.MORE.getIndex(), false);
                    return true;
                }
                if (title.equals(getResources().getString(R.string.title_activity_main))) {
                    replaceFragment(MainFragmentCategories.MAIN_FRAGMENT.getIndex(), false);
                    return true;
                }
                if (posProfileWrapper != null) {
                    if (posProfileWrapper.getUser() != null) {
                        if (posProfileWrapper.getUser().isAssignVendor()) {
                            if (AppPrefs.getVendorID(context) !=-1) {
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();

                                if (title.equals(getResources().getString(R.string.title_activity_main))) {
                                    replaceFragment(MainFragmentCategories.MAIN_FRAGMENT.getIndex(), false);
                                } else if (title.equals(getResources().getString(R.string.navigation_drawer_orders)) || title.equals(getResources().getString(R.string.navigation_drawer_pos_orders))) {
                                    // startActivity(new Intent(MainActivity.this,BarCodeActivity.class));
                                    //
                                    replaceFragment(MainFragmentCategories.PURCHASE_HISTORY.getIndex(), false);
                                } else if (title.equals(getResources().getString(R.string.navigation_drawer_cart))) {
                                    replaceFragment(MainFragmentCategories.PURCHASE.getIndex(), false);
                                } else if (title.equals(getResources().getString(R.string.navigation_drawer_communication))) {
                                    replaceFragment(MainFragmentCategories.SUPPORT_TICKETS.getIndex(), false);
                                } else if (title.equals(getResources().getString(R.string.navigation_drawer_survey))) {
                                    replaceFragment(MainFragmentCategories.SURVEY.getIndex(), false);
                                } else if (title.equals(getResources().getString(R.string.add_pos_tab_text))) {
                                    replaceFragment(MainFragmentCategories.ADDPOS.getIndex(), false);
                                } else if (title.equals(getResources().getString(R.string.button_label_pos_location))) {
                                    replaceFragment(MainFragmentCategories.SALESMAN.getIndex(), false);
                                } else if (title.equals(getString(R.string.ship_orders))) {
                                    replaceFragment(MainFragmentCategories.SHIP_ORDERS.getIndex(), false);
                                } else if (title.equals(getString(R.string.delivery_orders))) {
                                    replaceFragment(MainFragmentCategories.DELIVERY_ORDERS.getIndex(), false);
                                }
                            } else {
                                SelectVendorDialog selectVendorDialog = new SelectVendorDialog();
                                selectVendorDialog.show(getSupportFragmentManager(), "");
                                return false;
                            }
                        } else {
                            SelectVendorDialog selectVendorDialog = new SelectVendorDialog();
                            selectVendorDialog.show(getSupportFragmentManager(), "");
                            return false;
                        }
                    }
                }

                return true;
            }

        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public void performNoBackStackTransaction(final FragmentManager fragmentManager2, final String tag, final Fragment fragment) {
        final int newBackStackLength = fragmentManager2.getBackStackEntryCount() + 1;

        fragmentManager2.beginTransaction()
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit();


        fragmentManager2.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int nowCount = fragmentManager2.getBackStackEntryCount();
                if (newBackStackLength != nowCount) {
                    fragmentManager2.removeOnBackStackChangedListener(this);

                    if (newBackStackLength > nowCount) {

                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        for (int i = 0; i < bottomNavigation.getItemsCount(); i++) {
            if (textViewTitle.getText().toString().equals(bottomNavigation.getItem(i).getTitle(context))) {
                bottomNavigation.setCurrentItem(i, false);

            } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                finish();
            }
        }
    }
}
