package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;

/**
 * Created by rua on 10-Apr-19.
 */

public class PromotionType implements Serializable {

    Long id ;
    Boolean deleted ;
    Boolean inactive ;
    String code ;
    String type ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
