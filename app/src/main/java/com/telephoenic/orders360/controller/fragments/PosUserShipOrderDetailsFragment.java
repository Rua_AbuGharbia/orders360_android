package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.DateUtil;
import com.telephoenic.orders360.view.adapters.OrderDeliveryDetailsAdapter;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PosUserShipOrderDetailsFragment extends BlankFragment implements OnRecyclerClick {

    public static final String ORDER_ID = "id";

    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    private ShipModel shipModel;
    private ShipViewModel shipViewModel;
    private String TAG = PosUserShipOrderDetailsFragment.class.getName();
    private List<ShipItemsModel> shipItemsModelList;
    private OrderDeliveryDetailsAdapter adapter;
    private Long orderId;


    public PosUserShipOrderDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pos_user_ship_order_details_fragment, container, false);
        ButterKnife.bind(this, view);
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        if (getArguments() != null) {
            orderId =  getArguments().getLong(ORDER_ID);
        }
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {
        shipItemsModelList = new ArrayList<>();
        adapter = new OrderDeliveryDetailsAdapter(getContext(), this, shipItemsModelList);
        recyclerView.setAdapter(adapter);

        getShipItem(orderId);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.ship_orders));
        createComponents(view);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && !isInternetConnection) {
            this.onViewCreated(getView(), new Bundle());
        }
    }


    @Override
    public void getPosition(int position) {

    }

    private void getShipItem(Long id) {
        paginationLoadingView.setVisibility(View.VISIBLE);
        shipViewModel.getShipItemOrder(id).observe(this, new ApiObserver<List<ShipItemsModel>>() {
            @Override
            protected void noConnection() {
                paginationLoadingView.setVisibility(View.GONE);
                // noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<ShipItemsModel> data) {
                paginationLoadingView.setVisibility(View.GONE);
                shipItemsModelList.addAll(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            protected void onError(String error) {
                paginationLoadingView.setVisibility(View.GONE);
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        });
    }
}
