package com.telephoenic.orders360.controller.interfaces;

import com.telephoenic.orders360.controller.server.model.shiporder.ChequePaymentsModel;

public interface GetChequeData {
    void getChequeData(ChequePaymentsModel chequePayments);
}
