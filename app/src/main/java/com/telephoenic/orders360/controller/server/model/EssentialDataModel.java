package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rua on 05-May-19.
 */

public class EssentialDataModel implements Serializable {

    @JsonProperty("vendors")
    List<VendorWrapper> vendors ;

    @JsonProperty("images")
    List<String> images ;

  /*  @JsonProperty("carts")
    List<String> carts ; // TODO change type of the List*/

    public List<VendorWrapper> getVendors() {
        return vendors;
    }

    public void setVendors(List<VendorWrapper> vendors) {
        this.vendors = vendors;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

  /*  public List<String> getCarts() {
        return carts;
    }

    public void setCarts(List<String> carts) {
        this.carts = carts;
    }*/
}
