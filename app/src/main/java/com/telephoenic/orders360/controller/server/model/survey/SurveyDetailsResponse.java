package com.telephoenic.orders360.controller.server.model.survey;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class SurveyDetailsResponse implements Serializable {

	@JsonProperty("status")
	private String status;

	@JsonProperty("questions")
	private List<SurveyQuestionWrapper> questions;

	/*@JsonProperty("radioButtonList")
	private List<SurveyQuestionWrapper> radioButtonList;

	@JsonProperty("chechBoxList")
	private List<SurveyQuestionWrapper> checkBoxList;

	@JsonProperty("textAreatList")
	private List<SurveyQuestionWrapper> textAreaList;*/

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SurveyQuestionWrapper> getQuestions() {
		return questions;
	}

	public void setQuestions(List<SurveyQuestionWrapper> questions) {
		this.questions = questions;
	}
}
