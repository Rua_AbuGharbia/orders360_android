package com.telephoenic.orders360.controller.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.telephoenic.orders360.controller.constants.AppConstants.URL_PRIVACY_AR;
import static com.telephoenic.orders360.controller.constants.AppConstants.URL_PRIVACY_EN;


public class PrivacyPolicyFragment extends BlankFragment {

    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    @BindView(R.id.privacy_TextView)
    TextView privacyTextView;
    private String TAG = PrivacyPolicyFragment.class.getName();
    private WebView webView;

    public PrivacyPolicyFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//     AppUtils.changeTitle(Objects.requireNonNull(getActivity()), getString(R.string.navigation_drawer_privacy_provisions));
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_privacy_provisions));
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void createComponents(final View v) {

       // privacyTextView.setMovementMethod(new ScrollingMovementMethod());
        paginationLoadingView.setVisibility(View.VISIBLE);
        webView = v.findViewById(R.id.webView);
        if (SharedPreferenceManager.getLanguage(getActivity()).equals("en")) {
            webView.loadUrl(URL_PRIVACY_EN);
            webView.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        } else {
            webView.loadUrl(URL_PRIVACY_AR);
            webView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        }
        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                paginationLoadingView.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
        webView.restoreState(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LanguageManager.setLanguage(getActivity());
    }
}
