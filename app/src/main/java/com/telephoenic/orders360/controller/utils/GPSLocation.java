package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.view.customcontrols.CustomDialog;

import java.util.List;
import java.util.Locale;

/**
 * GPS Location
 * 
 * @author Abd Arahman Shoman
 * @since October 3 2013
 * @version 1.0.0
 * 
 */
public class GPSLocation {

	private static LocationManager locationManager;

	/**
	 * Get Location
	 *
	 * @param Context context
	 * @return Location location
	 */
	public static Location getLocation(Context context) {
		Location location = null;
		if (isGPSAvilable(context)) {
			locationManager = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);

			Criteria criteria = new Criteria();
			String provider = locationManager.getBestProvider(criteria, false);
			location = locationManager.getLastKnownLocation(provider);
		}
		return location;
	}

	public static boolean isGPSAvilable(final Context context) {
		PackageManager packageManager = context.getPackageManager();
		boolean hasGpsFeature = packageManager
				.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
		if (hasGpsFeature) {
			LocationManager manager = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
			boolean enabledGps = manager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			return enabledGps;
		} else {
			return false;
		}
	}

	public static Location getLastKnownLocation(Context context) {
		locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		List<String> providers = locationManager.getProviders(true);
		Location bestLocation = null;
		for (String provider : providers) {
			Location l = locationManager.getLastKnownLocation(provider);
			Log.d("last known location, provider: %s, location: %s", provider);

			if (l == null) {
				continue;
			}
			if (bestLocation == null
					|| l.getAccuracy() < bestLocation.getAccuracy()) {
				Log.d("found best last known location: %s", l.getProvider());
				bestLocation = l;
			}
		}
		if (bestLocation == null) {
			return null;
		}
		return bestLocation;
	}


	public static Address getAddress(double lat, double lng , Context context) {
		Address address = null;
		try{
			Geocoder geocoder = new Geocoder(context, Locale.getDefault());
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			address = addresses.get(0);
			//city = addresses.get(0).getAdminArea();
			//area = addresses.get(0).getFeatureName();
			//addresses.get(0).get
		}catch (Exception ex){

		}
		return address;
	}

	public static void displayDialog(final Context context) {
		final CustomDialog dialogGPS = new CustomDialog(context);
		dialogGPS.setTitle(context.getResources().getString(R.string.titles_dialog_gps_enabling));
		dialogGPS.setMessage(context.getResources().getString(R.string.messages_warning_dialog_gps_enabling));
		dialogGPS.show();

		dialogGPS.getOkButton().setText(
				context.getResources().getString(R.string.button_labels_enableGPS));
		dialogGPS.getOkButton().setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// Launch settings, allowing user to make a change
				Intent intent = new Intent(
						Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				context.startActivity(intent);
				//startActivityForResult(intent, AppConstants.OPEN_GPS_SETTINGS_REQUEST);
				dialogGPS.dismiss();
			}
		});
	}

	private static void showErrorDialog(String errorMessge , Context context) {
		final CustomDialog errorDialog = new CustomDialog(context);
		errorDialog.setTitle(context.getResources().getString(R.string.titles_dialog_general_error));
		errorDialog.setMessage(errorMessge);
		errorDialog.show();
		errorDialog.getOkButton().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				errorDialog.dismiss();
			}
		});
	}

	public static  boolean checkGPS(Context context) {
		//GPSTracker gpsTracker = null;

		if (!GPSLocation.isGPSAvilable(context)) {
			displayDialog(context);
			return false;
		}

		/*gpsTracker = new GPSTracker(context);
		if (!gpsTracker.canGetLocation() || gpsTracker.getLocation() == null) {
			showErrorDialog(context.getResources().getString(R.string.messages_gps_not_allow_null),context);
			return false;
		}

		double latitude = gpsTracker.getLatitude();
		double longitude = gpsTracker.getLongitude();
		Address address = getAddress(latitude, longitude,context);

		if(address == null) {
			Toast.makeText(context,context.getResources().getString(R.string.messages_gps_not_allow_null), Toast.LENGTH_LONG).show();
			return false;
		}*/

		return  true;

	}

	public static boolean isGPSLocationEmpty(String countryCode) {
		if(countryCode == null || countryCode.isEmpty()) {
			return true;
		}
		return false;
	}



}