package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Guideline;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.basemodel.typedef.PaymentMethodType;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.interfaces.GetAmount;
import com.telephoenic.orders360.controller.interfaces.GetChequeData;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.SingleItem;
import com.telephoenic.orders360.controller.server.model.shiporder.CashPayments;
import com.telephoenic.orders360.controller.server.model.shiporder.ChequePaymentsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ClaimPayments;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentMethodModel;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentResponse;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipmentArrival;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.adapters.PaymentMethodAdapter;
import com.telephoenic.orders360.view.customcontrols.CashPaymentDialog;
import com.telephoenic.orders360.view.customcontrols.ChequePaymentDialog;
import com.telephoenic.orders360.view.customcontrols.ClaimPaymentDialog;
import com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentFragment extends BlankFragment implements OnRecyclerClick, GetChequeData {
    public static String ARRIVAL_DATA = "data";
    private static int NO_SELECT_ITEM = -1;
    public static String AMOUNT = " amount";
    public static final String INVOICE_NUMBER = "invoice_number";

    @BindView(R.id.Invoice_TextView)
    TextView invoiceTextView;
    @BindView(R.id.TotalAmount_TextView)
    TextView totalAmountTextView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.RemainingAmount_TextView)
    TextView remainingAmountTextView;
    @BindView(R.id.AddPaymentMethod_TextView)
    TextView addPaymentMethodButton;
    @BindView(R.id.CreditCard_RadioButton)
    RadioButton creditCardRadioButton;
    @BindView(R.id.Cash_RadioButton)
    RadioButton cashRadioButton;
    @BindView(R.id.Cheque_RadioButton)
    RadioButton chequeRadioButton;
    @BindView(R.id.Claim_RadioButton)
    RadioButton claimRadioButton;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.Cash__ImageView)
    ImageView cashImage;
    @BindView(R.id.Claim_ImageView)
    ImageView claimImage;
    @BindView(R.id.Cheque_ImageView)
    ImageView chequeImage;
    private PaymentMethodAdapter adapter;
    private @PaymentMethodType
    int paymentMethodType;
    private List<Object> paymentMethodModelList;
    private ShipmentArrival shipmentArrival = new ShipmentArrival();
    private List<PaymentMethodModel> paymentMethodModels = new ArrayList<>();
    private ShipViewModel shipViewModel;
    private ClaimPayments claimPayments = new ClaimPayments();
    private CashPayments cashPayments = new CashPayments();
    private ChequePaymentsModel chequePaymentsModel = new ChequePaymentsModel();
    private int editPosition = -NO_SELECT_ITEM;
    private boolean cashAdded = false;
    private double amount;
    private double remainingAmount;
    private int deletePosition = NO_SELECT_ITEM;
    private ClaimPayments editClaimPayments;
    private ChequePaymentsModel editChequePaymentsModel;
    private Double editAmount;
    private Long invoiceNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_fragment, container, false);
        ButterKnife.bind(this, view);
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        if (getArguments() != null) {
            amount = getArguments().getDouble(AMOUNT);
            invoiceNumber = getArguments().getLong(INVOICE_NUMBER);
            shipmentArrival = (ShipmentArrival) getArguments().getSerializable(ARRIVAL_DATA);
        }

        return view;
    }

    @SuppressLint("SetTextI18n")
    private void inti() {
        cashAdded = false;
        remainingAmount = amount;
        invoiceTextView.setText(getString(R.string.invoice_no) + invoiceNumber);
        totalAmountTextView.setText( getString(R.string.fragment_purchase_history_title_total_amount)+ " : " + String.format(Locale.ENGLISH, "%.2f", amount) + " " + AppConstants.CURRENCY);
        remainingAmountTextView.setText(getString(R.string.remaining_amount)+" : " + String.format(Locale.ENGLISH, "%.2f", remainingAmount) + " " + AppConstants.CURRENCY);
        getPaymentMethod();
        paymentMethodModelList = new ArrayList<>();
        if (AppUtils.paymentMethodModelList != null) {
            paymentMethodModelList = AppUtils.paymentMethodModelList;
            remainingAmount = 0;
            remainingAmountTextView.setText(getString(R.string.remaining_amount)+" : "+ String.format(Locale.ENGLISH, "%.2f", remainingAmount) + " " + AppConstants.CURRENCY);
        }
        paymentMethodType = -1;
        adapter = new PaymentMethodAdapter(getContext(), paymentMethodModelList, this, deletePayment);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);
        radioGroup.clearCheck();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Cash_RadioButton:
                        paymentMethodType = PaymentMethodType.CASH;
                        break;
                    case R.id.Claim_RadioButton:
                        paymentMethodType = PaymentMethodType.CLAIM;
                        break;
                    case R.id.CreditCard_RadioButton:
                        paymentMethodType = PaymentMethodType.CREDIT_CARD;
                        break;
                    case R.id.Cheque_RadioButton:
                        paymentMethodType = PaymentMethodType.CHEQUE;
                        break;
                    default:
                        paymentMethodType = -1;
                        break;
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), "Payment");
        inti();
    }


    @OnClick({R.id.AddPaymentMethod_TextView, R.id.AddPayment_ImageView})
    public void onViewClicked() {
        if (remainingAmount != 0) {
            switch (paymentMethodType) {
                case PaymentMethodType.CASH:
                    if (!cashAdded) {
                        editPosition = NO_SELECT_ITEM;
                        addCashPaymentDialog();
                    } else {
                        Toast.makeText(getContext(), "You Add Cash Payment", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case PaymentMethodType.CLAIM:
                    editPosition = NO_SELECT_ITEM;
                    addClaimPaymentDialog();
                    break;
                case PaymentMethodType.CHEQUE:
                    editPosition = NO_SELECT_ITEM;
                    Bundle bundle = new Bundle();
                    bundle.putDouble(ChequePaymentDialog.REMAINING_AMOUNT, remainingAmount);
                    ChequePaymentDialog chequePaymentDialog = new ChequePaymentDialog(this);
                    chequePaymentDialog.setArguments(bundle);
                    chequePaymentDialog.show(getFragmentManager(), "");
                    break;
                case PaymentMethodType.CREDIT_CARD:
                    break;
                default:
                    Toast.makeText(getContext(), getString(R.string.please_select_payment_method), Toast.LENGTH_SHORT).show();
                    break;

            }
        } else {
            Toast.makeText(getContext(), getString(R.string.you_can_add_an_amount_you_are_already_paid_the_total_amount), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.Save_Button)
    public void onViewClickedSave() {
        if (remainingAmount == 0) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(ConfirmDeliveryFragment.PAYMENT_DATA, (Serializable) paymentMethodModelList);
            bundle.putSerializable(ConfirmDeliveryFragment.SHIPMENT_ARRIVAL, shipmentArrival);
            bundle.putDouble(ConfirmDeliveryFragment.AMOUNT, amount);
            AppUtils.paymentMethodModelList = paymentMethodModelList;
            gotoFragment(new ConfirmDeliveryFragment(), getFragmentManager(), bundle);
        } else {
            Toast.makeText(getContext(), "Please Add More Payment Method", Toast.LENGTH_SHORT).show();
        }
    }

    private void addCashPaymentDialog() {
        Bundle bundle = new Bundle();
        bundle.putDouble(CashPaymentDialog.REMAINING_AMOUNT, remainingAmount);
        CashPaymentDialog cashPaymentDialog = new CashPaymentDialog(getCashAmount);
        cashPaymentDialog.setArguments(bundle);
        cashPaymentDialog.show(getFragmentManager(), "");
    }

    private void addClaimPaymentDialog() {
        Bundle bundle = new Bundle();
        bundle.putDouble(ClaimPaymentDialog.REMAINING_AMOUNT, remainingAmount);
        ClaimPaymentDialog claimPaymentDialog = new ClaimPaymentDialog(getClaimAmount);
        claimPaymentDialog.setArguments(bundle);
        claimPaymentDialog.show(getFragmentManager(), "");
    }

    private GetAmount getClaimAmount = new GetAmount() {
        @SuppressLint("SetTextI18n")
        @Override
        public void getAmount(Double amount) {
            if (amount <= remainingAmount) {
                remainingAmount = remainingAmount - amount;
                ClaimPayments claimPayments1 = new ClaimPayments(claimPayments.getPaymentMethod(), amount);
                claimPayments.setAmount(amount);
                if (editPosition == NO_SELECT_ITEM) {
                    paymentMethodModelList.add(claimPayments1);
                } else {
                    ClaimPayments claimPayments2 = (ClaimPayments) paymentMethodModelList.get(editPosition);
                    remainingAmount = remainingAmount + claimPayments2.getAmount();
                    paymentMethodModelList.set(editPosition, claimPayments1);
                }
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText("Remaining Amount : " + remainingAmount + " " + AppConstants.CURRENCY);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(paymentMethodModelList.size() - 1);
                setNestedScrollView();
            } else {
                Toast.makeText(getContext(), "payment amount greater than the remaining amount ", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private GetAmount getCashAmount = new GetAmount() {
        @SuppressLint("SetTextI18n")
        @Override
        public void getAmount(Double amount) {
            if (amount <= remainingAmount) {
                remainingAmount = remainingAmount - amount;
                cashAdded = true;
                CashPayments cashPayments1 = new CashPayments(cashPayments.getPaymentMethod(), amount);
                cashPayments.setAmount(amount);

                if (editPosition == NO_SELECT_ITEM) {
                    paymentMethodModelList.add(cashPayments1);
                } else {
                    CashPayments cashPayments2 = (CashPayments) paymentMethodModelList.get(editPosition);
                    remainingAmount = remainingAmount + cashPayments2.getAmount();
                    paymentMethodModelList.set(editPosition, cashPayments1);
                }
                Log.e("getAmount: ", remainingAmount + "");
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                Log.e("getAmount: ", remainingAmount + "");
                remainingAmountTextView.setText("Remaining Amount : " + String.format(Locale.ENGLISH, "%.2f", remainingAmount) + " " + AppConstants.CURRENCY);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(paymentMethodModelList.size() - 1);
                setNestedScrollView();
            } else {
                cashAdded = false;
                Toast.makeText(getContext(), "payment amount greater than the remaining amount ", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void getPaymentMethod() {
        LoadingDialog.showDialog(getContext());

        shipViewModel.getPaymentMethod().observe(this, new ApiObserver<PaymentResponse>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();

            }

            @Override
            protected void onSuccess(PaymentResponse data) {
                LoadingDialog.dismiss();
                paymentMethodModels.addAll(data.getContent());
                setPaymentMethod(paymentMethodModels);
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
            }
        });
    }

    private void setPaymentMethod(List<PaymentMethodModel> paymentMethod) {
        if (paymentMethod != null && paymentMethod.size() > 0) {
            for (int i = 0; i < paymentMethod.size(); i++) {
                switch (paymentMethod.get(i).getCode()) {
                    case AppConstants.CASH_CODE:
                        cashImage.setVisibility(View.VISIBLE);
                        cashRadioButton.setVisibility(View.VISIBLE);
                        cashPayments = new CashPayments();
                        cashPayments.setPaymentMethod(paymentMethod.get(i));
                        break;
                    case AppConstants.CLAIM_CODE:
                        claimImage.setVisibility(View.VISIBLE);
                        claimRadioButton.setVisibility(View.VISIBLE);
                        claimPayments = new ClaimPayments();
                        claimPayments.setPaymentMethod(paymentMethod.get(i));

                        break;
                    case AppConstants.CHEQUE_CODE:
                        chequeImage.setVisibility(View.VISIBLE);
                        chequeRadioButton.setVisibility(View.VISIBLE);
                        chequePaymentsModel = new ChequePaymentsModel();
                        chequePaymentsModel.setPaymentMethod(paymentMethod.get(i));
                        break;
                }
            }
        }
    }

    private void setNestedScrollView() {
        nestedScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final int scrollViewHeight = nestedScrollView.getHeight();
                if (scrollViewHeight > 0) {
                    nestedScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    final View lastView = nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);
                    final int lastViewBottom = lastView.getBottom() + nestedScrollView.getPaddingBottom();
                    final int deltaScrollY = lastViewBottom - scrollViewHeight - nestedScrollView.getScrollY();
                    /* If you want to see the scroll animation, call this. */
                    nestedScrollView.smoothScrollBy(0, deltaScrollY);
                    /* If you don't want, call this. */
                    // nestedScrollView.scrollBy(0, deltaScrollY);
                }
            }
        });
    }

    @Override
    public void getPosition(int position) {
        editPosition = position;
        Bundle bundle = new Bundle();
        bundle.putDouble(CashPaymentDialog.REMAINING_AMOUNT, remainingAmount);
        if (paymentMethodModelList.get(position) instanceof CashPayments) {
            CashPayments payments = (CashPayments) paymentMethodModelList.get(position);
            editAmount = payments.getAmount();
            bundle.putSerializable(CashPaymentDialog.DATA, payments);
            CashPaymentDialog cashPaymentDialog = new CashPaymentDialog(upDateCashAmount);
            cashPaymentDialog.setArguments(bundle);
            cashPaymentDialog.show(getFragmentManager(), "");
        } else if (paymentMethodModelList.get(position) instanceof ClaimPayments) {
            editClaimPayments = new ClaimPayments();
            editClaimPayments = (ClaimPayments) paymentMethodModelList.get(position);
            bundle.putSerializable(ClaimPaymentDialog.DATA, claimPayments);
            ClaimPaymentDialog claimPaymentDialog = new ClaimPaymentDialog(upDateClaimAmount);
            claimPaymentDialog.setArguments(bundle);
            claimPaymentDialog.show(getFragmentManager(), "");
        } else {
            editChequePaymentsModel = new ChequePaymentsModel();
            editChequePaymentsModel = (ChequePaymentsModel) paymentMethodModelList.get(position);
            chequePaymentsModel = (ChequePaymentsModel) paymentMethodModelList.get(position);
            editAmount = editChequePaymentsModel.getAmount();
            bundle.putSerializable(ChequePaymentDialog.DATA, chequePaymentsModel);
            ChequePaymentDialog chequePaymentDialog = new ChequePaymentDialog(upDateChequeData);
            chequePaymentDialog.setArguments(bundle);
            chequePaymentDialog.show(getFragmentManager(), "");
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void getChequeData(ChequePaymentsModel chequePayments) {
        if (chequePayments.getAmount() <= remainingAmount) {
            remainingAmount = remainingAmount - chequePayments.getAmount();
            chequePayments.setPaymentMethod(chequePaymentsModel.getPaymentMethod());
            if (editPosition == NO_SELECT_ITEM) {
                paymentMethodModelList.add(chequePayments);
            } else {
                ChequePaymentsModel chequePaymentsModel1 = (ChequePaymentsModel) paymentMethodModelList.get(editPosition);
                remainingAmount = remainingAmount + chequePaymentsModel1.getAmount();
                paymentMethodModelList.set(editPosition, chequePayments);
            }
            remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
            remainingAmountTextView.setText("Remaining Amount : " + String.format(Locale.ENGLISH, "%.2f", remainingAmount) + " " + AppConstants.CURRENCY);
            adapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(paymentMethodModelList.size() - 1);
            setNestedScrollView();
        } else {
            Toast.makeText(getContext(), "payment amount greater than the remaining amount ", Toast.LENGTH_SHORT).show();
        }

    }

    private OnRecyclerClick deletePayment = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            deletePosition = position;
            confirmDelete();
        }
    };

    private void confirmDelete() {
        Bundle bundle = new Bundle();
        String msg = "Are you sure you want to remove payment method?";
//        confirmDeleteDialog = new CustomAlertDialog(getActivity(), R.string.btn_confirm, msg, R.string.btn_cancel, R.string.ok, onDeleteDialogClicked);
//        confirmDeleteDialog.show();
        ConfirmOrderDialog confirmOrderDialog = new ConfirmOrderDialog();
        bundle.putString(ConfirmOrderDialog.MASSAGE, msg);
        confirmOrderDialog.setArguments(bundle);
        confirmOrderDialog.onClickStatus(clickStatus);
        confirmOrderDialog.show(getFragmentManager(), "");
    }

    private ConfirmOrderDialog.onClickStatus clickStatus = new ConfirmOrderDialog.onClickStatus() {
        @Override
        public void getStatusClick(int Status) {
            switch (Status) {
                case ConfirmOrderDialog.StatusClick.CANCEL:
                    break;
                case ConfirmOrderDialog.StatusClick.OKAY:
                    if (paymentMethodModelList.get(deletePosition) instanceof CashPayments) {
                        cashAdded = false;
                        CashPayments payments = (CashPayments) paymentMethodModelList.get(deletePosition);
                        remainingAmount = remainingAmount + payments.getAmount();
                        remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));

                    } else if (paymentMethodModelList.get(deletePosition) instanceof ClaimPayments) {
                        ClaimPayments claimPayments = (ClaimPayments) paymentMethodModelList.get(deletePosition);
                        remainingAmount = remainingAmount + claimPayments.getAmount();
                        remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));

                    } else {
                        ChequePaymentsModel chequePaymentsModel = (ChequePaymentsModel) paymentMethodModelList.get(deletePosition);
                        remainingAmount = remainingAmount + chequePaymentsModel.getAmount();
                        remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));

                    }

                    remainingAmountTextView.setText(String.format("Remaining Amount : %s %s", String.format(Locale.ENGLISH, "%.2f", remainingAmount), AppConstants.CURRENCY));
                    paymentMethodModelList.remove(deletePosition);
                    adapter.notifyDataSetChanged();

                    break;

            }
        }
    };

    private GetAmount upDateClaimAmount = new GetAmount() {
        @SuppressLint("SetTextI18n")
        @Override
        public void getAmount(Double amount) {
            // ClaimPayments claimPayments2 = (ClaimPayments) paymentMethodModelList.get(editPosition);
            remainingAmount = remainingAmount + editClaimPayments.getAmount();

            if (amount <= remainingAmount) {
                remainingAmount = remainingAmount - amount;
                ClaimPayments claimPayments1 = new ClaimPayments(claimPayments.getPaymentMethod(), amount);
                claimPayments.setAmount(amount);
                paymentMethodModelList.set(editPosition, claimPayments1);
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText("Remaining Amount : " + remainingAmount + " " + AppConstants.CURRENCY);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(paymentMethodModelList.size() - 1);
                setNestedScrollView();
            } else {
                remainingAmount = remainingAmount - editClaimPayments.getAmount();
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText("Remaining Amount : " + remainingAmount + " " + AppConstants.CURRENCY);
                Toast.makeText(getContext(), "payment amount greater than the remaining amount ", Toast.LENGTH_SHORT).show();
            }

        }
    };
    private GetChequeData upDateChequeData = new GetChequeData() {
        @Override
        public void getChequeData(ChequePaymentsModel chequePayments) {

            remainingAmount = remainingAmount + editAmount;

            if (chequePayments.getAmount() <= remainingAmount) {
                remainingAmount = remainingAmount - chequePayments.getAmount();
                chequePayments.setPaymentMethod(chequePaymentsModel.getPaymentMethod());
                paymentMethodModelList.set(editPosition, chequePayments);
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText(String.format("Remaining Amount : %s %s", remainingAmount, AppConstants.CURRENCY));
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(paymentMethodModelList.size() - 1);
                setNestedScrollView();
            } else {
                remainingAmount = remainingAmount - editAmount;
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText(String.format("Remaining Amount : %s %s", remainingAmount, AppConstants.CURRENCY));
                Toast.makeText(getContext(), "payment amount greater than the remaining amount ", Toast.LENGTH_SHORT).show();
            }

        }
    };

    private GetAmount upDateCashAmount = new GetAmount() {
        @SuppressLint("SetTextI18n")
        @Override
        public void getAmount(Double amount) {

            remainingAmount = remainingAmount + editAmount;

            if (amount <= remainingAmount) {
                remainingAmount = remainingAmount - amount;
                CashPayments cashPayments1 = new CashPayments(cashPayments.getPaymentMethod(), amount);
                cashPayments.setAmount(amount);
                paymentMethodModelList.set(editPosition, cashPayments1);
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText("Remaining Amount : " + remainingAmount + " " + AppConstants.CURRENCY);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(paymentMethodModelList.size() - 1);
                setNestedScrollView();
            } else {
                remainingAmount = remainingAmount - editAmount;
                remainingAmount = Double.valueOf((String.format(Locale.ENGLISH, "%.2f", remainingAmount)));
                remainingAmountTextView.setText("Remaining Amount : " + remainingAmount + " " + AppConstants.CURRENCY);
                Toast.makeText(getContext(), "payment amount greater than the remaining amount ", Toast.LENGTH_SHORT).show();
            }

        }
    };
}
