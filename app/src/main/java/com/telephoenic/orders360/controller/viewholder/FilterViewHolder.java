package com.telephoenic.orders360.controller.viewholder;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Name_RowFilter_TextView)
    public TextView nameRowFilterTextView;
    @BindView(R.id.Line_View)
    public View lineView;
    @BindView(R.id.RecyclerView)
    public RecyclerView recyclerView;
    @BindView(R.id.ChangeBackGround_ConstraintLayout)
    public ConstraintLayout changeBackGroundConstraintLayout;

    public FilterViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

//    public void Expand() {
//        if (imageRotationImageView.getVisibility() == View.GONE) {
//            imageRotationImageView.setVisibility(View.VISIBLE);
//        }
//        imageRotationImageView.setRotation(90);
//        //recyclerView.setVisibility(View.VISIBLE);
//    }
//
//    public void Reverse() {
//        if (imageRotationImageView.getVisibility() == View.GONE) {
//            imageRotationImageView.setVisibility(View.VISIBLE);
//        }
//        imageRotationImageView.setRotation(270);
//        // recyclerView.setVisibility(View.GONE);
//    }
}
