package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;

import java.util.Locale;

public class LanguageUtils {


    public static void setLanguage(Context context, String language, boolean restart) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.setLocale(locale);
        context.getResources().updateConfiguration(configuration,
                context.getResources().getDisplayMetrics());

        if (restart) {
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            context.startActivity(intent);
        }
    }
}
