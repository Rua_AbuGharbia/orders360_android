package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.basemodel.typedef.SerialType;
import com.telephoenic.orders360.controller.activities.BarCodeActivity;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.GetSerialsData;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.AllSerialModel;
import com.telephoenic.orders360.controller.server.model.shiporder.FromToSerialModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SerialsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SerialsQuantity;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SingleSerialModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.adapters.SerialAdapter;
import com.telephoenic.orders360.view.customcontrols.SerialsDialog;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class ScanItemSerialsFragment extends BlankFragment implements OnRecyclerClick, GetSerialsData {

    public static final String SERIALS = "serials";
    public static final String ITEM_ID = "item_id";

    @BindView(R.id.OrderName_RowMyOrder_TextView)
    TextView OrderNameRowMyOrderTextView;
    @BindView(R.id.Quantity_RowMyOrder_TextView)
    TextView QuantityRowMyOrderTextView;
    @BindView(R.id.EnteredSerials_RowMyOrder_TextView)
    TextView enteredSerialsRowMyOrderTextView;
    @BindView(R.id.SingleSerial_RadioButton)
    RadioButton singleSerialRadioButton;
    @BindView(R.id.FromToSerial_RadioButton)
    RadioButton fromToSerialRadioButton;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.Quntity_RadioButton)
    RadioButton quntityRadioButton;
    @BindView(R.id.radioGroup2)
    RadioGroup radioGroup2;
    private SerialAdapter adapter;
    private List<AllSerialModel> allSerialModels;
    private int currantPosition = -1;
    private List<SerialsModel> serials;
    private ShipItemsModel shipItemsModel;
    private Integer shipmentQuantity = 0;
    private Integer serialsQuantity = -1;
    private Boolean completeSerials = true;

    private List<SingleSerialModel> singleSerialList;
    private List<FromToSerialModel> rangeSerialList;
    private ShipViewModel shipViewModel;
    private long itemId = 0L;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scan_item_serials_fragment, container, false);
        ButterKnife.bind(this, view);
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        allSerialModels = new ArrayList<>();
        if (getArguments() != null) {
            shipItemsModel = (ShipItemsModel) getArguments().getSerializable(SERIALS);
            if (shipItemsModel != null) {
                itemId = shipItemsModel.getId();
                serials = shipItemsModel.getSerials();
                OrderNameRowMyOrderTextView.setText(shipItemsModel.getItemName());
                shipmentQuantity = shipItemsModel.getShipmentQuantity();
                serialsQuantity = shipItemsModel.getSerialsQuantity();

                if (shipmentQuantity.equals(serialsQuantity)) {
                    quntityRadioButton.setClickable(false);
                    fromToSerialRadioButton.setClickable(false);
                    singleSerialRadioButton.setClickable(false);
                }

                QuantityRowMyOrderTextView.setText(String.format("%d", shipItemsModel.getShipmentQuantity()));
                enteredSerialsRowMyOrderTextView.setText(String.format("%d", shipItemsModel.getSerialsQuantity()));
            }
        }
        if (serials != null) {
            for (int i = 0; i < serials.size(); i++) {
                if (serials.get(i).getQuantity() != null && serials.get(i).getQuantity() != 0) {
                    allSerialModels.add(new AllSerialModel(serials.get(i).getQuantity() + "", SerialType.QUANTITY));
                    quntityRadioButton.setChecked(true);
                }
                if (serials.get(i).getSerial() != null && !TextUtils.isEmpty(serials.get(i).getSerial())) {
                    allSerialModels.add(new AllSerialModel(serials.get(i).getSerial() + "", SerialType.SINGLE));
                    singleSerialRadioButton.setChecked(true);
                }
                if (serials.get(i).getFromSerial() != null && !TextUtils.isEmpty(serials.get(i).getFromSerial())) {
                    allSerialModels.add(new AllSerialModel(serials.get(i).getFromSerial() + "", serials.get(i).getToSerial(), SerialType.FROM_TO));
                    fromToSerialRadioButton.setChecked(true);
                }
            }

        }
        adapter = new SerialAdapter(getContext(), allSerialModels, this, onRemoveItem, getSerial, getRangeSerial);
        recyclerView.setAdapter(adapter);

        radioGroup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), getString(R.string.please_remove_the_enterd_serials_to_change_your), Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), "Scan Item Serials");
    }


    @OnClick(R.id.AddSerial_ImageView)
    public void onViewClicked() {
        List<AllSerialModel> showListSerials = new ArrayList<>();
        showListSerials = allSerialModels;
        Bundle bundle;
        if (serialsQuantity < shipmentQuantity) {
            if (singleSerialRadioButton.isChecked()) {
                bundle = new Bundle();
                bundle.putSerializable(SerialsDialog.DATA, (Serializable) showListSerials);
                bundle.putInt(SerialsDialog.SERIAL_TYPE, SerialType.SINGLE);
                bundle.putInt(SerialsDialog.QUANTITY, shipmentQuantity);
                SerialsDialog serialsDialog = new SerialsDialog(this);
                serialsDialog.setArguments(bundle);
                serialsDialog.show(getFragmentManager(), "");

            } else if (fromToSerialRadioButton.isChecked()) {
                bundle = new Bundle();
                bundle.putSerializable(SerialsDialog.DATA, (Serializable) showListSerials);
                bundle.putInt(SerialsDialog.SERIAL_TYPE, SerialType.FROM_TO);
                bundle.putInt(SerialsDialog.QUANTITY, shipmentQuantity);
                SerialsDialog serialsDialog = new SerialsDialog(this);
                serialsDialog.setArguments(bundle);
                serialsDialog.show(getFragmentManager(), "");

            } else if (quntityRadioButton.isChecked() && allSerialModels.size() == 0) {
                bundle = new Bundle();
                bundle.putSerializable(SerialsDialog.DATA, (Serializable) showListSerials);
                bundle.putInt(SerialsDialog.SERIAL_TYPE, SerialType.QUANTITY);
                bundle.putInt(SerialsDialog.QUANTITY, shipmentQuantity);
                SerialsDialog serialsDialog = new SerialsDialog(this);
                serialsDialog.setArguments(bundle);
                serialsDialog.show(getFragmentManager(), "");
            }

        } else {
            Toast.makeText(getContext(), getString(R.string.there_no_item_left_to_add_serial_number), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getPosition(int position) {
        currantPosition = position;
        Bundle bundle;
        bundle = new Bundle();
        bundle.putSerializable(SerialsDialog.DATA, (Serializable) allSerialModels);
        bundle.putInt(SerialsDialog.SERIAL_TYPE, allSerialModels.get(position).getSerialType());
        bundle.putSerializable(SerialsDialog.EDIT_DATA, allSerialModels.get(position));
        bundle.putInt(SerialsDialog.QUANTITY, shipmentQuantity);
        bundle.putInt(SerialsDialog.POSITION, position);
        SerialsDialog serialsDialog = new SerialsDialog(this);
        serialsDialog.setArguments(bundle);
        serialsDialog.show(getFragmentManager(), "");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (currantPosition != -1) {
                allSerialModels.get(currantPosition).setSerialNumber(data.getStringExtra(BarCodeActivity.BAR_CODE));
                adapter.notifyItemChanged(currantPosition);
            }
        }
    }

    private OnRecyclerClick onRemoveItem = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (allSerialModels.size() == 1) {
                quntityRadioButton.setClickable(true);
                fromToSerialRadioButton.setClickable(true);
                singleSerialRadioButton.setClickable(true);
            }
            if (position < allSerialModels.size()) {
                if (fromToSerialRadioButton.isChecked()) {
                    int quantity = 0;
                    BigInteger fromSerials = new BigInteger(allSerialModels.get(position).getSerialNumber());
                    BigInteger toSerials = new BigInteger(allSerialModels.get(position).getToNumber());
                    BigInteger result = toSerials.subtract(fromSerials);
                    quantity = result.intValue() + 1;
                    serialsQuantity = serialsQuantity - quantity;
                } else if (quntityRadioButton.isChecked()) {
                    serialsQuantity = serialsQuantity - Integer.parseInt(allSerialModels.get(position).getSerialNumber());
                } else {
                    serialsQuantity = serialsQuantity - 1;
                }
            }
            enteredSerialsRowMyOrderTextView.setText(String.format(Locale.ENGLISH, "%d", serialsQuantity));

            if (position < allSerialModels.size())
                allSerialModels.remove(position);
            completeSerials = false;
            adapter.notifyDataSetChanged();

        }
    };

    @OnClick(R.id.Save_Buttom)
    public void onSaveClicked() {
        if (allSerialModels.size() > 0) {
            if (singleSerialRadioButton.isChecked()) {
                if (!serialsQuantity.equals(shipmentQuantity)) {
                    Toast.makeText(getContext(), getString(R.string.please_add_serial), Toast.LENGTH_SHORT).show();

                } else {
                    singleSerialList = new ArrayList<>();
                    for (int i = 0; i < allSerialModels.size(); i++) {
                        singleSerialList.add(new SingleSerialModel(allSerialModels.get(i).getSerialNumber()));
                    }
                    LoadingDialog.showDialog(getContext());
                    shipViewModel.saveSingleSerials(itemId, singleSerialList).observe(this, new ApiCompleted() {
                        @Override
                        protected void noConnection() {
                            LoadingDialog.dismiss();

                        }

                        @Override
                        protected void onSuccess() {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), "Save Successfully", Toast.LENGTH_SHORT).show();
                            if (getActivity() != null)
                                getActivity().onBackPressed();

                        }

                        @Override
                        protected void onError(String error) {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            } else if (quntityRadioButton.isChecked()) {

                if (!serialsQuantity.equals(shipmentQuantity)) {
                    Toast.makeText(getContext(), getString(R.string.please_add_serial), Toast.LENGTH_SHORT).show();

                } else {
                    int quantit = Integer.parseInt(allSerialModels.get(0).getSerialNumber());
                    List<SerialsQuantity> serialsQuantities = new ArrayList<>();
                    serialsQuantities.add(new SerialsQuantity(quantit));
                    LoadingDialog.showDialog(getContext());
                    shipViewModel.saveQuantitySerials(itemId, serialsQuantities).observe(this, new ApiCompleted() {
                        @Override
                        protected void noConnection() {
                            LoadingDialog.dismiss();

                        }

                        @Override
                        protected void onSuccess() {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), "Save Successfully", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }

                        @Override
                        protected void onError(String error) {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            } else if (fromToSerialRadioButton.isChecked()) {
                if (!serialsQuantity.equals(shipmentQuantity)) {
                    completeSerials = false;
                    Toast.makeText(getContext(), getString(R.string.please_add_serials), Toast.LENGTH_SHORT).show();

                } else {
                    rangeSerialList = new ArrayList<>();
                    for (int i = 0; i < allSerialModels.size(); i++) {
                        rangeSerialList.add(new FromToSerialModel(allSerialModels.get(i).getSerialNumber(), allSerialModels.get(i).getToNumber()));
                    }
                    LoadingDialog.showDialog(getContext());
                    shipViewModel.saveRangeSerials(itemId, rangeSerialList).observe(this, new ApiCompleted() {
                        @Override
                        protected void noConnection() {
                            LoadingDialog.dismiss();

                        }

                        @Override
                        protected void onSuccess() {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), "Save Successfully", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();

                        }

                        @Override
                        protected void onError(String error) {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.please_add_serials), Toast.LENGTH_SHORT).show();
        }
    }

    private OnRecyclerClick getSerial = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {


        }
    };
    private OnRecyclerClick getRangeSerial = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {

        }
    };

    @Override
    public void getSerialsData(List<AllSerialModel> addSerials, int quantity) {
        if (singleSerialRadioButton.isChecked()) {
            quntityRadioButton.setClickable(false);
            fromToSerialRadioButton.setClickable(false);
            serialsQuantity = addSerials.size();
            enteredSerialsRowMyOrderTextView.setText(String.format(Locale.ENGLISH, "%d", serialsQuantity));
            allSerialModels = addSerials;
            adapter.notifyDataSetChanged();

        } else if (fromToSerialRadioButton.isChecked()) {
            quntityRadioButton.setClickable(false);
            singleSerialRadioButton.setClickable(false);
            serialsQuantity = quantity;
            enteredSerialsRowMyOrderTextView.setText(String.format(Locale.ENGLISH, "%d", serialsQuantity));
            allSerialModels = addSerials;
            adapter.notifyDataSetChanged();
        } else {
            singleSerialRadioButton.setClickable(false);
            fromToSerialRadioButton.setClickable(false);
            serialsQuantity = Integer.valueOf(addSerials.get(0).getSerialNumber());
            enteredSerialsRowMyOrderTextView.setText(String.format(Locale.ENGLISH, "%d", serialsQuantity));
            adapter.notifyDataSetChanged();
        }
    }
}
