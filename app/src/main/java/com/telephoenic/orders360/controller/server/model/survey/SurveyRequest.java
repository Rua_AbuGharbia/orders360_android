package com.telephoenic.orders360.controller.server.model.survey;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SurveyRequest implements Serializable {

	@JsonProperty("vendorId")
	private Long vendorId;

	@JsonProperty("pageNumber")
	private String pageNumber;

	@JsonProperty("pageSize")
	private String pageSize;

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
}
