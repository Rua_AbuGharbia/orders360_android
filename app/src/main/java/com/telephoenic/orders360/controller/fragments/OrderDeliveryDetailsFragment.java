package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.DateUtil;
import com.telephoenic.orders360.view.adapters.OrderDeliveryDetailsAdapter;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OrderDeliveryDetailsFragment extends BlankFragment implements OnRecyclerClick {

    public static final String ORDER_ID = "id";
    public static final String ITEM_DATA = "data";

    @BindView(R.id.NamePosUser_Row_OrderDelivery_TextView)
    TextView namePosUserRowOrderDeliveryTextView;
    @BindView(R.id.OrderId_Row_OrderDelivery_TextView)
    TextView orderIdRowOrderDeliveryTextView;
    @BindView(R.id.TotalAmount_Row_OrderDelivery_TextView)
    TextView totalAmountRowOrderDeliveryTextView;
    @BindView(R.id.OrderDate_Row_OrderDelivery_TextView)
    TextView orderDateRowOrderDeliveryTextView;
    @BindView(R.id.ShipDate_Row_OrderDelivery_TextView)
    TextView shipDateRowOrderDeliveryTextView;
    @BindView(R.id.City_Row_OrderDelivery_TextView)
    TextView cityRowOrderDeliveryTextView;
    @BindView(R.id.Region_Row_OrderDelivery_TextView)
    TextView regionRowOrderDeliveryTextView;
    @BindView(R.id.DeliveryStatus_Row_OrderDelivery_TextView)
    TextView deliveryStatusRowOrderDeliveryTextView;
    @BindView(R.id.Location_TextView)
    TextView locationTextView;
    @BindView(R.id.Location_ImageView)
    ImageView locationImageView;
    @BindView(R.id.text_order_name)
    TextView textOrderName;
    @BindView(R.id.text_quantity)
    TextView textQuantity;
    @BindView(R.id.text_order_date)
    TextView textOrderDate;
    @BindView(R.id.text_price)
    TextView textPrice;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    @BindView(R.id.Warehouse_Row_OrderDelivery_TextView)
    TextView WarehouseRowOrderDeliveryTextView;
    @BindView(R.id.ShipNumber_Row_OrderDelivery_TextView)
    TextView shipNumberRowOrderDeliveryTextView;
    private ShipModel shipModel;
    private ShipViewModel shipViewModel;
    private String TAG = OrderDeliveryDetailsFragment.class.getName();
    private List<ShipItemsModel> shipItemsModelList;
    private OrderDeliveryDetailsAdapter adapter;


    public OrderDeliveryDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_delivery_details_fragment, container, false);
        ButterKnife.bind(this, view);
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        if (getArguments() != null) {
            shipModel = (ShipModel) getArguments().getSerializable(ITEM_DATA);
        }
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {
        locationImageView.setVisibility(View.VISIBLE);
        locationTextView.setVisibility(View.VISIBLE);
        deliveryStatusRowOrderDeliveryTextView.setVisibility(View.GONE);
        shipNumberRowOrderDeliveryTextView.setVisibility(View.GONE);
        WarehouseRowOrderDeliveryTextView.setVisibility(View.GONE);

        cityRowOrderDeliveryTextView.setText(String.format("%s : %s", getContext().getString(R.string.city_text), shipModel.getCity()));
        regionRowOrderDeliveryTextView.setText(String.format("%s : %s", getContext().getString(R.string.region_text), shipModel.getRegion()));
        //  totalAmountRowOrderDeliveryTextView.setText(String.format("%s : %s", getContext().getString(R.string.fragment_purchase_history_title_total_amount), shipModel.getAmount()));
        totalAmountRowOrderDeliveryTextView.setText("Total Amount : " + String.format(Locale.ENGLISH, "%.2f", shipModel.getAmount()) + " " + AppConstants.CURRENCY);

        orderIdRowOrderDeliveryTextView.setText(String.format("%s%s", getContext().getString(R.string.order_id), shipModel.getOrderNumber()));
        namePosUserRowOrderDeliveryTextView.setText(shipModel.getPosName());
        String dateString = DateUtil.formatDate(DateUtil.parseDate((shipModel.getOrderDate())));
        String shipDate = DateUtil.formatDate(DateUtil.parseDate((shipModel.getShipmentDate())));


        orderDateRowOrderDeliveryTextView.setText(String.format("%s : %s", getContext().getString(R.string.order_date), dateString));
        shipDateRowOrderDeliveryTextView.setText(String.format("%s : %s", getContext().getString(R.string.ship_date), shipDate));


        shipItemsModelList = new ArrayList<>();
        adapter = new OrderDeliveryDetailsAdapter(getContext(), this, shipItemsModelList);
        recyclerView.setAdapter(adapter);

        getShipItem(shipModel.getShipmentId());


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.delivery_details));
        createComponents(view);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && !isInternetConnection) {
            this.onViewCreated(getView(), new Bundle());
        }
    }


    @OnClick({R.id.Location_TextView, R.id.Location_ImageView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Location_TextView:
            case R.id.Location_ImageView:
                AppUtils.goToLocation(shipModel.getLatitude(), shipModel.getLongitude(), getContext());
                break;
        }
    }

    @Override
    public void getPosition(int position) {

    }

    @OnClick(R.id.Confirm_Arrival_Buttom)
    public void onViewClicked() {
        AppUtils.arrivalSaveData = null;
        Bundle bundle = new Bundle();
        bundle.putLong(ConfirmArrivalDeliveryFragment.SHIPMENT_ID, shipModel.getShipmentId());
        bundle.putLong(ConfirmArrivalDeliveryFragment.INVOICE_NUMBER, shipModel.getInvoiceNumber());
        bundle.putDouble(ConfirmArrivalDeliveryFragment.AMOUNT, shipModel.getAmount());
        gotoFragment(new ConfirmArrivalDeliveryFragment(), getFragmentManager(), bundle);
    }

    private void getShipItem(Long id) {
        paginationLoadingView.setVisibility(View.VISIBLE);
        shipViewModel.getShipItemOrder(id).observe(this, new ApiObserver<List<ShipItemsModel>>() {
            @Override
            protected void noConnection() {
                paginationLoadingView.setVisibility(View.GONE);
                // noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<ShipItemsModel> data) {
                paginationLoadingView.setVisibility(View.GONE);
                shipItemsModelList.addAll(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            protected void onError(String error) {
                paginationLoadingView.setVisibility(View.GONE);
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        });
    }
}
