package com.telephoenic.orders360.controller.server.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserPassword implements Serializable {

    private String oldPassword;
    private String newPassword;
    @SerializedName("userId")
    private Long posId;
    private String mobileNo;

    public Boolean getForceFirstLogin() {
        return forceFirstLogin;
    }

    public void setForceFirstLogin(Boolean forceFirstLogin) {
        this.forceFirstLogin = forceFirstLogin;
    }

    private Boolean forceFirstLogin;

    public UserPassword(String oldPassword, String newPassword, Long posId, String mobileNo,Boolean forceFirstLogin) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.posId = posId;
        this.mobileNo = mobileNo;
        this.forceFirstLogin =forceFirstLogin;
    }

    public UserPassword() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
