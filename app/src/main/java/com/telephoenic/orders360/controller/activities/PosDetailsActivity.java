package com.telephoenic.orders360.controller.activities;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.CityViewModel;
import com.telephoenic.orders360.viewmodel.PosGroupViewModel;
import com.telephoenic.orders360.viewmodel.RegionViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;

import static com.telephoenic.orders360.controller.utils.AppUtils.isValidEmailAddress;

public class PosDetailsActivity extends AppCompatActivity implements DialogUtils.OnDialogClickListener{

    private static final String TAG = "PosDetailsActivity";
    @BindInput(R.id.pos_details_shopName_editText)
    Orders360EditText posDetailsShopNameEditText;
    @BindInput(R.id.pos_details_TRN)
    Orders360EditText posDetailsTRN;
    @BindInput(R.id.pos_details_phoneNumber_editText)
    Orders360EditText posDetailsPhoneNumberEditText;
    @BindInput(R.id.pos_details_ownerName_editText)
    Orders360EditText posDetailsOwnerNameEditText;
    @BindInput(R.id.pos_details_ownerPhoneNumber_editText)
    Orders360EditText posDetailsOwnerPhoneNumberEditText;
    @BindInput(R.id.pos_details_landNumber_editText)
    Orders360EditText posDetailsLandNumberEditText;
    @BindInput(R.id.pos_details_faxNumber_editText)
    Orders360EditText posDetailsFaxNumberEditText;
    @BindInput(R.id.pos_details_email_editText)
    Orders360EditText posDetailsEmailEditText;
    @BindInput(R.id.pos_details_selectcity_editText)
    Orders360EditText posDetailsSelectcityEditText;
    @BindInput(R.id.pos_details_selectregin_editText)
    Orders360EditText posDetailsSelectreginEditText;
    @BindInput(R.id.pos_details_selectGroup_editText)
    Orders360EditText posDetailsSelectGroupEditText;
    private User user;
    private boolean hasLocation = false;
    // views
    private Button cancelButton, nextButton;

    private ListItemDialog listItemDialog;

    private boolean isDialogVisible = false;
    private String myService ="";

    long groupId = -1;

    // Objects
    private List<CityModel> citiesList = new ArrayList<>();
    private List<RegionModel> regionsList = new ArrayList<>();
    private List<String> citiesArray;
    private List<String> regionsArray;
    private String city_id = "nothing";
    private String region_id = "nothing";
    private String type_id = "nothing";

    private List<GroupModel> groupModelList = new ArrayList<>();

    // api call obj
    private RegionViewModel regionViewModel;
    private CityViewModel cityViewModel;
    private PosGroupViewModel posGroupViewModel;

    @SuppressLint({"ResourceAsColor", "NewApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos_details);
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.white));
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView imageViewBack = toolbar.findViewById(R.id.imageView_back);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        citiesArray = new ArrayList<>();
        regionsArray = new ArrayList<>();
        regionViewModel = ViewModelProviders.of(this).get(RegionViewModel.class);
        cityViewModel = ViewModelProviders.of(this).get(CityViewModel.class);
        posGroupViewModel = ViewModelProviders.of(this).get(PosGroupViewModel.class);

        initViews();
        Intent intent = getIntent();

        if (intent != null) {
            // Edit Mood

            user = (User) intent.getSerializableExtra(AppConstants.USER_INTENT_PUT_EXTRA);
            if (user != null) {
                hasLocation = true; // that's means the user in edit Mood
                if (user.getName() != null) {
                    posDetailsShopNameEditText.setText(user.getName());
                }

                posDetailsShopNameEditText.setEditTextEnable(false);
                posDetailsShopNameEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsShopNameEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                if (user.getTrn() != null) {
                    posDetailsTRN.setText(user.getTrn());
                }

                posDetailsTRN.setEditTextEnable(false);
                posDetailsTRN.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsTRN.setFocusable(View.NOT_FOCUSABLE);
                }


                if (user.getMobileNumber() != null) {
                    posDetailsPhoneNumberEditText.setText(user.getMobileNumber());
                }

                posDetailsPhoneNumberEditText.setEditTextEnable(false);
                posDetailsPhoneNumberEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsPhoneNumberEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                if (user.getEmail() != null) {
                    posDetailsEmailEditText.setText(user.getEmail());
                }

                posDetailsEmailEditText.setEditTextEnable(false);
                posDetailsEmailEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsEmailEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                if (user.getOwnerMobileNumber() != null) {
                    posDetailsOwnerPhoneNumberEditText.setText(user.getOwnerMobileNumber());
                }

                posDetailsOwnerPhoneNumberEditText.setEditTextEnable(false);
                posDetailsOwnerPhoneNumberEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsOwnerPhoneNumberEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                if (user.getOwnerName() != null) {
                    posDetailsOwnerNameEditText.setText(user.getOwnerName());
                }

                posDetailsOwnerNameEditText.setEditTextEnable(false);
                posDetailsOwnerNameEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsOwnerNameEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                if (user.getLandNumber() != null) {
                    posDetailsLandNumberEditText.setText(user.getLandNumber());
                }

                posDetailsLandNumberEditText.setEditTextEnable(false);
                posDetailsLandNumberEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsLandNumberEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                if (user.getFax() != null) {
                    posDetailsFaxNumberEditText.setText(user.getFax());
                }

                posDetailsFaxNumberEditText.setEditTextEnable(false);
                posDetailsFaxNumberEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsFaxNumberEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                posDetailsSelectcityEditText.setEditTextEnable(false);
                posDetailsSelectcityEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsSelectcityEditText.setFocusable(View.NOT_FOCUSABLE);
                }

                posDetailsSelectreginEditText.setEditTextEnable(false);
                posDetailsSelectreginEditText.setEditTextColor(R.color.black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    posDetailsSelectreginEditText.setFocusable(View.NOT_FOCUSABLE);
                }


                if (user.getPosGroup() != null) {
                    groupId = user.getPosGroup();
                }
                fillGroupSpinnerFromServer(groupId);
                if (user.getRegion() != null) {
                    if (user.getRegion().getCity().getName() != null) {
                        posDetailsSelectcityEditText.setText(user.getRegion().getCity().getName());
                    } else {
                        posDetailsSelectcityEditText.setText(getString(R.string.no_city_text));
                    }
                    if (user.getRegion().getName() != null) {
                        posDetailsSelectreginEditText.setText(user.getRegion().getName());
                    } else {
                        posDetailsSelectreginEditText.setText(getString(R.string.no_region_text));
                    }
                } else {
                    posDetailsSelectreginEditText.setText(getString(R.string.no_region_text));
                    posDetailsSelectcityEditText.setText(getString(R.string.no_city_text));
                }
                posDetailsSelectGroupEditText.getContentEditText().setFocusable(false);
                posDetailsSelectGroupEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (groupModelList.size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ListItemDialog.DATA, (Serializable) groupModelList);
                            bundle.putString(ListItemDialog.TITLE, getString(R.string.pos_group));
                            listItemDialog = new ListItemDialog(group);
                            listItemDialog.setArguments(bundle);
                            listItemDialog.show(getSupportFragmentManager(), "");
                        }
                    }
                });
            } else {
                posDetailsSelectcityEditText.getContentEditText().setFocusable(false);
                posDetailsSelectreginEditText.getContentEditText().setFocusable(false);
                posDetailsSelectGroupEditText.getContentEditText().setFocusable(false);

                posDetailsSelectGroupEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (groupModelList.size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ListItemDialog.DATA, (Serializable) groupModelList);
                            bundle.putString(ListItemDialog.TITLE, getString(R.string.pos_group));
                            listItemDialog = new ListItemDialog(group);
                            listItemDialog.setArguments(bundle);
                            listItemDialog.show(getSupportFragmentManager(), "");
                        }
                    }
                });

                posDetailsSelectcityEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (citiesList.size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ListItemDialog.DATA, (Serializable) citiesList);
                            bundle.putString(ListItemDialog.TITLE, getString(R.string.city_text));
                            listItemDialog = new ListItemDialog(city);
                            listItemDialog.setArguments(bundle);
                            listItemDialog.show(getSupportFragmentManager(), "");
                        }
                    }
                });

                posDetailsSelectreginEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (regionsList.size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ListItemDialog.DATA, (Serializable) regionsList);
                            bundle.putString(ListItemDialog.TITLE, getString(R.string.region_text));
                            listItemDialog = new ListItemDialog(regain);
                            listItemDialog.setArguments(bundle);
                            listItemDialog.show(getSupportFragmentManager(), "");
                        }
                    }
                });

                hasLocation = false;
                int newUserFlag = intent.getIntExtra(AppConstants.ADD_NEW_POS_FLAG, -1);
                if (newUserFlag == AppConstants.ADD_NEW_POS_FLAG_VALUE) {
                    posDetailsPhoneNumberEditText.setText(intent.getStringExtra(AppConstants.PHONE_NUMBER_FLAG)); // set the phone number retrived from the previous activity
                    fillCitySpinnerFromServer();
                    fillGroupSpinnerFromServer(-1);
                }
            }// end intent != null if statement
        }

    }// end onCreate Method

    private void fillGroupSpinnerFromServer(final long groupId) {
        posGroupViewModel.getAllGroups(AppPrefs.getVendorID(PosDetailsActivity.this))
                .observe(this, new ApiObserver<List<GroupModel>>() {
                    @Override
                    protected void noConnection() {
                        myService = AppConstants.SERVICE_GET_POS_GROUP;
                        if(!isDialogVisible){
                            noConnectionInternetDialog();
                            isDialogVisible = true;
                        }
                    }

                    @Override
                    protected void onSuccess(List<GroupModel> data) {
                        Log.e(TAG, "onSuccess:  Rua" );
                        if (data != null) {
                            groupModelList = data;
                            int selectedIndex = -1;
                            for (GroupModel groupModel : groupModelList) {
                                if (groupModel.getId().equals(groupId)) {
                                    selectedIndex = groupModelList.indexOf(groupModel);
                                }
                            }
                            if (selectedIndex != -1) {
                                posDetailsSelectGroupEditText.setText(groupModelList.get(selectedIndex).getName());
                                type_id = String.valueOf(groupModelList.get(selectedIndex).getId());
                            }
                        }
                    }

                    @Override
                    protected void onError(String error) {
                        Toast.makeText(PosDetailsActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void fillCitySpinnerFromServer() {

        // error ***********here there error showed up when the instance run is enabled in android studio, solved successfully *****************
        LoadingDialog.showDialog(this);
        cityViewModel.getAllCities(AppPrefs.getCountryId(PosDetailsActivity.this))
                .observe(this, new ApiObserver<List<CityModel>>() {
                    @Override
                    protected void noConnection() {
                        myService = AppConstants.SERVICE_GET_CITY;
                        LoadingDialog.dismiss();
                        if(!isDialogVisible){
                            noConnectionInternetDialog();
                            isDialogVisible = true;
                        }
                    }

                    @Override
                    protected void onSuccess(List<CityModel> data) {
                        assert data != null;
                        citiesList = data;
                        citiesArray.clear();
                        LoadingDialog.dismiss();
                    }

                    @Override
                    protected void onError(String error) {
                        Toast.makeText(PosDetailsActivity.this, error, Toast.LENGTH_SHORT).show();
                        LoadingDialog.dismiss();
                    }
                });
    }

    private void initViews() {
        posDetailsPhoneNumberEditText.setEditTextEnable(false);

        cancelButton = findViewById(R.id.pos_details_cancel_button);
        nextButton = findViewById(R.id.pos_details_next_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PosDetailsActivity.this, LocationActivity.class);
                User userObj = new User(); // this created to fill it with the user data from the sales agent and send it to the location activity when the Mood is Add Mood
                if (hasLocation) { // existed user
                    if (!type_id.equals("nothing")) {
                        user.setPosGroup(Long.parseLong(type_id));
                    } else {
                        posDetailsSelectGroupEditText.setErrorMessage(getString(R.string.please_select_pos_group));
                        posDetailsSelectGroupEditText.validate();
                        return;
                    }
                } else {
                    posDetailsLandNumberEditText.clearFocus();
                    posDetailsShopNameEditText.clearFocus();
                    posDetailsOwnerPhoneNumberEditText.clearFocus();
                    posDetailsOwnerNameEditText.clearFocus();
                    posDetailsPhoneNumberEditText.clearFocus();
                    posDetailsTRN.clearFocus();
                    posDetailsFaxNumberEditText.clearFocus();
                    posDetailsEmailEditText.clearFocus();

                    String shopName = posDetailsShopNameEditText.getEditTextContent().trim();
                    String posMobileNumber = posDetailsPhoneNumberEditText.getEditTextContent().trim();
                    String ownerName = posDetailsOwnerNameEditText.getEditTextContent().trim();
                    String ownerMobileNumber = posDetailsOwnerPhoneNumberEditText.getEditTextContent().trim();
                    String landNumber = posDetailsLandNumberEditText.getEditTextContent().trim();
                    String faxNumber = posDetailsFaxNumberEditText.getEditTextContent().trim();
                    String emailAddress = posDetailsEmailEditText.getEditTextContent().trim();
                    String trn = posDetailsTRN.getEditTextContent().trim(); // new requirment SDS

                    String city = posDetailsSelectcityEditText.getEditTextContent();

                    String region = "nothing";
                    if (TextUtils.isEmpty(posDetailsSelectreginEditText.getEditTextContent())) {
                        region = posDetailsSelectreginEditText.getEditTextContent();
                    }

                    String posGroup = posDetailsSelectGroupEditText.getEditTextContent();

                    if (TextUtils.isEmpty(shopName)) {
                        posDetailsShopNameEditText.setErrorMessage(getResources().getString(R.string.required_shop_name));
                        posDetailsShopNameEditText.validate();
                        posDetailsShopNameEditText.requestFocus();
                        return;
                    }
                    if (!TextUtils.isEmpty(shopName) && shopName.length() < 3) {
                        posDetailsShopNameEditText.setErrorMessage(getResources().getString(R.string.min_lenght_3_digits));
                        posDetailsShopNameEditText.validate();
                        posDetailsShopNameEditText.requestFocus();
                        return;
                    }

                    if (TextUtils.isEmpty(posMobileNumber)) {
                        posDetailsPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_requird));
                        posDetailsPhoneNumberEditText.validate();
                        posDetailsPhoneNumberEditText.requestFocus();
                        return;
                    }

                    if (TextUtils.isEmpty(emailAddress)) {
                        userObj.setEmail(null);
                    } else {
                        if (isValidEmailAddress(emailAddress)) {
                            userObj.setEmail(emailAddress);
                        } else {
                            posDetailsEmailEditText.setErrorMessage(getResources().getString(R.string.email_pattern_text));
                            posDetailsEmailEditText.validate();
                            posDetailsEmailEditText.requestFocus();
                            return;
                        }
                    }

                    if (TextUtils.isEmpty(faxNumber)) {
                        userObj.setFax(null);
                    } else {
                        for (int i = 0; i < faxNumber.length(); i++) {
                            if (!Character.isDigit(faxNumber.charAt(i))) {
                                posDetailsFaxNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_validation_only_digits_allowes_add_pos));
                                posDetailsFaxNumberEditText.validate();
                                posDetailsFaxNumberEditText.requestFocus();
                                return;
                            }
                        }

                        if (faxNumber.length() < 9) {
                            posDetailsFaxNumberEditText.setErrorMessage(getResources().getString(R.string.please_enter_9_digits_fax_number));
                            posDetailsFaxNumberEditText.validate();
                            posDetailsFaxNumberEditText.requestFocus();
                            return;
                        }
                        if (!faxNumber.startsWith("05")) {
                            posDetailsFaxNumberEditText.validate();
                            posDetailsFaxNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
                            posDetailsFaxNumberEditText.requestFocus();
                            return;
                        }

                        userObj.setFax(faxNumber);
                    }

                    if (TextUtils.isEmpty(trn)) {
                        posDetailsTRN.setErrorMessage(getString(R.string.trn_validation_message));
                        posDetailsTRN.validate();
                        posDetailsTRN.requestFocus();
                        return;
                    } else if (trn.length() != 15) {
                        posDetailsTRN.setErrorMessage(getString(R.string.trn_validation_lenght_message));
                        posDetailsTRN.validate();
                        posDetailsTRN.requestFocus();
                        return;
                    } else {
                        for (int i = 0; i < trn.length(); i++) {
                            if (!Character.isDigit(trn.charAt(i))) {
                                posDetailsTRN.setErrorMessage(getResources().getString(R.string.trn_validation_only_digit_message));
                                posDetailsTRN.validate();
                                posDetailsTRN.requestFocus();
                                return;
                            }
                        }
                        userObj.setTrn(trn);
                    }

                    // ***********************************************************

                    if (TextUtils.isEmpty(ownerMobileNumber)) {
                        userObj.setOwnerMobileNumber(null);
                    } else {
                        for (int i = 0; i < ownerMobileNumber.length(); i++) {
                            if (!Character.isDigit(ownerMobileNumber.charAt(i))) {
                                posDetailsOwnerPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_validation_only_digits_allowes_add_pos));
                                posDetailsOwnerPhoneNumberEditText.validate();
                                posDetailsOwnerPhoneNumberEditText.requestFocus();
                                return;
                            }
                        }

                        if (ownerMobileNumber.length() < 10) {
                            posDetailsOwnerPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.owner_mobile_number_validation_min_lenght_add_pos));
                            posDetailsOwnerPhoneNumberEditText.validate();
                            posDetailsOwnerPhoneNumberEditText.requestFocus();
                            return;
                        }
                        if (!ownerMobileNumber.startsWith("05")) {
                            posDetailsOwnerPhoneNumberEditText.validate();
                            posDetailsOwnerPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
                            posDetailsOwnerPhoneNumberEditText.requestFocus();
                            return;
                        }
                        userObj.setOwnerMobileNumber(ownerMobileNumber);
                    }
                    if (TextUtils.isEmpty(landNumber)) {
                        userObj.setLandNumber(null);
                    } else {
                        for (int i = 0; i < landNumber.length(); i++) {
                            if (!Character.isDigit(landNumber.charAt(i))) {
                                posDetailsLandNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_validation_only_digits_allowes_add_pos));
                                posDetailsLandNumberEditText.validate();
                                posDetailsLandNumberEditText.requestFocus();
                                return;
                            }
                        }
                        if (landNumber.length() < 9) {
                            posDetailsLandNumberEditText.setErrorMessage(getResources().getString(R.string.please_enter_9_digits_land_number));
                            posDetailsLandNumberEditText.validate();
                            posDetailsLandNumberEditText.requestFocus();
                            return;
                        }

                        if (!landNumber.startsWith("05")) {
                            posDetailsLandNumberEditText.validate();
                            posDetailsLandNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
                            posDetailsLandNumberEditText.requestFocus();
                            return;
                        }
                        userObj.setLandNumber(landNumber);
                    }

                    if (TextUtils.isEmpty(posDetailsSelectcityEditText.getEditTextContent()) || city.isEmpty()) {
                        posDetailsSelectcityEditText.setErrorMessage(getString(R.string.select_city_text));
                        posDetailsSelectcityEditText.validate();
                        return;
                    }
                    if (TextUtils.isEmpty(posDetailsSelectreginEditText.getEditTextContent()) || region == null) {
                        posDetailsSelectreginEditText.setErrorMessage(getString(R.string.select_region_text));
                        posDetailsSelectreginEditText.validate();
                        return;
                    }
                    if (TextUtils.isEmpty(posDetailsSelectGroupEditText.getEditTextContent()) || posGroup.isEmpty()) {
                        posDetailsSelectGroupEditText.setErrorMessage(getString(R.string.please_select_pos_group));
                        posDetailsSelectGroupEditText.validate();
                        return;
                    }

                    userObj.setName(shopName);
                    userObj.setMobileNumber(posMobileNumber);
                    userObj.setOwnerName(ownerName);

                    // TODO PUT posGroup INSTED OF POS TYPE
                   /* GroupModel groupModel = new GroupModel();
                    groupModel.setName(posGroup);
                    groupModel.setId(Long.parseLong(type_id));*/
                    userObj.setPosGroup(Long.parseLong(type_id));

                    RegionModel regionObj = new RegionModel();
                    regionObj.setName(region);
                    regionObj.setId(Long.parseLong(region_id));
                    CityModel cityObj = new CityModel();
                    cityObj.setName(city);
                    cityObj.setId(Long.parseLong(city_id));
                    regionObj.setCity(cityObj);
                    userObj.setRegion(regionObj);

                }

                if (hasLocation) {
                    intent.putExtra(AppConstants.EDIT_POS_USER_EXTRA, user);
                } else {
                    if (userObj != null) {
                        intent.putExtra(AppConstants.ADD_NEW_POS_USER_EXTRA, userObj);
                    }
                }
                startActivity(intent);
//                finish();
            }
        });
    }// end initViews Method

    private void fillRegionSpinnerFromServer(String cityId) {
        RegionModel regionModel = new RegionModel();
        CityModel cityModel = new CityModel();
        cityModel.setId(Long.valueOf(cityId));
        regionModel.setCity(cityModel);

        regionViewModel.getAllRegionsAtSelectedCity(regionModel).observe(this, new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                myService = AppConstants.SERVICE_GET_REGION;
                if(!isDialogVisible){
                    noConnectionInternetDialog();
                    isDialogVisible = true;
                }
            }

            @Override
            protected void onSuccess(ResponseBody data) {
                try {
                    assert data != null;
                    JSONObject jsonObj = new JSONObject(data.string());
                    JSONArray jsonArray = jsonObj.getJSONArray("content");
                    regionsList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        RegionModel orderModel = JsonUtils.toJavaObject(jsonArray.get(i).toString(), RegionModel.class);
                        regionsList.add(orderModel);
                    }
                    regionsArray.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onError(String error) {
                Toast.makeText(PosDetailsActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        posDetailsShopNameEditText.requestFocus();
    }

    private OnRecyclerClick city = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            posDetailsSelectcityEditText.setText(citiesList.get(position).getName());
            posDetailsSelectreginEditText.setText("");
            city_id = String.valueOf(citiesList.get(position).getId());
            fillRegionSpinnerFromServer(city_id);
        }
    };
    private OnRecyclerClick regain = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            posDetailsSelectreginEditText.setText(regionsList.get(position).getName());
            region_id = String.valueOf(regionsList.get(position).getId());
        }
    };

    private OnRecyclerClick group = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            posDetailsSelectGroupEditText.setText(groupModelList.get(position).getName());
            type_id = String.valueOf(groupModelList.get(position).getId());
        }
    };

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_GET_POS_GROUP.equals(myService)){
                fillGroupSpinnerFromServer(groupId);
                fillCitySpinnerFromServer();
            } else if(AppConstants.SERVICE_GET_REGION.equals(myService)){
                fillRegionSpinnerFromServer(city_id);
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(PosDetailsActivity.this, settingsDialogModel, this);
    }
}