package com.telephoenic.orders360.controller.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChequeMethodViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Delete_ImageView)
    public ImageView deleteImageView;
    @BindInput(R.id.ChequeTo_OrdersEditText)
    public Orders360EditText chequeToOrdersEditText;
    @BindInput(R.id.BankName_OrdersEditText)
    public Orders360EditText bankNameOrdersEditText;
    @BindInput(R.id.IssuerName_OrdersEditText)
    public Orders360EditText issuerNameOrdersEditText;
    @BindInput(R.id.ChequeNumber_Orders360EditText)
    public Orders360EditText chequeNumberOrders360EditText;
    @BindInput(R.id.ChequeDate_Orders360EditText)
    public Orders360EditText chequeDateOrders360EditText;
    @BindInput(R.id.Amount_Orders360EditText)
    public Orders360EditText amountOrders360EditText;
    OrdersView ordersView;

    public ChequeMethodViewHolder(@NonNull View itemView) {
        super(itemView);
        ordersView = new OrdersView();
        ordersView.bind(this, itemView);
        ButterKnife.bind(this, itemView);
    }
}
