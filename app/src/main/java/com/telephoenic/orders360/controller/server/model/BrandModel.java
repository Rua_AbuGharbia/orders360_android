package com.telephoenic.orders360.controller.server.model;

import java.util.List;

public class BrandModel {
    private List<BrandWrapper> content;

    public List<BrandWrapper> getBrandWrapperList() {
        return content;
    }

    public void setBrandWrapperList(List<BrandWrapper> content) {
        this.content = content;
    }
}
