package com.telephoenic.orders360.controller.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.AddCustomerActivity;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.controller.PostExample;
import com.telephoenic.orders360.controller.server.model.CustomerModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.model.DialogWrapper;
import com.telephoenic.orders360.view.adapters.CampaignAdapter;
import com.telephoenic.orders360.view.customcontrols.SendSMSDialog;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;


public class CampaignFragment extends BlankFragment implements ImageController {
    private String TAG = CampaignFragment.class.getName();

    ListView campaignListView ;
    CampaignAdapter campaignAdapter;
    List<CustomerModel> customerModelList ;
    private int indexClick;

    CheckBox selectAllCheckBox ;
    Button addButton ,sendButton ;

    PosProfileWrapper posProfileWrapper;
    List <String> privilegeList ;
    boolean flag_edit = false;

    boolean flag_loading = true;
    int pageNumber =0 ;
    int pagesiz =10 ;

    public CampaignFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_campaign, container, false);
    }

    private void createComponents(View v){

        campaignListView = v.findViewById(R.id.campaign_listview);

        selectAllCheckBox = v.findViewById(R.id.checkBox_campaign);

        addButton =  v.findViewById(R.id.button_add);
        sendButton =  v.findViewById(R.id.button_send);

        customerModelList = new ArrayList<>();
        /*customerModelList = AppUtils.listCustomerParse(AppPrefs.getCustomerlist(getContext()));
        if(customerModelList == null){
            fillData();
        } else {
            setCampaignAdapter();
        }*/

        selectAllCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    for (CustomerModel model : customerModelList){
                        if(b){
                            model.setChecked(true);
                        }else {
                            model.setChecked(false);
                        }
                    }
                setCampaignAdapter();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),AddCustomerActivity.class);
                intent.putExtra("type",AppConstants.ADD_CUSTOMER_TYPE_NEW);
                intent.putExtra("listSize",""+customerModelList.size());
                startActivityForResult(intent,AppConstants.ADD_CUSTOMER_REQUEST_CODE);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List <String> customerIDs = new ArrayList<String>();
                for (CustomerModel model : customerModelList){
                    if(model.isChecked()){
                        customerIDs.add(model.getId());
                    }
                }
                if(customerIDs.size()==0){
                    String message=getActivity().getResources().getString(R.string.toast_required_fill_checkbox);
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                } else {
                    createSMSDialog(customerIDs);
                }
            }
        });

        posProfileWrapper = AppUtils.getProfile(getContext());

        try {
            getEndUserWebService();
        } catch (ExceptionHandling exceptionHandling) {
            exceptionHandling.printStackTrace();
        } finally {
        }

        sendRequest();

        privilege();

        campaignListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int firstVisibleItem1 = 0, visibleItemCount1 = 0 ,totalItemCount1 =0 ;
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                final int lastItem = firstVisibleItem1 + visibleItemCount1;
                if (lastItem == totalItemCount1 && scrollState == SCROLL_STATE_IDLE) {
                    if(flag_loading == false)
                    {
                        flag_loading = true;
                        sendRequest();

                    }
                }
            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                firstVisibleItem1 = firstVisibleItem;
                visibleItemCount1 = visibleItemCount;
                totalItemCount1 = totalItemCount;

            }
        });

    }

    private void sendRequest(){
        try {
            getEndUserWebService();
        } catch (ExceptionHandling exceptionHandling) {
            exceptionHandling.printStackTrace();
        } finally {
        }
    }

    private void privilege(){
        if(posProfileWrapper!=null) {
            privilegeList = posProfileWrapper.getUser().getPermissions();
            for (String item : privilegeList) {
                if(AppConstants.ROLE_CAMPAIGN_ADD_END_USER.equals(item)){
                    addButton.setVisibility(View.VISIBLE);
                } else if(AppConstants.ROLE_CAMPAIGN_EDIT_END_USER.equals(item)){
                    flag_edit = true;
                } else if(AppConstants.ROLE_CAMPAIGN_SEND_SMS.equals(item)){
                    sendButton.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void getEndUserWebService() throws ExceptionHandling {
        Thread thread = new Thread(new Runnable(){
            public void run() {
                try {
                    PostExample postExample = new PostExample();
                    String json = JsonUtils.toJsonString(posProfileWrapper); // To send Request as RequestBody
                    Object response = postExample.post( AppConstants.URL_LIVE + "campaign/end_user/find-by-example/"
                            +pageNumber+"/"+pagesiz, json);
                    if (response != null) {
                        if(response instanceof String ) {
                            try {
                                flag_loading = false;
                                JSONObject jsonObj = new JSONObject(response.toString());
                               final JSONArray jsonArray = jsonObj.getJSONArray("content");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    CustomerModel customerModel = JsonUtils.toJavaObject(jsonArray.get(i).toString(), CustomerModel.class);
                                    customerModelList.add(customerModel);
                                }
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Your UI updates here
                                        if(jsonArray.length()>0){
                                            pageNumber ++;
                                            setCampaignAdapter();
                                        }
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if(response instanceof ResponseWrapper){
                           final ResponseWrapper responseWrapper = (ResponseWrapper) response;
                            String errorCode = responseWrapper.getHttpCode();
                            String unauthorizedCode = String.valueOf(HttpStatus.UNAUTHORIZED.value());
                            if(unauthorizedCode.equals(errorCode)){
                                AppPrefs.setPOS(getContext(),null);
                                IntentFactory.startActivity(getActivity(), LoginActivity.class,true);
                            }
                            else {
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Your UI updates here
                                        DialogWrapper dialogWrapper = new DialogWrapper();
                                        dialogWrapper.setContext(getActivity());
                                        dialogWrapper.setTitle(getActivity().getString(R.string.titles_dialog_general_error));
                                        dialogWrapper.setMessage(responseWrapper.getMessage());
                                        dialogWrapper.setIsFinished(false);
                                        AppUtils.showAlertDialog(dialogWrapper);
                                    }
                                });
                            }
                        }
                    } else {
                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Your UI updates here
                                DialogWrapper dialogWrapper = new DialogWrapper();
                                dialogWrapper.setContext(getActivity());
                                dialogWrapper.setTitle(getActivity().getString(R.string.titles_dialog_general_error));
                                dialogWrapper.setMessage(getActivity().getString(R.string.unknown_error));
                                dialogWrapper.setIsFinished(false);
                                AppUtils.showAlertDialog(dialogWrapper);
                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

          if(requestCode == AppConstants.ADD_CUSTOMER_REQUEST_CODE) {
              if (data != null) {
                  CustomerModel newCustomer  = (CustomerModel) data.getSerializableExtra("customerModel");
                  String type = data.getStringExtra("type");
                  if(AppConstants.ADD_CUSTOMER_TYPE_NEW.equals(type)){
                      customerModelList.add(newCustomer);
                  }else if(AppConstants.ADD_CUSTOMER_TYPE_EDIT.equals(type)){
                      String newCustomerID = newCustomer.getId();
                      for(CustomerModel customer : customerModelList){
                          String customerID = customer.getId();
                        if(customerID.equals(newCustomerID)){
                            int index = customerModelList.indexOf(customer);
                            customer = newCustomer;
                            customerModelList.remove(index);
                            customerModelList.add(index,customer);
                            break;
                        }
                      }
                  }
                  setCampaignAdapter();
              }
          }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void fillData(){
        customerModelList = new ArrayList<>();
        CustomerModel customerModel ;
        customerModel = new CustomerModel();
        customerModel.setId("1");
        customerModel.setName("Rua");
        customerModel.setMobileNumber("0788654274");
        customerModel.setEmail("r.abugharbiah@telephonic.com");
        //customerModel.setAddress("Zarqa");
        customerModel.setChecked(true);
        customerModelList.add(customerModel);

        customerModel = new CustomerModel();
        customerModel.setId("2");
        customerModel.setName("Nisreen");
        customerModel.setMobileNumber("0786070286");
        customerModel.setEmail("r.abugharbiah@telephonic.com");
       // customerModel.setAddress("Zarqa");
        customerModel.setChecked(false);
        customerModelList.add(customerModel);

        customerModel = new CustomerModel();
        customerModel.setId("3");
        customerModel.setName("Roa");
        customerModel.setMobileNumber("0796220916");
        customerModel.setEmail("r.abugharbiah@telephonic.com");
       // customerModel.setAddress("Zarqa");
        customerModel.setChecked(false);
        customerModelList.add(customerModel);
        setCampaignAdapter();

    }
    private void setCampaignAdapter(){
        if(campaignAdapter == null){
            campaignAdapter = new CampaignAdapter(getContext(),R.layout.item_campaign,customerModelList,onIconClicked);
        }else {
            campaignAdapter.notifyDataSetChanged();
        }

     /*   Gson gson = new Gson();
        String customerList = gson.toJson(customerModelList);
        AppPrefs.setCustomerlist(getContext(),customerList);*/

        campaignListView.setAdapter(campaignAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        pageNumber -- ;
        campaignAdapter = null;
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    private View.OnClickListener onIconClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            indexClick = (Integer) v.getTag();
            CustomerModel customerModel = customerModelList.get(indexClick);
            switch (v.getId()){
                case R.id.text_customer_mobile:
                case R.id.text_customer_name:
                    if(flag_edit){
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("customerModel",customerModel);
                        Intent intent = new Intent(getActivity(),AddCustomerActivity.class);
                        intent.putExtra("bundle",bundle);
                        intent.putExtra("type",AppConstants.ADD_CUSTOMER_TYPE_EDIT);
                        startActivityForResult(intent,AppConstants.ADD_CUSTOMER_REQUEST_CODE);
                    }
                    break;
                case R.id.checkBox_campaign:
                    if(customerModel.isChecked()){
                        customerModel.setChecked(false);
                    } else {
                        customerModel.setChecked(true);
                    }
                    setCampaignAdapter();
                    break;
            }
        }
    };

    private void createSMSDialog(List<String> customerIDs){
        SendSMSDialog sendSMSDialog = new SendSMSDialog(getContext(),getActivity(),customerIDs);
        sendSMSDialog.show();
    }
}
