package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.ClickGetFilter;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.controller.server.model.DiscountFilterModel;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.BrandFilterAdapter;
import com.telephoenic.orders360.view.adapters.DiscountFilterAdapter;
import com.telephoenic.orders360.view.adapters.FilterExpandAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.BrandViewModel;
import com.telephoenic.orders360.viewmodel.CategoryViewModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.telephoenic.orders360.controller.fragments.FilterFragment.rootPadding.FINAL_ROOT_PADDING;
import static com.telephoenic.orders360.controller.fragments.FilterFragment.rootPadding.FIRST_ROOT_PADDING;
import static com.telephoenic.orders360.controller.fragments.FilterFragment.rootPadding.SECOND_ROOT_PADDING;
import static com.telephoenic.orders360.controller.fragments.FilterFragment.rootPadding.THIRD_ROOT_PADDING;

public class FilterFragment extends BlankFragment implements OnRecyclerClick {
    private static final int NO_SELECT_ITEM = -1;
    private static final int ALL_CATEGORY = 0;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.Image_Rotation_ImageView)
    ImageView imageRotationImageView;
    @BindView(R.id.NameRoot_RowFilter_TextView)
    TextView nameRootRowFilterTextView;
    @BindView(R.id.ImageBrand_Rotation_ImageView)
    ImageView imageBrandRotationImageView;
    @BindView(R.id.BrandRecyclerView)
    RecyclerView brandRecyclerView;
    @BindView(R.id.category_loading_view)
    SpinKitView categoryLoadingView;
    @BindView(R.id.nav_category)
    ConstraintLayout navCategory;
    @BindView(R.id.brand_loading_view)
    SpinKitView brandLoadingView;
    @BindView(R.id.nav_brands)
    ConstraintLayout navBrands;
    @BindView(R.id.ImageDiscount_Rotation_ImageView)
    ImageView ImageDiscountRotationImageView;
    @BindView(R.id.Discount_CardView)
    ConstraintLayout discountCardView;
    @BindView(R.id.DiscontRecyclerView)
    RecyclerView discontRecyclerView;
    @BindView(R.id.discont_loading_view)
    SpinKitView discontLoadingView;
    @BindView(R.id.nav_discont)
    ConstraintLayout navDiscont;
    private CategoryViewModel categoryViewModel;
    private BrandViewModel brandViewModel;
    private FilterExpandAdapter adapter;
    private List<CategoryNodeWrapper> categoryNodeWrappers = new ArrayList<>();
    private ClickGetFilter clickGetFilter;
    protected CategoryNodeWrapper allCategory;
    private BrandFilterAdapter brandFilterAdapter;
    private int selectPosition = NO_SELECT_ITEM;
    private int brandPosition = NO_SELECT_ITEM;
    private List<DiscountFilterModel> discountFilterModels = new ArrayList<>();
    private DiscountFilterAdapter discountAdapter;

    private String myService = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_fragment, container, false);
        if (getActivity() != null)
            ButterKnife.bind(this, view);
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        brandViewModel = ViewModelProviders.of(this).get(BrandViewModel.class);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && !isInternetConnection) {
            if (getView() != null)
                this.onViewCreated(getView(), new Bundle());
        }
    }

    private void createComponents() {
        discontRecyclerView.setNestedScrollingEnabled(false);
        brandRecyclerView.setNestedScrollingEnabled(false);
        recyclerView.setNestedScrollingEnabled(false);
        if (AppUtils.showDiscount != null && !AppUtils.showDiscount) {
            discountCardView.setVisibility(View.GONE);
        } else {
            discountCardView.setVisibility(View.VISIBLE);
        }

        if (AppUtils.discountFilterModels == null) {
            discountFilterModels = new ArrayList<>();
            discountFilterModels.add(new DiscountFilterModel(false));
            AppUtils.discountFilterModels = discountFilterModels;
            discountAdapter = new DiscountFilterAdapter(getContext(), discountFilterModels, this);
            discontRecyclerView.setAdapter(discountAdapter);

        } else {
            discountFilterModels = AppUtils.discountFilterModels;
            discountAdapter = new DiscountFilterAdapter(getContext(), discountFilterModels, this);
            discontRecyclerView.setAdapter(discountAdapter);
        }
        categoryNodeWrappers = new ArrayList<>();
        if (AppUtils.filterCategory == null) {
            allCategory = new CategoryNodeWrapper();
            allCategory.setName(getString(R.string.all_categories));
            allCategory.setSelect(true);
            allCategory.setRootPadding(rootPadding.FIRST_ROOT_PADDING);
            categoryNodeWrappers.add(allCategory);
            categoryLoadingView.setVisibility(View.VISIBLE);
            categoryViewModel.getCategory(getContext(), this).observe(this, new ApiObserver<List<CategoryNodeWrapper>>() {
                @Override
                protected void noConnection() {
                    categoryLoadingView.setVisibility(View.GONE);
                    isInternetConnection = false;
                }

                @Override
                protected void onSuccess(List<CategoryNodeWrapper> data) {
                    categoryNodeWrappers.addAll(1, data);
                    AppUtils.allCategory = categoryNodeWrappers;
                    sortList(categoryNodeWrappers);
                    categoryLoadingView.setVisibility(View.GONE);
                }

                @Override
                protected void onError(String error) {
                    if (error != null)
                        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                    categoryLoadingView.setVisibility(View.GONE);

                }
            });
        } else {
            categoryNodeWrappers = AppUtils.allCategory;
            for (int i = 0; i < AppUtils.filterCategory.size(); i++) {
                if (AppUtils.filterCategory.get(i).getSelect() != null && AppUtils.filterCategory.get(i).getSelect()) {
                    nameRootRowFilterTextView.setVisibility(View.VISIBLE);
                    nameRootRowFilterTextView.setText(AppUtils.filterCategory.get(i).getName());
                }
            }
            setAdapter(AppUtils.filterCategory);
            onViewClickedCategory();
        }
    }

    private void getBrands() {
        myService = AppConstants.SERVICE_BRAND;
        brandLoadingView.setVisibility(View.VISIBLE);
        brandViewModel.getBrands(AppPrefs.getVendorID(getContext())).observe(this, new ApiObserver<List<BrandWrapper>>() {
            @Override
            protected void noConnection() {
                brandLoadingView.setVisibility(View.GONE);
            }

            @Override
            protected void onSuccess(List<BrandWrapper> data) {
                setBrandAdapter(data);
                brandLoadingView.setVisibility(View.GONE);
            }

            @Override
            protected void onError(String error) {
                if (error != null)
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                brandLoadingView.setVisibility(View.GONE);

            }
        });
    }

    private void setData(@NonNull List<CategoryNodeWrapper> data) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isHasChild() || data.get(i).getParent() != null) {
                List<CategoryNodeWrapper> childList = new ArrayList<>();
                for (int i1 = 0; i1 < categoryNodeWrappers.size(); i1++) {
                    if (categoryNodeWrappers.get(i1).getParent() != null) {
                        if (data.get(i).getId().equals(categoryNodeWrappers.get(i1).getParent().getId())) {
                            childList.add(categoryNodeWrappers.get(i1));
                            data.get(i).setNodes(childList);
                            if (categoryNodeWrappers.get(i1).isHasChild()) {
                                setData(data.get(i).getNodes());
                            }
                        }
                    }
                }
            }
        }
    }

    private void sortList(@NonNull List<CategoryNodeWrapper> data) {
        List<CategoryNodeWrapper> childList = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getParent() == null) {
                data.get(i).setRootPadding(rootPadding.FIRST_ROOT_PADDING);
                childList.add(data.get(i));
            }
        }
        AppUtils.filterCategory = childList;
        adapter = new FilterExpandAdapter(getContext(), childList);
        adapter.setOnItemClickListener(onItemClickListener);
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.ChangeBackGround_ConstraintLayout)
    public void onViewClickedCategory() {
        if (navCategory.getVisibility() == View.VISIBLE) {
            imageRotationImageView.setRotation(270);
            navCategory.setVisibility(View.GONE);
        } else {
            imageRotationImageView.setRotation(90);
            navCategory.setVisibility(View.VISIBLE);
        }
    }

    private FilterExpandAdapter.OnItemClickListener onItemClickListener = new FilterExpandAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(List<CategoryNodeWrapper> listItem, int position) {
            setSelectCategory(listItem, position);
            addNewRoot(listItem, position);
            selectPosition = position;
            AppUtils.selectCategory = listItem.get(position);

        }
    };
    private BrandFilterAdapter.OnItemClickListener onBrandCheck = new BrandFilterAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull List<BrandWrapper> brandWrapperList, int position) {
            brandPosition = position;
            AppUtils.selectBrands = new ArrayList<>();
            if (brandWrapperList.get(position).getSelect() != null && brandWrapperList.get(position).getSelect()) {
                brandWrapperList.get(position).setSelect(false);
            } else {
                brandWrapperList.get(position).setSelect(true);
            }

            for (int i = 0; i < brandWrapperList.size(); i++) {
                if (brandWrapperList.get(i).getSelect() != null && brandWrapperList.get(i).getSelect()) {
                    AppUtils.selectBrands.add(brandWrapperList.get(i).getId());
                }
            }

            AppUtils.allBrand = brandWrapperList;
            brandFilterAdapter.notifyItemChanged(position);
        }
    };

    public void onFilterClick(ClickGetFilter clickGetFilter) {
        this.clickGetFilter = clickGetFilter;
    }

    private void addNewRoot(List<CategoryNodeWrapper> listItem, int position) {
        if (listItem.get(position).isHasChild() && listItem.get(position).getParent() == null) {
            List<CategoryNodeWrapper> newRoot = new ArrayList<>();
            listItem.get(position).setRootPadding(rootPadding.SECOND_ROOT_PADDING);
            newRoot.add(listItem.get(0));
            newRoot.add(listItem.get(position));
            for (int j = 0; j < categoryNodeWrappers.size(); j++) {
                if (categoryNodeWrappers.get(j).getParent() != null) {
                    if (listItem.get(position).getId().equals(categoryNodeWrappers.get(j).getParent().getId())) {
                        categoryNodeWrappers.get(j).setRootPadding(rootPadding.THIRD_ROOT_PADDING);
                        newRoot.add(2, categoryNodeWrappers.get(j));
                    }
                }
            }

            listItem = new ArrayList<>();
            listItem.addAll(newRoot);
            setAdapter(listItem);

        } else if (listItem.get(position).isHasChild() && listItem.get(position).getParent() != null) {
            List<CategoryNodeWrapper> newRoot = new ArrayList<>();
            newRoot.add(listItem.get(0));

            for (int i = 0; i < categoryNodeWrappers.size(); i++) {
                if (listItem.get(position).getParent().getId().equals(categoryNodeWrappers.get(i).getId())) {
                    categoryNodeWrappers.get(i).setRootPadding(rootPadding.SECOND_ROOT_PADDING);
                    newRoot.add(categoryNodeWrappers.get(i));
                }
            }
            listItem.get(position).setRootPadding(rootPadding.THIRD_ROOT_PADDING);
            newRoot.add(listItem.get(position));

            for (int i = 0; i < categoryNodeWrappers.size(); i++) {
                if (categoryNodeWrappers.get(i).getParent() != null) {
                    if (listItem.get(position).getId().equals(categoryNodeWrappers.get(i).getParent().getId())) {
                        categoryNodeWrappers.get(i).setRootPadding(rootPadding.FINAL_ROOT_PADDING);
                        newRoot.add(categoryNodeWrappers.get(i));
                    }
                }
            }

            listItem = new ArrayList<>();
            listItem.addAll(newRoot);
            setAdapter(listItem);

        } else if (position == ALL_CATEGORY) {
            sortList(categoryNodeWrappers);

        } else {
            AppUtils.filterCategory = listItem;
            adapter.notifyDataSetChanged();
        }
    }

    private void setAdapter(List<CategoryNodeWrapper> categoryNodeWrappersResult) {
        AppUtils.filterCategory = categoryNodeWrappersResult;
        adapter = new FilterExpandAdapter(getContext(), categoryNodeWrappersResult);
        adapter.setOnItemClickListener(onItemClickListener);
        recyclerView.setAdapter(adapter);
    }

    private void setSelectCategory(List<CategoryNodeWrapper> listItem, int position) {
        if (position == ALL_CATEGORY) {
            nameRootRowFilterTextView.setVisibility(View.GONE);
        } else {
            nameRootRowFilterTextView.setVisibility(View.VISIBLE);
            nameRootRowFilterTextView.setText(listItem.get(position).getName());
        }

        listItem.get(position).setSelect(true);
        for (int i = 0; i < listItem.size(); i++) {
            if (position != i) {
                listItem.get(i).setSelect(false);
            }
        }
    }

    @OnClick(R.id.Brand_CardView)
    public void onViewClickedBrand() {
        if (navBrands.getVisibility() == View.VISIBLE) {
            imageBrandRotationImageView.setRotation(270);
            navBrands.setVisibility(View.GONE);
        } else {
            imageBrandRotationImageView.setRotation(90);
            navBrands.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.Done_Filter_TextView, R.id.Reset_Filter_TextView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Done_Filter_TextView:
                if (clickGetFilter != null) {
                    clickGetFilter.getPosition(selectPosition, brandPosition);
                }
                break;
            case R.id.Reset_Filter_TextView:
                clearFilter();
                break;
        }
    }


    @OnClick(R.id.Discount_CardView)
    public void onViewClicked() {
        if (navDiscont.getVisibility() == View.VISIBLE) {
            ImageDiscountRotationImageView.setRotation(270);
            navDiscont.setVisibility(View.GONE);
        } else {
            ImageDiscountRotationImageView.setRotation(90);
            navDiscont.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getPosition(int position) {
        if (discountFilterModels.get(position).isSelect()) {
            discountFilterModels.get(position).setSelect(false);
        } else {
            discountFilterModels.get(position).setSelect(true);
        }
        AppUtils.discountFilterModels = discountFilterModels;
        discountAdapter.notifyDataSetChanged();

    }

    @IntDef({FIRST_ROOT_PADDING, SECOND_ROOT_PADDING, THIRD_ROOT_PADDING, FINAL_ROOT_PADDING})
    @Retention(RetentionPolicy.CLASS)
    public @interface rootPadding {
        int FIRST_ROOT_PADDING = 25;
        int SECOND_ROOT_PADDING = 40;
        int THIRD_ROOT_PADDING = 80;
        int FINAL_ROOT_PADDING = 120;
    }

    private void setBrandAdapter(List<BrandWrapper> data) {
        brandFilterAdapter = new BrandFilterAdapter(getContext(), data, String.valueOf(System.currentTimeMillis()));
        brandFilterAdapter.setOnItemClickListener(onBrandCheck);
        brandRecyclerView.setAdapter(brandFilterAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        categoryNodeWrappers = new ArrayList<>();
        nameRootRowFilterTextView.setVisibility(View.GONE);
        if (navCategory.getVisibility() == View.VISIBLE) {
            imageRotationImageView.setRotation(270);
            navCategory.setVisibility(View.GONE);
        }
        if (navBrands.getVisibility() == View.VISIBLE) {
            imageBrandRotationImageView.setRotation(270);
            navBrands.setVisibility(View.GONE);
        }

    }

    private void clearFilter() {
        if (selectPosition != NO_SELECT_ITEM || brandPosition != NO_SELECT_ITEM || discountFilterModels.get(0).isSelect()) {
            AppUtils.discountFilterModels = null;
            AppUtils.selectCategory = null;
            AppUtils.filterCategory = null;
            AppUtils.selectBrands = null;
            AppUtils.allBrand = null;
            if (getView() != null) {
                this.onViewCreated(getView(), new Bundle());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUtils.allBrand != null) {
            setBrandAdapter(AppUtils.allBrand);
        } else {
            getBrands();
        }
        createComponents();
    }
}
