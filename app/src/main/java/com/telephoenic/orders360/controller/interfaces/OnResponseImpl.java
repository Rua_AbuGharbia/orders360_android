package com.telephoenic.orders360.controller.interfaces;

import android.app.Activity;
import android.content.Context;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.server.model.ErrorResponse;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.model.DialogWrapper;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

/**
 * Created by Ihab on 8/24/2016.
 */
public abstract class OnResponseImpl implements OnResponse{

    @Override
    public void onSuccess(Object response, Context context) {
        if (response instanceof ResponseWrapper) {
            ResponseWrapper<ErrorResponse> responseWrapper = (ResponseWrapper<ErrorResponse>) response;
            String code = responseWrapper.getResponse().getErrorCode();
            if(String.valueOf(HttpStatus.UNAUTHORIZED.value()).equals(code)){
                AppPrefs.setPOS(context,null);
                IntentFactory.logOut((Activity) context);
            } else {
                String msg = "";
            /*if (AppPrefs.getLanguage(context).contains("ar")) {
                msg = ((ErrorResponse) responseWrapper.getResponse())
                        .getErrorMsg1();
            } else if (AppPrefs.getLanguage(context).contains("en")) {
                msg = ((ErrorResponse) responseWrapper.getResponse())
                        .getErrorMsg2();
            }*/
                msg = responseWrapper.getResponse().getDeveloperMessage();

                DialogWrapper dialogWrapper = new DialogWrapper();
                dialogWrapper.setContext(context);
                dialogWrapper.setTitle(context.getString(R.string.titles_dialog_general_error));
                dialogWrapper.setMessage(msg);
                dialogWrapper.setIsFinished(false);
                AppUtils.showAlertDialog(dialogWrapper);
            }
        }
    }

    @Override
    public void onException(Exception exception, Context context) {
        int msgId = 0;
        StringBuilder title = new StringBuilder();

        if (exception instanceof RestClientException) {
            if (exception.getCause() instanceof SocketTimeoutException) {
                msgId = R.string.messages_error_dialog_connectivity_timeout;

            } else if (exception.getCause() instanceof ConnectException) {
                msgId = R.string.messages_error_dialog_internet_connectivity;
            } else {
                msgId = R.string.messages_error_server;
            }
            title.append(context.getString(R.string.titles_dialog_connectivity));
        } else {
            title.append(context.getString(R.string.titles_dialog_general_error));
            msgId = R.string.unknown_error;
        }

        if (Order360Application.IS_CONNECTED){
            DialogWrapper dialogWrapper = new DialogWrapper();
            dialogWrapper.setContext(context);
            dialogWrapper.setTitle(title.toString());
            dialogWrapper.setMessage(context.getString(msgId));
            dialogWrapper.setIsFinished(false);
            AppUtils.showAlertDialog(dialogWrapper);
        }
    }
}
