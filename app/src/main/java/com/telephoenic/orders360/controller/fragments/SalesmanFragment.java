package com.telephoenic.orders360.controller.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.view.customcontrols.DateTimeDialog;

import java.util.Calendar;


public class SalesmanFragment extends BlankFragment {
    private String TAG = SalesmanFragment.class.getName();

    private DateTimeDialog startDateDialog ,endDateDialog;

    private ImageButton startDateImage , endDateImage ;

    private EditText startDateEditText , endDateEditText ;

    public SalesmanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_salesman, container, false);
    }

    private void createComponents(View v){

        startDateEditText = v.findViewById(R.id.etxt_start_date);
        endDateEditText = v.findViewById(R.id.etxt_end_date);

        startDateImage = v.findViewById(R.id.image_start_date);
        endDateImage = v.findViewById(R.id.image_end_date);


        startDateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDateDialog = new DateTimeDialog(getActivity());
                startDateDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if(!TextUtils.isEmpty(startDateDialog.getDateTime().toString())) {
                            Calendar c = Calendar.getInstance();
                            c.get(Calendar.DATE);
                            startDateEditText.setText(startDateDialog.getDateTime().toString());
                        }
                    }
                });
                startDateDialog.show();
            }
        });

        endDateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endDateDialog = new DateTimeDialog(getActivity());
                endDateDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if(!TextUtils.isEmpty(endDateDialog.getDateTime().toString())) {
                            Calendar c = Calendar.getInstance();
                            c.get(Calendar.DATE);
                            endDateEditText.setText(endDateDialog.getDateTime().toString());
                        }
                    }
                });
                endDateDialog.show();
            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
