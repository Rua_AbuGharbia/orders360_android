package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class SaveOrderRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("totalAmount")
    private Double totalAmount;

    @JsonProperty("orderDetail")
    private List<OrderItems> orderItems;

    @JsonProperty("posUser")
    private User posUser;

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<OrderItems> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    public User getPosUser() {
        return posUser;
    }

    public void setPosUser(User posUser) {
        this.posUser = posUser;
    }
}