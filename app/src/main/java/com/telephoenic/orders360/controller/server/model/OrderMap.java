package com.telephoenic.orders360.controller.server.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Rua on 01-Feb-18.
 */


@Entity(tableName = "orders"/*,primaryKeys = {"user_id", "vendor_id"}*/)
public class OrderMap {

   /* @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "order_id")
    private int orderId;*/

    @PrimaryKey
    @ColumnInfo(name = "id_pk")
    @JsonProperty("idPK")
    private Long idPk;

    @ColumnInfo(name = "vendor_id")
    private Long vendorId;

    @ColumnInfo(name = "order_list")    // order list
    private String orderList;

    @ColumnInfo(name = "user_id")
    private Long userId;

    @ColumnInfo(name = "product_order")    // the List of  product_order
    private String productOrder;

    @ColumnInfo(name = "combo_order")    // the List of  combo_order
    private String comboOrder;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(String productOrder) {
        this.productOrder = productOrder;
    }

    public String getComboOrder() {
        return comboOrder;
    }

    public void setComboOrder(String comboOrder) {
        this.comboOrder = comboOrder;
    }

    public String getOrderList() {
        return orderList;
    }

    public void setOrderList(String orderList) {
        this.orderList = orderList;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Long getIdPk() {
        return idPk;
    }

    public void setIdPk(Long idPk) {
        this.idPk = idPk;
    }

    @Override
    public String toString() {
        return "OrderMap{" +
                "idPk=" + idPk +
                ", vendorId=" + vendorId +
                ", orderList='" + orderList + '\'' +
                ", userId=" + userId +
                ", productOrder='" + productOrder + '\'' +
                ", comboOrder='" + comboOrder + '\'' +
                '}';
    }
}
