package com.telephoenic.orders360.controller.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RowCashAndClaimViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Delete_ImageView)
    public ImageView deleteImageView;
    @BindInput(R.id.CashPayment_OrdersEditText)
    public Orders360EditText cashPaymentOrdersEditText;
    @BindView(R.id.Title_PaymentMethod)
    public TextView titlePaymentMethod;

    public RowCashAndClaimViewHolder(@NonNull View itemView) {
        super(itemView);
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this, itemView);
        ButterKnife.bind(this, itemView);
    }
}
