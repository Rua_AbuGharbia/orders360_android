package com.telephoenic.orders360.controller.server.model.shiporder;

public class ArrivalUnsuccessfulModel {
    private Double latitude;
    private Double longitude;
    private String note;
    private OrderRejectedReason reason;

    public ArrivalUnsuccessfulModel(Double latitude, Double longitude, String note, OrderRejectedReason reason) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.note = note;
        this.reason = reason;
    }

    public ArrivalUnsuccessfulModel() {
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public OrderRejectedReason getReason() {
        return reason;
    }

    public void setReason(OrderRejectedReason reason) {
        this.reason = reason;
    }
}
