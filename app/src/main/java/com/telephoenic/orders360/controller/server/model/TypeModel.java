package com.telephoenic.orders360.controller.server.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 08-Nov-17.
 */

public class TypeModel implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("deleted")
    private boolean deleted;

    @JsonProperty("inactive")
    private boolean inactive;

    @JsonProperty("code")
    private String code;

    @JsonProperty("type")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TypeModel{" +
                "id='" + id + '\'' +
                ", deleted=" + deleted +
                ", inactive=" + inactive +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
