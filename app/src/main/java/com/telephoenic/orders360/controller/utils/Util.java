package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

public class Util {

	static AssetManager assetManager;
	static Typeface tt;
	static Typeface tth;
	public static String imagePathRequest = "";
	public static String imagePathView = "";

	public static void displayToast(Context context, String message) {
		Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	public static void displayToast1(Context context, String message) {
		Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.BOTTOM, 0, 0);
		toast.show();
	}

	public static String getProperty(String key,Context context) throws IOException {
		Properties properties = new Properties();;
		AssetManager assetManager = context.getAssets();
		InputStream inputStream = assetManager.open("config.properties");
		properties.load(inputStream);
		return properties.getProperty(key);

	}


//	public static Typeface getFontBold() {
//		if (tt != null) {
//			return tt;
//		} else {
//			tt = Typeface.createFromAsset(App.getInstance()
//					.getApplicationContext().getAssets(),
//					"Font/GE_Thameen_DemiBold.otf");
//			return tt;
//		}
//	}

//	public static Typeface getFontLight() {
//		if (tth != null) {
//			return tth;
//		} else {
//			tth = Typeface.createFromAsset(App.getInstance()
//					.getApplicationContext().getAssets(),
//					"Font/GE_Thameen_Light_Italic.otf");
//			return tth;
//		}
//	}

	public static boolean hasFroyo() {
		// Can use static final constants like FROYO, declared in later versions
		// of the OS since they are inlined at compile time. This is guaranteed
		// behavior.
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	public static void updateLocale(Context context, String languageCode) {
		Locale locale = null;
		if (languageCode.length() > 2) {
			char ch3 = languageCode.charAt(2);
			if (ch3 == '_') {
				String language = languageCode.substring(0, 2);
				String country = languageCode.substring(3, 5);
				locale = new Locale(language, country);
			}
		} else {
			locale = new Locale(languageCode);
		}

		Configuration config = context.getResources().getConfiguration();
		config.locale = locale;
		context.getResources().updateConfiguration(config,
				context.getResources().getDisplayMetrics());

	}

//	static TypefaceSpan typeFaceSpan;
//
//	public static SpannableString createSpannableString(Context context,
//			String text) {
//		if (typeFaceSpan == null) {
//			typeFaceSpan = new TypefaceSpan(context, "GE_Thameen_DemiBold.otf");
//		}
//		SpannableString s = new SpannableString(text);
//		s.setSpan(typeFaceSpan, 0, s.length(),
//				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		return s;
//	}

	/*public static void setActiobarTitle(ActionBar actionBar, String title) {
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.actionbar);

		View v = actionBar.getCustomView();
		TextView titleTxtView = (TextView) v
				.findViewById(R.id.action_bar_title_center);
		titleTxtView.setText(title);
		//titleTxtView.setTypeface(getFontBold());
	}*/

	public static String getEditTextValue(EditText editText) {
		return editText.getText().toString().trim();
	}
	
	public static boolean isEmpty(EditText editText){
		return TextUtils.isEmpty(editText.getText().toString().trim());
	}
	
//	public static String getStringFromResources(int stringId){
//
//		return UmniahApplication.getInstance().getApplicationContext().getResources().getString(stringId);
//	}
}
