package com.telephoenic.orders360.controller.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.GPSLocation;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.view.customcontrols.CustomDialog;

import static com.telephoenic.orders360.controller.constants.AppConstants.PERMISSIONS_REQ;
import static com.telephoenic.orders360.controller.constants.AppConstants.PERMISSIONS_REQUEST;

public class PosLocationActivity extends FragmentActivity implements OnMapReadyCallback {
    public String TAG = PosLocationActivity.class
            .getSimpleName();

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;

    private String stringLatitude;
    private String stringLongitude;
    private double posLatitude;
    private double posLongitude;
    private String posName;
    private String posNumber;
    private String cityName;
    private String regionName;
    private LatLng currentLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pso_location);

        Intent intent = getIntent();
        if (intent != null) {
            posLatitude = intent.getDoubleExtra(IntentExtraNames.LATITUDE, 0.0);
            /*if(stringLatitude != null){
                posLatitude = Double.parseDouble(stringLatitude);
            }*/

            posLongitude = intent.getDoubleExtra(IntentExtraNames.LONGITUDE, 0.0);
           /* if(stringLongitude != null){
                posLongitude = Double.parseDouble(stringLongitude);
            }*/

            posName = intent.getStringExtra(IntentExtraNames.NAME);
            posNumber = intent.getStringExtra(IntentExtraNames.NUMBER);
            regionName = intent.getStringExtra(IntentExtraNames.REGION_NAME);
            cityName = intent.getStringExtra(IntentExtraNames.CITY_NAME);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .build();
        }

        GPSLocation.checkGPS(this);

        if (Build.VERSION.SDK_INT >= 23) {
            checkPermission();
        }

    }

    public void checkPermission() {
        Log.i(TAG, "Show button pressed. Checking permission.");
        // BEGIN_INCLUDE(permission)
        // Check if the  permission is already available.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // permission has not been granted.
            requestPermission();

        } else {
            // permissions is already available, show the preview.
            Log.i(TAG,
                    "permission has already been granted. Displaying preview.");
        }
// END_INCLUDE(cpermission)
    }

    private void requestPermission() {
        Log.i(TAG, "permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i(TAG, " permission rationale to provide additional context.");

            ActivityCompat.requestPermissions(this,
                    PERMISSIONS_REQ,
                    PERMISSIONS_REQUEST);
        } else {
            //  permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_REQ,
                    PERMISSIONS_REQUEST);
        }
        //navigateToLogin();
        // END_INCLUDE(permission_request)
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for  permission.
            Log.i(TAG, "Received response for  permission request.");
            // Check if the only required permission has been granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //  permission has been granted, preview can be displayed
                Log.i(TAG, " permission has now been granted. Showing preview.");
                showMyLocation();

            } else {
                Log.i(TAG, " permission was NOT granted.");

                finish();

            }
            // END_INCLUDE(permission_result)
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
    /*    LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        //locationModel.setLatitude("32.9798554");
        //locationModel.setLongitude("35.8515541");
        //  LatLng(double latitude, double longitude)
        if (posLatitude == 0.0 || posLongitude == 0.0) {
            AppUtils.showAlertToast(getResources().getString(R.string.toast_attention_pos_direction), PosLocationActivity.this);
        } else {
            LatLng sydney = new LatLng(posLatitude, posLongitude);
            mMap.addMarker(new MarkerOptions().position(sydney).title(posName));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14));
        }

        showMyLocation();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //int position = (int)(marker.getTag());
                //Using position get Value from arraylist
                //Toast.makeText(PosLocationActivity.this,marker.getTitle().toString(),Toast.LENGTH_SHORT).show();
                displayDialog(PosLocationActivity.this);
                return false;
            }
        });

    }

    public void displayDialog(final Context context) {
        final CustomDialog dialogInformation = new CustomDialog(context);
        dialogInformation.setTitle(context.getResources().getString(R.string.title));
        //dialogInformation.setMessage(context.getResources().getString(R.string.message));
        dialogInformation.setMessage(posName + "\n" + posNumber + "\n" + cityName + "\n" + regionName + "\n");
        dialogInformation.show();

        dialogInformation.getOkButton().setText(
                context.getResources().getString(R.string.button_labels_done));
        dialogInformation.getOkButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogInformation.dismiss();
            }
        });
    }

    private void showMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            //mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
            //mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            //mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }

    }

  /*  public boolean checkGPS() {
        boolean check = false;
        if (GPSLocation.isGPSAvilable(this)) {

        }

        return check;
    }*/
}
