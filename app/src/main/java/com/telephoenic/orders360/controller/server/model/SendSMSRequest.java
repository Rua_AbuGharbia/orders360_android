package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rua on 02-Nov-17.
 */

public class SendSMSRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("posID")
    private Long posID;

    @JsonProperty("customerIDs")
    private List <String> customerIDs;

    @JsonProperty("message")
    private String message;


    public Long getPosID() {
        return posID;
    }

    public void setPosID(Long posID) {
        this.posID = posID;
    }

    public List<String> getCustomerIDs() {
        return customerIDs;
    }

    public void setCustomerIDs(List<String> customerIDs) {
        this.customerIDs = customerIDs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
