package com.telephoenic.orders360.controller.constants;

import android.Manifest;
import android.os.Environment;


public final class AppConstants {

    public final static boolean TESTING = false; // TODO True for Testing False for Production
    public final static boolean TESTING_ON_EMULATER = false;  // TODO True for Testing False for Production

    public static String CURRENCY = "JOR";
    public static boolean HAS_PROMOTION_OTHER = false;
    public static Boolean IS_PROMOTION = false;
    public static Boolean SHOW_POINT = false;


    // QA Local
    //  public static String URL_LIVE = "http://192.168.1.59:8686/orders360-web/";
    //   public static String URL_LIVE = "http://192.168.1.59:8080/orders360-web/"; ///////

    //QA Public
    // public static String URL_LIVE = "http://109.107.241.44:8686/orders360-web/";
    // public static String URL_LIVE = "http://109.107.241.44:6547/orders360-web/";
    //orders360 production URL
    //public static String URL_LIVE = "https://portal.orders-360.com:8558/orders360-web/";

    // new QA 2020
//    public static String URL_LIVE = "http://109.107.241.44:1953/orders360-web/";
    public static String URL_LIVE = "http://109.107.241.44:3698/orders360-web/";
    // new QA  Publi
    //  public static String URL_LIVE = "http://109.107.241.44:6547/orders360-web/";
    //Omar PCpush
    /// public static String URL_LIVE = "http://192.168.1.138:8585/orders360-web/";
    // Rosan PC
    //  public static String URL_LIVE = "http://192.168.1.209:8686/orders360-web/";
    // Ahmad PC
    //  public static String URL_LIVE = "http://192.168.1.147:7083/orders360-web/";
    // public static String URL_LIVE = "http://192.168.1.144:8080/orders360-web/"; // hamzeh PC With Cabel

    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44:1953/orders360-web/"; // united food
    //public static String URL_LIVE = "http://192.168.1.92:8080/orders360-web/";
    //public static String URL_LIVE = "http://192.168.1.199:8080/orders360-web/";
    //public static String URL_LIVE = "http://109.107.241.44:1953/orders360-web/";
    // public static String URL_LIVE = "http://192.168.1.92:8080/orders360-web/";

    //orders360 production cloud environment URL
  //  public static String URL_LIVE = "https://test.orders-360.com:8558/orders360-web/";

    // mu'awya Najjar PC With Cable
   // public static String URL_LIVE = "http://192.168.1.54:8080/orders360-web/";
    //  public static String URL_LIVE = "http://192.168.1.203:8080/orders360-web/"; // mu'awya Najjar PC Without Cable

    // sharja drug store live url
    //public static String URL_LIVE = "https://sds.orders-360.com:8443/orders360-web/";
    //public static String URL_LIVE = "http://sds.orders-360.com:8088/orders360-web/"; // TODO new life

    public static String URL_PRIVACY_AR = "http://109.107.241.44:3030/privacy_ar.html";
    public static String URL_PRIVACY_EN = "http://109.107.241.44:3030/privacy_en.html";

    public static final String CONFIG_FILENAME = "config_file";

    public static final String RIGHT_TO_LEFT = "RIGHT_TO_LEFT";
    public static final String LEFT_TO_RIGHT = "LEFT_TO_RIGHT";

    public static String MY_ACTIVITY = "Splash";

    public static String LAST_TITLE = "";
    public static String TIME_OUT_CODE = "400";

    public static String ADD_CUSTOMER_TYPE_NEW = "NEW";
    public static String ADD_CUSTOMER_TYPE_EDIT = "EDIT";

    public static String USER_TYPE_POS = "PU";
    public static String USER_TYPE_SALES_MAN = "SA";
    public static String USER_TYPE_DELIVERY_MAN = "DU";

    public static String CATEGORY_FRAGMENT = "CategoryFragment";
    public static String MAIN_FRAGMENT = "MainFragment";
    public static String SALESMAN_FRAGMENT = "SalesmanFragment";
    public static String PRIVACY_POLICY_FRAGMENT = "PrivacyPolicyFragment";
    public static String PURCHASE_FRAGMENT = "PurchaseFragment";
    public static String PURCHASE_HISTORY_FRAGMENT = "PurchaseHistoryFragment";
    public static String SETTINGS_FRAGMENT = "SettingsFragment";
    public static String VENDOR_FRAGMENT = "VendorFragment";
    public static String PRODUCT_FRAGMENT = "ProductFragment";
    public static String SURVEY_FRAGMENT = "SurveyFragment";
    public static String CHOOSE_SURVEY_FRAGMENT = "ChooseSurveyFragment";
    public static String CAMPAIGN_FRAGMENT = "CampaignFragment";
    public static String POINTS_FRAGMENT = "PointsFragment";
    public static String REWARDS_FRAGMENT = "RewardsFragment";
    public static String PROFILE_PERSONALLY_FRAGMENT = "ProfilePersonallyFragment";
    public static String PRODUCT_COMBO_FRAGMENT = "ProductComboFragment";
    public static String SUPPORT_TICKETS_FRAGMENT = "SupportTicketsFragment";
    public static String ORDER_NOW_FRAGMENT = "OrderNowFragment";
    public static String COMBO_INFO_FRAGMENT = "ComboInfoFragment";
    public static String ADD_POS_FRAGMENT = "AddPOSFragment"; // Diaa
    public static String MORE_FRAGMENT = "More"; // Diaa
    public static String PROFILE_FRAGMENT = "Profile"; // Diaa

    public static String ADD_NEW_POS_USER_EXTRA = "ADD_NEW_POS_USER_EXTRA"; // Diaa
    public static String EDIT_POS_USER_EXTRA = "EDIT_POS_USER_EXTRA"; // Diaa

    public static String COMBO_OFFER = "combo_offer";
    public static boolean GET_FROM_DATA = false;

    public static String HEADERS = "HEADERS";
    public static String BODY = "BODY";
    public static String X_AUTH_TOKEN = "x-auth-token";

    public static int ADD_CUSTOMER_REQUEST_CODE = 1;

    public static int ACTIVITY_REQUEST_CODE = 1;

    public static int PERMISSIONS_REQUEST = 1;

    public static Integer PROMOTION_TYPE_MANUAL = 1;

    public static Integer PROMOTION_TYPE_SYSTEM = 2;

    public static final int OPEN_GPS_SETTINGS_REQUEST = 50;

    public static final int OPEN_WIFI_SETTINGS_REQUEST = 3;

    public static String[] PERMISSIONS_REQ = {
            android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,};

    // permissions for logged in user
    public static String ROLE_ORDER = "ROLE_ORDER";
    //    public static String ROLE_POINTS = "ROLE_POINTS";
//    public static String ROLE_CAMPAIGN = "ROLE_CAMPAIGN";
//    public static String ROLE_ORDER_CREATE = "ROLE_ORDER_CREATE";
//    public static String ROLE_ORDER_CREATE_COMBO = "ROLE_ORDER_CREATE_COMBO";
    public static String ROLE_ORDER_DETAIL_FOR_POS = "ROLE_ORDER_DETAIL_FOR_POS";
    //    public static String ROLE_ORDER_CREATE_THROUGH_VENDOR = "ROLE_ORDER_CREATE_THROUGH_VENDOR";
    public static String ROLE_POINTS_REDEMPTION = "ROLE_POINTS_REDEMPTION";
    //    public static String ROLE_CAMPAIGN_VIEW = "ROLE_CAMPAIGN_VIEW";
    public static String ROLE_CAMPAIGN_ADD_END_USER = "ROLE_CAMPAIGN_ADD_END_USER";
    public static String ROLE_CAMPAIGN_EDIT_END_USER = "ROLE_CAMPAIGN_EDIT_END_USER";
    public static String ROLE_CAMPAIGN_SEND_SMS = "ROLE_CAMPAIGN_SEND_SMS";
    //    public static String ROLE_POS_LOCATION_VIEW = "ROLE_POS_LOCATION_VIEW";
    public static String ROLE_ORDER_DETAIL_FOR_SALES = "ROLE_ORDER_DETAIL_FOR_SALES";

//    public static String ROLE_SURVEY = "ROLE_SURVEY";
//    public static String ROLE_SUPPORT_TICKET = "ROLE_SUPPORT_TICKET";


    public static String EDIT_ORDER = "edit_order";
    public static String NEW_ORDER = "new_order";

    public static String CHECKBOX_QUESTION_CODE = "C";
    public static String RADIO_QUESTION_CODE = "R";
    public static String TEXTAREA_QUESTION_CODE = "F";

    public static String TICKET_REPLIED_CODE = "RP";
    public static String TICKET_NOT_REPLIED_CODE = "NR";

    public static boolean PROMOTION_OPEND = false;

    // todo just for testing
    public static boolean VIEW_PROMOTION = false; // false for new change true for old

    // todo just for testing
    public static boolean DEFAULT_IMAGE = false; // to change default Image

    public static Long PEPSI_ID = 26l;

    public static String PEPSI_CODE = "00001";

    public static String PROMOTIONS = "promotions";
    public static String PRODUCTS = "products";

    // permissions for logged in user new by Dia'a
    public static final String ROLE_PROFILE_EDIT = "ROLE_PROFILE_EDIT";// To view and edit profile screen
    public static final String ROLE_PRIVACY_POLICY_VIEW = "ROLE_PRIVACY_POLICY_VIEW"; // To view privacy policy screen
    public static final String ROLE_SETTING_EDIT = "ROLE_SETTING_EDIT"; // To view and edit settings screen
    public static final String ROLE_POINTS_REDEEM = "ROLE_POINTS_REDEEM"; // To view redemption points and redeem rewards
    public static final String ROLE_POS_ORDERS_VIEW = "ROLE_POS_ORDERS_VIEW"; // To view POS orders and details screens
    public static final String ROLE_POS_LOCATION_VIEW = "ROLE_POS_LOCATION_VIEW"; // To view POS location screen
    public static final String ROLE_POS_ADD = "ROLE_POS_ADD"; // To create a new POS
    //    public static final String ROLE_POS_ADD = "ROLE_POS_ADD"; // To create a new POS
    public static final String ROLE_POS_ = "ROLE_POS_"; // To create a new POS
    public static final String ROLE_MY_ORDERS_VIEW = "ROLE_MY_ORDERS_VIEW"; // To view my orders screen (purchase history)
    public static final String ROLE_PRODUCT_VIEW = "ROLE_PRODUCT_VIEW"; // To view order now -> products tab screen
    public static final String ROLE_PROMOTION_VIEW = "ROLE_PROMOTION_VIEW"; // To view order now -> promotion tab screen , To view order now -> products tab -> promotion button
    public static final String ROLE_COMBO_VIEW = "ROLE_COMBO_VIEW"; // To view order now -> combo tab and combo details screens
    public static final String ROLE_PRODUCT_ADD = "ROLE_PRODUCT_ADD"; // To add product from order now -> product tab screen , To view +- icons from order now -> product tab screen
    public static final String ROLE_PROMOTION_ADD = "ROLE_PROMOTION_ADD"; // To add promotion from order now -> product tab , To add promotion from order now -> promotion tab , To enable promotion radio from order now -> promotion tab -> promotion details screen
    public static final String ROLE_COMBO_ADD = "ROLE_COMBO_ADD"; // To add combo from order now -> combo tab , To view +- icons from order now -> combo tab -> details screen
    public static final String ROLE_CART_EDIT = "ROLE_CART_EDIT"; // To view “Add to cart” button from all screens product, promotion, and combo , To view the “Cart” icon in the top right corner
    public static final String ROLE_SUPPORT_TICKET_SEND = "ROLE_SUPPORT_TICKET_SEND"; // To view and send support ticket message
    public static final String ROLE_SURVEY_SUBMIT = "ROLE_SURVEY_SUBMIT";
    public static final String ROLE_SHIP_ORDERS = "ROLE_SHIP_ORDERS";
    public static final String ROLE_DELIVER_ORDERS = "ROLE_DELIVER_ORDERS";
    // To view and submit survey
    public static final String LOG_OUT = "LOG_OUT";

    //delivery_status
    public final static String  NEW_CODE = "NE";
    public final static String  ORDER_ACKNOWLEDGED_CODE = "AC";
    public final static String ORDER_RECEIVED_CODE = "OR";
    public final static String PARTIAL_COMPLETE_CODE = "PC";
    public final static String PARTIAL_IN_COMPLETE_CODE = "PI";
    public final static String FULL_DELIVERED_CODE = "FD";
    public final static String COMPLETE_ORDER_CODE = "CO";
    public final static String ORDER_CANCEL_CODE = "CA";
    public final static String ORDER_REJECTED_CODE = "RE";
    public final static String ORDER_CLOSED_CODE = "CL";
    public final static String ORDER_OPEN_CODE = "OP";



    // Diaa
    public static final String USER_INTENT_PUT_EXTRA = "USER";
    public static final String ADD_NEW_POS_FLAG = "ADD_NEW_POS_FLAG";
    public static final int ADD_NEW_POS_FLAG_VALUE = 111;
    public static final String LOCATION_LONG_EXTRA = "LOCATION_LONGITUDE";
    public static final String LOCATION_LAT_EXTRA = "LOCATION_LATITUDE";
    public static final String PHONE_NUMBER_FLAG = "PHONE_NUMBER_FLAG";
    public static final String MOBILE_NUMBER_KEY = "com.telephoenic.orders360.controller.activities.MOBILE_NUMBER_KEY";

    public static final String PROMOTION_TYPE_QUANTITY = "QTY";
    public static final String PROMOTION_TYPE_ATTRIBUTE = "ATR";


    // To save Vendor Image
    public static final String IMAGES_DIRECTORY = Environment.getExternalStorageDirectory().toString() + "/Pharma360";
    public static final String IMAGES_VENDOR_NAME = "vendor_image.jpg";

    public static final String INTERNET_CONNECTION = "900";

    // service
    public static final String SERVICE_LOGIN = "service_login";
    public static final String SERVICE_UPDATE_RID = "service_update_rid";
    public static final String SERVICE_ESSENTIAL_DATA = "service_essential_data";
    public static final String SERVICE_BRAND = "service_brand";
    public static final String SERVICE_SALES_VENDOR = "service_sales_vendor";
    public static final String SERVICE_POS_ORDER = "service_pos_order";
    public static final String SERVICE_SALES_ORDER = "service_sales_order";
    public static final String SERVICE_SUPPORT_TYPE = "service_support_type";
    public static final String SERVICE_SAVE_TICKETS = "service_save_tickets";
    public static final String SERVICE_PRODUCT_WITH_POINTS = "service_product_with_points";
    public static final String SERVICE_REDEEM_POINTS = "service_redeem_points";
    public static final String SERVICE_SURVEY_DETAILS = "service_survey_details";
    public static final String SERVICE_SUBMIT_SURVEY = "service_submit_survey";
    public static final String SERVICE_GET_PROFILE = "service_submit_survey";
    public static final String SERVICE_GET_CITY = "service_get_city";
    public static final String SERVICE_GET_REGION = "service_get_region";
    public static final String SERVICE_SAVE_POS = "service_save_pos";
    public static final String SERVICE_UPDATE_POS = "service_update_pos";
    public static final String SERVICE_UPDATE_PASSWORD = "service_update_password";
    public static final String SERVICE_GET_POS_GROUP = "service_get_pos_group";
    public static final String SERVICE_CHECK_MOBILE_NUMBER = "service_check_mobile_number";
    public static final String SERVICE_SEND_PASS_CODE = "service_send_pass_code";
    public static final String SERVICE_CHANGE_PASSWORD = "service_change_password";
    public static final String SERVICE_ADD_TO_CART = "service_add_to_cart";
    public static final String SERVICE_GET_BUNDEL = "service_get_bundel";
    public static final String SERVICE_SAVE_ORDER = "service_save_order";
    public static final String SERVICE_GET_PROMOTION = "service_get_promotion";
    public static final String READY_TO_SHIP_STATUS = "RTS";
    public static final String SHIPPED_STATUS = "SH";
    public static final String REASON_CODE = "DNS";
    public static final String CHEQUE_CODE = "CH";
    public static final String CASH_CODE = "CA";
    public static final String CLAIM_CODE = "CL";
    public static final String CANCEL_CODE ="CO";

}