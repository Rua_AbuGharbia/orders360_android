package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Promotion extends OrderItems implements Serializable  {

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("deleted")
	private boolean deleted;

	@JsonProperty("inactive")
	private boolean inactive;

	@JsonProperty("name")
	private String name;

	@JsonProperty("startDate")
	private String startDate;

	@JsonProperty("endDate")
	private String endDate;

	@JsonProperty("vendor")
	private VendorWrapper vendor;

	@JsonProperty("products")
	List<ProductWrapper> products;

	public Promotion(Integer id) {
		this.id = id;
	}

	public Promotion( ) {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public VendorWrapper getVendor() {
		return vendor;
	}

	public void setVendor(VendorWrapper vendor) {
		this.vendor = vendor;
	}

	public List<ProductWrapper> getProducts() {
		return products;
	}

	public void setProducts(List<ProductWrapper> products) {
		this.products = products;
	}
}