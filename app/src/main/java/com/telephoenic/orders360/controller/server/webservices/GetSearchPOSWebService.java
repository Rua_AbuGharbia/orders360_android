package com.telephoenic.orders360.controller.server.webservices;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.controller.RestWebService;
import com.telephoenic.orders360.controller.server.model.PosUser;
import com.telephoenic.orders360.controller.server.model.SearchRequest;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.utils.JsonUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetSearchPOSWebService extends RestWebService {
    @Override
    protected Object callService(String... parameters) {

        SearchRequest request = JsonUtils.toJavaObject(parameters[0], SearchRequest.class);

        String url = AppConstants.URL_LIVE +
                "pos_user/filter_assigned_pos_to_sales_agent/{page}/{pageSize}/{salesAgentId}?search_term={search_term}";
        Map<String, String> uriVariables = new HashMap<String, String>();
        uriVariables.put("salesAgentId",request.getSalesSupervisorId());
        uriVariables.put("page",request.getPage().toString());
        uriVariables.put("pageSize",request.getPageSize().toString());
        uriVariables.put("search_term",request.getSearch_term().toString());
        Object response = callGet(url, uriVariables);
        return response;
    }

    public static class POSParser {
        public static List<User> parse(String json) {
            Gson gson = new Gson();
            return gson.fromJson(json, new TypeToken<List<User>>() {
            }.getType());
        }
    }

}