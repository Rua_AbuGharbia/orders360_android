package com.telephoenic.orders360.controller.viewholder;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PromotionDetailsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.RowSelect_ImageView)
    public ImageView rowSelectImageView;
    @BindView(R.id.PromotionOffer_TextView)
    public TextView promotionOfferTextView;
    @BindView(R.id.edit_text_items)
    public EditText editTextItems;
    @BindView(R.id.edit_text_free_items)
    public EditText editTextFreeItems;
    @BindView(R.id.plus_TextView)
    public TextView plusTextView;
    @BindView(R.id.Free_TextView)
    public TextView freeTextView;
    @BindView(R.id.OtherShow_ConstraintLayout)
    public ConstraintLayout otherShowConstraintLayout;

    public PromotionDetailsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
