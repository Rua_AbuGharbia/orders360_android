package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import android.widget.ImageView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.TicketModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.TicketsAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.SupportTicketViewModel;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class SupportTicketsFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {

    private String TAG = SupportTicketsFragment.class.getName();
    private PosProfileWrapper posProfileWrapper;
    private ImageView fab;
    private List<TicketModel> listOfTickets;
    private RecyclerView ticketRecyclerView;
    private ConstraintLayout constraintLayout;

    private TicketsAdapter ticketsAdapter;
    private SupportTicketViewModel supportTicketViewModel;

    public SupportTicketsFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppUtils.changeTitle(Objects.requireNonNull(getActivity()), getString(R.string.navigation_drawer_communication));
        return inflater.inflate(R.layout.fragment_support_tickets, container, false);
    }

    @SuppressLint("RestrictedApi")
    private void createComponents(View v) {
        listOfTickets = new ArrayList<>();
        posProfileWrapper = AppUtils.getProfile(getContext());
        fab = v.findViewById(R.id.fab);
        ticketRecyclerView = v.findViewById(R.id.messages_list);
        constraintLayout = v.findViewById(R.id.constraint_layout);
        constraintLayout.setVisibility(View.GONE);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoFragment(new SendMessageFragment(), Objects.requireNonNull(getActivity()).getSupportFragmentManager());
            }
        });

        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(new SendMessageFragment(), Objects.requireNonNull(getActivity()).getSupportFragmentManager());

            }
        });

        try {
            getTicketsWebService();
        } catch (ExceptionHandling exceptionHandling) {
            exceptionHandling.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getTicketsWebService() throws ExceptionHandling {
        LoadingDialog.showDialog(getActivity());
        try {
            supportTicketViewModel.getAllTicket(posProfileWrapper.getUser().getId().toString(), AppPrefs.getVendorID(getContext()).toString())
                    .observe(this, new ApiObserver<List<TicketModel>>() {
                        @Override
                        protected void noConnection() {
                            LoadingDialog.dismiss();
                            noConnectionInternetDialog();
                        }

                        @Override
                        protected void onSuccess(List<TicketModel> data) {
                            LoadingDialog.dismiss();
                            if (data.size() > 0) {
                                listOfTickets.addAll(data);
                            }
                            fullData();
                        }

                        @Override
                        protected void onError(String error) {
                            LoadingDialog.dismiss();
                            Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
                        }
                    });
        } catch (Exception e) {
            LoadingDialog.dismiss();
            throw new ExceptionHandling(e.toString());
        }
    }

    @SuppressLint("RestrictedApi")
    private void fullData() {
        if (listOfTickets.size() > 0) {
            constraintLayout.setVisibility(View.GONE);
            ticketRecyclerView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        } else {
            constraintLayout.setVisibility(View.VISIBLE);
            ticketRecyclerView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
        ticketRecyclerView.post(new Runnable() {
            public void run() {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                if (ticketsAdapter == null) {
                    ticketsAdapter = new TicketsAdapter(getContext(), R.layout.item_ticket, listOfTickets, onRecyclerClick);
                    ticketRecyclerView.setLayoutManager(mLayoutManager);
                    ticketRecyclerView.setAdapter(ticketsAdapter);
                } else {
                    ticketsAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        supportTicketViewModel = ViewModelProviders.of(this).get(SupportTicketViewModel.class);
        createComponents(view);
    }

    private OnRecyclerClick onRecyclerClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(SupportTicketsDetailsFragment.TICKET_MODEL, listOfTickets.get(position));
            SupportTicketsDetailsFragment supportTicketsDetailsFragment = new SupportTicketsDetailsFragment();
            gotoFragment(supportTicketsDetailsFragment, getFragmentManager(), bundle);
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        ticketsAdapter = null;
        LoadingDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LoadingDialog.dismiss();
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS),AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            try {
                getTicketsWebService();
            } catch (ExceptionHandling exceptionHandling) {
                exceptionHandling.printStackTrace();
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}
