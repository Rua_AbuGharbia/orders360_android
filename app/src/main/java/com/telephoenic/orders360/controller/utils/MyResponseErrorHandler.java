package com.telephoenic.orders360.controller.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class MyResponseErrorHandler implements ResponseErrorHandler {

	@Override
	public void handleError(ClientHttpResponse clienthttpresponse)
			throws IOException {
		if (clienthttpresponse.getStatusCode() == HttpStatus.FORBIDDEN) {
			System.out.println("clienthttpresponse" + clienthttpresponse.getBody());
		}
	}

	@Override
	public boolean hasError(ClientHttpResponse clienthttpresponse)
			throws IOException {

		if (clienthttpresponse.getStatusCode() != HttpStatus.OK) {

			if (clienthttpresponse.getStatusCode() == HttpStatus.FORBIDDEN) {
				return true;
			}
		}
		return false;
	}
}
