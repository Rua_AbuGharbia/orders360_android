package com.telephoenic.orders360.controller.interfaces;

import com.telephoenic.orders360.controller.server.model.AllSerialModel;

import java.util.List;

public interface GetSerialsData {
    void getSerialsData(List<AllSerialModel> allSerialModels,int quantity);
}
