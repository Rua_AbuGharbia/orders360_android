package com.telephoenic.orders360.controller.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.telephoenic.orders360.BuildConfig;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.UpDateApplicationsDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.telephoenic.orders360.controller.constants.AppConstants.PERMISSIONS_REQ;
import static com.telephoenic.orders360.controller.constants.AppConstants.PERMISSIONS_REQUEST;

public class SplashScreenActivity extends AppCompatActivity implements DialogUtils.OnDialogClickListener {

    private final String APP_CONFIG_NAME = "app_config";
    private final String VERSION_NAME = "appVersion";
    private final String VERSION_NAME_NEW = "appVersionNew";

    public String TAG = SplashScreenActivity.class.getSimpleName();
    private static final int TIME_TO_LEAVE = 1500;
    PosProfileWrapper posProfileWrapper;
    private String newVersion = "";
    private FirebaseRemoteConfig firebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);


        LanguageManager.setLanguage(this);

        Log.e(TAG, "onCreate: ");

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= 19) {
            checkPermission();
        } else {
            navigate();
        }
    }

    public void checkPermission() {
        Log.i(TAG, "Show button pressed. Checking permission.");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            navigate();
        }
    }

    private void requestPermission() {
        Log.i(TAG, "permission has NOT been granted. Requesting permission.");
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Log.i(TAG, " permission rationale to provide additional context.");
            ActivityCompat.requestPermissions(this, PERMISSIONS_REQ, PERMISSIONS_REQUEST);
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS_REQ, PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            Log.i(TAG, "Received response for  permission reqed permission result for  permission.uest.");
            navigate();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void navigate() {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        HashMap<String, Object> keyApp = new HashMap<>();
        keyApp.put("app_config", "");

        firebaseRemoteConfig.setDefaults(keyApp);
        final Task<Void> voidTask = firebaseRemoteConfig.fetch(0);
        voidTask.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                firebaseRemoteConfig.activateFetched();

                if (task.isSuccessful()) {
                    String appVersion = firebaseRemoteConfig.getString("app_config");
                    try {
                        JSONObject categoryListInJson = new JSONObject(appVersion);
                        appVersion = (String) categoryListInJson.get("appVersion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        appVersion = BuildConfig.VERSION_NAME;
                    }

                    if (!appVersion.equals(BuildConfig.VERSION_NAME)) {
                        UpDateApplicationsDialog appDateApplicationsDialog = new UpDateApplicationsDialog();
                        appDateApplicationsDialog.show(getSupportFragmentManager(), "");

                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                posProfileWrapper = AppUtils.getProfile(SplashScreenActivity.this);
                                if (posProfileWrapper != null && !
                                        posProfileWrapper.getUser().getFirstLogin()) {
                                    Bundle bundle = new Bundle();
                                    bundle.putLong(IntentExtraNames.ID, posProfileWrapper.getUser().getId());
                                    IntentFactory.startActivity(SplashScreenActivity.this, MainActivity.class, true, true, bundle);
                                } else {
                                    IntentFactory.startActivity(SplashScreenActivity.this, LoginActivity.class, true);
                                }
                            }
                        }, TIME_TO_LEAVE);
                    }
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            posProfileWrapper = AppUtils.getProfile(SplashScreenActivity.this);
                            if (posProfileWrapper != null && !
                                    posProfileWrapper.getUser().getFirstLogin()) {
                                Bundle bundle = new Bundle();
                                bundle.putLong(IntentExtraNames.ID, posProfileWrapper.getUser().getId());
                                IntentFactory.startActivity(SplashScreenActivity.this, MainActivity.class, true, true, bundle);
                            } else {
                                IntentFactory.startActivity(SplashScreenActivity.this, LoginActivity.class, true);
                            }
                        }
                    }, TIME_TO_LEAVE);
                }
            }
        });

        voidTask.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        posProfileWrapper = AppUtils.getProfile(SplashScreenActivity.this);
                        if (posProfileWrapper != null && !
                                posProfileWrapper.getUser().getFirstLogin()) {
                            Bundle bundle = new Bundle();
                            bundle.putLong(IntentExtraNames.ID, posProfileWrapper.getUser().getId());
                            IntentFactory.startActivity(SplashScreenActivity.this, MainActivity.class, true, true, bundle);
                        } else {
                            IntentFactory.startActivity(SplashScreenActivity.this, LoginActivity.class, true);
                        }
                    }
                }, TIME_TO_LEAVE);
            }
        });

//
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onOkClicked() {
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

    }

    @Override
    public void onCancelClicked() {
        onBackPressed();

    }
}