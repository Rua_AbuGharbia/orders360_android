package com.telephoenic.orders360.controller.utils;

/**
 * Created by Ihab on 3/21/2016.
 */
public class IntentExtraNames {

    public static final String ID = "id";
    public static final String PRODUCT_ID = "product_id";
    public static final String POS_ID = "pos_id";
    public static final String PRICE = "price";
    public static final String BUNDLE = "bundle";
    public static final String LIST = "list";
    public static final String COMO_ITEMS_LIST = "como_items_list";
    public static final String SINGLE_ITEMS_LIST = "single_items_list";
    public static final String TYPE = "type";
    public static final String PRODUCT_TYPE = "product_type";
    public static final String OBJECT = "object";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String NAME = "name";
    public static final String NUMBER = "number";
    public static final String REGION_NAME = "region_name";
    public static final String CITY_NAME = "city_name";
    public static final String POINTS = "points";
    public static final String DATABASE_TYPE = "database_type";
    public static final String IMAGES_NAME = "images_name";
    public static final String DESCRIPTION = "description";
    public static final String NUMBER_OF_TABS = "number_of_tabs";
    public static final String TYPE_OF_OFFER = "type_of_offer";
    public static final String INDEX = "index";
    public static final String TITLE = "title";

}
