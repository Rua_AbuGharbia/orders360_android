package com.telephoenic.orders360.controller.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.OrderMap;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rua on 26-Dec-17.
 */

@Database(entities = {User.class ,OrderMap.class }, version = 2 , exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract UserDao userDao();

    public abstract OrderDao orderDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .fallbackToDestructiveMigration()  //TODO
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

  /*  public static void saveOrderToDatabase(Context context ,User user ,OrderModel orderModel){
        try {
            AppDatabase database = AppDatabase.getAppDatabase(context);
                   String orderJson = JsonUtils.toJsonString(orderModel);
                   OrderMap order = new OrderMap();
                   order.setUserId(user.getId());
                   order.setOrderList(orderJson);
                   database.orderDao().insertOrder(order);
            AppUtils.updateBasketOrder((Activity) context);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }*/

    public static void saveOrderToDatabase(Context context ,User user){
        try {
            Long vendorId = AppPrefs.getVendorID(context);
            AppDatabase database = AppDatabase.getAppDatabase(context);
            OrderMap orderMap = database.orderDao().findOrder(user.getId(), vendorId);
            if(orderMap == null){
                List<ProductWrapper> myProductModelList = new ArrayList<>();
                List<ComboWrapper> myComboModelList = new ArrayList<>();
//                for (Object object : myOrderModelList){
//                    if(object instanceof ProductWrapper){
//                        myProductModelList.add((ProductWrapper)object);
//                    } else if(object instanceof ComboWrapper){
//                        myComboModelList.add((ComboWrapper)object);
//                    }
//                }
                String orderProductJson = JsonUtils.toJsonString(myProductModelList);
                String orderComboJson = JsonUtils.toJsonString(myComboModelList);
                OrderMap order = new OrderMap();
                order.setUserId(user.getId());
                order.setProductOrder(orderProductJson);
                order.setComboOrder(orderComboJson);
                order.setVendorId(vendorId);
                database.orderDao().insertOrder(order);
            } else {
                updateOrderFromDatabase(context , user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

  /*  public static  void updateOrderFromDatabase(Context context ,OrderMap orderMap){
        try {
            AppDatabase database = AppDatabase.getAppDatabase(context);
            database.orderDao().updateOrder(orderMap);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }*/

    public static void updateOrderFromDatabase(Context context ,User user){
        try {
            Long vendorId = AppPrefs.getVendorID(context);
            AppDatabase database = AppDatabase.getAppDatabase(context);
            OrderMap myOrder = database.orderDao().findOrder(user.getId(), vendorId);
            List<ProductWrapper> myProductModelList = new ArrayList<>();
            List<ComboWrapper> myComboModelList = new ArrayList<>();
//            for (Object object : myOrderModelList){
//                if(object instanceof ProductWrapper){
//                    myProductModelList.add((ProductWrapper)object);
//                } else if(object instanceof ComboWrapper){
//                    myComboModelList.add((ComboWrapper)object);
//                }
//            }
            String orderProductJson = JsonUtils.toJsonString(myProductModelList);
            String orderComboJson = JsonUtils.toJsonString(myComboModelList);
            myOrder.setProductOrder(orderProductJson);
            myOrder.setComboOrder(orderComboJson);
            database.orderDao().updateOrder(myOrder);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public static void getOrderFromDataBase(Context context , User user){
        try {
            Long vendorId = AppPrefs.getVendorID(context);
            AppDatabase database = AppDatabase.getAppDatabase(context);
            OrderMap orderMap = database.orderDao().findOrder(user.getId(), vendorId);
           // myOrderModelList.clear();

            if(orderMap != null) {
                Log.e("database ", orderMap.toString());
                List<ProductWrapper> myProductModelList = parseProduct(orderMap.getProductOrder());
                List<ComboWrapper> myComboModelList = parseCombo(orderMap.getComboOrder());
                List<Object> orderList = new ArrayList<>();
                orderList.addAll(myProductModelList);
                orderList.addAll(myComboModelList);
              //  myOrderModelList.addAll(orderList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

   /* public static void getOrderFromDataBase(Context context , User user){
        try {
            myOrderModelList.clear();
            AppDatabase database = AppDatabase.getAppDatabase(context);
            List<OrderMap> orderMapList = database.orderDao().findOrder(user.getId());
            for(OrderMap order : orderMapList){
                OrderModel orderModel = parse(order.getOrderList());
                orderModel.setId(order.getOrderId());
                myOrderModelList.add(orderModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Rua" , e.getMessage());
        } finally {
        }
    }*/

    public static OrderModel parse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<OrderModel>() {
        }.getType());
    }

    public static List<ProductWrapper> parseProduct(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<ProductWrapper>>() {
        }.getType());
    }

    public static List<ComboWrapper> parseCombo(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<ComboWrapper>>() {
        }.getType());
    }

    public static void deleteOrdersFromDataBase (Context context , User user){
        try {
            AppDatabase database = AppDatabase.getAppDatabase(context);
            database.orderDao().deleteOrdersByPos(user.getId(), AppPrefs.getVendorID(context));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

   /* public static void deleteOrdersFromDataBase (Context context , OrderModel orderModel){
        try {
            AppDatabase database = AppDatabase.getAppDatabase(context);
            database.orderDao().deleteOrder(orderModel.getId());
            AppUtils.updateBasketOrder((Activity) context);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }*/

}
