package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscountFilterViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Discount_RowBrand_TextView)
    public TextView discountRowBrandTextView;
    @BindView(R.id.checkbox)
    public CheckBox checkbox;

    public DiscountFilterViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
