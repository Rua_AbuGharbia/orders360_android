package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PromotionItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.Title_TextView)
    public TextView titleTextView;
    @BindView(R.id.Number_TextView)
    public TextView numberTextView;
    @BindView(R.id.line_view)
    public View lineView;

    public PromotionItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
