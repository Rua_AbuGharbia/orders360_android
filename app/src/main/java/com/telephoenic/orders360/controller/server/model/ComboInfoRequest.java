package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 02-Nov-17.
 */

public class ComboInfoRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("comboId")
    private String comboId;

    public String getComboId() {
        return comboId;
    }

    public void setComboId(String comboId) {
        this.comboId = comboId;
    }
}
