package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.analytics.ecommerce.Product;

import java.io.Serializable;

public class PromotionItemDetails extends OrderItems implements Serializable  {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("deleted")
	private boolean deleted;

	@JsonProperty("inactive")
	private boolean inactive;

	@JsonProperty("freeItem")
	private Long freeItem;

	public Long getPromotionValueId() {
		return promotionValueId;
	}

	public void setPromotionValueId(Long promotionValueId) {
		this.promotionValueId = promotionValueId;
	}

	private Long promotionValueId;

	@JsonProperty("quantity")
	private Long quantity;

	@JsonProperty("orderQuantity")
	private Long orderQuantity;

	@JsonProperty("promotionItem")
	private PromotionItem promotionItem;

	@JsonProperty("promotionType")
	private Integer promotionType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public Long getFreeItem() {
		return freeItem;
	}

	public void setFreeItem(Long freeItem) {
		this.freeItem = freeItem;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Long orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public PromotionItem getPromotionItem() {
		return promotionItem;
	}

	public void setPromotionItem(PromotionItem promotionItem) {
		this.promotionItem = promotionItem;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}
}