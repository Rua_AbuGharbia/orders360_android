package com.telephoenic.orders360.controller.customcontrols;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Ihab Alnaqib on 04/08/2016.
 */
public class IconTreeItemHolder extends TreeNode.BaseNodeViewHolder<CategoryNodeWrapper> {
    private TextView tvValue;
    private PrintView arrowView;
    private boolean hasChild;

    public IconTreeItemHolder(Context context,boolean hasChild) {
        super(context);
        AppUtils.getLayoutDirection(((Activity)context));
        this.hasChild = hasChild;
    }

    @Override
    public View createNodeView(final TreeNode node, CategoryNodeWrapper productNodeWrapper) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_icon_node, null, false);
        tvValue =  view.findViewById(R.id.node_value);

        tvValue.setText(productNodeWrapper.getName());
        /*if(AppPrefs.getLanguage(context).languge.equals("en"))
            tvValue.setText(productNodeWrapper.getEnglishTitle());
        else
            tvValue.setText(productNodeWrapper.getArabicTitle());*/

        final PrintView iconView =  view.findViewById(R.id.icon);
        //iconView.setIconText(context.getResources().getString(value.icon));

       // iconView.setIconColorRes(R.drawable.ic_back);

        arrowView =  view.findViewById(R.id.arrow_icon);

        if(!hasChild){
            arrowView.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void toggle(boolean active) {
        if(AppPrefs.getLanguage(context).equals("en"))
            arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
        else
            arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_left));
    }

    /*public static class IconTreeItem {
        public int icon;
        public String text;

        public IconTreeItem(int icon, String text) {
            this.icon = icon;
            this.text = text;
        }
    }*/
}
