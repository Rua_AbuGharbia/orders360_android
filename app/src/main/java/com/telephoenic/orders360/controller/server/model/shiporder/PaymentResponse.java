package com.telephoenic.orders360.controller.server.model.shiporder;

import java.util.List;

public class PaymentResponse {
    private List<PaymentMethodModel> content;

    public List<PaymentMethodModel> getContent() {
        return content;
    }

    public void setContent(List<PaymentMethodModel> content) {
        this.content = content;
    }
}
