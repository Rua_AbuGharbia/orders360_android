package com.telephoenic.orders360.controller.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.utils.PagerTransUtils;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;

import java.util.Objects;

public class OnBoardingActivity extends AppCompatActivity {

    private static final int MAX_STEP = 4;
    private static final int PAGER_MARGIN = 10;

    private int screenSize;
    private ViewPager viewPager;
    private TextView textNext, txtSkip;
    private String title_array[];
    private String description_array[];
    private int images_array[] = {
            R.drawable.ic_order_onbord,
            R.drawable.ic_card_onboard,
            R.drawable.ic_point_onboerd,
            R.drawable.ic_support_onboend
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_on_boarding);
        screenSize = ScreenDensityUtil.getScreenSizeWidth(this);
        title_array = getResources().getStringArray(R.array.onBoardingActivity_title_array);
        description_array = getResources().getStringArray(R.array.onBoardingActivity_description_array);
        initComponent();
    }

    private void initComponent() {
        viewPager = findViewById(R.id.view_pager);
        textNext = findViewById(R.id.button_next);
        txtSkip = findViewById(R.id.skip);
        // adding bottom dots
        bottomProgressDots(0);

        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.setClipToPadding(false);
        viewPager.setPadding(100, 0, 100, 0);
        viewPager.setPageMargin(PAGER_MARGIN);
        viewPager.setPageTransformer(true, new PagerTransUtils());
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        textNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = viewPager.getCurrentItem() + 1;
                if (current < MAX_STEP) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    startActivity(new Intent(OnBoardingActivity.this, MainActivity.class));
                    finish();
                }
            }
        });

        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OnBoardingActivity.this, MainActivity.class));
                finish();
            }
        });

    }

    private void bottomProgressDots(int current_index) {
        LinearLayout dotsLayout = findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.grey_600), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        dots[current_index].setImageResource(R.drawable.shape_circle);
        dots[current_index].setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @SuppressLint("SetTextI18n")
        @Override
        public void onPageSelected(final int position) {
            if (position == images_array.length - 1) {
                textNext.setText(R.string.finish_lebel);
                txtSkip.setVisibility(View.GONE);

            } else {
                textNext.setText(R.string.button_labels_next);
                txtSkip.setVisibility(View.VISIBLE);
            }
            bottomProgressDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = Objects.requireNonNull(layoutInflater).inflate(R.layout.item_stepper_wizard_onboarding, container, false);
            ((TextView) view.findViewById(R.id.description)).setText(description_array[position]);
            ((TextView) view.findViewById(R.id.title)).setText(title_array[position]);
            ((ImageView) view.findViewById(R.id.image)).setImageResource(images_array[position]);
            RelativeLayout layoutColor = view.findViewById(R.id.lyt_parent);
            RelativeLayout layoutImage = view.findViewById(R.id.ScreenSize_LinearLayout);
            int densityScreen = screenSize / 220;


            layoutColor.getLayoutParams().height = ((screenSize) / 2);
            layoutImage.getLayoutParams().height = (int) (screenSize - screenSize / densityScreen);
            layoutColor.requestLayout();
            layoutImage.requestLayout();
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return title_array.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}