package com.telephoenic.orders360.controller.fragments;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.PosDetailsActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;

import java.util.Objects;

public class AddPOSFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {

    private static final String TAG = "AddPOSFragment";
    @BindInput(R.id.addPOS_fragment_phoneNumber_editText)
    Orders360EditText addPOSFragmentPhoneNumberEditText;
    // views
    private Button nextButton;
    private ProgressBar progressBar;

    private PosProfileWrapper posProfile ;
    private String phoneNumber ;

    public AddPOSFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_add_po, container, false);
        AppUtils.changeTitle(getActivity(), getString(R.string.add_pos_tab_text));
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this,mView);
        initViews(mView);
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // methods
    private void initViews(View mView) {
        nextButton = mView.findViewById(R.id.addPOS_fragment_next_button);
        progressBar = mView.findViewById(R.id.addPOS_fragment_progressDialog);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPosData();
            }
        });
    }

    private void getPosUserByMobile(String mobileNumber, long userId){
        // TODO Call Request
        PosUserViewModel posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);
        posUserViewModel.getPosUserByMobile(mobileNumber, userId).observe(this, new ApiObserver<User>() {
            @Override
            protected void noConnection() {
                nextButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                noConnectionInternetDialog();

            }

            @Override
            protected void onSuccess(User data) {
                try {
                    if (data!=null) {
//                        User user = JsonUtils.toJavaObject(data.string(),User.class);
                        if (data.isAssignVendor()) {
                            showExistingPosDialog(data, getResources().getString(R.string.existing_pos_assigned_to_vendor));
                        } else {
                            showExistingPosDialog(data, getResources().getString(R.string.existing_pos_NOT_assigned_to_vendor));
                        }
                    } else {
                        showAddPosDialog();
                    }
                    nextButton.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(),error ,Toast.LENGTH_LONG).show();
                nextButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void getPosData() {
        phoneNumber = addPOSFragmentPhoneNumberEditText.getEditTextContent().trim();
        if (TextUtils.isEmpty(phoneNumber)) {
            addPOSFragmentPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_requird));
            addPOSFragmentPhoneNumberEditText.validate();
            addPOSFragmentPhoneNumberEditText.requestFocus();
            return;
        }

        if (phoneNumber.length() != 10) {
            addPOSFragmentPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_validation_min_lenght_add_pos));
            addPOSFragmentPhoneNumberEditText.validate();
            addPOSFragmentPhoneNumberEditText.requestFocus();
            return;
        }

        // check if the user enter only a digits in the mobile number Edit text.
        for (int i = 0; i < phoneNumber.length(); i++) {
            if (!Character.isDigit(phoneNumber.charAt(i))) {
                addPOSFragmentPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_validation_only_digits_allowes_add_pos));
                addPOSFragmentPhoneNumberEditText.validate();
                addPOSFragmentPhoneNumberEditText.requestFocus();
                return;
            }
        }

        if (!phoneNumber.startsWith("05")) {
            addPOSFragmentPhoneNumberEditText.setErrorMessage(getString(R.string.must_starts_with_05));
            addPOSFragmentPhoneNumberEditText.validate();
            addPOSFragmentPhoneNumberEditText.requestFocus();
            return;
        }

        User user = new User();
        user.setMobileNumber(phoneNumber);
        posProfile = AppUtils.getProfile(getActivity());

        nextButton.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        getPosUserByMobile(phoneNumber ,posProfile.getUser().getId());

        /*OrderAPI api = RetrofitClient.getInstance().create(OrderAPI.class);

        Call<User> posUserCall = api.getPosUserByMobile(AppPrefs.getToken(getActivity()),
                phoneNumber, posProfile.getUser().getId());

        posUserCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        User user = response.body();
                        if (response.body().isAssignVendor()) {
                            showExistingPosDialog(user, getResources().getString(R.string.existing_pos_assigned_to_vendor));
                        } else {
                            showExistingPosDialog(user, getResources().getString(R.string.existing_pos_NOT_assigned_to_vendor));
                        }
                    } else {
                        showAddPosDialog();
                    }
                } else {
//                    Log.e(TAG, "onResponse: " + response.code());
//                    if (response.code() == 500) {
//                        // that's means the entered mobile number is a sales agent mobile number.
////                        showSalesAgentDialog();
//                        Toast.makeText(getActivity(), getResources().getString(R.string.cant_add_as_pos), Toast.LENGTH_SHORT).show();
//                    }

                    // parse the response body …
                    ResponseAPIError error = ErrorUtils.parseError(response);
                    // … and use it to show error information

                    // … or just log the issue like we’re doing :)
                    Log.d("error message", error.getMessage());
//                    WarningDialog.show(getActivity(), error.getHttpStatus().name(), error.getMessage(), new WarningDialog.OnOkClickListener() {
//                        @Override
//                        public void onOkClicked() {
//                            Toast.makeText(getActivity(), "Arrow", Toast.LENGTH_SHORT).show();
//                        }
//                    });

                    if (error != null) {
                        WarningDialog.show(getActivity(), error.getHttpStatus().name(), error.getMessage(), new WarningDialog.OnOkClickListener() {
                            @Override
                            public void onOkClicked() {

                            }
                        });
                    }
                  *//*  DialogUtils.showInformationDialog(getActivity(), error.getMessage(), new DialogUtils.OnDialogClickListener() {
                        @Override
                        public void onOkClicked() {
                        }

                        @Override
                        public void onCancelClicked() {
                        }
                    });*//*

                }
                nextButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                nextButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });*/
    }

    private void showAddPosDialog() {
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.add_pos_dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        (dialog.findViewById(R.id.add_pos_dialog_button_close))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        (dialog.findViewById(R.id.add_pos_dialog_button_add))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), PosDetailsActivity.class);
                        intent.putExtra(AppConstants.ADD_NEW_POS_FLAG, AppConstants.ADD_NEW_POS_FLAG_VALUE);
                        intent.putExtra(AppConstants.PHONE_NUMBER_FLAG, addPOSFragmentPhoneNumberEditText.getEditTextContent().trim());
                        addPOSFragmentPhoneNumberEditText.setText("");
                        startActivity(intent);
                    }
                });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showExistingPosDialog(final User user, String message) {
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.pos_already_exist_dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        TextView messageTextView = dialog.findViewById(R.id.pos_already_exist_dialog_message_textView);
        messageTextView.setText(message);


        (dialog.findViewById(R.id.pos_already_exist_dialog_yes_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), PosDetailsActivity.class);
                        intent.putExtra(AppConstants.USER_INTENT_PUT_EXTRA, user);
                        Log.e(TAG, "onClick: " + user);
                        addPOSFragmentPhoneNumberEditText.setText("");
                        startActivity(intent);
                    }
                });

        (dialog.findViewById(R.id.pos_already_exist_dialog_no_Button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            getPosUserByMobile(phoneNumber ,posProfile.getUser().getId());
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}