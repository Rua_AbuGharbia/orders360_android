package com.telephoenic.orders360.controller.fragments;

import android.animation.Animator;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.DetailsActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.constants.MainFragmentCategories;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.interfaces.AddToCartAnim;
import com.telephoenic.orders360.controller.interfaces.ClickGetFilter;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.BundleWraper;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.OrderMap;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.Promotion;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.CircleAnimationUtil;
import com.telephoenic.orders360.controller.utils.EndlessRecyclerViewScrollListener;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.ComboAdapter;
import com.telephoenic.orders360.view.adapters.ProducatsAdapter;
import com.telephoenic.orders360.view.adapters.ProductsWithPromotionAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ProductDetailsDialog;
import com.telephoenic.orders360.viewmodel.CartViewModel;
import com.telephoenic.orders360.viewmodel.ComboViewModel;
import com.telephoenic.orders360.viewmodel.ProductViewModel;
import com.telephoenic.orders360.viewmodel.PromotionViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

import static com.telephoenic.orders360.controller.utils.AppUtils.hideSoftKeyboard;
import static com.telephoenic.orders360.controller.utils.AppUtils.isInternetConnected;

public class OrderProductFragment extends BlankFragment implements OnRecyclerClick, ClickGetFilter ,DialogUtils.OnDialogClickListener {

    private static final int NO_SELECT_ITEM = -1;
    @BindView(R.id.RightMenu_FrameLayout)
    FrameLayout RightMenuFrameLayout;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.SelectCategory_TextView)
    TextView selectCategoryTextView;
    @BindView(R.id.category_linear_layout)
    LinearLayout categoryLinear;
    @BindView(R.id.img_cpy)
    ImageView imgCpy;
    @BindView(R.id.LinearBackground_RowMyOrder_LinearLayout)
    LinearLayout LinearBackgroundRowMyOrderLinearLayout;
    @BindView(R.id.searchView_products)
    SearchView searchViewProducts;
    @BindView(R.id.Filter_ImageView)
    ImageView filterImageView;
    @BindView(R.id.Filter_TextView)
    TextView filterTextView;
    @BindView(R.id.image_empty)
    ImageView imageEmpty;
    @BindView(R.id.text_empty_message)
    TextView textEmptyMessage;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.product_listview)
    RecyclerView productListview;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    private String type;
    private VendorWrapper vendor;
    private List<ProductWrapper> listOfProducts;
    private List<ProductWrapper> listOfPromotion;
    private List<ComboWrapper> listOfCombos;
    private ProducatsAdapter productsAdapter;
    private ProductsWithPromotionAdapter promotionAdapter;
    private ComboAdapter comboAdapter;
    private int screenSize;
    private SearchView productsSearchView;
    boolean flag_loading = true;
    Bundle bundle;
    private boolean hasNext = true;
    OrderMap orderMap;
    OrderModel orderModel;
    private AddToCartModel addToCartModel;
    int pageNumber = 0;
    int pageSize = 30;
    private boolean filtered = false;
    private FilterFragment filterFragment;
    private ProductFilter productFilter;
    private CartViewModel cartViewModel;
    private ProductViewModel productViewModel;
    private PromotionViewModel promotionViewModel;
    private ComboViewModel comboViewModel;

    @BindView(R.id.TextTabOne_OrderNow_LinearLayout)
    TextView textTabOneOrderNowLinearLayout;
    @BindView(R.id.ViewTabOne_OrderNow_LinearLayout)
    View viewTabOneOrderNowLinearLayout;
    @BindView(R.id.TabOne_OrderNow_LinearLayout)
    LinearLayout tabOneOrderNowLinearLayout;
    @BindView(R.id.TextTabTow_OrderNow_LinearLayout)
    TextView textTabTowOrderNowLinearLayout;
    @BindView(R.id.ViewTabTow_OrderNow_LinearLayout)
    View viewTabTowOrderNowLinearLayout;
    @BindView(R.id.TabTow_OrderNow_LinearLayout)
    LinearLayout tabTowOrderNowLinearLayout;
    @BindView(R.id.TextTabThree_OrderNow_LinearLayout)
    TextView textTabThreeOrderNowLinearLayout;
    @BindView(R.id.ViewTabThree_OrderNow_LinearLayout)
    View viewTabThreeOrderNowLinearLayout;
    @BindView(R.id.TabThree_OrderNow_LinearLayout)
    LinearLayout tabThreeOrderNowLinearLayout;
    private PosProfileWrapper posProfileWrapper;
    private boolean isTabOneClicked = false;
    private boolean isTabTowClicked = false;
    private boolean isTabThreeClicked = false;
    private GridLayoutManager layoutManager;
    private Runnable productsRunnable, promotionRunnable, bundleRunnable;
    private Handler productsHandler;
    private ImageView animationImage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.order_product_fragment, container, false);
        ButterKnife.bind(this, view);
        if (getActivity() != null)
            screenSize = (int) (ScreenDensityUtil.getScreenSizeWidth(getActivity()) / 2.1);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        promotionViewModel = ViewModelProviders.of(this).get(PromotionViewModel.class);
        comboViewModel = ViewModelProviders.of(this).get(ComboViewModel.class);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        productsHandler = new Handler();
        createComponents(view);
        textTabOneOrderNowLinearLayout.setText(R.string.fragment_product_title);
        textTabTowOrderNowLinearLayout.setText(R.string.fragment_product_commission);
        textTabThreeOrderNowLinearLayout.setText(R.string.combo_tab_title);
        if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(getContext()))) {
            textTabTowOrderNowLinearLayout.setText(R.string.sds_title);
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            int numOfTabs = bundle.getInt(IntentExtraNames.NUMBER_OF_TABS);
        }
        switchView();
        hideSoftKeyboard(Objects.requireNonNull(getActivity()).getWindow());
        return view;
    }

    private void createComponents(View v) {
        filterFragment = new FilterFragment();
        filterFragment.onFilterClick(this);
        layoutManager = new GridLayoutManager(getContext(), 2);
        productListview.setLayoutManager(layoutManager);
        productListview.setHasFixedSize(true);
        productListview.setItemViewCacheSize(20);
        productListview.setDrawingCacheEnabled(true);
        productListview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        productListview.setNestedScrollingEnabled(false);
        productListview.getLayoutManager().setMeasurementCacheEnabled(false);
        productListview.getLayoutManager().setItemPrefetchEnabled(true);
        productsSearchView = v.findViewById(R.id.searchView_products);
        posProfileWrapper = AppUtils.getProfile(getContext());

        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawerLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        int id = productsSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        final TextView searchText = productsSearchView.findViewById(id);
        Typeface typeface = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.order360_normal_font);
        searchText.setTypeface(typeface);
        searchText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        searchText.setTextSize(16);
        if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ENGLISH)) {
            searchText.setPadding(0, 5, 0, 0);
        } else {
            searchText.setPadding(0, 16, 16, 0);
        }
        AppUtils.hideKeyboard(Objects.requireNonNull(getActivity()));

        productsSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listOfProducts.clear();
                listOfPromotion.clear();
                listOfCombos.clear();
                productsAdapter = null;
                comboAdapter = null;
                promotionAdapter = null;
                pageNumber = 0;
            }
        });
        productsSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listOfProducts.clear();
                listOfPromotion.clear();
                listOfCombos.clear();
                productsAdapter = null;
                comboAdapter = null;
                promotionAdapter = null;
                pageNumber = 0;
                getAllProduct("", type);
                return false;
            }
        });

        final EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (!flag_loading && hasNext) {
                    flag_loading = true;
                    if (type.equals(AppConstants.PRODUCTS)) {
                        pageNumber = pageNumber + pageSize;
                    } else {
                        pageNumber++;
                    }

                    String searchTerm = "";
                    if (!TextUtils.isEmpty(searchText.getText().toString().trim())) {
                        searchTerm = searchText.getText().toString().trim();
                    }
                    getAllProduct(searchTerm, type);
                    hideSoftKeyboard(Objects.requireNonNull(getActivity()).getWindow());
                }

            }
        };

        productListview.addOnScrollListener(scrollListener);
    }

    @OnClick({R.id.TabOne_OrderNow_LinearLayout, R.id.TabTow_OrderNow_LinearLayout,
            R.id.TabThree_OrderNow_LinearLayout, R.id.Filter_ImageView
            , R.id.Filter_TextView, R.id.category_linear_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.TabOne_OrderNow_LinearLayout:
                if (!isTabOneClicked) {
                    if(!isInternetConnected(getContext())){
                        noConnectionInternetDialog();
                    }
                    AppConstants.IS_PROMOTION = false;
                    searchViewProducts.setQuery("", false);
                    AppUtils.showDiscount = true;
                    productViewModel = new ProductViewModel();
                    pageNumber = 0;
                    pageSize = 30;
                    clearAdapters();
                    listOfPromotion.clear();
                    listOfProducts.clear();
                    listOfCombos.clear();
                    comboAdapter = null;
                    promotionAdapter = null;
                    productsAdapter = null;
                    productListview.removeAllViews();
                    clearFilter();
                    isTabOneClicked = true;
                    isTabTowClicked = false;
                    isTabThreeClicked = false;
                    viewTabOneOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_blue_edittext_shape));
                    viewTabTowOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
                    viewTabThreeOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
                    switchAdapter(AppConstants.PRODUCTS);
                    type = AppConstants.PRODUCTS;
                    getAllProduct("", type);
                }
                break;
            case R.id.TabTow_OrderNow_LinearLayout:
                if (!isTabTowClicked) {
                    if(!isInternetConnected(getContext())){
                        noConnectionInternetDialog();
                    }
                    AppConstants.IS_PROMOTION = true;
                    searchViewProducts.setQuery("", false);
                    AppUtils.showDiscount = false;
                    productViewModel = new ProductViewModel();
                    pageNumber = 0;
                    pageSize = 30;
                    clearAdapters();
                    listOfPromotion.clear();
                    listOfProducts.clear();
                    listOfCombos.clear();
                    comboAdapter = null;
                    productsAdapter = null;
                    promotionAdapter = null;
                    productListview.removeAllViews();
                    clearFilter();
                    isTabOneClicked = false;
                    isTabTowClicked = true;
                    isTabThreeClicked = false;
                    viewTabOneOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
                    viewTabTowOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_blue_edittext_shape));
                    viewTabThreeOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
                    switchAdapter(AppConstants.PROMOTIONS);
                    type = AppConstants.PROMOTIONS;
                    getAllProduct("", type);

                }
                break;
            case R.id.TabThree_OrderNow_LinearLayout:
                if (!isTabThreeClicked) {
                    if(!isInternetConnected(getContext())){
                        noConnectionInternetDialog();
                    }
                    searchViewProducts.setQuery("", false);
                    AppUtils.showDiscount = false;
                    productViewModel = new ProductViewModel();
                    pageNumber = 0;
                    pageSize = 30;
                    clearAdapters();
                    listOfPromotion.clear();
                    listOfProducts.clear();
                    listOfCombos.clear();
                    productsAdapter = null;
                    promotionAdapter = null;
                    comboAdapter = null;
                    productListview.removeAllViews();
                    clearFilter();
                    isTabOneClicked = false;
                    isTabTowClicked = false;
                    isTabThreeClicked = true;
                    viewTabOneOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
                    viewTabTowOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
                    viewTabThreeOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_blue_edittext_shape));
                    switchAdapter(AppConstants.COMBO_OFFER);
                    type = AppConstants.COMBO_OFFER;
                    getAllProduct("", type);
                }
                break;
            case R.id.category_linear_layout:
            case R.id.Filter_ImageView:
            case R.id.Filter_TextView:
                if (drawerLayout != null && !filtered) {
                    replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
                    drawerLayout.openDrawer(Gravity.END);
                    filtered = true;
                } else {
                    if (drawerLayout != null) {
                        drawerLayout.openDrawer(Gravity.END);
                    }
                }
                break;
        }
    }

    private void clearFilter() {
        selectCategoryTextView.setText(getString(R.string.all_categories));
        AppUtils.filterCategory = null;
        AppUtils.selectCategory = null;
        AppUtils.allBrand = null;
        AppUtils.selectBrands = null;
        AppUtils.discountFilterModels = null;
        filterFragment = new FilterFragment();
        filterFragment.onFilterClick(this);
        replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
    }

    private void switchView() {
        if (!isTabOneClicked) {
            AppUtils.showDiscount = true;
            AppUtils.filterCategory = null;
            clearFilter();
            isTabOneClicked = true;
            isTabTowClicked = false;
            isTabThreeClicked = false;
            viewTabOneOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_blue_edittext_shape));
            viewTabTowOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
            viewTabThreeOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
            switchAdapter(AppConstants.PRODUCTS);
            type = AppConstants.PRODUCTS;

        } else if (!isTabTowClicked) {
            AppUtils.showDiscount = false;
            productListview.removeAllViews();
            AppUtils.filterCategory = null;
            isTabOneClicked = false;
            isTabTowClicked = true;
            isTabThreeClicked = false;
            viewTabOneOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
            viewTabTowOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_blue_edittext_shape));
            viewTabThreeOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
            type = AppConstants.PROMOTIONS;

        } else if (!isTabThreeClicked) {
            AppUtils.showDiscount = false;
            productListview.removeAllViews();
            AppUtils.filterCategory = null;
            isTabOneClicked = false;
            isTabTowClicked = false;
            isTabThreeClicked = true;
            viewTabOneOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
            viewTabTowOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_gray_edittext_shape));
            viewTabThreeOrderNowLinearLayout.setBackground(getResources().getDrawable(R.drawable.rounded_blue_edittext_shape));
            type = AppConstants.COMBO_OFFER;
        }
    }

    @Override
    public void getPosition(int categoryPosition, int brandPosition) {
        if (categoryPosition != NO_SELECT_ITEM || brandPosition != NO_SELECT_ITEM || AppUtils.discountFilterModels != null) {
            Filter();
        } else {
            selectCategoryTextView.setText(getString(R.string.all_categories));
        }
        drawerLayout.closeDrawers();
    }

    @Override
    public void getPosition(int position) {
    }

    private void switchAdapter(String type) {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        if (AppConstants.COMBO_OFFER.equals(type)) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            productsSearchView.setQueryHint(Objects.requireNonNull(getContext()).getResources().getString(R.string.hint_search_combo));
            filterImageView.setVisibility(View.GONE);
            filterTextView.setVisibility(View.GONE);
            categoryLinear.setVisibility(View.GONE);
            textEmptyMessage.setText(R.string.no_bundle_available);
        } else if (AppConstants.PRODUCTS.equals(type)) {
            filterImageView.setVisibility(View.VISIBLE);
            filterTextView.setVisibility(View.VISIBLE);
            categoryLinear.setVisibility(View.VISIBLE);
            intiFilter();
            productsSearchView.setQueryHint(Objects.requireNonNull(getContext()).getResources().getString(R.string.hint_search_product));
            textEmptyMessage.setText(R.string.no_product_available);

        } else if (AppConstants.PROMOTIONS.equals(type)) {
            filterImageView.setVisibility(View.VISIBLE);
            filterTextView.setVisibility(View.VISIBLE);
            categoryLinear.setVisibility(View.VISIBLE);
            intiFilter();
            if (AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(getContext()))) {
                productsSearchView.setQueryHint(getContext().getResources().getString(R.string.hint_search_promotion));
                textEmptyMessage.setText(R.string.no_promotion_available);
            } else if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(getContext()))) {
                productsSearchView.setQueryHint(getContext().getResources().getString(R.string.hint_search_sds));
                textEmptyMessage.setText(R.string.no_sds_available);
            }
        }
    }

    private void intiFilter() {
        if (drawerLayout != null && !filtered) {
            replaceFragment(R.id.RightMenu_FrameLayout, filterFragment, "", null);
            filtered = true;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pageNumber = 0;
        productsAdapter = null;
        promotionAdapter = null;
        comboAdapter = null;
        listOfCombos = new ArrayList<>();
        listOfProducts = new ArrayList<>();
        listOfPromotion = new ArrayList<>();
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.order_now_text));
        createComponents(view);
    }

    private void getAllProduct(final String search, final String type) {
        vendor = new VendorWrapper();
        if (AppPrefs.getVendorID(getContext()) != -1) {
            Long vendorId = AppPrefs.getVendorID(getContext());
            vendor.setId(vendorId);
        }
        productFilter = new ProductFilter();
        productFilter.setPriceWithDiscount(false);
        if (posProfileWrapper == null) {
            posProfileWrapper = AppUtils.getProfile(getContext());
        }

        if (AppUtils.selectBrands == null && AppUtils.selectCategory == null && AppUtils.discountFilterModels == null) {
            productFilter = new ProductFilter();
            productFilter.setPriceWithDiscount(false);

        } else {
            if (AppUtils.selectBrands != null && AppUtils.selectBrands.size() > 0) {
                productFilter.setBrandIds(AppUtils.selectBrands);
            } else {
                productFilter.setBrandIds(null);
            }

            if (AppUtils.selectCategory != null) {
                productFilter.setCategoryId(AppUtils.selectCategory.getId());

            } else {
                productFilter.setCategoryId(null);
            }
            if (AppUtils.discountFilterModels != null) {
                productFilter.setPriceWithDiscount(AppUtils.discountFilterModels.get(0).isSelect());
            } else {
                productFilter.setPriceWithDiscount(false);
            }
        }
        emptyLayout.setVisibility(View.GONE);
        productListview.setVisibility(View.VISIBLE);
        if (AppConstants.COMBO_OFFER.equals(type)) {

            productsHandler.removeCallbacks(bundleRunnable);
            productsHandler.removeCallbacks(productsRunnable);
            productsHandler.removeCallbacks(promotionRunnable);

            bundleRunnable = new Runnable() {
                @Override
                public void run() {
                    paginationLoadingView.setVisibility(View.VISIBLE);
                    comboViewModel.getAllBundle(pageNumber, pageSize, vendor.getId(), posProfileWrapper.getUser().getId(), search).observe(getActivity(), new ApiObserver<BundleWraper>() {
                        @Override
                        protected void noConnection() {
                            paginationLoadingView.setVisibility(View.GONE);
                            emptyLayout.setVisibility(View.VISIBLE);
                            productListview.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onSuccess(BundleWraper data) {
                            if (isTabThreeClicked) {
                                paginationLoadingView.setVisibility(View.GONE);
                                flag_loading = false;
                                setComboAdapter(data.getContent());
                            } else {
                                data.getContent().clear();
                            }
                        }

                        @Override
                        protected void onError(String error) {
                            paginationLoadingView.setVisibility(View.GONE);
                            emptyLayout.setVisibility(View.VISIBLE);
                            productListview.setVisibility(View.GONE);

                        }
                    });

                }
            };
            productsHandler.post(bundleRunnable);

        } else if (AppConstants.PRODUCTS.equals(type)) {
            productsHandler.removeCallbacks(bundleRunnable);
            productsHandler.removeCallbacks(productsRunnable);
            productsHandler.removeCallbacks(promotionRunnable);

            productsRunnable = new Runnable() {
                @Override
                public void run() {
                    paginationLoadingView.setVisibility(View.VISIBLE);
                    if (posProfileWrapper.getUser().getPosGroup() == null) {
                        posProfileWrapper.getUser().setPosGroup((long) -1);
                    }
                    productViewModel.getAllProduct(pageNumber, pageSize, vendor.getId(), posProfileWrapper.getUser().getId(), posProfileWrapper.getUser().getPosGroup(), search, productFilter).observe(getActivity(), new ApiObserver<List<ProductWrapper>>() {
                        @Override
                        protected void noConnection() {
                            paginationLoadingView.setVisibility(View.GONE);
                            emptyLayout.setVisibility(View.VISIBLE);
                            productListview.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onSuccess(List<ProductWrapper> data) {
                            if (isTabOneClicked) {
                                paginationLoadingView.setVisibility(View.GONE);
                                listOfProducts.addAll(data);
                                if (listOfProducts.size() > 0) {
                                    hasNext = listOfProducts.size() >= 30;
                                    emptyLayout.setVisibility(View.GONE);
                                    productListview.setVisibility(View.VISIBLE);
                                    flag_loading = false;
                                    productListview.post(new Runnable() {
                                        public void run() {
                                            if (productsAdapter == null) {
                                                productListview.removeAllViews();
                                                productsAdapter = new ProducatsAdapter(getActivity(), R.layout.item_product, listOfProducts, onRecyclerClickProducats, screenSize, addToCartAnim, String.valueOf(System.currentTimeMillis()));

                                                productListview.setAdapter(productsAdapter);

                                            } else {
                                                productsAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    });
                                } else {
                                    emptyLayout.setVisibility(View.VISIBLE);
                                    productListview.setVisibility(View.GONE);
                                }
                            } else {
                                data.clear();
                                productViewModel.resetData();

                            }
                        }

                        @Override
                        protected void onError(String error) {
                            paginationLoadingView.setVisibility(View.GONE);
                            Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
                            emptyLayout.setVisibility(View.VISIBLE);
                            productListview.setVisibility(View.GONE);
                        }
                    });
                }
            };
            productsHandler.post(productsRunnable);


        } else {
            productsHandler.removeCallbacks(bundleRunnable);
            productsHandler.removeCallbacks(productsRunnable);
            productsHandler.removeCallbacks(promotionRunnable);

            paginationLoadingView.setVisibility(View.VISIBLE);
            promotionRunnable = new Runnable() {
                @Override
                public void run() {
                    promotionViewModel.getAllPromotion(pageNumber, pageSize, vendor.getId(), posProfileWrapper.getUser().getPosGroup(), search, productFilter).observe(getActivity(), new ApiObserver<PromotionWraper>() {
                        @Override
                        protected void noConnection() {
                            paginationLoadingView.setVisibility(View.GONE);
                            emptyLayout.setVisibility(View.VISIBLE);
                            productListview.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onSuccess(final PromotionWraper data) {
                            if (isTabTowClicked) {
                                paginationLoadingView.setVisibility(View.GONE);
                                listOfPromotion.addAll(data.getContent());
                                if (listOfPromotion.size() > 0) {
                                    if (listOfPromotion.size() < 30) {
                                        hasNext = false;
                                    }
                                    emptyLayout.setVisibility(View.GONE);
                                    productListview.setVisibility(View.VISIBLE);
                                    flag_loading = false;
                                    productListview.post(new Runnable() {
                                        public void run() {
                                            if (promotionAdapter == null) {
                                                productListview.removeAllViews();
                                                promotionAdapter = new ProductsWithPromotionAdapter(getContext(), R.layout.item_product, listOfPromotion
                                                        , onRecyclerClick, type, screenSize, String.valueOf(System.currentTimeMillis()));
                                                productListview.setAdapter(promotionAdapter);
                                            } else {
                                                promotionAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    });
                                } else {
                                    emptyLayout.setVisibility(View.VISIBLE);
                                    productListview.setVisibility(View.GONE);
                                }
                            } else {
                                data.getContent().clear();
                                promotionViewModel.resetData();
                            }
                        }

                        @Override
                        protected void onError(String error) {
                            paginationLoadingView.setVisibility(View.GONE);
                            Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
                            emptyLayout.setVisibility(View.VISIBLE);
                            productListview.setVisibility(View.GONE);
                        }
                    });

                }
            };
            productsHandler.post(promotionRunnable);

        }
    }

    private OnRecyclerClick onRecyclerClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listOfPromotion.size() > 0) {
                ProductWrapper productWrapper = listOfPromotion.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable(DetailsActivity.PROMOTION, productWrapper);
                bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, 3);
                bundle.putInt(IntentExtraNames.INDEX, MainFragmentCategories.PROMOTION.getIndex());
                bundle.putString(IntentExtraNames.TITLE, getContext().getResources().getString(R.string.order_now_text));
                IntentFactory.startActivity(getActivity(), DetailsActivity.class, false, true, bundle);
            }


        }
    };
    private OnRecyclerClick onRecyclerClickProducats = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listOfProducts.size() > 0) {
                ProductWrapper productWrapper = listOfProducts.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable(DetailsActivity.PROMOTION, productWrapper);
                bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, 3);
                bundle.putInt(IntentExtraNames.INDEX, MainFragmentCategories.PROMOTION.getIndex());
                bundle.putString(IntentExtraNames.TITLE, getContext().getResources().getString(R.string.order_now_text));
                IntentFactory.startActivity(getActivity(), DetailsActivity.class, false, true, bundle);
            }
        }
    };


    private AddToCartAnim addToCartAnim = new AddToCartAnim() {
        @Override
        public void onQuantityChange(ImageView cardv, final int position) {

            animationImage = cardv;

            LoadingDialog.showDialog(getActivity());

            List<AddToCartModel> saveCart = new ArrayList<>();
            VendorWrapper vendorWrapper = new VendorWrapper();
            vendorWrapper.setId(AppPrefs.getVendorID(getContext()));
            SaveCart.getInstance().getSelectVendorCart().setPosUser(posProfileWrapper.getUser());
            SaveCart.getInstance().getSelectVendorCart().setVendor(vendorWrapper);
            SaveCart.getInstance().prepearCartToSend(SaveCart.getInstance().getSelectVendorCart());
            saveCart.add(SaveCart.getInstance().getSelectVendorCart());

            cartViewModel.addToCart(saveCart).observe(getActivity(), new ApiObserver<ResponseBody>() {
                @Override
                protected void noConnection() {
                    LoadingDialog.dismiss();
                    noConnectionInternetDialog();

                }

                @Override
                protected void onSuccess(ResponseBody data) {
                    LoadingDialog.dismiss();
                    addProductToCart(position);

                }

                @Override
                protected void onError(String error) {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

                }
            });

//            if (save[0]) {
//
//                if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ENGLISH))
//                    makeFlyAnimation(cardv, position);
//
//                AppUtils.updateNotifactionOrder(getActivity());
//                AppUtils.showAlertToast(getActivity().getString(R.string.fragment_product_add_to_cart_message), getContext());
//            }

        }

    };

    private void addProductToCart(final int position) {
        if (SharedPreferenceManager.getLanguage(getActivity()).equals(LanguageManager.ENGLISH))
            makeFlyAnimation(animationImage, position);

        AppUtils.updateNotifactionOrder(getActivity());
        AppUtils.showAlertToast(getActivity().getString(R.string.fragment_product_add_to_cart_message), getContext());
    }

    private void makeFlyAnimation(ImageView targetView, final int position) {

        new CircleAnimationUtil().attachActivity(getActivity()).setTargetView(targetView).setMoveDuration(200).setDestView(DetailsActivity.cartImage).setAnimationListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {


            }

            @Override
            public void onAnimationEnd(Animator animation) {


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }


    @Override
    public void onPause() {
        super.onPause();
        searchViewProducts.setQuery("", false);
        clearAdapters();
        productListview.removeAllViews();
        pageNumber = 0;
        productsAdapter = null;
        promotionAdapter = null;
        comboAdapter = null;
        listOfCombos = new ArrayList<>();
        listOfProducts = new ArrayList<>();
        listOfPromotion = new ArrayList<>();
    }


    @Override
    public void onResume() {
        super.onResume();
        if(!isInternetConnected(getContext())){
            noConnectionInternetDialog();
        }
        getAllProduct("", type);
        productsSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                clearAdapters();
                listOfPromotion.clear();
                listOfProducts.clear();
                listOfCombos.clear();
                promotionAdapter = null;
                productsAdapter = null;
                comboAdapter = null;
                pageNumber = 0;
                getAllProduct(query, type);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText != null) {
                    clearAdapters();
                    listOfPromotion.clear();
                    listOfProducts.clear();
                    listOfCombos.clear();
                    promotionAdapter = null;
                    productsAdapter = null;
                    comboAdapter = null;
                    pageNumber = 0;
                    if (newText.equals("")) {
                        getAllProduct(newText, type);
                    }
                    return false;

                } else {
                    return true;
                }
            }
        });
        productsSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                getAllProduct("", type);

                return false;
            }
        });
        hideSoftKeyboard(Objects.requireNonNull(getActivity()).getWindow());
        AppUtils.updateNotifactionOrder(getActivity());

    }

    private View.OnClickListener onIconClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int indexClick = (Integer) v.getTag();
            switch (v.getId()) {
                case R.id.image_combo:
                    if (listOfCombos.size() > 0) {
                        ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(getActivity(),
                                listOfCombos.get(indexClick).getName(), listOfCombos.get(indexClick).getLogoImageName(), null, listOfCombos.get(indexClick).getId(), ProductDetailsDialog.BUNDLE);
                        productDetailsDialog.show();
                    }
                    break;
                case R.id.button_details:
                    if (listOfCombos.size() > 0) {
                        ComboWrapper comboWrapper = listOfCombos.get(indexClick);
                        ComboWrapper newComboWrapper = new ComboWrapper();
                        newComboWrapper.setId(comboWrapper.getId());
                        newComboWrapper.setName(comboWrapper.getName());
                        newComboWrapper.setPrice(comboWrapper.getPrice());
                        newComboWrapper.setListOfComboInfo(comboWrapper.getListOfComboInfo());
                        newComboWrapper.setStartDate(comboWrapper.getStartDate());
                        newComboWrapper.setEndDate(comboWrapper.getEndDate());
                        newComboWrapper.setCode(comboWrapper.getCode());
                        newComboWrapper.setDeleted(comboWrapper.isDeleted());
                        newComboWrapper.setInactive(comboWrapper.isInactive());
                        newComboWrapper.setCount(comboWrapper.getCount());
                        newComboWrapper.setLogoImageName(comboWrapper.getLogoImageName());
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(DetailsActivity.BUNDLE, newComboWrapper);
                        bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, 3);
                        bundle.putInt(IntentExtraNames.INDEX, MainFragmentCategories.BUNDLE.getIndex());
                        bundle.putString(IntentExtraNames.TITLE, getContext().getResources().getString(R.string.combo_info_title));
                        IntentFactory.startActivity(getActivity(), DetailsActivity.class, false, true, bundle);
                    }
                    break;
            }
        }
    };

    private void Filter() {
        if (AppUtils.selectCategory != null) {
            selectCategoryTextView.setText(AppUtils.selectCategory.getName());
        } else {
            selectCategoryTextView.setText(getString(R.string.all_categories));

        }

        productListview.removeAllViews();
        clearAdapters();
        listOfProducts.clear();
        listOfCombos.clear();
        listOfPromotion.clear();
        productsAdapter = null;
        comboAdapter = null;
        promotionAdapter = null;
        pageNumber = 0;
        emptyLayout.setVisibility(View.GONE);
        getAllProduct(productsSearchView.getQuery().toString(), type);

    }

    private void setComboAdapter(List<ComboWrapper> comboWrapperList) {
        listOfCombos.addAll(comboWrapperList);
        if (listOfCombos.size() > 0) {
            hasNext = listOfCombos.size() >= 30;
            emptyLayout.setVisibility(View.GONE);
            productListview.setVisibility(View.VISIBLE);
            productListview.post(new Runnable() {
                public void run() {
                    if (comboAdapter == null) {
                        productListview.removeAllViews();
                        comboAdapter = new ComboAdapter(getContext(), R.layout.item_combo, listOfCombos, onIconClicked, screenSize, String.valueOf(System.currentTimeMillis()));
                        productListview.setAdapter(comboAdapter);

                    } else {
                        comboAdapter.notifyDataSetChanged();
                    }
                }
            });
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            productListview.setVisibility(View.GONE);
        }
    }

    private void clearAdapters() {
        if (productsAdapter != null && listOfProducts != null) {
            productsAdapter.notifyItemRangeRemoved(0, listOfProducts.size());
            productsAdapter.notifyDataSetChanged();
        }
        if (promotionAdapter != null) {
            promotionAdapter.notifyItemRangeRemoved(0, listOfPromotion.size());
            promotionAdapter.notifyDataSetChanged();
        }
        if (comboAdapter != null) {
            comboAdapter.notifyItemRangeRemoved(0, listOfCombos.size());
            comboAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onOkClicked() {
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));  //Or ACTION_WIRELESS_SETTINGS
    }

    @Override
    public void onCancelClicked() {

    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }

}
