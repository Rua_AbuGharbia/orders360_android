package com.telephoenic.orders360.controller.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.ImagesModel;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.view.adapters.FullScreenImageAdapter;

import java.util.ArrayList;
import java.util.List;


public class FullScreenImageFragment extends BlankFragment {

    ViewPager productsPhotos ;
    String imagesName , productDescription ;
    int productID ;
    TextView textDescription ;

    private List<ImageView> imageViews;
    LinearLayout imagesLayout ;
    List<ImagesModel> imagesModelList ;

    public FullScreenImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_full_screen_image, container, false);
    }

    private void createComponents(View v){

        productsPhotos = v.findViewById(R.id.pager);
        textDescription = v.findViewById(R.id.text_description);
        imagesLayout = v.findViewById(R.id.layout_images_number);

        imagesModelList = new ArrayList<>();

        Bundle bundle = getArguments();
        if(bundle!= null){
            imagesName = bundle.getString(IntentExtraNames.IMAGES_NAME);
            productDescription = bundle.getString(IntentExtraNames.DESCRIPTION);
            productID = bundle.getInt(IntentExtraNames.ID);
        }

        if(productDescription!=null){
            textDescription.setText(productDescription);
        }

        if(imagesName!= null){
            if(imagesName.contains(";")){
                String[] imageList = imagesName.split(";");
                for(int i =0 ;i<imageList.length ;i++){
                    imagesModelList.add(new ImagesModel(productID,imageList[i]));
                }
            } else {
                imagesModelList.add(new ImagesModel(productID,imagesName));
            }
        //    FullScreenImageAdapter adapter = new FullScreenImageAdapter(getActivity(),imagesModelList,String.valueOf(System.currentTimeMillis()));
          //  productsPhotos.setAdapter(adapter);
            initViews();
        }


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void initViews() {
        imageViews = new ArrayList<>();
        //LinearLayout, which contains several child TextViews and ImageViews ...
        for (int i = 0; i < imagesModelList.size(); i++) {
            ImageView imageView = new ImageView(getActivity());
            if(i==0){
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.loading1));
            }else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.loading2));
            }
            imageViews.add(imageView);
            imagesLayout.addView(imageView);
        }
        changeViews();
    }

    private void changeViews(){
        //LinearLayout, which contains several child TextViews and ImageViews ...
        for (int i = 0; i < imagesLayout.getChildCount(); i++) {
            ImageView imageView = ((ImageView) imagesLayout.getChildAt(i));
            if(productsPhotos.getCurrentItem()==i){
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.loading1));
            }else{
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.loading2));
            }
        }
    }

}
