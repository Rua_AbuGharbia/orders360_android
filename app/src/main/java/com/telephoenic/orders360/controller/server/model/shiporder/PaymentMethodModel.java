package com.telephoenic.orders360.controller.server.model.shiporder;

import android.util.Log;

import java.io.Serializable;
import java.util.List;

public class PaymentMethodModel implements Serializable {
    private Long id;
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
