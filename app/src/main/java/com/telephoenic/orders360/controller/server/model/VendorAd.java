package com.telephoenic.orders360.controller.server.model;

/**
 * Created by Ru'a on 8/21/2019.
 */

public class VendorAd {

    private Long id ;
    private String description;
    private String imagesName;
    private VendorWrapper vendor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagesName() {
        return imagesName;
    }

    public void setImagesName(String imagesName) {
        this.imagesName = imagesName;
    }

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    @Override
    public String toString() {
        return "VendorAd{" +
                "description='" + description + '\'' +
                ", imagesName='" + imagesName + '\'' +
                ", vendor=" + vendor +
                '}';
    }
}
