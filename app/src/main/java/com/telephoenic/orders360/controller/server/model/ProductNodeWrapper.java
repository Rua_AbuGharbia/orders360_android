package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;
import java.util.List;

public class ProductNodeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String arabicTitle;
	private String englishTitle;
	private boolean hasChild;
	private List<ProductNodeWrapper> nodes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getArabicTitle() {
		return arabicTitle;
	}

	public void setArabicTitle(String arabicTitle) {
		this.arabicTitle = arabicTitle;
	}

	public String getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}

	public boolean isHasChild() {
		return hasChild;
	}

	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}

	public List<ProductNodeWrapper> getNodes() {
		return nodes;
	}

	public void setNodes(List<ProductNodeWrapper> nodes) {
		this.nodes = nodes;
	}

}
