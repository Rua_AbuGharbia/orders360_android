package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;
import java.util.List;

public class ArrivalSuccessfulModel implements Serializable {
    private ShipmentArrival shipmentArrival;
    private List<CashPayments> cashPayments;
    private List< ChequePaymentsModel> chequePayments;
    private List <ClaimPayments> claimPayments;

    public ShipmentArrival getShipmentArrival() {
        return shipmentArrival;
    }

    public void setShipmentArrival(ShipmentArrival shipmentArrival) {
        this.shipmentArrival = shipmentArrival;
    }

    public List<CashPayments> getCashPayments() {
        return cashPayments;
    }

    public void setCashPayments(List<CashPayments> cashPayments) {
        this.cashPayments = cashPayments;
    }

    public List<ChequePaymentsModel> getChequePayments() {
        return chequePayments;
    }

    public void setChequePayments(List<ChequePaymentsModel> chequePayments) {
        this.chequePayments = chequePayments;
    }

    public List<ClaimPayments> getClaimPayments() {
        return claimPayments;
    }

    public void setClaimPayments(List<ClaimPayments> claimPayments) {
        this.claimPayments = claimPayments;
    }
}
