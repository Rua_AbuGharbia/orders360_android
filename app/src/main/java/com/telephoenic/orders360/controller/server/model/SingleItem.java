package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class SingleItem extends OrderItems implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("deleted")
    private boolean deleted;

    @JsonProperty("inactive")
    private boolean inactive;

    @JsonProperty("deliverdQuantity")
    private Integer deliverdQuantity;

    @JsonProperty("product")
    private ProductWrapper product;

    @JsonProperty("discountedPrice")
    private Double discountedPrice;


    private boolean hasDiscount;

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    private Double discountPercentage;

    public boolean isHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }


    public Integer getId() {
        return id;
    }

    @JsonProperty("quantityToValuePromotion")
    private Map<Long, Long> quantityToValuePromotion;

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public Integer getDeliverdQuantity() {
        return deliverdQuantity;
    }

    public void setDeliverdQuantity(Integer deliverdQuantity) {
        this.deliverdQuantity = deliverdQuantity;
    }

    public ProductWrapper getProduct() {
        return product;
    }

    public void setProduct(ProductWrapper product) {
        this.product = product;
    }

    public Double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Map<Long, Long> getQuantityToValuePromotion() {
        return quantityToValuePromotion;
    }

    public void setQuantityToValuePromotion(Map<Long, Long> quantityToValuePromotion) {
        this.quantityToValuePromotion = quantityToValuePromotion;
    }
}