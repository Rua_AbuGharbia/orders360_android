package com.telephoenic.orders360.controller.server.model.shiporder;

public class SingleSerialModel {
    private String serial;

    public SingleSerialModel(String serial) {
        this.serial = serial;
    }

    public SingleSerialModel() {
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
