package com.telephoenic.orders360.controller.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.DetailsActivity;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.CustomerModel;
import com.telephoenic.orders360.controller.server.model.DiscountFilterModel;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.SingleItem;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSaveData;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipmentArrival;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.model.DialogWrapper;
import com.telephoenic.orders360.view.customcontrols.CustomDialog;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;


public class AppUtils {
    public static Locale sysLocale;
    private static final String TAG = "AppUtils";

    public static boolean comboType = false;
    public static boolean specialType = false;
    public static List<CategoryNodeWrapper> filterCategory;
    public static List<CategoryNodeWrapper> allCategory;
    public static CategoryNodeWrapper selectCategory;
    public static List<Long> selectBrands;
    public static List<BrandWrapper> allBrand;
    public static List<DiscountFilterModel> discountFilterModels;
    public static Boolean change = null;
    public static Boolean showDiscount = null;
    public static ArrivalSaveData arrivalSaveData = null;
    public static List<Object> paymentMethodModelList = null;

    public static void hideKeyboard(EditText... editTexts) {
        for (EditText editText : editTexts) {
            if (editText == null) {
                continue;
            }
            editText.clearFocus();
            InputMethodManager imm = (InputMethodManager) Order360Application.getInstance().getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public static void openKeyBoard() {
        //open KeyBoard
        InputMethodManager imm = (InputMethodManager) Order360Application.getInstance().getApplicationContext().getSystemService(Order360Application.getInstance().getApplicationContext().INPUT_METHOD_SERVICE);
        //InputMethodManager imm = (InputMethodManager) context.getSystemService(I);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static String getSerial(Context context) {
        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String serialNumber = tel.getSimSerialNumber();
        int maxDigits = Math.min(18, serialNumber.length());
        return serialNumber.substring(0, maxDigits);
    }

    public static int getVersionCode(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static void showAlertToast(String message, Context context) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        //toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            return true;
        } else {
            return false;
        }
    }

    // this method to resize the images before set them to the cardviews
    public Drawable resizeImage(int imageResource, Context context) {
        // Get device dimensions
        Display display = ((Activity) context.getApplicationContext()).getWindowManager().getDefaultDisplay();
        double deviceWidth = display.getWidth();

        BitmapDrawable bd = (BitmapDrawable) context.getResources().getDrawable(
                imageResource);
        double imageHeight = bd.getBitmap().getHeight();
        double imageWidth = bd.getBitmap().getWidth();

        double ratio = deviceWidth / imageWidth;
        int newImageHeight = (int) (imageHeight * ratio);

        Bitmap bMap = BitmapFactory.decodeResource(context.getResources(), imageResource);
        Drawable drawable = new BitmapDrawable(context.getResources(), getResizedBitmap(bMap, newImageHeight, (int) deviceWidth));

        return drawable;
    }

    /******** Resize Bitmap ***********/
    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();
        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }

    public static void hideSoftKeyboard(Window window) { // added by Dia'a
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void showAlertDialog(final DialogWrapper dialogWrapper) {
        if (dialogWrapper == null)
            return;
        try {
            final CustomDialog errorDialog = new CustomDialog(dialogWrapper.getContext());
            errorDialog.setTitle(dialogWrapper.getTitle());
            errorDialog.setMessage(dialogWrapper.getMessage());
            errorDialog.setCancelable(false);
            errorDialog.show();
            errorDialog.getOkButton().setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    errorDialog.dismiss();
                    if (dialogWrapper.isFinished()) {
                        ((Activity) dialogWrapper.getContext()).finish();
                    }
                }
            });
        } catch (Exception e) {
            Log.i("App Utils", e.getMessage());
            e.getMessage();
        }
    }

    public static String getSystemDateTime(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format, new Locale("en"));
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static boolean isValidEmailAddress(String emailAddress) {
        String emailRegEx;
        Pattern pattern;
        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
        // Compare the regex with the email address
        pattern = Pattern.compile(emailRegEx);
        Matcher matcher = pattern.matcher(emailAddress);
        if (!matcher.find()) {
            return false;
        }
        return true;
    }

    public static void updateLocale(Context context, String languageCode) {
        Locale locale = null;
        if (languageCode.length() > 2) {
            char ch3 = languageCode.charAt(2);
            if (ch3 == '_') {
                String language = languageCode.substring(0, 2);
                String country = languageCode.substring(3, 5);
                locale = new Locale(language, country);
            }
        } else {
            locale = new Locale(languageCode);
        }

        Configuration config = context.getResources().getConfiguration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

    }

    /**
     * Get App Shared Preferences
     */
    public static SharedPreferences getAppSharedPreferences(Context context) {
        SharedPreferences settings = context.getSharedPreferences(
                context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);

        return settings;
    }

    public static String parseDate(String time) {

        String inputPattern = "dd-MM-yyyy";

        String outputPattern = "dd-MMM-yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);

            Log.i("mini", "Converted Date Today:" + str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static Date parseDate1(String time) {

        String inputPattern = "dd-MM-yyyy";

        String outputPattern = "dd-MMM-yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);

            Log.i("mini", "Converted Date Today:" + str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean checkIntervalDate(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return true;
        }
        long diff = Math.abs(fromDate.getTime() - toDate.getTime());
        long diffDays = diff / (24 * 60 * 60 * 1000);
        if (diffDays > 6) {
            return false;
        } else {
            return true;
        }

    }

    public static String getLayoutDirection(Activity activity) {

        sysLocale = activity.getApplicationContext().getResources().getConfiguration().locale;
        // left & right
        if (sysLocale.getLanguage().equals("ar")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            return AppConstants.RIGHT_TO_LEFT;
        } else if (sysLocale.getLanguage().equals("en")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        return AppConstants.LEFT_TO_RIGHT;
    }

    public static void initToolbar(ActionBar actionBar, Activity activity, String title, boolean needBack) {
        if (actionBar != null) {
            if (needBack) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                // left & right
                if (AppUtils.getLayoutDirection(activity).equals(AppConstants.RIGHT_TO_LEFT)) {
                    actionBar.setHomeAsUpIndicator(R.drawable.ic_back_ar);
                } else if (AppUtils.getLayoutDirection(activity).equals(AppConstants.LEFT_TO_RIGHT)) {
                    actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
                }

            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(title);
        }
    }

    public static void changeTitle(ActionBar actionBar, String title) {
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public static void refreshActivity(Activity activity) {
        Intent intent = activity.getIntent();
        activity.finish();
        activity.startActivity(intent);
    }

    public static String getTitle(Fragment fragment, Context context) {
        String title = "";
        String fragmentName = fragment.getClass().getName();
        if (fragmentName.contains(AppConstants.CATEGORY_FRAGMENT)) {
            title = context.getResources().getString(R.string.title_category);
        } else if (fragmentName.contains(AppConstants.MAIN_FRAGMENT) || fragmentName.contains(AppConstants.SALESMAN_FRAGMENT)) {
            title = context.getResources().getString(R.string.home_page_title);
        } else if (fragmentName.contains(AppConstants.PRIVACY_POLICY_FRAGMENT)) {
            title = context.getResources().getString(R.string.navigation_drawer_privacy_provisions);
        } else if (fragmentName.contains(AppConstants.PURCHASE_FRAGMENT)) {
            title = context.getResources().getString(R.string.title_orders);
        } else if (fragmentName.contains(AppConstants.PURCHASE_HISTORY_FRAGMENT)) {
            title = context.getResources().getString(R.string.navigation_drawer_purchase_history);
        } else if (fragmentName.contains(AppConstants.SETTINGS_FRAGMENT)) {
            title = context.getResources().getString(R.string.navigation_drawer_settings);
        } else if (fragmentName.contains(AppConstants.VENDOR_FRAGMENT) || fragmentName.contains(AppConstants.PRODUCT_COMBO_FRAGMENT)) {
            title = context.getResources().getString(R.string.fragment_vendor_title);
        }/*else if(fragmentName.contains(AppConstants.PRODUCT_FRAGMENT) ){
            if(comboType){
                title = Order360Application.getInstance().getResources().getString(R.string.button_label_combo_offers);
            }else {
                title = Order360Application.getInstance().getResources().getString(R.string.fragment_product_title);
            }
        }*/ else if (fragmentName.contains(AppConstants.SURVEY_FRAGMENT) || fragmentName.contains(AppConstants.CHOOSE_SURVEY_FRAGMENT)) {
            title = context.getResources().getString(R.string.navigation_drawer_survey);
        } else if (fragmentName.contains(AppConstants.CAMPAIGN_FRAGMENT)) {
            title = context.getResources().getString(R.string.navigation_drawer_campaign);
        } else if (fragmentName.contains(AppConstants.POINTS_FRAGMENT)) {
            title = context.getResources().getString(R.string.fragment_points_title);
        } else if (fragmentName.contains(AppConstants.REWARDS_FRAGMENT)) {
            title = context.getResources().getString(R.string.fragment_rewards_title);
        } else if (fragmentName.contains(AppConstants.PROFILE_PERSONALLY_FRAGMENT)) {
            title = context.getResources().getString(R.string.title_my_profile);
        } else if (fragmentName.contains(AppConstants.SUPPORT_TICKETS_FRAGMENT)) {
            title = context.getResources().getString(R.string.navigation_drawer_communication);
        } else if (fragmentName.contains(AppConstants.ORDER_NOW_FRAGMENT)) {
            if (specialType) {
                title = context.getResources().getString(R.string.button_label_special_offers);
            } else {
                title = context.getResources().getString(R.string.button_label_order_now);
            }
        } else if (fragmentName.contains(AppConstants.COMBO_INFO_FRAGMENT)) {
            if (specialType) {
                title = context.getResources().getString(R.string.button_label_special_offers);
            } else {
                title = context.getResources().getString(R.string.button_label_order_now);
            }
            // Diaa
        } else if (fragmentName.contains(AppConstants.ADD_POS_FRAGMENT)) {
            title = context.getResources().getString(R.string.add_pos_tab_text);
        }
        // Diaa
        else if (fragmentName.contains(AppConstants.MORE_FRAGMENT)) {
            title = context.getResources().getString(R.string.more_tab_text);
        } else if (fragmentName.contains(AppConstants.PROFILE_FRAGMENT)) {
            title = context.getResources().getString(R.string.profile_text);
        }
        return title;
    }

    public static List<CustomerModel> listCustomerParse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<CustomerModel>>() {
        }.getType());

    }

    public static List<OrderHistoryModel> listOrderParse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<OrderHistoryModel>>() {
        }.getType());
    }

    public static List<OrderModel> listOrderModelParse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<OrderModel>>() {
        }.getType());
    }

    public static List<ComboItem> listComboItemParse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<ComboItem>>() {
        }.getType());
    }

    public static List<PromotionItem> listPromotionItemParse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<PromotionItem>>() {
        }.getType());
    }

    public static List<SingleItem> listSingleItemParse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<SingleItem>>() {
        }.getType());
    }

    public static String stringToBase64(String text) {
        String base64 = "";
        try {
            byte[] data = text.getBytes(StandardCharsets.UTF_8);
            base64 = new String(Base64.encode(data, Base64.NO_CLOSE));
            // base64 = Base64.encodeToString(text.getBytes(),0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

        return base64;
    }

    public static PosProfileWrapper getProfile(Context context) {
        PosProfileWrapper posProfileWrapper = null;
        String json = AppPrefs.getPOS(context);
        Gson gson = new Gson();
        if (json != null) {
            posProfileWrapper = gson.fromJson(json, PosProfileWrapper.class);
        }
        return posProfileWrapper;
    }

    public static void updateNotifactionOrder(Activity activity) {
        Log.e(TAG, "updateNotifactionOrder: ");
        if (activity == null) {
            return;
        }
        AHBottomNavigation bottomNavigation = activity.findViewById(R.id.navigation);
        String notification = "";
        if (SaveCart.getInstance().NotifactionCount() > 0) {
            notification = SaveCart.getInstance().NotifactionCount() + "";
        } else {
            notification = null;

        }
        Log.e(TAG, "updateNotifactionOrder: notification " + notification);

//        if (myOrderModelList != null) {
//            if (myOrderModelList.size() > 0) {
//                notification = myOrderModelList.size() + "";
//            } else if (myOrderModelList.size() == 0) {
//                notification = null;
//            } else {
//                notification = "";
//            }
//        }

        if (activity instanceof MainActivity) {
            try {
                String cartTitle = activity.getResources().getString(R.string.navigation_drawer_cart);
                if (bottomNavigation.getItem(2).getTitle(activity).equals(cartTitle)) {
                    bottomNavigation.setNotification(notification, 2);
                }
            } catch (Exception e) {

            }
        } else if (activity instanceof DetailsActivity) {
            if (notification == null) {
                TextView countCart;
                Toolbar toolbar = activity.findViewById(R.id.toolbar);
                countCart = toolbar.findViewById(R.id.CountOrder_TestView);
                if (countCart.getVisibility() == View.VISIBLE) {
                    countCart.setVisibility(View.GONE);
                }
            } else if (!notification.equals("")) {
                TextView countCart;
                Toolbar toolbar = activity.findViewById(R.id.toolbar);
                countCart = toolbar.findViewById(R.id.CountOrder_TestView);
                if (countCart.getVisibility() == View.GONE) {
                    countCart.setVisibility(View.VISIBLE);
                }
                countCart.setText(notification);
            }
        }
    }

    public static void changeTitle(@NonNull Activity activity, @NonNull String title) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.textViewTitle);
        textViewTitle.setText(title);
    }

    public static void changeVendorLogo(Activity activity) {
        if (activity != null) {
            Toolbar toolbar = activity.findViewById(R.id.toolbar);
            CircleImageView logo = toolbar.findViewById(R.id.VendorLogo_ToolBar_CircleImageView);
            Glide.with(activity).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(activity) + "/" + AppPrefs.getVendorLogo(activity)).dontTransform()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis()))).into(logo);
        }
    }

    // used in OnBoardingActivity
    public static void setSystemBarColor(Activity act, @ColorRes int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = act.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(act.getResources().getColor(color));
        }
    }

    // used in OnBoardingActivity
    public static void setSystemBarLight(Activity act) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View view = act.findViewById(android.R.id.content);
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
        }
    }

    public static void writeExceptionsFile(String fileName, String folderName, String textData) {
        try {
            if (textData == null || textData.equalsIgnoreCase("null") || textData.isEmpty()) {
                return;
            }

            String exStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File myNewFolder = new File(exStorageDirectory + "/" + folderName + "/");

            if (!myNewFolder.exists()) {
                try {
                    myNewFolder.mkdirs();
                } catch (Exception e) {
                }
            }

            File file = new File(myNewFolder, fileName + ".txt");

            if (!file.exists()) {
                try {
                    fileName = fileName.replace("-", "_");
                    fileName = fileName.replace(":", "_");
                    fileName = fileName.replace(" ", "(");

                    file.createNewFile();

                } catch (IOException e) {
                }
            }
            FileWriter fr = new FileWriter(file, true);
            fr.append(textData);
            fr.write("\r\n");
            fr.close();

        } catch (Exception ex) {
        }
    }

    public static ArrayList<RadioGroup> getAllRadioGroup(View view) {
        ViewGroup vg = (ViewGroup) view;

        ArrayList<RadioGroup> radioGroups = new ArrayList<>();

        for (int i = 0; i < vg.getChildCount(); i++) {
            View vv = vg.getChildAt(i);
            if (vg.getChildAt(i) instanceof RadioGroup) {
                radioGroups.add((RadioGroup) vv);
            }
        }


        return radioGroups;
    }

    protected void saveFile(String name) {
        File mainFolder = new File(AppConstants.IMAGES_DIRECTORY);
        mainFolder.mkdirs();
        File folder = new File(mainFolder.toString() + "/" + name);
        folder.mkdirs();
    }

    public static void goToLocation(Double latitude, Double longitude, Context context) {
        if (latitude != null && longitude != null && latitude != 0 && longitude != 0) {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=&daddr=" + latitude + "," + longitude));
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Location not available", Toast.LENGTH_SHORT).show();
        }
    }

    public static String amountFormat(@NonNull double amount) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat formatter = (DecimalFormat) nf;
        formatter.applyPattern("#,###,###.##");
        return formatter.format(amount);
    }

}
