package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.LoginActivity;
import com.telephoenic.orders360.controller.activities.PosUserLocationActivity;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.server.model.MoreItemChild;
import com.telephoenic.orders360.controller.server.model.MoreListItem;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.MoreChildAdapter;
import com.telephoenic.orders360.view.adapters.MoreRecyclerViewAdapter;
import com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog;
import com.telephoenic.orders360.view.customcontrols.CustomAlertDialog;
import com.telephoenic.orders360.view.customcontrols.SelectVendorDialog;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.telephoenic.orders360.controller.constants.AppConstants.LOG_OUT;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_MY_ORDERS_VIEW;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_POINTS_REDEEM;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_POS_LOCATION_VIEW;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_PRIVACY_POLICY_VIEW;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_PROFILE_EDIT;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_SETTING_EDIT;
import static com.telephoenic.orders360.controller.constants.AppConstants.ROLE_SURVEY_SUBMIT;
import static com.telephoenic.orders360.controller.fragments.MoreFragment.childClick.CHANGE_PASSWORD;
import static com.telephoenic.orders360.controller.fragments.MoreFragment.childClick.LANGUAGE;
import static com.telephoenic.orders360.controller.fragments.MoreFragment.childClick.LOCATION;
import static com.telephoenic.orders360.controller.fragments.MoreFragment.childClick.MY_PROFILE;

public class MoreFragment extends BlankFragment implements MoreRecyclerViewAdapter.OnItemClickListener, View.OnClickListener, ConfirmOrderDialog.onClickStatus, MoreChildAdapter.OnItemClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "MoreFragment";

    // views
    private RecyclerView recyclerView;
    private TextView nameTextView, emailTextView;
    private CustomAlertDialog confirmLogoutDialog;

    // vars
    private ArrayList<MoreListItem> mMoreListItems = new ArrayList<>();
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragment = null;

    private String mParam1;
    private String mParam2;
    private List<MoreItemChild> moreItemChildren;
    private MoreRecyclerViewAdapter adapter;
    private String type;
    private boolean hasVendor = false;

    private OnFragmentInteractionListener mListener;

    public MoreFragment() {
        // Required empty public constructor
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        AppUtils.changeTitle(getActivity(), getString(R.string.more_tab_text));
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        recyclerView = view.findViewById(R.id.more_recycler_view);
        nameTextView = view.findViewById(R.id.more_fragment_name_textView);
        emailTextView = view.findViewById(R.id.more_fragment_email_textView);
    }

    @Override
    public void onResume() {
        super.onResume();
        LanguageManager.setLanguage(getActivity());
        PosProfileWrapper posProfileWrapper = AppUtils.getProfile(getActivity());
        String name = posProfileWrapper.getUser().getName();
        String email = posProfileWrapper.getUser().getMobileNumber();
        List<String> permissions = posProfileWrapper.getUser().getPermissions();
        hasVendor = posProfileWrapper.getUser().isAssignVendor();

        if (permissions == null) {
            // we can here show an other activity with no permissions in case there is no permissions.
            return;
        }
        if (name != null) {
            nameTextView.setText(name);
        }

        if (email != null) {
            emailTextView.setText(email);
        }

        mMoreListItems.clear();

        for (String item : permissions) {

//            if (AppConstants.ROLE_ORDER.equals(item) ||
//                    AppConstants.ROLE_SUPPORT_TICKET_SEND.equals(item) ||
//                    AppConstants.ROLE_ORDER_DETAIL_FOR_SALES.equals(item) ||
//                    AppConstants.ROLE_POS_ADD.equals(item) ||
//                    AppConstants.ROLE_CART_EDIT.equals(item)) {
//                continue;
//                // skip this permissions.
//            }

            if (ROLE_PROFILE_EDIT.equals(item)) {
                moreItemChildren = new ArrayList<>();
                moreItemChildren.add(new MoreItemChild(getString(R.string.title_my_profile), R.drawable.ic_person_pin_black_24dp, childClick.MY_PROFILE));
                moreItemChildren.add(new MoreItemChild(getString(R.string.location_text), R.drawable.ic_location_pin, childClick.LOCATION));
//                moreItemChildren.add(new MoreItemChild(getString(R.string.change_password_text), R.drawable.ic_setting_blue));
                mMoreListItems.add(new MoreListItem(getString(R.string.profile_text), item, R.drawable.ic_personal, moreItemChildren, false));
            }

            if (ROLE_SETTING_EDIT.equals(item)) {
                moreItemChildren = new ArrayList<>();
                moreItemChildren.add(new MoreItemChild(getString(R.string.language_settings_text), R.drawable.ic_language_gray_24dp, childClick.LANGUAGE));
                moreItemChildren.add(new MoreItemChild(getString(R.string.change_password_text), R.drawable.change_password, childClick.CHANGE_PASSWORD));
                mMoreListItems.add(new MoreListItem(getString(R.string.navigation_drawer_settings), item, R.drawable.ic_setting_blue, moreItemChildren, false));
            }
            if (ROLE_PRIVACY_POLICY_VIEW.equals(item)) {
                mMoreListItems.add(new MoreListItem(getString(R.string.navigation_drawer_privacy_provisions), item, R.drawable.ic_pulicy_blue, null, false));
            }

            if (ROLE_POINTS_REDEEM.equals(item)) {
                mMoreListItems.add(new MoreListItem(getString(R.string.button_label_points), item, R.drawable.ic_points, null, false));
            }

           /* if (ROLE_POS_ORDERS_VIEW.equals(item)) {
                mMoreListItems.add(new MoreListItem(getString(R.string.orders_menu_item), item, R.drawable.ic_order_menu_item));
            }*/

//            if (ROLE_POS_LOCATION_VIEW.equals(item)) {
//                mMoreListItems.add(new MoreListItem(getString(R.string.pos_location_text_menu_item), item, R.drawable.ic_place_menu_item, null, false));
//            }

            /*if (ROLE_MY_ORDERS_VIEW.equals(item)) {
                mMoreListItems.add(new MoreListItem(getString(R.string.purchase_history_text_menu_item), item, R.drawable.ic_history_menu_item));
            }*/

            if (ROLE_SURVEY_SUBMIT.equals(item)) {
                mMoreListItems.add(new MoreListItem(getString(R.string.navigation_drawer_survey), item, R.drawable.ic_survey_blue, null, false));
            }

        }// end for
        mMoreListItems.add(new MoreListItem(getString(R.string.navigation_drawer_logout), LOG_OUT, R.drawable.ic_logout_blue, null, false));
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        adapter = new MoreRecyclerViewAdapter(mMoreListItems, getActivity(), this, this);
        //recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(MoreListItem listItem, int position) {
        type = listItem.getItemType();
        switch (listItem.getItemType()) {
            case ROLE_PROFILE_EDIT:
            case ROLE_SETTING_EDIT:

                if (listItem.isShowItem()) {
                    listItem.setShowItem(false);
                } else {
                    listItem.setShowItem(true);
                }
                adapter.notifyItemChanged(position);

                break;
            case ROLE_PRIVACY_POLICY_VIEW:
                fragment = new PrivacyPolicyFragment();
                replaceFragment(fragment, true);
                break;
            case ROLE_POINTS_REDEEM:
                if (AppPrefs.getVendorID(getContext()) != -1 && hasVendor) {
                    fragment = new PointsFragment();
                    replaceFragment(fragment, true);
                } else {
                    SelectVendorDialog selectVendorDialog = new SelectVendorDialog();
                    if (getActivity() != null)
                        selectVendorDialog.show(getActivity().getSupportFragmentManager(), "");
                }
                break;
            case ROLE_POS_LOCATION_VIEW:
                Toast.makeText(getActivity(), listItem.getName(), Toast.LENGTH_LONG).show();
                break;
            case ROLE_MY_ORDERS_VIEW:
                fragment = new PurchaseHistoryFragment();
                replaceFragment(fragment, true);
                break;
            case ROLE_SURVEY_SUBMIT:
                if (AppPrefs.getVendorID(getContext()) != -1 && hasVendor) {
                    fragment = new ChooseSurveyFragment();
                    replaceFragment(fragment, true);
                } else {
                    SelectVendorDialog selectVendorDialog = new SelectVendorDialog();
                    if (getActivity() != null)
                        selectVendorDialog.show(getActivity().getSupportFragmentManager(), "");
                }
                break;
            case LOG_OUT:
                confirmLogOut();
                break;
        }
    }

    private void replaceFragment(Fragment fragment, boolean addToBackStack) {
        if (getActivity() != null)
            gotoFragment(fragment, getActivity().getSupportFragmentManager());
//        fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
//        fragmentTransaction = fragmentManager.beginTransaction();
        // String title = AppUtils.getTitle(fragment);
        //AppUtils.changeTitle(Objects.requireNonNull(getActivity()), title);
//        fragmentTransaction.replace(R.id.container, fragment);
//        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//        if (addToBackStack) fragmentTransaction.addToBackStack("");
//        fragmentTransaction.commit();
    }

    @Override
    public void getStatusClick(int Status) {

        switch (Status) {
            case ConfirmOrderDialog.StatusClick.CANCEL:
                break;
            case ConfirmOrderDialog.StatusClick.OKAY:
                SaveCart.getInstance().ClearCart();
                AppPrefs.setToken(getContext(), null);
                IntentFactory.logOut(getActivity());
                break;
        }

    }

    @Override
    public void onItemClick(MoreItemChild listItem, int position) {

        switch (listItem.getChildClick()) {
            case childClick.MY_PROFILE:
                fragment = new ProfileFragment();
                replaceFragment(fragment, true);
                break;
            case childClick.LOCATION:
                startActivity(new Intent(getActivity(), PosUserLocationActivity.class));
//                  Intent tostart = new Intent(Intent.ACTION_VIEW);
//                    tostart.setDataAndType(Uri.parse("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"), "video/*");
//                    startActivity(tostart);
                break;
            case childClick.LANGUAGE:
                fragment = new SettingsFragment();
                replaceFragment(fragment, true);

                break;
            case childClick.CHANGE_PASSWORD:
                fragment = new ChangePasswordFragment();
                replaceFragment(fragment, true);
                break;
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void confirmLogOut() {
        Bundle bundle = new Bundle();
        String msg = getString(R.string.titles_dialog_logout_confirm_desc);
        ConfirmOrderDialog confirmOrderDialog = new ConfirmOrderDialog();
        confirmOrderDialog.onClickStatus(this);
        bundle.putString(ConfirmOrderDialog.MASSAGE, msg);
        confirmOrderDialog.setArguments(bundle);
        confirmOrderDialog.show(getFragmentManager(), "");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_positive:
                confirmLogoutDialog.dismiss();
                break;
            case R.id.btn_negative:
                SaveCart.getInstance().ClearCart();
                AppPrefs.setPOS(getActivity(), null);
                AppPrefs.setCountryId(getContext(), -1L);
                Intent intnt = new Intent(getActivity(), LoginActivity.class);
                intnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intnt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (getActivity() != null)
                    getActivity().startActivity(intnt);
                confirmLogoutDialog.dismiss();
                break;
        }
    }

    @IntDef({MY_PROFILE, LOCATION, LANGUAGE, CHANGE_PASSWORD})
    @Retention(RetentionPolicy.SOURCE)
    public @interface childClick {
        int MY_PROFILE = 0;
        int LOCATION = 1;
        int LANGUAGE = 2;
        int CHANGE_PASSWORD = 3;
    }
}