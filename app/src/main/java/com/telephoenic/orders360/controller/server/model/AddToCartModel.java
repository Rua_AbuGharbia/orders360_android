package com.telephoenic.orders360.controller.server.model;

import com.airbnb.lottie.L;

import java.util.List;

public class AddToCartModel {
    private Double totalAmount;
    private User posUser;
    private VendorWrapper vendor;
    private List<SingleItem> singleItems;
    private List<ComboItem> comboItems;
    private List<PromotionItem> promotionItems;
    private List<CartRewardItem> rewardItems;

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public User getPosUser() {
        return posUser;
    }

    public void setPosUser(User posUser) {
        this.posUser = posUser;
    }

    public List<SingleItem> getSingleItems() {
        return singleItems;
    }

    public void setSingleItems(List<SingleItem> singleItems) {
        this.singleItems = singleItems;
    }

    public List<ComboItem> getComboItems() {
        return comboItems;
    }

    public void setComboItems(List<ComboItem> comboItems) {
        this.comboItems = comboItems;
    }

    public List<PromotionItem> getPromotionItems() {
        return promotionItems;
    }

    public void setPromotionItems(List<PromotionItem> promotionItems) {
        this.promotionItems = promotionItems;
    }

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    public List<CartRewardItem> getRewardItems() {
        return rewardItems;
    }

    public void setRewardItems(List<CartRewardItem> rewardItems) {
        this.rewardItems = rewardItems;
    }
}
