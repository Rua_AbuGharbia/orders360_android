package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.constants.MainFragmentCategories;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.view.orders360view.Validation;
import com.telephoenic.orders360.viewmodel.CityViewModel;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;
import com.telephoenic.orders360.viewmodel.RegionViewModel;
import com.telephoenic.orders360.viewmodel.UserViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

public class ProfileFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        , ImageController {
    private static final String TAG = "ProfileFragment";
    @Validation(validation = {MainFragmentCategories.MORE, MainFragmentCategories.CAMPAIGN})
    @BindInput(R.id.pos_details_shopName_editText)
    Orders360EditText posDetailsShopNameEditText;
    @Validation(validation = {MainFragmentCategories.POINTS})
    @BindInput(R.id.pos_details_TRN)
    Orders360EditText posDetailsTRN;
    @BindInput(R.id.pos_details_phoneNumber_editText)
    Orders360EditText posDetailsPhoneNumberEditText;
    @BindInput(R.id.pos_details_ownerName_editText)
    Orders360EditText posDetailsOwnerNameEditText;
    @BindInput(R.id.pos_details_ownerPhoneNumber_editText)
    Orders360EditText posDetailsOwnerPhoneNumberEditText;
    @BindInput(R.id.pos_details_landNumber_editText)
    Orders360EditText posDetailsLandNumberEditText;
    @BindInput(R.id.pos_details_faxNumber_editText)
    Orders360EditText posDetailsFaxNumberEditText;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindInput(R.id.pos_details_selectcity_editText)
    Orders360EditText posDetailsSelectcityEditText;
    @BindInput(R.id.pos_details_selectregin_editText)
    Orders360EditText posDetailsSelectreginEditText;
    private PosProfileWrapper posProfile;
    private User mUser = new User();
    private List<CityModel> citiesList = new ArrayList<>();
    private Map<String, String> citiesMap = new HashMap<>();
    private List<RegionModel> regionsList = new ArrayList<>();
    private Map<String, String> regionsMap = new HashMap<>();
    private List<String> citiesArray = new ArrayList<>();
    private List<String> regionsArray = new ArrayList<>();
    private CityModel cityModel = new CityModel();
    private RegionModel regionModel = new RegionModel();
    private OrdersView ordersView;
    private ListItemDialog listItemDialog;
    private OnFragmentInteractionListener mListener;
    private PosUserViewModel posUserViewModel;
    private UserViewModel userViewModel;
    private CityViewModel cityViewModel;
    private RegionViewModel regionViewModel;
    private String myService = "";
    private String cityId;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.profile_text));
        posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        cityViewModel = ViewModelProviders.of(this).get(CityViewModel.class);
        regionViewModel = ViewModelProviders.of(this).get(RegionViewModel.class);
        getUserData();
    }

    private void getUserData() {
        posProfile = AppUtils.getProfile(getActivity());
        LoadingDialog.showDialog(getActivity());
        myService = AppConstants.SERVICE_GET_PROFILE;
        userViewModel.getUserProfile(posProfile.getUser().getId()).observe(this, new ApiObserver<User>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(User data) {
                LoadingDialog.dismiss();
                if (data != null) {
                    mUser = data;
                    posDetailsOwnerNameEditText.setText(mUser.getOwnerName());
                    posDetailsShopNameEditText.setText(mUser.getName());
                    posDetailsPhoneNumberEditText.setText(mUser.getMobileNumber());
                    posDetailsOwnerPhoneNumberEditText.setText(mUser.getOwnerMobileNumber());
                    posDetailsLandNumberEditText.setText(mUser.getLandNumber());
                    posDetailsFaxNumberEditText.setText(mUser.getFax());
                    posDetailsTRN.setText(mUser.getTrn());
                    fillCitySpinnerFromServer();
                }
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
            }
        });
    }

    @SuppressLint({"ClickableViewAccessibility", "NewApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        ordersView = new OrdersView();
        ordersView.bind(this, view);

        posDetailsSelectcityEditText.getContentEditText().setFocusable(false);
        posDetailsSelectreginEditText.getContentEditText().setFocusable(false);

        posDetailsSelectreginEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (regionsList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) regionsList);
                    bundle.putString(ListItemDialog.TITLE, getString(R.string.region_text));
                    listItemDialog = new ListItemDialog(regain);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }
            }
        });
        posDetailsSelectcityEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (citiesList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) citiesList);
                    bundle.putString(ListItemDialog.TITLE, getString(R.string.city_text));
                    listItemDialog = new ListItemDialog(city);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.profile_fragment_save_button)
    public void onViewClickedUpDate() {
        posDetailsLandNumberEditText.clearFocus();
        posDetailsShopNameEditText.clearFocus();
        posDetailsOwnerPhoneNumberEditText.clearFocus();
        posDetailsOwnerNameEditText.clearFocus();
        posDetailsPhoneNumberEditText.clearFocus();
        posDetailsTRN.clearFocus();
        posDetailsFaxNumberEditText.clearFocus();
        Long id = posProfile.getUser().getId();
        String shopName = TextUtils.isEmpty(posDetailsShopNameEditText.getEditTextContent()) ? "" : posDetailsShopNameEditText.getEditTextContent();
        String name = TextUtils.isEmpty(posDetailsOwnerNameEditText.getEditTextContent()) ? null : posDetailsOwnerNameEditText.getEditTextContent();
        String mobileNumber = TextUtils.isEmpty(posDetailsPhoneNumberEditText.getEditTextContent()) ? "" : posDetailsPhoneNumberEditText.getEditTextContent();
        String ownerMobileNumber = TextUtils.isEmpty(posDetailsOwnerPhoneNumberEditText.getEditTextContent()) ? null : posDetailsOwnerPhoneNumberEditText.getEditTextContent();
        String TRN = TextUtils.isEmpty(posDetailsTRN.getEditTextContent().trim()) ? "" : posDetailsTRN.getEditTextContent().trim();
        String land = TextUtils.isEmpty(posDetailsLandNumberEditText.getEditTextContent().trim()) ? null : posDetailsLandNumberEditText.getEditTextContent().trim();
        String fax = TextUtils.isEmpty(posDetailsFaxNumberEditText.getEditTextContent().trim()) ? null : posDetailsFaxNumberEditText.getEditTextContent().trim();
        mUser.setId(id);
        if (TextUtils.isEmpty(shopName)) {
            posDetailsShopNameEditText.validate();
            posDetailsShopNameEditText.setErrorMessage(getResources().getString(R.string.required_shop_name));
            posDetailsShopNameEditText.requestFocus();
        } else if (shopName.length() < 3) {
            posDetailsShopNameEditText.validate();
            posDetailsShopNameEditText.setErrorMessage(getResources().getString(R.string.min_lenght_3_digits));
            posDetailsShopNameEditText.requestFocus();
        } else if (TextUtils.isEmpty(mobileNumber)) {
            posDetailsPhoneNumberEditText.validate();
            posDetailsPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_requird));
            posDetailsPhoneNumberEditText.requestFocus();
        } else if (mobileNumber.length() != 10) {
            posDetailsPhoneNumberEditText.validate();
            posDetailsPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.mobile_number_validation_min_lenght_add_pos));
            posDetailsPhoneNumberEditText.requestFocus();
        } else if (!mobileNumber.startsWith("05")) {
            posDetailsPhoneNumberEditText.validate();
            posDetailsPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
            posDetailsPhoneNumberEditText.requestFocus();
        } else if (TextUtils.isEmpty(TRN)) {
            posDetailsTRN.validate();
            posDetailsTRN.setErrorMessage(getResources().getString(R.string.trn_validation_message));
            posDetailsTRN.requestFocus();
        } else if (TRN.length() != 15) {
            posDetailsTRN.validate();
            posDetailsTRN.setErrorMessage(getResources().getString(R.string.trn_validation_lenght_message));
            posDetailsTRN.requestFocus();
        } else if (ownerMobileNumber != null && ownerMobileNumber.length() != 10) {
            posDetailsOwnerPhoneNumberEditText.validate();
            posDetailsOwnerPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.owner_mobile_number_validation_min_lenght_add_pos));
            posDetailsOwnerPhoneNumberEditText.requestFocus();
        } else if (ownerMobileNumber != null && !ownerMobileNumber.startsWith("05")) {
            posDetailsOwnerPhoneNumberEditText.validate();
            posDetailsOwnerPhoneNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
            posDetailsOwnerPhoneNumberEditText.requestFocus();

        } else if (land != null && land.length() < 9) {
            posDetailsLandNumberEditText.validate();
            posDetailsLandNumberEditText.setErrorMessage(getResources().getString(R.string.please_enter_9_digits_land_number));
            posDetailsLandNumberEditText.requestFocus();

        } else if (land != null && land.length() == 9 && !land.startsWith("05")) {
            posDetailsLandNumberEditText.validate();
            posDetailsLandNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
            posDetailsLandNumberEditText.requestFocus();
        } else if (TextUtils.isEmpty(posDetailsSelectcityEditText.getEditTextContent())) {
            posDetailsSelectcityEditText.setErrorMessage(getString(R.string.please_select_city));
            posDetailsSelectcityEditText.validate();
        } else if (TextUtils.isEmpty(posDetailsSelectreginEditText.getEditTextContent())) {
            posDetailsSelectreginEditText.setErrorMessage(getString(R.string.please_select_region));
            posDetailsSelectreginEditText.validate();
        } else if (fax != null && fax.length() < 9) {
            posDetailsFaxNumberEditText.validate();
            posDetailsFaxNumberEditText.setErrorMessage(getResources().getString(R.string.please_enter_9_digits_fax_number));
            posDetailsFaxNumberEditText.requestFocus();

        } else if (fax != null && !fax.startsWith("05")) {
            posDetailsFaxNumberEditText.validate();
            posDetailsFaxNumberEditText.setErrorMessage(getResources().getString(R.string.must_starts_with_05));
            posDetailsFaxNumberEditText.requestFocus();
        } else {
            mUser.setName(shopName);
            mUser.setOwnerName(name);
            mUser.setMobileNumber(mobileNumber);
            mUser.setOwnerMobileNumber(ownerMobileNumber);
            mUser.setLandNumber(land);
            mUser.setFax(fax);
            mUser.setRegion(regionModel);
            mUser.setTrn(TRN);

            LoadingDialog.showDialog(getActivity());
            myService = AppConstants.SERVICE_UPDATE_POS;

            posUserViewModel.updateUserProfile(mUser).observe(this, new ApiCompleted() {
                @Override
                protected void noConnection() {
                    LoadingDialog.dismiss();
                    noConnectionInternetDialog();
                }

                @Override
                protected void onSuccess() {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), R.string.information_updated_successfully, Toast.LENGTH_SHORT).show();
                    PosProfileWrapper posProfileWrapper = AppUtils.getProfile(getContext());
                    posProfileWrapper.getUser().setMobileNumber(mUser.getMobileNumber());
                    posProfileWrapper.getUser().setName(mUser.getName());
                    AppPrefs.setPOS(getContext(), JsonUtils.toJsonString(posProfileWrapper));
                    Objects.requireNonNull(getContext()).startActivity(new Intent(getActivity(), MainActivity.class));
                }

                @Override
                protected void onError(String error) {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @OnClick(R.id.pos_details_cancel_button)
    public void onViewClicked() {
        if (getActivity() != null && getContext() != null)
            getContext().startActivity(new Intent(getActivity(), MainActivity.class));
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void fillCitySpinnerFromServer() {
        Long countryId = -1L;
        if (AppPrefs.getVendorID(getContext()) != -1) {
            countryId = AppPrefs.getCountryId(getActivity());
        }
        myService = AppConstants.SERVICE_GET_CITY;
        cityViewModel.getAllCities(countryId)
                .observe(this, new ApiObserver<List<CityModel>>() {
                    @Override
                    protected void noConnection() {
                        noConnectionInternetDialog();
                    }

                    @Override
                    protected void onSuccess(List<CityModel> data) {
                        assert data != null;
                        citiesList = data;
                        citiesArray.clear();
                        for (CityModel cityModel : citiesList) {
                            citiesMap.put(cityModel.getName(), String.valueOf(cityModel.getId()));
                            citiesArray.add(cityModel.getName());
                        }
                        if (mUser.getRegion().getName() != null) {
                            for (int i = 0; i < citiesList.size(); i++) {
                                if (mUser.getRegion().getCity().getId().equals(citiesList.get(i).getId())) {
                                    posDetailsSelectcityEditText.setText(citiesList.get(i).getName());
                                    cityId = String.valueOf(citiesList.get(i).getId());
                                    fillRegionSpinnerFromServer(cityId);
                                }
                            }
                        }

                    }

                    @Override
                    protected void onError(String error) {
                        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void fillRegionSpinnerFromServer(String cityId) {

        cityModel.setId(Long.valueOf(cityId));
        CityModel cityModel = new CityModel();
        cityModel.setId(Long.valueOf(cityId));
        regionModel.setCity(cityModel);
        regionModel.setId(null);
        myService = AppConstants.SERVICE_GET_REGION;

        regionViewModel.getAllRegionsAtSelectedCity(regionModel).observe(this, new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(ResponseBody data) {
                try {
                    assert data != null;
                    JSONObject jsonObj = new JSONObject(data.string());
                    JSONArray jsonArray = jsonObj.getJSONArray("content");
                    regionsList=new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        RegionModel orderModel = JsonUtils.toJavaObject(jsonArray.get(i).toString(), RegionModel.class);
                        regionsList.add(orderModel);
                    }
                    regionsArray.clear();
                    if (regionsList.size() > 0) {
                        for (RegionModel regionModel : regionsList) {
                            regionsMap.put(regionModel.getName(), String.valueOf(regionModel.getId()));
                            regionsArray.add(regionModel.getName());
                        }

                    }
                    if (mUser.getRegion().getId() != null) {
                        for (int i = 0; i < regionsList.size(); i++) {
                            if (mUser.getRegion().getId().equals(regionsList.get(i).getId())) {
                                posDetailsSelectreginEditText.setText(regionsList.get(i).getName());
                                regionModel.setId(regionsList.get(i).getId());
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private OnRecyclerClick city = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            posDetailsSelectcityEditText.setText(citiesList.get(position).getName());
            posDetailsSelectreginEditText.setText("");
            cityId = String.valueOf(citiesList.get(position).getId());
            fillRegionSpinnerFromServer(cityId);
        }
    };
    private OnRecyclerClick regain = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            if (listItemDialog != null)
                listItemDialog.dismiss();
            posDetailsSelectreginEditText.setText(regionsList.get(position).getName());
            regionModel.setId(regionsList.get(position).getId());
        }
    };

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {

        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if (AppConstants.SERVICE_GET_PROFILE.equals(myService)) {
                getUserData();
            } else if (AppConstants.SERVICE_GET_CITY.equals(myService)) {
                fillCitySpinnerFromServer();
            } else if (AppConstants.SERVICE_GET_REGION.equals(myService)) {
                fillRegionSpinnerFromServer(cityId);
            } else if (AppConstants.SERVICE_UPDATE_POS.equals(myService)) {
                onViewClickedUpDate();
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}
