package com.telephoenic.orders360.controller.server.model.shiporder;

public class SerialsQuantity  {
    private Integer quantity;

    public SerialsQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
