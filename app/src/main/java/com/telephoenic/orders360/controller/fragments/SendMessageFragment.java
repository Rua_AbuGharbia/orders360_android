package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SendTicketRequest;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.SupportTicketTypeModel;
import com.telephoenic.orders360.controller.server.model.SupportTicketWrapper;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.SupportTicketViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;

public class SendMessageFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {

    @BindInput(R.id.etxt_subject)
    Orders360EditText etxtSubject;
    @BindInput(R.id.etxt_message)
    Orders360EditText etxtMessage;
    @BindInput(R.id.TypeSupportTickets_EditText)
    Orders360EditText typeSupportTicketsEditText;
    private OrdersView ordersView;
    private String TAG = SendMessageFragment.class.getName();
    private PosProfileWrapper posProfileWrapper;

    private Button sendButton;
    private SendTicketRequest sendTicketRequest;
    private SupportTicketViewModel supportTicketViewModel;
    private List<SupportTicketTypeModel> supportTicketTypeModelList = new ArrayList<>();
    private ListItemDialog listItemDialog;
    private SupportTicketTypeModel supportTicketTypeModel = new SupportTicketTypeModel();
    private String myService;

    public SendMessageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_message, container, false);
        ordersView = new OrdersView();
        ButterKnife.bind(this, view);
        ordersView.bind(this, view);
        supportTicketViewModel = ViewModelProviders.of(this).get(SupportTicketViewModel.class);
        return view;
    }

    private void createComponents(View v) {
        sendButton = v.findViewById(R.id.button_send);
        getSupportType();
        typeSupportTicketsEditText.getContentEditText().setFocusable(false);

        typeSupportTicketsEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ListItemDialog.DATA, (Serializable) supportTicketTypeModelList);
                    bundle.putString(ListItemDialog.TITLE, getString(R.string.type_text));
                    listItemDialog = new ListItemDialog(onRecyclerClick);
                    listItemDialog.setArguments(bundle);
                    if (getFragmentManager() != null)
                        listItemDialog.show(getFragmentManager(), "");
                }
        });

        posProfileWrapper = AppUtils.getProfile(getContext());

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidInputs()) {
                    prepareTicketRequest();
                    sendTicketsRequest();
                }
            }
        });
    }

    private OnRecyclerClick onRecyclerClick = new OnRecyclerClick() {
        @Override
        public void getPosition(int position) {
            typeSupportTicketsEditText.setText(supportTicketTypeModelList.get(position).getName());
            supportTicketTypeModel = supportTicketTypeModelList.get(position);
            listItemDialog.dismiss();
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    private boolean isValidInputs() {

        if (TextUtils.isEmpty(typeSupportTicketsEditText.getEditTextContent())) {
            typeSupportTicketsEditText.setErrorMessage(getResources().getString(R.string.select_type_text));
            typeSupportTicketsEditText.validate();
            typeSupportTicketsEditText.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etxtSubject.getEditTextContent())) {
            etxtSubject.setErrorMessage(getResources().getString(R.string.toast_required_fill_subject));
            etxtSubject.validate();
            etxtSubject.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etxtMessage.getEditTextContent())) {
            etxtMessage.setErrorMessage(getResources().getString(R.string.toast_required_fill_message));
            etxtMessage.validate();
            etxtMessage.requestFocus();
            return false;
        }
        return true;
    }

    private void prepareTicketRequest() {
        sendTicketRequest = new SendTicketRequest();
        User user = new User();
        user.setId(posProfileWrapper.getUser().getId());
        sendTicketRequest.setPosUser(user);
        VendorWrapper vendorWrapper = new VendorWrapper();
        if (AppPrefs.getVendorID(getContext()) != -1) {
            Long vendorId = AppPrefs.getVendorID(getContext());
            vendorWrapper.setId(vendorId);
        }
        sendTicketRequest.setSupportTicketType(supportTicketTypeModel);
        sendTicketRequest.setVendor(vendorWrapper);
        sendTicketRequest.setPmsg(etxtMessage.getEditTextContent().toString().trim());
        sendTicketRequest.setSubject(etxtSubject.getEditTextContent().toString().trim());
    }

    private void sendTicketsRequest() {
        myService = AppConstants.SERVICE_SAVE_TICKETS;
        LoadingDialog.showDialog(getContext());
        supportTicketViewModel.saveTicket(sendTicketRequest).observe(this, new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(ResponseBody data) {
                LoadingDialog.dismiss();
                typeSupportTicketsEditText.setText("");
                etxtSubject.setText("");
                etxtMessage.setText("");
                AppUtils.showAlertToast(getContext().getResources().getString(R.string.dialog_send_sms_toast_message_sent), getContext());
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSupportType() {
        myService = AppConstants.SERVICE_SUPPORT_TYPE;
        LoadingDialog.showDialog(getContext());
        supportTicketViewModel.getType(0, 20, AppPrefs.getVendorID(getContext())).observe(this, new ApiObserver<SupportTicketWrapper>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(SupportTicketWrapper data) {
                supportTicketTypeModelList = data.getContent();
                LoadingDialog.dismiss();
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_SUPPORT_TYPE.equals(myService)){
                getSupportType();
            } else if(AppConstants.SERVICE_SAVE_TICKETS.equals(myService)){
                sendTicketsRequest();
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}
