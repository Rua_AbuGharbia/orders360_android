package com.telephoenic.orders360.controller.server.model.survey;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class SurveyDetailsRequest implements Serializable {

	@JsonProperty("surveyId")
	private Long surveyId;

	@JsonProperty("participantId")
	private Long participantId;

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	public Long getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}

}
