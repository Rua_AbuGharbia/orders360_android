package com.telephoenic.orders360.controller.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.model.AppPrefs;

import org.springframework.http.HttpStatus;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Rua on 14-Dec-17.
 */

public class PostExample {
    private static ResponseWrapper responseWrapper;
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    public Object post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("x-auth-token", AppPrefs.getToken(Order360Application.getInstance().getApplicationContext()))
                .build();
        try (Response response = client.newCall(request).execute()) {
            if(response.code()== org.springframework.http.HttpStatus.OK.value()){
                return response.body().string();
            } else if (response.code() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                responseWrapper = new ResponseWrapper();
                responseWrapper.setHttpCode(HttpStatus.INTERNAL_SERVER_ERROR.value()+"");
                responseWrapper.setResponse(response);
                responseWrapper.setMessage(response.message());
               // response = responseWrapper;
                return responseWrapper;
            } else if (response.code()== HttpStatus.UNAUTHORIZED.value()){
                responseWrapper = new ResponseWrapper();
                responseWrapper.setHttpCode(HttpStatus.UNAUTHORIZED.value()+"");
                responseWrapper.setResponse(response);
                return responseWrapper;
            }
            return null;
        }
    }
}
