package com.telephoenic.orders360.controller.server.model;

public class ShipmentOrderFilter {
    private Long orderDateFrom;
    private Long orderDateTo;
    private Long shipmentDateFrom;
    private Long shipmentDateTo;
    private Long regionId;
    private Long cityId;
    private Long warehouseId;

    public Long getOrderDateFrom() {
        return orderDateFrom;
    }

    public void setOrderDateFrom(Long orderDateFrom) {
        this.orderDateFrom = orderDateFrom;
    }

    public Long getOrderDateTo() {
        return orderDateTo;
    }

    public void setOrderDateTo(Long orderDateTo) {
        this.orderDateTo = orderDateTo;
    }

    public Long getShipmentDateFrom() {
        return shipmentDateFrom;
    }

    public void setShipmentDateFrom(Long shipmentDateFrom) {
        this.shipmentDateFrom = shipmentDateFrom;
    }

    public Long getShipmentDateTo() {
        return shipmentDateTo;
    }

    public void setShipmentDateTo(Long shipmentDateTo) {
        this.shipmentDateTo = shipmentDateTo;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }
}
