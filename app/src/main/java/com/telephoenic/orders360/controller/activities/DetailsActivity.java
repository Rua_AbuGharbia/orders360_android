package com.telephoenic.orders360.controller.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.constants.MainFragmentCategories;
import com.telephoenic.orders360.controller.fragments.CampaignFragment;
import com.telephoenic.orders360.controller.fragments.ChooseSurveyFragment;
import com.telephoenic.orders360.controller.fragments.ComboInfoFragment;
import com.telephoenic.orders360.controller.fragments.MainFragment;
import com.telephoenic.orders360.controller.fragments.OrderProductFragment;
import com.telephoenic.orders360.controller.fragments.PrivacyPolicyFragment;
import com.telephoenic.orders360.controller.fragments.ProfilePersonallyFragment;
import com.telephoenic.orders360.controller.fragments.PurchaseFragment;
import com.telephoenic.orders360.controller.fragments.PurchaseHistoryFragment;
import com.telephoenic.orders360.controller.fragments.SalesmanFragment;
import com.telephoenic.orders360.controller.fragments.SettingsFragment;
import com.telephoenic.orders360.controller.fragments.SupportTicketsFragment;
import com.telephoenic.orders360.controller.fragments.VendorFragment;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.view.customcontrols.CommissionDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DetailsActivity extends AppCompatActivity {

    public static final String PROMOTION = "promotion";
    public static final String BUNDLE = "bundle";

    private int index;
    private int numberOfTabes;

    private ImageView imageViewBasket, imageViewBack;
    private TextView textViewBasket;

    PosProfileWrapper posProfileWrapper;
    private List<String> privilegeList;
    private ArrayList<VendorWrapper> vendorsList;
    private Bundle bundle;
    private ConstraintLayout myCart;
    private TextView countCart;
    @SuppressLint("StaticFieldLeak")
    public static  ImageView cartImage;
    private ProductWrapper promotionWraper;
    private ComboWrapper comboWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.white));
        }

        setContentView(R.layout.activity_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppUtils.changeVendorLogo(this);
        myCart = toolbar.findViewById(R.id.MyCart_ConstraintLayout);
        cartImage = toolbar.findViewById(R.id.img_cpy);
        myCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailsActivity.this, MainActivity.class)
                        .putExtra(MainActivity.TAG, 3));
            }
        });


        AppUtils.updateNotifactionOrder(this);

        posProfileWrapper = AppUtils.getProfile(this);
        if (posProfileWrapper.getUser().getType().getCode().equals(AppConstants.USER_TYPE_SALES_MAN)) {
            myCart.setVisibility(View.INVISIBLE);
        }

        bundle = getIntent().getBundleExtra(IntentExtraNames.BUNDLE);
        if (bundle != null) {
            index = bundle.getInt(IntentExtraNames.INDEX, -1);
            numberOfTabes = bundle.getInt(IntentExtraNames.NUMBER_OF_TABS, -1);
            vendorsList = (ArrayList<VendorWrapper>) bundle.getSerializable(VendorFragment.VENDOR_LIST);
            promotionWraper = (ProductWrapper) this.bundle.getSerializable(PROMOTION);
            comboWrapper = (ComboWrapper) bundle.getSerializable(BUNDLE);
        }

        replaceFragment(index, false);
        ImageView imageViewBack = toolbar.findViewById(R.id.imageView_back);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void replaceFragment(int index, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        switch (Objects.requireNonNull(MainFragmentCategories.getByIndex(index))) {
            case MAIN_FRAGMENT:
                fragment = new MainFragment();
                break;
            case SALES_MAN:
                fragment = new SalesmanFragment(); // TODO Go To SALES MAN Fragment
                break;
            case SETTINGS:
                fragment = new SettingsFragment();
                break;
            case PURCHASE_HISTORY:
                fragment = new PurchaseHistoryFragment();
                break;
            case PURCHASE:
                fragment = new PurchaseFragment();
                break;
            case PRIVACY_POLICY:
                fragment = new PrivacyPolicyFragment();
                break;
            case SURVEY:
                fragment = new ChooseSurveyFragment();
                break;
            case CAMPAIGN:
                fragment = new CampaignFragment();
                break;
            case PROFILE:
                fragment = new ProfilePersonallyFragment();
                break;
            case SUPPORT_TICKETS:
                fragment = new SupportTicketsFragment();
                break;
            case ORDER_NOW:
                Bundle bundle = new Bundle();
                bundle.putInt(IntentExtraNames.NUMBER_OF_TABS, numberOfTabes);

                //  fragment = new OrderNowFragment();
                fragment = new OrderProductFragment();
                fragment.setArguments(bundle);
                break;
            case SPECIAL_OFFERS:
                Bundle bundle1 = new Bundle();
                bundle1.putInt(IntentExtraNames.NUMBER_OF_TABS, numberOfTabes);
                fragment = new OrderProductFragment();
                fragment.setArguments(bundle1);
                break;
            case VENDORS:
                myCart.setVisibility(View.INVISIBLE);
                Bundle vendorBundle = new Bundle();
                vendorBundle.putSerializable(VendorFragment.VENDOR_LIST, vendorsList);
                fragment = new VendorFragment();
                fragment.setArguments(vendorBundle);

                break;
            case PROMOTION:
                PosProfileWrapper posProfile = AppUtils.getProfile(this);
                fragment = new CommissionDialog(this, posProfile.getUser().getId().toString(), promotionWraper, AppConstants.NEW_ORDER);
                break;
            case BUNDLE:
                Bundle sendData =new Bundle();
                sendData.putSerializable(IntentExtraNames.OBJECT, comboWrapper);
                sendData.putString(IntentExtraNames.TYPE, AppConstants.NEW_ORDER);
                 fragment = new ComboInfoFragment();
                 fragment.setArguments(sendData);

                break;
        }
        String title = AppUtils.getTitle(fragment, this);
        AppUtils.changeTitle(this, title);
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        cartImage = toolbar.findViewById(R.id.img_cpy);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppUtils.changeTitle(this,
                AppUtils.getTitle(getSupportFragmentManager().findFragmentById(R.id.container)
                , this));
    }

    public void gotoFragment(Fragment fragment, FragmentManager fragmentManager, boolean addToBackStack, Bundle data) {
        fragment.setArguments(data);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.container, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}