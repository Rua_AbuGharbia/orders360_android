package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by ruaabugharbia on 13-Sep-18.
 */

public class ComboRequest implements Serializable {

    @JsonProperty("pageNum")
    String pageNum;

    @JsonProperty("pageSize")
    String pageSize;

    @JsonProperty("vendorId")
    String vendorId;

    @JsonProperty("name")
    String name;

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
