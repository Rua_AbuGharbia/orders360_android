package com.telephoenic.orders360.controller.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;

public class LoadingDialog {

    private static Dialog loading;

    @SuppressLint("ResourceType")
    public static void showDialog(Context context) {
        loading = new Dialog(context);
        loading.setContentView(R.layout.loading_dialog);
        loading.setCancelable(false);
        if (loading.getWindow() != null) {
            loading.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        showDialog();
    }

    public static void dismiss() {
        if (loading != null && loading.isShowing())
            loading.dismiss();
    }

    private static void showDialog() {
        if (loading.isShowing()){
            loading.dismiss();
        }
        if (!loading.isShowing() &&loading !=null) {
            loading.show();
        }
    }
}