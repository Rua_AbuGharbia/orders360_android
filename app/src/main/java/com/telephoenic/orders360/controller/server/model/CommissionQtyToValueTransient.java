package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 02-Nov-17.
 */

public class CommissionQtyToValueTransient implements Serializable {

    @JsonProperty("promotionType")
    private Integer promotionType;

    @JsonProperty("quantity")
    private Long quantity;

    @JsonProperty("value") // Free Items
    private Long value;

    private String count ;// TODO Added By Rua  // Ordered PromotionValueQuantity

    @JsonProperty("id")
    private Long id;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }
}
