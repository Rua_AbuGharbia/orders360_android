package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class PromotionItem extends OrderItems implements Serializable  {

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("deleted")
	private boolean deleted;

	@JsonProperty("inactive")
	private boolean inactive;

	@JsonProperty("product")
	private ProductWrapper product;

	@JsonProperty("promotion")
	private Promotion promotion;

	@JsonProperty("promotionItemDetailsList")
	List<PromotionItemDetails> promotionItemDetailsList;

	@JsonProperty("quantity")
	private Long quantity;

	@JsonProperty("freeItems")
	private Long freeItems;

	@JsonProperty("promotionDetailId")
	private Integer promotionDetailId;

	/*@JsonProperty("deliverdQuantity")
	private Integer deliverdQuantity;

	@JsonProperty("orderItemHistory")
	private String orderItemHistory;*/

	@JsonProperty("promotionDetail")
	private String promotionDetail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public ProductWrapper getProduct() {
		return product;
	}

	public void setProduct(ProductWrapper product) {
		this.product = product;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public List<PromotionItemDetails> getPromotionItemDetailsList() {
		return promotionItemDetailsList;
	}

	public void setPromotionItemDetailsList(List<PromotionItemDetails> promotionItemDetailsList) {
		this.promotionItemDetailsList = promotionItemDetailsList;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getFreeItems() {
		return freeItems;
	}

	public void setFreeItems(Long freeItems) {
		this.freeItems = freeItems;
	}

	public String getPromotionDetail() {
		return promotionDetail;
	}

	public void setPromotionDetail(String promotionDetail) {
		this.promotionDetail = promotionDetail;
	}

	public Integer getPromotionDetailId() {
		return promotionDetailId;
	}

	public void setPromotionDetailId(Integer promotionDetailId) {
		this.promotionDetailId = promotionDetailId;
	}
}