package com.telephoenic.orders360.controller.interfaces;

import android.view.View;
import android.widget.ImageView;

public interface AddToCartAnim {
     void onQuantityChange(ImageView cardv,int position);
}
