package com.telephoenic.orders360.controller.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.jsibbold.zoomage.ZoomageView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.model.AppPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity {
    public static final String IMAGE_NAME = "imageName";
    public static final String IMAGE_URL = "imageUrl";
    public static final String IMAGE_ID = "imageId";

    @BindView(R.id.myZoomageView)
    ZoomageView myZoomageView;
    String imagesName ;
    String urlName ;
    Integer imageID ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        if (getIntent() !=null){
            imagesName = getIntent().getStringExtra(IMAGE_NAME);
            urlName = getIntent().getStringExtra(IMAGE_URL);
            imageID = getIntent().getIntExtra(IMAGE_ID,-1);
        }

        try {
            if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                imagesName = "Empty";
            }
            Glide.with(this)
                    .load(AppConstants.URL_LIVE + urlName + imageID + "/" + imagesName)
                    // .dontTransform()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                            Glide.with(Main2Activity.this).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(getBaseContext()) + "/" + AppPrefs.getVendorLogo(getBaseContext()))
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .error(R.drawable.place_holder_order)
                                    .into(myZoomageView);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {

                            return false;
                        }
                    })
                    //  .crossFade(800)
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .into(myZoomageView);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
