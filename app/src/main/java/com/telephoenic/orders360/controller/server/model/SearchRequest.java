package com.telephoenic.orders360.controller.server.model;

/**
 * @author Ihab Alnaqib
 * @since 1.0
 * 
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SearchRequest implements Serializable {

	/**
	 *
	 */
	@JsonProperty("search_term")
	private String search_term;

	@JsonProperty("salesSupervisorId")
	private String salesSupervisorId;

	@JsonProperty("pageSize")
	private Integer pageSize;

	@JsonProperty("page")
	private Integer page;

	public String getSearch_term() {
		return search_term;
	}

	public void setSearch_term(String search_term) {
		this.search_term = search_term;
	}


	public String getSalesSupervisorId() {
		return salesSupervisorId;
	}

	public void setSalesSupervisorId(String salesSupervisorId) {
		this.salesSupervisorId = salesSupervisorId;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
}