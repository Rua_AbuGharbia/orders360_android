package com.telephoenic.orders360.controller.server.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityWrapper {

    @SerializedName("content")
    private List<CityModel> cityModelList;

    public CityWrapper() {
    }

    public CityWrapper(List<CityModel> cityModelList) {
        this.cityModelList = cityModelList;
    }

    public List<CityModel> getCityModelList() {
        return cityModelList;
    }

    public void setCityModelList(List<CityModel> cityModelList) {
        this.cityModelList = cityModelList;
    }

    @Override
    public String toString() {
        return "CityWrapper{" +
                "cityModelList=" + cityModelList +
                '}';
    }
}
