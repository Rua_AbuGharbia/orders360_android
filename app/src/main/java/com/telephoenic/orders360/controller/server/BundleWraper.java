package com.telephoenic.orders360.controller.server;

import com.telephoenic.orders360.controller.server.model.ComboWrapper;

import java.util.List;

public class BundleWraper {
    private List<ComboWrapper> content;

    public List<ComboWrapper> getContent() {
        return content;
    }

    public void setContent(List<ComboWrapper> content) {
        this.content = content;
    }
}
