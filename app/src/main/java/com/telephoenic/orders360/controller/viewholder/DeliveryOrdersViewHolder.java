package com.telephoenic.orders360.controller.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliveryOrdersViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.NamePosUser_Row_OrderDelivery_TextView)
    public TextView namePosUserRowOrderDeliveryTextView;
    @BindView(R.id.OrderId_Row_OrderDelivery_TextView)
    public TextView orderIdRowOrderDeliveryTextView;
    @BindView(R.id.TotalAmount_Row_OrderDelivery_TextView)
    public TextView totalAmountRowOrderDeliveryTextView;
    @BindView(R.id.OrderDate_Row_OrderDelivery_TextView)
    public TextView orderDateRowOrderDeliveryTextView;
    @BindView(R.id.ShipDate_Row_OrderDelivery_TextView)
    public TextView shipDateRowOrderDeliveryTextView;
    @BindView(R.id.City_Row_OrderDelivery_TextView)
    public TextView cityRowOrderDeliveryTextView;
    @BindView(R.id.Region_Row_OrderDelivery_TextView)
    public TextView regionRowOrderDeliveryTextView;
    @BindView(R.id.DeliveryStatus_Row_OrderDelivery_TextView)
    public TextView deliveryStatusRowOrderDeliveryTextView;
    @BindView(R.id.Arrow_ImageView)
    public ImageView arrowGo;
    @BindView(R.id.Warehouse_Row_OrderDelivery_TextView)
    public TextView warehouseRowOrderDeliveryTextView;
    @BindView(R.id.ShipNumber_Row_OrderDelivery_TextView)
    public TextView shipNumberRowOrderDeliveryTextView;

    public DeliveryOrdersViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
