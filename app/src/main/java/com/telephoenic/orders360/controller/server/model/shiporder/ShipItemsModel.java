package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;
import java.util.List;

public class ShipItemsModel implements Serializable {
    private List<SerialsModel> serials;
    private Long id;
    private String itemName;
    private Integer shipmentQuantity;
    private Integer serialsQuantity;
    private Boolean readyToShipment;
    private Double amount;

    public List<SerialsModel> getSerials() {

        return serials;
    }

    public void setSerials(List<SerialsModel> serials) {
        this.serials = serials;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getShipmentQuantity() {
        return shipmentQuantity;
    }

    public void setShipmentQuantity(Integer shipmentQuantity) {
        this.shipmentQuantity = shipmentQuantity;
    }

    public Integer getSerialsQuantity() {
        return serialsQuantity;
    }

    public void setSerialsQuantity(Integer serialsQuantity) {
        this.serialsQuantity = serialsQuantity;
    }

    public Boolean getReadyToShipment() {
        return readyToShipment;
    }

    public void setReadyToShipment(Boolean readyToShipment) {
        this.readyToShipment = readyToShipment;
    }

    public Double getTotalAmount() {
        return amount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.amount = totalAmount;
    }
}
