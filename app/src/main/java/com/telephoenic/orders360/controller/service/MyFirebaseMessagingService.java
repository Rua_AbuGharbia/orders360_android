package com.telephoenic.orders360.controller.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.NotificationActivity;
import com.telephoenic.orders360.model.NotificationResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private Gson gson = new Gson();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);

        NotificationResponse notificationResponse = null;
        try {
            notificationResponse = gson.fromJson(object.getString("data"), NotificationResponse.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String NOTIFICATION_CHANNEL_ID = "order_channel";

        long pattern[] = {0, 500, 500,500};

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Your Notifications",
                        NotificationManager.IMPORTANCE_HIGH);

                notificationChannel.setDescription("");
                notificationChannel.enableLights(true);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                notificationChannel.setLightColor(Color.BLUE);
              //notificationChannel.setVibrationPattern(pattern);
                notificationChannel.enableVibration(true);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }

            // to diaplay notification in DND Mode
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = mNotificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID);
                channel.canBypassDnd();
                channel.setShowBadge(true);
            }
            Drawable d = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                d = getDrawable(R.drawable.ic_point_onboerd);
            }
            if (d != null) {
                Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
            }

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

            notificationBuilder.setAutoCancel(true)
                    .setColor(ContextCompat.getColor(this, R.color.white))
                    .setContentTitle(notificationResponse.getTitle())
                    .setContentText(notificationResponse.getMessage())
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setAutoCancel(true);
//                    .setLargeIcon(bitmap)
//                   .setStyle(new NotificationCompat.BigPictureStyle()
//                    .bigPicture(bitmap));

            Intent notificationIntent = new Intent(getApplicationContext(), NotificationActivity.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            int notificationId = (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
            notificationIntent.putExtra("notification", notificationResponse);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    notificationId, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            notificationBuilder.setContentIntent(pendingIntent);
            mNotificationManager.notify(notificationId, notificationBuilder.build());
        }
    }
}
