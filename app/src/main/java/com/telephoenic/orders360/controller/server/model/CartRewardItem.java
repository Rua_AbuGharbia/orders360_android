package com.telephoenic.orders360.controller.server.model;

import com.google.android.gms.analytics.ecommerce.Product;

import java.io.Serializable;

public class CartRewardItem extends OrderItems implements Serializable {
    private Long point;
    private Product product;

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
