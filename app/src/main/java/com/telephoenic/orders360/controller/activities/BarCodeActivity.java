package com.telephoenic.orders360.controller.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class BarCodeActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {
    public static String BAR_CODE = "bar code";
    @BindView(R.id.BarCode)
    ZBarScannerView barCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        barCode = new ZBarScannerView(this);
        setContentView(R.layout.activity_bar_code);
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void onResume() {
        super.onResume();

        barCode.setResultHandler(this); // Register ourselves as a handler for scan results.
        barCode.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        barCode.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here


        Intent intent = getIntent();
        intent.putExtra(BAR_CODE, rawResult.getContents());
        setResult(RESULT_OK, intent);
        finish();
//        Log.v("R", rawResult.getContents());
//        Toast.makeText(this, rawResult.getContents() + "", Toast.LENGTH_SHORT).show();
//        // Prints scan results
//        Log.v("R2", rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
    }

    @OnClick(R.id.TryAgain_TextView)
    public void onViewClicked() {
        barCode.resumeCameraPreview(this);
    }
}
