    package com.telephoenic.orders360.controller.gcm;

    import android.annotation.SuppressLint;
    import android.content.Context;
    import android.os.AsyncTask;
    import android.util.Log;

    import com.google.android.gms.common.ConnectionResult;
    import com.google.android.gms.common.GoogleApiAvailability;
    import com.google.android.gms.common.GooglePlayServicesUtil;
    import com.google.android.gms.gcm.GoogleCloudMessaging;
    import com.google.android.gms.iid.InstanceID;
    import com.telephoenic.orders360.controller.utils.AppUtils;
    import com.telephoenic.orders360.model.AppPrefs;


    import java.io.IOException;

    public class GCMRegistrar {
        private static final String TAG = GCMRegistrar.class.getSimpleName();
        //This is the project number you got from the API Console. YOUR_SENDER_ID
        private static final String GCM_SENDER_ID = "119962418019";  //TODO Order360 project number
        private Context context;

        public GCMRegistrar(Context context) {
            this.context = context;
        }

        public void doGcmWork() {
            if (checkPlayServices()) {
                registerInBackground(context);
               // registerInBackground(context);
              /* String regId = getRegistrationId(context);
                if (regId == null) {
                    registerInBackground(context);
                }*/
            } else {
                Log.i(TAG, "No valid Google Play Services APK found.");
            }
        }

        private String getRegistrationId(Context context) {
            String registrationId = AppPrefs.getRegistrationId(context);
            if (registrationId == null) {
                Log.i(TAG, "Registration not found.");
                return null;
            }
            int registeredVersion = AppPrefs.getAppVersion(context);
            int currentVersion = AppUtils.getVersionCode(context);
            if (registeredVersion != currentVersion) {
                Log.i(TAG, "App version changed.");
                return null;
            }
            return registrationId;
        }

        @SuppressLint("StaticFieldLeak")
        private void registerInBackground(final Context context) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    try {

                        String regId = InstanceID.getInstance(context)
                                .getToken(GCM_SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
//                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
//                        String regId = gcm.register(GCM_SENDER_ID);
                        if (regId != null && !regId.isEmpty()) {
                            AppPrefs.setRegistrationId(context, regId);
                            AppPrefs.setAppVersion(context, AppUtils.getVersionCode(context));
                            //AppPrefs.setUserRegistered(context, false);
                            return regId;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String regId) {
                    super.onPostExecute(regId);
                 /*   //TODO : check this
                    *//*if (regId != null && !regId.isEmpty()) {
                        return;
                    }*//*

                    PosProfileWrapper posProfile = null ;

                    try {
                        posProfile = JsonUtils.toJavaObject(AppPrefs.getPOS(context), PosProfileWrapper.class);
                    }catch (Exception Handling){

                    }

                    UpdateGCMWrapperRequest updateGCMWrapperRequest = new UpdateGCMWrapperRequest();
                    if(posProfile != null)
                        updateGCMWrapperRequest.setPosId(posProfile.getId());

                    updateGCMWrapperRequest.setRegistrationId(regId);

                    new GetWebserviceTask(context, UpdateRegIdWebService.class) {
                        @Override
                        public void onResponseReady(Object response) {
                            super.onResponseReady(response);
                            if (response != null) {
                                String res = response.toString();
                                if (res.equals(C.RESPONSE_SUCCESSFUL)) {
                                    AppPrefs.setUserRegistered(context, true);
                                }
                            }
                        }
                    }.execute(JsonUtils.toJsonString(updateGCMWrapperRequest));*/
                }
            }.execute(null, null, null);
        }

        private boolean checkPlayServices() {
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);

        //    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
            int resultCode =  GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
            if (resultCode != ConnectionResult.SUCCESS) {
                if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                    // GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                    // TODO: show this dialog in schools fragment
                } else {
                    Log.i(TAG, "This device is not supported.");
                }
                return false;
            }

            return true;
        }

        // SHARED PREFERENCES //

    }
