package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class RedeemModel implements Serializable {

	@JsonProperty("vendorId")
	private Long vendorId;

	@JsonProperty("vendorName")
	private String vendorName;

	@JsonProperty("totalPoints")
	private Long totalPoints;

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Long getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(Long totalPoints) {
		this.totalPoints = totalPoints;
	}
}