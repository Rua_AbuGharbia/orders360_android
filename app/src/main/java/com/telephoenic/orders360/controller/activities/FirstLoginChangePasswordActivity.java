package com.telephoenic.orders360.controller.activities;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.exception.ExceptionHandling;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.controller.server.model.UserPassword;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.NoInternetConnectionDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;
import com.telephoenic.orders360.viewmodel.UserViewModel;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

public class FirstLoginChangePasswordActivity extends AppCompatActivity implements DialogUtils.OnDialogClickListener
        ,ImageController {
    private static final String TAG = "FirstLoginChangePasswor";

    @BindInput(R.id.OldPassword_ChangePassword_EditText)
    Orders360EditText oldPasswordChangePasswordEditText;
    @BindInput(R.id.NewPassword_ChangePassword_EditText)
    Orders360EditText newPasswordChangePasswordEditText;
    @BindInput(R.id.ConfrimPassword_ChangePassword_EditText)
    Orders360EditText confrimPasswordChangePasswordEditText;
    private Context context;
    private UpdateGPSRequest updateGPSRequest;
    private PosProfileWrapper posProfile;
    private PosUserViewModel posUserViewModel;
    private UserViewModel userViewModel;
    private String myService ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_login_change_password);
        ButterKnife.bind(this);
        OrdersView ordersView = new OrdersView();
        ordersView.bind(this);
        posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        context = this;
    }

    private void upDatePassword() {
        String currentPassword = oldPasswordChangePasswordEditText.getEditTextContent().trim();
        String newPassword = newPasswordChangePasswordEditText.getEditTextContent().trim();
        String confirmPassword = confrimPasswordChangePasswordEditText.getEditTextContent().trim();

        if (TextUtils.isEmpty(currentPassword)) {
            oldPasswordChangePasswordEditText.setErrorMessage(getString(R.string.please_enter_current_password));
            oldPasswordChangePasswordEditText.validate();
            oldPasswordChangePasswordEditText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(newPassword)) {
            newPasswordChangePasswordEditText.setErrorMessage(getString(R.string.please_enter_new_password));
            newPasswordChangePasswordEditText.validate();
            newPasswordChangePasswordEditText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            confrimPasswordChangePasswordEditText.setErrorMessage(getString(R.string.please_enter_confirm_new_password));
            confrimPasswordChangePasswordEditText.validate();
            confrimPasswordChangePasswordEditText.requestFocus();
            return;
        }

        if (!Objects.equals(newPassword, confirmPassword)) {
            confrimPasswordChangePasswordEditText.setErrorMessage(getString(R.string.passwords_did_not_match));
            confrimPasswordChangePasswordEditText.validate();
            confrimPasswordChangePasswordEditText.requestFocus();
            return;
        }

        posProfile = AppUtils.getProfile(context);
        UserPassword userPassword = new UserPassword();
        userPassword.setForceFirstLogin(true);
        userPassword.setOldPassword(currentPassword);
        userPassword.setNewPassword(newPassword);
        userPassword.setPosId(posProfile.getUser().getId());

        LoadingDialog.showDialog(context);
        myService = AppConstants.SERVICE_UPDATE_PASSWORD;
        userViewModel.changePasswordInsideApp(userPassword).observe(this, new ApiObserver<Boolean>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(Boolean data) {
                LoadingDialog.dismiss();
                if (data) {
                    if (AppConstants.USER_TYPE_POS.equals(posProfile.getUser().getType().getCode())) {
                        try {
                            prepareUpdateGPSRequest();
                            sendUpdateGPSRequest();
                        } catch (ExceptionHandling exceptionHandling) {
                            exceptionHandling.printStackTrace();
                        }
                    } else {
                        navigateToHome();
                    }
                } else {
                    Toast.makeText(context, R.string.current_password_did_not_match_existing_password, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(FirstLoginChangePasswordActivity.this,error,Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R.id.Next_ChangePassword_Button)
    public void onViewClicked() {
        upDatePassword();
    }

    private void prepareUpdateGPSRequest() {
        updateGPSRequest = new UpdateGPSRequest();
        updateGPSRequest.setId(posProfile.getUser().getId());
        updateGPSRequest.setRegistrationId(AppPrefs.getRegistrationId(this));
        myService = AppConstants.SERVICE_UPDATE_RID ;
    }

    @SuppressLint("StaticFieldLeak")
    private void sendUpdateGPSRequest() throws ExceptionHandling {
        try {
            posUserViewModel.updateRegistrationID(updateGPSRequest).observe(this, new ApiObserver<ResponseBody>() {
                @Override
                protected void noConnection() {
                    noConnectionInternetDialog();
                }

                @Override
                protected void onSuccess(ResponseBody data) {
                    Log.d(TAG, "onSuccess: "+data);
                    navigateToHome();
                }

                @Override
                protected void onError(String error) {
                    Log.e(TAG, "onError: "+error );
                }
            });
        } catch (Exception e) {
            throw new ExceptionHandling(e.toString());
        }
    }

    private void navigateToHome() {
        Bundle bundle = new Bundle();
        bundle.putLong(IntentExtraNames.ID, posProfile.getUser().getId());
        AppConstants.MY_ACTIVITY = "Login";
        IntentFactory.startActivity(FirstLoginChangePasswordActivity.this, MainActivity.class, true, true, bundle);
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {

    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_UPDATE_PASSWORD.equals(myService)){
                upDatePassword();
            }else if(AppConstants.SERVICE_UPDATE_RID.equals(myService)){
                try {
                    sendUpdateGPSRequest();
                } catch (ExceptionHandling exceptionHandling) {
                    exceptionHandling.printStackTrace();
                }
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(FirstLoginChangePasswordActivity.this, settingsDialogModel, this);
    }
}
