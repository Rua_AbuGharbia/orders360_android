package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;

public class CashPayments implements Serializable {
    private PaymentMethodModel paymentMethod;
    private Double amount;

    public CashPayments(PaymentMethodModel paymentMethod, Double amount) {
        this.paymentMethod = paymentMethod;
        this.amount = amount;
    }

    public CashPayments() {
    }

    public PaymentMethodModel getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethodModel paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
