package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

import retrofit2.http.Header;

/**
 * Created by Rua on 02-Nov-17.
 */

public class PosProfileWrapper implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("user")
    private User user;

    @JsonProperty("content")
    private List<User> content;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getContent() {
        return content;
    }

    public void setContent(List<User> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "PosProfileWrapper{" +
                "user=" + user +
                ", content=" + content +
                '}';
    }
}
