package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeneralConfig {

	@JsonProperty("RestAPIConnectTimeout")
	private int RestAPIConnectTimeout = 120000;

	@JsonProperty("RestAPIReadTimeout")
	private int RestAPIReadTimeout = 120000;

	@JsonProperty("MobileRestAPIConnectTimeout")
	private int MobileRestAPIConnectTimeout = 120000;

	@JsonProperty("MobileRestAPIReadTimeout")
	private int MobileRestAPIReadTimeout = 120000;

	/**
	 * @return the restAPIConnectTimeout
	 */
	public int getRestAPIConnectTimeout() {
		return RestAPIConnectTimeout;
	}

	/**
	 * @param restAPIConnectTimeout
	 *            the restAPIConnectTimeout to set
	 */
	public void setRestAPIConnectTimeout(int restAPIConnectTimeout) {
		RestAPIConnectTimeout = restAPIConnectTimeout;
	}

	/**
	 * @return the restAPIReadTimeout
	 */
	public int getRestAPIReadTimeout() {
		return RestAPIReadTimeout;
	}

	/**
	 * @param restAPIReadTimeout
	 *            the restAPIReadTimeout to set
	 */
	public void setRestAPIReadTimeout(int restAPIReadTimeout) {
		RestAPIReadTimeout = restAPIReadTimeout;
	}

	/**
	 * @return the mobileRestAPIConnectTimeout
	 */
	public int getMobileRestAPIConnectTimeout() {
		return MobileRestAPIConnectTimeout;
	}

	/**
	 * @param mobileRestAPIConnectTimeout
	 *            the mobileRestAPIConnectTimeout to set
	 */
	public void setMobileRestAPIConnectTimeout(int mobileRestAPIConnectTimeout) {
		MobileRestAPIConnectTimeout = mobileRestAPIConnectTimeout;
	}

	/**
	 * @return the mobileRestAPIReadTimeout
	 */
	public int getMobileRestAPIReadTimeout() {
		return MobileRestAPIReadTimeout;
	}

	/**
	 * @param mobileRestAPIReadTimeout
	 *            the mobileRestAPIReadTimeout to set
	 */
	public void setMobileRestAPIReadTimeout(int mobileRestAPIReadTimeout) {
		MobileRestAPIReadTimeout = mobileRestAPIReadTimeout;
	}

}
