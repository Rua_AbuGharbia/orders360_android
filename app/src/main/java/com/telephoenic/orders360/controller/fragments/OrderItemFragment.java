package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.CancelOrderModel;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.SingleItem;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.MyOrderDetailsAdapter;
import com.telephoenic.orders360.view.customcontrols.CancelOrderDialog;
import com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ListItemDialog;
import com.telephoenic.orders360.view.customcontrols.OrderDetailsDialog;
import com.telephoenic.orders360.viewmodel.OrderHistoryViewModel;
import com.telephoenic.orders360.viewmodel.OrderItemViewModel;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class OrderItemFragment extends BlankFragment implements OnRecyclerClick, DialogUtils.OnDialogClickListener
        , ImageController, CancelOrderDialog.getReason {

    @BindView(R.id.Cancel_Buttom)
    Button cancelButtom;
    Unbinder unbinder;
    private String TAG = OrderItemFragment.class.getName();

    private RecyclerView recyclerView;
    List<OrderItemModel> orderItemModelList;
    private MyOrderDetailsAdapter adapter;
    private int promotionType = 0;
    private int singleType = 1;
    String type;

    int orderId;
    OrderModel orderModel;
    TextView orderDate, orderNumber, totalAmount, orderStatus, vendorName;
    ImageView vendorImage, arrowImage;

    ConstraintLayout constraintLayout;

    int indexClick = 0;

    private OrderItemViewModel orderItemViewModel;
    private double oldPrice;
    private OrderHistoryViewModel orderHistoryViewModel;
    private List<OrderRejectedReason> orderRejectedReasonList = new ArrayList<>();
    private ListItemDialog listItemDialog;
    private Long reasonId;
    private CancelOrderDialog cancelOrderDialog;
    private CancelOrderModel cancelOrderModel;

    public OrderItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppUtils.changeTitle(getActivity(), getString(R.string.navigation_drawer_orders));
        View view = inflater.inflate(R.layout.fragment_order_item, container, false);
        ButterKnife.bind(this, view);
        orderHistoryViewModel = ViewModelProviders.of(this).get(OrderHistoryViewModel.class);
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {
        orderItemViewModel = ViewModelProviders.of(this).get(OrderItemViewModel.class);
        orderDate = v.findViewById(R.id.text_order_delivery_date);
        orderNumber = v.findViewById(R.id.text_order_number);
        totalAmount = v.findViewById(R.id.text_order_total_amount);
        orderStatus = v.findViewById(R.id.text_order_status);
        vendorName = v.findViewById(R.id.text_vendor_name);
        vendorImage = v.findViewById(R.id.image_vendor);
        arrowImage = v.findViewById(R.id.image_arrow);
        constraintLayout = v.findViewById(R.id.layout_purchase);
        recyclerView = v.findViewById(R.id.RecyclerView);


        //orderItemsListView.setIndicatorBounds(orderItemsListView.getRight()- 50, orderItemsListView.getWidth());

        Bundle bundle = getArguments();
        if (bundle != null) {
            orderId = bundle.getInt(IntentExtraNames.ID);
            orderModel = (OrderModel) bundle.getSerializable(IntentExtraNames.OBJECT);

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            String dateString = formatter.format(new Date(Long.valueOf(orderModel.getAudit().getCreatedDate())));

            orderDate.setText(String.format("%s : %s", getString(R.string.order_date), dateString));
            if (orderModel.getVendor() != null) {
                vendorName.setText(orderModel.getVendor().getName());
            } else {
                vendorName.setVisibility(View.INVISIBLE);
            }
            orderNumber.setText(orderModel.getOrderNumber());

            totalAmount.setText(" " + String.format(Locale.ENGLISH, "%.2f", orderModel.getTotalAmount()) + " " + AppConstants.CURRENCY);
            //  totalAmount.setText(String.format("%s %s", (float) Math.round(orderModel.getTotalAmount() * 100) / 100, AppConstants.CURRENCY));

            orderStatus.setText(orderModel.getDeliveryStatus().getName());
            String statusCode = orderModel.getDeliveryStatus().getCode();

            switch (statusCode) {
                case AppConstants.NEW_CODE:
                    getOrderReasons();
                    cancelButtom.setText(getString(R.string.cancel_order));
                    orderStatus.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case AppConstants.ORDER_ACKNOWLEDGED_CODE:
                    getOrderReasons();
                    cancelButtom.setText(getString(R.string.cancel_order));
                    orderStatus.setTextColor(getResources().getColor(R.color.acknowledged_color));
                    break;
                case AppConstants.ORDER_OPEN_CODE:
                    orderStatus.setTextColor(getResources().getColor(R.color.partial_complete_color));
                    break;
                case AppConstants.ORDER_REJECTED_CODE:
                    cancelButtom.setVisibility(View.GONE);
                    orderStatus.setTextColor(getResources().getColor(R.color.partial_in_complete_color));
                    break;
                case AppConstants.ORDER_CLOSED_CODE:
                    orderStatus.setTextColor(getResources().getColor(R.color.full_delivered_color));
                    break;
                case AppConstants.ORDER_CANCEL_CODE:
                    cancelButtom.setVisibility(View.GONE);
                    orderStatus.setTextColor(getResources().getColor(R.color.default_blue_light));
                    break;
            }

//            try {
//                Glide.with(getContext()).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(getContext()) + "/" + AppPrefs.getVendorLogo(getContext()))
//                        .error(R.drawable.place_holder_order)
//                        .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
//                        .into(vendorImage);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

        }

        orderItemModelList = new ArrayList<>();

        getMyOrder(String.valueOf(orderId));

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createComponents(view);
    }


    @Override
    public void getPosition(int position) {

        Bundle bundle = new Bundle();
        if (!orderItemModelList.get(position).isHasDiscount()) {
            bundle.putInt(OrderDetailsDialog.TYPE, promotionType);
            bundle.putDouble(OrderDetailsDialog.PRICE_AFTER_DISCONT, orderItemModelList.get(position).getTotalAmount() / orderItemModelList.get(position).getOrderedQuantity());


        } else {
            bundle.putInt(OrderDetailsDialog.TYPE, singleType);

            if (orderItemModelList.get(position).getDiscountedPrice() != null)
                bundle.putDouble(OrderDetailsDialog.PRICE_AFTER_DISCONT, orderItemModelList.get(position).getDiscountedPrice());

            bundle.putDouble(OrderDetailsDialog.DISCOUNT_PERCENTAGE, orderItemModelList.get(position).getDiscountPercentage());


        }
        bundle.putInt(OrderDetailsDialog.ID, orderItemModelList.get(position).getId());
        bundle.putDouble(OrderDetailsDialog.PRICE, orderItemModelList.get(position).getPrice());
        //bundle.putDouble(OrderDetailsDialog.PRICE_AFTER_DISCONT, orderItemModelList.get(position).getTotalAmount() / orderItemModelList.get(position).getOrderedQuantity());
        bundle.putString(OrderDetailsDialog.ORDER_QTY, String.valueOf(orderItemModelList.get(position).getOrderedQuantity()));
        OrderDetailsDialog orderDetailsDialog = new OrderDetailsDialog();
        orderDetailsDialog.setArguments(bundle);
        orderDetailsDialog.show(getFragmentManager(), "");
        //gotoFragment(new OrderItemFragment(), getActivity().getSupportFragmentManager(),bundle);
    }


    private void getMyOrder(String id) {
        orderItemViewModel.getMyOrderHistory(id, getActivity(), this).observe(this, new ApiObserver<OrderItemModel>() {
            @Override
            protected void noConnection() {
                isInternetConnection = false;
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(OrderItemModel data) {
                intiAdapter(data);
                LoadingDialog.dismiss();
            }

            @Override
            protected void onError(String error) {
                if (error != null)
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && !isInternetConnection) {
            this.onViewCreated(getView(), new Bundle());
        }
    }

    private void intiAdapter(OrderItemModel data) {
        if (data.getComboItems() != null) {
            for (int i = 0; i < data.getComboItems().size(); i++) {
                orderItemModelList.add(new OrderItemModel(data.getComboItems().get(i).getId(), Integer.parseInt(data.getComboItems().get(i).getOrderedQuantity()), data.getComboItems().get(i).getTotalAmount(), data.getComboItems().get(i).getCombo().getName(), data.getComboItems().get(i).getPrice(), false));
            }
        }
        if (data.getPromotionItems() != null) {
            for (int i = 0; i < data.getPromotionItems().size(); i++) {
                orderItemModelList.add(new OrderItemModel(data.getPromotionItems().get(i).getId(), Integer.parseInt(data.getPromotionItems().get(i).getOrderedQuantity()), data.getPromotionItems().get(i).getTotalAmount(), data.getPromotionItems().get(i).getProduct().getName(), data.getPromotionItems().get(i).getPrice(), false));
            }
        }
        if (data.getSingleItems() != null) {
            for (int i = 0; i < data.getSingleItems().size(); i++) {

                SingleItem singleItem = data.getSingleItems().get(i);
                double price = singleItem.getTotalAmount() / Integer.parseInt(singleItem.getOrderedQuantity());

                OrderItemModel singleModel = new OrderItemModel(singleItem.getId(),
                        Integer.parseInt(singleItem.getOrderedQuantity()),
                        singleItem.getTotalAmount(),
                        singleItem.getProduct().getName(),
                        singleItem.getPrice(),
                        true);
                singleModel.setDiscountedPrice(singleItem.getDiscountedPrice());
                singleModel.setDiscountPercentage(singleItem.getDiscountPercentage());

                orderItemModelList.add(singleModel);
            }
        }
        adapter = new MyOrderDetailsAdapter(orderItemModelList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            getMyOrder(String.valueOf(orderId));
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }


    @OnClick(R.id.Cancel_Buttom)
    public void onViewClicked() {
        if (cancelButtom.getText().equals(getString(R.string.cancel_order))) {
            if (orderRejectedReasonList.size() > 0) {
                cancelOrderDialog = new CancelOrderDialog(orderRejectedReasonList);
                cancelOrderDialog.reason(this);
                if (getFragmentManager() != null)
                    cancelOrderDialog.show(getFragmentManager(), "");
            }
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong(PosUserOrdersShipFragment.ORDER_ID, orderId);
            gotoFragment(new PosUserOrdersShipFragment(), getActivity().getSupportFragmentManager(), bundle);
        }


    }

    private void getOrderReasons() {
        orderHistoryViewModel.getOrderReason(AppPrefs.getVendorID(getContext())).observe(this, new ApiObserver<List<OrderRejectedReason>>() {
            @Override
            protected void noConnection() {
                isInternetConnection = false;
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<OrderRejectedReason> data) {
                orderRejectedReasonList = data;
                orderRejectedReasonList.add(new OrderRejectedReason("Other", -1L));

            }

            @Override
            protected void onError(String error) {
                Toast.makeText(getActivity(), error + "", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void confirmCancel() {
        Bundle bundle = new Bundle();
        String msg = getString(R.string.cancel_order_message);
        ConfirmOrderDialog confirmOrderDialog = new ConfirmOrderDialog();
        bundle.putString(ConfirmOrderDialog.MASSAGE, msg);
        confirmOrderDialog.setArguments(bundle);
        confirmOrderDialog.onClickStatus(clickStatus);
        confirmOrderDialog.show(getFragmentManager(), "");
    }

    private ConfirmOrderDialog.onClickStatus clickStatus = new ConfirmOrderDialog.onClickStatus() {
        @Override
        public void getStatusClick(int Status) {
            switch (Status) {
                case ConfirmOrderDialog.StatusClick.CANCEL:
                    break;
                case ConfirmOrderDialog.StatusClick.OKAY:
                    cancelOrder();
                    break;
            }
        }
    };

    private void cancelOrder() {
        LoadingDialog.showDialog(getContext());

        orderHistoryViewModel.cancelOrder(cancelOrderModel).observe(this, new ApiCompleted() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                isInternetConnection = false;
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess() {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), "Order Cancel Successfully", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();

            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void getReason(OrderRejectedReason rejectedReason, String otherReasonText) {
        cancelOrderModel = new CancelOrderModel();
        if (rejectedReason.getId() != -1L) {
            cancelOrderModel.setOrderId(rejectedReason.getId());
        } else {
            cancelOrderModel.setReasonText(otherReasonText);
        }
        cancelOrderModel.setOrderId((long) orderId);

        confirmCancel();

    }
}
