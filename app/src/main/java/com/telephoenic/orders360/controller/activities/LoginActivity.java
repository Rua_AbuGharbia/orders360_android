package com.telephoenic.orders360.controller.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Paint;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.customcontrols.NewLoading;
import com.telephoenic.orders360.controller.gcm.GCMRegistrar;
import com.telephoenic.orders360.controller.locationUtil.LocationHelper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.GPSLocation;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.controller.utils.IntentFactory;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.model.DialogWrapper;
import com.telephoenic.orders360.view.customcontrols.CustomAlertDialog;
import com.telephoenic.orders360.view.customcontrols.CustomDialog;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog;
import com.telephoenic.orders360.view.customcontrols.NoInternetConnectionDialog;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.LoginViewModel;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;

import static com.telephoenic.orders360.controller.utils.AppUtils.hideSoftKeyboard;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback, DialogUtils.OnDialogClickListener {

    private static final String TAG = "LoginActivity";
    @BindInput(R.id.edit_text_user_name)
    Orders360EditText editTextUserName;
    @BindInput(R.id.edit_text_user_pass)
    Orders360EditText editTextUserPass;
    private CheckBox rememberPassword;

    private UpdateGPSRequest updateGPSRequest;
    private String loginBase64 ="";

    // Retrofit and MVVM
    private LoginViewModel loginViewModel;
    private PosUserViewModel posUserViewModel;

    private CustomAlertDialog confirmDialog;
    private PosProfileWrapper posProfile;

    private Location mLastLocation;
    LocationHelper locationHelper;
    boolean isPermissionGranted;
    private CustomDialog dialogGPS;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    double latitude;
    double longitude;
    private NewLoading loadingDialog = new NewLoading();
    private OrdersView ordersView;

    private String myService = "";
    private ForgetPasswordDialog forgetPasswordDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ordersView = new OrdersView();
        ordersView.bind(this);
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        new GCMRegistrar(this).doGcmWork(); // for registrationId
        locationHelper = new LocationHelper(this);
        locationHelper.checkpermission();

        // check availability of play services
        if (locationHelper.checkPlayServices()) {
            // Building the GoogleApi client
            locationHelper.buildGoogleApiClient();
        }

       // AppUtils.getLayoutDirection(this);

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);

        createComponents();
//        Objects.requireNonNull(getSupportActionBar()).hide();
        hideSoftKeyboard(this.getWindow()); // to hide the keyboard when the activity created.
    }

    public boolean checkGPS() {
        boolean check = false;
        if (GPSLocation.isGPSAvilable(this)) {
            mLastLocation = locationHelper.getLocation();
            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                check = true;
            }
        } else {
            displayDialog();
        }
        return check;
    }

    public void displayDialog() {
        dialogGPS = new CustomDialog(LoginActivity.this);
        dialogGPS.setTitle(getString(R.string.titles_dialog_gps_enabling));
        dialogGPS.setMessage(getString(R.string.messages_warning_dialog_gps_enabling));
        dialogGPS.show();
        dialogGPS.getOkButton().setText(getString(R.string.button_labels_enableGPS));
        dialogGPS.getOkButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, AppConstants.OPEN_GPS_SETTINGS_REQUEST);
                dialogGPS.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case AppConstants.OPEN_GPS_SETTINGS_REQUEST:
                if (checkGPS())
                    // displayConfirmRechargeDialog();
                    break;
            case AppConstants.OPEN_WIFI_SETTINGS_REQUEST :
                if(forgetPasswordDialog!=null){
                    forgetPasswordDialog.onActivityResult(requestCode, resultCode, intent);
                }else {
                    if(AppConstants.SERVICE_LOGIN.equals(myService)){
                        sendLoginRequest();
                    } else if(AppConstants.SERVICE_UPDATE_RID.equals(myService)){
                        sendUpdateRegistrationID();
                    }
                }
                break;
            default:
                break;
        }
    }

    public void signInAutomaticlly() {
        AppPrefs.setIsRememberMe(this, rememberPassword.isChecked());
        if (rememberPassword.isChecked()) {
            AppPrefs.setUserName(this, editTextUserName.getEditTextContent().toString().trim());
            AppPrefs.setPassword(this, editTextUserPass.getEditTextContent().toString().trim());
        } else {
            AppPrefs.setUserName(this, "");
            AppPrefs.setPassword(this, "");
        }
    }

    private void createComponents() {
        Button loginBtn = findViewById(R.id.button_login);
        rememberPassword = findViewById(R.id.dont_forget_pass);
        rememberPassword.setChecked(AppPrefs.getIsRememberMe(this));
        rememberPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                signInAutomaticlly();
            }
        });

        if (AppPrefs.getIsRememberMe(this)) {
            editTextUserName.setText(AppPrefs.getUserName(this));
            editTextUserPass.setText(AppPrefs.getPassword(this));
        }

        TextView forgetPassword = findViewById(R.id.txt_forget_password);
        forgetPassword.setPaintFlags(forgetPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG); // this to underline the text view style.
        loginBtn.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                // Dia'a Modification
//                if (isInternetConnected(this)) {
                    if (isValidInputs()) {
                        //   startActivity(new Intent(this, FirstLoginChangePasswordActivity.class));
                        prepareLoginRequest();
                        sendLoginRequest();
                    }
               /* } else {
                    SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
                    DialogUtils.showSettingsDialog(this, settingsDialogModel, this);
                }*/
                break;
            case R.id.txt_forget_password:
                openForgetPasswordActivity();
                break;

            case R.id.btn_negative:
                if (AppConstants.TESTING_ON_EMULATER) {
                    navigateToHome();
                } else {
                    if (checkGPS()) {
                        try {
                            prepareUpdateRegistrationID();
                            sendUpdateRegistrationID();
                        } catch (Exception exceptionHandling) {
                            exceptionHandling.printStackTrace();
                        }
                    }
                }
                confirmDialog.dismiss();
                break;
            case R.id.btn_positive:
                confirmDialog.dismiss();
                break;
        }
    }

    private void openForgetPasswordActivity() {
//         startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
        forgetPasswordDialog = new ForgetPasswordDialog();
        forgetPasswordDialog.showNow(getSupportFragmentManager(), "");
    }

    private void navigateToHome() {
        Bundle bundle = new Bundle();
        bundle.putLong(IntentExtraNames.ID, posProfile.getUser().getId());
        AppConstants.MY_ACTIVITY = "Login";
        IntentFactory.startActivity(LoginActivity.this, MainActivity.class, true, true, bundle);
    }

    private void prepareLoginRequest() {
        String loginParameter = editTextUserName.getEditTextContent().toString().trim() + ":" + editTextUserPass.getEditTextContent().toString().trim();
        loginBase64 = AppUtils.stringToBase64(loginParameter).trim();
        AppPrefs.setLoginBase64(this, loginBase64);
        Log.e(TAG, "prepareLoginRequest: " +loginBase64 );
        myService = AppConstants.SERVICE_LOGIN;
    }

    private void sendLoginRequest() {
        LoadingDialog.showDialog(this);
        loginViewModel.login(this,loginBase64).observe(this, new ApiObserver<PosProfileWrapper>() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(PosProfileWrapper data) {
                try {
                    LoadingDialog.dismiss();
                    signInAutomaticlly();
                    String body = JsonUtils.toJsonString(data);
                    Log.e(TAG, "onSuccess: body "+ body);
                    posProfile = data;
                    if (posProfile.getUser().isInactive()) {
                        DialogWrapper dialogWrapper = new DialogWrapper();
                        dialogWrapper.setContext(LoginActivity.this);
                        dialogWrapper.setTitle(LoginActivity.this.getString(R.string.titles_dialog_general_error));
                        dialogWrapper.setMessage(LoginActivity.this.getString(R.string.inactive_user_error));
                        dialogWrapper.setIsFinished(false);
                        AppUtils.showAlertDialog(dialogWrapper);
                    } else if (posProfile.getUser().getFirstLogin()) {
                        AppPrefs.setPOS(LoginActivity.this, body);
                        AppPrefs.USER_ID = String.valueOf(posProfile.getUser().getId());
                        IntentFactory.startActivity(LoginActivity.this, FirstLoginChangePasswordActivity.class, false);
                    } else {
                        AppPrefs.setPOS(LoginActivity.this, body);
                        AppPrefs.USER_ID = String.valueOf(posProfile.getUser().getId());
                        if (AppConstants.USER_TYPE_POS.equals(posProfile.getUser().getType().getCode())) {
                            try {
                                prepareUpdateRegistrationID();
                                sendUpdateRegistrationID();
                            } catch (Exception exceptionHandling) {
                                exceptionHandling.printStackTrace();
                            }
                        } else {
                            navigateToHome();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                DialogWrapper dialogWrapper = new DialogWrapper();
                dialogWrapper.setContext(LoginActivity.this);
                dialogWrapper.setTitle(LoginActivity.this.getString(R.string.titles_dialog_general_error));
                dialogWrapper.setMessage(error);
                dialogWrapper.setIsFinished(false);
                AppUtils.showAlertDialog(dialogWrapper);
            }
        });
    }

    private void prepareUpdateRegistrationID() {
        updateGPSRequest = new UpdateGPSRequest();
        updateGPSRequest.setId(posProfile.getUser().getId());
        updateGPSRequest.setRegistrationId(AppPrefs.getRegistrationId(this));
    }

    private void sendUpdateRegistrationID(){
        myService = AppConstants.SERVICE_UPDATE_RID;
        posUserViewModel.updateRegistrationID(updateGPSRequest).observe(this, new ApiObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(ResponseBody data) {
                Log.d(TAG, "onSuccess: "+data);
                navigateToHome();
            }

            @Override
            protected void onError(String error) {
                Log.e(TAG, "onError: "+error );
                DialogWrapper dialogWrapper = new DialogWrapper();
                dialogWrapper.setContext(LoginActivity.this);
                dialogWrapper.setTitle(LoginActivity.this.getString(R.string.titles_dialog_general_error));
                dialogWrapper.setMessage(error);
                dialogWrapper.setIsFinished(false);
                AppUtils.showAlertDialog(dialogWrapper);
            }
        });
    }

    private boolean isValidInputs() {
        if (TextUtils.isEmpty(editTextUserName.getEditTextContent().toString().trim())) {
            editTextUserName.setErrorMessage(getString(R.string.toast_required_fill_user_name));
            editTextUserName.validate();
            editTextUserName.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(editTextUserPass.getEditTextContent().toString().trim())) {
            editTextUserPass.validate();
            editTextUserPass.setErrorMessage(getString(R.string.toast_required_fill_user_password));
            editTextUserPass.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("Connection failed:", " ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int i) {
        locationHelper.connectApiClient();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = locationHelper.getLocation();
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS),AppConstants.OPEN_WIFI_SETTINGS_REQUEST);  //Or ACTION_WIRELESS_SETTINGS
    }

    @Override
    public void onCancelClicked() {
//    System.exit(0);
    }

    private void noConnectionInternetDialog() {
//        NoInternetConnectionDialog connectionDialog = new NoInternetConnectionDialog(getContext(), this);
//        connectionDialog.show();
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(LoginActivity.this, settingsDialogModel, this);
    }
}