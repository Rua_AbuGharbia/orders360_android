package com.telephoenic.orders360.controller.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentDetailsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.PaymentDetails_TextView)
    public TextView paymentDetailsTextView;

    public PaymentDetailsViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
