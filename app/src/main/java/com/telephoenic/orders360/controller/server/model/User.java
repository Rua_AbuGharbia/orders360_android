package com.telephoenic.orders360.controller.server.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rua on 01-Feb-18.
 */
@Entity(tableName = "users")
public class User implements Serializable {

    public User() {

    }

    @PrimaryKey
    @ColumnInfo(name = "user_id")
    @JsonProperty("id")
    private Long id;

    @ColumnInfo(name = "name")
    @JsonProperty("name")
    private String name;

    @ColumnInfo(name = "inactive")
    @JsonProperty("inactive")
    private Boolean inactive;

    @ColumnInfo(name = "email")
    @JsonProperty("email")
    private String email;

    @ColumnInfo(name = "mobile_number")
    @JsonProperty("mobileNumber")
    private String mobileNumber;

    @ColumnInfo(name = "first_login")
    @JsonProperty("firstLogin")
    private Boolean firstLogin;

    @Ignore
    @JsonProperty("permissions") // TODO List of permissions
    private List<String> permissions;

    @Ignore
    @JsonProperty("region")
    private RegionModel region;

    @Ignore
    @JsonProperty("type")
    private TypeModel type;

    @ColumnInfo(name = "latitude")
    @JsonProperty("latitude")
    private Double latitude;


    @ColumnInfo(name = "longitude")
    @JsonProperty("longitude")
    private Double longitude;

    @ColumnInfo(name = "ownerName")
    @JsonProperty("ownerName")
    private String ownerName;

    @ColumnInfo(name = "ownerMobileNumber")
    @JsonProperty("ownerMobileNumber")
    private String ownerMobileNumber;

    @ColumnInfo(name = "landNumber")
    @JsonProperty("landNumber")
    private String landNumber;

    @ColumnInfo(name = "fax")
    @JsonProperty("fax")
    private String fax;

    // new added Diaa
    @Ignore
    @JsonProperty("posType")
    private PosTypeModel posType;

    @Ignore
    @JsonProperty("posGroup")
    private Long posGroup;

    // new requirment SDS , Dia'a.
    @Ignore
    @JsonProperty("trn")
    private String trn;

    @Ignore
    @JsonProperty("vendor")
    private VendorWrapper vendor;

    public String getTrn() {
        return trn;
    }

    public void setTrn(String trn) {
        this.trn = trn;
    }

    public boolean isAssignVendor() {
        return assignVendor;
    }

    public void setAssignVendor(boolean assignVendor) {
        this.assignVendor = assignVendor;
    }

    private boolean assignVendor;

    public PosTypeModel getPosType() {
        return posType;
    }

    public void setPosType(PosTypeModel posType) {
        this.posType = posType;
    }

    //added by rua just for testing ...

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Boolean getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public TypeModel getType() {
        return type;
    }

    public void setType(TypeModel type) {
        this.type = type;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public RegionModel getRegion() {
        return region;
    }

    public void setRegion(RegionModel region) {
        this.region = region;
    }

    public Boolean isInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerMobileNumber() {
        return ownerMobileNumber;
    }

    public void setOwnerMobileNumber(String ownerMobileNumber) {
        this.ownerMobileNumber = ownerMobileNumber;
    }

    public String getLandNumber() {
        return landNumber;
    }

    public void setLandNumber(String landNumber) {
        this.landNumber = landNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Long getPosGroup() {
        return posGroup;
    }

    public void setPosGroup(Long posGroup) {
        this.posGroup = posGroup;
    }

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inactive=" + inactive +
                ", email='" + email + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", firstLogin='" + firstLogin + '\'' +
                ", permissions=" + permissions +
                ", region=" + region +
                ", type=" + type +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", ownerMobileNumber='" + ownerMobileNumber + '\'' +
                ", landNumber='" + landNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", posType=" + posType +
                ", assignVendor=" + assignVendor +
                '}';
    }
}
