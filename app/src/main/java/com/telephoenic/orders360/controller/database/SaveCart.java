package com.telephoenic.orders360.controller.database;

import android.util.Log;

import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.SingleItem;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;


import java.util.ArrayList;
import java.util.List;

public final class SaveCart {
    private static SaveCart saveCart;
    private static List<AddToCartModel> addToCartModels;
    private static AddToCartModel selectVendorCart;

    public static SaveCart getInstance() {
        if (saveCart == null) {
            saveCart = new SaveCart();
            addToCartModels = new ArrayList<>();
            selectVendorCart = new AddToCartModel();
        }
        return saveCart;
    }

    public void appDateCart(List<AddToCartModel> cartItem) {
        addToCartModels = cartItem;
    }

    public List<AddToCartModel> getCart() {
        return addToCartModels;
    }

    public AddToCartModel getSelectVendorCart() {
        selectVendorCart = new AddToCartModel();

        for (int i = 0; i < addToCartModels.size(); i++) {
            if (addToCartModels.get(i).getVendor().getId().equals(AppPrefs.getVendorID(Order360Application.getInstance().getBaseContext()))) {
                selectVendorCart = addToCartModels.get(i);
            }
        }
        return selectVendorCart;
    }

    public void SaveSingleItem(ProductWrapper productWrapper) {
        double productTotalAmount;
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        boolean isFound = false;
        if (addToCartModel == null || addToCartModel.getSingleItems() == null) {
            SaveCart.getInstance().getSelectVendorCart();
            if (productWrapper.getDiscount() > 0) {
                productTotalAmount = productWrapper.getDiscountedPrice() * Integer.parseInt(productWrapper.getCount());

            } else {
                productTotalAmount = productWrapper.getPrice() * Integer.parseInt(productWrapper.getCount());

            }

            List<SingleItem> singleItems = new ArrayList<>();
            if (addToCartModel.getSingleItems() != null)
                singleItems = addToCartModel.getSingleItems();
            SingleItem singleItem = new SingleItem();
            singleItem.setPrice(productWrapper.getPrice());
            if (productWrapper.getDiscount() != 0.0) {
                singleItem.setHasDiscount(true);
            } else {
                singleItem.setHasDiscount(false);
            }
            if (productWrapper.getExpiryDate() != null) {
                singleItem.setExpiryDate(productWrapper.getExpiryDate());
            }
            singleItem.setProduct(productWrapper);
            singleItem.setDiscountedPrice(productWrapper.getDiscountedPrice());
            singleItem.setDiscountPercentage(productWrapper.getDiscount());
            singleItem.setTotalAmount(productTotalAmount);
            singleItem.setOrderedQuantity(productWrapper.getCount());
            singleItems.add(singleItem);
            addToCartModel.setSingleItems(singleItems);
            isFound = true;
        } else {
            for (int i = 0; i < addToCartModel.getSingleItems().size(); i++) {
                if (addToCartModel.getSingleItems().get(i).getProduct().getId().equals(productWrapper.getId())) {
                    isFound = true;
                    int count = Integer.parseInt(productWrapper.getCount())
                            + Integer.parseInt(addToCartModel.getSingleItems().get(i).getOrderedQuantity());
                    addToCartModel.getSingleItems().get(i).setOrderedQuantity(String.valueOf(count));
                    if (productWrapper.getDiscount() > 0) {
                        productTotalAmount = productWrapper.getDiscountedPrice() * count;

                    } else {
                        productTotalAmount = productWrapper.getPrice() * count;

                    }

                    addToCartModel.getSingleItems().get(i).setTotalAmount(productTotalAmount);
                }
            }
        }
        if (!isFound) {

            if (productWrapper.getDiscount() > 0) {
                productTotalAmount = productWrapper.getDiscountedPrice() * Integer.parseInt(productWrapper.getCount());

            } else {
                productTotalAmount = productWrapper.getPrice() * Integer.parseInt(productWrapper.getCount());

            }

            SingleItem singleItem = new SingleItem();
            singleItem.setPrice(productWrapper.getPrice());
            if (productWrapper.getDiscount() != 0.0) {
                singleItem.setHasDiscount(true);
            } else {
                singleItem.setHasDiscount(false);
            }
            singleItem.setProduct(productWrapper);
            singleItem.setDiscountedPrice(productWrapper.getDiscountedPrice());
            singleItem.setDiscountPercentage(productWrapper.getDiscount());
            singleItem.setTotalAmount(productTotalAmount);
            singleItem.setOrderedQuantity(productWrapper.getCount());
            addToCartModel.getSingleItems().add(singleItem);
        }

        SaveCart.selectVendorCart = addToCartModel;
    }

    public void prepearCartToSend(AddToCartModel cartModels) {
        double totalAmount = 0.0;
        boolean isFound = false;
        if (addToCartModels.size() == 0) {
            addToCartModels.add(cartModels);
            isFound = true;
        } else {
            for (int i = 0; i < addToCartModels.size(); i++) {
                if (addToCartModels.get(i).getVendor().getId().equals(cartModels.getVendor().getId())) {
                    addToCartModels.get(i).setSingleItems(cartModels.getSingleItems());
                    addToCartModels.get(i).setComboItems(cartModels.getComboItems());
                    addToCartModels.get(i).setPromotionItems(cartModels.getPromotionItems());
                    isFound = true;
                }
            }
        }
        if (!isFound) {
            addToCartModels.add(cartModels);
        }
        for (int i = 0; i < addToCartModels.size(); i++) {
            if (addToCartModels.get(i).getVendor().getId().equals(cartModels.getVendor().getId())) {

                if (addToCartModels.get(i).getSingleItems() != null) {
                    for (int j = 0; j < addToCartModels.get(i).getSingleItems().size(); j++) {
                        totalAmount = totalAmount + addToCartModels.get(i).getSingleItems().get(j).getTotalAmount();

                    }
                }
                if (addToCartModels.get(i).getComboItems() != null) {
                    for (int j = 0; j < addToCartModels.get(i).getComboItems().size(); j++) {
                        totalAmount = totalAmount + addToCartModels.get(i).getComboItems().get(j).getTotalAmount();
                    }
                }
                if (addToCartModels.get(i).getPromotionItems() != null) {
                    for (int j = 0; j < addToCartModels.get(i).getPromotionItems().size(); j++) {
                        totalAmount = totalAmount + addToCartModels.get(i).getPromotionItems().get(j).getTotalAmount();
                    }
                }
            }
            addToCartModels.get(i).setTotalAmount(totalAmount);
        }
    }

    public void addPromotionItem(PromotionItem promotionItem) {

        Log.e("PPPFF", JsonUtils.toJsonString(promotionItem));
        promotionItem.getProduct().setCommissionListAttribute(null);

        int count = 0;
        int totalOrderCount = 0;

        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();

        boolean isFound = false;
        if (addToCartModel == null || addToCartModel.getPromotionItems() == null) {
            isFound = true;
            SaveCart.getInstance().getSelectVendorCart();

            List<PromotionItem> promotionItems = new ArrayList<>();
            if (addToCartModel.getPromotionItems() != null) {
                promotionItems = addToCartModel.getPromotionItems();
            }
            promotionItems.add(promotionItem);
            addToCartModel.setPromotionItems(promotionItems);

        } else {
            for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
                if (promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
                    if (promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId())
                            && promotionItem.getPromotionItemDetailsList().get(0).getFreeItem().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getFreeItem())
                            && promotionItem.getPromotionItemDetailsList().get(0).getQuantity().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity())
                            && promotionItem.getProduct().getId().equals(addToCartModel.getPromotionItems().get(i).getProduct().getId())) {


                        addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setOrderQuantity(promotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity() + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getOrderQuantity());

                        count = (int) ((promotionItem.getPromotionItemDetailsList().get(0).getFreeItem() + promotionItem.getPromotionItemDetailsList().get(0).getQuantity()) * addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getOrderQuantity());
                        addToCartModel.getPromotionItems().get(i).setOrderedQuantity(String.valueOf(count));
                        addToCartModel.getPromotionItems().get(i).setTotalAmount(addToCartModel.getPromotionItems().get(i).getPrice() * addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getOrderQuantity() * addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity());

                        isFound = true;

                    }

                    ////////


//                if (addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
//                    if (addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {
//                        if (promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
//                            for (int j = 0; j < addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().size(); j++) {
//                                count = (int) (count + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(j).getOrderQuantity());
//                            }
//                            isFound = true;
//                            totalOrderCount = Integer.parseInt(promotionItem.getOrderedQuantity()) + Integer.parseInt(addToCartModel.getPromotionItems().get(i).getOrderedQuantity());
//                            count = (int) (count + promotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity());
//                            addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setFreeItem(promotionItem.getPromotionItemDetailsList().get(0).getFreeItem() + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getFreeItem());
//                            //  PromotionAttribute promotionAttribute = new PromotionAttribute(addToCartModel.getPromotionItems().get(i).getProduct().getCommissionListAttribute().get(0).getPromotionAttribute().getValue() + promotionItem.getProduct().getCommissionListAttribute().get(0).getPromotionAttribute().getValue());
//                            addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setQuantity(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity() + promotionItem.getPromotionItemDetailsList().get(0).getQuantity());
//                            addToCartModel.getPromotionItems().get(i).setOrderedQuantity(String.valueOf(totalOrderCount));
//                            double productTotalAmount = promotionItem.getPrice() * addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity();
//                            addToCartModel.getPromotionItems().get(i).setTotalAmount(productTotalAmount);
//                            addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setOrderQuantity((long) count);
//                        }
//                    }
                } else if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(promotionItem.getPromotion().getId())
                        && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
                        .get(0).getPromotionValueId().equals(promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {

                    for (int j = 0; j < addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().size(); j++) {
                        count = (int) (count + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(j).getOrderQuantity());
                    }
                    isFound = true;
                    totalOrderCount = Integer.parseInt(promotionItem.getOrderedQuantity()) + Integer.parseInt(addToCartModel.getPromotionItems().get(i).getOrderedQuantity());
                    count = (int) (count + promotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity());

                    addToCartModel.getPromotionItems().get(i).setOrderedQuantity(String.valueOf(totalOrderCount));
                    double productTotalAmount = promotionItem.getPrice() * promotionItem.getPromotionItemDetailsList().get(0).getQuantity() * count;
                    addToCartModel.getPromotionItems().get(i).setTotalAmount(productTotalAmount);
                    addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setOrderQuantity((long) count);
                }
            }
        }
        if (!isFound) {
            addToCartModel.getPromotionItems().add(promotionItem);
        }

        SaveCart.selectVendorCart = addToCartModel;
        Log.e("addToCartModel", JsonUtils.toJsonString(addToCartModel));


    }

    public void setPromotionItem(PromotionItem promotionItem) {
        Log.e("PPPFF", JsonUtils.toJsonString(promotionItem));
        promotionItem.getProduct().setCommissionListAttribute(null);

        int count = 0;
        int totalOrderCount = 0;

        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();

        boolean isFound = false;
        if (addToCartModel == null || addToCartModel.getPromotionItems() == null) {
            isFound = true;
            SaveCart.getInstance().getSelectVendorCart();

            List<PromotionItem> promotionItems = new ArrayList<>();
            if (addToCartModel.getPromotionItems() != null) {
                promotionItems = addToCartModel.getPromotionItems();
            }
            promotionItems.add(promotionItem);
            addToCartModel.setPromotionItems(promotionItems);

        } else {
            for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
                if (addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
                    if (addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {
                        if (promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
                            for (int j = 0; j < addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().size(); j++) {
                                count = (int) (count + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(j).getOrderQuantity());
                            }
                            isFound = true;
                            totalOrderCount = Integer.parseInt(promotionItem.getOrderedQuantity()) + Integer.parseInt(addToCartModel.getPromotionItems().get(i).getOrderedQuantity());
                            count = (int) (count + promotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity());
                            addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setFreeItem(promotionItem.getPromotionItemDetailsList().get(0).getFreeItem() + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getFreeItem());
                            //  PromotionAttribute promotionAttribute = new PromotionAttribute(addToCartModel.getPromotionItems().get(i).getProduct().getCommissionListAttribute().get(0).getPromotionAttribute().getValue() + promotionItem.getProduct().getCommissionListAttribute().get(0).getPromotionAttribute().getValue());
                            addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setQuantity(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity() + promotionItem.getPromotionItemDetailsList().get(0).getQuantity());
                            addToCartModel.getPromotionItems().get(i).setOrderedQuantity(String.valueOf(totalOrderCount));
                            double productTotalAmount = promotionItem.getPrice() * addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity();
                            addToCartModel.getPromotionItems().get(i).setTotalAmount(productTotalAmount);
                            addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setOrderQuantity((long) count);
                        }
                    }
                } else if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(promotionItem.getPromotion().getId())
                        && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
                        .get(0).getPromotionValueId().equals(promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {

                    for (int j = 0; j < addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().size(); j++) {
                        count = (int) (count + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(j).getOrderQuantity());
                    }
                    isFound = true;
                    totalOrderCount = Integer.parseInt(promotionItem.getOrderedQuantity()) + Integer.parseInt(addToCartModel.getPromotionItems().get(i).getOrderedQuantity());
                    count = (int) (count + promotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity());

                    addToCartModel.getPromotionItems().get(i).setOrderedQuantity(String.valueOf(totalOrderCount));
                    double productTotalAmount = promotionItem.getPrice() * promotionItem.getPromotionItemDetailsList().get(0).getQuantity() * count;
                    addToCartModel.getPromotionItems().get(i).setTotalAmount(productTotalAmount);
                    addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).setOrderQuantity((long) count);
                }
            }
        }
        if (!isFound) {
            addToCartModel.getPromotionItems().add(promotionItem);
        }

        SaveCart.selectVendorCart = addToCartModel;
        Log.e("addToCartModel", JsonUtils.toJsonString(addToCartModel));

    }

    public void setPromotiomAtrebute(PromotionItem promotionItem) {
        promotionItem.getProduct().setCommissionListAttribute(null);


        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();

        boolean isFound = false;
        if (addToCartModel == null || addToCartModel.getPromotionItems() == null) {
            isFound = true;
            SaveCart.getInstance().getSelectVendorCart();

            List<PromotionItem> promotionItems = new ArrayList<>();
            if (addToCartModel.getPromotionItems() != null) {
                promotionItems = addToCartModel.getPromotionItems();
            }
            promotionItems.add(promotionItem);
            addToCartModel.setPromotionItems(promotionItems);

        } else {
            addToCartModel.getPromotionItems().add(promotionItem);
        }
        SaveCart.selectVendorCart = addToCartModel;
        prepearCartToSend(addToCartModel);
    }

    public int NotifactionCount() {
        int notifcationCount = 0;
        if (SaveCart.getInstance().getSelectVendorCart() == null) {
            return notifcationCount;
        } else {
            if (saveCart.getSelectVendorCart().getSingleItems() != null) {
                notifcationCount = notifcationCount + saveCart.getSelectVendorCart().getSingleItems().size();
            }
            if (saveCart.getSelectVendorCart().getPromotionItems() != null) {
                notifcationCount = notifcationCount + saveCart.getSelectVendorCart().getPromotionItems().size();
            }
            if (saveCart.getSelectVendorCart().getComboItems() != null) {
                notifcationCount = notifcationCount + saveCart.getSelectVendorCart().getComboItems().size();
            }
            return notifcationCount;
        }
    }

    public void ClearCart() {
        selectVendorCart = new AddToCartModel();
        addToCartModels = new ArrayList<>();

    }

    public void SaveVendor(VendorWrapper vendorWrapper) {
        boolean found = false;
        if (addToCartModels.size() > 0) {
            for (int i = 0; i < addToCartModels.size(); i++) {
                if (addToCartModels.get(i).getVendor().getId().equals(vendorWrapper.getId())) {
                    found = true;
                }
            }
        } else {
            AddToCartModel addToCartModel = new AddToCartModel();
            addToCartModel.setVendor(vendorWrapper);
            addToCartModels.add(addToCartModel);
            found = true;
        }
        if (!found) {
            AddToCartModel addToCartModel = new AddToCartModel();
            addToCartModel.setVendor(vendorWrapper);
            addToCartModels.add(addToCartModel);
        }

    }

    public void setBundleItem(ComboItem orderItems) {
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        boolean isFound = false;
        if (addToCartModel == null || addToCartModel.getComboItems() == null) {
            List<ComboItem> comboItems = new ArrayList<>();
            comboItems.add(orderItems);
            addToCartModel.setComboItems(comboItems);
            isFound = true;
        } else {
            for (int i = 0; i < addToCartModel.getComboItems().size(); i++) {
                if (addToCartModel.getComboItems().get(i).getCombo().getId().equals(orderItems.getCombo().getId())) {
                    isFound = true;
                    double totalAmount = orderItems.getTotalAmount() + addToCartModel.getComboItems().get(i).getTotalAmount();
                    addToCartModel.getComboItems().get(i).setTotalAmount(totalAmount);
                    int orderQuantity = Integer.parseInt(addToCartModel.getComboItems().get(i).getOrderedQuantity()) + Integer.parseInt(orderItems.getOrderedQuantity());
                    addToCartModel.getComboItems().get(i).setOrderedQuantity(String.valueOf(orderQuantity));
                }
            }
        }
        if (!isFound) {

            addToCartModel.getComboItems().add(orderItems);

        }
        SaveCart.selectVendorCart = addToCartModel;
        Log.e("addToCartModel", JsonUtils.toJsonString(addToCartModel));

    }

    public void DeleteItemCart(SingleItem productWrapper) {
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        for (int i = 0; i < addToCartModel.getSingleItems().size(); i++) {
            if (addToCartModel.getSingleItems().get(i).getProduct().getId().equals(productWrapper.getProduct().getId())) {
                addToCartModel.getSingleItems().remove(i);
            }
        }

        SaveCart.selectVendorCart = addToCartModel;
        prepearCartToSend(addToCartModel);

    }

    public void DeleteItemCart(PromotionItem promotionItem) {
        Long valueID = Long.valueOf(-1);
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
            if (promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId().equals(valueID)) {
                if (addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId()) &&
                        promotionItem.getPromotionItemDetailsList().get(0).getFreeItem().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getFreeItem()) &&
                        promotionItem.getPromotionItemDetailsList().get(0).getQuantity().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity())) {
                    addToCartModel.getPromotionItems().remove(i);
                }
            } else if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(promotionItem.getPromotion().getId())
                    && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
                    .get(0).getPromotionValueId().equals(promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {
                addToCartModel.getPromotionItems().remove(i);

            }
        }

        SaveCart.selectVendorCart = addToCartModel;
        prepearCartToSend(addToCartModel);
    }

    public void DeleteItemCart(ComboItem orderItems) {
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        for (int i = 0; i < addToCartModel.getComboItems().size(); i++) {
            if (addToCartModel.getComboItems().get(i).getCombo().getId().equals(orderItems.getCombo().getId())) {
                addToCartModel.getComboItems().remove(i);
            }
        }
        SaveCart.selectVendorCart = addToCartModel;
        prepearCartToSend(addToCartModel);
    }

    public void changeProductOrderedQuantity(ProductWrapper productWrapper) {
        double productTotalAmount;
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();

        for (int i = 0; i < addToCartModel.getSingleItems().size(); i++) {
            if (addToCartModel.getSingleItems().get(i).getProduct().getId().equals(productWrapper.getId())) {
                int orderCount = Integer.parseInt(productWrapper.getCount());
                if (productWrapper.getDiscount() > 0) {
                    productTotalAmount = productWrapper.getDiscountedPrice() * Integer.parseInt(productWrapper.getCount());

                } else {
                    productTotalAmount = productWrapper.getPrice() * Integer.parseInt(productWrapper.getCount());

                }
                addToCartModel.getSingleItems().get(i).setTotalAmount(productTotalAmount);
                addToCartModel.getSingleItems().get(i).setOrderedQuantity(String.valueOf(orderCount));
            }
        }
        SaveCart.selectVendorCart = addToCartModel;
        prepearCartToSend(addToCartModel);
    }

    public void updatePromoitionAt(PromotionItem promotionItem, PromotionItem oldPromotionItem) {
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
            if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(oldPromotionItem.getPromotion().getId())
                    && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
                    .get(0).getPromotionValueId().equals(oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId())) {
                addToCartModel.getPromotionItems().set(i, promotionItem);
                SaveCart.selectVendorCart = addToCartModel;
                Log.e("ATT", JsonUtils.toJsonString(addToCartModel));
//                prepearCartToSend(addToCartModel);
//                setPromotiomAtrebute(promotionItem);
                prepearCartToSend(SaveCart.selectVendorCart);

            }
        }
    }

    public void changeProductOrderedQuantity(PromotionItem promotionItem, PromotionItem oldPromotionItem, Long promotionValueId) {
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();


        for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
            if (promotionValueId == -1) {
                if (addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId().equals(oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId())&&
                        addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(oldPromotionItem.getProduct().getId()) &&
                        oldPromotionItem.getPromotionItemDetailsList().get(0).getFreeItem().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getFreeItem()) &&
                        oldPromotionItem.getPromotionItemDetailsList().get(0).getQuantity().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity()))
                {

                    addToCartModel.getPromotionItems().remove(i);
                    SaveCart.selectVendorCart = addToCartModel;
                    prepearCartToSend(addToCartModel);
                    addPromotionItem(promotionItem);
                    prepearCartToSend(SaveCart.selectVendorCart);
                }

            } else if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(oldPromotionItem.getPromotion().getId())
                    && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
                    .get(0).getPromotionValueId().equals(oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(oldPromotionItem.getProduct().getId())) {

                addToCartModel.getPromotionItems().remove(i);
                prepearCartToSend(addToCartModel);
                addPromotionItem(promotionItem);
                prepearCartToSend(SaveCart.selectVendorCart);

            }
        }


//        if (promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId().equals(promotionValueId)) {
//            for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
//
//                if (addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
//                    if (addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId()) &&
//                            promotionItem.getPromotionItemDetailsList().get(0).getFreeItem().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getFreeItem()) &&
//                            promotionItem.getPromotionItemDetailsList().get(0).getQuantity().equals(addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getQuantity())){
//
//                        addToCartModel.getPromotionItems().remove(i);
//                        SaveCart.selectVendorCart = addToCartModel;
//                        prepearCartToSend(addToCartModel);
//                        setPromotionItem(promotionItem);
//                        prepearCartToSend(SaveCart.selectVendorCart);
//                    }
//                }
//
//
//                if (addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
//                    if (addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {
//                        addToCartModel.getPromotionItems().remove(i);
//                        SaveCart.selectVendorCart = addToCartModel;
//                        prepearCartToSend(addToCartModel);
//                        setPromotionItem(promotionItem);
//                        prepearCartToSend(SaveCart.selectVendorCart);
//                    }
//                } else if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(promotionItem.getPromotion().getId())
//                        && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
//                        .get(0).getPromotionValueId().equals(promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {
//                    addToCartModel.getPromotionItems().remove(i);
//                    SaveCart.selectVendorCart = addToCartModel;
//                    prepearCartToSend(addToCartModel);
//                    setPromotionItem(promotionItem);
//                    prepearCartToSend(SaveCart.selectVendorCart);
//
//                }
//            }
//        } else {
//            for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
//                if (addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
//                    if (addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(oldPromotionItem.getProduct().getId())) {
//                        addToCartModel.getPromotionItems().remove(i);
//                        prepearCartToSend(addToCartModel);
//                        setPromotionItem(promotionItem);
//                        prepearCartToSend(SaveCart.selectVendorCart);
//                    }
//                } else if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(oldPromotionItem.getPromotion().getId())
//                        && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
//                        .get(0).getPromotionValueId().equals(oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(oldPromotionItem.getProduct().getId())) {
//
//                    addToCartModel.getPromotionItems().remove(i);
//                    prepearCartToSend(addToCartModel);
//                    setPromotionItem(promotionItem);
//                    prepearCartToSend(SaveCart.selectVendorCart);
//
//                }
//            }
//        }
    }

    public void changeProductOrderedQuantity(ComboItem comboWrapper) {
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();

        for (int i = 0; i < addToCartModel.getComboItems().size(); i++) {
            if (addToCartModel.getComboItems().get(i).getCombo().getId().equals(comboWrapper.getCombo().getId())) {
                addToCartModel.getComboItems().remove(i);
                SaveCart.selectVendorCart = addToCartModel;
                prepearCartToSend(addToCartModel);
                setBundleItem(comboWrapper);
                prepearCartToSend(SaveCart.selectVendorCart);
            }
        }
    }

    public int getProductCount(ProductWrapper productWrapper) {
        int totalCount = 0;

        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();
        if (addToCartModel != null && addToCartModel.getSingleItems() != null) {
            for (int i = 0; i < addToCartModel.getSingleItems().size(); i++) {
                if (productWrapper.getId().equals(addToCartModel.getSingleItems().get(i).getProduct().getId())) {
                    totalCount = Integer.parseInt(productWrapper.getCount()) + Integer.parseInt(addToCartModel.getSingleItems().get(i).getOrderedQuantity());
                }
            }
        }
        return totalCount;
    }

    public int getPromotionCount(PromotionItem promotionItem) {
        Log.e("PDC", JsonUtils.toJsonString(promotionItem));
        int totalCount = 0;
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        Log.e("PDC2", JsonUtils.toJsonString(addToCartModel));

        SaveCart.getInstance().getSelectVendorCart();
        if (addToCartModel != null && addToCartModel.getPromotionItems() != null) {
            for (int i = 0; i < addToCartModel.getPromotionItems().size(); i++) {
                if (addToCartModel.getPromotionItems().get(i).getPromotion().getId().equals(promotionItem.getPromotion().getId())
                        && addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList()
                        .get(0).getPromotionValueId().equals(promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) && addToCartModel.getPromotionItems().get(i).getProduct().getId().equals(promotionItem.getProduct().getId())) {

                    for (int j = 0; j < addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().size(); j++) {
                        totalCount = (int) (totalCount + addToCartModel.getPromotionItems().get(i).getPromotionItemDetailsList().get(j).getOrderQuantity());
                    }
                    totalCount = (int) (totalCount + promotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity());
                }
            }
        }
        return totalCount;
    }

    public int getCompoCount(ComboItem orderItems) {
        int totalCount = 0;
        AddToCartModel addToCartModel = SaveCart.getInstance().getSelectVendorCart();
        SaveCart.getInstance().getSelectVendorCart();

        if (addToCartModel != null && addToCartModel.getComboItems() != null) {
            for (int i = 0; i < addToCartModel.getComboItems().size(); i++) {
                if (addToCartModel.getComboItems().get(i).getCombo().getId().equals(orderItems.getCombo().getId())) {
                    totalCount = Integer.parseInt(addToCartModel.getComboItems().get(i).getOrderedQuantity()) + Integer.parseInt(orderItems.getOrderedQuantity());
                }
            }
        }
        return totalCount;
    }
}
