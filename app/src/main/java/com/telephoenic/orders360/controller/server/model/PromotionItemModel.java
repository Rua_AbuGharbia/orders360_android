package com.telephoenic.orders360.controller.server.model;

public class PromotionItemModel {
    private String title;
    private String quantity;

    public PromotionItemModel(String title, String quantity) {
        this.title = title;
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
