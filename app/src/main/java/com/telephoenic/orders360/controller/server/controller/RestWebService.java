package com.telephoenic.orders360.controller.server.controller;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ErrorResponse;
import com.telephoenic.orders360.controller.server.model.ResponseWrapper;
import com.telephoenic.orders360.controller.utils.RestTemplateConfigure;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class RestWebService {

    private static ResponseWrapper responseWrapper;
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static Context context = Order360Application.getInstance().getApplicationContext();

    protected static Object callGet(String url, Map<String, String> params) {
        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeaders(Order360Application.getInstance().getApplicationContext());

            Map<String, Object> uriVariables = new HashMap<String, Object>();

            if(params != null)
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    uriVariables.put(entry.getKey(), entry.getValue());
                }
            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.GET, new org.springframework.http.HttpEntity(headers),
                            String.class, uriVariables);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) { // TODO set OK
                response = result.getBody();
                Log.e("Rua",result.getBody());
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper;
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.UNAUTHORIZED) { // TODO set UNAUTHORIZED
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrorCode(org.springframework.http.HttpStatus.UNAUTHORIZED+""); // TODO Login again
                errorResponse.setDeveloperMessage(context.getString(R.string.time_out_error));
                responseWrapper = new ResponseWrapper();
                responseWrapper.setResponse(errorResponse);
                response = responseWrapper;
                return response;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;

    }

    protected static Object callGetJson(String url) {  // TODO for Login
        Object response = null;
        MultiValueMap<String, Object> responseMap = new LinkedMultiValueMap<String, Object>();;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeadersLogin(Order360Application.getInstance().getApplicationContext());

            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.GET, new org.springframework.http.HttpEntity("", headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
               String athuntication =  result.getHeaders().getFirst(" x-auth-token");
               // Log.i("athuntication",result.getHeaders().getAuthorization());
                responseMap.add(AppConstants.HEADERS,result.getHeaders());
                responseMap.add(AppConstants.BODY,result.getBody());
               // response = result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper;
                return response;
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.UNAUTHORIZED) {
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setDeveloperMessage(context.getString(R.string.error_user_password));
                responseWrapper = new ResponseWrapper();
                responseWrapper.setResponse(errorResponse);
                response = responseWrapper;
                return response;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        if(responseMap.size() == 0){
            return null;
        }

        return responseMap;

    }
    protected static Object callGet(String url) {
        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeaders(Order360Application.getInstance().getApplicationContext());

            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.GET, new org.springframework.http.HttpEntity("", headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response = result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper;
               // return response;
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.UNAUTHORIZED) { // TODO set UNAUTHORIZED
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrorCode(org.springframework.http.HttpStatus.UNAUTHORIZED+""); // TODO Login again
                errorResponse.setDeveloperMessage(context.getString(R.string.time_out_error));
                responseWrapper = new ResponseWrapper();
                responseWrapper.setResponse(errorResponse);
                response = responseWrapper;
                return response;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }


        return response;

    }

    protected static Object callPutJson(String url, String json) {
        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeadersJson(Order360Application.getInstance().getApplicationContext());


            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.PUT, new org.springframework.http.HttpEntity(json, headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response = result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;

    }

    protected static Object callPut(String url, String json) {

        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeaders(Order360Application.getInstance().getApplicationContext());


            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.PUT, new org.springframework.http.HttpEntity(json, headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response =  result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper ;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;
    }

    protected static Object callPut(String url, Map<String, String> params) {

        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeaders(Order360Application.getInstance().getApplicationContext());

            Map<String, Object> uriVariables = new HashMap<String, Object>();

            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriVariables.put(entry.getKey(), entry.getValue());
            }

            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.PUT, new org.springframework.http.HttpEntity(uriVariables, headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response =  result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper ;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;
    }

    public static Object callPostService(String url, String json) {
        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeadersJson(Order360Application.getInstance().getApplicationContext());

            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.POST, new org.springframework.http.HttpEntity(json, headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                if(result.getBody()==null){
                    response = json;
                } else{
                    response =  result.getBody();
                }
            }  else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper ;
            }  else if (result.getStatusCode() == org.springframework.http.HttpStatus.UNAUTHORIZED) { // TODO set UNAUTHORIZED
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrorCode(org.springframework.http.HttpStatus.UNAUTHORIZED+""); // TODO Login again
                errorResponse.setDeveloperMessage(context.getString(R.string.time_out_error));
                responseWrapper = new ResponseWrapper();
                responseWrapper.setResponse(errorResponse);
                response = responseWrapper;
                return response;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        if(response != null)
        System.out.println("response="+response);
        //System.out.println(response.toString());
        return response;
    }

    public static Object callPostService(String url, Map<String, String> params , boolean urlParam ) {

        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeadersJson(Order360Application.getInstance().getApplicationContext());

            Map<String, Object> uriVariables = new HashMap<String, Object>();

            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriVariables.put(entry.getKey(), entry.getValue());
            }
            ResponseEntity<String> result = null;
            if(urlParam){
                result = RestTemplateConfigure
                        .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                                HttpMethod.POST, new org.springframework.http.HttpEntity( headers),
                                String.class,uriVariables);
            } else {
                result = RestTemplateConfigure
                        .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                                HttpMethod.POST, new org.springframework.http.HttpEntity(uriVariables, headers),
                                String.class);
            }

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response =  result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper ;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;
    }

    public static Object callPostServiceLong(String url, Map<String, Long> params) {

        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeadersJson(Order360Application.getInstance().getApplicationContext());

            Map<String, Object> uriVariables = new HashMap<String, Object>();

            for (Map.Entry<String, Long> entry : params.entrySet()) {
                uriVariables.put(entry.getKey(), entry.getValue());
            }

            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.POST, new org.springframework.http.HttpEntity(uriVariables, headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response =  result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper ;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;
    }


    public static Object postService(String url, String json) {

        Object response = null;

        try {

            //RequestBody body = RequestBody.create(JSON, json);

            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeadersJson(Order360Application.getInstance().getApplicationContext());

            Map<String, Object> uriVariables = new HashMap<String, Object>();

            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.POST, new org.springframework.http.HttpEntity(uriVariables, headers),
                            String.class);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response =  result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper ;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;
    }


    protected static Object callPost(String url, Map<String, String> params ,String json) {

        Object response = null;

        try {
            MultiValueMap<String, Object> headers = RestTemplateConfigure
                    .buildHeaders(Order360Application.getInstance().getApplicationContext());

            Map<String, Object> uriVariables = new HashMap<String, Object>();

            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriVariables.put(entry.getKey(), entry.getValue());
            }
            ResponseEntity<String> result = RestTemplateConfigure
                    .configureRestTemplate(Order360Application.getInstance().getApplicationContext()).exchange(url,
                            HttpMethod.POST, new org.springframework.http.HttpEntity(json,headers),
                            String.class, uriVariables);

            if (result.getStatusCode() == org.springframework.http.HttpStatus.OK) {
                response = result.getBody();
            } else if (result.getStatusCode() == org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR) {
                responseWrapper = objectMapper.readValue(
                        result.getBody(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        ResponseWrapper.class,
                                        ErrorResponse.class));
                response = responseWrapper;
            }
        } catch (RestClientException rce) {
            return rce;
        } catch (JsonParseException jpe) {
            return jpe;
        } catch (JsonMappingException jme) {
            return jme;
        } catch (IOException ioe) {
            return ioe;
        }

        return response;

    }

    protected abstract Object callService(String... params);

}