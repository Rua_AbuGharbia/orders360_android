package com.telephoenic.orders360.controller.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.lang.ref.WeakReference;


public class CircleAnimationUtil {
    private static final int DEFAULT_DURATION = 1000;
    private static final int DEFAULT_DURATION_DISAPPEAR = 200;
    private View mTarget;
    private View mDest;

    private float originX;
    private float originY;
    private float destX;
    private float destY;
    private Context context;

    private int mCircleDuration = DEFAULT_DURATION;
    private int mMoveDuration = DEFAULT_DURATION;
    private int mDisappearDuration = DEFAULT_DURATION_DISAPPEAR;

    private Activity mContextReference;
    private int mBorderWidth = 4;
    private int mBorderColor = Color.BLACK;
    //    private CircleLayout mCircleLayout;
    private Bitmap mBitmap;
    private ImageView mImageView;
    private int[] src = new int[2];
    private Animator.AnimatorListener mAnimationListener;

    public CircleAnimationUtil() {
    }

    public CircleAnimationUtil attachActivity(Activity activity) {
        mContextReference = activity;
        context = activity;
        return this;
    }

    public CircleAnimationUtil setTargetView(View view) {
        mTarget = new View(mContextReference);
        mTarget = view;
        mTarget.getLocationOnScreen(src);
        Log.e("SSSS", src[0] + "");
        Log.e("SSSS", src[1] + "");
        setOriginRect(mTarget.getWidth(), mTarget.getHeight());
        return this;
    }

    private CircleAnimationUtil setOriginRect(float x, float y) {
        originX = x;
        originY = y;
        return this;
    }

    private CircleAnimationUtil setDestRect(float x, float y) {
        destX = x;
        destY = y;
        return this;
    }

    public CircleAnimationUtil setDestView(View view) {
        mDest = view;
        setDestRect(mDest.getWidth(), mDest.getWidth());
        return this;
    }

    public CircleAnimationUtil setBorderWidth(int width) {
        mBorderWidth = width;
        return this;
    }

    public CircleAnimationUtil setBorderColor(int color) {
        mBorderColor = color;
        return this;
    }

    public CircleAnimationUtil setCircleDuration(int duration) {
        mCircleDuration = duration;
        return this;
    }

    public CircleAnimationUtil setMoveDuration(int duration) {
        mMoveDuration = duration;
        return this;
    }

    private boolean prepare() {
        if (mContextReference != null) {
            mBitmap = drawViewToBitmap(mTarget, mTarget.getWidth(), mTarget.getHeight());
            mImageView = new ImageView(mContextReference);
            mImageView.setImageBitmap(mBitmap);
            int[] src = new int[2];
            mTarget.getLocationOnScreen(src);

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(mTarget.getWidth(), mTarget.getHeight());
            params.leftMargin = src[0];
            params.topMargin = src[1];
           // params.setMargins(src[0], src[1], 0, 0);

            if (mImageView.getParent() == null){
                ViewGroup decoreView = (ViewGroup) mContextReference.getWindow().getDecorView();
                decoreView.addView(mImageView,params);
            }
        }
        return true;
    }

    public void startAnimation() {
        if (prepare()) {
            mTarget.setVisibility(View.INVISIBLE);
            getAvatarRevealAnimator().start();
        }
    }

    private AnimatorSet getAvatarRevealAnimator() {
        final float endRadius = Math.max(destX, destY) / 2;
        final float startRadius = Math.max(originX, originY);
        @SuppressLint("ObjectAnimatorBinding")
        Animator mRevealAnimator = ObjectAnimator.ofFloat(mImageView, "drawableRadius", startRadius, endRadius * 1.05f, endRadius * 0.9f, endRadius);
        mRevealAnimator.setInterpolator(new AccelerateInterpolator());

        //final float scaleFactor = Math.max(2f * destY / originY, 2f * destX / originX);
        final float scaleFactor = 0.5f;
        Animator scaleAnimatorY = ObjectAnimator.ofFloat(mImageView, View.SCALE_Y, 1, 1, scaleFactor, scaleFactor);
        Animator scaleAnimatorX = ObjectAnimator.ofFloat(mImageView, View.SCALE_X, 1, 1, scaleFactor, scaleFactor);

        AnimatorSet animatorCircleSet = new AnimatorSet();
        animatorCircleSet.setDuration(mCircleDuration);
        animatorCircleSet.playTogether(scaleAnimatorX, scaleAnimatorY, mRevealAnimator);
        animatorCircleSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (mAnimationListener != null)
                    mAnimationListener.onAnimationStart(animation);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                int[] dest = new int[2];
                int[] src = new int[2];
                mDest.getLocationOnScreen(dest);
                mImageView.getLocationOnScreen(src);
                float y = mImageView.getY();
                float x = mImageView.getX();
                Animator translatorX = ObjectAnimator.ofFloat(mImageView, View.X, x, x + dest[0] - (src[0] + (originX * scaleFactor - 2 * endRadius * scaleFactor) / 2) + (0.5f * destX - scaleFactor * endRadius));
                translatorX.setInterpolator(new TimeInterpolator() {
                    @Override
                    public float getInterpolation(float input) {
//                        return (float) (Math.sin((0.5f * input) * Math.PI));
                        //-(1-x)^2+1
                        return (float) (-Math.pow(input - 1, 2) + 1f);
                    }
                });
                Animator translatorY = ObjectAnimator.ofFloat(mImageView, View.Y, y, y + dest[1] - (src[1] + (originY * scaleFactor - 2 * endRadius * scaleFactor) / 2) + (0.5f * destY - scaleFactor * endRadius));
                translatorY.setInterpolator(new LinearInterpolator());

                AnimatorSet animatorMoveSet = new AnimatorSet();
                animatorMoveSet.playTogether(translatorX, translatorY);
                animatorMoveSet.setDuration(mMoveDuration);

                AnimatorSet animatorDisappearSet = new AnimatorSet();
                Animator disappearAnimatorY = ObjectAnimator.ofFloat(mImageView, View.SCALE_Y, scaleFactor, 0);
                Animator disappearAnimatorX = ObjectAnimator.ofFloat(mImageView, View.SCALE_X, scaleFactor, 0);
                animatorDisappearSet.setDuration(mDisappearDuration);
                animatorDisappearSet.playTogether(disappearAnimatorX, disappearAnimatorY);


                AnimatorSet total = new AnimatorSet();
                total.playSequentially(animatorMoveSet, animatorDisappearSet);
                total.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (mAnimationListener != null)
                            mAnimationListener.onAnimationEnd(animation);
                        reset();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                total.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        return animatorCircleSet;
    }

    private Bitmap drawViewToBitmap(View view, int width, int height) {
        Bitmap dest = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas();
        c.setBitmap(dest);
        Drawable drawable = new BitmapDrawable(context.getResources(), dest);
        Rect rect = new Rect();
        rect.set(0, 0, width, height);
        drawable.setBounds(rect);
        drawable.draw(c);
        view.draw(c);
        return dest;
    }

    private void reset() {
        mBitmap.recycle();
        mBitmap = null;
        if (mImageView.getParent() != null)
            ((ViewGroup) mImageView.getParent()).removeView(mImageView);
        mImageView = null;
//        mTarget.setVisibility(View.VISIBLE);
    }

    public CircleAnimationUtil setAnimationListener(Animator.AnimatorListener listener) {
        mAnimationListener = listener;
        return this;
    }
}
