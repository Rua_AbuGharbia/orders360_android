package com.telephoenic.orders360.controller.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.telephoenic.orders360.controller.server.model.User;

import java.util.List;

/**
 * Created by Rua on 03-Jan-18.
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM users")
    List<User> getAll();

   /* @Query("SELECT * FROM users WHERE user_name LIKE :name AND password LIKE :password LIMIT 1")
    User findByNameAndPassword(String name, String password);*/

    @Query("SELECT * FROM users WHERE user_id =:user_id LIMIT 1")
    User findUser(Long user_id);

        @Insert
        void insertAll(List<User> users);

        @Insert
        void insertUser(User user);

        @Update
        void updateUser(User user);

        @Delete
        void deleteUser(User user);
}
