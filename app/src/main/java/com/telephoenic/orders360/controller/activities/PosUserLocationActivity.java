package com.telephoenic.orders360.controller.activities;

import android.Manifest;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;
import com.telephoenic.orders360.viewmodel.UserViewModel;

import java.util.Objects;

public class PosUserLocationActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
        , LocationSource.OnLocationChangedListener, DialogUtils.OnDialogClickListener {

    private Dialog dialog;

    //AlertDialog.Builder dialog;
    private GoogleMap mMap;
    private static final String TAG = "PosUserLocationActivity";
    private boolean newPos = false;
    private boolean isDialogVisible = false;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 20000; /* 20 sec */

    // Views
    private Button cancelButton, saveButton;
    // wa7
    private ImageView pinView;
    private double lat, lng;
    private LatLng currentLatLng;
    private User posUser = null;
    private PosProfileWrapper posProfileWrapper;

    private PosUserViewModel posUserViewModel;
    private UserViewModel userViewModel;
    private String myService ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.white));
        }
        dialog = new Dialog(this);
        posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        // getUserData();


        initViews();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView imageViewBack = toolbar.findViewById(R.id.imageView_back);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    }

    private void initViews() {
        cancelButton = findViewById(R.id.location_activity_cancel_button);
        saveButton = findViewById(R.id.location_activity_save_button);
        pinView = findViewById(R.id.pin);
        cancelButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            DialogUtils.showLocationPermissionDialog(PosUserLocationActivity.this, getResources().getString(R.string.location_permission_title), getResources().getString(R.string.location_permission_message), new DialogUtils.OnDialogClickListener() {
                @Override
                public void onOkClicked() {
                    Log.e(TAG, "onOkClicked: Clicked");
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                @Override
                public void onCancelClicked() {
                    finish();
                }
            });
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        // Add a marker in saved location came from the previous activity by intent and move the camera.
        if (currentLatLng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
        }
    }

    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected: fired");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "onConnected Permission Denied", Toast.LENGTH_SHORT).show();
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        } else {
            Log.e(TAG, "onConnected: mLocation != null");
            getUserData();

        }

        if (isGpsEnabled()) {
            dismissLocationDialog();
        } else {
            showLocationDialog();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Start Location Updates Permission Denied", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void dismissLocationDialog() {
        if (isDialogVisible) {
            dialog.dismiss();
            getUserData();

        }
    }

    private void showLocationDialog() {
        isDialogVisible = true;
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.location__dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        (dialog.findViewById(R.id.location_dialog_cancel_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                    }
                });

        (dialog.findViewById(R.id.location_dialog_settings_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent locationSettingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(locationSettingsIntent);
                    }
                });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.location_activity_cancel_button:
                cancelButtonClicked();
                break;
            case R.id.location_activity_save_button:
                saveButtonClicked();
                break;
        }// end switch statement
    }// end onClick Method

    private void saveButtonClicked() {
        final Gson gson = new Gson();
        Log.e(TAG, "saveButtonClicked: JSON : " + gson.toJson(posUser));
        LoadingDialog.showDialog(this);

        LatLng location = mMap.getCameraPosition().target;

        final User upDateLocation = new User();
        upDateLocation.setId(posUser.getId());
        upDateLocation.setLatitude(location.latitude);
        upDateLocation.setLongitude(location.longitude);
        myService = AppConstants.SERVICE_UPDATE_POS;

        posUserViewModel.upDatePosLocation(upDateLocation).observe(this, new ApiCompleted() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess() {
                LoadingDialog.dismiss();
                Toast.makeText(PosUserLocationActivity.this, R.string.your_location_updated_successfully, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(PosUserLocationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(PosUserLocationActivity.this, error, Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void cancelButtonClicked() {
        finish();
    }

    private boolean isGpsEnabled() {
        ContentResolver contentResolver = this.getContentResolver();
        int mode = Settings.Secure.getInt(
                contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);
        if (mode != Settings.Secure.LOCATION_MODE_OFF) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        Log.e(TAG, "onLocationChanged: msg : " + msg);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    private void getUserData() {
        PosProfileWrapper posProfile = AppUtils.getProfile(this);

        userViewModel.getUserProfile(posProfile.getUser().getId()).observe(this, new ApiObserver<User>() {
            @Override
            protected void noConnection() {
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(User data) {
                if (data != null) {
                    posUser = data;
                    if (posUser.getLatitude() != null && posUser.getLongitude() != null) {
                        lat = (double) posUser.getLatitude();
                        lng = (double) posUser.getLongitude();
                        if (lat != 0 && lng != 0) {
                            currentLatLng = new LatLng(lat, lng);
                            //  currentLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                        } else {
                            currentLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                        }
                    } else {
                        currentLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                    }
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
            }

            @Override
            protected void onError(String error) {
                Toast.makeText(PosUserLocationActivity.this,error,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(this, settingsDialogModel, this);
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);  //Or ACTION_WIRELESS_SETTINGS
    }

    @Override
    public void onCancelClicked() {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_UPDATE_POS.equals(myService)){
                saveButtonClicked();
            }
        }
    }
}
