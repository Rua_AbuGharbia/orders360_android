package com.telephoenic.orders360.controller.server.model;

/**
 * @author Ihab Alnaqib
 * @since 1.0
 * 
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ProductsPointsModel implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("imagesName")
	private String imagesName;

	@JsonProperty("costInLoyaltyPoints")
	private String costInLoyaltyPoints;

	@JsonProperty("costInRewardPoints")
	private String costInRewardPoints;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImagesName() {
		return imagesName;
	}

	public void setImagesName(String imagesName) {
		this.imagesName = imagesName;
	}

	public String getCostInLoyaltyPoints() {
		return costInLoyaltyPoints;
	}

	public void setCostInLoyaltyPoints(String costInLoyaltyPoints) {
		this.costInLoyaltyPoints = costInLoyaltyPoints;
	}

	public String getCostInRewardPoints() {
		return costInRewardPoints;
	}

	public void setCostInRewardPoints(String costInRewardPoints) {
		this.costInRewardPoints = costInRewardPoints;
	}
}