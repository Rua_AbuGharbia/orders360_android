package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrandFilterViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.BrandLogo_RowBrand_CircleImageView)
    public ImageView brandLogoRowBrandCircleImageView;
    @BindView(R.id.BrandName_RowBrand_TextView)
    public TextView brandNameRowBrandTextView;
    @BindView(R.id.radioButton)
    public RadioButton brandCheck;

    public BrandFilterViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
