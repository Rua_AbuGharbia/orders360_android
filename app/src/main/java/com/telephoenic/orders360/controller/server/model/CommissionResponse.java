package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rua on 02-Nov-17.
 */

public class CommissionResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("deleted")
    private Boolean deleted;

    @JsonProperty("inactive")
    private Boolean inactive;

    @JsonProperty("name")
    private String name;

    @JsonProperty("quantityToValuePromotion")
    private String quantityToValuePromotion;

    @JsonProperty("products")
    private String products;

    @JsonProperty("pos")
    private String pos;

    @JsonProperty("posType")
    private String posType;

    @JsonProperty("promotionQtyToValueTransient")
    private List<CommissionQtyToValueTransient> promotionQtyToValueTransient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantityToValuePromotion() {
        return quantityToValuePromotion;
    }

    public void setQuantityToValuePromotion(String quantityToValuePromotion) {
        this.quantityToValuePromotion = quantityToValuePromotion;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getPosType() {
        return posType;
    }

    public void setPosType(String posType) {
        this.posType = posType;
    }

    public List<CommissionQtyToValueTransient> getPromotionQtyToValueTransient() {
        return promotionQtyToValueTransient;
    }

    public void setPromotionQtyToValueTransient(List<CommissionQtyToValueTransient> promotionQtyToValueTransient) {
        this.promotionQtyToValueTransient = promotionQtyToValueTransient;
    }
}
