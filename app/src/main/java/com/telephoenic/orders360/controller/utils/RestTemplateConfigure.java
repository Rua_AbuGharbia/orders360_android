package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.widget.Toast;

import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.GeneralConfig;
import com.telephoenic.orders360.model.AppPrefs;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class RestTemplateConfigure {

	private static GeneralConfig generalConfig;
	private String acceptedLang;
	
	public RestTemplateConfigure(String acceptedLang) {
		super();
		this.acceptedLang = acceptedLang;
	}

	public static RestTemplate configureRestTemplate(Context context) {
		RestTemplate restTemplate = new RestTemplate();
		getConfig(context);
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(generalConfig
				.getMobileRestAPIConnectTimeout());
		requestFactory.setReadTimeout(generalConfig
				.getMobileRestAPIReadTimeout());
		restTemplate.setRequestFactory(requestFactory);

		restTemplate.getMessageConverters().add(
				new StringHttpMessageConverter(Charset.forName("UTF-8")));
		restTemplate.getMessageConverters().add(
				new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
		restTemplate.getMessageConverters().add(
				new ByteArrayHttpMessageConverter());

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		return restTemplate;
	}

	public static String URIBuilder(String path) {
		return AppConstants.URL_LIVE + path;

	}

	private static void getConfig(Context context) {
		generalConfig = new GeneralConfig();

		try {
			final String UTF8 = "utf8";
			final int BUFFER_SIZE = 8192;
			StringBuilder configString = new StringBuilder();
			String line;

			if (context.getFileStreamPath(AppConstants.CONFIG_FILENAME)
					.exists()) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						context.openFileInput(AppConstants.CONFIG_FILENAME),
						UTF8), BUFFER_SIZE);
				while ((line = br.readLine()) != null) {
					configString.append(line);
				}
				generalConfig = JsonUtils.toJavaObject(configString.toString(),
						GeneralConfig.class);
			}
		} catch (FileNotFoundException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	// To be used later, need to be modified
	public static MultiValueMap<String, Object> buildHeaders(Context context) {
		MultiValueMap<String, Object> headers = new LinkedMultiValueMap<String, Object>();
		headers.add("Accept-Language", AppPrefs.getLanguage(context));
		headers.add("api_key", "s+5kwnduLg+fftQQcriRmg==");
		headers.add("Content-Type", "application/json");
		headers.add("x-auth-token",AppPrefs.getToken(context));
		return headers;
	}

	public static MultiValueMap<String, Object> buildHeadersJson(Context context) {
		MultiValueMap<String, Object> headers = new LinkedMultiValueMap<String, Object>();
		headers.add("Accept-Language", AppPrefs.getLanguage(context));
		headers.add("api_key", "s+5kwnduLg+fftQQcriRmg==");
		headers.add("x-auth-token",AppPrefs.getToken(context));
		//headers.add("source", AppConstants.APP_SOURCE);
		headers.add("Content-Type", "application/json");

		return headers;
	}

	public static MultiValueMap<String, Object> buildHeadersLogin(Context context) {
		String base64Name = AppPrefs.getLoginBase64(context);
		MultiValueMap<String, Object> headers = new LinkedMultiValueMap<String, Object>();
		headers.add("Authorization","Basic "+base64Name);
		//headers.add("Authorization","Basic YWRtaW5AdGVsZXBob2VuaWMuY29tOmFkbWlu");

		return headers;
	}


	public static String getDate() {
		try {
			String date="";
			String dateformat = "EEE, dd MMM yyyy HH:mm:ss z";
			Locale locale = Locale.ENGLISH;

			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(dateformat, locale);
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			//sdf.setLenient(true);
			String baseString = sdf.format(d);

			date = baseString;



			return date;
		} catch (Exception e) {
			return null;
		}
	}

}
