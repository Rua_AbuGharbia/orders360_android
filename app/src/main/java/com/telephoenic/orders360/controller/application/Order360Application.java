package com.telephoenic.orders360.controller.application;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.telephoenic.orders360.controller.exception.CustomExceptionHandler;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;

import io.fabric.sdk.android.Fabric;


public class Order360Application extends Application {
    public static String lang = "ar";
    protected static Order360Application order360Application;
    public static boolean IS_CONNECTED = false;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppUtils.updateLocale(this, AppPrefs.getLanguage(this));
        order360Application = this;
        Fabric.with(this, new CrashlyticsCore(), new Crashlytics());

        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(this));
        }

//        if(TextUtils.isEmpty(AppPrefs.getLanguage(this)))
//            lang = "ar";
//        else
//            lang = AppPrefs.getLanguage(this);
    }

    public static Order360Application getInstance() {
        return order360Application;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        AppUtils.updateLocale(this, AppPrefs.getLanguage(this));
    }
}