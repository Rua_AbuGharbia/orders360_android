package com.telephoenic.orders360.controller.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

public class ScreenDensityUtil {
    public static int getScreenSizeWidth(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        int width = -1;
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        return width;
    }

    public static int getScreenSizeHeight(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        int height = -1;
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        return height;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void setWidthCellSize(View view, int cellSize) {
        view.getLayoutParams().width = cellSize;
        view.requestLayout();
    }

    public static void setWidthHeightCellSize(View view, int cellSize) {
        view.getLayoutParams().width = cellSize;
        view.getLayoutParams().height = cellSize;
        view.requestLayout();
    }
}
