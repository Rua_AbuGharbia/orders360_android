package com.telephoenic.orders360.controller.server.model;

import java.io.Serializable;

public class PromotionValueQuantity extends PromotionValue implements Serializable {

	private Long quantity;
	
	public PromotionValueQuantity() {
		
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}
