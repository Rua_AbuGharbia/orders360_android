package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LanguageManager {

    public static final String ENGLISH = "en";
    public static final String ARABIC = "ar";

    public static void autoFlipLanguage(Context context) {
        if (SharedPreferenceManager.getLanguage(context).equals(ARABIC)) {
            SharedPreferenceManager.setLanguage(context, ENGLISH);
            LanguageUtils.setLanguage(context, ENGLISH, true);
        } else {
            SharedPreferenceManager.setLanguage(context, ARABIC);
            LanguageUtils.setLanguage(context, ARABIC, true);
        }
    }

    public static void setLanguage(Context context) {
        if (SharedPreferenceManager.getLanguage(context) != null) {
            LanguageUtils.setLanguage(context, SharedPreferenceManager.getLanguage(context), false);
        } else {
            SharedPreferenceManager.setLanguage(context, ENGLISH);
            LanguageUtils.setLanguage(context, ENGLISH, false);
        }
    }

    @StringDef({ARABIC, ENGLISH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LanguageType {
    }
}
