package com.telephoenic.orders360.controller.server.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class ProductWrapper implements Serializable  {

	private Integer id;
	private boolean deleted;
	private boolean inactive;
	private String name;
	private String alternativeName;
	private String barcode;
	private Double price;
	private double commission;
	private Integer points;
	private Long loyaltyPoint;
	private String description;
	private String imagesName;
	private CategoryNodeWrapper category;
	private VendorWrapper vendor;
	private Double retailPrice;

	public Double getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(Double oldPrice) {
		this.oldPrice = oldPrice;
	}

	private Double oldPrice;

	private  String count;
	private  String selectedCommission;

	private boolean canDelete ;
	private int hasPromotion ;

	private String startDate;
	private String  endDate;
	private String  expiryDate;
	private Double vat;
	private Double discount;
	private Double priceWithDiscount;

	public Double getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(Double discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	private Double discountedPrice;

	private CommissionQtyToValueTransient commissionQtyToValueTransient;

	private List<PromotionValueQuantity> commissionListQantity;

	private List<PromotionValueAttribute> commissionListAttribute;

	public ProductWrapper() {

	}

//	@Override
//	public int describeContents() {
//		return 0;
//	}
//
//	@Override
//	public void writeToParcel(Parcel parcel, int i) {
//
//	}
//
//	public static final Creator<ProductWrapper> CREATOR = new Creator<ProductWrapper>() {
//		@Override
//		public ProductWrapper createFromParcel(Parcel source) {
//			return new ProductWrapper();
//		}
//
//		@Override
//		public ProductWrapper[] newArray(int size) {
//			return new ProductWrapper[size];
//		}
//	};

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagesName() {
		return imagesName;
	}

	public void setImagesName(String imagesName) {
		this.imagesName = imagesName;
	}

	public CategoryNodeWrapper getCategory() {
		return category;
	}

	public void setCategory(CategoryNodeWrapper category) {
		this.category = category;
	}

	public VendorWrapper getVendor() {
		return vendor;
	}

	public void setVendor(VendorWrapper vendor) {
		this.vendor = vendor;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getSelectedCommission() {
		return selectedCommission;
	}

	public void setSelectedCommission(String selectedCommission) {
		this.selectedCommission = selectedCommission;
	}

	public CommissionQtyToValueTransient getCommissionQtyToValueTransient() {
		return commissionQtyToValueTransient;
	}

	public void setCommissionQtyToValueTransient(CommissionQtyToValueTransient commissionQtyToValueTransient) {
		this.commissionQtyToValueTransient = commissionQtyToValueTransient;
	}

	public boolean isCanDelete() {
		return canDelete;
	}

	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Long getLoyaltyPoint() {
		return loyaltyPoint;
	}

	public void setLoyaltyPoint(Long loyaltyPoint) {
		this.loyaltyPoint = loyaltyPoint;
	}

	public int getHasPromotion() {
		return hasPromotion;
	}

	public void setHasPromotion(int hasPromotion) {
		this.hasPromotion = hasPromotion;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAlternativeName() {
		return alternativeName;
	}

	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Double getVat() {
		return vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	public List<PromotionValueQuantity> getCommissionListQantity() {
		return commissionListQantity;
	}

	public void setCommissionListQantity(List<PromotionValueQuantity> commissionListQantity) {
		this.commissionListQantity = commissionListQantity;
	}

	public List<PromotionValueAttribute> getCommissionListAttribute() {
		return commissionListAttribute;
	}

	public void setCommissionListAttribute(List<PromotionValueAttribute> commissionListAttribute) {
		this.commissionListAttribute = commissionListAttribute;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getPriceWithDiscount() {
		return priceWithDiscount;
	}

	public void setPriceWithDiscount(Double priceWithDiscount) {
		this.priceWithDiscount = priceWithDiscount;
	}

	public Double getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
}
