package com.telephoenic.orders360.controller.server.controller;

import android.content.Context;
import android.os.AsyncTask;

public class GetWebserviceTask extends AsyncTask<String, Void, Object> implements OnResponseReady {
    private Context context;
    private OnResponseReady onResponseReady;
    private Class webserviceClass;
    private String response;

    public GetWebserviceTask(Context context, Class webserviceClass) {
        this.context = context;
        this.webserviceClass = webserviceClass;
        onResponseReady = this;
    }

    public GetWebserviceTask(Context context, Class webserviceClass, OnResponseReady onResponseReady) {
        this(context, webserviceClass);
        this.onResponseReady = onResponseReady;
    }

    @Override
    protected Object doInBackground(String... params) {
        try {
            RestWebService restWebService = (RestWebService) webserviceClass.newInstance();
            System.out.print("restWebService="+restWebService);
            return restWebService.callService(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object response) {
        super.onPostExecute(response);
        onResponseReady.onResponseReady(response);
        if(response instanceof Exception)
            onResponseReady.exceptionHandler((Exception)response);
    }

    @Override
    public void onResponseReady(Object response) {
    }

    @Override
    public void exceptionHandler(Exception exception) {
    }




}