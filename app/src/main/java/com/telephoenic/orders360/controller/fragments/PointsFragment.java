package com.telephoenic.orders360.controller.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.IntentExtraNames;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.PosUserViewModel;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;


public class PointsFragment extends BlankFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {

    // const
    private String TAG = PointsFragment.class.getName();

    // views
    private TextView pointsTextView, posNameTextView;
    private Button redeemPointsButton;

    private PosProfileWrapper posProfileWrapper;
    private List<String> privilegeList;

    private PosUserViewModel posUserViewModel;

    public PointsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        AppUtils.changeTitle(getActivity(), getString(R.string.fragment_rewards_points));

        return inflater.inflate(R.layout.fragment_points, container, false);
    }

    private void createComponents(View v) {
        pointsTextView = v.findViewById(R.id.txt_points);
        posNameTextView = v.findViewById(R.id.text_pos_name);
        redeemPointsButton = v.findViewById(R.id.button_redeem_points);

        prepareAndSendPointsRequest();

        if (posProfileWrapper != null) {
            posNameTextView.setText(posProfileWrapper.getUser().getName());
            privilegeList = posProfileWrapper.getUser().getPermissions();
            for (String item : privilegeList) {
                if (AppConstants.ROLE_POINTS_REDEMPTION.equals(item)) {
                    redeemPointsButton.setVisibility(View.VISIBLE);
                }
            }
        }

        redeemPointsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
               bundle.putLong(IntentExtraNames.POINTS, Long.parseLong(pointsTextView.getText().toString()));
                AppUtils.changeTitle((getActivity()), getContext().getResources().getString(R.string.fragment_rewards_title));
                gotoFragment(new RewardsFragment(), getActivity().getSupportFragmentManager(), bundle);
            }
        });
    }

    private void prepareAndSendPointsRequest() {
        LoadingDialog.showDialog(getActivity());
        posProfileWrapper = AppUtils.getProfile(getContext());
        if (posProfileWrapper != null) {

            posUserViewModel.getAllPoints(posProfileWrapper.getUser().getId().toString(),
                    AppPrefs.getVendorID(getContext()).toString()).observe(this, new ApiObserver<ResponseBody>() {
                @Override
                protected void noConnection() {
                    LoadingDialog.dismiss();
                    noConnectionInternetDialog();
                }

                @Override
                protected void onSuccess(ResponseBody data) {
                    LoadingDialog.dismiss();
                    try {
                        pointsTextView.setText(data.string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                    }
                }

                @Override
                protected void onError(String error) {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), error,Toast.LENGTH_LONG).show();

                }
            });
        }

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        posUserViewModel = ViewModelProviders.of(this).get(PosUserViewModel.class);
        createComponents(view);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            prepareAndSendPointsRequest();
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }
}