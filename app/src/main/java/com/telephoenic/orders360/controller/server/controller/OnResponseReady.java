package com.telephoenic.orders360.controller.server.controller;

public interface OnResponseReady {
    void onResponseReady(Object response);
    void exceptionHandler(Exception exception);
}