
package com.telephoenic.orders360.controller.customcontrols;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;

import butterknife.ButterKnife;

public class NewLoading extends DialogFragment {

    // private static Dialog loading;
    private Dialog dialog;

//    @SuppressLint("ResourceType")
//    public static void showDialog(Context context) {
//        loading = new Dialog(context);
//        loading.setContentView(R.layout.loading_dialog);
//        loading.setCancelable(false);
//        if (loading.getWindow() != null) {
//            loading.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        }
//
//        showDialog();
//    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.loading_dialog);
            dialog.setCancelable(false);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ButterKnife.bind(this, dialog);
        }
        return dialog;
    }

//    public static void dismiss() {
//        if (loading != null && loading.isShowing())
//            loading.dismiss();
//    }
//
//    private static void showDialog() {
//        if (!loading.isShowing() &&loading !=null) {
//            loading.show();
//        }
//    }
}

