package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 02-Nov-17.
 */

public class PointsRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("posId")
    private String posId;

    @JsonProperty("vendorId")
    private String vendorId;

    @JsonProperty("pageNum")
    String pageNum;

    @JsonProperty("pageSize")
    String pageSize;

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
