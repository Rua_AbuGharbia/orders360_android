package com.telephoenic.orders360.controller.server.model;

import com.telephoenic.orders360.basemodel.typedef.SerialType;

import java.io.Serializable;

public class AllSerialModel implements Serializable {
    private String serialNumber;
    private String toNumber;
    private @SerialType
    int serialType;
    private Boolean validation;
    private Boolean validationToSerial;
    private String validationMassage;
    private String validationToSerialMassage;

    public AllSerialModel(String serialNumber, @SerialType int serialType) {
        this.serialType = serialType;
        this.serialNumber = serialNumber;
    }

    public AllSerialModel(String serialNumber, String toNumber, @SerialType int serialType) {
        this.serialType = serialType;
        this.serialNumber = serialNumber;
        this.toNumber = toNumber;
    }

    public AllSerialModel() {
        this.serialNumber = "";
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public @SerialType
    int getSerialType() {
        return serialType;
    }

    public void setSerialType(@SerialType int serialType) {
        this.serialType = serialType;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public Boolean getValidationToSerial() {
        return validationToSerial;
    }

    public void setValidationToSerial(Boolean validationToSerial) {
        this.validationToSerial = validationToSerial;
    }

    public String getValidationMassage() {
        return validationMassage;
    }

    public void setValidationMassage(String validationMassage) {
        this.validationMassage = validationMassage;
    }

    public String getValidationToSerialMassage() {
        return validationToSerialMassage;
    }

    public void setValidationToSerialMassage(String validationToSerialMassage) {
        this.validationToSerialMassage = validationToSerialMassage;
    }
}
