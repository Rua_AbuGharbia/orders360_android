package com.telephoenic.orders360.controller.server.webservices;


import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.controller.RestWebService;

public class PostUpdateEndUserWebService extends RestWebService {
    @Override
    protected Object callService(String... parameters) {

        final String url = AppConstants.URL_LIVE + "campaign/end_user/update";
        Object response = callPostService(url, parameters[0]);

        return response;
    }
}