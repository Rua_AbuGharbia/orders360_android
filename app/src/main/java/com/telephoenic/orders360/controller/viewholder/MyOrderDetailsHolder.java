package com.telephoenic.orders360.controller.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyOrderDetailsHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.OrderName_RowMyOrder_TextView)
    public TextView orderNameRowMyOrderTextView;
    @BindView(R.id.Quantity_RowMyOrder_TextView)
    public TextView quantityRowMyOrderTextView;
    @BindView(R.id.ItemPrice_RowMyOrder_TextView)
    public TextView itemPriceRowMyOrderTextView;
    @BindView(R.id.TotalAmount_RowMyOrder_TextView)
    public TextView totalAmountRowMyOrderTextView;
    @BindView(R.id.Next_RowMyOrder_ImageView)
    public ImageView nextRowMyOrderImageView;
    @BindView(R.id.LinearBackground_RowMyOrder_LinearLayout)
    public LinearLayout linearBackgroundRowMyOrderLinearLayout;
    @BindView(R.id.Line_RowMyOrder_View)
    public View lineRowMyOrderView;
    @BindView(R.id.checkBox)
    public CheckBox checkBox;

    public MyOrderDetailsHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
