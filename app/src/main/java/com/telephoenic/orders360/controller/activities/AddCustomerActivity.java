package com.telephoenic.orders360.controller.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.CustomerModel;
import com.telephoenic.orders360.controller.server.model.CustomerRequest;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.webservices.PostAddEndUserWebService;
import com.telephoenic.orders360.controller.server.webservices.PostUpdateEndUserWebService;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.mvp.MVPView;
import com.telephoenic.orders360.mvp.Presenter;
import com.telephoenic.orders360.mvp.PresenterImpl;

import org.springframework.util.LinkedMultiValueMap;

public class AddCustomerActivity extends AppCompatActivity implements View.OnClickListener , MVPView {
    private String TAG = AddCustomerActivity.class.getName();
    EditText nameEditText , mobileEditText ,emailEditText ,addressEditText ;
    Button saveButton, resetButton ;

    private PosProfileWrapper posProfile ;
    protected ProgressDialog progressDialog;

    private String customerID ;
   // private boolean isCheKed ;
    private String type ;
    private CustomerModel customerModel ;
    private Presenter addCustomerPresenter;
    private CustomerRequest customerRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        AppUtils.initToolbar(getSupportActionBar(),this,getResources().getString(R.string.navigation_drawer_campaign),false);
        AppUtils.getLayoutDirection(this);
        createComponents();
    }

    private void createComponents(){
        nameEditText = findViewById(R.id.etxt_customer_name);
        mobileEditText = findViewById(R.id.etxt_customer_mobile_number);
        emailEditText = findViewById(R.id.etxt_customer_email);
        addressEditText = findViewById(R.id.etxt_customer_addrees);
        saveButton =  findViewById(R.id.button_save);
        resetButton =  findViewById(R.id.button_reset);

        saveButton.setOnClickListener(this);
        resetButton.setOnClickListener(this);

        addCustomerPresenter= new PresenterImpl(this ,this);

        posProfile = AppUtils.getProfile(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.wait_msg));
        progressDialog.setCancelable(false);

        Intent intent = getIntent();
        if(intent!=null){
            type = intent.getStringExtra("type");
            if(AppConstants.ADD_CUSTOMER_TYPE_NEW.equals(type)){
               // customerID = intent.getStringExtra("listSize");
            } else if(AppConstants.ADD_CUSTOMER_TYPE_EDIT.equals(type)) {
                Bundle bundle = intent.getBundleExtra("bundle");
                customerModel = (CustomerModel) bundle.getSerializable("customerModel");
                customerID = customerModel.getId();
                //isCheKed = customerModel.isChecked();
                setCustomerFromBundel();
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_save:
                if(isValidInputs()){
                    addCustomer();
                    sendAddCustomerRequest();
                        //finish();
                }
                break;
            case R.id.button_reset:
                reset();
                break;
        }
    }

    private void addCustomer(){
        customerRequest = new CustomerRequest();
        //CustomerModel customerModel = new CustomerModel();
         if(AppConstants.ADD_CUSTOMER_TYPE_EDIT.equals(type)){
             customerRequest.setId(customerID);
         }
        customerRequest.setName(nameEditText.getText().toString().trim());
        customerRequest.setMobileNumber(mobileEditText.getText().toString().trim());
        customerRequest.setEmail(emailEditText.getText().toString().trim());
       // customerModel.setAddress(addressEditText.getText().toString().trim());
        /*if(AppConstants.ADD_CUSTOMER_TYPE_EDIT.equals(type)){
            customerModel.setChecked(isCheKed);
        }*/

    }

    private void sendAddCustomerRequest(){
        try {
            if(AppConstants.ADD_CUSTOMER_TYPE_NEW.equals(type)){
                addCustomerPresenter.validateCredentials(customerRequest, PostAddEndUserWebService.class);
            } else if(AppConstants.ADD_CUSTOMER_TYPE_EDIT.equals(type)){
                addCustomerPresenter.validateCredentials(customerRequest, PostUpdateEndUserWebService.class);
            }

        } catch (Exception e){

        }
    }

    private boolean isValidInputs(){
        if (TextUtils.isEmpty(nameEditText.getText().toString().trim())) {
            Snackbar.make(findViewById(android.R.id.content), R.string.toast_required_fill_name, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            nameEditText.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mobileEditText.getText().toString().trim())) {
            Snackbar.make(findViewById(android.R.id.content), R.string.toast_required_fill_mobile, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            mobileEditText.requestFocus();
            return false;
        }
        if(mobileEditText.getText().length()<10){
            Snackbar.make(findViewById(android.R.id.content), R.string.required_mobile_number_length, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            mobileEditText.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(emailEditText.getText().toString().trim())) {
            if (!emailEditText.getText().toString().trim().contains("@")) {
                Snackbar.make(findViewById(android.R.id.content), R.string.toast_required_fill_valid_email, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                emailEditText.requestFocus();
                return false;
            }
        }
       /* if (TextUtils.isEmpty(emailEditText.getText().toString().trim())) {
            Snackbar.make(findViewById(android.R.id.content), R.string.toast_required_fill_email, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            emailEditText.requestFocus();
            return false;
        }*/

     /*   if (TextUtils.isEmpty(addressEditText.getText().toString().trim())) {
            Snackbar.make(findViewById(android.R.id.content), R.string.toast_required_fill_address, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            addressEditText.requestFocus();
            return false;
        }*/

        return true;
    }

    private void reset(){
        nameEditText.setText("");
        mobileEditText.setText("");
        emailEditText.setText("");
        addressEditText.setText("");
        nameEditText.requestFocus();
        AppUtils.openKeyBoard();
    }
    private void setCustomerFromBundel(){
        nameEditText.setText(customerModel.getName());
        mobileEditText.setText(customerModel.getMobileNumber());
        emailEditText.setText(customerModel.getEmail());
        //addressEditText.setText(customerModel.getAddress());
        nameEditText.requestFocus();
        AppUtils.openKeyBoard();
    }

    @Override
    public void showProgress() {
       progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void navigate(String response) {
        customerModel = JsonUtils.toJavaObject(response,CustomerModel.class);
        Intent intent = new Intent();
        if(AppConstants.ADD_CUSTOMER_TYPE_NEW.equals(type)){
            intent.putExtra("type",AppConstants.ADD_CUSTOMER_TYPE_NEW);
        } else if(AppConstants.ADD_CUSTOMER_TYPE_EDIT.equals(type)){
            intent.putExtra("type",AppConstants.ADD_CUSTOMER_TYPE_EDIT);
        }
        intent.putExtra("customerModel", customerModel);
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public void navigate(LinkedMultiValueMap response) {

    }
}
