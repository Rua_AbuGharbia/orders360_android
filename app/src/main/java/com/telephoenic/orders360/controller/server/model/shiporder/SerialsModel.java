package com.telephoenic.orders360.controller.server.model.shiporder;

import java.io.Serializable;

public class SerialsModel implements Serializable {
    private Long id;
    private Long shipmentOrderItemId;
    private Integer quantity;
    private String serial;
    private String fromSerial;
    private String toSerial;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShipmentOrderItemId() {
        return shipmentOrderItemId;
    }

    public void setShipmentOrderItemId(Long shipmentOrderItemId) {
        this.shipmentOrderItemId = shipmentOrderItemId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getFromSerial() {
        return fromSerial;
    }

    public void setFromSerial(String fromSerial) {
        this.fromSerial = fromSerial;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }
}
