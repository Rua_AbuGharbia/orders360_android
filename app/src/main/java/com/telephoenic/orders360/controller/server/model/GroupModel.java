package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Ru'a on 7/25/2019.
 */

public class GroupModel implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("deleted")
    private Boolean deleted;

    @JsonProperty("inactive")
    private Boolean inactive;

    @JsonProperty("name")
    private String name;

    @JsonProperty("vendor")
    private VendorWrapper vendor;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

}
