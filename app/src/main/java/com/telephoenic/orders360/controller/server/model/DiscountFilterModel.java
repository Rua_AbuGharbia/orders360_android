package com.telephoenic.orders360.controller.server.model;

public class DiscountFilterModel {
    private boolean select;

    public DiscountFilterModel(boolean select) {
        this.select = select;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
