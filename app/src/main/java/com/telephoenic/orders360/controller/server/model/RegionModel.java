package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
/**
 * Created by Rua on 08-Nov-17.
 */

public class RegionModel implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("deleted")
    private Boolean deleted;

    @JsonProperty("inactive")
    private Boolean inactive;

    @JsonProperty("name")
    private String name;

    @JsonProperty("code")
    private String code;

    @JsonProperty("city")
    private CityModel city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "RegionModel{" +
                "id='" + id + '\'' +
                ", deleted=" + deleted +
                ", inactive=" + inactive +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", city=" + city +
                '}';
    }
}
