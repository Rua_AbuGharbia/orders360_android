package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.view.adapters.ReadyToShipOrderDetailsAdapter;
import com.telephoenic.orders360.view.customcontrols.DialogUtils;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReadyToShipFragment extends BlankFragment implements OnRecyclerClick, DialogUtils.OnDialogClickListener {
    public static final String ORDER_ID = "id";
    private ReadyToShipOrderDetailsAdapter adapter;
    private List<ShipItemsModel> shipItemsModelList;
    @BindView(R.id.RecyclerView_Order)
    RecyclerView recyclerViewOrder;
    @BindView(R.id.pagination_loading_view)
    SpinKitView paginationLoadingView;
    private ShipViewModel shipViewModel;
    private Long id = -1L;
    private boolean isAllShip = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ready_to_ship_fragment, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            id = getArguments().getLong(ORDER_ID);
        }
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        shipItemsModelList = new ArrayList<>();
        adapter = new ReadyToShipOrderDetailsAdapter(this, shipItemsModelList);
        recyclerViewOrder.setAdapter(adapter);
        getShipItem(id);
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), "Ready To Ship");
        createComponents(view);
    }


    @OnClick(R.id.ShipItems_Buttom)
    public void onViewClicked() {
        for (int i = 0; i < shipItemsModelList.size(); i++) {
            if (!shipItemsModelList.get(i).getReadyToShipment()) {
                isAllShip = false;
            }
        }
        if (isAllShip) {
            LoadingDialog.showDialog(getContext());
            shipViewModel.shipItemOrders(id).observe(this, new ApiCompleted() {
                @Override
                protected void noConnection() {
                    LoadingDialog.dismiss();
                    noConnectionInternetDialog();
                }

                @Override
                protected void onSuccess() {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), "Orders Ship Complete", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();

                }

                @Override
                protected void onError(String error) {
                    LoadingDialog.dismiss();
                    Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getContext(), "Please Complete Item Orders", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void getPosition(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ScanItemSerialsFragment.SERIALS, shipItemsModelList.get(position));
        //  bundle.putLong(ScanItemSerialsFragment.ITEM_ID, shipItemsModelList.get(position));
        gotoFragment(new ScanItemSerialsFragment(), getFragmentManager(), bundle);

    }

    private void getShipItem(Long id) {
        paginationLoadingView.setVisibility(View.VISIBLE);
        shipViewModel.getShipItemOrder(id).observe(this, new ApiObserver<List<ShipItemsModel>>() {
            @Override
            protected void noConnection() {
                paginationLoadingView.setVisibility(View.GONE);
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<ShipItemsModel> data) {
                paginationLoadingView.setVisibility(View.GONE);
                shipItemsModelList.addAll(data);
                adapter.notifyDataSetChanged();

            }

            @Override
            protected void onError(String error) {
                paginationLoadingView.setVisibility(View.GONE);
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onOkClicked() {
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));  //Or ACTION_WIRELESS_SETTINGS
    }

    @Override
    public void onCancelClicked() {

    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }

}
