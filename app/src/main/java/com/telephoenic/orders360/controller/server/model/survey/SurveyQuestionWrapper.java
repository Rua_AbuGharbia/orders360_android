package com.telephoenic.orders360.controller.server.model.survey;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;


public class SurveyQuestionWrapper implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("questionType")
    private QuestionType questionType;

    private List<SurveyAnswerWrapper> answerList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<SurveyAnswerWrapper> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<SurveyAnswerWrapper> answerList) {
        this.answerList = answerList;
    }
}
