package com.telephoenic.orders360.controller.server.model;

import java.util.List;

public class PromotionWraper {
    private List<ProductWrapper> content;

    public List<ProductWrapper> getContent() {
        return content;
    }

    public void setContent(List<ProductWrapper> content) {
        this.content = content;
    }
}
