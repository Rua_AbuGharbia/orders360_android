package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 1-Oct-18.
 */

public class SendTicketRequest implements Serializable {

    @JsonProperty("posUser")
    private User posUser;

    @JsonProperty("vendor")
    private VendorWrapper vendor;

    @JsonProperty("pmsg")
    private String pmsg;

    @JsonProperty("subject")
    private String subject;
    private SupportTicketTypeModel supportTicketType;

    public User getPosUser() {
        return posUser;
    }

    public void setPosUser(User posUser) {
        this.posUser = posUser;
    }

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    public String getPmsg() {
        return pmsg;
    }

    public void setPmsg(String pmsg) {
        this.pmsg = pmsg;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public SupportTicketTypeModel getSupportTicketType() {
        return supportTicketType;
    }

    public void setSupportTicketType(SupportTicketTypeModel supportTicketType) {
        this.supportTicketType = supportTicketType;
    }
}
