package com.telephoenic.orders360.controller.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rm.freedrawview.FreeDrawView;
import com.rm.freedrawview.PathDrawnListener;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiCompleted;
import com.telephoenic.orders360.controller.activities.MainActivity;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSuccessfulModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.viewmodel.ShipViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignatureOrderFragment extends BlankFragment {

    public static String ARRIVAL_DATA = "arrivalData";
    @BindView(R.id.SignatureText_TextView)
    TextView signatureTextTextView;
    @BindView(R.id.FreeDrawView)
    FreeDrawView freeDrawView;
    private ArrivalSuccessfulModel arrivalSuccessfulModel;
    private ShipViewModel shipViewModel;
    Bitmap bitmap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signature_order_fragment, container, false);
        ButterKnife.bind(this, view);
        shipViewModel = ViewModelProviders.of(this).get(ShipViewModel.class);
        if (getArguments() != null) {
            arrivalSuccessfulModel = (ArrivalSuccessfulModel) getArguments().getSerializable(ARRIVAL_DATA);
        }
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void createComponents(View v) {
        freeDrawView.setOnPathDrawnListener(new PathDrawnListener() {
            @Override
            public void onNewPathDrawn() {
                freeDrawView.getDrawScreenshot(new FreeDrawView.DrawCreatorListener() {
                    @Override
                    public void onDrawCreated(Bitmap draw) {
                        bitmap = draw;
                    }

                    @Override
                    public void onDrawCreationError() {
                    }
                });
            }

            @Override
            public void onPathStart() {

            }
        });


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            AppUtils.changeTitle(getActivity(), getString(R.string.confirm_delivery));
        createComponents(view);
    }


    @OnClick({R.id.ClearSignature_Buttom, R.id.Save_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ClearSignature_Buttom:
                freeDrawView.clearDraw();
                bitmap = null;
                break;
            case R.id.Save_Buttom:
                if (bitmap != null) {
                    saveArrivalData();
                } else {
                    Toast.makeText(getContext(), getString(R.string.please_singnature), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void saveArrivalData() {
        LoadingDialog.showDialog(getContext());
        shipViewModel.confirmArrivalSuccess(arrivalSuccessfulModel.getShipmentArrival().getShipmentId(), arrivalSuccessfulModel).observe(this, new ApiCompleted() {
            @Override
            protected void noConnection() {
                LoadingDialog.dismiss();
            }

            @Override
            protected void onSuccess() {
                LoadingDialog.dismiss();
                AppUtils.paymentMethodModelList = null;
                AppUtils.arrivalSaveData = null;
                Toast.makeText(getContext(), "Save Successful", Toast.LENGTH_SHORT).show();
                if (getActivity() != null && getContext() != null)
                    getContext().startActivity(new Intent(getActivity(), MainActivity.class));

            }

            @Override
            protected void onError(String error) {
                LoadingDialog.dismiss();
                Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
