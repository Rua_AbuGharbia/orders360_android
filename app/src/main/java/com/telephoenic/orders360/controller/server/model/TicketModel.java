package com.telephoenic.orders360.controller.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Rua on 25-Sep-18.
 */

public class TicketModel implements Serializable {

    @JsonProperty("vendor")
    private VendorWrapper vendor;

    @JsonProperty("ticketStatus")
    private TicketStatus ticketStatus;

    @JsonProperty("subject")
    private String subject;

    @JsonProperty("pmsg")
    private String pmsg;

    @JsonProperty("vmsg")
    private String vmsg;


    public SupportTicketTypeModel getSupportTicketType() {
        return supportTicketType;
    }

    public void setSupportTicketType(SupportTicketTypeModel supportTicketType) {
        this.supportTicketType = supportTicketType;
    }

    private SupportTicketTypeModel supportTicketType;


    public Audit getAudit() {
        if(audit == null ) {
            audit = new Audit();
        }
        return audit;
    }

    public void setAudit(Audit audit) {
        this.audit = audit;
    }

    private Audit audit;

    public VendorWrapper getVendor() {
        return vendor;
    }

    public void setVendor(VendorWrapper vendor) {
        this.vendor = vendor;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPmsg() {
        return pmsg;
    }

    public void setPmsg(String pmsg) {
        this.pmsg = pmsg;
    }

    public String getVmsg() {
        return vmsg;
    }

    public void setVmsg(String vmsg) {
        this.vmsg = vmsg;
    }
}
