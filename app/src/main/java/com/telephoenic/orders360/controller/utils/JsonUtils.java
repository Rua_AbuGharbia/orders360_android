package com.telephoenic.orders360.controller.utils;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.telephoenic.orders360.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class JsonUtils {

	public static final String TAG = JsonUtils.class.getSimpleName();

	/**
	 * From Json object to Java object
	 * 
	 * @param jsonString
	 * @param type
	 * @return
	 */
	public static <T> T toJavaObject(String jsonString, Class<T> type)
			throws JsonSyntaxException {
		Gson gson = new Gson();
		T object = gson.fromJson(jsonString, type);
		return object;

	}

	/**
	 * From Java object to String
	 * 
	 * @param T
	 *            javaObject
	 * @return String jsonObject
	 */
	public static <T> String toJsonString(T javaObject) {
		Gson gson = new Gson();
		String jsonObject = gson.toJson(javaObject);

		return jsonObject;
	}

	/**
	 * From Java object to JsonObject
	 * 
	 * @param <T>
	 * 
	 * @param T
	 *            javaObject
	 * @return JsonObject jsonObject
	 */
	public static <T> JsonObject toJsonObject(T javaObject) {
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		String jsonObject = gson.toJson(javaObject);
		JsonObject o = (JsonObject) parser.parse(jsonObject);
		return o;
	}

	/**
	 * Get Json element as Json array
	 * 
	 * @param String
	 *            jsonString
	 * @param Class
	 *            <T> type
	 * @return List<T> objectsList
	 */
	public static <T> List<T> getAsJsonArray(String jsonString, Class<T> type)
			throws JsonSyntaxException {
		JsonElement jsonElement = new JsonParser().parse(jsonString);
		JsonArray jsonArray = jsonElement.getAsJsonArray();

		Iterator<JsonElement> iterator = jsonArray.iterator();
		List<T> objectsList = new ArrayList<T>();
		while (iterator.hasNext()) {
			JsonElement json = iterator.next();
			Gson gson = new Gson();
			T object = gson.fromJson(json, type);
			objectsList.add(object);
		}
		return objectsList;
	}

	/**
	 * Get Json element by array name
	 * 
	 * @param String
	 *            jsonString
	 * @param Class
	 *            <T> type
	 * @param String
	 *            arrName
	 * @return List<T> objectsList
	 */
	public static <T> List<T> getAsJsonArray(String jsonString, Class<T> type,
											 String arrName) throws JsonSyntaxException {

		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject) parser.parse(jsonString);

		JsonArray jsonArray = obj.get(arrName).getAsJsonArray();

		Iterator<JsonElement> iterator = jsonArray.iterator();
		List<T> objectsList = new ArrayList<T>();
		while (iterator.hasNext()) {
			JsonElement json = iterator.next();
			Gson gson = new Gson();
			T object = gson.fromJson(json, type);
			objectsList.add(object);
		}
		return objectsList;
	}

	/**
	 * Read json string from file
	 * 
	 * @param fileName
	 *            : The stored json file to be read
	 * @param context
	 *            : The calling Activity
	 * @param type
	 *            : The class type of the stored json object, i.e
	 *            PosProfile.class
	 * @return Returns the stored object in the file of the selected type.
	 *         Returns Null in case I/O error is occurred
	 */
	public static <T> T readFile(String fileName, Context context, Class<T> type) {
		T object = null;
		final String UTF8 = "utf8";
		final int BUFFER_SIZE = 8192;
		StringBuilder userProfileString = new StringBuilder();
		String line;
		BufferedReader bufferedReader = null;
		try {
			if (context.getFileStreamPath(fileName).exists()) {
				bufferedReader = new BufferedReader(new InputStreamReader(
						context.openFileInput(fileName), UTF8), BUFFER_SIZE);
				while ((line = bufferedReader.readLine()) != null) {
					userProfileString.append(line);
				}
				object = JsonUtils.toJavaObject(userProfileString.toString(),
						type);
			} else {
				Toast.makeText(context, "File is not found", Toast.LENGTH_LONG)
						.show();
				CustomLog.w(TAG, "File is not found");
			}
		} catch (FileNotFoundException e) {
			logError(e, context, "readFile() - reading ");
		} catch (IOException e) {
			logError(e, context, "readFile() - reading ");
		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
			} catch (IOException e) {
				logError(e, context, "readFile() - reading ");
			}
		}
		return object;
	}

	/**
	 * Store json string in a file
	 */
	public static void storeJsonStringFile(Context context, String fileName,
										   String jsonString) {
		FileOutputStream json_file;
		try {
			File f = context.getFileStreamPath(fileName);
			if (f.exists()) {
				f.delete();
			}
			json_file = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			json_file.write(jsonString.getBytes());
			json_file.close();

		} catch (FileNotFoundException e) {
			logError(e, context, "storeJsonStringFile() - storing ");
		} catch (IOException e) {
			logError(e, context, "storeJsonStringFile() - storing ");
		}
	}

	private static void logError(Exception e, Context context, String methodName) {
		CustomLog.e(TAG, "Error - " + methodName + "JSON file failed.", e);
		Toast.makeText(context,
				context.getString(R.string.unknown_error),
				Toast.LENGTH_LONG).show();
	}
}
