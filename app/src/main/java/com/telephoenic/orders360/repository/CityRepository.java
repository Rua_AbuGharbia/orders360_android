package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.dao.city.CityDao;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class CityRepository extends BaseRepository {

    private CityDao cityDao;
    private SingleLiveEvent<ResponseWrapper<List<CityModel>>> cityLiveData;

    public CityRepository() {
        super();
        cityDao = new CityDao();
        cityLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<CityModel>>> getAllCities(Long countryId) {
        final ResponseWrapper<List<CityModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(cityDao.getAllCities(countryId).subscribeWith(new SingleObserver<List<CityModel>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                cityLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<CityModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                cityLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                cityLiveData.setValue(responseWrapper);
            }
        }));
        return cityLiveData;
    }
}
