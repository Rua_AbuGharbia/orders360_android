package com.telephoenic.orders360.repository;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.BrandModel;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.dao.category.CategoryDao;

import java.util.List;

public class CategoryRepository extends BaseRepository {
    private CategoryDao categoryDao;
    private SingleLiveEvent<ResponseWrapper<List<CategoryNodeWrapper>>> categoryLiveData;

    public CategoryRepository() {
        super();
        categoryDao = new CategoryDao();
        categoryLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<CategoryNodeWrapper>>> getCategory(Long vendorId, final Fragment fragment, final Context context) {
        final ResponseWrapper<List<CategoryNodeWrapper>> responseWrapper = new ResponseWrapper<>();
        //  LoadingDialog.showDialog(fragment.getContext());
        disposable.add(
                categoryDao.getCategory(vendorId).subscribeWith(new SingleObserver<List<CategoryNodeWrapper>>() {
                    @Override
                    protected void noConnection() {
                        //   LoadingDialog.dismiss();
//                        ShowNoConnectionInternet(context, fragment);
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        categoryLiveData.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(List<CategoryNodeWrapper> response) {
                        // LoadingDialog.dismiss();
                        responseWrapper.setContent(response);
                        responseWrapper.setHasError(false);
                        categoryLiveData.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {
                        //   LoadingDialog.dismiss();
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        categoryLiveData.setValue(responseWrapper);
                    }
                })

        );

        return categoryLiveData;
    }
}
