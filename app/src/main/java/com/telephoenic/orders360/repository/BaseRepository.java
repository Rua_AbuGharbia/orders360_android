package com.telephoenic.orders360.repository;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.dao.login.LoginDao;
import com.telephoenic.orders360.view.customcontrols.NoInternetConnectionDialog;

import org.springframework.http.HttpStatus;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class BaseRepository {
    CompositeDisposable disposable;
    private LoginDao loginDao;
    private ResponseWrapper<PosProfileWrapper> loginWrapper;

    public BaseRepository() {
        disposable = new CompositeDisposable();
        loginDao = new LoginDao();
        loginWrapper = new ResponseWrapper<>();
    }

    public void unsubscribe() {
        if (disposable != null) {
            disposable.dispose();
            disposable.clear();
        }
    }


    public ResponseWrapper<PosProfileWrapper> getNewToken(String base64Name) {
        final ResponseWrapper<PosProfileWrapper> responseWrapper = new ResponseWrapper<>();

        disposable.add(loginDao.login("Basic " + base64Name).subscribeWith(new SingleObserver<PosProfileWrapper>() {


            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                loginWrapper = responseWrapper;
            }

            @Override
            protected void onResponse(PosProfileWrapper response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                loginWrapper = responseWrapper;
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                loginWrapper = responseWrapper;
            }
        }));

        return loginWrapper;
    }


    protected void ShowNoConnectionInternet(Context context, Fragment fragment) {
        final NoInternetConnectionDialog connectionDialog = new NoInternetConnectionDialog(context, fragment);
        connectionDialog.show();
    }

    protected void ShowNoConnectionInternet(Context context, Activity activity) {
        final NoInternetConnectionDialog connectionDialog = new NoInternetConnectionDialog(context, activity);
        connectionDialog.show();
    }


}
