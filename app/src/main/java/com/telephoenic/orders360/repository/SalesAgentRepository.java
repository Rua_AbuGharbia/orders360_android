package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.dao.salesAgent.SalesAgentDao;

/**
 * Created by Ru'a on 12/8/2019.
 */

public class SalesAgentRepository extends BaseRepository {
    private SalesAgentDao vendorDao ;
    private SingleLiveEvent<ResponseWrapper<User>> userLiveData;

    public SalesAgentRepository() {
        super();
        vendorDao = new SalesAgentDao();
        userLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<User>> getVendor(Long userId){
        final ResponseWrapper<User> responseWrapper = new ResponseWrapper<>();
        disposable.add(vendorDao.getVendor(userId).subscribeWith(new SingleObserver<User>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(User response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                userLiveData.setValue(responseWrapper);
            }
        }));
        return userLiveData;
    }

}
