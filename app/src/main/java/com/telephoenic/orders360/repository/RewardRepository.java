package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.dao.reward.RewardDao;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/3/2019.
 */

public class RewardRepository extends BaseRepository {

    private RewardDao pointDao;

    private SingleLiveEvent<ResponseWrapper<ResponseBody>> redeemLiveData;

    public RewardRepository() {
        super();
        pointDao = new RewardDao();
        redeemLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> redeemPoints(String posId, String productId, String vendorId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();

        disposable.add(pointDao.redeemPoints(posId, productId, vendorId).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                redeemLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                redeemLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                redeemLiveData.setValue(responseWrapper);
            }
        }));

        return redeemLiveData;
    }
}
