package com.telephoenic.orders360.repository;

import android.annotation.SuppressLint;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.dao.cart.CartDao;
import com.telephoenic.orders360.model.AppPrefs;

import java.util.List;

import okhttp3.ResponseBody;

public class CartRepository extends BaseRepository {
    private CartDao cartDao;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> addToCartWrapper;

    public CartRepository() {
        super();
        cartDao = new CartDao();
        addToCartWrapper = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> addToCart(final List<AddToCartModel> addToCartModel) {
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(cartDao.addToCart(addToCartModel).subscribeWith(new SingleObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                addToCartWrapper.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                addToCartWrapper.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                addToCartWrapper.setValue(responseWrapper);

            }
        }));
        return addToCartWrapper;
    }
}