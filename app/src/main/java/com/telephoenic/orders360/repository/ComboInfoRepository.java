package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.dao.comboInfo.ComboInfoDao;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class ComboInfoRepository extends BaseRepository {
    private ComboInfoDao comboInfoDao;
    private SingleLiveEvent<ResponseWrapper<List<ComboInfoWrapper>>> getBundleInfo;

    public ComboInfoRepository() {
        super();
        comboInfoDao = new ComboInfoDao();
        getBundleInfo = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<ComboInfoWrapper>>> getBundleInfo (String comboID){
        final ResponseWrapper<List <ComboInfoWrapper>> responseWrapper = new ResponseWrapper<>();
        disposable.add(comboInfoDao.getBundleInfo(comboID).subscribeWith(new SingleObserver<List<ComboInfoWrapper>>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                getBundleInfo.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<ComboInfoWrapper> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                getBundleInfo.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                getBundleInfo.setValue(responseWrapper);
            }
        }));

        return getBundleInfo;
    }
}
