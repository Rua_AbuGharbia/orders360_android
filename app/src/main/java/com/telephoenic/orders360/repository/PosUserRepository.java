package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.MaybeObserver;
import com.telephoenic.orders360.basemodel.data.network.OnCompletedObserver;
import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.PosGroupModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.dao.pos.PosUserDao;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/9/2019.
 */

public class PosUserRepository extends BaseRepository {

    private PosUserDao posUserDao;
    private SingleLiveEvent<ResponseWrapper<User>> userLiveData;
    private SingleLiveEvent<ResponseWrapper> sendPosLiveData;
    private SingleLiveEvent<ResponseWrapper> updateLocationLiveData;
    private SingleLiveEvent<ResponseWrapper> updatePosLiveData;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> updateWrapper;
    private SingleLiveEvent<ResponseWrapper<PosGroupModel>> posGroupLiveData;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> pointLiveData ;

    public PosUserRepository() {
        super();
        posUserDao = new PosUserDao();
        userLiveData = new SingleLiveEvent<>();
        sendPosLiveData = new SingleLiveEvent<>();
        updateLocationLiveData = new SingleLiveEvent<>();
        updatePosLiveData = new SingleLiveEvent<>();
        updateWrapper = new SingleLiveEvent<>();
        posGroupLiveData = new SingleLiveEvent<>();
        pointLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<User>> getPosUserByMobile(String mobileNumber, long userId) {
        final ResponseWrapper<User> responseWrapper = new ResponseWrapper<>();
        disposable.add(posUserDao.getPosUserByMobile(mobileNumber, userId).subscribeWith(new MaybeObserver<User>() {
            @Override
            public void onComplete() {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(null);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(User response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                userLiveData.setValue(responseWrapper);

            }
        }));
        return userLiveData;
    }

    public SingleLiveEvent<ResponseWrapper> sendPosUser(long userId, long vendorId, User user) {
        final ResponseWrapper responseWrapper = new ResponseWrapper();
        disposable.add(
                posUserDao.sendPosUser(userId, vendorId, user).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        sendPosLiveData.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        sendPosLiveData.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        sendPosLiveData.setValue(responseWrapper);
                    }
                })
        );
        return sendPosLiveData;
    }

    public SingleLiveEvent<ResponseWrapper> upDatePosLocation(User user){
        final ResponseWrapper responseWrapper = new ResponseWrapper();
        disposable.add(posUserDao.upDatePosLocation(user).subscribeWith(new OnCompletedObserver<ResponseWrapper>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                updateLocationLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse() {
                responseWrapper.setHasError(false);
                updateLocationLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                updateLocationLiveData.setValue(responseWrapper);
            }
        }));
        return updateLocationLiveData;
    }

    public SingleLiveEvent<ResponseWrapper> updateUserProfile(User user){
        final ResponseWrapper responseWrapper = new ResponseWrapper();
        disposable.add(posUserDao.updateUserProfile(user).subscribeWith(new OnCompletedObserver<ResponseWrapper>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                updatePosLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse() {
                responseWrapper.setHasError(false);
                updatePosLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                updatePosLiveData.setValue(responseWrapper);
            }
        }));
        return updatePosLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> updateRegistrationID (final UpdateGPSRequest updateGPSRequest){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();

        disposable.add(posUserDao.updateRegistrationID(updateGPSRequest).subscribeWith(new SingleObserver<ResponseBody>(){

            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                updateWrapper.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                updateWrapper.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                updateWrapper.setValue(responseWrapper);
            }
        }));

        return updateWrapper;
    }

    public SingleLiveEvent<ResponseWrapper<PosGroupModel>> getPosGroupId(Long posId, Long vendorID) {
        final ResponseWrapper<PosGroupModel> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                posUserDao.getPosGroupId(posId, vendorID).subscribeWith(new SingleObserver<PosGroupModel>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        posGroupLiveData.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(PosGroupModel response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        posGroupLiveData.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        posGroupLiveData.setValue(responseWrapper);

                    }
                })
        );
        return posGroupLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getAllPoints(String posId, String vendorId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(posUserDao.getAllPoints(posId, vendorId).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                pointLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                pointLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                pointLiveData.setValue(responseWrapper);
            }
        }));

        return pointLiveData;
    }

}
