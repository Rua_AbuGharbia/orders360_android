package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.survey.SurveyDetailsResponse;
import com.telephoenic.orders360.controller.server.model.survey.SurveyQuestionWrapper;
import com.telephoenic.orders360.dao.survey.SurveyDao;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/5/2019.
 */

public class SurveyRepository extends BaseRepository {
    private SurveyDao surveyDao;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> surveyLiveData;
    private SingleLiveEvent<ResponseWrapper<SurveyDetailsResponse>> surveyDetailsLiveData;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> submitSurveyLiveData;

    public SurveyRepository() {
        super();
        surveyDao = new SurveyDao();
        surveyLiveData = new SingleLiveEvent<>();
        surveyDetailsLiveData = new SingleLiveEvent<>();
        submitSurveyLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getAllSurvey(String pageNumber, String pageSize, String vendorId, String posUserId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(surveyDao.getAllSurveys(pageNumber, pageSize, vendorId, posUserId).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                surveyLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                surveyLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                surveyLiveData.setValue(responseWrapper);
            }
        }));
        return surveyLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<SurveyDetailsResponse>> getSurveyDetails(Long surveyId ,Long participantId){
        final ResponseWrapper<SurveyDetailsResponse> responseWrapper = new ResponseWrapper<>();
        disposable.add(surveyDao.getSurveyDetails(surveyId, participantId).subscribeWith(new SingleObserver<SurveyDetailsResponse>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                surveyDetailsLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(SurveyDetailsResponse response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                surveyDetailsLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                surveyDetailsLiveData.setValue(responseWrapper);
            }
        }));

        return surveyDetailsLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> submitSurvey(Long surveyId, Long participantId, List<SurveyQuestionWrapper> questionsWithAnswers){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(surveyDao.submitSurvey(surveyId, participantId, questionsWithAnswers).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                submitSurveyLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                submitSurveyLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                submitSurveyLiveData.setValue(responseWrapper);
            }
        }));

        return submitSurveyLiveData;
    }
}
