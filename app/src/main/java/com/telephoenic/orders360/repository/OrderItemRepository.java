package com.telephoenic.orders360.repository;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.dao.orderItem.OrderItemDao;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class OrderItemRepository extends BaseRepository {

    private OrderItemDao orderItemDao ;
    private SingleLiveEvent<ResponseWrapper<OrderItemModel>> responseMyOrderHistory;

    public OrderItemRepository() {
        super();
        orderItemDao = new OrderItemDao();
        responseMyOrderHistory = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<OrderItemModel>> getMyOrder(String itemID, final Context context, final Fragment fragment) {
        final ResponseWrapper<OrderItemModel> responseWrapper = new ResponseWrapper<>();
//        LoadingDialog.showDialog(fragment.getContext());
        disposable.add(

                orderItemDao.getMyOrderItem(itemID).subscribeWith(new SingleObserver<OrderItemModel>() {
                    @Override
                    protected void noConnection() {
//                        LoadingDialog.dismiss();
//                        ShowNoConnectionInternet(context, fragment);
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        responseMyOrderHistory.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(OrderItemModel response) {
//                        LoadingDialog.dismiss();
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        responseMyOrderHistory.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {
//                        LoadingDialog.dismiss();
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        responseMyOrderHistory.setValue(responseWrapper);

                    }
                })
        );
        return responseMyOrderHistory;
    }
}
