package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.BundleWraper;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;
import com.telephoenic.orders360.dao.product.ProductDao;

import java.util.List;

import okhttp3.ResponseBody;

public class ProductRepository extends BaseRepository {
    private ProductDao productDao;
    private SingleLiveEvent<ResponseWrapper<List<ProductWrapper>>> getAllProduct;

    private SingleLiveEvent<ResponseWrapper<ResponseBody>> productsPointsLiveData;

    public ProductRepository() {
        super();
        productDao = new ProductDao();
        getAllProduct = new SingleLiveEvent<>();
        productsPointsLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<ProductWrapper>>> getGetAllProduct(int pageNumber, int pageIndex
            , long vendorId, long posId,long groupId, String search, ProductFilter productFilter) {
        final ResponseWrapper<List<ProductWrapper>> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                productDao.getAllProduct(pageNumber, pageIndex, vendorId, posId,groupId, search,productFilter).subscribeWith(new SingleObserver<List<ProductWrapper>>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        getAllProduct.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(List<ProductWrapper> response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        getAllProduct.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        getAllProduct.setValue(responseWrapper);
                    }
                })
        );
        return getAllProduct;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getProductWithPoints(String pageNum, String pageSize, String vendorId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();

        disposable.add(productDao.getProductWithPoints(pageNum,pageSize,
                vendorId).subscribeWith(new  SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                productsPointsLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                productsPointsLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                productsPointsLiveData.setValue(responseWrapper);
            }
        }));

        return productsPointsLiveData;
    }

}
