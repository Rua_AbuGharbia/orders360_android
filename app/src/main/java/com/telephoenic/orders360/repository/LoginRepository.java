package com.telephoenic.orders360.repository;

import android.content.Context;
import android.util.Log;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.UpdateGPSRequest;
import com.telephoenic.orders360.dao.login.LoginDao;

import org.springframework.http.HttpStatus;

import okhttp3.ResponseBody;


/**
 * Created by Ru'a on 11/26/2019.
 */

public class LoginRepository extends BaseRepository {

    private LoginDao loginDao ;
    private SingleLiveEvent<ResponseWrapper<PosProfileWrapper>> loginWrapper;

    public LoginRepository() {
        super();
        loginDao = new LoginDao();
        loginWrapper = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<PosProfileWrapper>> login(final Context context ,String base64Name) {
        final ResponseWrapper<PosProfileWrapper> responseWrapper = new ResponseWrapper<>();

        disposable.add(loginDao.login("Basic "+base64Name).subscribeWith(new SingleObserver<PosProfileWrapper>(){

            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                loginWrapper.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(PosProfileWrapper response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                loginWrapper.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                if(String.valueOf(HttpStatus.UNAUTHORIZED.value()).equals(error)){
                    responseWrapper.setErrorMessage(context.getString(R.string.error_user_password));
                } else {
                    responseWrapper.setErrorMessage(error);
                }
                loginWrapper.setValue(responseWrapper);
            }
        }));

        return loginWrapper;
    }
}
