package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.dao.posGroup.PosGroupDao;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class PosGroupRepository extends BaseRepository {
    private PosGroupDao posGroupDao ;
    private SingleLiveEvent<ResponseWrapper<List<GroupModel>>> groupsLiveData;


    public PosGroupRepository() {
        posGroupDao = new PosGroupDao();
        groupsLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<GroupModel>>> getAllGroups(Long vendorId) {
        final ResponseWrapper<List<GroupModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(posGroupDao.getAllGroups(vendorId).subscribeWith(new SingleObserver<List<GroupModel>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                groupsLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<GroupModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                groupsLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                groupsLiveData.setValue(responseWrapper);
            }
        }));
        return groupsLiveData;
    }
}
