package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.dao.region.RegionDao;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class RegionRepository extends BaseRepository {
    private RegionDao regionDao ;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> regionLiveData;

    public RegionRepository() {
        super();
        regionDao = new RegionDao();
        regionLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getAllRegionsAtSelectedCity(RegionModel regionModel) {
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(regionDao.getAllRegionsAtSelectedCity(regionModel).subscribeWith(new SingleObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                regionLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                regionLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                regionLiveData.setValue(responseWrapper);
            }
        }));
        return regionLiveData;
    }
}
