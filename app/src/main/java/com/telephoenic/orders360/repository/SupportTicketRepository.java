package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.SendTicketRequest;
import com.telephoenic.orders360.controller.server.model.SupportTicketWrapper;
import com.telephoenic.orders360.controller.server.model.TicketModel;
import com.telephoenic.orders360.dao.supportticket.SupportTicketDao;

import java.util.List;

import okhttp3.ResponseBody;

public class SupportTicketRepository extends BaseRepository {
    private SupportTicketDao supportTicketDao;
    private SingleLiveEvent<ResponseWrapper<SupportTicketWrapper>> singleLiveEvent;
    private SingleLiveEvent<ResponseWrapper<List<TicketModel>>> ticketLiveData;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> saveLiveData;

    public SupportTicketRepository() {
        super();
        supportTicketDao = new SupportTicketDao();
        singleLiveEvent = new SingleLiveEvent<>();
        ticketLiveData = new SingleLiveEvent<>();
        saveLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<SupportTicketWrapper>> getType(int pageNumber, int pageSize, Long vendorId) {
        final ResponseWrapper<SupportTicketWrapper> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                supportTicketDao.getType(pageNumber, pageSize, vendorId).subscribeWith(new SingleObserver<SupportTicketWrapper>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        singleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(SupportTicketWrapper response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        singleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        singleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return singleLiveEvent;
    }

    public SingleLiveEvent<ResponseWrapper<List<TicketModel>>> getAllTicket(String posId , String vendorId){
        final ResponseWrapper<List<TicketModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(supportTicketDao.getAllTicket(posId, vendorId).subscribeWith(new SingleObserver<List<TicketModel>>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                ticketLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<TicketModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                ticketLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                ticketLiveData.setValue(responseWrapper);
            }
        }));
        return ticketLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> saveTicket(SendTicketRequest sendTicketRequest){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(supportTicketDao.saveTicket(sendTicketRequest).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                saveLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                saveLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                saveLiveData.setValue(responseWrapper);
            }
        }));

        return saveLiveData;
    }
}
