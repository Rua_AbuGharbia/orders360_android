package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.WarehouseModel;
import com.telephoenic.orders360.dao.deliveryuser.DeliveryUserDao;

import java.util.List;

public class DeliveryUserRepository extends BaseRepository {
    private DeliveryUserDao vendorDao;
    private SingleLiveEvent<ResponseWrapper<User>> userLiveData;
    private SingleLiveEvent<ResponseWrapper<List<WarehouseModel>>> warehouseModelLiveData;
    private SingleLiveEvent<ResponseWrapper<List<CityModel>>> cityLiveData;
    private SingleLiveEvent<ResponseWrapper<List<RegionModel>>> regionLiveData;

    public DeliveryUserRepository() {
        super();
        vendorDao = new DeliveryUserDao();
        userLiveData = new SingleLiveEvent<>();
        warehouseModelLiveData = new SingleLiveEvent<>();
        cityLiveData = new SingleLiveEvent<>();
        regionLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<User>> getVendor(Long userId) {
        final ResponseWrapper<User> responseWrapper = new ResponseWrapper<>();
        disposable.add(vendorDao.getVendor(userId).subscribeWith(new SingleObserver<User>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(User response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                userLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                userLiveData.setValue(responseWrapper);
            }
        }));
        return userLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<List<WarehouseModel>>> getWarehouse(Long userId) {
        final ResponseWrapper<List<WarehouseModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(vendorDao.getWarehouse(userId).subscribeWith(new SingleObserver<List<WarehouseModel>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                warehouseModelLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<WarehouseModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                warehouseModelLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                warehouseModelLiveData.setValue(responseWrapper);
            }
        }));
        return warehouseModelLiveData;
    }


    public SingleLiveEvent<ResponseWrapper<List<CityModel>>> getCity() {
        final ResponseWrapper<List<CityModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(vendorDao.getCity().subscribeWith(new SingleObserver<List<CityModel>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                cityLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<CityModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                cityLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                cityLiveData.setValue(responseWrapper);
            }
        }));
        return cityLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<List<RegionModel>>> getRegion(Long cityId) {
        final ResponseWrapper<List<RegionModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(vendorDao.getRegion(cityId).subscribeWith(new SingleObserver<List<RegionModel>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                regionLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<RegionModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                regionLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                regionLiveData.setValue(responseWrapper);
            }
        }));
        return regionLiveData;
    }

}
