package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.dao.order.OrderDao;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public class OrderRepository extends BaseRepository {
    private OrderDao myOrderDao;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> posOrderLiveData;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> salesOrderLiveData;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> saveOrderLiveData;

    public OrderRepository() {
        super();
        myOrderDao = new OrderDao();
        posOrderLiveData = new SingleLiveEvent<>();
        salesOrderLiveData = new SingleLiveEvent<>();
        saveOrderLiveData = new SingleLiveEvent<>();
    }


    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getPOSOrder(String page, String pageSize, String posId, String vendorId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(myOrderDao.getPOSOrder(page, pageSize, posId, vendorId).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                posOrderLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                posOrderLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                posOrderLiveData.setValue(responseWrapper);
            }
        }));

        return posOrderLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getSalesOrder(int page, int pageSize, Long salesAgentId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(myOrderDao.getSalesOrder(page, pageSize, salesAgentId).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                salesOrderLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                salesOrderLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                salesOrderLiveData.setValue(responseWrapper);
            }
        }));
        return salesOrderLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> saveOrder(List<OrderModel> orderModelsRequest){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();

        disposable.add(myOrderDao.saveOrder(orderModelsRequest).subscribeWith(new SingleObserver<ResponseBody>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                saveOrderLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                saveOrderLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                saveOrderLiveData.setValue(responseWrapper);
            }
        }));

        return saveOrderLiveData;
    }

}
