package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.OnCompletedObserver;
import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.CancelOrderModel;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.dao.orderHistory.OrderHistoryDao;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class OrderHistoryRepository extends BaseRepository {
    private OrderHistoryDao orderHistoryDao;
    private SingleLiveEvent<ResponseWrapper<List<OrderHistoryModel>>> orderHistoryLiveData;
    private SingleLiveEvent<ResponseWrapper<List<OrderRejectedReason>>> orderReasonLiveData;
    private SingleLiveEvent<ResponseWrapper> orderCancelLiveData;
    private SingleLiveEvent<ResponseWrapper<List<ShipModel>>> orderShipLiveData;

    public OrderHistoryRepository() {
        super();
        orderHistoryDao = new OrderHistoryDao();
        orderHistoryLiveData = new SingleLiveEvent<>();
        orderReasonLiveData = new SingleLiveEvent<>();
        orderCancelLiveData = new SingleLiveEvent<>();
        orderShipLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<OrderHistoryModel>>> getOrderHistory(int id) {
        final ResponseWrapper<List<OrderHistoryModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(orderHistoryDao.getOrderHistory(id).subscribeWith(new SingleObserver<List<OrderHistoryModel>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                orderHistoryLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(List<OrderHistoryModel> response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                orderHistoryLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                orderHistoryLiveData.setValue(responseWrapper);
            }
        }));
        return orderHistoryLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<List<OrderRejectedReason>>> getReason(Long vendorId) {
        final ResponseWrapper<List<OrderRejectedReason>> responseWrapper = new ResponseWrapper<>();
        disposable.add(orderHistoryDao.getReason(vendorId).subscribeWith(new SingleObserver<List<OrderRejectedReason>>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                orderReasonLiveData.setValue(responseWrapper);

            }

            @Override
            protected void onResponse(List<OrderRejectedReason> response) {

                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                orderReasonLiveData.setValue(responseWrapper);

            }

            @Override
            protected void onFailure(String error) {

                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                orderReasonLiveData.setValue(responseWrapper);

            }
        }));
        return orderReasonLiveData;
    }

    public SingleLiveEvent<ResponseWrapper> cancelOrder(CancelOrderModel cancelOrderModel) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(orderHistoryDao.cancelOrder(cancelOrderModel).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        orderCancelLiveData.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        orderCancelLiveData.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        orderCancelLiveData.setValue(responseWrapper);
                    }
                })
        );
        return orderCancelLiveData;
    }
    public SingleLiveEvent<ResponseWrapper<List<ShipModel>>> getOrderShip(int pageIndex, int pageSize, Long orderId){
        final ResponseWrapper<List<ShipModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                orderHistoryDao.getShipOrders(pageIndex,pageSize,orderId).subscribeWith(new SingleObserver<List<ShipModel>>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        orderShipLiveData.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse(List<ShipModel> response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        orderShipLiveData.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        orderShipLiveData.setValue(responseWrapper);

                    }
                })
        );
        return orderShipLiveData;
    }
}
