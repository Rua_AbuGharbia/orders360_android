package com.telephoenic.orders360.repository;

import android.util.Log;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.EssentialData;
import com.telephoenic.orders360.dao.essentialData.EssentialDataDao;
import com.telephoenic.orders360.model.AppPrefs;

import org.springframework.http.HttpStatus;

/**
 * Created by Ru'a on 12/3/2019.
 */

public class EssentialDataRepository extends BaseRepository {
    private static final String TAG = "EssentialDataRepository";

    private EssentialDataDao essentialDataDao;
    private SingleLiveEvent<ResponseWrapper<EssentialData>> essentialLiveData;

    public EssentialDataRepository() {
        super();
        essentialDataDao = new EssentialDataDao();
        essentialLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<EssentialData>> getEssentialData(final long userId) {
        final ResponseWrapper<EssentialData> responseWrapper = new ResponseWrapper<>();

        disposable.add(essentialDataDao.getEssentialData(userId).subscribeWith(new SingleObserver<EssentialData>() {
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                essentialLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(EssentialData response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                essentialLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
//                if (error.equals(String.valueOf(HttpStatus.UNAUTHORIZED.value()))) {
//                    if (!getNewToken(AppPrefs.getLoginBase64(Order360Application.getInstance())).hasError()) {
//                        Log.e(TAG, "onFailure: essentialLiveData");
//                        getEssentialData(userId);
//                    }
//                } else {
                    responseWrapper.setHasError(true);
                    responseWrapper.setErrorMessage(error);
                    essentialLiveData.setValue(responseWrapper);
                //}
            }

//            @Override
//            protected void refreshData() {
//                if (!getNewToken(AppPrefs.getLoginBase64(Order360Application.getInstance())).hasError()) {
//                    Log.e(TAG, "onFailure: essentialLiveData");
//                    getEssentialData(userId);
//                }
//            }
        }));

        return essentialLiveData;
    }

}
