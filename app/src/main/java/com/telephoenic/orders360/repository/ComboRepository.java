package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.BundleWraper;
import com.telephoenic.orders360.dao.combo.ComboDao;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class ComboRepository extends BaseRepository {

    private ComboDao comboDao ;
    private SingleLiveEvent<ResponseWrapper<BundleWraper>> getAllBundle;

    public ComboRepository() {
        super();
        comboDao = new ComboDao();
        getAllBundle = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<BundleWraper>> getAllBundle(int pageNumber, int pageIndex
            , long vendorId, long userId, String search) {
        final ResponseWrapper<BundleWraper> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                comboDao.getAllBundle(pageNumber, pageIndex, vendorId, userId, search).subscribeWith(new SingleObserver<BundleWraper>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        getAllBundle.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(BundleWraper response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        getAllBundle.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        getAllBundle.setValue(responseWrapper);
                    }
                })
        );
        return getAllBundle;
    }

}
