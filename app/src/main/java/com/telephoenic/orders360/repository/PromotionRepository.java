package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ProductFilter;
import com.telephoenic.orders360.controller.server.model.PromotionWraper;
import com.telephoenic.orders360.dao.promotion.PromotionDao;

import okhttp3.ResponseBody;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class PromotionRepository extends BaseRepository {
    private PromotionDao promotionDao;
    private SingleLiveEvent<ResponseWrapper<PromotionWraper>> getAllPromotion;
    private SingleLiveEvent<ResponseWrapper<ResponseBody>> promotionLiveData;

    public PromotionRepository() {
        promotionDao = new PromotionDao();
        getAllPromotion = new SingleLiveEvent<>();
        promotionLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<PromotionWraper>> getGetAllPromotion(int pageNumber, int pageIndex
            , long vendorId, long groupId, String search, ProductFilter productFilter) {
        final ResponseWrapper<PromotionWraper> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                promotionDao.getAllPromotion(pageNumber, pageIndex, vendorId, groupId, search,productFilter).subscribeWith(new SingleObserver<PromotionWraper>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        getAllPromotion.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(PromotionWraper response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        getAllPromotion.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        getAllPromotion.setValue(responseWrapper);
                    }
                })
        );
        return getAllPromotion;
    }

    public SingleLiveEvent<ResponseWrapper<ResponseBody>> getPromotion(String prodId ,String groupId){
        final ResponseWrapper<ResponseBody> responseWrapper = new ResponseWrapper<>();
        disposable.add(promotionDao.getPromotion(prodId, groupId).subscribeWith(new SingleObserver<ResponseBody>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                promotionLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(ResponseBody response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                promotionLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                promotionLiveData.setValue(responseWrapper);
            }
        }));
        return promotionLiveData;
    }

}
