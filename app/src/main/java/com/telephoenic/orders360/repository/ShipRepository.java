package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.OnCompletedObserver;
import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ShipmentOrderFilter;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalSuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ArrivalUnsuccessfulModel;
import com.telephoenic.orders360.controller.server.model.shiporder.FromToSerialModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentMethodModel;
import com.telephoenic.orders360.controller.server.model.shiporder.PaymentResponse;
import com.telephoenic.orders360.controller.server.model.shiporder.SerialsQuantity;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.SingleSerialModel;
import com.telephoenic.orders360.dao.ship.ShipDao;

import java.util.List;

public class ShipRepository extends BaseRepository {
    private ShipDao shipDao;
    private SingleLiveEvent<ResponseWrapper<List<ShipModel>>> responseWrapperSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper<List<ShipItemsModel>>> shipItemsModelSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper> shipItemSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper> saveSerialsSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper> saveQuantitySerialsSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper> saveRangeSerialsSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper> confirmArrivalUnSuccessfulSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper> confirmArrivalSuccessfulSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper<List<OrderRejectedReason>>> reasonWrapperSingleLiveEvent;
    private SingleLiveEvent<ResponseWrapper<PaymentResponse>> paymentMethodSingleLiveEvent;


    public ShipRepository() {
        super();
        shipDao = new ShipDao();
        responseWrapperSingleLiveEvent = new SingleLiveEvent<>();
        shipItemsModelSingleLiveEvent = new SingleLiveEvent<>();
        shipItemSingleLiveEvent = new SingleLiveEvent<>();
        saveSerialsSingleLiveEvent = new SingleLiveEvent<>();
        saveQuantitySerialsSingleLiveEvent = new SingleLiveEvent<>();
        saveRangeSerialsSingleLiveEvent = new SingleLiveEvent<>();
        confirmArrivalUnSuccessfulSingleLiveEvent = new SingleLiveEvent<>();
        confirmArrivalSuccessfulSingleLiveEvent = new SingleLiveEvent<>();
        reasonWrapperSingleLiveEvent = new SingleLiveEvent<>();
        paymentMethodSingleLiveEvent = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<ShipModel>>> getShipOrders(int pageIndex, int pageSize, Long deliveryUserId, String status,
                                                                           ShipmentOrderFilter shipmentOrderFilter,String search_term) {
        final ResponseWrapper<List<ShipModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.getShipOrders(pageIndex, pageSize, deliveryUserId, status,shipmentOrderFilter,search_term).subscribeWith(new SingleObserver<List<ShipModel>>() {
                    @Override
                    protected void noConnection() {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        responseWrapperSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse(List<ShipModel> response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        responseWrapperSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        responseWrapperSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return responseWrapperSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper<List<ShipItemsModel>>> getShipItemOrders(Long id) {
        final ResponseWrapper<List<ShipItemsModel>> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.getShipItemOrder(id).subscribeWith(new SingleObserver<List<ShipItemsModel>>() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        shipItemsModelSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(List<ShipItemsModel> response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        shipItemsModelSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        shipItemsModelSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return shipItemsModelSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper> shipItemOrders(Long id) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.shipItems(id).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        shipItemSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        shipItemSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        shipItemSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return shipItemSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper> saveSingleSerials(Long id, List<SingleSerialModel> serials) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.saveSingleSerials(id, serials).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        saveSerialsSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        saveSerialsSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        saveSerialsSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return saveSerialsSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper> saveQuantitySerials(Long id, List<SerialsQuantity> serials) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.saveQuantitySerials(id, serials).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        saveQuantitySerialsSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        saveQuantitySerialsSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        saveQuantitySerialsSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return saveQuantitySerialsSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper> saveRangeSerials(Long id, List<FromToSerialModel> serials) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.saveRangeSerials(id, serials).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        saveRangeSerialsSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        saveRangeSerialsSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        saveRangeSerialsSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return saveRangeSerialsSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper> confirmArrivalSuccess(Long shipmentId, ArrivalSuccessfulModel arrivalSuccessfulModel) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.confirmArrivalSuccessful(shipmentId, arrivalSuccessfulModel).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        confirmArrivalSuccessfulSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        confirmArrivalSuccessfulSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        confirmArrivalSuccessfulSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return confirmArrivalSuccessfulSingleLiveEvent;

    }

    public SingleLiveEvent<ResponseWrapper> confirmArrivalUnSuccess(Long shipmentId, ArrivalUnsuccessfulModel arrivalUnsuccessfulModel) {
        final ResponseWrapper responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.confirmArrivalUnSuccessful(shipmentId, arrivalUnsuccessfulModel).subscribeWith(new OnCompletedObserver() {
                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        confirmArrivalUnSuccessfulSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse() {
                        responseWrapper.setHasError(false);
                        confirmArrivalUnSuccessfulSingleLiveEvent.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        confirmArrivalUnSuccessfulSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );
        return confirmArrivalUnSuccessfulSingleLiveEvent;
    }

    public SingleLiveEvent<ResponseWrapper<List<OrderRejectedReason>>> getReason(String code, Long vendorId) {
        final ResponseWrapper<List<OrderRejectedReason>> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.getReason(code, vendorId).subscribeWith(new SingleObserver<List<OrderRejectedReason>>() {
                    @Override
                    protected void noConnection() {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        reasonWrapperSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse(List<OrderRejectedReason> response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        reasonWrapperSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        reasonWrapperSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return reasonWrapperSingleLiveEvent;
    }

    public SingleLiveEvent<ResponseWrapper<PaymentResponse>> getPaymentMethod() {
        final ResponseWrapper<PaymentResponse> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                shipDao.getPaymentMethod().subscribeWith(new SingleObserver<PaymentResponse>() {
                    @Override
                    protected void noConnection() {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        paymentMethodSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse(PaymentResponse response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        paymentMethodSingleLiveEvent.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {

                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(error);
                        paymentMethodSingleLiveEvent.setValue(responseWrapper);

                    }
                })
        );

        return paymentMethodSingleLiveEvent;

    }
}
