package com.telephoenic.orders360.repository;

import android.app.Activity;
import android.content.Context;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.User;
import com.telephoenic.orders360.controller.server.model.UserPassword;
import com.telephoenic.orders360.dao.user.UserDao;


public class UserRepository extends BaseRepository {
    private UserDao userDao;
    private SingleLiveEvent<ResponseWrapper<Boolean>> checkMobileNumber;
    private SingleLiveEvent<ResponseWrapper<String>> sendPassCode;
    private  SingleLiveEvent<ResponseWrapper<Boolean>> changePassword;
    private  SingleLiveEvent<ResponseWrapper<Boolean>> changePasswordLiveData;
    private SingleLiveEvent<ResponseWrapper<User>> getUserLiveData;

    public UserRepository() {
        super();
        userDao = new UserDao();
        checkMobileNumber = new SingleLiveEvent<>();
        sendPassCode = new SingleLiveEvent<>();
        changePassword = new SingleLiveEvent<>();
        changePasswordLiveData = new SingleLiveEvent<>();
        getUserLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<Boolean>> getCheckMobileNumber(String mobileNumber, final Context context, final Activity activity) {
        final ResponseWrapper<Boolean> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                userDao.checkMobileNumberAvailability(mobileNumber).subscribeWith(new SingleObserver<Boolean>() {
                    @Override
                    protected void noConnection() {
//                        ShowNoConnectionInternet(context, activity);
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        checkMobileNumber.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(Boolean response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        checkMobileNumber.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        checkMobileNumber.setValue(responseWrapper);
                    }
                })
        );
        return checkMobileNumber;
    }

    public SingleLiveEvent<ResponseWrapper<String>> sendPassCode(String mobileNumber,long otp, final Context context, final Activity activity) {
        final ResponseWrapper<String> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                userDao.sendPassCode(mobileNumber,otp).subscribeWith(new SingleObserver<String>() {
                    @Override
                    protected void noConnection() {
//                        ShowNoConnectionInternet(context, activity);
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        sendPassCode.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(String response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        sendPassCode.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        sendPassCode.setValue(responseWrapper);
                    }
                })
        );
        return sendPassCode;
    }

    public SingleLiveEvent<ResponseWrapper<Boolean>> changePassword(UserPassword userPassword, final Context context, final Activity activity) {
        final ResponseWrapper<Boolean> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                userDao.changePasswordFromOTP(userPassword).subscribeWith(new SingleObserver<Boolean>() {
                    @Override
                    protected void noConnection() {
//                        ShowNoConnectionInternet(context, activity);
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        changePassword.setValue(responseWrapper);
                    }

                    @Override
                    protected void onResponse(Boolean response) {
                        responseWrapper.setHasError(false);
                        responseWrapper.setContent(response);
                        changePassword.setValue(responseWrapper);

                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorMessage(error);
                        changePassword.setValue(responseWrapper);
                    }
                })
        );
        return changePassword;
    }

    public SingleLiveEvent<ResponseWrapper<Boolean>> changePasswordInsideApp(UserPassword userPassword){
        final ResponseWrapper<Boolean> responseWrapper = new ResponseWrapper<>();
        disposable.add(userDao.changePasswordInsideApp(userPassword).subscribeWith(new SingleObserver<Boolean>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                changePasswordLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(Boolean response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                changePasswordLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                changePasswordLiveData.setValue(responseWrapper);
            }
        }));
        return changePasswordLiveData;
    }

    public SingleLiveEvent<ResponseWrapper<User>> getUserProfile(long id){
        final ResponseWrapper<User> responseWrapper = new ResponseWrapper<>();
        disposable.add(userDao.getUserProfile(id).subscribeWith(new SingleObserver<User>(){
            @Override
            protected void noConnection() {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                getUserLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onResponse(User response) {
                responseWrapper.setHasError(false);
                responseWrapper.setContent(response);
                getUserLiveData.setValue(responseWrapper);
            }

            @Override
            protected void onFailure(String error) {
                responseWrapper.setHasError(true);
                responseWrapper.setErrorMessage(error);
                getUserLiveData.setValue(responseWrapper);
            }
        }));
        return getUserLiveData;
    }
}
