package com.telephoenic.orders360.repository;

import com.telephoenic.orders360.basemodel.data.network.ResponseWrapper;
import com.telephoenic.orders360.basemodel.data.network.SingleLiveEvent;
import com.telephoenic.orders360.basemodel.data.network.SingleObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.BrandModel;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.dao.brand.BrandDao;

import java.util.List;

/**
 * Created by Ru'a on 12/24/2019.
 */

public class BrandRepository extends BaseRepository {

    private BrandDao brandDao;
    private SingleLiveEvent<ResponseWrapper<List<BrandWrapper>>> brandsLiveData;

    public BrandRepository() {
        super();
        brandDao = new BrandDao();
        brandsLiveData = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<ResponseWrapper<List<BrandWrapper>>> getBrandsLiveData(final Long vendorID) {
        final ResponseWrapper<List<BrandWrapper>> responseWrapper = new ResponseWrapper<>();
        disposable.add(
                brandDao.getBrands(vendorID).subscribeWith(new SingleObserver<List<BrandWrapper>>() {

                    @Override
                    protected void noConnection() {
                        responseWrapper.setHasError(true);
                        responseWrapper.setErrorCode(AppConstants.INTERNET_CONNECTION);
                        brandsLiveData.setValue(responseWrapper);

                    }

                    @Override
                    protected void onResponse(List<BrandWrapper> response) {
                        responseWrapper.setContent(response);
                        responseWrapper.setHasError(false);
                        brandsLiveData.setValue(responseWrapper);
                    }

                    @Override
                    protected void onFailure(String error) {
                        responseWrapper.setErrorMessage(error);
                        responseWrapper.setHasError(true);
                        brandsLiveData.setValue(responseWrapper);

                    }
                })
        );
        return brandsLiveData;
    }
}
