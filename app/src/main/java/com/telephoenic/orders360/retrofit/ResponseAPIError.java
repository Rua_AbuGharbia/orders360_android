package com.telephoenic.orders360.retrofit;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ResponseAPIError {

//    private int statusCode;
//    private String message;
    private HttpStatus httpStatus;
    private String message;
    private String code;
    private List<String> errors;
    private Object extraInfo;

    public ResponseAPIError() {}

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    public List<String> getErrors() {
        return errors;
    }

    public Object getExtraInfo() {
        return extraInfo;
    }

    @Override
    public String toString() {
        return "ResponseAPIError{" +
                "httpStatus=" + httpStatus +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                ", errors=" + errors +
                ", extraInfo=" + extraInfo +
                '}';
    }
}