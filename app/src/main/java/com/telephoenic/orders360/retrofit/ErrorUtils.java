package com.telephoenic.orders360.retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static ResponseAPIError parseError(Response<?> response) {
        Converter<ResponseBody, ResponseAPIError> converter =
                RetrofitClient.getInstance()
                        .responseBodyConverter(ResponseAPIError.class, new Annotation[0]);

        ResponseAPIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ResponseAPIError();
        }

        return error;
    }
}