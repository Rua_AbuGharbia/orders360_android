package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;

import java.util.List;
import java.util.Locale;

public class OrderItemsAdapter extends ArrayAdapter<OrderItemModel> {
    private Context mContext;
    private List<OrderItemModel> data;
    private View.OnClickListener onClickListener;


    public OrderItemsAdapter(Context context, int textViewResourceId, List<OrderItemModel> objects ,View.OnClickListener onClickListener ) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
        this.onClickListener = onClickListener;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;

            row = LayoutInflater.from(mContext).inflate(R.layout.order_items, null);

            final TextView productName =  row.findViewById(R.id.text_product_name);
            final TextView quantity =  row.findViewById(R.id.text_product_quntity);
            final TextView itemPrice =  row.findViewById(R.id.text_item_price);
            final TextView totalAmount =  row.findViewById(R.id.text_product_total_amount);
            //final Button history=  row.findViewById(R.id.button_history);
            final ImageView arrow=  row.findViewById(R.id.image_arrow);

        productName.setTag(position);
        quantity.setTag(position);
        itemPrice.setTag(position);
        totalAmount.setTag(position);
       // history.setTag(position);
        arrow.setTag(position);

       // itemPrice.setText(data.get(position).getType());

        Integer deliveredQuantity = data.get(position).getDeliverdQuantity();
        if (deliveredQuantity==null){
            deliveredQuantity = 0;
        }
        Integer orderedQuantity = data.get(position).getOrderedQuantity();
        if (orderedQuantity==null){
            orderedQuantity = 0;
        }
        Integer allQuantity = deliveredQuantity+orderedQuantity;

        if(data.get(position).getProduct()!=null){
            productName.setText(data.get(position).getProduct().getName());
            double productPrice = data.get(position).getTotalAmount() / allQuantity;
            itemPrice.setText((float)Math.round(productPrice * 100) / 100+" "+ AppConstants.CURRENCY);
        } else if(data.get(position).getCombo()!=null) {
            productName.setText(data.get(position).getCombo().getName());
            List<ComboInfoWrapper> listOfComboInfo = data.get(position).getCombo().getListOfComboInfo();
            /*double totalPrice = 0.0;
            for(ComboInfoWrapper wrapper :listOfComboInfo){
                totalPrice+= wrapper.getPrice();
            }*/
            itemPrice.setText((float)Math.round(data.get(position).getCombo().getPrice() * 100) / 100+"");
        }

        quantity.setText(allQuantity+"");

        totalAmount.setText(" " + String.format(Locale.ENGLISH, "%.2f",data.get(position).getTotalAmount()) + " " + AppConstants.CURRENCY);


        //totalAmount.setText(data.get(position).getTotalAmount() +" "+AppConstants.CURRENCY);

        //history.setOnClickListener(onClickListener);
        arrow.setOnClickListener(onClickListener);

        return row;
    }

    @Override
    public OrderItemModel getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
