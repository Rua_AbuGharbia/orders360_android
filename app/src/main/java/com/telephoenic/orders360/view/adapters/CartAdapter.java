package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.typedef.MaxQtyItem;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.database.AppDatabase;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.fragments.PurchaseFragment;
import com.telephoenic.orders360.controller.server.model.ComboItem;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.SingleItem;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.CommissionDialog;
import com.telephoenic.orders360.view.customcontrols.ProductDetailsDialog;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Object> data = new ArrayList<>();
    View.OnClickListener clickListener;
    private String signature;
    private changeProductItem changeProductItem;


    String imagesName;

    private boolean isChanged = true;
    PosProfileWrapper posProfileWrapper;
    private List<String> privilegeList;

    public CartAdapter(Context context, int textViewResourceId, List<Object> objects, View.OnClickListener clickListener, String signature) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper = AppUtils.getProfile(context);
        this.signature = signature;

    }

    public CartAdapter(Context context, int textViewResourceId, List<Object> objects) {
        mContext = context;
        data = objects;
        posProfileWrapper = AppUtils.getProfile(context);

    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType) {
            case 0:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_promotion, parent, false);
                return new PromotionViewHolder(itemView);
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_product, parent, false);
                return new ProductViewHolder(itemView);
            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_combo, parent, false);
                return new ComboViewHolder(itemView);
        }

        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat expiryformatter = new SimpleDateFormat("MM/yyyy", Locale.ENGLISH);
        switch (holder.getItemViewType()) {
            case 1:
                final ProductViewHolder productViewHolder = (ProductViewHolder) holder;
                final SingleItem productWrapper = (SingleItem) data.get(position);
                productViewHolder.loaderView.setVisibility(View.VISIBLE);
                productViewHolder.imageClose.setVisibility(View.VISIBLE);
                if (productWrapper.getDiscountPercentage() == null) {
                    productWrapper.getProduct().setDiscount(0.0);
                    productWrapper.setDiscountPercentage(0.0);
                }else {
                    productWrapper.getProduct().setDiscount(productWrapper.getDiscountPercentage());
                    productWrapper.getProduct().setDiscountedPrice(productWrapper.getDiscountedPrice());

                }
                if (posProfileWrapper != null) {
                    privilegeList = posProfileWrapper.getUser().getPermissions();
                }

                for (String item : privilegeList) {
                    if (AppConstants.ROLE_ORDER_DETAIL_FOR_POS.equals(item)) {
                        productViewHolder.plusMinusActions.setVisibility(View.VISIBLE);
                        productViewHolder.totalPrice.setVisibility(View.VISIBLE);
                    }
                }

                if (productWrapper.getDiscountPercentage() != null && productWrapper.getDiscountPercentage() > 0) {
                    productViewHolder.discountLinear.setVisibility(View.VISIBLE);
                   // productViewHolder.oldPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getProduct().getPrice()) + " " + AppConstants.CURRENCY);
                    productViewHolder.oldPrice.setText(String.format(" %s %s", AppUtils.amountFormat(productWrapper.getProduct().getPrice()), AppConstants.CURRENCY));

                    productViewHolder.oldPrice.setPaintFlags(productViewHolder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    productViewHolder.discount.setText(productWrapper.getDiscountPercentage() + "%");
                    if (!AppConstants.SHOW_POINT)
                    productViewHolder.points.setVisibility(View.GONE);
                   // productViewHolder.price.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getDiscountedPrice()) + AppConstants.CURRENCY);
                    productViewHolder.price.setText(String.format(" %s %s", AppUtils.amountFormat(productWrapper.getDiscountedPrice()), AppConstants.CURRENCY));



                } else {
                  //  productViewHolder.price.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice()) + AppConstants.CURRENCY);
                    productViewHolder.price.setText(String.format(" %s %s", AppUtils.amountFormat(productWrapper.getPrice()), AppConstants.CURRENCY));
                }
               // productViewHolder.price.setText(String.format(" %s %s ", String.format(Locale.ENGLISH, "%.2f", (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getProduct().getDiscount()) / 100)), AppConstants.CURRENCY));

                productViewHolder.imageClose.setTag(position);
                productViewHolder.imageClose.setOnClickListener(clickListener);

                productViewHolder.name.setText(productWrapper.getProduct().getName());
                productViewHolder.alternativeName.setText(productWrapper.getProduct().getAlternativeName());


                productViewHolder.purchaseNumber.setText(productWrapper.getOrderedQuantity() == null ? "0" : productWrapper.getOrderedQuantity());

                productViewHolder.points.setText(productWrapper.getProduct().getLoyaltyPoint() == null ? "+ 0 Pts" : "+" + productWrapper.getProduct().getLoyaltyPoint() + " Pts");

                Integer currentQty = Integer.parseInt(productViewHolder.purchaseNumber.getText().toString());
                double total_price;
                if (productWrapper.getDiscountPercentage() > 0) {
                    total_price = currentQty * productWrapper.getDiscountedPrice();

                } else {
                    total_price = currentQty * productWrapper.getPrice();
                }
               // productViewHolder.totalPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                productViewHolder.totalPrice.setText(String.format(" %s %s", AppUtils.amountFormat(total_price), AppConstants.CURRENCY));


                productViewHolder.imageMinus.setTag(position);
                productViewHolder.imagePlus.setTag(position);

                if (productWrapper.getProduct().getExpiryDate() != null) {
                    String expiryDateString = expiryformatter.format(new java.util.Date(Long.valueOf(productWrapper.getProduct().getExpiryDate())));
                    productViewHolder.expiryDate.setText(expiryDateString);
                } else {
                    productViewHolder.expiryDate.setVisibility(View.GONE);
                    productViewHolder.expiryDateTitle.setVisibility(View.GONE);
                }

                productViewHolder.purchaseNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void afterTextChanged(Editable s) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        // TODO Auto-generated method stub
                    }

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().equals("") || s.toString().startsWith("0") && s.toString().endsWith("0") || s.toString().equals("-")) {

                            productViewHolder.purchaseNumber.setHint("1");
                            double total_price;
                            if (productWrapper.getDiscountPercentage() > 0) {
                                total_price = 1 * productWrapper.getDiscountedPrice();

                            } else {
                                total_price = 1 * productWrapper.getPrice();
                            }
                          //  productViewHolder.totalPrice.setText(String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                            productViewHolder.totalPrice.setText(String.format(" %s %s", AppUtils.amountFormat(total_price), AppConstants.CURRENCY));

                            productWrapper.setOrderedQuantity("1");
                            productWrapper.getProduct().setCount("1");
                            productWrapper.getProduct().setSelectedCommission(null);
                            productWrapper.getProduct().setCommissionQtyToValueTransient(null);

                        } else {
                            Integer currentQty = Integer.parseInt(productViewHolder.purchaseNumber.getText().toString());
                            // double total_price = currentQty * (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getDiscountPercentage()) / 100);
                            double total_price;
                            if (productWrapper.getDiscountPercentage() > 0) {
                                total_price = currentQty * productWrapper.getDiscountedPrice();

                            } else {
                                total_price = currentQty * productWrapper.getPrice();
                            }
                           // productViewHolder.totalPrice.setText(String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                            productViewHolder.totalPrice.setText(String.format(" %s %s", AppUtils.amountFormat(total_price), AppConstants.CURRENCY));

                            productWrapper.setOrderedQuantity(s + "");
                            productWrapper.getProduct().setCount(currentQty.toString());
                            productWrapper.getProduct().setSelectedCommission(null);
                            productWrapper.getProduct().setCommissionQtyToValueTransient(null);
                            productWrapper.getProduct().setDiscountedPrice(productWrapper.getDiscountedPrice());
                        }

                        SaveCart.getInstance().changeProductOrderedQuantity(productWrapper.getProduct());
                        AppUtils.updateNotifactionOrder((Activity) mContext);
                        PurchaseFragment.setSum();
                        if (changeProductItem != null) {
                            changeProductItem.change(true);
                        }

                    }
                });

                productViewHolder.imagePlus.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        Integer currentQty;
                        if (productViewHolder.purchaseNumber.getText().toString().equals("")) {
                            currentQty = 1;
                        } else {
                            currentQty = Integer.parseInt(productViewHolder.purchaseNumber.getText().toString());
                        }
                        if (currentQty < MaxQtyItem.MAX_QTY) {
                            currentQty++;
                            //   double total_price = currentQty * (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getDiscountPercentage()) / 100);
                            double total_price;
                            if (productWrapper.getDiscountPercentage() > 0) {
                                total_price = currentQty * productWrapper.getDiscountedPrice();

                            } else {
                                total_price = currentQty * productWrapper.getPrice();
                            }
                         //   productViewHolder.totalPrice.setText(String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                            productViewHolder.totalPrice.setText(String.format(" %s %s", AppUtils.amountFormat(total_price), AppConstants.CURRENCY));

                            productViewHolder.purchaseNumber.setText(String.valueOf(currentQty));
                            productWrapper.getProduct().setCount(currentQty.toString());
                            productWrapper.getProduct().setSelectedCommission(null);
                            productWrapper.getProduct().setCommissionQtyToValueTransient(null);
                            SaveCart.getInstance().changeProductOrderedQuantity(productWrapper.getProduct());
                            AppUtils.updateNotifactionOrder((Activity) mContext);
                            PurchaseFragment.setSum();
                            if (changeProductItem != null) {
                                changeProductItem.change(true);
                            }
                        }

                    }
                });

                productViewHolder.imageMinus.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        if (!productViewHolder.purchaseNumber.getText().toString().equals("")) {
                            Integer currentQty = Integer.parseInt(productViewHolder.purchaseNumber.getText().toString());
                            if (currentQty > 1) {
                                currentQty--;
                                //   double total_price = currentQty * (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getProduct().getDiscount()) / 100);
                                double total_price;
                                if (productWrapper.getDiscountPercentage() > 0) {
                                    total_price = currentQty * productWrapper.getDiscountedPrice();

                                } else {
                                    total_price = currentQty * productWrapper.getPrice();
                                }
                               // productViewHolder.totalPrice.setText(String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                                productViewHolder.totalPrice.setText(String.format(" %s %s", AppUtils.amountFormat(total_price), AppConstants.CURRENCY));

                                productViewHolder.purchaseNumber.setText(String.valueOf(currentQty));
                                productWrapper.getProduct().setCount(currentQty.toString());
                                productWrapper.getProduct().setSelectedCommission(null);
                                productWrapper.getProduct().setCommissionQtyToValueTransient(null);
                                SaveCart.getInstance().changeProductOrderedQuantity(productWrapper.getProduct());
                                AppUtils.updateNotifactionOrder((Activity) mContext);
                                PurchaseFragment.setSum();
                                if (changeProductItem != null) {
                                    changeProductItem.change(true);
                                }
                            }
                        }
                    }
                });

                productViewHolder.productImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(mContext,
                                productWrapper.getProduct().getName(), productWrapper.getProduct().getImagesName(), productWrapper.getProduct().getDescription(), productWrapper.getProduct().getRetailPrice(), productWrapper.getProduct().getId(), ProductDetailsDialog.PRODUCT);
                        productDetailsDialog.show();
                    }
                });
                try {
                    //TODO set Image from server
                    imagesName = productWrapper.getProduct().getImagesName();

                    Integer imageID = productWrapper.getProduct().getId();
                    if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                        imagesName = "empty";
                    }


                    if (imagesName != null || !TextUtils.isEmpty(imagesName)) {
                        if (imagesName.contains(";")) {
                            imagesName = imagesName.split(";")[0];
                        }

                        Glide.with(mContext)
                                .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                                // .dontTransform()
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                        productViewHolder.loaderView.setVisibility(View.GONE);
                                        Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                                .signature(new StringSignature(String.valueOf(signature)))
                                                .error(R.drawable.place_holder_order)
                                                .into(productViewHolder.productImage);
                                        return true;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                        productViewHolder.loaderView.setVisibility(View.GONE);

                                        return false;
                                    }
                                })
                                .crossFade(800)
                                .signature(new StringSignature(String.valueOf(signature)))
                                .into(productViewHolder.productImage);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case 0:
                final PromotionViewHolder promotionViewHolder = (PromotionViewHolder) holder;
                final PromotionItem productWrapper1 = (PromotionItem) data.get(position);
                promotionViewHolder.loaderView.setVisibility(View.VISIBLE);
                promotionViewHolder.imageClose.setVisibility(View.VISIBLE);

                if (productWrapper1.getProduct().getEndDate() != null) {
                    String endDateString = formatter.format(new java.util.Date(Long.valueOf(productWrapper1.getProduct().getEndDate())));
                    Log.d("Promotion Adapter", endDateString);
                    promotionViewHolder.endDate.setText(endDateString);
                } else {
                    promotionViewHolder.endDate.setVisibility(View.GONE);
                    promotionViewHolder.endDateTitle.setVisibility(View.GONE);
                }

                if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(mContext))) {
                    promotionViewHolder.commission.setText(mContext.getResources().getString(R.string.label_details));
                    promotionViewHolder.endDate.setVisibility(View.GONE);
                    promotionViewHolder.endDateTitle.setVisibility(View.GONE);
                }

                if (productWrapper1.getProduct().getExpiryDate() != null) {
                    String expiryDateString = expiryformatter.format(new java.util.Date(Long.valueOf(productWrapper1.getProduct().getExpiryDate())));
                    promotionViewHolder.expiryDate.setText(expiryDateString);
                } else {
                    promotionViewHolder.expiryDate.setVisibility(View.GONE);
                    promotionViewHolder.expiryDateTitle.setVisibility(View.GONE);

                }

                promotionViewHolder.imageClose.setTag(position);
                promotionViewHolder.imageClose.setOnClickListener(clickListener);

                promotionViewHolder.name.setText(productWrapper1.getProduct().getName());
                promotionViewHolder.alternativeName.setText(productWrapper1.getProduct().getAlternativeName());
              //  promotionViewHolder.price.setText(String.format(" %s %s ", String.format(Locale.ENGLISH, "%.2f", productWrapper1.getPrice()), AppConstants.CURRENCY));
                promotionViewHolder.price.setText(String.format(" %s %s", AppUtils.amountFormat(productWrapper1.getPrice()), AppConstants.CURRENCY));


                promotionViewHolder.commission.setTag(position);
                promotionViewHolder.commission.setOnClickListener(clickListener);
//                promotionViewHolder.commission.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        PosProfileWrapper posProfile = AppUtils.getProfile(Order360Application.getInstance().getApplicationContext());
//                        final CommissionDialog commissionDialog = new CommissionDialog(mContext, posProfile.getUser().getId().toString(), productWrapper1, AppConstants.EDIT_ORDER);
//                        commissionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                            @Override
//                            public void onDismiss(DialogInterface dialog) {
//                                if (commissionDialog.getCommissionObject() != null
//                                    /* && commissionDialog.getCommissionList().size()>0*/) {
//                                    PurchaseFragment.setSum();
//                                    notifyDataSetChanged();
//                                }
//                            }
//                        });
//                        commissionDialog.show();
//                    }
//                });

                promotionViewHolder.productImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(mContext,
                                productWrapper1.getProduct().getName(), productWrapper1.getProduct().getImagesName(), productWrapper1.getProduct().getDescription(), productWrapper1.getProduct().getRetailPrice(), productWrapper1.getProduct().getId(), ProductDetailsDialog.PRODUCT);
                        productDetailsDialog.show();
                    }
                });
                try {
                    //TODO set Image from server
                    imagesName = productWrapper1.getProduct().getImagesName();

                    Integer imageID = productWrapper1.getProduct().getId();
                    if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                        imagesName = "empty";
                    }


                    if (imagesName != null || !TextUtils.isEmpty(imagesName)) {
                        if (imagesName.contains(";")) {
                            imagesName = imagesName.split(";")[0];
                        }

                        Glide.with(mContext)
                                .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                                // .dontTransform()
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                        promotionViewHolder.loaderView.setVisibility(View.GONE);
                                        Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                                .signature(new StringSignature(String.valueOf(signature)))
                                                .error(R.drawable.place_holder_order)
                                                .into(promotionViewHolder.productImage);
                                        return true;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                        promotionViewHolder.loaderView.setVisibility(View.GONE);

                                        return false;
                                    }
                                })
                                .crossFade(800)
                                .signature(new StringSignature(String.valueOf(signature)))
                                .into(promotionViewHolder.productImage);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case 2:
                final ComboViewHolder comboViewHolder = (ComboViewHolder) holder;
                final ComboItem comboWrapper = (ComboItem) data.get(position);

                comboViewHolder.loaderView.setVisibility(View.VISIBLE);

                comboViewHolder.imageClose.setVisibility(View.VISIBLE);

                comboViewHolder.name.setText(comboWrapper.getCombo().getName());

              //  comboViewHolder.price.setText(String.format("%s %s ", (float) Math.round(comboWrapper.getPrice() * 100) / 100, AppConstants.CURRENCY));
                comboViewHolder.price.setText(String.format(" %s %s", AppUtils.amountFormat(comboWrapper.getPrice()), AppConstants.CURRENCY));



                if (comboWrapper.getCombo().getEndDate() != null) {
                    String endDateString = formatter.format(new Date(Long.valueOf(comboWrapper.getCombo().getEndDate())));
                    comboViewHolder.endDate.setText(endDateString);
                }

                comboViewHolder.buttonDetails.setTag(position);
                comboViewHolder.imageClose.setTag(position);
                comboViewHolder.imageClose.setOnClickListener(clickListener);
                comboViewHolder.buttonDetails.setOnClickListener(clickListener);

                comboViewHolder.imageCombo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(mContext,
                                comboWrapper.getCombo().getName(), comboWrapper.getCombo().getLogoImageName(), null, comboWrapper.getCombo().getId(), ProductDetailsDialog.BUNDLE);
                        productDetailsDialog.show();
                    }
                });

                try {

                    imagesName = comboWrapper.getCombo().getLogoImageName();
                    if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                        imagesName = "Empty";
                    }

                    int imageID = comboWrapper.getCombo().getId();
                    if (imagesName != null || !TextUtils.isEmpty(imagesName)) {
                        if (imagesName.contains(";")) {
                            imagesName = imagesName.split(";")[0];
                        }

                        Glide.with(mContext)
                                .load(AppConstants.URL_LIVE + "combo/attachments/" + imageID + "/" + imagesName)
                                // .dontTransform()
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                        comboViewHolder.loaderView.setVisibility(View.GONE);
                                        Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                                .signature(new StringSignature(String.valueOf(signature)))
                                                .error(R.drawable.place_holder_order)
                                                .into(comboViewHolder.imageCombo);
                                        return true;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                        comboViewHolder.loaderView.setVisibility(View.GONE);

                                        return false;
                                    }
                                })
                                //  .crossFade(800)
                                .dontTransform()
                                .crossFade(800)
                                .thumbnail(0.5f)
                                .signature(new StringSignature(String.valueOf(signature)))
                                .into(comboViewHolder.imageCombo);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof SingleItem) {
            return 1;
        } else if (data.get(position) instanceof PromotionItem) {
            return 0;
            //       }
//            ProductWrapper productWrapper = (ProductWrapper) data.get(position);
//            if (productWrapper.getCommissionListQantity() != null || productWrapper.getCommissionListAttribute() != null) {
//                return 0; // promotion
//            }
//            return 1;  // product
        } else if (data.get(position) instanceof ComboItem) {
            return 2;  // combo
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        final TextView name, alternativeName, price, points, totalPrice, titleTotalPrice, expiryDate, expiryDateTitle, discount, oldPrice;
        final EditText purchaseNumber;
        final ImageView productImage, imageMinus, imagePlus, imageClose;
        final ConstraintLayout layout;
        final LinearLayout plusMinusActions, discountLinear;
        final SpinKitView loaderView;

        public ProductViewHolder(View view) {
            super(view);
            loaderView = view.findViewById(R.id.pagination_loading_view);
            name = view.findViewById(R.id.text_product_name);
            alternativeName = view.findViewById(R.id.text_product_alternative_name);
            productImage = view.findViewById(R.id.image_product);
            imageMinus = view.findViewById(R.id.image_minus);
            imagePlus = view.findViewById(R.id.image_plus);
            imageClose = view.findViewById(R.id.image_close);
            price = view.findViewById(R.id.text_product_price);
            points = view.findViewById(R.id.text_product_points);
            expiryDate = view.findViewById(R.id.textView_expiryDate);
            totalPrice = view.findViewById(R.id.text_total_price);
            titleTotalPrice = view.findViewById(R.id.text_total_price_title);
            purchaseNumber = view.findViewById(R.id.purchases_num);
            layout = view.findViewById(R.id.layout);
            plusMinusActions = view.findViewById(R.id.plus_minus_actions);
            expiryDateTitle = view.findViewById(R.id.textView_expiryDate_title);
            discount = view.findViewById(R.id.Discount_TextView);
            oldPrice = view.findViewById(R.id.OldPrice_TextView);
            discountLinear = view.findViewById(R.id.Discount_LinerLayout);

        }
    }

    public class PromotionViewHolder extends RecyclerView.ViewHolder {
        final TextView name, alternativeName, price, totalPrice, expiryDate;
        final TextView endDate, endDateTitle, expiryDateTitle;
        final ImageView productImage, imageClose;
        final Button commission;
        final ConstraintLayout layout;
        final SpinKitView loaderView;

        public PromotionViewHolder(View view) {
            super(view);
            loaderView = view.findViewById(R.id.pagination_loading_view);
            name = view.findViewById(R.id.text_product_name);
            alternativeName = view.findViewById(R.id.text_product_alternative_name);
            productImage = view.findViewById(R.id.image_product);
            imageClose = view.findViewById(R.id.image_close);
            price = view.findViewById(R.id.text_product_price);
            commission = view.findViewById(R.id.button_commission);
            endDate = view.findViewById(R.id.textView_end_date);
            endDateTitle = view.findViewById(R.id.textView_end_date_title);
            layout = view.findViewById(R.id.layout);
            totalPrice = view.findViewById(R.id.text_total_price);
            expiryDate = view.findViewById(R.id.textView_expiryDate);
            expiryDateTitle = view.findViewById(R.id.textView_expiryDate_title);
        }
    }

    public class ComboViewHolder extends RecyclerView.ViewHolder {
        final TextView name, price;
        final TextView endDate;
        final ImageView imageClose, imageCombo;
        final Button buttonDetails;
        final ConstraintLayout layout;
        final SpinKitView loaderView;

        public ComboViewHolder(View view) {
            super(view);
            loaderView = view.findViewById(R.id.pagination_loading_view);
            name = view.findViewById(R.id.text_product_name);
            imageClose = view.findViewById(R.id.image_close);
            imageCombo = view.findViewById(R.id.image_combo);
            price = view.findViewById(R.id.text_product_price);
            endDate = view.findViewById(R.id.textView_end_date);
            buttonDetails = view.findViewById(R.id.button_details);
            layout = view.findViewById(R.id.layout);
        }
    }

    public interface changeProductItem {
        void change(boolean change);
    }

    public void setChangeProductItem(changeProductItem changeProductItem) {
        this.changeProductItem = changeProductItem;
    }

}
