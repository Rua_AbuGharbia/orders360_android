package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.utils.DateUtil;
import com.telephoenic.orders360.controller.viewholder.DeliveryOrdersViewHolder;

import java.util.List;
import java.util.Locale;

public class ReadyToShipOrdersAdapter extends RecyclerView.Adapter<DeliveryOrdersViewHolder> {
    private Context context;
    private OnRecyclerClick onRecyclerClick;
    private List<ShipModel> data;
    private String status;

    public ReadyToShipOrdersAdapter(Context context, List<ShipModel> data, OnRecyclerClick onRecyclerClick, String status) {
        this.context = context;
        this.onRecyclerClick = onRecyclerClick;
        this.data = data;
        this.status = status;
    }

    @NonNull
    @Override
    public DeliveryOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_order_delivery, viewGroup, false);
        return new DeliveryOrdersViewHolder(myRoot);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DeliveryOrdersViewHolder holder, final int i) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClick != null) {
                    onRecyclerClick.getPosition(i);
                }
            }
        });
        holder.arrowGo.setVisibility(View.VISIBLE);
        holder.cityRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.city_text), data.get(i).getCity()));
        holder.regionRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.region_text), data.get(i).getRegion()));
        holder.totalAmountRowOrderDeliveryTextView.setText("Total Amount : " + String.format(Locale.ENGLISH, "%.2f", data.get(i).getAmount()) + " " + AppConstants.CURRENCY);

        // holder.totalAmountRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.fragment_purchase_history_title_total_amount), data.get(i).getAmount()));
        holder.orderIdRowOrderDeliveryTextView.setText(String.format("%s%s", context.getString(R.string.order_id), data.get(i).getOrderNumber()));
        holder.shipNumberRowOrderDeliveryTextView.setText(String.format("%s%s",context.getString(R.string.shipment_number), data.get(i).getShipmentNumber()));
        holder.namePosUserRowOrderDeliveryTextView.setText(data.get(i).getPosName());


        String dateString = DateUtil.formatDate(DateUtil.parseDate((data.get(i).getOrderDate())));
        String shipDate = DateUtil.formatDate(DateUtil.parseDate((data.get(i).getShipmentDate())));


        holder.shipDateRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.ship_date), shipDate));
        if (status.equals(AppConstants.READY_TO_SHIP_STATUS)) {
            holder.deliveryStatusRowOrderDeliveryTextView.setVisibility(View.GONE);

        }
        holder.orderDateRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.order_date), dateString));


        if (data.get(i).getStatusCode().equals(AppConstants.READY_TO_SHIP_STATUS)) {
           // holder.orderDateRowOrderDeliveryTextView.setText(String.format("%s : %s",context.getString(R.string.ready_to_ship_date), shipDate));
            holder.warehouseRowOrderDeliveryTextView.setVisibility(View.VISIBLE);
            holder.warehouseRowOrderDeliveryTextView.setText(data.get(i).getWarehouseName());
            holder.deliveryStatusRowOrderDeliveryTextView.setText("Status : Ready To Ship");
        } else if (data.get(i).getStatusCode().equals(AppConstants.SHIPPED_STATUS))
           // holder.orderDateRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.order_date), dateString));
        holder.deliveryStatusRowOrderDeliveryTextView.setText("Status : Shipped");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
