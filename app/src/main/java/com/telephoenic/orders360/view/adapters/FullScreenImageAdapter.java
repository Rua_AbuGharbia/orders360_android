package com.telephoenic.orders360.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.activities.Main2Activity;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ImagesModel;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.ProductDetailsDialog;

import java.util.List;

/**
 * Created by rua on 02-Apr-18.
 */

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private List<ImagesModel> data;
    private LayoutInflater inflater;
    private String signature;
    private String urlName;

    // constructor
    public FullScreenImageAdapter(Activity activity, List<ImagesModel> data, String signature, int viewStatus) {
        this._activity = activity;
        this.data = data;
        this.signature = signature;
        if (viewStatus == ProductDetailsDialog.BUNDLE) {
            urlName = "combo/attachments/";
        } else {
            urlName = "product/attachments/";
        }
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ImageView imgDisplay;
        final SpinKitView loaderView;


        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.item_fullscreen_image, container,
                false);

        imgDisplay = viewLayout.findViewById(R.id.imgDisplay);
        loaderView = viewLayout.findViewById(R.id.pagination_loading_view);
        loaderView.setVisibility(View.VISIBLE);


        ImagesModel model = data.get(position);

        // TODO SET IMAGE TO IMAGE VIEW
        String imagesName = data.get(position).getImageName();
        final Integer imageID = data.get(position).getImageID();

        final String finalImagesName = imagesName;
        imgDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.startActivity(new Intent(_activity, Main2Activity.class).putExtra(Main2Activity.IMAGE_NAME, finalImagesName).putExtra(Main2Activity.IMAGE_URL,urlName)
                .putExtra(Main2Activity.IMAGE_ID,imageID));
            }
        });

        try {
            if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                imagesName = "Empty";
            }
            Glide.with(_activity)
                    .load(AppConstants.URL_LIVE + urlName + imageID + "/" + imagesName)
                    // .dontTransform()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                            loaderView.setVisibility(View.GONE);
                            Glide.with(_activity).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(_activity) + "/" + AppPrefs.getVendorLogo(_activity))
                                    .signature(new StringSignature(String.valueOf(signature)))
                                    .error(R.drawable.place_holder_order)
                                    .into(imgDisplay);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                            loaderView.setVisibility(View.GONE);

                            return false;
                        }
                    })
                    //  .crossFade(800)
                    .signature(new StringSignature(String.valueOf(signature)))
                    .into(imgDisplay);


        } catch (Exception e) {
            e.printStackTrace();
        }


        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}



