package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.FilterUtils;
import com.telephoenic.orders360.controller.viewholder.VendorListViewHolder;

import java.util.ArrayList;

public class VendorListAdapter extends RecyclerView.Adapter<VendorListViewHolder> {
    public ArrayList<VendorWrapper> vendorsLisFilter;
    private OnRecyclerClick onRecyclerClick;
    private Context context;
    private FilterUtils filter;
    private String signature ;

    public VendorListAdapter(ArrayList<VendorWrapper> vendorsLis, OnRecyclerClick onRecyclerClick, Context context,String signature) {
        this.vendorsLisFilter = vendorsLis;
        this.onRecyclerClick = onRecyclerClick;
        this.context = context;
        this.signature =signature;
        filter = new FilterUtils(vendorsLis, this);
    }

    @NonNull
    @Override
    public VendorListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_vendor_item, parent, false);
        return new VendorListViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorListViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (vendorsLisFilter.get(position).isSelected()) {
            holder.vendorSelectImageView.setBackground(context.getResources().getDrawable(R.drawable.circle_gray_fill_white_larg_stroke));
        } else {
            holder.vendorSelectImageView.setBackground(context.getResources().getDrawable(R.drawable.circle_blue_fill_white));

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.getPosition(position);
            }
        });
        holder.vendorNameRowVendorTextView.setText(vendorsLisFilter.get(position).getName());
        try {
            Log.e("ID",vendorsLisFilter.get(position).getId()+"");

            Glide.with(context).load(AppConstants.URL_LIVE + "vendor/attachments/" + vendorsLisFilter.get(position).getId() + "/" + vendorsLisFilter.get(position).getLogoImageName())
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .dontTransform()
                    .dontAnimate()
                    .error(R.drawable.place_holder_blue)
                    .signature(new StringSignature(String.valueOf(signature)))
                    .skipMemoryCache(false)
                    .into(holder.vendorLogoRowVendorCircleImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return vendorsLisFilter.size();
    }

    public void setList(ArrayList<VendorWrapper> list) {
        this.vendorsLisFilter = new ArrayList<>();
        this.vendorsLisFilter = list;
        notifyDataSetChanged();
    }

    //call when you want to category
    public void filterList(String text) {
        filter.filter(text);
    }
}
