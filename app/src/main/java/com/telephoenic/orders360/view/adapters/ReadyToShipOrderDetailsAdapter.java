package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.viewholder.MyOrderDetailsHolder;

import java.util.List;

public class ReadyToShipOrderDetailsAdapter extends RecyclerView.Adapter<MyOrderDetailsHolder> {
    private List<ShipItemsModel> data;
    private OnRecyclerClick onRecyclerClick;

    public ReadyToShipOrderDetailsAdapter(OnRecyclerClick onRecyclerClick, List<ShipItemsModel> data) {
        this.data = data;
        this.onRecyclerClick = onRecyclerClick;

    }

    @NonNull
    @Override
    public MyOrderDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_order_detials, parent, false);
        return new MyOrderDetailsHolder(myRoot);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyOrderDetailsHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.getPosition(position);
            }
        });

        holder.totalAmountRowMyOrderTextView.setVisibility(View.GONE);
        holder.checkBox.setVisibility(View.VISIBLE);
        holder.checkBox.setClickable(false);
        holder.checkBox.setChecked(data.get(position).getReadyToShipment());
        holder.quantityRowMyOrderTextView.setText(data.get(position).getShipmentQuantity()+"");
        holder.orderNameRowMyOrderTextView.setText(data.get(position).getItemName());

        // Integer orderedQuantity = data.get(position).getOrderedQuantity();
        //holder.itemPriceRowMyOrderTextView.setText(String.format("%s", data.get(position).getPrice()));
        // holder.totalAmountRowMyOrderTextView.setText(String.format("%s2f", totalAmount));
        // holder.totalAmountRowMyOrderTextView.setText(" " + String.format(Locale.ENGLISH, "%.2f",data.get(position).getTotalAmount()) + " " + AppConstants.CURRENCY);

        //  holder.quantityRowMyOrderTextView.setText(String.format("%s", orderedQuantity));
        //  holder.orderNameRowMyOrderTextView.setText(String.format("%s", data.get(position).getName()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
