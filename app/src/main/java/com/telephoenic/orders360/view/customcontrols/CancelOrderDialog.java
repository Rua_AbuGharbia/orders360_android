package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.view.adapters.CancelOrderReasonAdapter;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class CancelOrderDialog extends DialogFragment implements OnRecyclerClick {
    private static final Long OTHER_SELECT = -1L;
    @BindView(R.id.cancelRecyclerView)
    RecyclerView cancelRecyclerView;
    @BindInput(R.id.Reason_Orders360EditTextT)
    Orders360EditText reasonOrders360EditTextT;
    private Dialog dialog;
    private getReason rejectedReason;
    private List<OrderRejectedReason> rejectedReasonList;
    private CancelOrderReasonAdapter adapter;
    private int selectPosition = -1;
    private String reasonText;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.cancel_order_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ButterKnife.bind(this, dialog);
            OrdersView ordersView = new OrdersView();
            ordersView.bind(this, dialog.getWindow().getDecorView());
            reasonOrders360EditTextT.setVisibility(View.GONE);
            if (rejectedReasonList != null) {
                adapter = new CancelOrderReasonAdapter(rejectedReasonList, this);
                cancelRecyclerView.setAdapter(adapter);
            }
        }
        return dialog;
    }

    @SuppressLint("ValidFragment")
    public CancelOrderDialog(List<OrderRejectedReason> data) {
        rejectedReasonList = data;
    }

    @OnClick({R.id.Cancel_Buttom, R.id.Save_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Cancel_Buttom:
                dismiss();
                break;
            case R.id.Save_Buttom:
                if (rejectedReason != null && selectPosition != -1) {
                    if (rejectedReasonList.get(selectPosition).getId().equals(OTHER_SELECT)) {
                        if (TextUtils.isEmpty(reasonOrders360EditTextT.getEditTextContent().trim())) {
                            reasonOrders360EditTextT.setErrorMessage("Please fill text");
                            reasonOrders360EditTextT.validate();
                            return;
                        } else {
                            reasonText = reasonOrders360EditTextT.getEditTextContent().trim();
                        }
                    }
                    rejectedReason.getReason(rejectedReasonList.get(selectPosition), reasonText);
                    dismiss();
                } else {
                    Toast.makeText(getContext(), "Please Select Reason", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    @Override
    public void getPosition(int position) {
        selectPosition = position;
        for (int i = 0; i < rejectedReasonList.size(); i++) {
            if (i == position) {
                rejectedReasonList.get(i).setSelect(true);
            } else {
                rejectedReasonList.get(i).setSelect(false);

            }
            if (rejectedReasonList.get(position).getId().equals(OTHER_SELECT)) {
                reasonOrders360EditTextT.setVisibility(View.VISIBLE);
            } else {
                reasonOrders360EditTextT.setVisibility(View.GONE);
            }
        }
        adapter.notifyDataSetChanged();
    }

    public getReason reason(getReason rejectedReason) {
        return this.rejectedReason = rejectedReason;
    }

    public interface getReason {
        void getReason(OrderRejectedReason rejectedReason, String otherReasonText);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (selectPosition != -1) {
            rejectedReasonList.get(selectPosition).setSelect(false);
            reasonOrders360EditTextT.setVisibility(View.GONE);
        }

        super.onDismiss(dialog);
    }
}
