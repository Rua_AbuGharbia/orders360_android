package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.GetAmount;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ClaimPayments;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.telephoenic.orders360.controller.utils.AppUtils.hideKeyboard;


@SuppressLint("ValidFragment")
public class ClaimPaymentDialog extends DialogFragment {
    public static final String DATA = " data";
    public static final String REMAINING_AMOUNT = "remaining_amount";
    @BindInput(R.id.CashPayment_OrdersEditText)
    Orders360EditText claimPaymentOrdersEditText;
    @BindView(R.id.Cancel_Buttom)
    Button cancelButtom;
    @BindView(R.id.Save_Buttom)
    Button saveButtom;
    @BindView(R.id.RemainingAmount_TextView)
    TextView remainingAmountTextView;
    private Dialog dialog;
    private ClaimPayments claimPayments;
    private GetAmount getAmount;
    private double remainingAmount;
    private Double amount;


    public ClaimPaymentDialog(GetAmount getAmount) {
        this.getAmount = getAmount;

    }

    public ClaimPaymentDialog() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.row_cash_payment);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            if (getArguments() != null) {
                claimPayments = (ClaimPayments) getArguments().getSerializable(DATA);
                remainingAmount = getArguments().getDouble(REMAINING_AMOUNT);
            }
            ButterKnife.bind(this, dialog);
            OrdersView ordersView = new OrdersView();
            ordersView.bind(this, dialog.getWindow().getDecorView());
            inti();
        }
        return dialog;
    }

    private void inti() {
        cancelButtom.setVisibility(View.VISIBLE);
        saveButtom.setVisibility(View.VISIBLE);
        claimPaymentOrdersEditText.setTitle("Claim");
        if (claimPayments != null) {
            if (claimPayments.getAmount() != null) {
                remainingAmount = remainingAmount + claimPayments.getAmount();
                claimPaymentOrdersEditText.setText(" " + String.format(Locale.ENGLISH, "%.2f", claimPayments.getAmount()));
                amount = claimPayments.getAmount();
            }

        } else {
            claimPayments = new ClaimPayments();
        }
        remainingAmountTextView.setText(String.format("Remaining Amount : %s %s", remainingAmount, AppConstants.CURRENCY));
        claimPaymentOrdersEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String result = s.toString();
                if (!result.equals("") && !TextUtils.isEmpty(result.trim()) && !result.startsWith(".") ) {
                    if (result.endsWith("."))
                        result = result.replace("\\.", "");
                    amount = (Double.valueOf(result.trim()));
                } else
                    amount = 0.0;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.Delete_ImageView)
    public void onViewClicked() {
        dialog.dismiss();
    }

    @OnClick({R.id.Cancel_Buttom, R.id.Save_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Cancel_Buttom:
                dialog.dismiss();
                break;
            case R.id.Save_Buttom:
                if (TextUtils.isEmpty(claimPaymentOrdersEditText.getEditTextContent()) || amount== 0.0) {
                    claimPaymentOrdersEditText.setErrorMessage("Please enter the amount");
                    claimPaymentOrdersEditText.validate();
                } else if (amount <= remainingAmount) {
                    hideKeyboard(getActivity());
                    getAmount.getAmount(amount);
                    dismiss();
                } else {
                    claimPaymentOrdersEditText.setErrorMessage("The amount entered is greater than the remaining amount");
                    claimPaymentOrdersEditText.validate();
                }
                break;
        }
    }
}
