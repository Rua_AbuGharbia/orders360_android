package com.telephoenic.orders360.view.orders360view;

import java.util.ArrayList;

public interface Orders360InputValidator {
    void onError(ArrayList<String> errors);
}
