package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class UpDateApplicationsDialog extends DialogFragment {

    Unbinder unbinder;
    private Dialog dialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null) {
            if (getActivity() != null)
                dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.app_date_applications_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ButterKnife.bind(this, dialog);

        }
        return dialog;
    }

    @OnClick({R.id.Cancel_ImageView, R.id.UpDate_Button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Cancel_ImageView:
               dismiss();
                break;
            case R.id.UpDate_Button:
                String appPackageName = Objects.requireNonNull(getActivity()).getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        getActivity().finish();
        super.onDismiss(dialog);
    }
}