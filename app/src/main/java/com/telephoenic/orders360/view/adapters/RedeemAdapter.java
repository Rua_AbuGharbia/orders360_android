package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.RedeemModel;

import java.util.List;
import java.util.Locale;

public class RedeemAdapter extends ArrayAdapter<RedeemModel> {
    private Context mContext;
    private List<RedeemModel> data;
    private View.OnClickListener onClickListener;

    public static Locale sysLocale;

    public RedeemAdapter(Context context, int textViewResourceId, List<RedeemModel> objects , View.OnClickListener onClickListener ) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
        this.onClickListener = onClickListener;
        sysLocale = mContext.getResources().getConfiguration().locale;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
//        if (convertView == null) {
            row = LayoutInflater.from(mContext).inflate(R.layout.item_redeem, null);

            final TextView vendorName =  row.findViewById(R.id.text_vendor_name);
            final TextView redeemPoints =  row.findViewById(R.id.text_redeem_points);
            final Button redeemButton =  row.findViewById(R.id.button_redeem);


        vendorName.setTag(position);
        redeemPoints.setTag(position);
        redeemButton.setTag(position);

        vendorName.setOnClickListener(onClickListener);
        redeemPoints.setOnClickListener(onClickListener);
        redeemButton.setOnClickListener(onClickListener);


        redeemPoints.setText(data.get(position).getTotalPoints()+"");
        vendorName.setText(data.get(position).getVendorName());


        return row;
    }

    @Override
    public RedeemModel getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
