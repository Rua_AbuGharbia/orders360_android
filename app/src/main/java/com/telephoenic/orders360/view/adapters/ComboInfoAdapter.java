package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ComboInfoAdapter extends RecyclerView.Adapter<ComboInfoAdapter.MyViewHolder> {
    private Context mContext;
    private List<ComboInfoWrapper> data;
    View.OnClickListener clickListener;
    private String signature;

    public static Locale sysLocale;

    private boolean isChanged = true;

    public ComboInfoAdapter(Context context, int textViewResourceId, List<ComboInfoWrapper> objects, View.OnClickListener clickListener,String signature) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        this.signature = signature;

    }

    public ComboInfoAdapter(Context context, int textViewResourceId, List<ComboInfoWrapper> objects,String signature) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        this.signature =signature;
      //  sysLocale = mContext.getResources().getConfiguration().locale;

    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_combo_info, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ComboInfoWrapper comboInfoWrapper = data.get(position);

         /*if(position%2 == 0){
            holder.layout.setBackgroundColor(mContext.getResources().getColor(R.color.odd_background));
        }*/

        holder.setIsRecyclable(false);

        holder.name.setText(comboInfoWrapper.getProduct().getName());
        holder.alterName.setText(comboInfoWrapper.getProduct().getAlternativeName());

        // holder.price.setText(String.format("%.2f", comboInfoWrapper.getPrice()));
      //  holder.oldPrice.setText((float) Math.round(comboInfoWrapper.getPrice() * 100) / 100 + " " + AppConstants.CURRENCY);
        holder.oldPrice.setText(String.format(" %s %s", AppUtils.amountFormat(comboInfoWrapper.getPrice()), AppConstants.CURRENCY));

       // holder.price.setText((float) Math.round(comboInfoWrapper.getProduct().getPrice() * 100) / 100 + " " + AppConstants.CURRENCY);
        holder.price.setText(String.format(" %s %s", AppUtils.amountFormat(comboInfoWrapper.getProduct().getPrice()), AppConstants.CURRENCY));

        holder.quntity.setText("/ " + comboInfoWrapper.getQuantity() + "");
        if (comboInfoWrapper.getDiscountAmount() != null) {
            holder.discount.setText(comboInfoWrapper.getDiscountAmount() + "%");
        } else {
            holder.discount.setText("0.0");
        }

        SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy", Locale.ENGLISH);

        if (comboInfoWrapper.getProduct().getExpiryDate() != null) {
            String expiryDateString = formatter.format(new Date(Long.valueOf(comboInfoWrapper.getProduct().getExpiryDate())));
            holder.expiryDate.setText(expiryDateString);
        }


        try {
            //TODO set Image from server
            String imagesName = comboInfoWrapper.getProduct().getImagesName();

            if (imagesName != null) {
                if (imagesName.contains(";")) {
                    imagesName = imagesName.split(";")[0];
                }
                Integer imageID = comboInfoWrapper.getProduct().getId();


                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                        // .dontTransform()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                        .signature(new StringSignature(String.valueOf(signature)))
                                        .error(R.drawable.place_holder_order)
                                        .into(holder.productImage);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {


                                return false;
                            }
                        })
                        //  .crossFade(800)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(holder.productImage);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView name, alterName, price, quntity, discount, oldPrice, expiryDate;
        final ImageView productImage;
        final ConstraintLayout layout;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.text_product_name);
            alterName = view.findViewById(R.id.text_alter_name);
            productImage = view.findViewById(R.id.image_product);
            price = view.findViewById(R.id.text_product_price);
            oldPrice = view.findViewById(R.id.text_product_old_price);
            expiryDate = view.findViewById(R.id.textView_expiryDate);
            quntity = view.findViewById(R.id.text_product_quntity);
            discount = view.findViewById(R.id.text_product_discount);
            layout = view.findViewById(R.id.layout);
        }
    }
}
