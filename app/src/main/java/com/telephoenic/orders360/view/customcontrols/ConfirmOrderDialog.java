package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog.StatusClick.CANCEL;
import static com.telephoenic.orders360.view.customcontrols.ConfirmOrderDialog.StatusClick.OKAY;

public class ConfirmOrderDialog extends DialogFragment {
    public static final String MASSAGE = " massage";
    @BindView(R.id.MassageTitle_Dialog_TextView)
    TextView massageTitleDialogTextView;
    private Dialog dialog;
    private onClickStatus onClickStatus;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.confirm_order_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ButterKnife.bind(this, dialog);
            if (getArguments() != null) {
                massageTitleDialogTextView.setText(getArguments().getString(MASSAGE));
            }
        }
        return dialog;
    }


    @OnClick({R.id.Cancel_TextView, R.id.OK_TextView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Cancel_TextView:
                onClickStatus.getStatusClick(CANCEL);
                dismiss();
                break;
            case R.id.OK_TextView:
                onClickStatus.getStatusClick(OKAY);
                dismiss();
                break;
        }
    }

    public interface onClickStatus {
        void getStatusClick(int Status);
    }

    @IntDef({CANCEL, OKAY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface StatusClick {
        int CANCEL = 0;
        int OKAY = 1;
    }

    public onClickStatus onClickStatus(onClickStatus onClickStatus) {
        return this.onClickStatus = onClickStatus;
    }
}
