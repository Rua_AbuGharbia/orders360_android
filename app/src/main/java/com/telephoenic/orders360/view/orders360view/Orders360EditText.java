package com.telephoenic.orders360.view.orders360view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.MainFragmentCategories;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.List;

import static com.telephoenic.orders360.view.orders360view.Orders360EditText.validStatus.LENGTH;
import static com.telephoenic.orders360.view.orders360view.Orders360EditText.validStatus.NOT_NULL;

public class Orders360EditText extends Orders360InputView implements TextWatcher {

    private static final int TEXT_TYPE = 1;
    private static final int NUMBER_TYPE = 2;
    private static final int TEXT_PASSWORD = 3;
    private static final int PHONE_TYPE = 4;
    private static final int TEXT_MULTILINE = 5;
    private static final int NUMBER_DECIMAL = 6;
    private static final int MAX_LINE = 7;

    private static final int GRAVITY_CENTER = 1;
    private static final int GRAVITY_START = 2;
    private static final int GRAVITY_END = 3;

    private static final int VIEW_StART = 1;
    private static final int CENTER = 2;


    private ImageView endIconImageView;
    private TextView titleTextView;
    private EditText contentEditText;
    private TextView errorView;
    private boolean hasError;
    private boolean hasTitle;
    private boolean hasIcon;
    private String title;
    private String errorMessage;
    private String hint;
    private int endIcon;
    private int minLines;
    private int marginStart;
    private int marginEnd;
    private int inputType;
    private int gravity;
    private int maxLength;
    private int backGround;
    private int textColor;
    private int textAlignment;
    private int maxLine;

    private AddOnTextChangeListener addOnTextChangeListener;

    public Orders360EditText(@NonNull Context context) {
        super(context);
        init(null);
    }

    public Orders360EditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public Orders360EditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.orders360_edit_text, this);
        endIconImageView = contentView.findViewById(R.id.EndIcon_Order360EditText_ImageView);
        titleTextView = contentView.findViewById(R.id.Title_Orders360EditText_TextView);
        errorView = contentView.findViewById(R.id.MassageError_Order360EditText_View);
        contentEditText = contentView.findViewById(R.id.Content_Orders360EditText_EditText);

        contentEditText.addTextChangedListener(this);
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.Orders360EditText);
        hasError = ta.getBoolean(R.styleable.Orders360EditText_ed_has_error, true);
        hasTitle = ta.getBoolean(R.styleable.Orders360EditText_ed_has_title, true);
        hasIcon = ta.getBoolean(R.styleable.Orders360EditText_ed_has_icon, false);
        title = ta.getString(R.styleable.Orders360EditText_ed_title);
        errorMessage = ta.getString(R.styleable.Orders360EditText_ed_error_message);
        hint = ta.getString(R.styleable.Orders360EditText_ed_hint);
        endIcon = ta.getResourceId(R.styleable.Orders360EditText_ed_end_icon, -1);
        minLines = ta.getInt(R.styleable.Orders360EditText_ed_min_lines, 1);
        maxLength = ta.getInt(R.styleable.Orders360EditText_ed_max_length, 0);
        marginStart = ta.getDimensionPixelSize(R.styleable.Orders360EditText_ed_margin_start, -1);
        marginEnd = ta.getDimensionPixelSize(R.styleable.Orders360EditText_ed_margin_end, -1);
        isRequired = ta.getBoolean(R.styleable.Orders360EditText_ed_required, true);
        inputType = ta.getInteger(R.styleable.Orders360EditText_ed_input_type, 1);
        backGround = ta.getResourceId(R.styleable.Orders360EditText_ed_background, R.drawable.rounded_edittext_shape);
        textColor = ta.getResourceId(R.styleable.Orders360EditText_ed_text_color, R.color.default_blue_light);
        gravity = ta.getInt(R.styleable.Orders360EditText_ed_gravity, GRAVITY_START);
        textAlignment = ta.getInt(R.styleable.Orders360EditText_ed_text_alignment, VIEW_StART);
        maxLine = ta.getInt(R.styleable.Orders360EditText_ed_max_line, 0);


        switch (inputType) {
            case TEXT_TYPE:
                contentEditText.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case NUMBER_TYPE:
                contentEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TEXT_PASSWORD:
                contentEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                contentEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                break;
            case PHONE_TYPE:
                contentEditText.setInputType(InputType.TYPE_CLASS_PHONE);
                break;
            case TEXT_MULTILINE:
                contentEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                contentEditText.setSingleLine(false);
                contentEditText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                break;
            case NUMBER_DECIMAL:
                contentEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                break;

        }
        if (maxLength > 0) {
            InputFilter[] filterArray = new InputFilter[1];
            filterArray[0] = new InputFilter.LengthFilter(maxLength);
            contentEditText.setFilters(filterArray);
        }
        if (maxLine > 0) {
            contentEditText.setMaxLines(minLines);
        }

        errorView.setVisibility(hasError ? GONE : GONE);
        titleTextView.setVisibility(hasTitle ? VISIBLE : GONE);
        endIconImageView.setVisibility(hasIcon ? VISIBLE : GONE);
        contentEditText.setMinLines(minLines);
        contentEditText.setBackgroundResource(backGround);
        contentEditText.setTextColor(getResources().getColor(textColor));
        if (endIcon != -1)
            endIconImageView.setImageResource(endIcon);

        if (hasTitle) {
            titleTextView.setText(title);
        }
        if (hint != null)
            contentEditText.setHint(hint);

        switch (gravity) {
            case GRAVITY_CENTER:
                contentEditText.setGravity(Gravity.CENTER);
                break;
            case GRAVITY_START:
                contentEditText.setGravity(Gravity.START);
                break;
            case GRAVITY_END:
                contentEditText.setGravity(Gravity.END);
                break;
        }
        switch (textAlignment) {
            case VIEW_StART:
                contentEditText.setTextAlignment(TEXT_ALIGNMENT_VIEW_START);
                break;
            case CENTER:
                contentEditText.setTextAlignment(TEXT_ALIGNMENT_CENTER);
                break;

        }
        setupLayoutMargins();
        ta.recycle();
    }

    private void setupLayoutMargins() {
        RelativeLayout.LayoutParams clickableParams = (RelativeLayout.LayoutParams) contentEditText.getLayoutParams();
        RelativeLayout.LayoutParams titleParams = new RelativeLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);

        if (marginStart != -1) {
            clickableParams.setMarginStart(marginStart);
            int standardMarginStart = 25;
            titleParams.setMarginStart(marginStart + standardMarginStart);
            contentEditText.setLayoutParams(clickableParams);
            titleTextView.setLayoutParams(titleParams);
        }

        if (marginEnd != -1) {
            clickableParams.setMarginEnd(marginEnd);
            titleParams.setMarginEnd(marginEnd);
            contentEditText.setLayoutParams(clickableParams);
            titleTextView.setLayoutParams(titleParams);
        }
    }

    @Override
    public void validate() {
        if (isRequired) {
            if (!isAnimationRunning)
                endIconErrorAnimation();
        }
    }

    @Override
    public boolean validate2(List<Integer> validationList, Field[] fields) {
        for (final Field field : fields) {
            Annotation annotation = field.getAnnotation(Validation.class);
            if (annotation != null) {
                Validation validation = (Validation) annotation;
                MainFragmentCategories[] v = validation.validation();
                for (int i = 0; i < v.length; i++) {
                    Log.e("validation", validation.validation().length + "");
                }
            }
        }
        if (validationList != null) {
            switch (validationList.get(0)) {
                case NOT_NULL:
                    if (TextUtils.isEmpty(getEditTextContent())) {
                        validate();
                        return true;
                    } else {
                        return false;
                    }
            }
        }
        return false;
    }

    private void endIconErrorAnimation() {
        animateIcon(endIconImageView, errorView, true, hasIcon, endIcon);
    }

    public void setText(String text) {
        contentEditText.setText(text);
    }

    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    public void setTitle(int titleId) {
        titleTextView.setText(titleId);
    }

    public EditText getContentEditText() {

        return contentEditText;
    }

    public void setEditTextEnable(boolean enable) {
        contentEditText.setEnabled(enable);
    }

    public void setEditTextColor(int color) {
        contentEditText.setTextColor(getResources().getColor(color));
    }

    public void setIcon(int icon) {
        endIcon = icon;
        endIconImageView.setVisibility(VISIBLE);
        endIconImageView.setImageResource(icon);
    }

    @Override
    public boolean isValid() {
        return !TextUtils.isEmpty(contentEditText.getText().toString());
    }

    public String getEditTextContent() {
        return contentEditText.getText().toString();
    }

    public void addOnTextChangeListener(AddOnTextChangeListener addOnTextChangeListener) {
        this.addOnTextChangeListener = addOnTextChangeListener;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if (hasError)
            animateIcon(endIconImageView, errorView, false, hasIcon, endIcon);

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (addOnTextChangeListener != null) {
            addOnTextChangeListener.onTextChange(charSequence.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public interface AddOnTextChangeListener {
        void onTextChange(String data);
    }

    @IntDef({NOT_NULL, LENGTH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface validStatus {
        int NOT_NULL = 0;
        int LENGTH = 1;
        int START_WITH_05 = 2;
        int MIN_LENGTH = 3;
    }
}
