package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.MoreItemChild;

import java.util.List;

public class MoreChildAdapter
        extends RecyclerView.Adapter<MoreChildAdapter.ViewHolder> {

    private List<MoreItemChild> mMoreListItems;
    private Context mContext;
    private OnItemClickListener mClickListener;

    public MoreChildAdapter(List<MoreItemChild> mMoreListItems, Context mContext, OnItemClickListener mClickListener) {
        this.mMoreListItems = mMoreListItems;
        this.mContext = mContext;
        this.mClickListener = mClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.more_child_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        MoreItemChild listItem = mMoreListItems.get(position);
        holder.moreTextView.setText(listItem.getTitle());
        if (position == mMoreListItems.size() - 1) {
            holder.line.setVisibility(View.GONE);
        }
        holder.moreImageView.setImageResource(listItem.getIcon());
    }

    @Override
    public int getItemCount() {
        return mMoreListItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView moreImageView;
        private TextView moreTextView;
        private View line;

        ViewHolder(View itemView) {
            super(itemView);
            moreImageView = itemView.findViewById(R.id.menu_list_item_imageView);
            moreTextView = itemView.findViewById(R.id.menu_list_item_textView);
            line = itemView.findViewById(R.id.line_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(mMoreListItems.get(getAdapterPosition()), getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(MoreItemChild listItem, int position);
    }
}
