package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.JsonSyntaxException;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.basemodel.typedef.MaxQtyItem;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.customcontrols.LoadingDialog;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.fragments.BlankFragment;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.AddToCartModel;
import com.telephoenic.orders360.controller.server.model.CommissionQtyToValueTransient;
import com.telephoenic.orders360.controller.server.model.CommissionRequest;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.server.model.Promotion;
import com.telephoenic.orders360.controller.server.model.PromotionAttribute;
import com.telephoenic.orders360.controller.server.model.PromotionItem;
import com.telephoenic.orders360.controller.server.model.PromotionItemDetails;
import com.telephoenic.orders360.controller.server.model.PromotionValueAttribute;
import com.telephoenic.orders360.controller.server.model.PromotionValueQuantity;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.JsonUtils;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.adapters.PromotionAdapter;
import com.telephoenic.orders360.viewmodel.CartViewModel;
import com.telephoenic.orders360.viewmodel.PromotionViewModel;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;


@SuppressLint("ValidFragment")
public class CommissionDialog extends BlankFragment implements  DialogUtils.OnDialogClickListener
        ,ImageController {

    private Context mContext;
    private ImageView productImage;
    private TextView productName;
    private TextView productPrice;
    private TextView numOfItems, numOfItemsTitle, totalItems, totalItemsTitle;
    private TextView numOfFreeItems, numOfFreeItemsTitle;
    private TextView totalAmount, totalAmountTitle;
    private TextView startDate, endDate, endDateTitle;
    private ConstraintLayout showAddQtyConstraintLayout;
    private ImageView imageMinus, imagePlus;
    private EditText countEditText;
    private TextView totalPriceText;
    private CardView cardView;

    //private RecyclerView promotionRecycler;
    private LinearLayout promotionLayout;
    private ConstraintLayout otherLayout;

    private Button confirmButton;
    private ImageView imageClose;
    int indexClick = -1;

    private String posId;
    private String type;
    private ProductWrapper productWrapper;

    private String selectedCommission = "";
    private Object commissionObject;

    //List<CommissionResponse> commissionResponseList ;
    List<PromotionValueQuantity> commissionResponseListQ;
    List<PromotionValueAttribute> commissionResponseListA;
    private PromotionAdapter promotionAdapter;
    /*List<CommissionQtyToValueTransient> qtyToValueTransientList = new ArrayList<>();
	List<CommissionQtyToValueTransient> commissionList ;*/

    List<Object> qtyToValueTransientList = new ArrayList<>();
    //List<Object> commissionList ;

    private List<PromotionValueQuantity> commissionListQantity;
    private List<PromotionValueAttribute> commissionListAttribute;

    PosProfileWrapper posProfileWrapper;
    private List<String> privilegeList;
    private int count = 0;

    private Long allItems = 0L, items = 0L, freeItems = 0L;

    private double sumPrice = 0.0;

    private boolean otherFlag = true;
    private boolean addLayout = false;
    private boolean isEnabled = false;
    private CartViewModel cartViewModel;
    private List<AddToCartModel> sendCart = new ArrayList<>();

    private EditText editTextItems, editTextFreeItems;
    private TextView textViewOp, textViewFree;
    private Long selectId;
    private PromotionItem oldPromotionItem;
    //	private ScrollView scrollView ;
    private PromotionViewModel promotionViewModel;
//    private String myService = "";

    @SuppressLint("ValidFragment")
    public CommissionDialog(Context context, String posId, ProductWrapper productWrapper, String type) {
        this.posId = posId;
        this.type = type;
        this.productWrapper = productWrapper;
        mContext = context;
        promotionViewModel = new PromotionViewModel();
        posProfileWrapper = AppUtils.getProfile(context);
        if (productWrapper.getCommissionListQantity() != null && productWrapper.getCommissionListQantity().size() > 0) {
            commissionListQantity = productWrapper.getCommissionListQantity();
            Log.e("SIZE", commissionListQantity.size() + "");
        } else {
            commissionListQantity = new ArrayList<>();
        }

        if (productWrapper.getCommissionListAttribute() != null && productWrapper.getCommissionListAttribute().size() > 0) {
            commissionListAttribute = productWrapper.getCommissionListAttribute();
        } else {
            commissionListAttribute = new ArrayList<>();
        }
    }

    @SuppressLint("ValidFragment")
    public CommissionDialog(Context context, String posId, PromotionItem promotionItem, String type) {
        this.posId = posId;
        this.type = type;
        this.productWrapper = promotionItem.getProduct();
        this.oldPromotionItem = promotionItem;
        selectId = promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId();


        productWrapper.setCount(promotionItem.getProduct().getCount() + "");
        mContext = context;
        posProfileWrapper = AppUtils.getProfile(context);
        if (productWrapper.getCommissionListQantity() != null && productWrapper.getCommissionListQantity().size() > 0) {
            commissionListQantity = productWrapper.getCommissionListQantity();
            Log.e("SIZE", commissionListQantity.size() + "");
        } else {
            commissionListQantity = new ArrayList<>();
        }

        if (productWrapper.getCommissionListAttribute() != null && productWrapper.getCommissionListAttribute().size() > 0) {
            commissionListAttribute = productWrapper.getCommissionListAttribute();
        } else {
            commissionListAttribute = new ArrayList<>();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_commission, container, false);
        ButterKnife.bind(this, view);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        promotionViewModel = ViewModelProviders.of(this).get(PromotionViewModel.class);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppUtils.changeTitle(Objects.requireNonNull(getActivity()), getString(R.string.label_details));
        createComponents(view);
    }

    @SuppressLint("InflateParams")
    private void createComponents(View view) {

        imageMinus = view.findViewById(R.id.image_minus);
        imagePlus = view.findViewById(R.id.image_plus);
        countEditText = view.findViewById(R.id.purchases_num);
        totalPriceText = view.findViewById(R.id.text_total_price);
        showAddQtyConstraintLayout = view.findViewById(R.id.constraintLayout);
        productImage = view.findViewById(R.id.image_product);
        productName = view.findViewById(R.id.text_product_name);
        productPrice = view.findViewById(R.id.text_product_price);
        numOfItems = view.findViewById(R.id.text_items);
        totalItems = view.findViewById(R.id.text_total_items);
        numOfFreeItems = view.findViewById(R.id.text_free_items);
        cardView = view.findViewById(R.id.cardview2);

        numOfItemsTitle = view.findViewById(R.id.text_items_title);
        totalItemsTitle = view.findViewById(R.id.text_total_items_title);
        numOfFreeItemsTitle = view.findViewById(R.id.text_free_items_title);

        totalAmount = view.findViewById(R.id.text_total_amount);
        totalAmountTitle = view.findViewById(R.id.text_total_amount_title);
        //startDate = findViewById(R.id.text_start_date);
        endDate = view.findViewById(R.id.text_end_date);
        endDateTitle = view.findViewById(R.id.text_end_date_title);
        //promotionRecycler = findViewById(R.id.promotion_recyclerView);
        promotionLayout = view.findViewById(R.id.promotion_linear_layout);
        confirmButton = view.findViewById(R.id.btn_confirm);
        imageClose = view.findViewById(R.id.image_close);

        otherLayout = (ConstraintLayout) getLayoutInflater().inflate(R.layout.item_other_layout, null);
        editTextItems = otherLayout.findViewById(R.id.edit_text_items);
        editTextFreeItems = otherLayout.findViewById(R.id.edit_text_free_items);
        textViewOp = otherLayout.findViewById(R.id.textView3);
        textViewFree = otherLayout.findViewById(R.id.textView4);


        editTextItems.addTextChangedListener(textWatcher);
        editTextFreeItems.addTextChangedListener(textWatcher);

        if (posProfileWrapper != null) {
            privilegeList = posProfileWrapper.getUser().getPermissions();
        }

        for (String item : privilegeList) {
            switch (item) {
                case AppConstants.ROLE_PRODUCT_ADD:
                case AppConstants.ROLE_PROMOTION_ADD:
                case AppConstants.ROLE_COMBO_ADD:
                    isEnabled = true;
                    break;

                case AppConstants.ROLE_PRODUCT_VIEW:
                case AppConstants.ROLE_PROMOTION_VIEW:
                case AppConstants.ROLE_COMBO_VIEW:
                    isEnabled = false;
                    break;
            }
        }

        if (isEnabled) {
            confirmButton.setVisibility(View.VISIBLE);
            numOfItems.setVisibility(View.VISIBLE);
            totalItems.setVisibility(View.VISIBLE);
            numOfFreeItems.setVisibility(View.VISIBLE);
            numOfItemsTitle.setVisibility(View.VISIBLE);
            totalItemsTitle.setVisibility(View.VISIBLE);
            numOfFreeItemsTitle.setVisibility(View.VISIBLE);
            showAddQtyConstraintLayout.setVisibility(View.VISIBLE);

        } else {
            cardView.setVisibility(View.GONE);
            confirmButton.setVisibility(View.INVISIBLE);
            numOfItems.setVisibility(View.GONE);
            totalItems.setVisibility(View.GONE);
            numOfFreeItems.setVisibility(View.GONE);
            numOfItemsTitle.setVisibility(View.GONE);
            totalItemsTitle.setVisibility(View.GONE);
            numOfFreeItemsTitle.setVisibility(View.GONE);
            totalAmount.setVisibility(View.GONE);
            totalAmountTitle.setVisibility(View.GONE);
            showAddQtyConstraintLayout.setVisibility(View.GONE);
        }

        if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(mContext))) {
            textViewOp.setText("-");
            textViewFree.setVisibility(View.GONE);
            endDateTitle.setVisibility(View.GONE);
            endDate.setVisibility(View.GONE);
            numOfItems.setVisibility(View.GONE);
            totalItems.setVisibility(View.GONE);
            numOfFreeItems.setVisibility(View.GONE);
            numOfItemsTitle.setVisibility(View.GONE);
            totalItemsTitle.setVisibility(View.GONE);
            numOfFreeItemsTitle.setVisibility(View.GONE);
            showAddQtyConstraintLayout.setVisibility(View.GONE);
            totalAmount.setVisibility(View.VISIBLE);
            totalAmountTitle.setVisibility(View.VISIBLE);
        }

        productName.setText(productWrapper.getName());
        productPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice()) + " " + AppConstants.CURRENCY);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        if (productWrapper.getEndDate() != null) {
            String endDateString = formatter.format(new Date(Long.valueOf(productWrapper.getEndDate())));
            endDate.setText(endDateString);
        }

        try {
            int imageID = productWrapper.getId();
            String prodImage = productWrapper.getImagesName();

            if (prodImage != null) {
                if (prodImage.contains(";")) {
                    prodImage = prodImage.split(";")[0];
                }
                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + prodImage)
                        .crossFade(800)
                        .dontTransform()
                        .error(R.drawable.place_holder_blue)
                        .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                        .into(productImage);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        CommissionRequest commissionRequest = new CommissionRequest();
        commissionRequest.setPosId(posId);
        commissionRequest.setGroupId(String.valueOf(posProfileWrapper.getUser().getPosGroup()));
        commissionRequest.setProdId(productWrapper.getId() + "");

        sendRequest();

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prepareToAddToCart();

                if (addToCart(productWrapper)) {
                    AppUtils.showAlertToast(mContext.getString(R.string.fragment_promotion_add_to_cart_message), mContext);
                    AppUtils.updateNotifactionOrder((Activity) mContext);
                    getActivity().onBackPressed();

                }
            }
        });


        imageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (indexClick != -1) {
                    if (TextUtils.isEmpty(countEditText.getText())) {
                        count = 1;
                    } else {
                        count = Integer.parseInt(countEditText.getText().toString());

                    }
                    if (count > 1) {
                        countEditText.setText(String.valueOf(--count));
                        setSum();
                    }

                }
            }
        });

        imagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (indexClick != -1) {

                    if (!TextUtils.isEmpty(countEditText.getText())) {
                        count = Integer.parseInt(countEditText.getText().toString());

                    } else {
                        count = 1;
                    }
                    if (count < MaxQtyItem.MAX_QTY) {
                        countEditText.setText(String.valueOf(++count));
                        setSum();

                    }
                }
            }
        });

    }


    private boolean addToCart(ProductWrapper productWrapper) {
        ProductWrapper newProductWrapper = new ProductWrapper();
        newProductWrapper.setId(productWrapper.getId());
        newProductWrapper.setName(productWrapper.getName());
        newProductWrapper.setAlternativeName(productWrapper.getAlternativeName());
        newProductWrapper.setPrice(productWrapper.getPrice());
        newProductWrapper.setImagesName(productWrapper.getImagesName());
        newProductWrapper.setDescription(productWrapper.getDescription());
        newProductWrapper.setLoyaltyPoint(productWrapper.getLoyaltyPoint());
        newProductWrapper.setHasPromotion(productWrapper.getHasPromotion());
        newProductWrapper.setStartDate(productWrapper.getStartDate());
        newProductWrapper.setEndDate(productWrapper.getEndDate());
        newProductWrapper.setExpiryDate(productWrapper.getExpiryDate());
        newProductWrapper.setVat(productWrapper.getVat());
        newProductWrapper.setRetailPrice(productWrapper.getRetailPrice());

        newProductWrapper.setCount(String.valueOf(allItems));
        newProductWrapper.setSelectedCommission(selectedCommission);
        newProductWrapper.setCommissionListAttribute(commissionListAttribute);
        newProductWrapper.setCommissionListQantity(commissionListQantity);

        if (newProductWrapper.getCount() == null || newProductWrapper.getCount().equals("0")) {
            AppUtils.showAlertToast(mContext.getString(R.string.fragment_purchase_warning_message2), mContext);
            return false;
        }

        if (AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(getContext()))) {
            List<PromotionValueQuantity> promotionList = newProductWrapper.getCommissionListQantity();
            if (promotionList != null && !promotionList.isEmpty()) {
                PromotionItem promotionItem = new PromotionItem();
                promotionItem.setPrice(newProductWrapper.getPrice());
                promotionItem.setExpiryDate(newProductWrapper.getExpiryDate());
                promotionItem.setVat(newProductWrapper.getVat());
                promotionItem.setProduct(newProductWrapper);
                List<PromotionItemDetails> promotionItemDetailsList = new ArrayList<>();
                Double productTotalAmount = 0.0;
                Long freeItems = 0L;
                Long totalQuantity = 0L;
                int promotionID = 0;
                for (PromotionValueQuantity obj : promotionList) {
                    Integer count = Integer.valueOf(obj.getCount());
                    productTotalAmount += obj.getQuantity(); //* count ;
                    PromotionItemDetails promotionItemDetails = new PromotionItemDetails();
                    promotionItemDetails.setPromotionValueId(obj.getId());
                    if (obj.getPromoitonTransient() != null) {
                        promotionID = obj.getPromoitonTransient().intValue();
                    }
                    promotionItem.setPromotion(new Promotion(promotionID));
                    Long quantityInt = obj.getQuantity();
                    promotionItemDetails.setQuantity(quantityInt.longValue());
                    promotionItemDetails.setOrderQuantity(Long.valueOf(count));
                    totalQuantity = totalQuantity + obj.getQuantity();
                    Long integer = obj.getFreeItem();
                    freeItems = freeItems + integer;
                    promotionItemDetails.setFreeItem(integer);
                    promotionItemDetails.setPromotionType(obj.getPromotionType());
                    promotionItemDetailsList.add(promotionItemDetails);
                }
                promotionItem.setPromotionItemDetailsList(promotionItemDetailsList);
                promotionItem.setOrderedQuantity(newProductWrapper.getCount() + "");
                productTotalAmount = count * newProductWrapper.getPrice() * totalQuantity;
                promotionItem.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
                sendCart = new ArrayList<>();

                if (AppConstants.EDIT_ORDER.equals(type)) {
                    if (!promotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId().equals(selectId)) {
                        if (SaveCart.getInstance().getPromotionCount(promotionItem) <= MaxQtyItem.MAX_QTY) {
                            SaveCart.getInstance().changeProductOrderedQuantity(promotionItem, oldPromotionItem, selectId);

                        } else {
                            oldPromotionItem.getProduct().setCommissionListQantity(null);
                            Toast.makeText(mContext, mContext.getString(R.string.you_have_exceeded_the_quantity_allowable), Toast.LENGTH_SHORT).show();
                            return false;
                        }

                    } else {
                        SaveCart.getInstance().changeProductOrderedQuantity(promotionItem, oldPromotionItem, selectId);
                    }
//
                } else if (AppConstants.NEW_ORDER.equals(type)) {
                    if (SaveCart.getInstance().getPromotionCount(promotionItem) <= MaxQtyItem.MAX_QTY) {
                        SaveCart.getInstance().addPromotionItem(promotionItem);
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.you_have_exceeded_the_quantity_allowable), Toast.LENGTH_SHORT).show();
                        return false;
                    }


                }
                VendorWrapper vendorWrapper = new VendorWrapper();
                vendorWrapper.setId(AppPrefs.getVendorID(getContext()));
                SaveCart.getInstance().getSelectVendorCart().setPosUser(posProfileWrapper.getUser());
                SaveCart.getInstance().getSelectVendorCart().setVendor(vendorWrapper);
                SaveCart.getInstance().prepearCartToSend(SaveCart.getInstance().getSelectVendorCart());
                sendCart.add(SaveCart.getInstance().getSelectVendorCart());

                LoadingDialog.showDialog(getContext());

                cartViewModel.addToCart(sendCart).observe(getActivity(), new ApiObserver<ResponseBody>() {
                    @Override
                    protected void noConnection() {
                        LoadingDialog.dismiss();
                    }

                    @Override
                    protected void onSuccess(ResponseBody data) {
                        LoadingDialog.dismiss();
                        AppUtils.showAlertToast(mContext.getString(R.string.fragment_promotion_add_to_cart_message), mContext);
                        AppUtils.updateNotifactionOrder((Activity) mContext);
                        getActivity().onBackPressed();
                    }

                    @Override
                    protected void onError(String error) {
                        LoadingDialog.dismiss();
                        Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();


                    }
                });

            }
        } else if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(getContext()))) {
            List<PromotionValueAttribute> promotionList = newProductWrapper.getCommissionListAttribute();
            if (promotionList != null && !promotionList.isEmpty()) {
                PromotionItem promotionItem = new PromotionItem();
                promotionItem.setPrice(newProductWrapper.getPrice());
                promotionItem.setExpiryDate(newProductWrapper.getExpiryDate());
                promotionItem.setVat(newProductWrapper.getVat());
                promotionItem.setProduct(newProductWrapper);
                List<PromotionItemDetails> promotionItemDetailsList = new ArrayList<>();

                double productTotalAmount = 0.0;
                Long freeItems = 0L;
                Long totalQuantity = 0L;
                int promotionID = 0;
                for (PromotionValueAttribute obj : promotionList) {
                    Integer count = Integer.valueOf(obj.getCount());
                    productTotalAmount += obj.getPromotionAttribute().getValue(); //* count ;
                    PromotionItemDetails promotionItemDetails = new PromotionItemDetails();
                    promotionItemDetails.setPromotionValueId(obj.getId());
                    if (obj.getPromoitonTransient() != null) {
                        promotionID = obj.getPromoitonTransient().intValue();
                    }
                    promotionItem.setPromotion(new Promotion(promotionID));
                    Long quantityInt = obj.getPromotionAttribute().getValue();
                    promotionItemDetails.setQuantity(quantityInt.longValue());
                    promotionItemDetails.setOrderQuantity(count.longValue());
                    totalQuantity = totalQuantity + obj.getPromotionAttribute().getValue();
                    Long integer = obj.getFreeItem();
                    freeItems = freeItems + integer;
                    promotionItemDetails.setFreeItem(integer);
                    promotionItemDetails.setPromotionType(obj.getPromotionType());
                    promotionItemDetailsList.add(promotionItemDetails);
                }

                promotionItem.setPromotionItemDetailsList(promotionItemDetailsList);
                // promotionItem.setQuantity(Long.valueOf(totalQuantity.longValue()));
                promotionItem.setFreeItems(Long.valueOf(freeItems.longValue()));
                promotionItem.setOrderedQuantity(newProductWrapper.getCount() + "");
                productTotalAmount = productTotalAmount * newProductWrapper.getPrice();
                promotionItem.setTotalAmount((double) Math.round(productTotalAmount * 100) / 100);
                sendCart = new ArrayList<>();
                if (AppConstants.EDIT_ORDER.equals(type)) {
                    SaveCart.getInstance().updatePromoitionAt(promotionItem, oldPromotionItem);
                } else if (AppConstants.NEW_ORDER.equals(type)) {
                    SaveCart.getInstance().setPromotiomAtrebute(promotionItem);
                }
                VendorWrapper vendorWrapper = new VendorWrapper();
                vendorWrapper.setId(AppPrefs.getVendorID(getContext()));
                SaveCart.getInstance().getSelectVendorCart().setPosUser(posProfileWrapper.getUser());
                SaveCart.getInstance().getSelectVendorCart().setVendor(vendorWrapper);
                SaveCart.getInstance().prepearCartToSend(SaveCart.getInstance().getSelectVendorCart());
                sendCart.add(SaveCart.getInstance().getSelectVendorCart());
                final boolean[] save = new boolean[1];
                save[0] = true;
                cartViewModel.addToCart(sendCart).observe(getActivity(), new ApiObserver<ResponseBody>() {
                    @Override
                    protected void noConnection() {
                        save[0] = false;
                    }

                    @Override
                    protected void onSuccess(ResponseBody data) {

                        save[0] = true;
                    }

                    @Override
                    protected void onError(String error) {
                        Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
                        save[0] = false;


                    }
                });
                return save[0];
            }
        }


        return false;
    }

    public Object getCommissionObject() {
        return commissionObject;
    }

    public void setCommissionObject(Object commissionObject) {
        this.commissionObject = commissionObject;
    }

    public String getSelectedCommission() {
        return selectedCommission;
    }

    public void setSelectedCommission(String selectedCommission) {
        this.selectedCommission = selectedCommission;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }


/*	public void fullSpinner(){
		commissionAdapter = new CommissionAdapter(mContext , R.layout.item_lookup,commissionListString);
		//commissionAdapter = new CommissionAdapter(mContext , R.layout.item_lookup,commissionResponseList);
		commissionSpinner.setAdapter(commissionAdapter);

	}*/


    private void fullQuantityData() {
        RadioGroup radioGroup = new RadioGroup(mContext);
        for (final PromotionValueQuantity promotionValueQuantity : commissionResponseListQ) {
            RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.item_radio_button, null);
            radioButton.setId(commissionResponseListQ.indexOf(promotionValueQuantity));
            radioButton.setText(promotionValueQuantity.getQuantity() + " + " + promotionValueQuantity.getFreeItem() + " " + mContext.getResources().getString(R.string.fragment_product_free));
            radioButton.setEnabled(isEnabled);
            if (radioButton.getId() == indexClick) {
                radioButton.setChecked(true);
            }

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!otherFlag) {
                        promotionLayout.removeView(otherLayout);
                        otherFlag = true;
                    }
                    if (count == 0) {
                        count = 1;
                        countEditText.setText(count + "");
                    }
                    setSum();
                }
            });
            radioGroup.addView(radioButton);
        }
        if (AppConstants.HAS_PROMOTION_OTHER) {
            RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.item_radio_button, null);
            radioButton.setId(commissionResponseListQ.size());
            radioButton.setText(mContext.getResources().getString(R.string.fragment_product_other));
            radioButton.setEnabled(isEnabled);
            radioGroup.addView(radioButton);
            if (indexClick == -1 && commissionListQantity != null && commissionListQantity.size() > 0) {
                radioButton.setChecked(true);
                editTextItems.setText(commissionListQantity.get(0).getQuantity() + "");
                editTextFreeItems.setText(commissionListQantity.get(0).getFreeItem() + "");
                otherFlag = false;
                addLayout = true;
            } else if (oldPromotionItem != null) {
                if (oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
                    radioButton.setChecked(true);
                    editTextItems.setText(oldPromotionItem.getPromotionItemDetailsList().get(0).getQuantity() + "");
                    editTextFreeItems.setText(oldPromotionItem.getPromotionItemDetailsList().get(0).getFreeItem() + "");
                    otherFlag = false;
                    addLayout = true;

                }
            }
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (otherFlag) {
                        promotionLayout.addView(otherLayout);
                        otherFlag = false;
                        if (count == 0) {
                            count = 1;
                            countEditText.setText(count + "");
                        }
                        setSum();
                    }
                }
            });

            promotionLayout.addView(radioGroup);
            if (addLayout) {
                promotionLayout.addView(otherLayout);
                addLayout = false;
                otherFlag = false;
            }

            setSum();
        } else {
            promotionLayout.addView(radioGroup);
            setSum();
        }


    }

    private void fullAttributeData() {
        RadioGroup radioGroup = new RadioGroup(mContext);
        for (final PromotionValueAttribute promotionAttribute : commissionResponseListA) {
            RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.item_radio_button, null);
            radioButton.setId(commissionResponseListA.indexOf(promotionAttribute));
            radioButton.setText(promotionAttribute.getPromotionAttribute().getAttribute() + " - " + promotionAttribute.getFreeItem());
            radioButton.setEnabled(isEnabled);
            if (radioButton.getId() == indexClick) {
                radioButton.setChecked(true);
            }
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!otherFlag) {
                        promotionLayout.removeView(otherLayout);
                        otherFlag = true;
                    }
                    setSumAttribute();
                }
            });
            radioGroup.addView(radioButton);
        }

        RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.item_radio_button, null);
        radioButton.setId(commissionResponseListA.size());
        radioButton.setText(mContext.getResources().getString(R.string.fragment_product_other));
        radioButton.setEnabled(isEnabled);
        radioGroup.addView(radioButton);
        if (indexClick == -1 && commissionListAttribute != null && commissionListAttribute.size() > 0) {
            radioButton.setChecked(true);
            editTextItems.setText(commissionListAttribute.get(0).getPromotionAttribute().getValue() + "");
            editTextFreeItems.setText(commissionListAttribute.get(0).getFreeItem() + "");
            otherFlag = false;
            addLayout = true;
        } else if (oldPromotionItem != null) {
            if (oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
                radioButton.setChecked(true);
                editTextItems.setText(oldPromotionItem.getPromotionItemDetailsList().get(0).getQuantity() + "");
                editTextFreeItems.setText(oldPromotionItem.getPromotionItemDetailsList().get(0).getFreeItem() + "");
                otherFlag = false;
                addLayout = true;

            }
        }
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otherFlag) {
                    // productWrapper.setCommissionListQantity(new ArrayList<PromotionValueQuantity>());
                    //  productWrapper.setCommissionListAttribute(new ArrayList<PromotionValueAttribute>());
                    promotionLayout.addView(otherLayout);
                    otherFlag = false;
                    setSumAttribute();
                }
            }
        });

        promotionLayout.addView(radioGroup);
        if (addLayout) {
            promotionLayout.addView(otherLayout);
            addLayout = false;
            otherFlag = false;
        }
        setSumAttribute();
    }


    private View.OnClickListener onPromotionClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            indexClick = (Integer) v.getTag();
            CommissionQtyToValueTransient promotionModel = (CommissionQtyToValueTransient) qtyToValueTransientList.get(indexClick);
            int promotionCount = 0;
            if (promotionModel.getCount() != null) {
                promotionCount = Integer.parseInt(promotionModel.getCount());
            }
            //commissionObject = qtyToValueTransientList.get(indexClick);
            switch (v.getId()) {
                case R.id.text_promotion_value:
				/*	if(qtyToValueTransientList.size()>0){
						selectedCommission = commissionListString.get(indexClick).getPromotionName();
						for(PromotionModel promotionModel1:commissionListString){
							promotionModel1.setSelected(false);
						}
						commissionListString.get(indexClick).setSelected(true);
						promotionAdapter.notifyDataSetChanged();
					}*/
                    break;
                case R.id.image_minus:
                    if (promotionCount > 0) {
                        promotionCount--;
                        promotionModel.setCount(promotionCount + "");
                        promotionAdapter.notifyDataSetChanged();
                        setSum();
                    }
                    break;
                case R.id.image_plus:
                    promotionCount++;
                    promotionModel.setCount(promotionCount + "");
                    promotionAdapter.notifyDataSetChanged();
                    setSum();
                    break;
            }
        }
    };

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(mContext))) {
			/*	if( productWrapper.getCommissionListQantity()!= null){
					return;
				} else {
                setSum();
            }*/
                setSum();
            } else if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(mContext))) {
				/*if( productWrapper.getCommissionListAttribute()!= null){
					return;
				} else {
					setSumAttribute();
				}*/
                setSumAttribute();
            }


        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void setSumAttribute() {
        sumPrice = 0.0;
        allItems = 0L;
        items = 0L;
        freeItems = 0L;
        indexClick = getIndex();
        if (indexClick < commissionResponseListA.size() && indexClick != -1) {
            PromotionValueAttribute promotion = commissionResponseListA.get(indexClick);
            sumPrice = productWrapper.getPrice() * promotion.getPromotionAttribute().getValue();
            allItems = promotion.getPromotionAttribute().getValue() + promotion.getFreeItem();
            items = promotion.getPromotionAttribute().getValue();
            freeItems = promotion.getFreeItem();
        } else if (!TextUtils.isEmpty(editTextItems.getText().toString().trim())) {
            if (!TextUtils.isEmpty(editTextFreeItems.getText().toString().trim())) {
                freeItems = Long.parseLong(editTextFreeItems.getText().toString());
            }
            items = Long.parseLong(editTextItems.getText().toString());
            allItems = items + freeItems;
            sumPrice = productWrapper.getPrice() * items;
        }
        numOfItems.setText(String.valueOf(items));
        totalItems.setText(String.valueOf(allItems));
        numOfFreeItems.setText(String.valueOf(freeItems));
        totalAmount.setText(String.format(Locale.ENGLISH, "%.2f", sumPrice) + " " + AppConstants.CURRENCY);
    }

    private void setSum() {
        sumPrice = 0.0;
        allItems = 0L;
        items = 0L;
        freeItems = 0L;
        indexClick = getIndex();
        if (indexClick < commissionResponseListQ.size() && indexClick != -1) {
            if (count == 0) {
                count = 1;
            }
            countEditText.setEnabled(true);
            PromotionValueQuantity promotion = commissionResponseListQ.get(indexClick);
            sumPrice = productWrapper.getPrice() * promotion.getQuantity() * count;
            allItems = (promotion.getQuantity() + promotion.getFreeItem()) * count;
            items = promotion.getQuantity() * count;
            freeItems = promotion.getFreeItem() * count;
        } else if (!TextUtils.isEmpty(editTextItems.getText().toString().trim())) {
            if (!TextUtils.isEmpty(editTextFreeItems.getText().toString().trim())) {
                freeItems = Long.parseLong(editTextFreeItems.getText().toString());
            }
            countEditText.setEnabled(true);
            items = Long.parseLong(editTextItems.getText().toString());
            allItems = (items + freeItems) * count;
            sumPrice = productWrapper.getPrice() * items * count;
            items = Long.parseLong(editTextItems.getText().toString()) * count;

            if (!TextUtils.isEmpty(editTextFreeItems.getText().toString().trim())) {
                freeItems = Long.parseLong(editTextFreeItems.getText().toString()) * count;

            }
        }
        numOfItems.setText(String.valueOf(items));
        totalItems.setText(String.valueOf(allItems));
        numOfFreeItems.setText(String.valueOf(freeItems));
        totalAmount.setText(String.format(Locale.ENGLISH, "%s %s", String.format(Locale.ENGLISH, "%.2f", sumPrice), AppConstants.CURRENCY));
        totalPriceText.setText(String.format(Locale.ENGLISH, "%s %s", String.format(Locale.ENGLISH, "%.2f", sumPrice), AppConstants.CURRENCY));


    }

    private void sendRequest(){
//        myService = AppConstants.SERVICE_GET_PROMOTION;
        promotionViewModel.getPromotion(String.valueOf(productWrapper.getId()),String.valueOf(posProfileWrapper.getUser().getPosGroup()))
                .observe(this, new ApiObserver<ResponseBody>() {
                    @Override
                    protected void noConnection() {
                        noConnectionInternetDialog();
                    }

                    @Override
                    protected void onSuccess(ResponseBody data) {
                        try {
                            if (AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(mContext))) {
                                commissionResponseListQ = JsonUtils.getAsJsonArray(data.string(), PromotionValueQuantity.class);
                                if (commissionListQantity.size() > 0) {
                                    for (PromotionValueQuantity commision : commissionResponseListQ) {
                                        for (PromotionValueQuantity value : commissionListQantity) {
                                            if (commision.getId().equals(value.getId()) || value.getId() == -1) {
                                                indexClick = commissionResponseListQ.indexOf(commision);
                                                if (oldPromotionItem != null) {
                                                    countEditText.setText(oldPromotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity() + "");
                                                    count = (int) Integer.parseInt(String.valueOf(oldPromotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity()));
                                                    Log.e("Count1", count + "");

                                                } else {
                                                    countEditText.setText(value.getCount());
                                                    count = Integer.parseInt(value.getCount());

                                                }

                                            }
                                        }
                                    }
                                } else if (oldPromotionItem != null) {
                                    if (oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() != null) {
                                        for (PromotionValueQuantity commision : commissionResponseListQ) {
                                            if (commision.getId().equals(oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId()) || oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() == -1) {
                                                indexClick = commissionResponseListQ.indexOf(commision);
                                                countEditText.setText(oldPromotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity() + "");
                                                count = (int) Integer.parseInt(String.valueOf(oldPromotionItem.getPromotionItemDetailsList().get(0).getOrderQuantity()));

                                            }
                                        }
                                    }
                                }
                                fullQuantityData();
                            } else if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(mContext))) {
                                commissionResponseListA = JsonUtils.getAsJsonArray(data.string(), PromotionValueAttribute.class);
                                if (commissionListAttribute.size() > 0) {
                                    for (PromotionValueAttribute commision : commissionResponseListA) {
                                        for (PromotionValueAttribute value : commissionListAttribute) {
                                            if (commision.getId().equals(value.getId())) {
                                                indexClick = commissionResponseListA.indexOf(commision);
                                                break;
                                            }
                                        }
                                    }
                                } else if (oldPromotionItem != null) {
                                    if (oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId() != null) {
                                        for (PromotionValueAttribute commision : commissionResponseListA) {
                                            if (commision.getId().equals(oldPromotionItem.getPromotionItemDetailsList().get(0).getPromotionValueId())) {
                                                indexClick = commissionResponseListA.indexOf(commision);
                                                break;
                                            }
                                        }
                                    }
                                }
                                fullAttributeData();
                            }

                            countEditText.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void afterTextChanged(Editable s) {
                                    // TODO Auto-generated method stub
                                }

                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                    // TODO Auto-generated method stub
                                }

                                @SuppressLint("SetTextI18n")
                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count2) {
                                    if (s.toString().equals("") || s.toString().startsWith("0") && s.toString().endsWith("0")) {
                                        countEditText.setHint("1");
                                        count = 1;

                                    } else {
                                        count = Integer.parseInt(countEditText.getText().toString());
                                    }
                                    setSum();
                                }
                            });

                        }catch (Exception ee){
                            Log.e("Promotion", "onSuccess: ",ee );

                        }


                    }

                    @Override
                    protected void onError(String error) {

                    }
                });
    }

    private void prepareToAddToCart() {
        indexClick = getIndex();
        if (AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(mContext))) {
            if (commissionResponseListQ != null) {
                if (indexClick != -1 && indexClick < commissionResponseListQ.size()) {
                    PromotionValueQuantity promotionValueQuantity = commissionResponseListQ.get(indexClick);
                    promotionValueQuantity.setPromotionType(AppConstants.PROMOTION_TYPE_SYSTEM);
                    commissionObject = promotionValueQuantity;
                    promotionValueQuantity.setCount(count + "");
                    commissionListQantity.clear();
                    commissionListQantity.add(promotionValueQuantity);
                    setSum();
                } else {
                    if (!TextUtils.isEmpty(editTextItems.getText().toString().trim())) {
                        PromotionValueQuantity promotionValueQuantity = new PromotionValueQuantity();
                        promotionValueQuantity.setQuantity(Long.parseLong(editTextItems.getText().toString().trim()));
                        promotionValueQuantity.setCount(count + "");
                        if (!TextUtils.isEmpty(editTextFreeItems.getText().toString().trim())) {
                            promotionValueQuantity.setFreeItem(Long.parseLong(editTextFreeItems.getText().toString()));
                        } else {
                            promotionValueQuantity.setFreeItem(0L);
                        }
                        promotionValueQuantity.setPromoitonTransient(commissionResponseListQ.get(0).getPromoitonTransient());
                        promotionValueQuantity.setPromotionType(AppConstants.PROMOTION_TYPE_MANUAL);
                        promotionValueQuantity.setId(-1L);
                        commissionObject = promotionValueQuantity;
                        commissionListQantity.clear();
                        commissionListQantity.add(promotionValueQuantity);
                        setSum();
                    }
                }
            }
        } else if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(mContext))) {
            if (commissionResponseListA != null) {
                if (indexClick != -1 && indexClick < commissionResponseListA.size()) {
                    PromotionValueAttribute promotionAttribute = commissionResponseListA.get(indexClick);
                    promotionAttribute.setPromotionType(AppConstants.PROMOTION_TYPE_SYSTEM);
                    commissionObject = promotionAttribute;
                    promotionAttribute.setCount("1");
                    commissionListAttribute.clear();
                    commissionListAttribute.add(promotionAttribute);
                    setSumAttribute();
                } else {
                    if (!TextUtils.isEmpty(editTextItems.getText().toString().trim())) {
                        PromotionValueAttribute promotionValueAttribute = new PromotionValueAttribute();
                        promotionValueAttribute.setPromotionAttribute(new PromotionAttribute(Long.parseLong(editTextItems.getText().toString().trim())));
                        promotionValueAttribute.setCount("1");
                        if (!TextUtils.isEmpty(editTextFreeItems.getText().toString().trim())) {
                            promotionValueAttribute.setFreeItem(Long.parseLong(editTextFreeItems.getText().toString()));
                        } else {
                            promotionValueAttribute.setFreeItem(0L);
                        }
                        promotionValueAttribute.setPromotionType(AppConstants.PROMOTION_TYPE_MANUAL);
                        promotionValueAttribute.setId(-1L);
                        commissionObject = promotionValueAttribute;
                        commissionListAttribute.clear();
                        commissionListAttribute.add(promotionValueAttribute);
                        setSumAttribute();
                    }
                }
            }

        }
    }

    private int getIndex() {
        ArrayList<RadioGroup> radioGroups = AppUtils.getAllRadioGroup(promotionLayout);
        if (radioGroups != null && radioGroups.size() > 0) {
            RadioGroup radioGroup = radioGroups.get(0);
            return radioGroup.getCheckedRadioButtonId();
        }
        return -1;
    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            sendRequest();
        }*/
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }
}
