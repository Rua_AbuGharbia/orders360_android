package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.controller.utils.LanguageManager;
import com.telephoenic.orders360.controller.utils.SharedPreferenceManager;
import com.telephoenic.orders360.controller.viewholder.FilterViewHolder;

import java.util.List;

public class FilterExpandAdapter extends RecyclerView.Adapter<FilterViewHolder> {
    private Context context;
    private List<CategoryNodeWrapper> data;
    private FilterExpandAdapter.OnItemClickListener onItemClickListener;

    public FilterExpandAdapter(Context context, List<CategoryNodeWrapper> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_filter_category, parent, false);
        return new FilterViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.nameRowFilterTextView.setText(data.get(position).getName());
        if (data.get(position).getSelect() != null && data.get(position).getSelect()) {
            holder.nameRowFilterTextView.setTextColor(context.getResources().getColor(R.color.default_blue_light));
        } else {
            holder.nameRowFilterTextView.setTextColor(context.getResources().getColor(R.color.text_color1));
        }
        if (SharedPreferenceManager.getLanguage(context).equals(LanguageManager.ENGLISH)) {
            holder.nameRowFilterTextView.setPadding(data.get(position).getRootPadding(), 0, 0, 0);
        } else {
            holder.nameRowFilterTextView.setPadding(0, 0, data.get(position).getRootPadding(), 0);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null && data.get(position).getSelect() == null || !data.get(position).getSelect()) {
                    onItemClickListener.onItemClick(data, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnItemClickListener {
        void onItemClick(List<CategoryNodeWrapper> listItem, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
