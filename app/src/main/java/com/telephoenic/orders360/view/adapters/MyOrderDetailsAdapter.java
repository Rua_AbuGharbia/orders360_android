package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.viewholder.MyOrderDetailsHolder;

import java.util.List;
import java.util.Locale;

public class MyOrderDetailsAdapter extends RecyclerView.Adapter<MyOrderDetailsHolder> {
    private List<OrderItemModel> data;
    private OnRecyclerClick onRecyclerClick;

    public MyOrderDetailsAdapter(List<OrderItemModel> data, OnRecyclerClick onRecyclerClick) {
        this.data = data;
        this.onRecyclerClick = onRecyclerClick;

    }

    @NonNull
    @Override
    public MyOrderDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_order_detials, parent, false);
        return new MyOrderDetailsHolder(myRoot);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyOrderDetailsHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (position == data.size() - 1) {
            holder.lineRowMyOrderView.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.getPosition(position);
            }
        });

        Double totalAmount = data.get(position).getTotalAmount();
        Integer orderedQuantity = data.get(position).getOrderedQuantity();
        //holder.itemPriceRowMyOrderTextView.setText(String.format("%s", data.get(position).getPrice()));
        holder.itemPriceRowMyOrderTextView.setText(String.format(" %s %s", AppUtils.amountFormat(data.get(position).getPrice()), AppConstants.CURRENCY));

        // holder.totalAmountRowMyOrderTextView.setText(String.format("%s2f", totalAmount));
      //  holder.totalAmountRowMyOrderTextView.setText(" " + String.format(Locale.ENGLISH, "%.2f",data.get(position).getTotalAmount()) + " " + AppConstants.CURRENCY);
        holder.totalAmountRowMyOrderTextView.setText(String.format(" %s %s", AppUtils.amountFormat(data.get(position).getTotalAmount()), AppConstants.CURRENCY));


        holder.quantityRowMyOrderTextView.setText(String.format("%s", orderedQuantity));
        holder.orderNameRowMyOrderTextView.setText(String.format("%s", data.get(position).getName()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
