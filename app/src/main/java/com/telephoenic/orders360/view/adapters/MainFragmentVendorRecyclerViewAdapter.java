package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainFragmentVendorRecyclerViewAdapter extends
        RecyclerView.Adapter<MainFragmentVendorRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "MainFragmentVendorRecyc";

    private ArrayList<VendorWrapper> vendorsList;
    private Context mContext;
    private ItemClickListener mClickListener;
    private int cellSpan;
    private String signature;


    public MainFragmentVendorRecyclerViewAdapter(Context context, ArrayList<VendorWrapper> vendorsList, int cellSpan,String signature) {
        this.vendorsList = vendorsList;
        mContext = context;
        this.cellSpan = cellSpan;
        this.signature = signature;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.main_fragment_vendor_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (vendorsList.get(position).getLogoImageName() != null) {
            if (vendorsList.get(position).isSelected()) {
                holder.vendorImage.setBorderColor(mContext.getResources().getColor(R.color.default_blue_light));
                holder.vendorImage.setBorderWidth(5);
            } else {
                holder.vendorImage.setBorderColor(mContext.getResources().getColor(R.color.grey_40));
                holder.vendorImage.setBorderWidth((int) 0.4);
            }
            ScreenDensityUtil.setWidthCellSize(holder.mainLayout, cellSpan);
//            Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + vendorsList.get(position).getId() + "/" + vendorsList.get(position).getLogoImageName()).into(holder.vendorImage);
            holder.vendorName.setText(vendorsList.get(position).getCode());

           try {
               Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + vendorsList.get(position).getId() + "/" + vendorsList.get(position).getLogoImageName())
                       .dontTransform()
                       .crossFade(800)
                       .diskCacheStrategy(DiskCacheStrategy.ALL)
                       .error(R.drawable.place_holder_order)
                       .signature(new StringSignature(String.valueOf(signature)))
                       .into(holder.vendorImage);
           }
           catch (Exception e) {
               e.printStackTrace();
           }
        }
    }

    @Override
    public int getItemCount() {
        if (vendorsList.size() > 4) {
            return 4;
        }
        return vendorsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView vendorImage;
        private TextView vendorName;
        private LinearLayout mainLayout;

        ViewHolder(View itemView) {
            super(itemView);
            vendorImage = itemView.findViewById(R.id.main_fragment_vendor_list_item_circleimageview_vendorImage);
            vendorName = itemView.findViewById(R.id.main_fragment_vendor_list_item_TextView_vendorName);
            mainLayout = itemView.findViewById(R.id.main_layout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null &&getAdapterPosition()!=-1)
                mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public VendorWrapper getItem(int posistion) {
        return vendorsList.get(posistion);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}