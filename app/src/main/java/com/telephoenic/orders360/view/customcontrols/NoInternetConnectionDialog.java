package com.telephoenic.orders360.view.customcontrols;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.networkutils.NetworkConnectionUtils;
import com.telephoenic.orders360.controller.application.Order360Application;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class NoInternetConnectionDialog extends Dialog {

    private Fragment fragment;
    private Activity activity;

    public NoInternetConnectionDialog(Context context) {
        super(context);
    }

    public NoInternetConnectionDialog(Context context, Fragment fragment) {
        super(context);
        this.fragment = fragment;
    }
    public NoInternetConnectionDialog(Context context, Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.no_internet_connection_dialog);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.TryAgain_TextView)
    public void onViewClicked() {
        if (NetworkConnectionUtils.isNetworkAvailable(Order360Application.getInstance().getApplicationContext())) {
            if (fragment != null){
                fragment.onAttach(getContext());
            }

            dismiss();
        }
    }
}
