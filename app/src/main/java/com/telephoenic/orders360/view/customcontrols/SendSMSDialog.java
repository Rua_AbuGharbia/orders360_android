package com.telephoenic.orders360.view.customcontrols;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.SendSMSRequest;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.mvp.MVPView;
import com.telephoenic.orders360.mvp.Presenter;
import com.telephoenic.orders360.mvp.PresenterImpl;

import org.springframework.util.LinkedMultiValueMap;

import java.util.List;


public class SendSMSDialog extends Dialog implements View.OnClickListener , MVPView{

	private Button ok , reset;
	private Activity activity;
	private EditText messageEditText;
	private PosProfileWrapper posProfile ;
	private SendSMSRequest sendSMSRequest;
	private ProgressDialog progressDialog;
	List<String> customerIDS ;
	private Presenter presenterSendSMS;
	Context context ;

	public SendSMSDialog(Context context, Activity activity ,List<String> customerIDS) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_send_sms);

		this.activity = activity;
		this.customerIDS = customerIDS;
		this.context = context;
		setCancelable(false);
		init();

		presenterSendSMS = new PresenterImpl(this, context);

	    posProfile=AppUtils.getProfile(getContext());

		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(context.getResources().getString(R.string.wait_msg));
		progressDialog.setCancelable(false);
	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialog_button_send:
			if(isValidInputs()){
				AppUtils.showAlertToast("Your message have been sent ",context);
				dismiss();
				//sendSMS("0788654274",messageEditText.getText().toString().trim());
				//prepareAndAendSMSRequest();
			}
			break;
			case R.id.dialog_button_reset:
				messageEditText.setText("");
				break;
		default:
			break;
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		dismiss();
	}

	@Override
	public void onDetachedFromWindow() {
		//presenter.onDestroy();
		super.onDetachedFromWindow();
	}

	private void init() {
		ok =  findViewById(R.id.dialog_button_send);
		reset =  findViewById(R.id.dialog_button_reset);
		ok.setOnClickListener(this);
		reset.setOnClickListener(this);
		messageEditText =  findViewById(R.id.etxt_message);
	}

	private boolean isValidInputs() {
		if (TextUtils.isEmpty(messageEditText.getText().toString().trim())) {
			Snackbar.make(findViewById(android.R.id.content), R.string.toast_required_fill_message, Snackbar.LENGTH_LONG)
					.setAction("Action", null).show();
			messageEditText.requestFocus();
			return false;
		}
		return true;
	}

	@Override
	public void showProgress() {
		progressDialog.show();
	}

	@Override
	public void hideProgress() {
		progressDialog.hide();

	}

	@Override
	public void navigate(String response) {

	}

	@Override
	public void navigate(LinkedMultiValueMap response) {

	}


}
