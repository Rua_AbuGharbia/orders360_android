package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.ImageController;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.viewmodel.OrderHistoryViewModel;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderDetailsDialog extends DialogFragment implements DialogUtils.OnDialogClickListener
        ,ImageController {

    public static final String ID = "id";
    public static final String ORDER_QTY = "order_qty";
    public static final String DISCOUNT_PERCENTAGE = "discount_percentage";
    public static final String PRICE_AFTER_DISCONT = "price_discount";
    public static final String PRICE = "price";
    public static final String TYPE = "type";

    @BindView(R.id.text_order_quantity)
    TextView textOrderQuantity;
    @BindView(R.id.Delivered_Quantity_TextView)
    TextView deliveredQuantityTextView;
    @BindView(R.id.Total_Quantity_TextView)
    TextView totalQuantityTextView;
    @BindView(R.id.Delivered_Date_TextView)
    TextView deliveredDateTextView;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;
    @BindView(R.id.DiscountPercange_Date_TextView)
    TextView discountPercangeDateTextView;
    @BindView(R.id.DiscountPrice_Date_TextView)
    TextView discountPriceDateTextView;
    Unbinder unbinder;
    @BindView(R.id.textView12)
    TextView percantgeTitle;
    @BindView(R.id.textView13)
    TextView discountTitle;
    private Dialog dialog;
    private List<OrderHistoryModel> orderItemModelList;
    //    private Presenter orderHistoryPresenter;
//    private OrderHistoryRequest orderHistoryRequest;
    private int id;
    private double price;
    private Double priceDiscount;
    private int deliveredQy = 0;
    private int type;
    private int product = 1;
    private Double discountPercentage;
    private OrderHistoryViewModel orderHistoryViewModel;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.order_details_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ButterKnife.bind(this, dialog);
//            orderHistoryPresenter = new PresenterImpl(this, getContext());
            orderHistoryViewModel = ViewModelProviders.of(this).get(OrderHistoryViewModel.class);

            if (getArguments() != null) {
                type = getArguments().getInt(TYPE);
                spinKit.setVisibility(View.VISIBLE);
                id = getArguments().getInt(ID);
                textOrderQuantity.setText(getArguments().getString(ORDER_QTY));
                price = getArguments().getDouble(PRICE);
                priceDiscount = getArguments().getDouble(PRICE_AFTER_DISCONT);
                discountPercentage = getArguments().getDouble(DISCOUNT_PERCENTAGE);
                if (priceDiscount == null || priceDiscount == 0) {
                    priceDiscount = price;
                } else {
                    priceDiscount = getArguments().getDouble(PRICE_AFTER_DISCONT);

                }
                if (discountPercentage == null) {
                    discountPercentage = 0.0;
                }
                if (type != product) {
                    discountPercangeDateTextView.setVisibility(View.GONE);
                    discountTitle.setText(getString(R.string.fragment_purchase_history_title_item_price));
                    discountPriceDateTextView.setText(String.format("%s %s", String.format(Locale.ENGLISH, "%.2f", price), AppConstants.CURRENCY));
                    percantgeTitle.setVisibility(View.GONE);
                } else {
                    discountPriceDateTextView.setText(String.format("%s %s", String.format(Locale.ENGLISH, "%.2f", priceDiscount), AppConstants.CURRENCY));
                    discountPercangeDateTextView.setText(String.format("%s %s", String.format(Locale.ENGLISH, "%.2f", discountPercentage), "%"));
                }
            }
            prepareAndSendRequest();
        }
        return dialog;
    }

    @OnClick(R.id.Ok_TextView)
    public void onViewClicked() {
        dismiss();
    }

    private void prepareAndSendRequest() {

        orderHistoryViewModel.getOrderHistory(id).observe(this, new ApiObserver<List<OrderHistoryModel>>() {
            @Override
            protected void noConnection() {
                spinKit.setVisibility(View.GONE);
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(List<OrderHistoryModel> data) {
                orderItemModelList = data;
                if (orderItemModelList == null || orderItemModelList.isEmpty()) {
                    deliveredQuantityTextView.setText("0");
                    totalQuantityTextView.setText("0");
                } else {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.ENGLISH);
                    String dateString = formatter.format(new Date(Long.valueOf(orderItemModelList.get(orderItemModelList.size() - 1).getDeliveryDate())));
                    deliveredDateTextView.setText(dateString);
                    for (int i = 0; i < orderItemModelList.size(); i++) {
                        deliveredQy = deliveredQy + Integer.parseInt(orderItemModelList.get(i).getQuantity());
                    }
                    deliveredQuantityTextView.setText(deliveredQy + "");
                    double priceTotal = deliveredQy * priceDiscount;
                    totalQuantityTextView.setText(String.format(Locale.ENGLISH, "%.2f", priceTotal) + " " + AppConstants.CURRENCY);
                }
                spinKit.setVisibility(View.GONE);

            }

            @Override
            protected void onError(String error) {
                spinKit.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onOkClicked() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            prepareAndSendRequest();
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getActivity(), settingsDialogModel, this);
    }

}