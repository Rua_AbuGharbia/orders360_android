package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.OrderModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.model.AppPrefs;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewModel> {
    private Context mContext;
    private List<OrderModel> data;
    private View.OnClickListener onClickListener;
    private boolean viewPOSName;
    public static Locale sysLocale;
    private String signature;

    public OrderAdapter(Context context, int textViewResourceId, List<OrderModel> objects, View.OnClickListener onClickListener, Boolean viewPOSName,String signature) {
        mContext = context;
        data = objects;
        this.onClickListener = onClickListener;
        this.viewPOSName = viewPOSName;
        sysLocale = mContext.getResources().getConfiguration().locale;
        this.signature =signature;
    }


    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public OrderViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row;
        row = LayoutInflater.from(mContext).inflate(R.layout.item_purchase, parent, false);
        return new OrderViewModel(row);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderViewModel holder, int position) {

        holder.layout.setTag(position);

        holder.layout.setOnClickListener(onClickListener);

        holder.orderNumber.setText(data.get(position).getOrderNumber());

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        String dateString = formatter.format(new Date(Long.valueOf(String.valueOf(data.get(position).getAudit().getCreatedDate()))));
        holder.orderDate.setText(String.format("%s : %s", mContext.getString(R.string.order_date), dateString));

        if (data.get(position).getPosUser() !=null){
            holder.vendorName.setVisibility(View.VISIBLE);
            holder.vendorName.setText(data.get(position).getPosUser().getName());
        }
        //  holder.totalAmount.setText(String.format("%s %s",data.get(position).getTotalAmount(), AppConstants.CURRENCY));
       // holder.totalAmount.setText(" " + String.format(Locale.ENGLISH, "%.2f",data.get(position).getTotalAmount()) + " " + AppConstants.CURRENCY);
        holder.totalAmount.setText(String.format(" %s %s", AppUtils.amountFormat(data.get(position).getTotalAmount()), AppConstants.CURRENCY));


        holder.orderStatus.setText(data.get(position).getDeliveryStatus().getName());
        String statusCode = data.get(position).getDeliveryStatus().getCode();

        switch (statusCode) {
            case AppConstants.NEW_CODE:
                holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                break;
            case AppConstants.ORDER_ACKNOWLEDGED_CODE:
                holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.acknowledged_color));
                break;
            case AppConstants.ORDER_OPEN_CODE:
                holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.partial_complete_color));
                break;
            case AppConstants.ORDER_REJECTED_CODE:
                holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.partial_in_complete_color));
                break;
            case AppConstants.ORDER_CLOSED_CODE:
                holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.full_delivered_color));
                break;
            case AppConstants.ORDER_CANCEL_CODE:
                holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.default_blue_light));
                break;
        }

//        try {
////            String imageName = data.get(position).getVendor().getLogoImageName();
////
////            Long imageID = data.get(position).getVendor().getId();
////            if (imageName != null) {
////                Glide.with(mContext)
////                        .load(AppConstants.URL_LIVE + "vendor/attachments/" + imageID + "/" + imageName)
////                        .error(R.drawable.ic_no_icon)
//////                        .error(Drawable.createFromPath(""))
////                        .into(holder.vendorImage);
////            }  else {
////
////            }
//            Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
//                    .error(R.drawable.place_holder_blue)
//                    .signature(new StringSignature(String.valueOf(signature)))
//                    .into(holder.vendorImage);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class OrderViewModel extends RecyclerView.ViewHolder {
        final TextView orderDate;
        final TextView orderNumber;
        final TextView totalAmount;
        final TextView orderStatus;
        final TextView vendorName;
        final CircleImageView vendorImage;
        final ConstraintLayout layout;

        OrderViewModel(View itemView) {
            super(itemView);

            orderDate = itemView.findViewById(R.id.text_order_delivery_date);
            orderNumber = itemView.findViewById(R.id.text_order_number);
            totalAmount = itemView.findViewById(R.id.text_order_total_amount);
            orderStatus = itemView.findViewById(R.id.text_order_status);
            vendorName = itemView.findViewById(R.id.text_vendor_name);
            vendorImage = itemView.findViewById(R.id.image_vendor);
            layout = itemView.findViewById(R.id.layout_purchase);

        }
    }
}