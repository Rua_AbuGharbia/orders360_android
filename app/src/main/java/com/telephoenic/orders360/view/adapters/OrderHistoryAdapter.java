package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class OrderHistoryAdapter extends ArrayAdapter<OrderHistoryModel> {
    private Context mContext;
    private List<OrderHistoryModel> data;

    public static Locale sysLocale;

    public OrderHistoryAdapter(Context context, int textViewResourceId, List<OrderHistoryModel> objects  ) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
        sysLocale = mContext.getResources().getConfiguration().locale;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;

            row = LayoutInflater.from(mContext).inflate(R.layout.item_history, null);

            final TextView productDate =  row.findViewById(R.id.text_product_date);
            final TextView quantity =  row.findViewById(R.id.text_product_quntity);

        productDate.setTag(position);
        quantity.setTag(position);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        String dateString = formatter.format(new Date(Long.valueOf(data.get(position).getDeliveryDate())));

        productDate.setText(dateString);
        quantity.setText(data.get(position).getQuantity());

        return row;
    }

    @Override
    public OrderHistoryModel getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
