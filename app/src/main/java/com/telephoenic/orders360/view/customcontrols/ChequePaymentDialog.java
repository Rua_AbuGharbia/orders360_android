package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.GetChequeData;
import com.telephoenic.orders360.controller.server.model.shiporder.ChequePaymentsModel;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.telephoenic.orders360.controller.utils.AppUtils.hideKeyboard;


@SuppressLint("ValidFragment")
public class ChequePaymentDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    public static final String DATA = " data";
    public static final String REMAINING_AMOUNT = "remaining_amount";

    @BindView(R.id.Cancel_Buttom)
    Button cancelButtom;
    @BindView(R.id.Save_Buttom)
    Button saveButtom;
    @BindInput(R.id.ChequeTo_OrdersEditText)
    Orders360EditText chequeToOrdersEditText;
    @BindInput(R.id.BankName_OrdersEditText)
    Orders360EditText bankNameOrdersEditText;
    @BindInput(R.id.IssuerName_OrdersEditText)
    Orders360EditText issuerNameOrdersEditText;
    @BindInput(R.id.ChequeNumber_Orders360EditText)
    Orders360EditText chequeNumberOrders360EditText;
    @BindInput(R.id.ChequeDate_Orders360EditText)
    Orders360EditText chequeDateOrders360EditText;
    @BindInput(R.id.Amount_Orders360EditText)
    Orders360EditText amountOrders360EditText;
    @BindView(R.id.RemainingAmount_TextView)
    TextView remainingAmountTextView;
    Unbinder unbinder;
    private Dialog dialog;
    private ChequePaymentsModel chequePaymentsModel;
    private DatePickerDialog datePickerDialog;
    private GetChequeData getChequeData;
    private double remainingAmount;
    private double amount;
    private String chequeTo = "";
    private String bankName = "";
    private String issuerName = "";
    private String chequeNumber = "";
    private Date chequeDate = new Date();


    public ChequePaymentDialog(GetChequeData getChequeData) {
        this.getChequeData = getChequeData;
    }

    public ChequePaymentDialog() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.row_cheque_payment);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            if (getArguments() != null) {
                chequePaymentsModel = (ChequePaymentsModel) getArguments().getSerializable(DATA);
                remainingAmount = getArguments().getDouble(REMAINING_AMOUNT);
            }
            ButterKnife.bind(this, dialog);
            OrdersView ordersView = new OrdersView();
            ordersView.bind(this, dialog.getWindow().getDecorView());
            inti();
        }
        return dialog;
    }

    @SuppressLint("NewApi")
    private void inti() {
        datePickerDialog = new DatePickerDialog(
                Objects.requireNonNull(getActivity()), this, 2020, 1, 1);

//        long currentTime = new Date().getTime();
//        datePickerDialog.getDatePicker().setMinDate(currentTime);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            datePickerDialog.setOnDateSetListener(this);
        }
        chequeDateOrders360EditText.getContentEditText().setFocusable(false);
        chequeDateOrders360EditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getActivity());
                datePickerDialog.show();
            }
        });
        if (chequePaymentsModel != null) {
            if (chequePaymentsModel.getAmount() != null) {
                amount = chequePaymentsModel.getAmount();
                remainingAmount = remainingAmount + chequePaymentsModel.getAmount();
                amountOrders360EditText.setText(" " + String.format(Locale.ENGLISH, "%.2f", chequePaymentsModel.getAmount()));
            }


            chequeTo = chequePaymentsModel.getPayTo();
            bankName = chequePaymentsModel.getBankName();
            issuerName = chequePaymentsModel.getIssuerName();
            chequeNumber = chequePaymentsModel.getNumber();

            chequeNumberOrders360EditText.setText(chequePaymentsModel.getNumber());
            issuerNameOrdersEditText.setText(chequePaymentsModel.getIssuerName());
            chequeToOrdersEditText.setText(chequePaymentsModel.getPayTo());
            bankNameOrdersEditText.setText(chequePaymentsModel.getBankName());
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            String expiryDateString = formatter.format(chequePaymentsModel.getDueDate());
            chequeDateOrders360EditText.setText(expiryDateString);

        } else {
            chequePaymentsModel = new ChequePaymentsModel();
        }
        remainingAmountTextView.setText(String.format("Remaining Amount : %s %s", remainingAmount, AppConstants.CURRENCY));

        amountOrders360EditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String result = s.toString();
                if (!result.equals("") && !TextUtils.isEmpty(result.trim()) && !result.startsWith(".") ) {
                    if (result.endsWith("."))
                        result = result.replace("\\.", "");
                    amount = (Double.valueOf(result.trim()));
                } else
                    amount = 0.0;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        issuerNameOrdersEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    issuerName = (s.toString());


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        chequeToOrdersEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    chequeTo = (s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        chequeNumberOrders360EditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    chequeNumber = (s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        bankNameOrdersEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    bankName = (s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.Delete_ImageView)
    public void onViewClicked() {
        dialog.dismiss();
    }

    @OnClick({R.id.Cancel_Buttom, R.id.Save_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Cancel_Buttom:
                dialog.dismiss();
                break;
            case R.id.Save_Buttom:
                if (TextUtils.isEmpty(chequeToOrdersEditText.getEditTextContent())) {
                    chequeToOrdersEditText.setErrorMessage("Please enter name");
                    chequeToOrdersEditText.validate();

                } else if (TextUtils.isEmpty(bankNameOrdersEditText.getEditTextContent())) {
                    bankNameOrdersEditText.setErrorMessage("Please enter  bank name");
                    bankNameOrdersEditText.validate();

                } else if (TextUtils.isEmpty(issuerNameOrdersEditText.getEditTextContent())) {
                    issuerNameOrdersEditText.setErrorMessage("Please enter issuer name");
                    issuerNameOrdersEditText.validate();

                } else if (TextUtils.isEmpty(chequeNumberOrders360EditText.getEditTextContent())) {
                    chequeNumberOrders360EditText.setErrorMessage("Please enter cheque number");
                    chequeNumberOrders360EditText.validate();
                } else if (TextUtils.isEmpty(chequeDateOrders360EditText.getEditTextContent())) {
                    chequeDateOrders360EditText.setErrorMessage("Please enter cheque date");
                    chequeDateOrders360EditText.validate();
                } else if (TextUtils.isEmpty(amountOrders360EditText.getEditTextContent()) || amount == 0) {
                    amountOrders360EditText.setErrorMessage("Please enter the amount");
                    amountOrders360EditText.validate();
                } else if (amount <= remainingAmount) {
                    if (getChequeData != null) {
                        chequePaymentsModel.setAmount(amount);
                        chequePaymentsModel.setBankName(bankName);
                        chequePaymentsModel.setPayTo(chequeTo);
                        chequePaymentsModel.setNumber(chequeNumber);
                        chequePaymentsModel.setIssuerName(issuerName);
                        chequePaymentsModel.setDueDate(chequeDate.getTime());
                        getChequeData.getChequeData(chequePaymentsModel);
                    }
                    hideKeyboard(getActivity());
                    dismiss();
                } else {
                    amountOrders360EditText.setErrorMessage("The amount entered is greater than the remaining amount");
                    amountOrders360EditText.validate();
                }
                break;
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        month = month + 1;
        chequeDateOrders360EditText.setText(year + "/" + month
                + "/" + dayOfMonth);

        Date date = new Date(calendar.getTimeInMillis());
        chequeDate = date;
        // chequePaymentsModel.setDueDate(date);

    }
}
