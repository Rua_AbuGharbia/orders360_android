package com.telephoenic.orders360.view.customcontrols;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.utils.AppUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeDialog extends Dialog implements OnClickListener {

	private Context mContext;
	final  int RQS_1 = 1;
	 DatePicker pickerDate;
	 //TimePicker pickerTime;
	 Button buttonSetAlarm;
	private String dateTime = "";
	 
	public DateTimeDialog(Context mContext) {
		super(mContext);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.date_time_dialog);

		this.mContext = mContext;

		if(AppUtils.getLayoutDirection(((Activity)mContext)).equals(AppConstants.RIGHT_TO_LEFT)){
			getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
		}

		pickerDate =  findViewById(R.id.pickerdate);
		  //pickerTime =  findViewById(R.id.pickertime);
		  buttonSetAlarm =  findViewById(R.id.setdate);
		  buttonSetAlarm.setOnClickListener(this);

		pickerDate.setMaxDate(System.currentTimeMillis()-24*60*60*1000);
		//pickerDate.setMaxDate(yesterday());

	}
	private Date yesterday() {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();	
		dismiss();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	    Calendar cal = Calendar.getInstance();

		switch (v.getId()) {
		case R.id.setdate:
			Calendar current = Calendar.getInstance();
		    
		    cal.set(pickerDate.getYear(), 
		      pickerDate.getMonth(), 
		      pickerDate.getDayOfMonth());
		    
		    /*if(cal.compareTo(current) <= 0){
		     //The set Date/Time already passed
				AppUtils.showAlert(mContext.getString(R.string.invalid_date_time), mContext);

		    }else{*/
			DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
			//dateTime = dateFormat.format(cal.getTime()).toString();
				dateTime  = pickerDate.getDayOfMonth() + "-" +  (pickerDate.getMonth()  + 1) + "-" + pickerDate.getYear();//  + " " + pickerTime.getCurrentHour() + ":" + pickerTime.getCurrentMinute() + ":00"  ;
				dismiss();
//	    	}
			break;

		default:
			break;
		}
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

}
