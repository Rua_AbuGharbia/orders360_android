package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.controller.viewholder.MainBrandViewHolder;

import java.util.List;

public class MainBrandAdapter extends RecyclerView.Adapter<MainBrandViewHolder> {
    private Context context;
    private List<BrandWrapper> data;
    private String signature;

    public MainBrandAdapter(Context context, List<BrandWrapper> data, String signature) {
        this.context = context;
        this.data = data;
        this.signature = signature;
    }

    @NonNull
    @Override
    public MainBrandViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View muRoot = LayoutInflater.from(context).inflate(R.layout.row_brand_main, parent, false);
        return new MainBrandViewHolder(muRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull final MainBrandViewHolder holder, int position) {

        if (data.get(position).getImageName() != null) {
            if (data.get(position).getImageName().contains(";")) {
                data.get(position).setImageName(data.get(position).getImageName().split(";")[0]);
            }
            holder.loadingView.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(AppConstants.URL_LIVE + "brand/attachments/" + data.get(position).getId() + "/" + data.get(position).getImageName())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                            holder.loadingView.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                            holder.loadingView.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .placeholder( holder.loadingView.getProgressDrawable())
                    .crossFade(800)
                    .error(R.drawable.place_holder_order)
                    .signature(new StringSignature(String.valueOf(signature)))
                    .into(holder.brandImageView);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
