package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.CustomerModel;

import java.util.List;
import java.util.Locale;

public class CampaignAdapter extends ArrayAdapter<CustomerModel> {
    private Context mContext;
    private List<CustomerModel> data;
    View.OnClickListener clickListener ;

    public static Locale sysLocale;

    public CampaignAdapter(Context context, int textViewResourceId, List<CustomerModel> objects , View.OnClickListener clickListener) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        sysLocale = mContext.getResources().getConfiguration().locale;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
//        if (convertView == null) {
            row = LayoutInflater.from(mContext).inflate(R.layout.item_campaign, null);

        final TextView mobile =  row.findViewById(R.id.text_customer_mobile);
        final TextView name =  row.findViewById(R.id.text_customer_name);
        final CheckBox checkBox =  row.findViewById(R.id.checkBox_campaign);

        name.setTag(position);
        mobile.setTag(position);
        checkBox.setTag(position);

        mobile.setText(data.get(position).getMobileNumber());
        name.setText(data.get(position).getName());

        if(data.get(position).isChecked()){
            checkBox.setChecked(true);
        }

        name.setTag(position);
        mobile.setTag(position);
        checkBox.setTag(position);

        name.setOnClickListener(clickListener);
        mobile.setOnClickListener(clickListener);
        checkBox.setOnClickListener(clickListener);

        return row;
    }

    @Override
    public CustomerModel getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
