package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;

import java.util.List;

public class PromotionHorizontalAdapter extends RecyclerView.Adapter<PromotionHorizontalAdapter.MyViewHolder> {
    private Context mContext;
    private List<ProductWrapper> data;
    View.OnClickListener clickListener ;
    PosProfileWrapper posProfileWrapper;


    private boolean isChanged = true;

    public PromotionHorizontalAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects , View.OnClickListener clickListener) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper  = AppUtils.getProfile(context);
    }

    public PromotionHorizontalAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects ) {
        mContext = context;
        data = objects;
        posProfileWrapper  = AppUtils.getProfile(context);
    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_combo_horizontal, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {
        final ProductWrapper productWrapper = data.get(position);

        holder.setIsRecyclable(false);
        holder.layout.setTag(position);

        holder.layout.setOnClickListener(clickListener);

        holder.price.setText((float)Math.round(data.get(position).getPrice() * 100) / 100+" "+AppConstants.CURRENCY);

        try {
            //TODO set Image from server
           String  imagesName = productWrapper.getImagesName();
            if(imagesName.contains(";")){
                imagesName = imagesName.split(";")[0];
            }
            Integer imageID = productWrapper.getId();
            if (imagesName != null) {
                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                        .error(R.drawable.ic_no_icon)
                        .into(holder.promotionImage);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final ImageView promotionImage;
        final Button price ;
        final ConstraintLayout layout ;

        public MyViewHolder(View view) {
            super(view);
            promotionImage = view.findViewById(R.id.image_combo);
            price = view.findViewById(R.id.button_price);
            layout = view.findViewById(R.id.layout);
        }
    }
}
