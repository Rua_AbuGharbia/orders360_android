package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.GetAmount;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.CashPayments;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.telephoenic.orders360.controller.utils.AppUtils.hideKeyboard;


@SuppressLint("ValidFragment")
public class CashPaymentDialog extends DialogFragment {
    public static final String DATA = " data";
    public static final String REMAINING_AMOUNT = "remaining_amount";
    @BindInput(R.id.CashPayment_OrdersEditText)
    Orders360EditText cashPaymentOrdersEditText;
    @BindView(R.id.Cancel_Buttom)
    Button cancelButtom;
    @BindView(R.id.Save_Buttom)
    Button saveButtom;
    @BindView(R.id.RemainingAmount_TextView)
    TextView remainingAmountTextView;
    Unbinder unbinder;
    private OnRecyclerClick onRecyclerClick;
    private Dialog dialog;
    private String title;
    private CashPayments cashPayments;
    private GetAmount getAmount;
    private double remainingAmount;
    private Double amount;


    public CashPaymentDialog(GetAmount getAmount) {
        this.getAmount = getAmount;

    }

    public CashPaymentDialog() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.row_cash_payment);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            if (getArguments() != null) {
                cashPayments = (CashPayments) getArguments().getSerializable(DATA);
                remainingAmount = getArguments().getDouble(REMAINING_AMOUNT);
            }
            ButterKnife.bind(this, dialog);
            OrdersView ordersView = new OrdersView();
            ordersView.bind(this, dialog.getWindow().getDecorView());
            inti();
        }
        return dialog;
    }

    private void inti() {
        saveButtom.setVisibility(View.VISIBLE);
        cancelButtom.setVisibility(View.VISIBLE);
        if (cashPayments != null) {
            if (cashPayments.getAmount() != null)
                remainingAmount = remainingAmount + cashPayments.getAmount();
            amount = cashPayments.getAmount();
            cashPaymentOrdersEditText.setText(" " + String.format(Locale.ENGLISH, "%.2f", cashPayments.getAmount()));
        } else {
            cashPayments = new CashPayments();
        }
        remainingAmountTextView.setText(String.format("Remaining Amount : %s %s", remainingAmount, AppConstants.CURRENCY));
        cashPaymentOrdersEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String result = s.toString();
                if (!result.equals("") && !TextUtils.isEmpty(result.trim()) && !result.startsWith(".") ) {
                    if (result.endsWith("."))
                        result = result.replace("\\.", "");
                    amount = (Double.valueOf(result.trim()));
                } else
                    amount = 0.0;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @OnClick(R.id.Delete_ImageView)
    public void onViewClicked() {
        dialog.dismiss();
    }

    @OnClick({R.id.Cancel_Buttom, R.id.Save_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Cancel_Buttom:
                dialog.dismiss();
                break;
            case R.id.Save_Buttom:
                if (TextUtils.isEmpty(cashPaymentOrdersEditText.getEditTextContent()) || amount == 0) {
                    cashPaymentOrdersEditText.setErrorMessage("Please enter the amount");
                    cashPaymentOrdersEditText.validate();
                } else if (amount <= remainingAmount) {
                    hideKeyboard(getActivity());
                    getAmount.getAmount(amount);
                    dismiss();
                } else {
                    cashPaymentOrdersEditText.setErrorMessage("The amount entered is greater than the remaining amount");
                    cashPaymentOrdersEditText.validate();
                }
                break;
        }
    }
}
