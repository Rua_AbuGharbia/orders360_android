package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.typedef.MaxQtyItem;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.database.SaveCart;
import com.telephoenic.orders360.controller.interfaces.AddToCartAnim;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.ProductDetailsDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ProducatsAdapter extends RecyclerView.Adapter<ProducatsAdapter.MyViewHolder> {
    private Context mContext;
    private List<ProductWrapper> data;
    private OnRecyclerClick clickListener;
    PosProfileWrapper posProfileWrapper;
    private List<String> privilegeList = new ArrayList<>();
    private int cellSpan;
    private AddToCartAnim addToCartAnim;
    private String signature;
    private CountDownTimer cTimer = null;
    String imagesName;

    private boolean isChanged = true;

    public ProducatsAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects, OnRecyclerClick clickListener) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper = AppUtils.getProfile(context);
    }

    public ProducatsAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects) {
        mContext = context;
        data = objects;
        posProfileWrapper = AppUtils.getProfile(context);

    }

    public ProducatsAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects, OnRecyclerClick clickListener, int cellSpan, AddToCartAnim addToCartAnim, String signature) {
        mContext = context;
        data = objects;
        posProfileWrapper = AppUtils.getProfile(context);
        this.clickListener = clickListener;
        this.cellSpan = cellSpan;
        this.addToCartAnim = addToCartAnim;
        this.signature = signature;

    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProducatsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);

        return new ProducatsAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ProducatsAdapter.MyViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        final ProductWrapper productWrapper = data.get(holder.getAdapterPosition());
        if (productWrapper.getDiscount() == null) {
            productWrapper.setDiscount(0.0);
        }


        ScreenDensityUtil.setWidthCellSize(holder.mainLayout, cellSpan);
        holder.loaderView.setVisibility(View.VISIBLE);

        /*if(productWrapper.isCanDelete()){
            holder.imageClose.setVisibility(View.VISIBLE);
        } else {
            holder.imageClose.setVisibility(View.INVISIBLE);
        }*/

        if (productWrapper.getHasPromotion() == 1 && AppConstants.PROMOTION_TYPE_QUANTITY.equals(AppPrefs.getVendorType(mContext))) {
            holder.commission.setVisibility(View.VISIBLE);
        } else {
            holder.commission.setVisibility(View.GONE);
        }

        if (posProfileWrapper != null) {
            privilegeList = posProfileWrapper.getUser().getPermissions();
        }
        for (String item : privilegeList) {
            switch (item) {
                case AppConstants.ROLE_PRODUCT_ADD:
                case AppConstants.ROLE_PROMOTION_ADD:
                case AppConstants.ROLE_COMBO_ADD:
                    holder.plusMinusActions.setVisibility(View.VISIBLE);
                    holder.addToCart.setVisibility(View.VISIBLE);
                    holder.totalPrice.setVisibility(View.VISIBLE);
                    break;
                case AppConstants.ROLE_PRODUCT_VIEW:
                case AppConstants.ROLE_PROMOTION_VIEW:
                case AppConstants.ROLE_COMBO_VIEW:
                    holder.plusMinusActions.setVisibility(View.GONE);
                    holder.addToCart.setVisibility(View.GONE);
                    holder.totalPrice.setVisibility(View.GONE);
                    break;
            }
        }


//        holder.favoritesImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cTimer = new CountDownTimer(3000, 1000) {
//                    @SuppressLint("SetTextI18n")
//                    public void onTick(long millisUntilFinished) {
//                       holder.addToFavMassage.setVisibility(View.VISIBLE);
//                    }
//
//                    public void onFinish() {
//                        holder.addToFavMassage.setVisibility(View.GONE);
//
//                    }
//                };
//                cTimer.start();
//                holder.favoritesImage.setColorFilter(ContextCompat.getColor(mContext, R.color.red_400), PorterDuff.Mode.MULTIPLY);
//            }
//        });
//        holder.imageClose.setTag(position);L
//        holder.imageClose.setOnClickListener(clickListener);

        holder.name.setText(productWrapper.getName());
        holder.alternativeName.setText(productWrapper.getAlternativeName());
        if (productWrapper.getDiscount() > 0) {
            if (!AppConstants.SHOW_POINT)
            holder.points.setVisibility(View.GONE);

            holder.discountLinear.setVisibility(View.VISIBLE);
            holder.oldPrice.setText(" " + AppUtils.amountFormat(productWrapper.getPrice()) + " " + AppConstants.CURRENCY);
           // holder.oldPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice()) + " " + AppConstants.CURRENCY);
            holder.oldPrice.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.discount.setText(productWrapper.getDiscount() + "%");
         //   holder.price.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getDiscountedPrice()) + AppConstants.CURRENCY);
            holder.price.setText(" " + AppUtils.amountFormat(productWrapper.getDiscountedPrice()) + " " + AppConstants.CURRENCY);



        } else {
            double price = (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getDiscount()) / 100);
//            holder.price.setText(" " + String.format(Locale.ENGLISH, "%.2f", (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getDiscount()) / 100)) + " " + AppConstants.CURRENCY);
            holder.price.setText(" " + AppUtils.amountFormat(price) + " " + AppConstants.CURRENCY);

        }
        //   holder.price.setText(" " + String.format(Locale.ENGLISH, "%.2f", (productWrapper.getPrice() - (productWrapper.getPrice() * productWrapper.getDiscount()) / 100)) + " " + AppConstants.CURRENCY);

        holder.purchaseNumber.setText(productWrapper.getCount() == null ? "0" : productWrapper.getCount());

        holder.points.setText(productWrapper.getLoyaltyPoint() == null ? "+ 0 Pts" : "+ " + productWrapper.getLoyaltyPoint() + " Pts");

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        if (productWrapper.getExpiryDate() != null) {
            String expiryDateString = formatter.format(new Date(Long.valueOf(productWrapper.getExpiryDate())));
            holder.expiryDate.setText(expiryDateString);
        } else {
            holder.expiaryDateLayout.setVisibility(View.GONE);
        }


        if (productWrapper.getCount() != null) {
            Integer currentQty = Integer.parseInt(productWrapper.getCount());
            double total_price;
            if (productWrapper.getDiscount() > 0) {
                total_price = currentQty * productWrapper.getDiscountedPrice();

            } else {
                total_price = currentQty * productWrapper.getPrice();
            }
            holder.totalPrice.setText(" " + AppUtils.amountFormat(total_price) + " " + AppConstants.CURRENCY);
           // holder.totalPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
        }

        holder.imageMinus.setTag(position);
        holder.imagePlus.setTag(position);
        holder.addToCart.setTag(position);
        holder.name.setTag(position);
        holder.alternativeName.setTag(position);
        holder.price.setTag(position);
        holder.points.setTag(position);
        holder.totalPrice.setTag(position);
        holder.purchaseNumber.setTag(position);
        holder.commission.setTag(position);
        holder.expiryDate.setTag(position);

        holder.purchaseNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("") || s.toString().equals("-")) {
                    productWrapper.setCount("0");
                    holder.commission.setText(mContext.getResources().getString(R.string.fragment_product_commission));
                    productWrapper.setSelectedCommission(null);
                    productWrapper.setCommissionQtyToValueTransient(null);
                } else {
                    productWrapper.setCount(s + "");
                    Integer currentQty = Integer.parseInt(s + "");
                    double total_price;
                    if (productWrapper.getDiscount() > 0) {
                        total_price = currentQty * productWrapper.getDiscountedPrice();

                    } else {
                        total_price = currentQty * productWrapper.getPrice();
                    }
                    holder.totalPrice.setText(" " + AppUtils.amountFormat(total_price) + " " + AppConstants.CURRENCY);
                    holder.commission.setText(mContext.getResources().getString(R.string.fragment_product_commission));
                    productWrapper.setSelectedCommission(null);
                    productWrapper.setCommissionQtyToValueTransient(null);
                }
            }
        });

        holder.imagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.purchaseNumber.getText().toString().equals("")) {
                    Integer currentQty = Integer.parseInt(holder.purchaseNumber.getText().toString());
                    if (currentQty < MaxQtyItem.MAX_QTY) {
                        currentQty++;
                        double total_price;
                        if (productWrapper.getDiscount() > 0) {
                            total_price = currentQty * productWrapper.getDiscountedPrice();

                        } else {
                            total_price = currentQty * productWrapper.getPrice();
                        }
                        holder.totalPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                        holder.purchaseNumber.setText(String.valueOf(currentQty));
                        productWrapper.setCount(currentQty.toString());
                        holder.commission.setText(mContext.getResources().getString(R.string.fragment_product_commission));
                        productWrapper.setSelectedCommission(null);
                        productWrapper.setCommissionQtyToValueTransient(null);
                    }

                }
            }
        });

        holder.imageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.purchaseNumber.getText().toString().equals("")) {
                    Integer currentQty = Integer.parseInt(holder.purchaseNumber.getText().toString());
                    if (currentQty > 0) {
                        currentQty--;
                        double total_price;
                        if (productWrapper.getDiscount() > 0) {
                            total_price = currentQty * productWrapper.getDiscountedPrice();

                        } else {
                            total_price = currentQty * productWrapper.getPrice();
                        }
                        holder.totalPrice.setText(" " + String.format(Locale.ENGLISH, "%.2f", total_price) + " " + AppConstants.CURRENCY);
                        holder.purchaseNumber.setText(String.valueOf(currentQty));
                        productWrapper.setCount(currentQty.toString());
                        holder.commission.setText(mContext.getResources().getString(R.string.fragment_product_commission));
                        productWrapper.setSelectedCommission(null);
                        productWrapper.setCommissionQtyToValueTransient(null);
                    }
                }
            }
        });

        holder.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart(productWrapper, holder.commission, holder.purchaseNumber, holder, position);

            }
        });

        holder.commission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickListener.getPosition(position);
            }
        });

        holder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 8/21/2019 For Video
//                Intent tostart = new Intent(Intent.ACTION_VIEW);
//                tostart.setDataAndType(Uri.parse(AppConstants.URL_LIVE + "product/attachments/" + productWrapper.getId() + "/" + "sample.mp4"), "video/*");
//               mContext.startActivity(tostart);

                ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(mContext,
                        productWrapper.getName(), productWrapper.getImagesName(), productWrapper.getDescription(), productWrapper.getRetailPrice(), productWrapper.getId(), ProductDetailsDialog.PRODUCT);
                productDetailsDialog.show();
            }
        });
        try {
            //TODO set Image from server

            imagesName = productWrapper.getImagesName();

            Integer imageID = productWrapper.getId();
            if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                imagesName = "empty";
            }


            if (imagesName != null || !TextUtils.isEmpty(imagesName)) {
                if (imagesName.contains(";")) {
                    imagesName = imagesName.split(";")[0];
                }

                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                        // .dontTransform()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                holder.loaderView.setVisibility(View.GONE);
                                Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                        .signature(new StringSignature(String.valueOf(signature)))
                                        .error(R.drawable.place_holder_order)
                                        .into(holder.productImage);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                holder.loaderView.setVisibility(View.GONE);

                                return false;
                            }
                        })
                        //  .crossFade(800)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(holder.productImage);

                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                        //  .dontTransform()
                        //.crossFade(800)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                holder.loaderView.setVisibility(View.GONE);
                                Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                        .signature(new StringSignature(String.valueOf(signature)))
                                        .error(R.drawable.place_holder_order)
                                        .into(holder.productImageCopy);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                holder.loaderView.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(holder.productImageCopy);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addToCart(ProductWrapper productWrapper, Button commissionButton, final TextView countText, final ProducatsAdapter.MyViewHolder holder, final int pos) {
        final ProductWrapper newProductWrapper = new ProductWrapper();
        newProductWrapper.setId(productWrapper.getId());
        newProductWrapper.setName(productWrapper.getName());
        newProductWrapper.setAlternativeName(productWrapper.getAlternativeName());
        newProductWrapper.setPrice(productWrapper.getPrice());
        newProductWrapper.setRetailPrice(productWrapper.getRetailPrice());
        newProductWrapper.setImagesName(productWrapper.getImagesName());
        newProductWrapper.setDescription(productWrapper.getDescription());
        newProductWrapper.setLoyaltyPoint(productWrapper.getLoyaltyPoint());
        newProductWrapper.setHasPromotion(productWrapper.getHasPromotion());
        newProductWrapper.setStartDate(productWrapper.getStartDate());
        newProductWrapper.setEndDate(productWrapper.getEndDate());
        newProductWrapper.setExpiryDate(productWrapper.getExpiryDate());
        newProductWrapper.setVat(productWrapper.getVat());
        newProductWrapper.setDiscount(productWrapper.getDiscount());
        newProductWrapper.setDiscountedPrice(productWrapper.getDiscountedPrice());

        newProductWrapper.setCount(productWrapper.getCount());
        newProductWrapper.setSelectedCommission(productWrapper.getSelectedCommission());
        newProductWrapper.setCommissionQtyToValueTransient(productWrapper.getCommissionQtyToValueTransient());
        //newProductWrapper.setCommissionList(productWrapper.getCommissionList());
        newProductWrapper.setCommissionListAttribute(productWrapper.getCommissionListAttribute());
        newProductWrapper.setCommissionListQantity(productWrapper.getCommissionListQantity());
        if (newProductWrapper.getCount() == null || newProductWrapper.getCount().equals("0")) {
            if (commissionButton.getText().toString().equals(mContext.getResources().getString(R.string.fragment_product_commission))) {
                AppUtils.showAlertToast(mContext.getString(R.string.fragment_purchase_warning_message2), mContext);
                return;
            }
        }

        boolean addProduct = true;


//        if (SaveCart.getInstance().getSelectVendorCart().getSingleItems().isEmpty()) {
//
//            SingleItem singleItem = new SingleItem();
//                    singleItem.setId(newProductWrapper.getId());
//                    singleItem.setProduct(newProductWrapper);
//                    singleItem.setPrice(newProductWrapper.getPrice());
//                    singleItem.setTotalAmount(newProductWrapper.getPrice() );
//                    singleItem.setOrderedQuantity(newProductWrapper.getCount() + "");
//
//
//            SaveCart.getInstance().getSelectVendorCart().getSingleItems().add(singleItem);
//        } else {
//            for (Object object : myOrderModelList) {
//                if (object instanceof ProductWrapper) {
//                    if (((ProductWrapper) object).getId().equals(newProductWrapper.getId())) {
//                        if (((ProductWrapper) object).getCommissionListQantity() == null && ((ProductWrapper) object).getCommissionListAttribute() == null) {
//                            if (newProductWrapper.getCommissionListQantity() == null && newProductWrapper.getCommissionListAttribute() == null) {
//                                Integer totalCount = Integer.parseInt(((ProductWrapper) object).getCount()) + Integer.parseInt(newProductWrapper.getCount());
//                                if (totalCount <= MaxQtyItem.MAX_QTY) {
//                                    ((ProductWrapper) object).setCount(String.valueOf(totalCount));
//                                    addProduct = false;
//                                } else {
//                                    Toast.makeText(mContext, mContext.getString(R.string.you_have_exceeded_the_quantity_allowable), Toast.LENGTH_SHORT).show();
//                                    return;
//                                }
//                            }
//                        } else if (newProductWrapper.getCommissionListQantity() != null || newProductWrapper.getCommissionListAttribute() != null) {
//                            Integer totalCount = Integer.parseInt(((ProductWrapper) object).getCount()) + Integer.parseInt(newProductWrapper.getCount());
//
//                            if (totalCount <= MaxQtyItem.MAX_QTY) {
//                                ((ProductWrapper) object).setCount(String.valueOf(totalCount));
//                                ((ProductWrapper) object).setCount(String.valueOf(totalCount));
//                                ((ProductWrapper) object).setSelectedCommission(newProductWrapper.getSelectedCommission());
//                                ((ProductWrapper) object).setCommissionQtyToValueTransient(newProductWrapper.getCommissionQtyToValueTransient());
//                                ((ProductWrapper) object).setCommissionListQantity(newProductWrapper.getCommissionListQantity());
//                                ((ProductWrapper) object).setCommissionListAttribute(newProductWrapper.getCommissionListAttribute());
//                                addProduct = false;
//
//                            } else {
//                                Toast.makeText(mContext, mContext.getString(R.string.you_have_exceeded_the_quantity_allowable), Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//
//
//                            //boolean addIndex = true;
//                            //List<PromotionValueQuantity> newCommisionList = new ArrayList<>();
//                           /*   for(PromotionValueQuantity  newCommission :newProductWrapper.getCommissionListQantity()){
//                                  for(PromotionValueQuantity  commission :((ProductWrapper)object).getCommissionListQantity()){
//                                      if(newCommission.getFreeItem()==commission.getFreeItem() && newCommission.getQuantity()==commission.getQuantity()){
//                                          Integer countTotal = Integer.parseInt(commission.getCount()) + Integer.parseInt(newCommission.getCount());
//                                          commission.setCount(String.valueOf(countTotal));
//                                          addIndex = false ;
//                                      }
//                                  }
//                                  if(addIndex){
//                                      newCommisionList.add(newCommission);
//                                  } else {
//                                      addIndex = true ;
//                                  }
//                              }*/
//                            //((ProductWrapper)object).getCommissionList().addAll(newCommisionList);
//
//                        }
//                    }
//                }
//            }
//            if (addProduct) {
//                myOrderModelList.add(newProductWrapper);
//            }
//        }


        if (SaveCart.getInstance().getProductCount(productWrapper) <= MaxQtyItem.MAX_QTY) {
            countText.setText("0");
            commissionButton.setText(mContext.getResources().getString(R.string.fragment_product_commission));
            holder.productImageCopy.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    // Do what you need to do here.
                    // Then remove the listener:
                    SaveCart.getInstance().SaveSingleItem(newProductWrapper);
                    addToCartAnim.onQuantityChange(holder.productImageCopy, pos);
                    holder.productImageCopy.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

            });

        } else {
            Toast.makeText(mContext, mContext.getString(R.string.you_have_exceeded_the_quantity_allowable), Toast.LENGTH_SHORT).show();
            return;
        }


    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView name, alternativeName, price, points, totalPrice, expiryDate,
                discount, oldPrice, addToFavMassage;
        final EditText purchaseNumber;
        final ImageView productImage;
        final ImageView imageMinus, favoritesImage;
        final ImageView imagePlus;
        ImageView productImageCopy;
        final Button addToCart, commission;
        final ConstraintLayout layout, mainLayout;
        final LinearLayout plusMinusActions, expiaryDateLayout, discountLinear;
        final SpinKitView loaderView;

        private MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.text_product_name);
            addToFavMassage = view.findViewById(R.id.AddToFavMassage_TextView);
            loaderView = view.findViewById(R.id.pagination_loading_view);
            alternativeName = view.findViewById(R.id.text_product_alternative_name);
            productImage = view.findViewById(R.id.image_product);
            imageMinus = view.findViewById(R.id.image_minus);
            imagePlus = view.findViewById(R.id.image_plus);
            favoritesImage = view.findViewById(R.id.Favarit_ImageView);
            price = view.findViewById(R.id.text_product_price);
            totalPrice = view.findViewById(R.id.text_total_price);
            expiryDate = view.findViewById(R.id.textView_expiryDate);
            points = view.findViewById(R.id.text_product_points);
            purchaseNumber = view.findViewById(R.id.purchases_num);
            addToCart = view.findViewById(R.id.button_add_to_cart);
            commission = view.findViewById(R.id.button_commission);
            layout = view.findViewById(R.id.layout);
            mainLayout = view.findViewById(R.id.main_layout);
            plusMinusActions = view.findViewById(R.id.plus_minus_actions);
            expiaryDateLayout = view.findViewById(R.id.linerLayout_expiryDate);
            discount = view.findViewById(R.id.Discount_TextView);
            oldPrice = view.findViewById(R.id.OldPrice_TextView);
            discountLinear = view.findViewById(R.id.Discount_LinerLayout);
            productImageCopy = view.findViewById(R.id.image_product_copy);
        }
    }
}

