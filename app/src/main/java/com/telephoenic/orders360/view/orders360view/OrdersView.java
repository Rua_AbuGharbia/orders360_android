package com.telephoenic.orders360.view.orders360view;

import android.app.Activity;
import android.view.View;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class OrdersView {
    private int[] valid = null;
    private ArrayList<Orders360InputView> orders360InputViews = new ArrayList<>();

    public void bind(final Activity target) {
        bindOrders(target, target.getClass().getDeclaredFields(), null);
    }

    public void bind(final Object target, View contentView) {
        bindOrders(target, target.getClass().getDeclaredFields(), contentView);
    }

    private void bindOrders(final Object obj, Field[] fields, View contentView) {
        orders360InputViews.clear();
        for (final Field field : fields) {
            Annotation annotation = field.getAnnotation(BindInput.class);
            Annotation annotation2 = field.getAnnotation(Validation.class);
            if (annotation != null) {
                BindInput bindView = (BindInput) annotation;
                Validation validation = (Validation) annotation2;
                int id = bindView.value();
                Orders360InputView orders360InputView;
                if (contentView == null){
                    orders360InputView = ((Activity) obj).findViewById(id);
                    if (valid!=null )
                    orders360InputView.setValdit(valid);
                }
                else{
                    orders360InputView = contentView.findViewById(id);
                    if (valid!=null )
                        orders360InputView.setValdit(valid);
                    orders360InputViews.add(orders360InputView);
                }
                try {
                    field.setAccessible(true);
                    field.set(obj, orders360InputView);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
