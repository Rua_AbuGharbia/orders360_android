package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ImagesModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.FadePageTransformer;
import com.telephoenic.orders360.view.adapters.FullScreenImageAdapter;
import com.telephoenic.orders360.view.adapters.InfinitePagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ProductDetailsDialog extends AlertDialog {
    public static int BUNDLE = 1;
    public static int PRODUCT = 2;
    private Context context;
    private ViewPager productsPhotos;
    private String imagesName, productDescription, productName;
    private int productID;
    private int viewStatus;
    private List<ImageView> imageViews;
    private LinearLayout imagesLayout;
    private List<ImagesModel> imagesModelList;
    private Double retailPrice = 0.0;

    public ProductDetailsDialog(Context context, String productName, String imagesName, String productDescription, int productID, int viewStatus) {
        super(context);
        this.context = context;
        this.productName = productName;
        this.imagesName = imagesName;
        this.productDescription = productDescription;
        this.productID = productID;
        this.viewStatus = viewStatus;
    }

    public ProductDetailsDialog(Context context, String productName, String imagesName, String productDescription, Double retailPrice, int productID, int viewStatus) {
        super(context);
        this.context = context;
        this.productName = productName;
        this.imagesName = imagesName;
        this.productDescription = productDescription;
        this.productID = productID;
        this.viewStatus = viewStatus;
        this.retailPrice = retailPrice;
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_product_details, null);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(dialogView);


        TextView closeTextView = dialogView.findViewById(R.id.text_close);
        closeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        TextView textDescription = dialogView.findViewById(R.id.text_description);
        textDescription.setMovementMethod(new ScrollingMovementMethod());

        if (productDescription != null) {
            textDescription.setText(productDescription);
        }

        TextView textName = dialogView.findViewById(R.id.text_product_name);
        textName.setText(productName);
        TextView reatilPriceText = dialogView.findViewById(R.id.reatail_price);
        TextView tital = dialogView.findViewById(R.id.title);
        if (viewStatus == PRODUCT) {
            reatilPriceText.setVisibility(View.VISIBLE);
            tital.setVisibility(View.VISIBLE);

            if (retailPrice !=null && retailPrice>0){
                reatilPriceText.setText(" " + String.format(Locale.ENGLISH, "%.2f", retailPrice) + " " + AppConstants.CURRENCY);;
            }else {
                reatilPriceText.setVisibility(View.GONE);
                tital.setVisibility(View.GONE);
            }
        }

        productsPhotos = dialogView.findViewById(R.id.pager);
        imagesLayout = dialogView.findViewById(R.id.layout_images_number);

        imagesModelList = new ArrayList<>();

        if (imagesName != null) {
            if (imagesName.contains(";")) {
                String[] imageList = imagesName.split(";");
                for (String s : imageList) {
                    imagesModelList.add(new ImagesModel(productID, s));
                }
            } else {
                imagesModelList.add(new ImagesModel(productID, imagesName));
            }
            FullScreenImageAdapter adapter = new FullScreenImageAdapter((Activity) context, imagesModelList, String.valueOf(System.currentTimeMillis()), viewStatus);
            productsPhotos.setAdapter(adapter);
            if (imagesModelList.size() > 1) {
                initViews();

            } else {
                imagesLayout.setVisibility(View.GONE);
            }
        } else {
            imagesModelList.add(new ImagesModel(productID, imagesName));
            FullScreenImageAdapter adapter = new FullScreenImageAdapter((Activity) context, imagesModelList, String.valueOf(System.currentTimeMillis()), viewStatus);
            productsPhotos.setAdapter(adapter);

        }


        productsPhotos.setPageTransformer(true, new FadePageTransformer());

        productsPhotos.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                changeViews();
            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
    }

    private void initViews() {
        imageViews = new ArrayList<>();
        //LinearLayout, which contains several child TextViews and ImageViews ...
        for (int i = 0; i < imagesModelList.size(); i++) {
            ImageView imageView = new ImageView(context);
            if (i == 0) {
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.loading1));
            } else {
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.loading2));
            }
            imageView.setPaddingRelative(6, 0, 6, 0);
            imageViews.add(imageView);
            imagesLayout.addView(imageView);
        }
        changeViews();
    }

    private void changeViews() {
        //LinearLayout, which contains several child TextViews and ImageViews ...
        for (int i = 0; i < imagesLayout.getChildCount(); i++) {
            ImageView imageView = ((ImageView) imagesLayout.getChildAt(i));
            if (productsPhotos.getCurrentItem() == i) {
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.loading1));
            } else {
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.loading2));
            }
        }
    }
}