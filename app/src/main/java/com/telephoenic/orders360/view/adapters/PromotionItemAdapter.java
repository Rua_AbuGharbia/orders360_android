package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PromotionItemModel;
import com.telephoenic.orders360.controller.viewholder.PromotionItemViewHolder;

import java.util.ArrayList;

public class PromotionItemAdapter extends RecyclerView.Adapter<PromotionItemViewHolder> {
    private Context context;
    private ArrayList<PromotionItemModel> data;

    public PromotionItemAdapter(Context context, ArrayList<PromotionItemModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public PromotionItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_item_price_promotion_details, parent, false);
        return new PromotionItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PromotionItemViewHolder holder, int position) {
        holder.titleTextView.setText(data.get(position).getTitle());
        holder.numberTextView.setText(data.get(position).getQuantity());
        if (position == data.size()-1){
            holder.lineView.setVisibility(View.GONE);
            holder.numberTextView.setTextColor(context.getResources().getColor(R.color.default_blue_light));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
