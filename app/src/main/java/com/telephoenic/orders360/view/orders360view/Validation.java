package com.telephoenic.orders360.view.orders360view;

import com.telephoenic.orders360.controller.constants.MainFragmentCategories;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

@Retention(RetentionPolicy.RUNTIME)
public @interface Validation {
  MainFragmentCategories[] validation();
}
