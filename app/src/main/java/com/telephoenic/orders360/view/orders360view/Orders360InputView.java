package com.telephoenic.orders360.view.orders360view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import java.lang.reflect.Field;
import java.util.List;

public abstract class Orders360InputView extends FrameLayout {
    protected String errorMessage;
    protected boolean isRequired = true;
    protected boolean isAnimationRunning = false;
    protected int [] vlad;

    public Orders360InputView(@NonNull Context context) {
        super(context);
    }

    public Orders360InputView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Orders360InputView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public void  setValdit(int [] vlad){
        this.vlad =vlad;
        Log.e("VVV",vlad[0]+"");
    }

    public abstract void validate();
    public abstract boolean validate2(List<Integer> validationList,Field[] fields);

    public abstract boolean isValid();

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String getErrorMessage() {
        if (errorMessage == null)
            return null;
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    protected void animateIcon(final ImageView imageView, final TextView errorView, final boolean isError, final boolean hasIcon, final int icon) {
        AnimatorSet scaleDown = Orders360InputViewUtils.scaleDownView(imageView);
        final AnimatorSet scaleUp = Orders360InputViewUtils.scaleUpView(imageView);
        scaleDown.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (isError) {
                    imageView.setVisibility(VISIBLE);
                    imageView.setImageResource(R.drawable.ic_warning);
                    errorView.setVisibility(VISIBLE);
                    errorView.setText(errorMessage);
                    Orders360InputViewUtils.shakeAnimation(getContext(), imageView);
                    Orders360InputViewUtils.shakeAnimation(getContext(), errorView);
                } else {
                    if (hasIcon && icon != -1){
                        imageView.setVisibility(VISIBLE);
                        imageView.setImageResource(icon);
                    }else {
                        imageView.setVisibility(GONE);
                    }
                    errorView.setVisibility(GONE);

                }
                scaleUp.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        isAnimationRunning = false;
                    }
                });
                scaleUp.start();
            }
        });
        if (!isAnimationRunning)
            scaleDown.start();
        isAnimationRunning = true;
    }
}
