package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SelectVendorDialog extends DialogFragment {
    public TextView textViewMessage;
    Unbinder unbinder1;
    private Dialog dialog;
    private String message;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.select_vendor_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ButterKnife.bind(this, dialog);
            textViewMessage = dialog.findViewById(R.id.textView4);
            PosProfileWrapper posProfileWrapper = AppUtils.getProfile(getActivity());
            if (!posProfileWrapper.getUser().isAssignVendor()) {
                textViewMessage.setText(R.string.no_vendors_assigned_to_the_user);
            }
        }
        return dialog;
    }


    @OnClick(R.id.Ok_TextView)
    public void onViewClicked() {
        dialog.dismiss();
    }

}
