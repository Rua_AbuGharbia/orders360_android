package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.viewholder.PaymentDetailsViewHolder;

import java.util.List;

public class PaymentDetailsAdapter extends RecyclerView.Adapter<PaymentDetailsViewHolder> {
    private Context context;
    private List<String> data;

    public PaymentDetailsAdapter(Context context, List<String> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public PaymentDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_payment_details, viewGroup, false);
        return new PaymentDetailsViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentDetailsViewHolder paymentDetailsViewHolder, int i) {
        paymentDetailsViewHolder.paymentDetailsTextView.setText(data.get(i));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
