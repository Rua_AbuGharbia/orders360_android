package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.application.Order360Application;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.ProductWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.CommissionDialog;
import com.telephoenic.orders360.view.customcontrols.ProductDetailsDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ProductsWithPromotionAdapter extends RecyclerView.Adapter<ProductsWithPromotionAdapter.MyViewHolder> {

    private Context mContext;
    private List<ProductWrapper> data;
    private View.OnClickListener clickListener;
    private PosProfileWrapper posProfileWrapper;
    private String imagesName;
    private String type;
    private boolean isChanged = true;
    private int cellSpan;
    private OnRecyclerClick onRecyclerClick;
    private String signature;

    public ProductsWithPromotionAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects, OnRecyclerClick onRecyclerClick) {
        mContext = context;
        data = objects;
        this.onRecyclerClick = onRecyclerClick;
        posProfileWrapper = AppUtils.getProfile(context);
    }

    public ProductsWithPromotionAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects, String type) {
        mContext = context;
        data = objects;
        this.type = type;
        posProfileWrapper = AppUtils.getProfile(context);

    }

    public ProductsWithPromotionAdapter(Context context, int textViewResourceId, List<ProductWrapper> objects, OnRecyclerClick onRecyclerClick, String type, int cellSpan, String signature) {
        mContext = context;
        data = objects;
        this.type = type;
        this.onRecyclerClick = onRecyclerClick;
        posProfileWrapper = AppUtils.getProfile(context);
        this.cellSpan = cellSpan;
        this.signature = signature;

    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_with_promotion, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ProductWrapper productWrapper = data.get(position);
        ScreenDensityUtil.setWidthCellSize(holder.layout, cellSpan);
        holder.loaderView.setVisibility(View.VISIBLE);


        holder.setIsRecyclable(false);

        if (productWrapper.isCanDelete()) {
            holder.imageClose.setVisibility(View.VISIBLE);
        } else {
            holder.imageClose.setVisibility(View.GONE);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat formatterExpair = new SimpleDateFormat("MM/yyyy", Locale.ENGLISH);

        if (productWrapper.getEndDate() != null) {
            String endDateString = formatter.format(new Date(Long.valueOf(productWrapper.getEndDate())));
            Log.d("Promotion Adapter", endDateString);
            holder.endDate.setText(endDateString);
        }

        if (AppConstants.PROMOTION_TYPE_ATTRIBUTE.equals(AppPrefs.getVendorType(mContext))) {
            holder.commission.setText(mContext.getResources().getString(R.string.label_details));
            holder.endDate.setVisibility(View.GONE);
            holder.endDateTitle.setVisibility(View.GONE);
        }

        if (productWrapper.getExpiryDate() != null) {
            String expireDateString = formatterExpair.format(new Date(Long.valueOf(productWrapper.getExpiryDate())));
            holder.expiryDate.setText(expireDateString);
        } else {
            holder.expiryDate.setVisibility(View.GONE);
            holder.expiryDateTitle.setVisibility(View.GONE);

        }

//        holder.imageClose.setTag(position);
//        holder.imageClose.setOnClickListener(clickListener);

        holder.name.setText(productWrapper.getName());
        holder.alternativeName.setText(productWrapper.getAlternativeName());

       // holder.price.setText(" " + String.format(Locale.ENGLISH, "%.2f", productWrapper.getPrice()) + " " + AppConstants.CURRENCY);
        holder.price.setText(String.format(" %s %s", AppUtils.amountFormat(productWrapper.getPrice()), AppConstants.CURRENCY));



        //  holder.commission.setOnClickListener(clickListener);
        holder.commission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecyclerClick.getPosition(position);
            }
        });

        holder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(mContext,
                        productWrapper.getName(), productWrapper.getImagesName(), productWrapper.getDescription(), productWrapper.getRetailPrice(), productWrapper.getId(), ProductDetailsDialog.PRODUCT);
                productDetailsDialog.show();
            }
        });
        try {
            //TODO set Image from server
            imagesName = productWrapper.getImagesName();
            if (imagesName == null || TextUtils.isEmpty(imagesName)) {
                imagesName = "Empty";
            }

            Integer imageID = productWrapper.getId();
            if (imagesName != null || !TextUtils.isEmpty(imagesName)) {
                if (imagesName.contains(";")) {
                    imagesName = imagesName.split(";")[0];
                }

                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                        // .dontTransform()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                holder.loaderView.setVisibility(View.GONE);
                                Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                        .signature(new StringSignature(String.valueOf(signature)))
                                        .error(R.drawable.place_holder_order)
                                        .into(holder.productImage);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                holder.loaderView.setVisibility(View.GONE);

                                return false;
                            }
                        })
                        //  .crossFade(800)
                        .dontTransform()
                        .crossFade(800)
                        .thumbnail(0.5f)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(holder.productImage);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView name, alternativeName, price, expiryDate, expiryDateTitle;
        final TextView endDate, endDateTitle;
        final ImageView productImage, imageClose;
        final Button commission;
        final ConstraintLayout layout;
        final SpinKitView loaderView;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.text_product_name);
            alternativeName = view.findViewById(R.id.text_product_alternative_name);
            productImage = view.findViewById(R.id.image_product);
            imageClose = view.findViewById(R.id.image_close);
            price = view.findViewById(R.id.text_product_price);
            expiryDate = view.findViewById(R.id.textView_expiryDate);
            expiryDateTitle = view.findViewById(R.id.textView_expiryDate_title);
            // points = view.findViewById(R.id.text_product_points);
            loaderView = view.findViewById(R.id.pagination_loading_view);
            commission = view.findViewById(R.id.button_commission);
            endDate = view.findViewById(R.id.text_end_date);
            layout = view.findViewById(R.id.main_layout);
            endDateTitle = view.findViewById(R.id.text_end_date_title);
        }
    }
}
