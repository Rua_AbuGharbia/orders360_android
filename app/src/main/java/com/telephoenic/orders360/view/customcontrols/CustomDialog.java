package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.NoConnectivityException;
import com.telephoenic.orders360.basemodel.data.networkutils.NetworkConnectionUtils;
import com.telephoenic.orders360.controller.application.Order360Application;

import java.util.Objects;


public class CustomDialog extends Dialog implements
		View.OnClickListener {

	private Button okButton;
	private TextView titleTextView;
	private TextView messageTextView;
	private String title;
	private String message;
	private Fragment activity;

	public CustomDialog(Context context) {
		super(context);
		this.title = "";
		this.message = "";
	}
	public CustomDialog(Context context,Fragment activity) {
		super(context);
		this.title = "";
		this.message = "";
		this.activity = activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.custom_dialog);

		if (getWindow() != null) {
			getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		    getWindow().setGravity(Gravity.CENTER);
		}

		okButton =  findViewById(R.id.button_customdialog_ok);
		okButton.setOnClickListener(this);
		
		titleTextView =  findViewById(R.id.textView_customdialog_title);
		titleTextView.setText(title);
		
		messageTextView =  findViewById(R.id.textView_customdialog_message);
		messageTextView.setText(message);
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (NetworkConnectionUtils.isNetworkAvailable(Order360Application.getInstance().getApplicationContext())) {
				//	Objects.requireNonNull(getOwnerActivity()).recreate();
					activity.onAttach(getContext());
					dismiss();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		this.dismiss();
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the okButton
	 */
	public Button getOkButton() {
		return okButton;
	}

	/**
	 * @param okButton the okButton to set
	 */
	public void setOkButton(Button okButton) {
		this.okButton = okButton;
	}
}
