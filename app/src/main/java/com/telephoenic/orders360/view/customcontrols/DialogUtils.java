package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;

import java.util.Objects;

public class DialogUtils {

    private static Dialog dialog;

    private static OnDialogClickListener listener;

    public interface OnDialogClickListener {
        void onOkClicked();

        void onCancelClicked();
    }

    public static void showLocationPermissionDialog(Context context, String title, String message, DialogUtils.OnDialogClickListener listener) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.location_permission_dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        TextView locationPermissionTitleTextView = dialog.findViewById(R.id.location_permission_dialog_title_textView);
        TextView locationPermissionMessageTextView = dialog.findViewById(R.id.location_permission_dialog_message_textView);

        locationPermissionTitleTextView.setText(title);
        locationPermissionMessageTextView.setText(message);

        final DialogUtils.OnDialogClickListener mlistener = listener;
        (dialog.findViewById(R.id.location_permission_dialog_ok_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mlistener.onOkClicked();
                    }
                });

        (dialog.findViewById(R.id.location_permission_dialog_cancel_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mlistener.onCancelClicked();
                    }
                });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void showConfirmationDialog(Context context, String title, String message, DialogUtils.OnDialogClickListener listener) {
        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.confirm_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.CENTER);

        TextView confirmTitleTextView = dialog.findViewById(R.id.confirm_dialog_title_textView);
        TextView confirmMessageTextView = dialog.findViewById(R.id.confirm_dialog_message_textView);

        confirmTitleTextView.setText(title);
        confirmMessageTextView.setText(message);

        final DialogUtils.OnDialogClickListener mlistener = listener;
        (dialog.findViewById(R.id.confirm_dialog_yes_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mlistener.onOkClicked();
                    }
                });

        (dialog.findViewById(R.id.confirm_dialog_no_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mlistener.onCancelClicked();
                    }
                });

        dialog.show();
    }

    public static void showSettingsDialog(final Context context, SettingsDialogModel settingsDialogModel, DialogUtils.OnDialogClickListener listener) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.settings_dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (settingsDialogModel.getDialog_image() != 0) {
            ImageView imageView = dialog.findViewById(R.id.settings_dialog_settings_icon_imageView);
            imageView.setImageResource(settingsDialogModel.getDialog_image());
        }

        TextView titleTextView = dialog.findViewById(R.id.settings_dialog_settings_title_textView);
        titleTextView.setText(settingsDialogModel.getDialog_title());

        TextView messageTextView = dialog.findViewById(R.id.settings_dialog_settings_message_textView);
        messageTextView.setText(settingsDialogModel.getDialog_message());

        final DialogUtils.OnDialogClickListener mlistener = listener;
        (dialog.findViewById(R.id.settings_dialog_cancel_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mlistener.onCancelClicked();
                    }
                });

        (dialog.findViewById(R.id.settings_dialog_settings_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
//                        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        mlistener.onOkClicked(); // when Settings Clicked.
                    }
                });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void showSettingsDialog(final Context context, SettingsDialogModel settingsDialogModel) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.settings_dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (settingsDialogModel.getDialog_image() != 0) {
            ImageView imageView = dialog.findViewById(R.id.settings_dialog_settings_icon_imageView);
            imageView.setImageResource(settingsDialogModel.getDialog_image());
        }

        TextView titleTextView = dialog.findViewById(R.id.settings_dialog_settings_title_textView);
        titleTextView.setText(settingsDialogModel.getDialog_title());

        TextView messageTextView = dialog.findViewById(R.id.settings_dialog_settings_message_textView);
        messageTextView.setText(settingsDialogModel.getDialog_message());

        (dialog.findViewById(R.id.settings_dialog_cancel_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        (dialog.findViewById(R.id.settings_dialog_settings_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
