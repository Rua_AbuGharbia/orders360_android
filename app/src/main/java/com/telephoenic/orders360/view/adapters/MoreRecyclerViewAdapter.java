package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.MoreItemChild;
import com.telephoenic.orders360.controller.server.model.MoreListItem;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MoreRecyclerViewAdapter
        extends RecyclerView.Adapter<MoreRecyclerViewAdapter.ViewHolder>  {

    private List<MoreListItem> mMoreListItems;
    private Context mContext;
    private OnItemClickListener mClickListener;
    private MoreChildAdapter.OnItemClickListener childClickListener;

    public MoreRecyclerViewAdapter(List<MoreListItem> mMoreListItems, Context mContext, OnItemClickListener mClickListener, MoreChildAdapter.OnItemClickListener childClickListener) {
        this.mMoreListItems = mMoreListItems;
        this.mContext = mContext;
        this.mClickListener = mClickListener;
        this.childClickListener = childClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.menu_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        MoreListItem listItem = mMoreListItems.get(position);
        holder.moreTextView.setText(listItem.getName());
        holder.moreImageView.setImageResource(listItem.getImage());
        if (listItem.getMoreItemChildren() != null) {
            if (listItem.isShowItem()) {
                holder.imageRotation.setVisibility(View.VISIBLE);
                MoreChildAdapter moreChildAdapter = new MoreChildAdapter(listItem.getMoreItemChildren(), mContext, childClickListener);
                holder.recyclerView.setAdapter(moreChildAdapter);
                holder.recyclerView.setVisibility(View.VISIBLE);
                holder.imageRotation.setRotation(90);
            } else {
                holder.imageRotation.setVisibility(View.VISIBLE);
                MoreChildAdapter moreChildAdapter = new MoreChildAdapter(listItem.getMoreItemChildren(), mContext, childClickListener);
                holder.recyclerView.setAdapter(moreChildAdapter);
                holder.recyclerView.setVisibility(View.GONE);
                holder.imageRotation.setRotation(270);
            }

        }else {
            holder.recyclerView.setVisibility(View.GONE);
            holder.imageRotation.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        return mMoreListItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView moreImageView;
        private TextView moreTextView;
        private RecyclerView recyclerView;
        private ImageView imageRotation;

        ViewHolder(View itemView) {
            super(itemView);
            moreImageView = itemView.findViewById(R.id.menu_list_item_imageView);
            moreTextView = itemView.findViewById(R.id.menu_list_item_textView);
            recyclerView = itemView.findViewById(R.id.RecyclerView);
            imageRotation = itemView.findViewById(R.id.Image_Rotation);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(mMoreListItems.get(getAdapterPosition()), getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(MoreListItem listItem, int position);
    }
}
