package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipModel;
import com.telephoenic.orders360.controller.utils.DateUtil;
import com.telephoenic.orders360.controller.viewholder.DeliveryOrdersViewHolder;

import java.util.List;
import java.util.Locale;

public class PosUserShipOrdersAdapter extends RecyclerView.Adapter<DeliveryOrdersViewHolder> {
    private Context context;
    private OnRecyclerClick onRecyclerClick;
    private List<ShipModel> data;

    public PosUserShipOrdersAdapter(Context context, List<ShipModel> data, OnRecyclerClick onRecyclerClick) {
        this.context = context;
        this.onRecyclerClick = onRecyclerClick;
        this.data = data;
    }

    @NonNull
    @Override
    public DeliveryOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_order_ship, viewGroup, false);
        return new DeliveryOrdersViewHolder(myRoot);
    }


    @SuppressLint("SetTextI18n")
    @Override

    public void onBindViewHolder(@NonNull DeliveryOrdersViewHolder holder, final int i) {

        if (data.get(i).getAmount() > 0) {
            holder.arrowGo.setVisibility(View.VISIBLE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerClick != null) {
                        onRecyclerClick.getPosition(i);
                    }
                }
            });
            holder.totalAmountRowOrderDeliveryTextView.setVisibility(View.VISIBLE);
            holder.totalAmountRowOrderDeliveryTextView.setText("Total Amount : " + String.format(Locale.ENGLISH, "%.2f", data.get(i).getAmount()) + " " + AppConstants.CURRENCY);
        } else {
            holder.arrowGo.setVisibility(View.GONE);
            holder.totalAmountRowOrderDeliveryTextView.setVisibility(View.GONE);
        }


        holder.orderIdRowOrderDeliveryTextView.setText(String.format("%s%s", context.getString(R.string.order_id), data.get(i).getOrderNumber()));
        holder.shipNumberRowOrderDeliveryTextView.setText(String.format("%s%s", context.getString(R.string.shipment_number), data.get(i).getShipmentNumber()));
        holder.deliveryStatusRowOrderDeliveryTextView.setText(String.format("%s%s", context.getString(R.string.orders_status), data.get(i).getStatus()));
        if (data.get(i).getInvoiceNumber() != 0) {
            holder.cityRowOrderDeliveryTextView.setVisibility(View.VISIBLE);
            holder.cityRowOrderDeliveryTextView.setText(String.format("%s%s", context.getString(R.string.invoice_no), data.get(i).getInvoiceNumber()));
        } else {
            holder.arrowGo.setVisibility(View.GONE);
            holder.cityRowOrderDeliveryTextView.setVisibility(View.GONE);
        }


        String dateString = DateUtil.formatDate(DateUtil.parseDate((data.get(i).getOrderDate())));
        String shipDate = DateUtil.formatDate(DateUtil.parseDate((data.get(i).getShipmentDate())));


        holder.shipDateRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.ship_date), shipDate));
        holder.orderDateRowOrderDeliveryTextView.setText(String.format("%s : %s", context.getString(R.string.order_date), dateString));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
