package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {
    private Context mContext;
    private List<CategoryNodeWrapper> data;
    View.OnClickListener clickListener ;
    PosProfileWrapper posProfileWrapper;


    private boolean isChanged = true;

    public CategoriesAdapter(Context context, int textViewResourceId, List<CategoryNodeWrapper> objects , View.OnClickListener clickListener) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper  = AppUtils.getProfile(context);
    }

    public CategoriesAdapter(Context context, int textViewResourceId, List<CategoryNodeWrapper> objects ) {
        mContext = context;
        data = objects;
        posProfileWrapper  = AppUtils.getProfile(context);

    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {
        holder.setIsRecyclable(false);
        final CategoryNodeWrapper categoryWrapper= data.get(holder.getAdapterPosition());

        holder.name.setText(categoryWrapper.getName());

        holder.name.setTag(position);
        holder.imageArrow.setTag(position);
        holder.layout.setTag(position);

        holder.name.setOnClickListener(clickListener);
        holder.imageArrow.setOnClickListener(clickListener);
        holder.layout.setOnClickListener(clickListener);


    }


    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView name ;
        final ImageView  imageArrow;
        final ConstraintLayout layout ;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.category_name);
            imageArrow= view.findViewById(R.id.imageView_arrow);
            layout = view.findViewById(R.id.layout);
        }
    }
}
