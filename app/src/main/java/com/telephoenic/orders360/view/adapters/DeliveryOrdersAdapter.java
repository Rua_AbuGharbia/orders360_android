package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.viewholder.DeliveryOrdersViewHolder;

public class DeliveryOrdersAdapter extends RecyclerView.Adapter<DeliveryOrdersViewHolder> {
    private Context context;
    private OnRecyclerClick onRecyclerClick;

    public DeliveryOrdersAdapter(Context context, OnRecyclerClick onRecyclerClick) {
        this.context = context;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public DeliveryOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_order_delivery, viewGroup, false);
        return new DeliveryOrdersViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryOrdersViewHolder holder, final int i) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClick != null) {
                    onRecyclerClick.getPosition(i);
                }
            }
        });
        holder.arrowGo.setVisibility(View.VISIBLE);
        holder.cityRowOrderDeliveryTextView.setText("City : Amman");
        holder.regionRowOrderDeliveryTextView.setText("Region : Khalda");
        holder.totalAmountRowOrderDeliveryTextView.setText("Total Amount : 500 JD");
        holder.orderIdRowOrderDeliveryTextView.setText("OrderID : 19/00049");
        holder.namePosUserRowOrderDeliveryTextView.setText("Smart Buy");
        holder.orderDateRowOrderDeliveryTextView.setText("Order Date : 14/11/2019");
        holder.shipDateRowOrderDeliveryTextView.setText("Ship Date : 30/11/2019");
        holder.deliveryStatusRowOrderDeliveryTextView.setText("Delivery Status : Shipped");
    }

    @Override
    public int getItemCount() {
        return 8;
    }
}
