package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.fragments.ChooseSurveyFragment;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.survey.SurveyResponse;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.viewholder.SurveyViewHolder;
import com.telephoenic.orders360.model.AppPrefs;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyViewHolder> {
    private Context mContext;
    private List<SurveyResponse> mList;
    private String signature;
    private OnRecyclerClick onRecyclerClick;


    public SurveyAdapter(Context context, List<SurveyResponse> objects, String signature, OnRecyclerClick onRecyclerClick) {
        mContext = context;
        mList = objects;
        this.signature = signature;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public SurveyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(mContext).inflate(R.layout.item_survey, parent, false);
        return new SurveyViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull SurveyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        String startDateString = formatter.format(new Date(Long.valueOf(mList.get(position).getStartDate())));
        String endDateString = formatter.format(new Date(Long.valueOf(mList.get(position).getEndDate())));

        holder.startDate.setText(startDateString);
        holder.endDate.setText(endDateString);

        if (mList.get(position).getSubmittedStatus() == ChooseSurveyFragment.surveyStatus.SUBMITTED) {
            holder.imageArrow.setVisibility(View.GONE);
            holder.submitText.setText(mContext.getString(R.string.label_submitted));
        } else {
            holder.submitText.setText(mContext.getString(R.string.text_new));
            holder.imageArrow.setVisibility(View.VISIBLE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerClick != null)
                        onRecyclerClick.getPosition(position);
                }
            });
        }

        holder.points.setText(mList.get(position).getPoints() == null ? "+ 0 Pts" : "+ " + mList.get(position).getPoints() + " Pts");
        holder.vendorName.setText(mList.get(position).getVendorName());
        holder.txtTitle.setText(mList.get(position).getTitle());
        try {
            //TODO set Image from server
            String imagesName = mList.get(position).getLogoImageName();

            if (imagesName != null) {
                if (imagesName.contains(";")) {
                    imagesName = imagesName.split(";")[0];
                }
                Long imageID = -1L;
                if (AppPrefs.getVendorID(mContext) != -1) {
                    imageID = AppPrefs.getVendorID(mContext);
                }
                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "vendor/attachments/" + imageID + "/" + imagesName)
                        .error(R.drawable.place_holder_order)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(holder.imageVendor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}