package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ComboWrapper;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.ScreenDensityUtil;
import com.telephoenic.orders360.model.AppPrefs;
import com.telephoenic.orders360.view.customcontrols.ProductDetailsDialog;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ComboAdapter extends RecyclerView.Adapter<ComboAdapter.MyViewHolder> {
    private Context mContext;
    private List<ComboWrapper> data;
    View.OnClickListener clickListener;
    PosProfileWrapper posProfileWrapper;
    private int cellSpan;
    private String signature;


    private boolean isChanged = true;

    public ComboAdapter(Context context, int textViewResourceId, List<ComboWrapper> objects, View.OnClickListener clickListener) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper = AppUtils.getProfile(context);
    }

    public ComboAdapter(Context context, int textViewResourceId, List<ComboWrapper> objects, View.OnClickListener clickListener, int cellSpan,String signature) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper = AppUtils.getProfile(context);
        this.cellSpan = cellSpan;
        this.signature =signature;
    }

    public ComboAdapter(Context context, int textViewResourceId, List<ComboWrapper> objects) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        posProfileWrapper = AppUtils.getProfile(context);
    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_combo, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ComboWrapper comboWrapper = data.get(position);
        ScreenDensityUtil.setWidthCellSize(holder.layout, cellSpan);
        holder.loaderView.setVisibility(View.VISIBLE);

        holder.setIsRecyclable(false);

        if (comboWrapper.isCanDeleted()) {
            holder.imageClose.setVisibility(View.VISIBLE);
        } else {
            holder.imageClose.setVisibility(View.GONE);
        }


        holder.name.setText(comboWrapper.getName());

      //  holder.price.setText((float) Math.round(data.get(position).getPrice() * 100) / 100 + " " + AppConstants.CURRENCY);
       // holder.price.setText(String.format(" %s %s", AppUtils.amountFormat(data.get(position).getPrice()), AppConstants.CURRENCY));
        holder.price.setText(" " + AppUtils.amountFormat(data.get(position).getPrice()) + " " + AppConstants.CURRENCY);



        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        if (data.get(position).getEndDate() != null) {
            String endDateString = formatter.format(new Date(Long.valueOf(data.get(position).getEndDate())));
            holder.endDate.setText(endDateString);
        }


        holder.buttonDetails.setTag(position);
        holder.imageClose.setTag(position);

        holder.imageClose.setOnClickListener(clickListener);

        holder.buttonDetails.setOnClickListener(clickListener);
              //  holder.imageCombo.setTag(position);
        holder.imageCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDetailsDialog productDetailsDialog = new ProductDetailsDialog(mContext,
                        data.get(position).getName(), data.get(position).getLogoImageName(), null, data.get(position).getId(), ProductDetailsDialog.BUNDLE);
                productDetailsDialog.show();
            }
        });


        try {

            String imagesName = comboWrapper.getLogoImageName();
            if (imagesName == null || TextUtils.isEmpty(imagesName)){
                imagesName = "Empty";
            }

            int imageID = comboWrapper.getId();
            if (imagesName != null || !TextUtils.isEmpty(imagesName)) {
                if (imagesName.contains(";")) {
                    imagesName = imagesName.split(";")[0];
                }

                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "combo/attachments/" + imageID + "/" + imagesName)
                        // .dontTransform()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                holder.loaderView.setVisibility(View.GONE);
                                Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                                        .signature(new StringSignature(String.valueOf(signature)))
                                        .error(R.drawable.place_holder_order)
                                        .into(holder.imageCombo);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                holder.loaderView.setVisibility(View.GONE);

                                return false;
                            }
                        })
                        //  .crossFade(800)
                        .dontTransform()
                        .crossFade(800)
                        .thumbnail(0.5f)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(holder.imageCombo);
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView name, price;
        final TextView endDate;
        final ImageView imageClose;
        final ImageView imageCombo;
        final Button buttonDetails;
        ConstraintLayout layout;
        final SpinKitView loaderView;


        MyViewHolder(View view) {
            super(view);
            loaderView = view.findViewById(R.id.pagination_loading_view);
            name = view.findViewById(R.id.text_product_name);
            imageClose = view.findViewById(R.id.image_close);
            imageCombo = view.findViewById(R.id.image_combo);
            price = view.findViewById(R.id.text_product_price);
            endDate = view.findViewById(R.id.text_end_date);
            buttonDetails = view.findViewById(R.id.button_details);
            layout = view.findViewById(R.id.main_layout);

        }
    }
}
