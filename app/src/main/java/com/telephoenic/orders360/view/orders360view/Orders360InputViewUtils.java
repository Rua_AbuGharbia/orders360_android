package com.telephoenic.orders360.view.orders360view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.telephoenic.orders360.R;


 class Orders360InputViewUtils {

    static AnimatorSet scaleDownView(View view) {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(view, "scaleX", 0.0f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(view, "scaleY", 0.0f);
        scaleDownX.setDuration(200);
        scaleDownY.setDuration(200);

        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);
        scaleDown.setInterpolator(new DecelerateInterpolator());

        return scaleDown;
    }

    static AnimatorSet scaleUpView(View view) {
        final ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        final ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        scaleUpX.setDuration(200);
        scaleUpY.setDuration(200);

        AnimatorSet scaleUp = new AnimatorSet();
        scaleUp.play(scaleUpX).with(scaleUpY);
        scaleUp.setInterpolator(new DecelerateInterpolator());

        return scaleUp;
    }

    static void shakeAnimation(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.shake_error);
        view.startAnimation(animation);
    }
}
