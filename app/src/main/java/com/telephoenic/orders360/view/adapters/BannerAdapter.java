package com.telephoenic.orders360.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;

import java.util.List;

public class BannerAdapter extends PagerAdapter {
    private Context mContext;
    private List<String> data;
    private List<String> descriptionList;
    private View.OnClickListener onClickListener;
    private LayoutInflater inflater;
    private Activity _activity;
    private String signature;

    public BannerAdapter(Activity activity, List<String> images ,Context context ,String signature /*,View.OnClickListener onClickListener*/ ) {
        this._activity = activity;
        data = images;
        mContext = context;
        this.signature =signature;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.item_banner, container, false);

        final ImageView image = viewLayout.findViewById(R.id.image_banner);
        final SpinKitView loading = viewLayout.findViewById(R.id.pagination_loading_view);
        loading.setVisibility(View.VISIBLE);

        try {
            Glide.with(mContext)
                    .load(AppConstants.URL_LIVE + "vendor_ad/attachments/"+ data.get(position))
                    // .dontTransform()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                            loading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                            loading.setVisibility(View.GONE);

                            return false;
                        }
                    })
                    //  .crossFade(800)
                    .error(R.drawable.ic_ads_place_holder)
                    .signature(new StringSignature(String.valueOf(signature)))
                    .into(image);
        } catch (Exception e) {
            Log.e("Essential Data Error " ,e.toString());
            e.printStackTrace();
        }




        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);

    }

}
