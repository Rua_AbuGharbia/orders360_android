package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.DiscountFilterModel;
import com.telephoenic.orders360.controller.viewholder.DiscountFilterViewHolder;

import java.util.List;

public class DiscountFilterAdapter extends RecyclerView.Adapter<DiscountFilterViewHolder> {
    private Context context;
    private List<DiscountFilterModel> data;
    private OnRecyclerClick onRecyclerClick;

    public DiscountFilterAdapter(Context context, List<DiscountFilterModel> data, OnRecyclerClick onRecyclerClick) {
        this.context = context;
        this.data = data;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public DiscountFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myView = LayoutInflater.from(context).inflate(R.layout.row_discount_filter,parent,false);
        return new DiscountFilterViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(@NonNull DiscountFilterViewHolder holder, final int position) {
        if (data.get(position).isSelect()){
            holder.checkbox.setChecked(true);
        }else {
            holder.checkbox.setChecked(false);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.getPosition(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
