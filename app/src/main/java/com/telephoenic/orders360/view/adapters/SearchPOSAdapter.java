package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.PosUser;
import com.telephoenic.orders360.controller.server.model.User;

import java.util.List;
import java.util.Locale;

public class SearchPOSAdapter extends ArrayAdapter<User> {
    private Context mContext;
    private List<User> data;
    //View.OnClickListener clickListener ;

    public static Locale sysLocale;

    public SearchPOSAdapter(Context context, int textViewResourceId, List<User> objects ) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
       // this.clickListener = clickListener;
        sysLocale = mContext.getResources().getConfiguration().locale;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
//        if (convertView == null) {
            row = LayoutInflater.from(mContext).inflate(R.layout.item_search_pos, null);

        final TextView numberTextView =  row.findViewById(R.id.searchByNumberTxt);
        final TextView nameTextView=  row.findViewById(R.id.searchByNameTxt);

        nameTextView.setTag(position);
        numberTextView.setTag(position);

        nameTextView.setText(data.get(position).getName());
        numberTextView.setText(data.get(position).getMobileNumber());

       /* nameTextView.setOnClickListener(clickListener);
        numberTextView.setOnClickListener(clickListener);*/


        return row;
    }

    @Override
    public User getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
