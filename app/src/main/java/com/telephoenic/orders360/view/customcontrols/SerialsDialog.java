package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.typedef.SerialType;
import com.telephoenic.orders360.controller.activities.BarCodeActivity;
import com.telephoenic.orders360.controller.interfaces.GetSerialsData;
import com.telephoenic.orders360.controller.server.model.AllSerialModel;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


@SuppressLint("ValidFragment")
public class SerialsDialog extends DialogFragment {
    public static final String DATA = " data";
    public static final String SERIAL_TYPE = " serialsType";
    public static final String QUANTITY = " quantity";
    public static final String EDIT_DATA = " editData";
    public static final String POSITION = " position";
    @BindView(R.id.From_TextView)
    TextView fromTextView;
    @BindView(R.id.To_TextView)
    TextView toTextView;
    @BindView(R.id.Scan_ImageView)
    ImageView scanImageView;
    @BindView(R.id.textView19)
    TextView barCodeTextView;
    @BindInput(R.id.FromNumberScan_EditText)
    Orders360EditText fromNumberScanEditText;
    @BindInput(R.id.ToNumberScan_EditText)
    Orders360EditText toNumberScanEditText;
    private Dialog dialog;
    private List<AllSerialModel> allSerialModels;
    private @SerialType
    int serialsType;
    private int serialsQuantity;
    private GetSerialsData getSerialsData;
    private AllSerialModel allSerialModel =new AllSerialModel();
    private AllSerialModel editModeSerial;
    private boolean completeSerials = true;
    private int position;
    private boolean editMode = false;


    public SerialsDialog(GetSerialsData getSerialsData) {
        this.getSerialsData = getSerialsData;
    }

    public SerialsDialog() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.serials_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            if (getArguments() != null) {
                allSerialModels = (List<AllSerialModel>) getArguments().getSerializable(DATA);
                serialsType = getArguments().getInt(SERIAL_TYPE);
                serialsQuantity = getArguments().getInt(QUANTITY);
                editModeSerial = (AllSerialModel) getArguments().getSerializable(EDIT_DATA);
                position = getArguments().getInt(POSITION);
            }
            ButterKnife.bind(this, dialog);
            OrdersView ordersView = new OrdersView();
            ordersView.bind(this, dialog.getWindow().getDecorView());
            inti();
        }
        return dialog;
    }

    @SuppressLint("NewApi")
    private void inti() {
        if (editModeSerial == null) {
            allSerialModel = new AllSerialModel();
        } else {
            editMode = true;
            if (editModeSerial.getSerialType() == SerialType.FROM_TO) {
                toNumberScanEditText.setText(editModeSerial.getToNumber());
                fromNumberScanEditText.setText(editModeSerial.getSerialNumber());
            } else {
                fromNumberScanEditText.setText(editModeSerial.getSerialNumber());
            }
        }
        if (allSerialModels == null)
            allSerialModels = new ArrayList<>();
        if (serialsType == SerialType.FROM_TO) {
            fromTextView.setText("From Serial");
            fromTextView.setVisibility(View.VISIBLE);
            toNumberScanEditText.setVisibility(View.VISIBLE);
            toTextView.setVisibility(View.VISIBLE);
            scanImageView.setVisibility(View.VISIBLE);
        } else if (serialsType == SerialType.SINGLE) {
            fromTextView.setVisibility(View.GONE);
            toNumberScanEditText.setVisibility(View.GONE);
            toTextView.setVisibility(View.GONE);
            scanImageView.setVisibility(View.VISIBLE);
        } else {
            barCodeTextView.setVisibility(View.GONE);
            fromTextView.setVisibility(View.VISIBLE);
            fromTextView.setText("Quantity");
            scanImageView.setVisibility(View.INVISIBLE);
            toNumberScanEditText.setVisibility(View.GONE);
            toTextView.setVisibility(View.GONE);
        }


        fromNumberScanEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int c, int before, int count) {
                if (!s.equals("") && !TextUtils.isEmpty(s.toString())) {
                    allSerialModel.setSerialNumber(s.toString().trim());
                    allSerialModel.setSerialType(serialsType);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        toNumberScanEditText.getContentEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int c, int before, int count) {
                if (!s.equals("") && !TextUtils.isEmpty(s.toString())) {
                    allSerialModel.setToNumber(s.toString().trim());
                    allSerialModel.setSerialType(serialsType);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            fromNumberScanEditText.setText(data.getStringExtra(BarCodeActivity.BAR_CODE) + "");
        }
    }


    @OnClick({R.id.Scan_ImageView, R.id.textView19, R.id.Cancel_ImageView, R.id.Cancel_Buttom, R.id.Save_Buttom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Scan_ImageView:
            case R.id.textView19:
                startActivityForResult(new Intent(getActivity(), BarCodeActivity.class), 201);
                break;
            case R.id.Cancel_ImageView:
            case R.id.Cancel_Buttom:
                dismiss();
                break;
            case R.id.Save_Buttom:
                if (editMode) {
                    allSerialModels.remove(position);
                }
                if (serialsType == SerialType.SINGLE) {
                    if (allSerialModel.getSerialNumber() != null && !TextUtils.isEmpty(allSerialModel.getSerialNumber())) {

                        if (!findItemInTheList(allSerialModel)) {
                            allSerialModels.add(allSerialModel);
                            getSerialsData.getSerialsData(allSerialModels, allSerialModels.size());
                            dismiss();
                        } else {
                            if (editMode)
                                allSerialModels.add(position, editModeSerial);
                            fromNumberScanEditText.setErrorMessage("The serial entered already exists");
                            fromNumberScanEditText.validate();
                        }

                    } else {
                        if (editMode)
                            allSerialModels.add(position, editModeSerial);
                        fromNumberScanEditText.setErrorMessage("please enter serial");
                        fromNumberScanEditText.validate();
                    }

                } else if (serialsType == SerialType.FROM_TO) {
                    completeSerials = true;
                    if (allSerialModel.getSerialNumber() != null && !TextUtils.isEmpty(allSerialModel.getSerialNumber())) {
                        if (allSerialModel.getToNumber() != null && !TextUtils.isEmpty(allSerialModel.getToNumber())) {
                            allSerialModels.add(allSerialModel);
                            int quantity = 0;
                            for (int i = 0; i < allSerialModels.size(); i++) {
                                if (allSerialModels.get(i).getSerialNumber() != null && !TextUtils.isEmpty(allSerialModels.get(i).getSerialNumber())
                                        && allSerialModels.get(i).getToNumber() != null && !TextUtils.isEmpty(allSerialModels.get(i).getToNumber())) {
                                    BigInteger fromSerials = new BigInteger(allSerialModels.get(i).getSerialNumber());
                                    BigInteger toSerials = new BigInteger(allSerialModels.get(i).getToNumber());
                                    BigInteger result = toSerials.subtract(fromSerials);
                                    quantity = quantity + result.intValue() + 1;
                                    if (result.intValue() < 0) {
                                        completeSerials = false;
                                    }
                                }
                            }
                            if (quantity > 0 && quantity <= serialsQuantity && completeSerials) {
                                if (valiantRangeSerial(allSerialModels.get(allSerialModels.size() - 1))) {
                                    getSerialsData.getSerialsData(allSerialModels, quantity);
                                    dismiss();
                                } else {
                                    if (editMode)
                                        allSerialModels.add(position, editModeSerial);
                                    allSerialModels.remove(allSerialModel);
                                    Toast.makeText(getContext(), "The serial entered already exists", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (editMode)
                                    allSerialModels.add(position,editModeSerial);
                                allSerialModels.remove(allSerialModel);
                                Toast.makeText(getContext(), "The to serials entered is less than the From Serial", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            if (editMode)
                                allSerialModels.add(position,editModeSerial);
                            toNumberScanEditText.setErrorMessage("please enter  to serial");
                            toNumberScanEditText.validate();

                        }

                    } else {
                        if (editMode)
                            allSerialModels.add(position,editModeSerial);
                        fromNumberScanEditText.setErrorMessage("please enter from serial");
                        fromNumberScanEditText.validate();
                    }

                } else {
                    if (allSerialModel.getSerialNumber() != null && !TextUtils.isEmpty(allSerialModel.getSerialNumber())) {
                        if (allSerialModel.getSerialNumber().equals(serialsQuantity + "")) {
                            allSerialModels.add(allSerialModel);
                            getSerialsData.getSerialsData(allSerialModels, Integer.parseInt(allSerialModel.getSerialNumber()));
                            dismiss();
                        } else {
                            if (editMode)
                                allSerialModels.add(position,editModeSerial);
                            fromNumberScanEditText.setErrorMessage("the entered number is is not equal the required quantity !");
                            fromNumberScanEditText.validate();
                        }
                    } else {
                        if (editMode)
                            allSerialModels.add(position,editModeSerial);
                        fromNumberScanEditText.setErrorMessage("please enter quantity");
                        fromNumberScanEditText.validate();

                    }
                }
                break;
        }
    }

    private boolean findItemInTheList(AllSerialModel serialModel) {
        for (int i = 0; i < allSerialModels.size(); i++) {
            if (allSerialModels.get(i).getSerialNumber().equals(serialModel.getSerialNumber())) {
                return true;
            }
        }
        return false;
    }

    private boolean valiantRangeSerial(AllSerialModel addSerialModel) {
        if (allSerialModels.size() == 1) {
            return true;
        } else {
            BigInteger addFromNumber = new BigInteger(addSerialModel.getSerialNumber());
            BigInteger addToNumber = new BigInteger(addSerialModel.getToNumber());
            BigInteger numberItem = addToNumber.subtract(addFromNumber);
            List<BigInteger> addSerial = new ArrayList<>();
            addSerial.add(addFromNumber);
            for (int i = 0; i < numberItem.intValue(); i++) {
                addSerial.add(addFromNumber.add(BigInteger.valueOf(i + 1)));
            }
            for (int i = 0; i < allSerialModels.size() - 1; i++) {
                BigInteger fromNumber = new BigInteger(allSerialModels.get(i).getSerialNumber());
                BigInteger toNumber = new BigInteger(allSerialModels.get(i).getToNumber());
                BigInteger cuntItem = toNumber.subtract(fromNumber);
                List<BigInteger> serials = new ArrayList<>();
                serials.add(fromNumber);
                for (int j = 0; j < cuntItem.intValue(); j++) {
                    serials.add(fromNumber.add(BigInteger.valueOf(i + 1)));
                }
                for (int k = 0; k < serials.size(); k++) {
                    for (int j = 0; j < addSerial.size(); j++) {
                        if (addSerial.get(j).equals(serials.get(k))) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
