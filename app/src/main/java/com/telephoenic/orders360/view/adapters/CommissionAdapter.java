package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.telephoenic.orders360.R;

import java.util.List;
import java.util.Locale;

public class CommissionAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private List<String> data;

    //View.OnClickListener clickListener ;

    public static Locale sysLocale;

    public CommissionAdapter(Context context, int textViewResourceId, List<String> objects ) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
       // this.clickListener = clickListener;
        sysLocale = mContext.getResources().getConfiguration().locale;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
//        if (convertView == null) {
            row = LayoutInflater.from(mContext).inflate(R.layout.item_lookup, null);

        final TextView commissionValue =  row.findViewById(R.id.spinnertext);

        commissionValue.setTag(position);
        commissionValue.setText(data.get(position));

      /*  commissionValue.setOnClickListener(clickListener);
        vendorImage.setOnClickListener(clickListener);*/


        return row;
    }

    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
