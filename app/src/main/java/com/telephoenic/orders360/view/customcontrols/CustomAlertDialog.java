package com.telephoenic.orders360.view.customcontrols;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.utils.AppUtils;

public class CustomAlertDialog extends AlertDialog {
    private Context context;
    private String message;
    private int titleResId;
    private int negativeResId, positiveResId;
    private View.OnClickListener onClickListener;

    public CustomAlertDialog(Context context, int titleResId, String message, int negativeResId, int positiveResId, View.OnClickListener onClickListener) {
        super(context);
        this.context = context;
        this.titleResId = titleResId;
        this.message = message;
        this.negativeResId = negativeResId;
        this.positiveResId = positiveResId;
        this.onClickListener = onClickListener;
    }

    public CustomAlertDialog(Context context, int titleResId, int messageResId, int negativeResId, int positiveResId, View.OnClickListener onClickListener) {
        this(context, titleResId, context.getString(messageResId), negativeResId, positiveResId, onClickListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_close_message, null);
        setContentView(dialogView);
        setCancelable(false);

//        if(AppUtils.getLayoutDirection(((Activity)context)).equals(AppConstants.RIGHT_TO_LEFT)){
//            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
//        }

        TextView titleTextView =  dialogView.findViewById(R.id.txt_title);
        titleTextView.setText(titleResId);

        TextView messageTextView =  dialogView.findViewById(R.id.txt_message);
        messageTextView.setText(message);

        Button negativeButton =  dialogView.findViewById(R.id.btn_negative);
        if (negativeResId == 0) {
            negativeButton.setVisibility(View.GONE);
        } else {
            negativeButton.setText(negativeResId);
            negativeButton.setVisibility(View.VISIBLE);
            negativeButton.setText(negativeResId);
            negativeButton.setOnClickListener(onDialogButtonClicked);
        }

        Button positiveButton =  dialogView.findViewById(R.id.btn_positive);
        if (positiveResId == 0) {
            positiveButton.setVisibility(View.GONE);
        } else {
            positiveButton.setText(positiveResId);
            positiveButton.setVisibility(View.VISIBLE);
            positiveButton.setText(positiveResId);
            positiveButton.setOnClickListener(onDialogButtonClicked);
        }
        getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
    }

    private View.OnClickListener onDialogButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
            dismiss();
        }
    };
}