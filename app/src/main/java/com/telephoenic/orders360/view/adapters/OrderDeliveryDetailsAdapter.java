package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.ShipItemsModel;
import com.telephoenic.orders360.controller.viewholder.MyOrderDetailsHolder;

import java.util.List;
import java.util.Locale;

public class OrderDeliveryDetailsAdapter extends RecyclerView.Adapter<MyOrderDetailsHolder> {
    private Context context;
    private OnRecyclerClick onRecyclerClick;
    private List<ShipItemsModel> data;

    public OrderDeliveryDetailsAdapter(Context context, OnRecyclerClick onRecyclerClick, List<ShipItemsModel> data) {
        this.context = context;
        this.onRecyclerClick = onRecyclerClick;
        this.data = data;

    }

    @NonNull
    @Override
    public MyOrderDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_my_order_detials, parent, false);
        return new MyOrderDetailsHolder(myRoot);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyOrderDetailsHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.nextRowMyOrderImageView.setVisibility(View.GONE);
        if (position == data.size() - 1) {
            holder.lineRowMyOrderView.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.getPosition(position);
            }
        });

        holder.itemPriceRowMyOrderTextView.setText(String.format("%s", ""));
        // holder.totalAmountRowMyOrderTextView.setText(String.format("%s2f", totalAmount));
        holder.totalAmountRowMyOrderTextView.setText(String.format(" %s %s", String.format(Locale.ENGLISH, "%.2f", data.get(position).getTotalAmount()), AppConstants.CURRENCY));

        holder.quantityRowMyOrderTextView.setText(String.format("%s", data.get(position).getShipmentQuantity()));
        holder.orderNameRowMyOrderTextView.setText(String.format("%s", data.get(position).getItemName()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
