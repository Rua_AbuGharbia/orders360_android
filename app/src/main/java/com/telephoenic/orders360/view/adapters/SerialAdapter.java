package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.typedef.SerialType;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.AllSerialModel;
import com.telephoenic.orders360.controller.viewholder.SingeSerialViewHolder;

import java.util.List;

public class SerialAdapter extends RecyclerView.Adapter<SingeSerialViewHolder> {
    private Context context;
    private List<AllSerialModel> data;
    private OnRecyclerClick onRecyclerClick;
    private OnRecyclerClick onRemoveItem;
    private OnRecyclerClick getSerial;
    private OnRecyclerClick getRangeSerial;
    private TextWatcher serialTextChange;
    private TextWatcher toTextChange;


    public SerialAdapter(Context context, List<AllSerialModel> data, OnRecyclerClick onRecyclerClick, OnRecyclerClick onRemoveItem
            , OnRecyclerClick getSerial, OnRecyclerClick getRangeSerial) {
        this.context = context;
        this.data = data;
        this.onRecyclerClick = onRecyclerClick;
        this.onRemoveItem = onRemoveItem;
        this.getSerial = getSerial;
        this.getRangeSerial = getRangeSerial;
    }

    @NonNull
    @Override
    public SingeSerialViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_single_serial_item, viewGroup, false);
        return new SingeSerialViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull final SingeSerialViewHolder singeSerialViewHolder, @SuppressLint("RecyclerView") final int i) {
        if (data.get(i).getSerialNumber() != null) {
            singeSerialViewHolder.numberScanEditText.setText(data.get(i).getSerialNumber());
        }
        singeSerialViewHolder.toNumberScanEditText.getContentEditText().setFocusable(false);
        singeSerialViewHolder.numberScanEditText.getContentEditText().setFocusable(false);
        singeSerialViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClick != null)
                    onRecyclerClick.getPosition(i);

            }
        });
        singeSerialViewHolder.toNumberScanEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClick != null)
                    onRecyclerClick.getPosition(i);

            }
        });
        singeSerialViewHolder.numberScanEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClick != null)
                    onRecyclerClick.getPosition(i);

            }
        });

        if (data.get(i).getSerialType() == SerialType.FROM_TO) {
            singeSerialViewHolder.toNumberScanEditText.setText(data.get(i).getToNumber());
            singeSerialViewHolder.fromTextView.setText("From");
            singeSerialViewHolder.fromTextView.setVisibility(View.VISIBLE);
            singeSerialViewHolder.toNumberScanEditText.setVisibility(View.VISIBLE);
            singeSerialViewHolder.toTextView.setVisibility(View.VISIBLE);
        } else if (data.get(i).getSerialType() == SerialType.SINGLE) {
            singeSerialViewHolder.fromTextView.setVisibility(View.GONE);
            singeSerialViewHolder.toNumberScanEditText.setVisibility(View.GONE);
            singeSerialViewHolder.toTextView.setVisibility(View.GONE);
        } else {
            singeSerialViewHolder.fromTextView.setVisibility(View.VISIBLE);
            singeSerialViewHolder.fromTextView.setText("Quantity");
            singeSerialViewHolder.scanImageView.setVisibility(View.GONE);
            singeSerialViewHolder.toNumberScanEditText.setVisibility(View.GONE);
            singeSerialViewHolder.toTextView.setVisibility(View.GONE);
        }
        singeSerialViewHolder.cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRemoveItem != null)
                    onRemoveItem.getPosition(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
