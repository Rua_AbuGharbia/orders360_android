package com.telephoenic.orders360.view.customcontrols;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import java.util.Objects;

public class WarningDialog {

    private static Dialog dialog;
    private static OnOkClickListener listener;

    public interface OnOkClickListener{
        void onOkClicked();
    }

    public static void show(Context context, String title,String message,OnOkClickListener listener) {

        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.warning_dialog);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

//        TextView warningTitleTextView = dialog.findViewById(R.id.warning_title_textView);
        TextView warningMessageTextView = dialog.findViewById(R.id.warning_message_textView);

//        warningTitleTextView.setText(title);
        warningMessageTextView.setText(message);

        final OnOkClickListener mlistener = listener;
        (dialog.findViewById(R.id.warning_dialog_ok_Button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mlistener .onOkClicked();
                    }
                });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}