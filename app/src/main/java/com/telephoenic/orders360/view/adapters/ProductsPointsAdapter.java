package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ProductsPointsModel;
import com.telephoenic.orders360.model.AppPrefs;

import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProductsPointsAdapter extends ArrayAdapter<ProductsPointsModel> {
    private Context mContext;
    private List<ProductsPointsModel> data;
    private View.OnClickListener onClickListener;
    private String signature;

    public static Locale sysLocale;

    public ProductsPointsAdapter(Context context, int textViewResourceId, List<ProductsPointsModel> objects, View.OnClickListener onClickListener,String signature) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
        this.onClickListener = onClickListener;
        this.signature =signature;
        sysLocale = mContext.getResources().getConfiguration().locale;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
//        if (convertView == null) {
        row = LayoutInflater.from(mContext).inflate(R.layout.item_product_points, null);

        final TextView productName = row.findViewById(R.id.text_product_name);
        final TextView productPointsText = row.findViewById(R.id.text_product_points);
        final Button productPoints = row.findViewById(R.id.button_product_points);
        final ImageView productImage = row.findViewById(R.id.image_product);
        final ConstraintLayout layout = row.findViewById(R.id.layout);
        final LinearLayout linearLayout = row.findViewById(R.id.linerLayout_item_points);
        // if you don't want the animation you can just comment the below line.
//        linearLayout.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_scale_animation));

//        if(position%2 == 0){
//          layout.setBackgroundColor(mContext.getResources().getColor(R.color.odd_background));
//        }

        productName.setTag(position);
        productPointsText.setTag(position);
        productPoints.setTag(position);

        productName.setText(data.get(position).getName());
        productPointsText.setText(data.get(position).getCostInRewardPoints() + " Pts");

        productName.setOnClickListener(onClickListener);
        productPointsText.setOnClickListener(onClickListener);
        productPoints.setOnClickListener(onClickListener);

        try {
            //TODO set Image from server


            String imagesName = data.get(position).getImagesName();
            if (imagesName.contains(";")) {
                imagesName = imagesName.split(";")[0];
            }
            String imageID= data.get(position).getId();
            if (imagesName != null && !TextUtils.isEmpty(imagesName)) {
                // new DownloadImageTask(holder.productImage).execute(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName);
                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE + "product/attachments/" + imageID + "/" + imagesName)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .crossFade(800)
                        .error(R.drawable.place_holder_order)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(productImage);
            } else {
                Glide.with(mContext).load(AppConstants.URL_LIVE + "vendor/attachments/" + AppPrefs.getVendorID(mContext) + "/" + AppPrefs.getVendorLogo(mContext))
                        .error(R.drawable.place_holder_order)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .into(productImage);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public ProductsPointsModel getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }
}