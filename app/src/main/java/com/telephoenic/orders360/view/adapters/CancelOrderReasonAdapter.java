package com.telephoenic.orders360.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.viewmodel.CancelOrderReasonViewHolder;

import java.util.List;

public class CancelOrderReasonAdapter extends RecyclerView.Adapter<CancelOrderReasonViewHolder> {
    private List<OrderRejectedReason> data;
    private OnRecyclerClick onRecyclerClick;

    public CancelOrderReasonAdapter(List<OrderRejectedReason> data, OnRecyclerClick onRecyclerClick) {
        this.data = data;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public CancelOrderReasonViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myRoot = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cancel_order_reason, viewGroup, false);
        return new CancelOrderReasonViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull CancelOrderReasonViewHolder cancelOrderReasonViewHolder, final int i) {
        cancelOrderReasonViewHolder.reasonNameRadioButton.setText(data.get(i).getName());
        if (data.get(i).getSelect() != null && data.get(i).getSelect()) {
            cancelOrderReasonViewHolder.reasonNameRadioButton.setChecked(true);
        } else {
            cancelOrderReasonViewHolder.reasonNameRadioButton.setChecked(false);
        }
        cancelOrderReasonViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClick != null)
                    onRecyclerClick.getPosition(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
