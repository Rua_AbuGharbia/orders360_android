package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.constraint.Group;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.basemodel.data.network.ApiObserver;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.SettingsDialogModel;
import com.telephoenic.orders360.controller.server.model.UserPassword;
import com.telephoenic.orders360.view.orders360view.BindInput;
import com.telephoenic.orders360.view.orders360view.Orders360EditText;
import com.telephoenic.orders360.view.orders360view.OrdersView;
import com.telephoenic.orders360.viewmodel.UserViewModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog.PHASE.PHASE_ONE;
import static com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog.PHASE.PHASE_THREE;
import static com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog.PHASE.PHASE_TOW;
import static com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog.validCodeStatus.EXPIRE;
import static com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog.validCodeStatus.INVALID;
import static com.telephoenic.orders360.view.customcontrols.ForgetPasswordDialog.validCodeStatus.VALID;

public class ForgetPasswordDialog extends DialogFragment implements DialogUtils.OnDialogClickListener
         {
    @BindView(R.id.Title_ForgetPassword_TestView)
    TextView titleForgetPasswordTestView;
    @BindView(R.id.Description_ForgetPassword_TextView)
    TextView descriptionForgetPasswordTextView;
    @BindInput(R.id.PhoneNumber_ForgetPassword_EditText)
    Orders360EditText phoneNumberForgetPasswordEditText;
    @BindInput(R.id.Code_ForgetPassword_EditText)
    Orders360EditText codeForgetPasswordEditText;
    @BindInput(R.id.NewPassword_ForgetPassword_EditText)
    Orders360EditText newPasswordForgetPasswordEditText;
    @BindInput(R.id.ConfirmPassword_ForgetPassword_EditText)
    Orders360EditText confirmPasswordForgetPasswordEditText;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;
    @BindView(R.id.NewPassword_Group)
    Group vewPasswordGroup;
    @BindView(R.id.ResendCode_Group)
    Group resendCodeGroup;
    @BindView(R.id.PhoneNumber_Group)
    Group phoneNumberGroup;
    @BindView(R.id.ResendCode_ForgetPassword_TextView)
    TextView resendCodeForgetPasswordTextView;
    private Dialog dialog;
    private ConfirmOrderDialog.onClickStatus onClickStatus;
    private int phase = PHASE.PHASE_ONE;
    private Animation animation;
    private UserViewModel userViewModel;
    private String mobileNumber;
    private int counter;
    CountDownTimer cTimer = null;
    private boolean reSendAttive = false;
    private OrdersView ordersView;
    private String myService ="";
    private UserPassword userPassword;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.forgetpassword_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            ordersView = new OrdersView();
            ordersView.bind(this, dialog.getWindow().getDecorView());
            ButterKnife.bind(this, dialog);
            animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_scale_animation);
            userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
            swapPhase(PHASE_ONE);
        }
        return dialog;
    }

    @OnClick({R.id.ResendCode_ForgetPassword_TextView, R.id.Submit_ForgetPassword_TextView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ResendCode_ForgetPassword_TextView:
                if (reSendAttive) {
                    reSendAttive = false;
                    phase = PHASE_ONE;
                    checkMobileNumber(mobileNumber);
                }
                break;
            case R.id.Submit_ForgetPassword_TextView:
                submitClick();
                break;
        }
    }

    public interface onClickStatus {
        void getStatusClick(int Status);
    }

    @IntDef({PHASE_ONE, PHASE_TOW, PHASE_THREE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PHASE {
        int PHASE_ONE = 0;
        int PHASE_TOW = 1;
        int PHASE_THREE = 2;
    }

    @StringDef({VALID, INVALID, EXPIRE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface validCodeStatus {
        String VALID = "1";
        String INVALID = "2";
        String EXPIRE = "3";
    }


    public ConfirmOrderDialog.onClickStatus onClickStatus(ConfirmOrderDialog.onClickStatus onClickStatus) {
        return this.onClickStatus = onClickStatus;
    }

    private void swapPhase(@PHASE int phase) {
        switch (phase) {
            case PHASE_ONE:
                phoneNumberGroup.setVisibility(View.VISIBLE);
                resendCodeGroup.setVisibility(View.GONE);
                vewPasswordGroup.setVisibility(View.GONE);
                titleForgetPasswordTestView.setText(getString(R.string.forget_password));
                descriptionForgetPasswordTextView.setText(getString(R.string.verification_message_text));
                break;
            case PHASE_TOW:
                phoneNumberGroup.setVisibility(View.GONE);
                resendCodeGroup.setVisibility(View.VISIBLE);
                vewPasswordGroup.setVisibility(View.GONE);
                descriptionForgetPasswordTextView.setText(getString(R.string.verification_msg_text));
                titleForgetPasswordTestView.setText(getString(R.string.verification_title_text));
                cTimer = new CountDownTimer(60000, 1000) {
                    @SuppressLint("SetTextI18n")
                    public void onTick(long millisUntilFinished) {
                        resendCodeForgetPasswordTextView.setText(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished * 2) + ":" + TimeUnit.MILLISECONDS.toSeconds((millisUntilFinished)));
                        counter++;
                    }

                    public void onFinish() {
                        resendCodeForgetPasswordTextView.setText(getString(R.string.resend_code));
                        reSendAttive = true;
                    }
                };
                cTimer.start();
                break;
            case PHASE_THREE:
                phoneNumberGroup.setVisibility(View.GONE);
                resendCodeGroup.setVisibility(View.GONE);
                vewPasswordGroup.setVisibility(View.VISIBLE);
                descriptionForgetPasswordTextView.setVisibility(View.GONE);
                vewPasswordGroup.setAnimation(animation);
                break;
        }
    }

    private void checkMobileNumber(String mobileNumber) {
        myService = AppConstants.SERVICE_CHECK_MOBILE_NUMBER;
        spinKit.setVisibility(View.VISIBLE);
        userViewModel.checkMobileNumber(mobileNumber, getContext(), dialog.getOwnerActivity()).observe(this, new ApiObserver<Boolean>() {
            @Override
            protected void noConnection() {
                spinKit.setVisibility(View.GONE);
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(Boolean data) {
                spinKit.setVisibility(View.GONE);
                phase = PHASE.PHASE_TOW;
                swapPhase(phase);
            }

            @Override
            protected void onError(String error) {
                spinKit.setVisibility(View.GONE);
                if (error != null)
                    Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void submitClick() {
        switch (phase) {
            case PHASE_ONE:
                if (TextUtils.isEmpty(phoneNumberForgetPasswordEditText.getEditTextContent())) {
                    phoneNumberForgetPasswordEditText.setErrorMessage(getResources().getString(R.string.mobile_number_requird));
                    phoneNumberForgetPasswordEditText.validate();
                    phoneNumberForgetPasswordEditText.requestFocus();
                } else if (phoneNumberForgetPasswordEditText.getEditTextContent().length() < 10) {
                    phoneNumberForgetPasswordEditText.setErrorMessage(getResources().getString(R.string.required_mobile_number_length));
                    phoneNumberForgetPasswordEditText.validate();

                    phoneNumberForgetPasswordEditText.requestFocus();
                } else {
                    mobileNumber = phoneNumberForgetPasswordEditText.getEditTextContent().toString();
                    checkMobileNumber(phoneNumberForgetPasswordEditText.getEditTextContent().toString());
                }
                break;
            case PHASE_TOW:
                if (TextUtils.isEmpty(codeForgetPasswordEditText.getEditTextContent().toString())) {
                    codeForgetPasswordEditText.setErrorMessage(getString(R.string.title_pin_code));
                    codeForgetPasswordEditText.validate();
                } else {
                    sendCode(mobileNumber, Long.valueOf(codeForgetPasswordEditText.getEditTextContent().toString()));
                }
                break;
            case PHASE_THREE:
                if (TextUtils.isEmpty(newPasswordForgetPasswordEditText.getEditTextContent().toString())) {
                    newPasswordForgetPasswordEditText.setErrorMessage(getString(R.string.enter_new_password_text));
                    newPasswordForgetPasswordEditText.validate();
                } else if (TextUtils.isEmpty(confirmPasswordForgetPasswordEditText.getEditTextContent().toString())) {
                    confirmPasswordForgetPasswordEditText.setErrorMessage(getString(R.string.enter_confirm_password));
                    confirmPasswordForgetPasswordEditText.validate();
                } else if (newPasswordForgetPasswordEditText.getEditTextContent().toString().equals(confirmPasswordForgetPasswordEditText.getEditTextContent().toString())) {
                    userPassword = new UserPassword();
                    userPassword.setMobileNo(mobileNumber);
                    userPassword.setNewPassword(newPasswordForgetPasswordEditText.getEditTextContent().toString());
                    changePassword(userPassword);
                }
                break;
        }
    }

    private void sendCode(String mobileNumber, Long otp) {
        myService = AppConstants.SERVICE_SEND_PASS_CODE;
        spinKit.setVisibility(View.VISIBLE);
        userViewModel.sendPassCode(mobileNumber, otp, getContext(), dialog.getOwnerActivity()).observe(this, new ApiObserver<String>() {
            @Override
            protected void noConnection() {
                spinKit.setVisibility(View.GONE);
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(String data) {
                spinKit.setVisibility(View.GONE);
                switch (data) {
                    case VALID:
                        phase = PHASE.PHASE_THREE;
                        swapPhase(phase);
                        break;
                    case EXPIRE:
                    case INVALID:
                        codeForgetPasswordEditText.setErrorMessage(getString(R.string.incorrect_code));
                        codeForgetPasswordEditText.validate();
                        break;
                }
            }

            @Override
            protected void onError(String error) {
                spinKit.setVisibility(View.GONE);
                if (error != null)
                    Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changePassword(UserPassword userPassword) {
        myService = AppConstants.SERVICE_CHANGE_PASSWORD;
        spinKit.setVisibility(View.VISIBLE);
        userViewModel.changePassword(userPassword, getContext(), dialog.getOwnerActivity()).observe(this, new ApiObserver<Boolean>() {
            @Override
            protected void noConnection() {
                spinKit.setVisibility(View.GONE);
                noConnectionInternetDialog();
            }

            @Override
            protected void onSuccess(Boolean data) {
                spinKit.setVisibility(View.GONE);
                Toast.makeText(getContext(), R.string.change_password_successfully, Toast.LENGTH_SHORT).show();
                dismiss();
            }

            @Override
            protected void onError(String error) {
                spinKit.setVisibility(View.GONE);
                if (error != null)
                    Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cTimer != null)
            cTimer.cancel();
    }

    @Override
    public void onOkClicked() {
        getActivity().startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), AppConstants.OPEN_WIFI_SETTINGS_REQUEST);
    }

    @Override
    public void onCancelClicked() {
//        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.OPEN_WIFI_SETTINGS_REQUEST) {
            if(AppConstants.SERVICE_CHANGE_PASSWORD.equals(myService)){
                changePassword(userPassword);
            }else if(AppConstants.SERVICE_CHECK_MOBILE_NUMBER.equals(myService)){
                checkMobileNumber(mobileNumber);
            }else if(AppConstants.SERVICE_SEND_PASS_CODE.equals(myService)){
                sendCode(mobileNumber, Long.valueOf(codeForgetPasswordEditText.getEditTextContent().toString()));
            }
        }
    }

    private void noConnectionInternetDialog() {
        SettingsDialogModel settingsDialogModel = new SettingsDialogModel(getString(R.string.internet_connection_dialog_titel), getString(R.string.internet_connection_dialog_message), R.drawable.noiternetconnection);
        DialogUtils.showSettingsDialog(getContext(), settingsDialogModel, this);
    }
}