package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.PosProfileWrapper;
import com.telephoenic.orders360.controller.server.model.TicketModel;
import com.telephoenic.orders360.controller.utils.AppUtils;
import com.telephoenic.orders360.controller.utils.DateUtil;

import java.util.List;
import java.util.Locale;

public class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.MyViewHolder> {

    private Context mContext;
    private List<TicketModel> data;
    private OnRecyclerClick clickListener;
    PosProfileWrapper posProfileWrapper;


    public static Locale sysLocale;

    private boolean isChanged = true;

    public TicketsAdapter(Context context, int textViewResourceId, List<TicketModel> objects, OnRecyclerClick clickListener) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        //  sysLocale = mContext.getResources().getConfiguration().locale;
        posProfileWrapper = AppUtils.getProfile(context);
    }

    public TicketsAdapter(Context context, int textViewResourceId, List<TicketModel> objects) {
        mContext = context;
        data = objects;
        //  sysLocale = mContext.getResources().getConfiguration().locale;
        posProfileWrapper = AppUtils.getProfile(context);
    }

    public void notifyOnQuantityChanged(int position) {
        isChanged = false;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final TicketModel ticketModel = data.get(position);

        holder.setIsRecyclable(false);
        /*if(position%2 == 0){
            holder.layout.setBackgroundColor(mContext.getResources().getColor(R.color.odd_background));
        }*/

        //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        //String dateString =  formatter.format(new Date(Long.valueOf(data.get(position).getAudit().getCreatedDate())));

        String dateString = DateUtil.formatDate(DateUtil.parseDate((data.get(position).getAudit().getCreatedDate())));
        if (TextUtils.isEmpty(dateString)) {
            holder.createDateTitle.setVisibility(View.GONE);
        }


        holder.createDate.setText(dateString);
        if (ticketModel.getSupportTicketType() != null)
            holder.ticketType.setText(ticketModel.getSupportTicketType().getName());
        holder.name.setText(ticketModel.getSubject());
        holder.subject.setText(ticketModel.getPmsg().trim());

        if (ticketModel.getTicketStatus().getCode().equals(AppConstants.TICKET_REPLIED_CODE)) {
            holder.statusTextView.setTextColor(mContext.getResources().getColor(R.color.verified_green2));
            holder.statusTextView.setText(mContext.getString(R.string.label_replied));
            holder.statusImage.setImageResource(R.drawable.ic_mail);
            //String replayString =  formatter.format(new Date(Long.valueOf(data.get(position).getAudit().getUpdatedDate())));
            String replayString = DateUtil.formatDate(DateUtil.parseDate(data.get(position).getAudit().getUpdatedDate()));
            holder.replyDate.setText(replayString);
//            holder.statusImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.replied));
        } else if (ticketModel.getTicketStatus().getCode().equals(AppConstants.TICKET_NOT_REPLIED_CODE)) {
            holder.dateTitle.setVisibility(View.GONE);
            holder.replyDate.setVisibility(View.GONE);
            holder.statusTextView.setTextColor(mContext.getResources().getColor(R.color.red_300));
            holder.statusTextView.setText(mContext.getResources().getString(R.string.not_replied));
            holder.statusImage.setImageResource(R.drawable.not_replay_ic);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.getPosition(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView name, subject, statusTextView, createDate, createDateTitle, replyDate, dateTitle, ticketType;
        //        final ImageView /*ticketImage ,*/ statusImage;
//        final Button statusButton;
        final CardView layout;
        ImageView statusImage;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.text_vendor_name);
            ticketType = view.findViewById(R.id.TicketType_TextView);
            createDate = view.findViewById(R.id.TicketCreateDate_TextView);
            replyDate = view.findViewById(R.id.TicketReplyDate_TextView);
            dateTitle = view.findViewById(R.id.ReplyTilteDate_TextView);
            createDateTitle = view.findViewById(R.id.CreateDateTitle);
//          ticketImage= view.findViewById(R.id.image_ticket);
//            statusImage = view.findViewById(R.id.image_status);
            subject = view.findViewById(R.id.text_subject);
//            statusButton = view.findViewById(R.id.button_sent);
            statusTextView = view.findViewById(R.id.statusTextView);
            layout = view.findViewById(R.id.layout);
            statusImage = view.findViewById(R.id.StatusImage_ImageView);
        }
    }
}