package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.server.model.CommissionQtyToValueTransient;

import java.util.List;

/**
 * Created by Rua on 24-Oct-18.
 */

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.MyViewHolder>  {

    private Context mContext;
    private List<CommissionQtyToValueTransient> data;
    private View.OnClickListener clickListener ;
    private TextWatcher textWatcher ;

    public PromotionAdapter(Context context, int textViewResourceId, List<CommissionQtyToValueTransient> objects , View.OnClickListener clickListener,TextWatcher textWatcher ) {
        mContext = context;
        data = objects;
        this.clickListener = clickListener;
        this.textWatcher = textWatcher;
    }

    @Override
    public PromotionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_promotion, parent, false);

        return new PromotionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PromotionAdapter.MyViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        holder.promotionValue.setTag(position);
        holder.imageMinus.setTag(position);
        holder.imagePlus.setTag(position);
        holder.promotionCount.setTag(position);

        final CommissionQtyToValueTransient commissionQtyToValueTransient = data.get(position);
        if(commissionQtyToValueTransient.getQuantity()!=null){
            holder.promotionValue.setText(commissionQtyToValueTransient.getQuantity()+"  +  "+commissionQtyToValueTransient.getValue()+
                    " "+mContext.getResources().getString(R.string.fragment_product_free));
        } else {
            holder.promotionValue.setText(mContext.getResources().getString(R.string.label_commission_error));
            holder.imageMinus.setVisibility(View.INVISIBLE);
            holder.imagePlus.setVisibility(View.INVISIBLE);
            holder.promotionCount.setVisibility(View.INVISIBLE);
            //holder.top.setVisibility(View.INVISIBLE);
            //holder.bottom.setVisibility(View.INVISIBLE);
        }

        if(commissionQtyToValueTransient.getCount()!=null){
            holder.promotionCount.setText(commissionQtyToValueTransient.getCount());
        } else {
            holder.promotionCount.setText("0");
        }

        holder.promotionValue.setOnClickListener(clickListener);
        holder.imageMinus.setOnClickListener(clickListener);
        holder.imagePlus.setOnClickListener(clickListener);
        holder.promotionCount.setOnClickListener(clickListener);

        holder.promotionCount.addTextChangedListener(textWatcher);

        /*holder.promotionCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().equals("")){
                    promotionModel.setCount("0");
                } else {
                    promotionModel.setCount(s+"");
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView promotionValue ;
        final ImageView imagePlus , imageMinus;
        final EditText promotionCount ;
        //final LinearLayout top, bottom ;

        public MyViewHolder(View view) {
            super(view);
            promotionValue = view.findViewById(R.id.text_promotion_value);
            imagePlus = view.findViewById(R.id.image_plus);
            imageMinus = view.findViewById(R.id.image_minus);
            promotionCount = view.findViewById(R.id.purchases_num);
            //top = view.findViewById(R.id.linearLayout3);
            //bottom = view.findViewById(R.id.linearLayout4);
        }
    }
}
