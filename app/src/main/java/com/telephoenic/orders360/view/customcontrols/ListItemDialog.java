package com.telephoenic.orders360.view.customcontrols;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.view.adapters.ItemListDialogAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


@SuppressLint("ValidFragment")
public class ListItemDialog extends DialogFragment {
    public static final String DATA = " data";
    public static final String TITLE = "title";
    @BindView(R.id.Title_Dialog_TextView)
    TextView titleDialogTextView;
    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.TextEmptyData)
    TextView textEmptyData;
    private Dialog dialog;
    private List<Object> data = new ArrayList<>();
    private OnRecyclerClick onRecyclerClick;
    private String title;


    public ListItemDialog(OnRecyclerClick onRecyclerClick) {
        this.onRecyclerClick = onRecyclerClick;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null && getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.list_item_dialog);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.CENTER);
            }
            if (getArguments() != null) {
                data = (List<Object>) getArguments().getSerializable(DATA);
                title = getArguments().getString(TITLE);
            }
            ButterKnife.bind(this, dialog);
            inti();
        }
        return dialog;
    }

    private void inti() {
        titleDialogTextView.setText(title);
        if (data != null && data.size() > 0) {
            recyclerView.setAdapter(new ItemListDialogAdapter(getContext(), data, onRecyclerClick));
        } else {
            textEmptyData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }
}
