package com.telephoenic.orders360.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.BrandWrapper;
import com.telephoenic.orders360.controller.server.model.CategoryNodeWrapper;
import com.telephoenic.orders360.controller.viewholder.BrandFilterViewHolder;

import java.util.List;

public class BrandFilterAdapter extends RecyclerView.Adapter<BrandFilterViewHolder> {
    private Context context;
    private List<BrandWrapper> data;
    private OnItemClickListener onItemClickListener;
    private String signature;

    public BrandFilterAdapter(Context context, List<BrandWrapper> data,String signature) {
        this.context = context;
        this.data = data;
        this.signature =signature;
    }

    @NonNull
    @Override
    public BrandFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_brand_filter, parent, false);
        return new BrandFilterViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull BrandFilterViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.brandNameRowBrandTextView.setText(data.get(position).getName());
        if (data.get(position).getSelect() != null && data.get(position).getSelect()) {
            holder.brandCheck.setChecked(true);
        } else {
            holder.brandCheck.setChecked(false);

        }

        if (data.get(position).getImageName() != null) {
            if (data.get(position).getImageName().contains(";")) {
                data.get(position).setImageName(data.get(position).getImageName().split(";")[0]);
            }

            Glide.with(context)
                    .load(AppConstants.URL_LIVE + "brand/attachments/" + data.get(position).getId() + "/" + data.get(position).getImageName())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .crossFade(800)
                    .error(R.drawable.place_holder_order)
                    .signature(new StringSignature(String.valueOf(signature)))
                    .into(holder.brandLogoRowBrandCircleImageView);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null)
                    onItemClickListener.onItemClick(data, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnItemClickListener {
        void onItemClick(List<BrandWrapper> brandWrapperList, int position);
    }

    public void setOnItemClickListener(BrandFilterAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
