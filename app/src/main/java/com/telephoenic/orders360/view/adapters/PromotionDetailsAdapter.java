package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.viewholder.PromotionDetailsViewHolder;

public class PromotionDetailsAdapter extends RecyclerView.Adapter<PromotionDetailsViewHolder> {
    private Context context;

    public PromotionDetailsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public PromotionDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_promtion_details, parent, false);
        return new PromotionDetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PromotionDetailsViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
