package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.shiporder.CashPayments;
import com.telephoenic.orders360.controller.server.model.shiporder.ChequePaymentsModel;
import com.telephoenic.orders360.controller.server.model.shiporder.ClaimPayments;
import com.telephoenic.orders360.controller.viewholder.ChequeMethodViewHolder;
import com.telephoenic.orders360.controller.viewholder.RowCashAndClaimViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PaymentMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int CASH = 0;
    private final int CHEQUE = 1;
    private final int CLAIM = 2;
    private Context context;
    private List<Object> data;
    private OnRecyclerClick onRecyclerClick;
    private OnRecyclerClick deleteClick;

    public PaymentMethodAdapter(Context context, List<Object> data, OnRecyclerClick onRecyclerClick, OnRecyclerClick deleteClick) {
        this.context = context;
        this.data = data;
        this.onRecyclerClick = onRecyclerClick;
        this.deleteClick = deleteClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = null;
        switch (viewType) {
            case CASH:
            case CLAIM:
                itemView = LayoutInflater.from(context).inflate(R.layout.row_payment_method, viewGroup, false);
                return new RowCashAndClaimViewHolder(itemView);
            case CHEQUE:
                itemView = LayoutInflater.from(context).inflate(R.layout.row_payment_method, viewGroup, false);
                return new RowCashAndClaimViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int i) {
        switch (holder.getItemViewType()) {
            case CASH:
                RowCashAndClaimViewHolder cashAndClaimViewHolder = (RowCashAndClaimViewHolder) holder;
                CashPayments cashPayments = (CashPayments) data.get(i);
                cashAndClaimViewHolder.cashPaymentOrdersEditText.getContentEditText().setFocusable(false);
                cashAndClaimViewHolder.titlePaymentMethod.setText(context.getString(R.string.cash));
                cashAndClaimViewHolder.cashPaymentOrdersEditText.setIcon(R.drawable.money_ci);
                cashAndClaimViewHolder.cashPaymentOrdersEditText.setText(" " + String.format(Locale.ENGLISH, "%.2f", cashPayments.getAmount()) + " " + AppConstants.CURRENCY);
                cashAndClaimViewHolder.cashPaymentOrdersEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRecyclerClick != null)
                            onRecyclerClick.getPosition(i);
                    }
                });
                cashAndClaimViewHolder.deleteImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (deleteClick != null) {
                            deleteClick.getPosition(i);
                        }

                    }
                });
                break;

            case CLAIM:
                RowCashAndClaimViewHolder claimViewHolder = (RowCashAndClaimViewHolder) holder;
                ClaimPayments claimPayments = (ClaimPayments) data.get(i);
                claimViewHolder.cashPaymentOrdersEditText.getContentEditText().setFocusable(false);
                claimViewHolder.titlePaymentMethod.setText("Claim");
                claimViewHolder.cashPaymentOrdersEditText.setIcon(R.drawable.claim_ic);
                claimViewHolder.cashPaymentOrdersEditText.setText(" " + String.format(Locale.ENGLISH, "%.2f", claimPayments.getAmount()) + " " + AppConstants.CURRENCY);
                claimViewHolder.cashPaymentOrdersEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRecyclerClick != null)
                            onRecyclerClick.getPosition(i);
                    }
                });
                claimViewHolder.deleteImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (deleteClick != null) {
                            deleteClick.getPosition(i);
                        }

                    }
                });

                break;
            case CHEQUE:
                RowCashAndClaimViewHolder chequeViewHolder = (RowCashAndClaimViewHolder) holder;
                ChequePaymentsModel chequePaymentsModel = (ChequePaymentsModel) data.get(i);
                chequeViewHolder.cashPaymentOrdersEditText.getContentEditText().setFocusable(false);
                chequeViewHolder.titlePaymentMethod.setText("Cheque");
                chequeViewHolder.cashPaymentOrdersEditText.setIcon(R.drawable.cheque_ci);
                chequeViewHolder.cashPaymentOrdersEditText.setText(" " + String.format(Locale.ENGLISH, "%.2f", chequePaymentsModel.getAmount()) + " " + AppConstants.CURRENCY);
                chequeViewHolder.cashPaymentOrdersEditText.getContentEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRecyclerClick != null)
                            onRecyclerClick.getPosition(i);
                    }
                });

                chequeViewHolder.deleteImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (deleteClick != null) {
                            deleteClick.getPosition(i);
                        }

                    }
                });
                break;

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof CashPayments) {
            return CASH;
        } else if (data.get(position) instanceof ChequePaymentsModel) {
            return CHEQUE;
        } else if (data.get(position) instanceof ClaimPayments) {
            return CLAIM;
        }

        return super.getItemViewType(position);
    }
}
