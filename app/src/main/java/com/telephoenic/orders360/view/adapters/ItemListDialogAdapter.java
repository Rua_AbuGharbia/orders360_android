package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.interfaces.OnRecyclerClick;
import com.telephoenic.orders360.controller.server.model.CityModel;
import com.telephoenic.orders360.controller.server.model.GroupModel;
import com.telephoenic.orders360.controller.server.model.RegionModel;
import com.telephoenic.orders360.controller.server.model.SupportTicketTypeModel;
import com.telephoenic.orders360.controller.server.model.WarehouseModel;
import com.telephoenic.orders360.controller.server.model.shiporder.OrderRejectedReason;
import com.telephoenic.orders360.controller.viewholder.ItemListDialogViewHolder;

import java.util.List;

public class ItemListDialogAdapter extends RecyclerView.Adapter<ItemListDialogViewHolder> {
    private Context context;
    private List<Object> data;
    private OnRecyclerClick onRecyclerClick;

    public ItemListDialogAdapter(Context context, List<Object> data, OnRecyclerClick onRecyclerClick) {
        this.data = data;
        this.context = context;
        this.onRecyclerClick = onRecyclerClick;
    }

    @NonNull
    @Override
    public ItemListDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myRoot = LayoutInflater.from(context).inflate(R.layout.row_item_dialog, parent, false);
        return new ItemListDialogViewHolder(myRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemListDialogViewHolder holder, final int position) {
        if (data.get(position) instanceof CityModel) {
            CityModel cityModelList = ((CityModel) data.get(position));
            holder.nameItemTextView.setText(cityModelList.getName());
        }
        if (data.get(position) instanceof RegionModel) {
            RegionModel regionWrapper = ((RegionModel) data.get(position));
            holder.nameItemTextView.setText(regionWrapper.getName());
        }
        if (data.get(position) instanceof GroupModel) {
            GroupModel groupModel = ((GroupModel) data.get(position));
            holder.nameItemTextView.setText(groupModel.getName());
        }
        if (data.get(position) instanceof SupportTicketTypeModel) {
            SupportTicketTypeModel supportTicketTypeModel = ((SupportTicketTypeModel) data.get(position));
            holder.nameItemTextView.setText(supportTicketTypeModel.getName());
        }
        if (data.get(position) instanceof String) {
            String language = ((String) data.get(position));
            holder.nameItemTextView.setText(language);
        }
        if (data.get(position) instanceof OrderRejectedReason) {
            OrderRejectedReason reason = ((OrderRejectedReason) data.get(position));
            holder.nameItemTextView.setText(reason.getName());
        }
        if (data.get(position) instanceof GroupModel) {
            GroupModel groupModel = ((GroupModel) data.get(position));
            holder.nameItemTextView.setText(groupModel.getName());
        }

        if (data.get(position) instanceof WarehouseModel) {
            WarehouseModel warehouseModel = ((WarehouseModel) data.get(position));
            holder.nameItemTextView.setText(warehouseModel.getName());
        }

        if (position == data.size() - 1) {
            holder.lineView.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.getPosition(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

