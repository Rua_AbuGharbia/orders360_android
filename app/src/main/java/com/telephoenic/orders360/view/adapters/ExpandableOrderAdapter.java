package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.ComboInfoWrapper;
import com.telephoenic.orders360.controller.server.model.OrderHistoryModel;
import com.telephoenic.orders360.controller.server.model.OrderItemModel;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

public class ExpandableOrderAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<OrderItemModel> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<OrderItemModel, List<OrderHistoryModel>> _listDataChild;

	public ExpandableOrderAdapter(Context context, List<OrderItemModel> listDataHeader,
                                  HashMap<OrderItemModel, List<OrderHistoryModel>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

		final OrderHistoryModel child = (OrderHistoryModel) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.item_history, null);
		}
		TextView textDate =  convertView.findViewById(R.id.text_product_date);
		TextView textQuntity =  convertView.findViewById(R.id.text_product_quntity);
		LinearLayout layout = convertView.findViewById(R.id.layout);

		if(child.getId() == -1){
			textDate.setText(child.getDeliveryDate());
			layout.setBackgroundColor(_context.getResources().getColor(R.color.odd_background));
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String dateString = formatter.format(new Date(Long.valueOf(child.getDeliveryDate())));
			textDate.setText(dateString);
		}

		textQuntity.setText(child.getQuantity());

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
		OrderItemModel header = (OrderItemModel) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.order_items, null);
		}

		final TextView productName =  convertView.findViewById(R.id.text_product_name);
		final TextView quantity =  convertView.findViewById(R.id.text_product_quntity);
		final TextView itemPrice =  convertView.findViewById(R.id.text_item_price);
		final TextView totalAmount =  convertView.findViewById(R.id.text_product_total_amount);

		Integer deliveredQuantity = header.getDeliverdQuantity();
		if (deliveredQuantity==null){
			deliveredQuantity = 0;
		}
		Integer orderedQuantity = header.getOrderedQuantity();
		if (orderedQuantity==null){
			orderedQuantity = 0;
		}
		Integer allQuantity = deliveredQuantity+orderedQuantity;

		if(header.getPrice() == null){
			itemPrice.setText("0.0");
		} else {
			itemPrice.setText(header.getPrice()+"");
		}

		if(header.getProduct()!=null){
			productName.setText(header.getProduct().getName());
			/*if(header.getProduct().getPrice()!= null){  // TODO Product Price will be changed from server
				double productPrice = header.getProduct().getPrice() / allQuantity;
				itemPrice.setText((float)Math.round(productPrice * 100) / 100+" "+ AppConstants.CURRENCY);
			}*/
		} else if(header.getCombo()!=null) {
			productName.setText(header.getCombo().getName());
			List<ComboInfoWrapper> listOfComboInfo = header.getCombo().getListOfComboInfo();
            /*double totalPrice = 0.0;
            for(ComboInfoWrapper wrapper :listOfComboInfo){
                totalPrice+= wrapper.getPrice();
            }*/
			//itemPrice.setText((float)Math.round(header.getCombo().getPrice() * 100) / 100+" "+ AppConstants.CURRENCY);
		}

		quantity.setText(allQuantity+"");

		if(header.getTotalAmount()!=null){
			totalAmount.setText((float)Math.round(header.getTotalAmount() * 100) / 100+" "+AppConstants.CURRENCY);
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
