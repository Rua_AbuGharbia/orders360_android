package com.telephoenic.orders360.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.telephoenic.orders360.R;
import com.telephoenic.orders360.controller.constants.AppConstants;
import com.telephoenic.orders360.controller.server.model.VendorWrapper;

import java.util.List;

public class VendorAdapter extends ArrayAdapter<VendorWrapper> {
    private Context mContext;
    private List<VendorWrapper> data;
    private String signature ;

    public VendorAdapter(Context context, int textViewResourceId, List<VendorWrapper> objects,String signature ) {
        super(context, textViewResourceId, objects);
        mContext = context;
        data = objects;
        this.signature = signature;
    }

    @Override
    public int getCount() {
        return data.size();
    }
     @NonNull
    @Override
    public View getView(final int position, View convertView,@NonNull ViewGroup parent) {
        View row;
//        if (row == null) {
            row = LayoutInflater.from(mContext).inflate(R.layout.item_vendor, null);

        final TextView name =  row.findViewById(R.id.text_vendor_name);
        final ImageView vendorImage=  row.findViewById(R.id.image_vendor);

        name.setTag(position);
        name.setText(data.get(position).getName());


        try {
            //TODO set Image from server
            String imagesName = data.get(position).getLogoImageName();
            Long imageID = data.get(position).getId();
            if(imagesName!=null){
             //  new DownloadImageTask(vendorImage).execute(AppConstants.URL_LIVE+"vendor/attachments/"+imageID+"/"+imagesName);
                Glide.with(mContext)
                        .load(AppConstants.URL_LIVE+"vendor/attachments/"+imageID+"/"+imagesName)
                        .error(R.drawable.place_holder_blue)
                        .signature(new StringSignature(String.valueOf(signature)))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(vendorImage);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return row;
    }
    @Override
    public VendorWrapper getItem(int position) {
        return super.getItem(position);
    }

    public void removeItem(int position){
        data.remove(position);
        notifyDataSetChanged();
    }
}
